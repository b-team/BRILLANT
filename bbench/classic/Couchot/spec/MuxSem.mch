/*
a simple parameterized mutual exclusion protocol from 
Automatic Deductive Verification with Invisible Invariants
A. Pnueli, S. Ruah, and L. Zuck (TACAS01)
*/
MACHINE
  MuxSem
SETS
  status ={Ii,Tt,Cc,Ee};
  semValues = {tr,fa};
  pid
VARIABLES
  state, 
  sem
INVARIANT
  state : pid --> status &
  sem : semValues &
  (!(h1,h2).((h1: pid & h2 :pid & state(h1)=Cc & state(h2)=Cc)=> h1=h2))
INITIALISATION
  state := pid * {Ii} ||
  sem := tr 
OPERATIONS
  sb =
    ANY hh WHERE hh : pid & state(hh) = Ii THEN
     state := state <+ {hh |->Tt} 
    END ;
  sc = 
    ANY hh WHERE hh : pid & state(hh) = Tt & sem = tr THEN
     state := state <+ {hh |->Cc} ||
     sem := fa
    END;
  sd =
    ANY hh WHERE hh : pid & state(hh) = Cc THEN
     state := state <+ {hh |->Ee} 
    END ;
  se = 
    ANY hh WHERE hh : pid & state(hh) = Ee  THEN
     state := state <+ {hh |->Ii} ||
     sem := tr
    END
END
