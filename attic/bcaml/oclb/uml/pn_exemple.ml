(*** $Id ***)



(** {5 Fichier d'exemples de mod�les UML }
@author 
@version 
*)



open Uml





(* EXAMPLE 1  (Geometric figures) *)
				   
let fi_classes = [
  Class("Circle",
	Multiplicity(Range("0","50")),
	[],
	[],
	[Attribute("radius",Multiplicity(Range("1","")),Real,"")],
	[],
	[],
	[]);
  Class("Point",
	Multiplicity(Range("0","50")),
	[],
	[],
	[Attribute("px",Multiplicity(Range("1","")),Real,""); 
	 Attribute("py",
		   Multiplicity(Range("1","")),
		   Real,
		   "")],
	[],
	[],
	[]);
  Class("Polygon",
	Multiplicity(Range("0","50")),
	[],
	[],
        [],
	[],
	[],
	[])
]
		    
and fi_assocs = [
  Association("circle_point",
	      [Role("point_circle",
		    Ass_end,
		    Multiplicity(Range("0","*")),
		    Role_type "Circle");			      
	       Role("center",
		    Ass_end,
		    Multiplicity(Range("1","")),
		    Role_type "Point")]);
  Association("polygon_point",
	      [Role("point_polygon",
		    Ass_end,
		    Multiplicity(Range("0","*")),
		    Role_type "Polygon");
	       Role("vertices",
		    Ass_end,
		    Multiplicity(Range("3","")),
		    Role_type "Point")])
]



(* EXAMPLE 2  (Documentation 1.4) *)

and ee_classes = [
  Class("Person",
	Multiplicity(Range("0","50")),
	[],
	[],
	[Attribute("isMarried",Multiplicity(Range("1","")),Boolean,"");
	 Attribute("isUnemployed",Multiplicity(Range("1","")),Att_type "Date","");
	 Attribute("birthDate",Multiplicity(Range("1","")),Att_type "Date","");
	 Attribute("sex",Multiplicity(Range("1","")),Att_type "Sex","");
	 Attribute("lastName",Multiplicity(Range("1","")),Striing,"");
	 Attribute("firstName",Multiplicity(Range("1","")),Striing,"");
	 Attribute("age",Multiplicity(Range("1","")),Integer,"")],
	[],
	[],
	[]);
  Class("Company",
	Multiplicity(Range("0","50")),
	[],
	[],
	[Attribute("name",Multiplicity(Range("1","")),Striing,"");
	 Attribute("numberOfEmployees",Multiplicity(Range("1","")),Integer,"");
	 Attribute("stockPrice",Multiplicity(Range("1","")),Real,"")],
	[],
	[],
	[]);
  Class("Job",
	Multiplicity(Range("0","50")),
	[],
	[],
	[Attribute("title",Multiplicity(Range("1","")),Striing,"");
	 Attribute("startDate",Multiplicity(Range("1","")),Att_type "Date","");
	 Attribute("salary",Multiplicity(Range("1","")),Integer,"")],
	[],
	[],
	[])
]

and ee_assocs = [
  Association("job",
	      [Role("employees",
		    Ass_end,
		    Multiplicity(Range("0","*")),
		    Role_type "Person");
	       Role("employer",
		    Ass_end,
		    Multiplicity(Range("0","*")),
		    Role_type "Company")]);
  Association("manager_managedCompanies",
	      [Role("managedCompanies",
		    Ass_end,
		    Multiplicity(Range("0","*")),
		    Role_type "Company");
	       Role("manager",
		    Ass_end,
		    Multiplicity(Range("1","")),
		    Role_type "Person")]);
  Association("marriage",
	      [Role("wife",
		    Ass_end,
		    Multiplicity(Range("0","1")),
		    Role_type "Person");
	       Role("husband",
		    Ass_end,
		    Multiplicity(Range("0","1")),
		    Role_type "Person")])
]



(* EXAMPLE 3  (The Royal and Loyal) *)

and rl_classes = [
  Class("Customer",
	Multiplicity(Range("0","50")),
	[],
	[],
	[Attribute("name",Multiplicity(Range("1","")),Striing,"");
	 Attribute("title",Multiplicity(Range("1","")),Striing,"");
	 Attribute("isMale",Multiplicity(Range("1","")),Boolean,"");
	 Attribute("dateOfBirth",Multiplicity(Range("1","")),Att_type "Date","");
	 Attribute("age",Multiplicity(Range("1","")),Integer,"")],
	[],
	[],
	[]);
  Class("LoyaltyProgram",
	Multiplicity(Range("0","50")),
	[],
	[],
	[],
	[],
	[],
	[]);
  Class("Membership",
	Multiplicity(Range("0","50")),
	[],
	[],
	[],
	[],
	[],
	[]);
  Class("CustomerCard",
	Multiplicity(Range("0","50")),
	[],
	[],
	[Attribute("valid",Multiplicity(Range("1","")),Boolean,"");
	 Attribute("validFrom",Multiplicity(Range("1","")),Att_type "Date","");
	 Attribute("goodThru",Multiplicity(Range("1","")),Att_type "Date","");
	 Attribute("color",Multiplicity(Range("1","")),Att_type "enum{silver,gold}","");
	 Attribute("printedName",Multiplicity(Range("1","")),Striing,"")],
	[],
	[],
	[]);
  Class("LoyaltyAccount",
	Multiplicity(Range("0","50")),
	[],
	[],
	[Attribute("points",Multiplicity(Range("1","")),Integer,"")],
	[],
	[],
	[]);
  Class("Transaction",
	Multiplicity(Range("0","50")),
	[],
	[],
	[Attribute("points",Multiplicity(Range("1","")),Integer,"");
	 Attribute("date",Multiplicity(Range("1","")),Att_type "Date","")],
	[],
	[],
	[])
]
		   
and rl_assocs = [
  Association("membership",
	      [Role("program",
		    Ass_end,
		    Multiplicity(Range("0","*")),
		    Role_type "LoyaltyProgram");
	       Role("loyaltyProgram_customer",
		    Ass_end,
		    Multiplicity(Range("0","*")),
		    Role_type "Customer")]);
  Association("customer_customerCard",
	      [Role("owner",
		    Ass_end,
		    Multiplicity(Range("1","")),
		    Role_type "Customer");
	       Role("cards",
		    Ass_end,
		    Multiplicity(Range("0","*")),
		    Role_type "CustomerCard")]);
  Association("customerCard_transaction",
	      [Role("card",
		    Ass_end,
		    Multiplicity(Range("1","")),
		    Role_type "CustomerCard");
	       Role("transactions",
		    Ass_end,
		    Multiplicity(Range("0","*")),
		    Role_type "Transaction")]);
  Association("loyaltyAccount_transaction",
	      [Role("transaction_loyaltyAccount",
		    Ass_end,
		    Multiplicity(Range("1","")),
		    Role_type "LoyaltyAccount");
	       Role("transactions",
		    Ass_end,
		    Multiplicity(Range("0","*")),
		    Role_type "Transaction")]);
  Association("membership_customerCard",
	      [Role("customerCard_membership",
		    Ass_end,
		    Multiplicity(Range("1","")),
		    Role_type "Memebership");
	       Role("card",
		    Ass_end,
		    Multiplicity(Range("1","")),
		    Role_type "CustomerCard")]);
  Association("membership_loyaltyAccount",
	      [Role("loyaltyAccount_membership",
		    Ass_end,
		    Multiplicity(Range("1","")),
		    Role_type "Memebership");
	       Role("membership_loyaltyAccount",
		    Ass_end,
		    Multiplicity(Range("0","1")),
		    Role_type "LoyaltyAccount")])
]



(* EXAMPLE 4  (Company model) *)

and co_classes = [
  Class("Employee",
	Multiplicity(Range("0","50")),
	[],
	[],
	[Attribute("eName",Multiplicity(Range("1","")),Striing,"");
	 Attribute("salary",Multiplicity(Range("1","")),Integer,"")],
	[],
	[],
	[]);
  Class("Department",
	Multiplicity(Range("0","50")),
	[],
	[],
	[Attribute("dName",Multiplicity(Range("1","")),Striing,"");
	 Attribute("location",Multiplicity(Range("1","")),Striing,"");
	 Attribute("dBudget",Multiplicity(Range("1","")),Integer,"")],
	[],
	[],
	[]);
  Class("Project",
	Multiplicity(Range("0","50")),
	[],
	[],
	[Attribute("pName",Multiplicity(Range("1","")),Striing,"");
	 Attribute("pBudget",Multiplicity(Range("1","")),Integer,"")],
	[],
	[],
	[]);
  Class("Company",
	Multiplicity(Range("0","50")),
	[],
	[],
	[Attribute("cName",Multiplicity(Range("1","")),Striing,"");
	 Attribute("numberEmployees",Multiplicity(Range("1","")),Integer,"");
	 Attribute("stockPrice",Multiplicity(Range("1","")),Integer,"")],
	[],
	[],
	[]);
  Class("Job",
	Multiplicity(Range("0","50")),
	[],
	[],
	[Attribute("title",Multiplicity(Range("1","")),Striing,"");
	 Attribute("startDate",Multiplicity(Range("1","")),Att_type "Date","");
	 Attribute("salary",Multiplicity(Range("1","")),Integer,"")],
	[],
	[],
	[])
]
	    
and co_assocs = [
  Association("worksIn",
	      [Role("unit",
		    Ass_end,
		    Multiplicity(Range("1","*")),
		    Role_type "Department");
	       Role("depMember",
		    Ass_end,
		    Multiplicity(Range("0","*")),
		    Role_type "Employee")]);
  Association("worksOn",
	      [Role("task",
		    Ass_end,
		    Multiplicity(Range("0","*")),
		    Role_type "Project");
	       Role("projMember",
		    Ass_end,
		    Multiplicity(Range("0","*")),
		    Role_type "Employee")]);
  Association("controls",
	      [Role("mission",
		    Ass_end,
		    Multiplicity(Range("0","*")),
		    Role_type "Project");
	       Role("division",
		    Ass_end,
		    Multiplicity(Range("1","")),
		    Role_type "Department")]);
  Association("job",
	      [Role("employees",
		    Ass_end,
		    Multiplicity(Range("0","*")),
		    Role_type "Employee");
	       Role("employer",
		    Ass_end,
		    Multiplicity(Range("0","*")),
		    Role_type "Company")]);
  Association("companyDep",
	      [Role("department_company",
		    Ass_end,
		    Multiplicity(Range("0","*")),
		    Role_type "Company");
	       Role("area",
		    Ass_end,
		    Multiplicity(Range("0","*")),
		    Role_type "Department")])
]



(* PASSAGE A NIVEAU *)

and pn_classes = [
  Class("BarrierSensor",
	Multiplicity(Range("0","50")),
	[],
	[],
	[Attribute("state",Multiplicity(Range("1","")),Att_type "BARRIERstate","")],
	[Operation("getBarrierState",[],[])],
	[],
	[]);
  Class("Light",
	Multiplicity(Range("0","50")),
	[],
	[],
	[Attribute("state",Multiplicity(Range("1","")),Att_type "LIGHTstate","")],
	[Operation("turnOn",[],[]);
	 Operation("turnOff",[],[])],
	[],
	[]);
  Class("Barrier",
	Multiplicity(Range("0","50")),
	[],
	[],
	[Attribute("state",Multiplicity(Range("1","")),Att_type "BARRIERstate","")],
	[Operation("open",[],[]);
	 Operation("close",[],[])],
	[],
	[]);
  Class("LevelCrossingCS",
	Multiplicity(Range("0","50")),
	[],
	[],
	[Attribute("state",Multiplicity(Range("1","")),Att_type "RLCstate","");
	 Attribute("trainPosition",Multiplicity(Range("1","")),Att_type "TrainPosition","")],
	[Operation("openBarrier",[],[]);
	 Operation("closeBarrier",[],[]);
	 Operation("turnOnRedLight",[],[]);
	 Operation("turnOnYellowLight",[],[]);
	 Operation("turnOffRedLight",[],[]);
	 Operation("turnOffYellowLight",[],[]);
	 Operation("ackRequest",[],[]);
	 Operation("detectTrain",[],[])],
	[],
	[]);
  Class("VehicleSensor",
	Multiplicity(Range("0","50")),
	[],
	[],
	[],
	[Operation("sendSignal",[],[])],
	[],
	[]);
  Class("OperationsCenter",
	Multiplicity(Range("0","50")),
	[],
	[],
	[],
	[],
	[],
	[]);
  Class("TrainborneCS",
	Multiplicity(Range("0","50")),
	[],
	[],
	[Attribute("state",Multiplicity(Range("1","")),Att_type "TRAINstate","")],
	[Operation("getStateLC",[],[]);
	 Operation("applyBreaks",[],[]);
	 Operation("continueRun",[],[]);
	 Operation("releaseBreaks",[],[]);
	 Operation("askAck",[],[]);
	 Operation("receiveAck",[],[]);
	 Operation("upSpeed",[],[]);
	 Operation("downSpeed",[],[]);
	 Operation("standStill",[],[])],
	[],
	[])
]

and pn_assocs = [
  Association("barrierSensor_barrier",
	      [Role("sensor",
		    Ass_end,
		    Multiplicity(Range("1","")),
		    Role_type "BarrierSensor");
	       Role("roleBarrier",
		    Composite,
		    Multiplicity(Range("1","")),
		    Role_type "Barrier")]);
  Association("barrierSensor_levelCrossingCS",
	      [Role("bSensor",
		    Ass_end,
		    Multiplicity(Range("1","")),
		    Role_type "BarrierSensor");
	       Role("roleLevelCrossingCS1",
		    Ass_end,
		    Multiplicity(Range("1","")),
		    Role_type "LevelCrossingCS")]);
  Association("light_levelCrossingCS1",
	      [Role("redLight",
		    Ass_end,
		    Multiplicity(Range("1","")),
		    Role_type "Light");
	       Role("roleLevelCrossingCS2",
		    Ass_end,
		    Multiplicity(Range("1","")),
		    Role_type "LevelCrossingCS")]);
  Association("light_levelCrossingCS2",
	      [Role("yellowLight",
		    Ass_end,
		    Multiplicity(Range("1","")),
		    Role_type "Light");
	       Role("roleLevelCrossingCS3",
		    Ass_end,
		    Multiplicity(Range("1","")),
		    Role_type "LevelCrossingCS")]);
  Association("barrier_levelCrossingCS",
	      [Role("theBarrier",
		    Ass_end,
		    Multiplicity(Range("1","")),
		    Role_type "Barrier");
	       Role("roleLevelCrossingCS4",
		    Ass_end,
		    Multiplicity(Range("1","")),
		    Role_type "LevelCrossingCS")]);
  Association("levelCrossinCS_vehicleSensor1",
	      [Role("roleLevelCrossingCS5",
		    Ass_end,
		    Multiplicity(Range("1","")),
		    Role_type "LevelCrossingCS");
	       Role("rear",
		    Ass_end,
		    Multiplicity(Range("1","")),
		    Role_type "VehicleSensor")]);
  Association("levelCrossingCS_vehicleSensor2",
	      [Role("roleLevelCrossingCS6",
		    Ass_end,
		    Multiplicity(Range("1","")),
		    Role_type "LevelCrossingCS");
	       Role("entry",
		    Ass_end,
		    Multiplicity(Range("1","")),
		    Role_type "VehicleSensor")]);
  Association("comm_LC_OC",
	      [Role("lc",
		    Ass_end,
		    Multiplicity(Range("0","*")),
		    Role_type "LevelCrossingCS");
	       Role("oc",
		    Ass_end,
		    Multiplicity(Range("1","")),
		    Role_type "OperationsCenter")]);
  Association("comm_LC_Train",
	      [Role("lc",
		    Ass_end,
		    Multiplicity(Range("0","1")),
		    Role_type "LevelCrossingCS");
	       Role("train",
		    Ass_end,
		    Multiplicity(Range("0","*")),
		    Role_type "TrainborneCS")]);
  Association("comm_Train_OC",
	      [Role("oc",
		    Ass_end,
		    Multiplicity(Range("1","")),
		    Role_type "OperationsCenter");
	       Role("train",
		    Ass_end,
		    Multiplicity(Range("0","*")),
		    Role_type "TrainborneCS")])
]



let model_classes = fi_classes@rl_classes@co_classes@pn_classes
and model_assocs = fi_assocs@rl_assocs@co_assocs@pn_assocs
let model_sys = Model("fig",model_classes,model_assocs)

