(*******************************************************)
(*                                                     *)
(*           Copyright 2004 J�r�me Rocheteau           *)
(*                                                     *)
(*   This file is distributed under the terms of the   *)
(*    GNU Lesser General Public License Version 2.1    *)
(*                                                     *)
(*******************************************************)

(** Type Library *)

(** Types are [t ::= x | c (t,..,t) | /\x t]. *)
type t = 
    Var of string
  | Cst of int Symb.t * t list
  | Abs of string * t

(** The function [subst f t] returns [t [x <- u]] if [f (x) = u]. *)
let rec subst = fun f -> function
    Var x -> if List.mem_assoc x f then List.assoc x f else Var x
  | Cst (c,l) -> Cst (c,(List.map (fun x -> subst f x) l))
  | Abs (x,t)-> if List.mem_assoc x f then  Abs (x,t) else Abs (x,subst f t)

(** The function [mem x t] is true if [x] is a free variable of [t] and false else. *)
let rec mem = fun e -> function 
    Var x -> x = e 
  | Cst (c,l) -> List.exists (fun x -> mem e x) l
  | Abs (x,t) -> if x = e then false else mem e t

(** The function [base t] returns the free variables list of [t]. *)
let base = fun t -> 
  let rec f = function 
      Var x -> [x] 
    | Cst (c,l) -> List.concat (List.map (fun x -> f x) l)
    | Abs (x,t) -> Misc.minus x (f t) 
  in Misc.set_of (List.sort String.compare (f t))

(** The function [compare u v] returns 0 if [u = v], -1 if [u < v] and  1 if [u > v]. *)
let compare = 
  let rec f = function 
      Var x -> 
	begin function 
	    Var x' -> String.compare x x'
	  | _  -> -1
	end 
    | Cst (c,l) -> 
	begin function 
	    Var x -> 1 
	  | Cst (c',l') -> 
	      begin match Symb.compare Pervasives.compare c c' with 
		  0 -> g l l'
		| m -> m
	      end 
	  | _ -> -1
	end 
    | Abs (x,t) -> 
	begin function 
	    Abs (x',t') -> f t (subst [x',Var x] t') 
	  | _ -> 1
	end 
  and g = function 
      [] -> 
	begin function 
	    [] -> 0 
	  | _ -> assert false
	end 
    | t :: l -> 
	begin function 
	    [] -> assert false 
	  | t' :: l' -> 
	      begin match f t t' with 
		| 0 -> g l l'
		| m -> m
	      end 
	end 
  in f 

(** The function [unify sigma epsilon] returns the most general unificator that satisfy all type equations in [epsilon]. *)
let unify = fun sigma -> 
  let g = fun f (x,t) -> (x,subst f t) in 
  let h = fun f (a,b) -> (subst f a,subst f b) in 
  let rec f = fun sigma -> function 
      [] -> sigma 
    | (Var x,Var x') :: epsilon -> 
	if x = x' 
	then f sigma epsilon 
	else 
	  f ((x,Var x') :: (List.map (fun e ->  g [x,Var x'] e) sigma)) 
	    (List.map (fun e -> h [x,Var x'] e) epsilon)
    | (Var x,t) :: epsilon 
    | (t,Var x) :: epsilon -> 
	if mem x t 
	then failwith ("error: unification failed by occur-check\n")
	else 
	  f ((x,t) :: (List.map (fun e ->  g [x,t] e) sigma)) 
	    (List.map (fun e -> h [x,t] e) epsilon)
    | (Cst (c,l),Cst (c',l')) :: epsilon -> 
	if (Symb.compare Pervasives.compare c c') = 0
	then f sigma ((List.combine l l') @ epsilon)
	else failwith ("error: unification of " ^ (Symb.string_of c) ^ " and " ^ (Symb.string_of c') ^ " failed by clash\n" )
    | (Abs (x,t),u) :: epsilon 
    | (u,Abs (x,t)) :: epsilon -> f sigma ((t,u) :: epsilon) 
  in f sigma

let var = fun x -> Var x

let prop = Cst ((Symb.symbol_of "prop" 0),[])

let arr = fun (x,y) -> Cst ((Symb.symbol_of "fun" 2),[x;y])

let abs = fun x t -> Abs (x,t)

let rec close = fun t -> function
    [] -> t
  | x::l -> close (abs x t) l
