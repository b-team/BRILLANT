(* $Id$ *)

(* This library gives functions to help (without warranty though)
   killing processes and subprocesses under Linux. Other OSes aren't
   supported yet *)

open Unix

let getDirFiles dir=
  let dirHandle=Unix.opendir dir in
  let rec auxGetDirFiles dirHandle=
    try
      let read=Unix.readdir dirHandle in
	read::(auxGetDirFiles dirHandle)
    with
	End_of_file -> []
  in
  let result=auxGetDirFiles dirHandle in
    Unix.closedir dirHandle;
    result

let isProcessDir path fname=
  try
    let nb=int_of_string fname in 
(* if no exception is raised, then fname is a number *)
    let finfo=Unix.stat (path ^ "/" ^ fname) in
      match finfo.st_kind with
	| Unix.S_DIR -> true
	| _ -> false
      with
	  Failure "int_of_string" -> false

	     
let getFather pidDir=
  let pidStat=pidDir ^ "/stat" in
  let pidStatHandle=open_in pidStat in
  let pidInfo=input_line pidStatHandle in
    close_in pidStatHandle;
    let infoList=Str.split (Str.regexp "[ \t]") pidInfo in
    let fatherPid=List.nth infoList 3 in
      int_of_string fatherPid


let getAllSons pid=
  let procName="/proc" in
  let procFiles=getDirFiles procName in
  let procDirs=List.filter (isProcessDir procName) procFiles in 
  let goodDirs=
    List.filter
      (fun d -> (getFather (procName ^ "/" ^ d)) = pid)
      procDirs
  in
    List.map int_of_string goodDirs


let rec killFamily pid=
  let sons=getAllSons pid in
    Unix.kill pid Sys.sigkill;
    List.iter killFamily sons

