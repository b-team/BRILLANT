 

Require Import Blib.

Theorem op:
forall Edges NODE Weight, ( 
 (In (Power_set1 BZ) NODE) -> 
(In (relation NODE NODE) Edges) -> 
(In (total_function Edges BN ) Weight)
  -> (
 (Included (Empty_set _) Edges) ) )
 .
intuition.
Qed.
 