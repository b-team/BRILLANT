(*==================================================================================================
  Project : BiCoq2
  Developed for Coq 8.1, using UTF-8 and the BiCoq font
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/ANSSI & UPMC/LIP6 - St: Sep 2005, Md: Dec 2006 (approximately)
  Provided under licence CeCILL-B
  ================================================================================================*)

This is the source code of BiCoq2, a deep embedding of B in Coq.

This version is considered deprecated, and has been replaced by a full redevelopment in BiCoq3.
Names are inappropriate, notations are clumsy, de Bruijn computations inefficient, proof methods
are not stable, and some advanced results are weaker that in the last more recent version.

This version is provided for reference, as it includes for example a proven prover which has not
yet been redeveloped in BiCoq3.

The first letter of filename provides an indication:
- M for tools not associated to B (such as natural values, lists, etc.)
- B for embedding the B logic
- P for the proven prover

The file make.txt describes the dependencies between the files.