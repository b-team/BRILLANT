(*** $Id$ ***)



(** {5 Description du mod�le UML }
@author 
@version 
*)





(* DEFINITION DES TYPES *)

(*** Types li�s au mod�le UML ***)

(** mod�le UML, au sens d'un diagramme de classes *)
type model = Model of
  ( name                 (* nom du mod�le *)
    * uml_class list     (* classes du diagramme *)
    * association list ) (* associations du diagramme *)

(** classe d'un diagramme UML *)
and uml_class = Class of
  ( name                   (* nom de la classe *)
    * multiplicity         (* nb d'instances de la classe pouvant �tre cr��es *)
    * association list     (* si la classe est une classe-association *)
    * inheritance list     (* super-classes dont cette classe h�rite *)
    * attribute list       (* attributs de la classe *)
    * operation list       (* op�rations de la classe *)
    * ocl_constraint list  (* contraintes OCL li�es � cette classe *)
    * state_machine list ) (* diagrammes d'�tats-transitions de cette classe *)

(** association de classes *)
and association = Association of
  ( name         (* nom de l'association *)
    * role list) (* d�finition des r�les de l'association (liste de 2 �l�ments) *)



(*** Types li�s au concept de classe ***)

(** une classe B h�rite d'une classe A ssi A est une g�n�ralisation de B *)
and inheritance = Generalization of parent

(** indique la classe de laquelle on h�rite *)
and parent = Parent of name

(** attribut d'une classe *)
and attribute = Attribute of
  ( name              (* nom de l'attribut *)
    * multiplicity    (* nb de valeurs que peut avoir l'attribut (en g�n�ral: 1) *)
    * attribute_type  (* type de l'attribut *)
    * initial_value ) (* valeur de l'attribut � sa cr�ation *)

(** type d'un attribut *)
and attribute_type =
    Striing
  | Char
  | Integer
  | Real
  | Boolean
  | Undefined          (* type non d�fini *)
  | Att_type of string (* type d�fini par l'utilisateur *)

(** valeur initiale d'un attribut *)
and initial_value = string

(** op�ration d'une classe *)
and operation = Operation of
  ( name               (* nom de l'op�ration *)
    * parameter list   (* arguments de l'op�ration *)
    * parameter list ) (* valeurs de retour *)

(** un param�tre est d�fini par son nom et son type *)
and parameter = Parameter of (name * attribute_type)

(** contrainte OCL *)
and ocl_constraint = string



(*** Types li�s au concept d'association ***)

(** caract�ristiques d'une extr�mit� d'association  *)
and role = Role of
  ( name           (* nom du r�le *)
    * aggregation  (* type d'association de cette extr�mit� *)
    * multiplicity (* multiplicit� de cette extr�mit� de l'association *)
    * role_type )  (* classe associ�e � ce r�le *)

(** type d'association, caract�risant une extr�mit� d'association *)
and aggregation =
    Aggregate (* agr�gation: pour exprimer la notion d'appartenance *)
  | Composite (* composition: agr�gation plus forte (un composant fait
			      partie d'un unique compos�, et le composant est
			      cr�� et d�truit au m�me moment que le compos�) *)
  | Ass_end   (* association simple *)

(** type d'un r�le (classe associ�e � ce r�le) *)
and role_type = Role_type of name



(*** Types li�s au concept d'automate � �tats-transitions ***)

(** diagramme d'�tats-transitions d'une classe *)
and state_machine = State_machine of
  ( name                (* nom du diagramme d'�tats-transitions *)
    * state list        (* ensemble des �tats de l'automate *)
    * transition list ) (* transitions *)

(** caract�risation d'un �tat de l'automate *)
and state =
    Simplestate of name (* �tat ordinaire (atomique) *)
  | Pseudostate of name (* �tat initial *)
  | Finalstate of name  (* �tat final *)

(** transition entre 2 �tats de l'automate *)
and transition = Transition of
  ( name       (* nom de la transition *)
    * source   (* �tat de d�part *)
    * target   (* �tat d'arriv�e *)
    * event    (* stimulus qui d�clenche la transition *)
    * guard    (* condition de d�clenchement (contraint l'ex�cution de la transition) *)
    * action ) (* op�ration qui s'ex�cute au moment de la transition *)

(** �tat de d�part d'une transition *)
and source = Source of state

(** �tat d'arriv�e d'une transition *)
and target = Target of state

(** �v�nement d'une transition (stimulus) *)
and event = string

(** condition de d�clenchement d'une transition *)
and guard = string

(** op�ration accompagnant le d�clenchement d'une transition *)
and action = string



(*** Types communs � tous les concepts ***)

and name = string

(** on d�finit la multiplicit� comme un intervalle de valeurs possibles,
avec la convention: 0..* pour d�signer n'importe qu'elle valeur *)
and multiplicity = Multiplicity of range

(** un intervalle est compos� d'une borne inf�rieure et d'un borne sup�rieure *)
and range = Range of (lower * upper)
and lower = string
and upper = string
;;





(* DEFINITION DES FONCTIONS *)

(** fonction destin�e � s'assurer qu'un identificateur a au moins 2 caract�res
@param a une cha�ne de caract�res
@return aa si a fait 1 caract�re, a sinon  *)
let rename a =
  match String.length(a) with
      1 -> a^a
    | _ -> a



(*** Fonctions qui renvoient le nom d'un objet ***)

(** renvoie le nom de la classe
@return le nom de la classe *)
let class_name (Class(a, _, _, _, _, _, _, _)) = a
				
(** renvoie le nom de l'association
@return le nom de l'association *)
let assoc_name (Association(a,_)) = a
				
(** renvoie le nom de l'attribut
@return le nom de l'attribut *)
let attr_name (Attribute(a, _ , _ , _)) = a



(*** Fonctions qui cherchent un objet � partir de son nom ***)

(** cherche une classe dans un mod�le UML
@param Model(a,b,c) un diagramme de classes
@param d un nom de classe 
@return la classe de nom d, si elle fait partie du diagramme de classes,
        ou une 'classe vide', sinon *)
let rec get_class (Model(a,b,c),d) =
  match b with
      e::f ->
	if class_name(e)=d
	then e
	else get_class (Model(a,f,c),d)
    | _ -> Class("", Multiplicity(Range("0","*")), [], [], [], [], [], [])

(** cherche une association dans un mod�le UML
@param Model(a,b,c) un diagramme de classes
@param d un nom
@return l'association r�f�renc�e par d (d d�signant le nom, un r�le
        ou une classe de l'association), si elle fait partie du mod�le,
        ou une 'association vide', sinon *)
let rec get_assoc (Model(a,b,c),d) =
  match c with
      (Association(g,((Role(h1, i1, j1, Role_type k1))::(Role(h2, i2, j2, Role_type k2))::q)) as e)::f ->
	  if rename(g)=rename(d) or (* d est le nom de l'association *)
	    rename(h1)=rename(d) or (* d est le nom du r�le n�1 *)
	    rename(h2)=rename(d) or (* d est le nom du r�le n�2 *)
	    rename(k1)=rename(d) or (* d est le nom de la classe n�1 *)
            rename(k2)=rename(d)    (* d est le nom de la classe n�2 *)
	  then e
	  else get_assoc (Model(a,b,f),d)
    | [] -> Association("", [])
    | _ -> failwith "get_assoc()"


(** cherche les caract�ristiques d'une association dans une liste d'associations
@param a un nom
@param b une liste d'associations
@return si une association r�f�renc�e par le nom a appartient � la liste :
        la fonction renvoit un couple compos� de l'identificateur de l'association
        et de la multiplicit� associ�e � cet identificateur, avec les r�gles suivantes :
        - si a est un nom de r�le, on garde ce nom comme identificateur
        - si a est un nom de classe, on prend comme identificateur 
          -- soit le nom de l'association, pour aller dans le sens r�le n�1 vers r�le n�2
          -- soit le nom de l'association + tilde (fct r�ciproque en B), pour aller dans l'autre sens
        - sinon, la fonction renvoit un couple ('association vide','multiplicit� vide') *)
let rec find_assoc (a,b) =
  match b with
    Association(g,((Role(h1, i1, j1, Role_type k1))::(Role(h2, i2, j2, Role_type k2))::q))::i ->
	if a=h1
	then begin
	  print_string(h1);
	  (h1,j1)
	end
	else                       (* a est le nom du r�le n�1 *)
	  if a=h2
	  then begin
	    print_string(h2);
	    (h2,j2)
	  end
	  else                     (* a est le nom du r�le n�2 *)
	    if a=k1
	    then begin
	      print_string(g);
	      (g,j1)
	    end
	    else                   (* a est le nom de la classe n�1 *)
	      if a=k2
	      then begin
		print_string(g);
		(g^"~",j2)
	      end
	      else find_assoc(a,i) (* a est le nom de la classe n�2 *)
    | [] -> ("",Multiplicity(Range("","")))
    | _ -> failwith "find_assoc()"

(** cherche un attribut dans une liste d'attributs
@param a un nom d'attribut
@param b une liste d'attributs
@return l'attribut de nom a, s'il appartient � la liste b,
        ou un 'attribut vide', sinon *)
let rec find_attrib1 (a,b)=
  match b with
    Attribute(c,d,e,f)::g ->
      if rename(a)=rename(c)
      then Attribute(rename(c),d,e,f)
      else find_attrib1(a,g)
  | [] -> Attribute("",Multiplicity(Range("","")),Att_type "","")

(** cherche un attribut dans une liste de classes
@param a un nom d'attribut
@param b une liste de classes
@return l'attribut de nom a, s'il fait partie de l'une des classes de la liste b,
        ou un 'attribut vide', sinon *)
let rec find_attrib2 (a,b)=
  match b with
      Class(c,d,e,f,g,h,i,k)::j ->
      let x = find_attrib1(a,g) in          (* on cherche pour chaque classe de la liste *)
	begin match x with
	    Attribute("",_,_,_) -> find_attrib2(a,j) (* si l'attribut ne fait pas partie *)
	  | _ -> x             (* d'une classe donn�e, alors on cherche dans la suivante *)
	end
    | [] -> Attribute("",Multiplicity(Range("","")),Att_type "", "")



(*** Fonctions qui ajoutent un objet ***)

(** ajoute une classe � un mod�le UML *)
let set_class (Model(a,b,c),d) = Model(a,d::b,c)

(** ajoute une association � un mod�le UML *)
let set_assoc (Model(a,b,c),d) = Model(a,b,d::c)

(** ajoute un attribut � une classe *)
let set_attrib(a,Class(b,c,d,e,f,g,h,i)) = Class(b,c,d,e,f@a,g,h,i)

(** ajoute une op�ration � une classe *)
let set_oper(a,Class(b,c,d,e,f,g,h,i)) = Class(b,c,d,e,f,g@a,h,i)


(** introduit/modifie une classe
@param a une liste de classes
@param b une classe
@return si la liste a poss�de une classe de m�me nom,
        ses propri�t�s sont remplac�es par celles de b,
        sinon, b est ajout�e � la liste a *)
let rec replace_class (a,b) =
  match a with
      d::e -> 
	if class_name(d)=class_name(b) 
	then b::e 
	else d::replace_class(e,b)
    | [] -> [b]

(** introduit/modifie une association
@param a une liste d'associations
@param b une association
@return si la liste a poss�de une association de m�me nom,
        ses propri�t�s sont remplac�es par celles de b,
        sinon, b est ajout�e � la liste a *)
let rec replace_assoc (a,b) =
  match a with
      d::e -> 
	if assoc_name(d)=assoc_name(b) 
	then b::e 
	else d::replace_assoc(e,b)
    | [] -> [b]



(*** Fonctions qui cr�ent un nouvel objet ***)

(** ajoute une 'classe vide' � un mod�le UML
@param a le nom de la nouvelle classe
@param b le mod�le UML *)
let new_class (a,b) = set_class(b, Class(a, Multiplicity(Range("0","*")), [], [], [], [], [], []))

(** renvoie un attribut
@param a nom de l'attribut � cr�er
@param b la cardinalit� min de l'attribut
@param c la cardinalit� max
@param d le type de l'attribut *)
let new_attrib (a,b,c,d) = Attribute(a, Multiplicity(Range(b,c)),Att_type d, "")

