(*==================================================================================================
  Project : BiCoq3
  Module : bbprop.v
  Propositional calculus theorems from the B-Book
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Jul 2008, Md: Jul 2008
  ================================================================================================*)

Require Export lemma.

(* Theorems --------------------------------------------------------------------------------------*)
(* The name bbt_x_y_z means Theorem x.y.z from the B-Book *)

Theorem bbt_1_2_1:forall (G:Γ)(p:Π), G⊢p⇒¬¬p.
Proof.
 intros G p; apply Bpr_impi; apply Bpr_not2; Btac_hyp.
Qed.

Theorem bbt_1_2_2:forall (p1 p2:Π), ⊢(p1⋀¬p2)⇒¬(p1⇒p2).
Proof.
 intros p1 p2; apply Bpr_impi; apply Bpr_ands; apply Bpr_absn with (p2:=p2);
  [apply Bpr_modp with (p1:=p1) | ]; Btac_hyp.
Qed.

Theorem bbt_1_2_3:forall (p1 p2:Π), ⊢(p1⇒¬p2)⇒¬(p1⋀p2).
Proof.
 intros p1 p2; apply Bpr_impi; apply Bpr_absn with (p2:=p2); apply Bpr_ands;
  [ | apply Bpr_modp with (p1:=p1)]; Btac_hyp.
Qed.

Theorem bbt_1_2_4:forall (p1 p2:Π), ⊢(p1⇒p2)⇒(¬¬p1⇒p2).
Proof.
 intros p1 p2; apply Bpr_impi; apply Bpr_impi; apply Bpr_cutr with (p1:=p1);
  [apply Bpr_impe; apply Bpr_ctnp; apply Bpr_impi | apply Bpr_modp with (p1:=p1)]; Btac_hyp.
Qed.

Theorem bbt_1_2_5:forall (p1 p2 p3:Π), ⊢(p1⇒(¬p2⇒p3))⇒(¬(p1⇒p2)⇒p3).
Proof.
 intros p1 p2 p3; apply Bpr_impi; apply Bpr_impi; apply Bpr_absp with (p2:=p1⇒p2).
  apply Bpr_impi; apply Bpr_absp with (p2:=p3); [apply Bpr_impe; apply Bpr_impe | ]; Btac_hyp.
  Btac_hyp.
Qed.

Theorem bbt_1_2_6:forall (p1 p2 p3:Π), ⊢(¬p1⇒p3)⋀(¬p2⇒p3)⇒¬(p1⋀p2)⇒p3.
Proof.
 intros p1 p2 p3; apply Bpr_impi; apply Bpr_ands; apply Bpr_impi; apply Bpr_absp with (p2:=p1⋀p2).
  apply Bpr_andi.
   apply Bpr_absp with (p2:=p3); [apply Bpr_modp with (p1:=¬p1) | ]; Btac_hyp.
   apply Bpr_absp with (p2:=p3); [apply Bpr_modp with (p1:=¬p2) | ]; Btac_hyp.
  Btac_hyp.
Qed.

Theorem bbt_1_2_7:forall (p1 p2 p3:Π), ⊢(¬p1⇒p3)⋀(p2⇒p3)⇒(p1⇒p2)⇒p3.
Proof.
 intros p1 p2 p3; apply Bpr_impi; apply Bpr_ands; apply Bpr_impi; apply Bpr_absp with (p2:=p1⇒p2).
  Btac_hyp.
  apply Bpr_modp with (p1:=p1⋀¬p2).
   apply Bpr_andi.
    apply Bpr_absp with (p2:=p3); [apply Bpr_modp with (p1:=¬p1) | ]; Btac_hyp.
    apply Bpr_absn with (p2:=p3); [apply Bpr_modp with (p1:=p2) | ]; Btac_hyp.
   apply Bpr_nulg; apply bbt_1_2_2.
Qed.

Theorem bbt_1_2_8:forall (p1 p2 p3:Π), ⊢(p1⇒p2⇒p3)⇒(p1⋀p2⇒p3).
Proof.
 intros p1 p2 p3; apply Bpr_impi; apply Bpr_impi; apply Bpr_ands; apply Bpr_modp with (p1:=p2);
  [ | apply Bpr_modp with (p1:=p1)]; Btac_hyp.
Qed.

(* Rules for the propositional tactic ------------------------------------------------------------*)
(* We name these rules according to the naming of the B-Book *)

Theorem bbded:forall (G:Γ)(p1 p2:Π), G·p1⊢p2->G⊢p1⇒p2.
Proof.
 apply Bpr_impi.
Qed.

Theorem bbcnj:forall (G:Γ)(p1 p2:Π), G⊢p1->G⊢p2->G⊢p1⋀p2.
Proof.
 apply Bpr_andi.
Qed.

Theorem bbdr1:forall (G:Γ)(p:Π), G⊢p->G⊢¬¬p.
Proof.
 apply Bpr_not2.
Qed.

Theorem bbdr2:forall (G:Γ)(p1 p2:Π), G⊢p1->G⊢¬p2->G⊢¬(p1⇒p2).
Proof.
 intros G p1 p2 Hp1 Hp2; apply Bpr_modp with (p1:=p1⋀¬p2).
  apply Bpr_andi; [apply Hp1 | apply Hp2].
  apply Bpr_nulg; apply bbt_1_2_2.
Qed.

Theorem bbdr3:forall (G:Γ)(p1 p2:Π), G⊢p1⇒¬p2->G⊢¬(p1⋀p2).
Proof.
 intros G p1 p2 Himp; apply Bpr_modp with (p1:=p1⇒¬p2).
  apply Himp.
  apply Bpr_nulg; apply bbt_1_2_3.
Qed.

Theorem bbdr4:forall (G:Γ)(p1 p2:Π), G⊢p1⇒p2->G⊢¬¬p1⇒p2.
Proof.
 intros G p1 p2 Himp; apply Bpr_modp with (p1:=p1⇒p2).
  apply Himp.
  apply Bpr_nulg; apply bbt_1_2_4.
Qed.

Theorem bbdr5:forall (G:Γ)(p1 p2 p3:Π), G⊢p1⇒(¬p2⇒p3)->G⊢¬(p1⇒p2)⇒p3.
Proof.
 intros G p1 p2 p3 Himp; apply Bpr_modp with (p1:=p1⇒(¬p2⇒p3)).
  apply Himp.
  apply Bpr_nulg; apply bbt_1_2_5.
Qed.

Theorem bbdr6:forall (G:Γ)(p1 p2 p3:Π), G⊢¬p1⇒p3->G⊢¬p2⇒p3->G⊢¬(p1⋀p2)⇒p3.
Proof.
 intros G p1 p2 p3 Hp1 Hp2; apply Bpr_modp with (p1:=(¬p1⇒p3)⋀(¬p2⇒p3)).
  apply Bpr_andi; [apply Hp1 | apply Hp2].
  apply Bpr_nulg; apply bbt_1_2_6.
Qed.

Theorem bbdr7:forall (G:Γ)(p1 p2 p3:Π), G⊢¬p1⇒p3->G⊢p2⇒p3->G⊢(p1⇒p2)⇒p3.
Proof.
 intros G p1 p2 p3 Hp1 Hp2; apply Bpr_modp with (p1:=(¬p1⇒p3)⋀(p2⇒p3)).
  apply Bpr_andi; [apply Hp1 | apply Hp2].
  apply Bpr_nulg; apply bbt_1_2_7.
Qed.

Theorem bbdr8:forall (G:Γ)(p1 p2 p3:Π), G⊢p1⇒p2⇒p3->G⊢p1⋀p2⇒p3.
Proof.
 intros G p1 p2 p3 Himp; apply Bpr_modp with (p1:=p1⇒p2⇒p3).
  apply Himp.
  apply Bpr_nulg; apply bbt_1_2_8.
Qed.

Theorem bbdb1:forall (G:Γ)(p1 p2:Π), p1∈ˠG=⊤->G⊢¬p1⇒p2.
Proof.
 intros G p1 p2 Hp1; apply Bpr_impi; apply Bpr_absp with (p2:=p1); Btac_hyp.
Qed.

Theorem bbdb2:forall (G:Γ)(p1 p2:Π), (¬p1)∈ˠG=⊤->G⊢p1⇒p2.
Proof.
 intros G p1 p2 Hp1; apply Bpr_impi; apply Bpr_absp with (p2:=p1); Btac_hyp.
Qed.

Theorem bbbs1:forall (G:Γ)(p1 p2:Π), p1∈ˠG=⊤->G⊢p2⇒p1.
Proof.
 intros G p1 p2 Hp1; apply Bpr_impi; Btac_hyp.
Qed.

Theorem bbbs2:forall (G:Γ)(p1:Π), p1∈ˠG=⊤->G⊢p1.
Proof.
 intros G p1 Hp1; apply Bpr_memb; apply Hp1.
Qed.

(* Propositional tactic --------------------------------------------------------------------------*)

 Ltac Btac_prop :=
  unfold blor;
  unfold biff;
  intros;
  repeat (first [BS1 | BS2 | DB1 | DB2 | DR])
  with BS1 := match goal with| |- ?G⊢_⇒?Q => match G with
                                             | ?G'·Q => apply bbbs1; Btac_gamma
                                             | ?G'·_ => apply Bpr_clear; BS1
                                             end end
 with BS2 := match goal with | |- ?G⊢?P => match G with
                                           | ?G'·P => apply bbbs2; Btac_gamma
                                           | ?G'·_ => apply Bpr_clear; BS2
                                           end end
 with DB1:= match goal with | |- ?G⊢¬?P⇒_ => match G with
                                             | ?G'·P => apply bbdb1; Btac_gamma
                                             | ?G'·_ => apply Bpr_clear; DB1
                                             end end
 with DB2:= match goal with | |- ?G⊢?P⇒_ => match G with
                                            | ?G'·¬P => apply bbdb2; Btac_gamma
                                            | ?G'·_ => apply Bpr_clear; DB2
                                            end end
 with DR:= match goal with |- ?G⊢¬¬?P => apply bbdr1
                           | |- _⊢¬(_⇒_) => apply bbdr2
                           | |- _⊢¬(_⋀_) => apply bbdr3
                           | |- _⊢¬¬_⇒_ => apply bbdr4
                           | |- _⊢¬(_⇒_) ⇒_ => apply bbdr5
                           | |- _⊢¬(_⋀_) ⇒_ => apply bbdr6
                           | |- _⊢(_⇒_) ⇒_ => apply bbdr7
                           | |- _⊢_⋀_⇒_ => apply bbdr8
                           | |- _⊢_⋀_ => apply bbcnj
                           | |- _⊢_⇒_ => apply bbded end.

(* Theorems --------------------------------------------------------------------------------------*)
(* We are here using the efficient tactic introduced just before. *)

Theorem bbt_1_2_9:forall (p1 p2 p3:Π), ⊢(¬p1⇒p2)⋀p3⇒(¬(p1⋀p3)⇒(p2⋀p3)).
Proof. Btac_prop. Qed.

Theorem bbt_1_2_10:forall (p1 p2 p3:Π), ⊢(p1⇒p3)⋀(p2⇒p3)⋀(p1⋁p2)⇒p3.
Proof. Btac_prop. Qed.

Theorem bbcase:forall (G:Γ)(p1 p2 p3:Π), G⊢p1⋁p2->G⊢p1⇒p3->G⊢p2⇒p3->G⊢p3.
Proof.
 intros G p1 p2 p3 Hor Hp1 Hp2; apply Bpr_modp with (p1:=(p1⇒p3)⋀(p2⇒p3)⋀(p1⋁p2)).
  apply Bpr_andi; [apply Hp1 | apply Bpr_andi; [apply Hp2 | apply Hor]].
  apply Bpr_nulg; apply bbt_1_2_10.
Qed.

Theorem bbclassic_1:forall (p1 p2:Π), ⊢p1⋁p2⇔p2⋁p1.
Proof. Btac_prop. Qed.

Theorem bbclassic_2:forall (p1 p2:Π), ⊢p1⋀p2⇔p2⋀p1.
Proof. Btac_prop. Qed.

Theorem bbclassic_3:forall (p1 p2:Π), ⊢(p1⇔p2)⇔(p2⇔p1).
Proof. Btac_prop. Qed.

Theorem bbclassic_4:forall (p1 p2 p3:Π), ⊢p1⋁(p2⋁p3)⇔(p1⋁p2)⋁p3.
Proof. Btac_prop. Qed.

Theorem bbclassic_5:forall (p1 p2 p3:Π), ⊢p1⋀(p2⋀p3)⇔(p1⋀p2)⋀p3.
Proof. Btac_prop. Qed.

Theorem bbclassic_6:forall (p1 p2 p3:Π), ⊢(p1⇔(p2⇔p3))⇔((p1⇔p2)⇔p3).
Proof. Btac_prop. Qed.

Theorem bbclassic_7:forall (p1 p2 p3:Π), ⊢p1⋀(p2⋁p3)⇔(p1⋀p2)⋁(p1⋀p3).
Proof. Btac_prop. Qed.

Theorem bbclassic_8:forall (p1 p2 p3:Π), ⊢p1⋁(p2⋀p3)⇔(p1⋁p2)⋀(p1⋁p3).
Proof. Btac_prop. Qed.

Theorem bbclassic_9:forall (p1 p2 p3:Π), ⊢(p1⇒(p2⋀p3))⇔(p1⇒p2)⋀(p1⇒p3).
Proof. Btac_prop. Qed.

Theorem bbclassic_10:forall (p:Π), ⊢p⋁¬p.
Proof. Btac_prop. Qed.

Theorem bbclassic_11:forall (p:Π), ⊢p⋁p⇔p.
Proof. Btac_prop. Qed.

Theorem bbclassic_12:forall (p:Π), ⊢p⋀p⇔p.
Proof. Btac_prop. Qed.

Theorem bbclassic_13:forall (p1 p2:Π), ⊢(p1⋁p2)⋀p1⇔p1.
Proof. Btac_prop. Qed.

Theorem bbclassic_14:forall (p1 p2:Π), ⊢(p1⋀p2)⋁p1⇔p1.
Proof. Btac_prop. Qed.

Theorem bbclassic_15:forall (p1 p2:Π), ⊢¬(p1⋁p2)⇔¬p1⋀¬p2.
Proof. Btac_prop. Qed.

Theorem bbclassic_16:forall (p1 p2:Π), ⊢¬(p1⋀p2)⇔¬p1⋁¬p2.
Proof. Btac_prop. Qed.

Theorem bbclassic_17:forall (p1 p2:Π), ⊢¬(p1⋀p2)⇔(p1⇒¬p2).
Proof. Btac_prop. Qed.

Theorem bbclassic_18:forall (p1 p2:Π), ⊢¬(p1⇒p2)⇔p1⋀¬p2.
Proof. Btac_prop. Qed.

Theorem bbclassic_19:forall (p1 p2:Π), ⊢(p1⇒p2)⇔(¬p2⇒¬p1).
Proof. Btac_prop. Qed.

Theorem bbclassic_20:forall (p1 p2:Π), ⊢(¬p1⇒p2)⇔(¬p2⇒p1).
Proof. Btac_prop. Qed.

Theorem bbclassic_21:forall (p1 p2:Π), ⊢(p1⇒¬p2)⇔(p2⇒¬p1).
Proof. Btac_prop. Qed.

Theorem bbclassic_22:forall (p:Π), ⊢p⇔¬¬p.
Proof. Btac_prop. Qed.

Theorem bbclassic_23:forall (p1 p2 p3:Π), ⊢(p1⇒p2)⋀(p2⇒p3)⇒(p1⇒p3).
Proof. Btac_prop. Qed.

Theorem bbclassic_24:forall (p1 p2 p3:Π), ⊢(p1⇒p2)⇒(p1⋀p3⇒p2⋀p3).
Proof. Btac_prop. Qed.

Theorem bbclassic_25:forall (p1 p2 p3:Π), ⊢(p1⇒p2)⇒(p1⋁p3⇒p2⋁p3).
Proof. Btac_prop. Qed.

Theorem bbclassic_26:forall (p1 p2 p3:Π), ⊢(p1⇒p2)⇒((p3⇒p1)⇒(p3⇒p2)).
Proof. Btac_prop. Qed.

Theorem bbclassic_27:forall (p1 p2 p3:Π), ⊢(p1⇒p2)⇒((p2⇒p3)⇒(p1⇒p3)).
Proof. Btac_prop. Qed.

Theorem bbclassic_28:forall (p1 p2:Π), ⊢(p1⇒p2)⇒(¬p2⇒¬p1).
Proof. Btac_prop. Qed.

Theorem bbclassic_29:forall (p1 p2 p3:Π), ⊢(p1⇔p2)⇒(p1⋀p3⇔p2⋀p3).
Proof. Btac_prop. Qed.

Theorem bbclassic_30:forall (p1 p2 p3:Π), ⊢(p1⇔p2)⇒(p1⋁p3⇔p2⋁p3).
Proof. Btac_prop. Qed.

Theorem bbclassic_31:forall (p1 p2 p3:Π), ⊢(p1⇔p2)⇒((p3⇒p1)⇔(p3⇒p2)).
Proof. Btac_prop. Qed.

Theorem bbclassic_32:forall (p1 p2 p3:Π), ⊢(p1⇔p2)⇒((p1⇒p3)⇔(p2⇒p3)).
Proof. Btac_prop. Qed.

Theorem bbclassic_33:forall (p1 p2 p3:Π), ⊢(p1⇔p2)⇒(¬p2⇔¬p1).
Proof. Btac_prop. Qed.

(*================================================================================================*)