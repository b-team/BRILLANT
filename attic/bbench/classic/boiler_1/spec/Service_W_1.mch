/*? **************************** DIGILOG ************************************
 * Date : @(#) sccs get 6/7/96
 *
 * File : Service_W_1.mch
 *
 * Version : 1.7
 *
 * Directory : /home/IPSN/ebt/DEMON/Control/spec/SCCS/s.Service_W_1.mch
 *
 * Type	:	MACHINE
 *
 * Object : Service concerning the controllers
 *
 * Copyright : (C) 1996 Jean-Raymond ABRIAL / DIGILOG - STERIA Mediterranee
 *
 *   Permission is granted to make and distribute verbatim copies of
 *   this file provided the copyright notice and this permission notice
 *   are preserved on all copies.
 *
 *   Permission is granted to copy and distribute modified versions of
 *   this file under the conditions for verbatim copying, provided also
 *   that the entire resulting derived work is distributed under the terms
 *   of a permission notice identical to this one.
 *
 *   Permission is granted to copy and distribute translations of this
 *   manual into another language, under the above conditions for modified
 *   versions
 *
 *	 This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *   This program can be redistributed or modified under the terms of
 *   the GNU General Public License. The General Public License can be
 *	 found in the file COPYING tha accompanies the source code.
 *
 *---------------------------------------------------------------------------
 *
 * Functions :
 *		read_water
 *		calculate_previous_level
 *		water_test
 *		evaluate_state
 *		adjust_water
 *---------------------------------------------------------------------------
 *
 * Modifications :
 *
 ************************************************************************* ?*/

MACHINE
	Service_W_1(controller_number)

CONSTRAINTS
	controller_number : NAT

SEES
	Constants_L_1,  Acq_1

VISIBLE_VARIABLES
	wim, wrm, wfam,pr,
	wpop_1, wpcl_0, 
	pa1, pa2,
	pc1, pc2,
	wfm, wram, wok, wtk,
	unknown_state
	
INVARIANT
	unknown_state : BOOL &
	wim : BOOL &
	wrm : BOOL &
	wfam : BOOL &
	pr : {0, PNOMINAL} &
	wpop_1 : BOOL &
	wpcl_0 : BOOL &
	pa1 : {0, PNOMINAL} & pa2 : {0, PNOMINAL} & pa1 <= pa2 &
	pc1 : {0, PNOMINAL} & pc2 : {0, PNOMINAL} & pc1 <= pc2 &
	wfm : BOOL &
	wram : BOOL &
	wok : BOOL &
	wtk : BOOL 

INITIALISATION
	unknown_state :: BOOL ||
	wim :: BOOL ||
	wrm :: BOOL ||
	wfam :: BOOL ||
	pr := 0 ||
	wpop_1 := FALSE ||
	wpcl_0 := FALSE ||
	pa1 := 0 ||
	pa2 := PNOMINAL ||
	pc1 := 0 ||
	pc2 := PNOMINAL ||
	wfm :: BOOL ||
	wram :: BOOL ||
	wok :: BOOL ||
	wtk :: BOOL 


OPERATIONS
	read_water =
		PRE
			controller_number : 1..NB_PUMP
		THEN
			wim := acq_wim(controller_number) ||
			wrm := acq_wrm(controller_number) ||
			wfam := acq_wfam(controller_number) ||
			pr := acq_pr(controller_number) ||
			wpop_1 := acq_wpop_1 ||
			wpcl_0 := acq_wpcl_0
		END;

	calculate_previous_level =
	PRE
		controller_number : 1..NB_PUMP
	THEN
		IF (wok = FALSE or wpcl_0 = TRUE or
			(wpop_1 = FALSE & pa1 = 0))
		THEN
			pc1 := 0
		ELSE
			pc1 := PNOMINAL
		END ||
		IF (wok = FALSE or wpop_1 = TRUE or
			(wpcl_0 = FALSE & pa2 = PNOMINAL))
		THEN
			pc2 := PNOMINAL
		ELSE
			pc2 := 0
		END
	END;

	water_test = 
		PRE
			controller_number : 1..NB_PUMP
		THEN
			wtk := bool (wim = TRUE &
					(wok = FALSE or wrm = FALSE ) &
					(wfam = FALSE or wfm = TRUE)) ||
			wfm := bool (((pr /: pc1..pc2) &
						 (wok = TRUE or wrm = TRUE)) or
						 (wfm = TRUE & wfam = FALSE)) ||

			wok := bool ((pr : pc1..pc2) &
						 (wok = TRUE or wrm = TRUE)) ||
			wram := wrm
		END;

	evaluate_state =
		BEGIN
			unknown_state := bool ( (wok = FALSE) or 
									(wpop_1 = TRUE & pa2 = 0 & pa1 = 0) or
									(wpcl_0 = TRUE & pa2 = PNOMINAL & 
									pa1 = PNOMINAL))
		END;

	adjust_water =
	PRE
		controller_number : 1..NB_PUMP
	THEN
		IF (unknown_state = TRUE)
		THEN
				pa1 := 0 ||
				pa2 := PNOMINAL
		ELSE
				pa1 := pr ||
				pa2 := pr
		END
	END
END

