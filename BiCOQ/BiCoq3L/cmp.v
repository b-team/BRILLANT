(*==================================================================================================
  Project : BiCoq3
  Module : cmp.v
  This module is dedicated to a number of lemmas, some of which are "technical", related to the
  interactions between lifting, collapsing, abstraction, application and substitution. The main
  interesting results are:
  - substitution is equivalent to abstraction followed by application,
  - alpha-renaming which is shown to reduce to the identity,
  - abstraction is equivalent to lifting followed by deskolemisation,
  - application is equivalent to skolemisation followed by downlifting (an operation defined here).
  We therefore prove that lifting (with downlifting), skolemisation and deskolemisation are the
  primitive operations of our calculus.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Jul 2008, Md: Sep 2008
  ================================================================================================*)

Require Export abstr.
Require Export apply.
Require Export esubst.

(* Substitution as the composition of abstraction and application --------------------------------*)
(* In our theory, we don't need to define substitution, as it is just a composition of abstraction
   and application. Note that in classical approaches representing the application as a term, it is
   easy to "match" (λx.T1)@T2, to obtain T1, T2 and then transform the redex in [x:=T2]T1. In our
   case, (λx.T1)@T2 is not a term but a function whose evaluation yields a new term T3, providing
   directly the result - but deducing T1 and T2 from T3 can, on the other hand, be difficult... *)

Theorem Bt_esubst_dec:forall (t:Τ)(a:Ε)(i:Ι), Bt_app (Bt_abstr i t) a=〈i←a@t〉.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (a:Ε)(i:Ι), Bt_app (Bt_abstr i t) a=〈i←a@t〉));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind a i;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) a i); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) a i); irewrite (Hind _ (sdepth_r t1 t2) a i);
       apply refl_equal).
  destruct (DB_eq i ¡(u',n')); simpl; [apply refl_equal | destruct i as [u n]].
   destruct u' as [| u']; [ destruct (nat_eq n n') | ]; nat_simpl; bool_simpl; apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) a i); irewrite (Hind _ (sdepth_r t1 t2) (Be_lift a) (ulft i));
    apply refl_equal.
  irewrite (Hind _ (sdepth t) (Be_lift a) (ulft i)); apply refl_equal.
Qed.

Theorem Bt_esubst_forall:forall (p:Π)(i:Ι)(a:Ε), 〈i←a@p〉=(∀(i∙p)@a|!∀(i∙p)).
Proof.
 intros p i a; apply sym_equal; unfold Bp_forall; unfold B_forapp; apply (Bt_esubst_dec p a i).
Qed.

(* Abstraction and substitution, Alpha-renaming --------------------------------------------------*)
(* We prove here that the De Bruijn representation is alpha-quotiented: two terms are alpha
   equivalent iff they are equal. *)

Theorem Bt_abstr_esubst:forall (t:Τ)(i i':Ι), (i∖t)->Bt_abstr i 〈i'←∂(i)@t〉=Bt_abstr i' t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i i':Ι), (i∖t)->Bt_abstr i 〈i'←∂(i)@t〉=Bt_abstr i' t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i i' Hit;
  try (apply refl_equal);
  try (irewrite (Hind _ (sdepth t) i i' Hit); apply refl_equal);
  try (destruct (orb_false_elim _ _ Hit) as [Hit1 Hit2];
       irewrite (Hind _ (sdepth_l t1 t2) i i' Hit1);
       irewrite (Hind _ (sdepth_r t1 t2) i i' Hit2); apply refl_equal).
  destruct (DB_eq i' ¡(u',n')); simpl; [DB_simpl | rewrite Hit]; apply refl_equal.
  destruct (orb_false_elim _ _ Hit) as [Hit1 Hit2]; irewrite (Hind _ (sdepth_l t1 t2) i i' Hit1);
   irewrite (Hind _ (sdepth_r t1 t2) (ulft i) (ulft i') Hit2); apply refl_equal.
  irewrite (Hind _ (sdepth t) (ulft i) (ulft i') Hit); apply refl_equal.
Qed.

Theorem Bp_forall_alpha:forall (p:Π)(i i':Ι), (i∖p)->∀(i'∙p)=∀(i∙〈i'←∂(i)@Π@p〉).
Proof.
 intros p i i' Hp; unfold Bp_forall; irewrite (Bt_abstr_esubst p i i' Hp); apply refl_equal.
Qed.

Theorem Be_cmpset_alpha:forall (p:Π)(e:Ε)(i i':Ι), (i∖p)->{i'∊e∣p}={i∊e∣〈i'←∂(i)@Π@p〉}.
Proof.
 intros p e i i' Hp; unfold Be_cmpset; irewrite (Bt_abstr_esubst p i i' Hp); apply refl_equal.
Qed.

Theorem Bp_forall_esubst_eq:forall (p:Π)(a:Ε)(i:Ι), 〈i←a@Π@∀(i∙p)〉=∀(i∙p).
Proof.
 intros p a i; irewrite (Bt_esubst_free _ _ (Bp_forall_free i p) a); apply refl_equal.
Qed.

Theorem Be_cmpset_esubst_eq:forall (p:Π)(e a:Ε)(i:Ι), (i∖e)->〈i←a@Ε@{i∊e∣p}〉={i∊e∣p}.
Proof.
 intros p e a i Hie; irewrite (Bt_esubst_free {i∊e∣p} i (Be_cmpset_free _ _ p Hie) a); intros Heq;
  rewrite Heq; apply refl_equal.
Qed.

Theorem Bt_abstr_esubst_comm:forall (t:Τ)(a:Ε)(i i':Ι), i<>i'->(i∖a)->
                             〈ulft i'←Be_lift a@Bt_abstr i t〉=Bt_abstr i 〈i'←a@t〉.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (a:Ε)(i i':Ι), i<>i'->(i∖a)->
                            〈ulft i'←Be_lift a@Bt_abstr i t〉=Bt_abstr i 〈i'←a@t〉));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind a i i' Hii' Hia;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) _ _ _ Hii' Hia); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) _ _ _ Hii' Hia);
       irewrite (Hind _ (sdepth_r t1 t2) _ _ _ Hii' Hia); apply refl_equal).
  case_eq (DB_eq i' ¡(u',n')); intros Hi'u'n'; case_eq (DB_eq i ¡(u',n')); simpl; intros Hiu'n'.
   reflect_in DB_eq_imp Hiu'n'; rewrite Hiu'n' in Hii'; reflect_in DB_eq_imp Hi'u'n';
    rewrite Hi'u'n' in Hii'; destruct Hii'; apply refl_equal.
   change ¡(u',if eq_0 u' then S n' else n') with (ulft ¡(u',n')); rewrite DB_eq_ulft;
    rewrite Hi'u'n'; apply (sym_equal (Bt_abstr_lift a i Hia)).
   rewrite Hiu'n'; destruct i' as [[| u''] n'']; simpl; nat_simpl; bool_simpl; apply refl_equal.
   rewrite Hiu'n'; change ¡(u',if eq_0 u' then S n' else n') with (ulft ¡(u',n'));
    rewrite DB_eq_ulft; rewrite Hi'u'n'; apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) _ _ _ Hii' Hia); assert (H'ii':ulft i<>ulft i').
   intros Heq; apply Hii'; generalize (imppt2 DB_eq_imp _ _ Heq); rewrite DB_eq_ulft;
    apply (fst (DB_eq_imp)).
  assert (H'ia:ulft i∖Be_lift a). irewrite (Bt_lift_free a i); apply Hia.
  irewrite (Hind _ (sdepth_r t1 t2) _ _ _ H'ii' H'ia); apply refl_equal.
  assert (H'ii':ulft i<>ulft i').
   intros Heq; apply Hii'; generalize (imppt2 DB_eq_imp _ _ Heq); rewrite DB_eq_ulft;
    apply (fst (DB_eq_imp)).
  assert (H'ia:ulft i∖Be_lift a). irewrite (Bt_lift_free a i); apply Hia.
  irewrite (Hind _ (sdepth t) _ _ _ H'ii' H'ia); apply refl_equal.
Qed.

Theorem Bp_forall_esubst_diff:forall (p:Π)(a:Ε)(i i':Ι),
                              i<>i'->(i∖a)->〈i'←a@Π@∀(i∙p)〉=∀(i∙〈i'←a@Π@p〉).
Proof.
 intros p a i i' Hii' Hia; simpl; irewrite (Bt_abstr_esubst_comm p a i i' Hii' Hia);
  apply refl_equal.
Qed.

Theorem Be_cmpset_esubst_diff:forall (p:Π)(e a:Ε)(i i':Ι),
                              i<>i'->(i∖a)->〈i'←a@Ε@{i∊e∣p}〉={i∊〈i'←a@Ε@e〉∣〈i'←a@Π@p〉}.
Proof.
 intros p e a i i' Hii' Hia; simpl; irewrite (Bt_abstr_esubst_comm p a i i' Hii' Hia);
  apply refl_equal.
Qed.

(* Reverse abstraction ---------------------------------------------------------------------------*)
(* By showing how to build a term in the functional representation from its De Bruijn version, we
   justify the existence of a pretty-printer but also prove equivalence of the various
   representations. *)

Theorem Bp_abstr_app:forall (t:Τ)(i:Ι), (i∖t)->(ulft i∖t)->Bt_abstr i (Bt_app t ∂(i))=t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι), (i∖t)->(ulft i∖t)->Bt_abstr i (Bt_app t ∂(i))=t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i Hit H'it;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) i Hit H'it); apply refl_equal);
  try (destruct (orb_false_elim _ _ Hit) as [Hit1 Hit2];
       destruct (orb_false_elim _ _ H'it) as [H'it1 H'it2];
       irewrite (Hind _ (sdepth_l t1 t2) i Hit1 H'it1);
       irewrite (Hind _ (sdepth_r t1 t2) i Hit2 H'it2); apply refl_equal).
  destruct u' as [| u']; destruct n' as [| n']; simpl.
   DB_simpl; apply refl_equal.
   case_eq (DB_eq i ¡(0,n')); simpl; intros Hii'.
    reflect_in DB_eq_imp Hii'; rewrite Hii' in H'it; simpl in H'it; nat_simpl; inversion H'it.
    apply refl_equal.
   rewrite Hit; apply refl_equal.
  rewrite Hit; apply refl_equal.
  destruct (orb_false_elim _ _ Hit) as [Hit1 Hit2];
   destruct (orb_false_elim _ _ H'it) as [H'it1 H'it2];
   irewrite (Hind _ (sdepth_l t1 t2) i Hit1 H'it1);
   irewrite (Hind _ (sdepth_r t1 t2) (ulft i) Hit2 H'it2); apply refl_equal.
  irewrite (Hind _ (sdepth t) (ulft i) Hit H'it); apply refl_equal.
Qed.

Theorem Bp_forall_inv:forall (p:Π)(i:Ι), (i∖p)->(ulft i∖p)->∀Δ(p)=∀(i∙Bp_app p ∂(i)).
Proof.
 intros p i Hip H'ip; unfold Bp_forall; irewrite (Bp_abstr_app p i Hip H'ip); apply refl_equal.
Qed.

Theorem Be_cmpset_inv:forall (p:Π)(e:Ε)(i:Ι), (i∖p)->(ulft i∖p)->ⅭΔ(e,p)={i∊e∣Bp_app p ∂(i)}.
Proof.
 intros p e i Hip H'ip; unfold Be_cmpset; irewrite (Bp_abstr_app p i Hip H'ip); apply refl_equal.
Qed.

(* Abstraction as substitution -------------------------------------------------------------------*)
(* Note that Bt_abstr i t is NOT 〈ulft i←∂(¡(0,0))@⇑(t)〉. Why this is locally equivalent, this does
   not cross binders. Indeed, both Bt_abstr i ∀Δ(t) and 〈ulft i←∂(¡(0,0))@⇑(∀Δ(t))〉 capture (ulft i)
   in t, but in the first case it is replaced by ∂(¡(0,0)) whereas in the second case it is replaced
   by ⇑(∂(¡(0,0))), that is ⇑(∂(¡(0,1))). *)

(*================================================================================================*)