(*==================================================================================================
  Project : BiCoq3
  Module : egraft_cmp.v
  We study some commutations lemmas between grafting and other operations.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Oct 2008, Md: Oct 2008
  ================================================================================================*)

Require Export egraft.
Require Export cmp.

Theorem Bt_app_egraft:forall (t:Τ)(i:Ι)(u n:nat)(a:Ε), i<>¡(S u,n)->
                      Bt_app 〈ulft i⇙a@t〉 ∂(¡(S u,n))=
                      〈i⇙Be_app a ∂(¡(S u,n))@Bt_app t ∂(¡(S u,n))〉.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι)(u n:nat)(a:Ε), i<>¡(S u,n)->
                            Bt_app 〈ulft i⇙a@t〉 ∂(¡(S u,n))=
                            〈i⇙Be_app a ∂(¡(S u,n))@Bt_app t ∂(¡(S u,n))〉));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i u n a Hiun;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) i u n a Hiun); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) i u n a Hiun);
       irewrite (Hind _ (sdepth_r t1 t2) i u n a Hiun); apply refl_equal).
  case_eq (DB_eq (ulft i) i'); intros Hii'.
   reflect_in DB_eq_imp Hii'; rewrite <- Hii' in *; clear i' Hii'; destruct i as [[| u1] n1];
    simpl; nat_simpl; bool_simpl; apply refl_equal.
   destruct i' as [[| u'] [| n']]; simpl.
    reflect_in DB_eq_imp Hiun; rewrite Hiun; apply refl_equal.
    change ¡(0,S n') with (ulft ¡(0,n')) in Hii'; rewrite DB_eq_ulft in Hii'; rewrite Hii';
     apply refl_equal.
    destruct i as [[| u1] n1]; simpl in *; nat_simpl; bool_simpl; [ | rewrite Hii'];
     apply refl_equal.
    destruct i as [[| u1] n1]; simpl in *; nat_simpl; bool_simpl; [ | rewrite Hii'];
     apply refl_equal.
  assert (H'iun:ulft i<>ulft ¡(S u,n)).
   intros Heq; apply Hiun; destruct i as [[| u1] n1]; inversion Heq; apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) i u n a Hiun);
   irewrite (Hind _ (sdepth_r t1 t2) (ulft i) u n a H'iun); apply refl_equal.
  assert (H'iun:ulft i<>ulft ¡(S u,n)).
   intros Heq; apply Hiun; destruct i as [[| u1] n1]; inversion Heq; apply refl_equal.
  irewrite (Hind _ (sdepth t) (ulft i) u n a H'iun); apply refl_equal.
Qed.

(*================================================================================================*)