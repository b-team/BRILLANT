open Page

let rec is_empty = function
    VBox pl -> List.for_all is_empty pl
  | HBox pl -> List.for_all is_empty pl
  | Word s -> s = ""

let rec iter_sep sep f = function
  | [] -> ()
  | [x] -> f x
  | x::((y::_) as l1) -> f x; sep y; iter_sep sep f l1

let hvsep fmt = function
    VBox _ -> Format.pp_print_break fmt 0 2 (*indentation de la sous-boite verticale*)
  | _ -> Format.pp_print_cut fmt ()

let rec page_txt fmt = function
    VBox pl ->
      Format.pp_open_vbox fmt 0; (* toutes les sous-boites au meme niveau *)
      iter_sep (hvsep fmt) (page_txt fmt) pl;
      Format.pp_close_box fmt ()
  | HBox pl ->
(*i      Format.open_hbox ();  i*)
      Format.pp_open_hovbox fmt 2;
      iter_sep (hvsep fmt) (page_txt fmt) pl;
      Format.close_box ()
  | Word s ->
      Format.pp_print_string fmt s


let blast_txt m =
  page_txt Format.std_formatter (Blast_page.blast_page m)
