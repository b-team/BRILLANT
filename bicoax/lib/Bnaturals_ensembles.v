(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter2.
Require Import Bfinite_subsets.
Require Import Constructive_sets.
Require Import Finite_sets_facts.
Require Import Bnaturals_basics.

Open Local Scope nat_scope.

(**
  * Sets of natural numbers: from pure BBook representation to Coq's nat representation
  *)

Definition Ensemble_bbN_of_nat (s: Ensemble nat) :=
fun x => exists y, In s y /\ x = bbN_of_nat y.

Definition Ensemble_nat_of_bbN (s: Ensemble (Ensemble BIG_type)) :=
fun x => exists y, In s y /\ x = nat_of_bbN y.

Theorem valid_Ensemble_bbN_of_nat: forall (s: Ensemble nat),
  s = Ensemble_nat_of_bbN (Ensemble_bbN_of_nat s).
Proof.
intros.
apply Extensionality_Ensembles; split.

red; intros.
red; red.
exists (bbN_of_nat x).
split.
red; red.
exists (nat_of_bbN (bbN_of_nat x)).
split.
rewrite nat_2coercions_identity; assumption.
rewrite bbN_of_nat_of_bbN.
reflexivity.
apply bbN_of_nat_in_bbN.
rewrite nat_2coercions_identity; reflexivity.

red; intros.
induction H.
induction H.
induction H.
induction H.
rewrite H1 in H0.
rewrite nat_2coercions_identity in H0.
rewrite H0; assumption.
Qed.


Theorem valid_Ensemble_nat_of_bbN: forall (s: Ensemble (Ensemble BIG_type)), In (Power_set bbN) s ->
  s = Ensemble_bbN_of_nat (Ensemble_nat_of_bbN s).
Proof.
intros.
apply Extensionality_Ensembles; split.

red; intros.
red; red.
exists (nat_of_bbN x).
split.
red; red.
exists (bbN_of_nat (nat_of_bbN x)).
split.
rewrite bbN_of_nat_of_bbN.
assumption.
induction H.
apply H; assumption.
rewrite nat_2coercions_identity.
reflexivity.
rewrite bbN_of_nat_of_bbN.
reflexivity.
induction H.
apply H; assumption.

red; intros.
induction H0.
induction H0.
induction H0.
induction H0.
rewrite H2 in H1.
rewrite bbN_of_nat_of_bbN in H1.
rewrite H1; assumption.
induction H.
apply H; assumption.
Qed.

Theorem valid_Ensemble_nat_belonging_l: forall (s: Ensemble nat) (n:nat),
  In s n -> In (Ensemble_bbN_of_nat s) (bbN_of_nat n).
Proof.
intros s n H.
red; red.
exists n.
intuition.
Qed.

Theorem valid_Ensemble_nat_belonging_r: forall (s: Ensemble nat) (n:nat),
  In (Ensemble_bbN_of_nat s) (bbN_of_nat n) -> In s n.
Proof.
intros s n H.
induction H.
induction H.
assert (n=x).
apply valid_nat_eq_r; assumption.
rewrite H1; assumption.
Qed.


Theorem valid_Ensemble_nat_belonging: forall (s: Ensemble nat) (n:nat),
  In s n <-> In (Ensemble_bbN_of_nat s) (bbN_of_nat n).
Proof.
intros s n.
split.
apply valid_Ensemble_nat_belonging_l.
apply valid_Ensemble_nat_belonging_r.
Qed.

Theorem valid_Ensemble_bbN_belonging_l: forall (s: Ensemble (Ensemble BIG_type)) (n: Ensemble BIG_type),
  In (Power_set bbN) s -> In bbN n ->
  (In s n -> In (Ensemble_nat_of_bbN s) (nat_of_bbN n)).
Proof.
intros s n H H0 H1.
red; red.
exists n.
intuition.
Qed.

Theorem valid_Ensemble_bbN_belonging_r: forall (s: Ensemble (Ensemble BIG_type)) (n: Ensemble BIG_type),
  In (Power_set bbN) s -> In bbN n ->
  (In (Ensemble_nat_of_bbN s) (nat_of_bbN n) -> In s n).
Proof.
intros s n H H0 H1.
induction H1.
induction H1.
assert (n=x).
assert (In bbN x).
induction H; apply H; assumption.
apply valid_bbN_eq_r; assumption.
rewrite H3; assumption.
Qed.

Theorem valid_Ensemble_bbN_belonging: forall (s: Ensemble (Ensemble BIG_type)) (n: Ensemble BIG_type),
  In (Power_set bbN) s -> In bbN n ->
  (In s n <-> In (Ensemble_nat_of_bbN s) (nat_of_bbN n)).
Proof.
intros s n H H0.
split.
apply valid_Ensemble_bbN_belonging_l; assumption.
apply valid_Ensemble_bbN_belonging_r; assumption.
Qed.

Theorem Ensemble_bbN_of_nat_injective: forall (s t: Ensemble nat),
  Ensemble_bbN_of_nat s = Ensemble_bbN_of_nat t -> s = t.
Proof.
intros s t H.
assert (Same_set _ (Ensemble_bbN_of_nat s) (Ensemble_bbN_of_nat t)).
rewrite H; intuition.
induction H0.
apply Extensionality_Ensembles.
split; red; intros.
apply valid_Ensemble_nat_belonging_r.
apply H0.
apply valid_Ensemble_nat_belonging_l; assumption.

apply valid_Ensemble_nat_belonging_r.
apply H1.
apply valid_Ensemble_nat_belonging_l; assumption.
Qed.

Theorem Ensemble_bbN_of_nat_empty_coercion:
  Ensemble_bbN_of_nat (Empty_set nat) = Empty_set (Ensemble BIG_type).
Proof.
apply (Extensionality_Ensembles _
  (Ensemble_bbN_of_nat (Empty_set nat)) (Empty_set (Ensemble BIG_type))).
split; red; intros.
induction H.
induction H; contradiction.
contradiction.
Qed.

Theorem Ensemble_bbN_of_nat_add_coercion: forall (s: Ensemble nat) (x: nat),
  Ensemble_bbN_of_nat (Add nat s x) =
  Add (Ensemble BIG_type) (Ensemble_bbN_of_nat s) (bbN_of_nat x).
Proof.
intros s x.
apply (Extensionality_Ensembles _
  (Ensemble_bbN_of_nat (Add nat s x)) (Add (Ensemble BIG_type) (Ensemble_bbN_of_nat s) (bbN_of_nat x))).
split; red; intros.

induction H.
induction H.
induction H.
left.
rewrite H0.
apply valid_Ensemble_nat_belonging_l; assumption.
right.
induction H.
intuition.

induction H.
induction H.
induction H.
rewrite H0.
apply valid_Ensemble_nat_belonging_l.
left; assumption.
induction H.
apply valid_Ensemble_nat_belonging_l.
right; intuition.
Qed.

Theorem finite_Ensemble_bbN_of_nat: forall (s: Ensemble nat), Finite s ->
  Finite (Ensemble_bbN_of_nat s).
Proof.
intros s H.
induction H.
rewrite Ensemble_bbN_of_nat_empty_coercion.
left.
rewrite  Ensemble_bbN_of_nat_add_coercion.
right.
assumption.
intros H1; apply H0.
apply valid_Ensemble_nat_belonging_r; assumption.
Qed.

Theorem Power_set_Ensemble_bbN_of_nat: forall (s: Ensemble nat), In (Power_set (Full_set nat)) s ->
  In (Power_set bbN) (Ensemble_bbN_of_nat s).
Proof.
intros s H.
induction H.
constructor.
red; intros.
induction H0; induction H0.
rewrite H1.
apply valid_Ensemble_bbN_belonging_r.
constructor; intuition.
apply bbN_of_nat_in_bbN.
rewrite nat_2coercions_identity.
replace (Ensemble_nat_of_bbN bbN) with (Full_set nat).
apply H; assumption.
apply Extensionality_Ensembles; split; red; intros.
do 2 red.
exists (bbN_of_nat x1).
split.
apply bbN_of_nat_in_bbN.
symmetry; apply nat_2coercions_identity.
constructor.
Qed.

Theorem Power_set1_Ensemble_bbN_of_nat: forall (s: Ensemble nat), In (Power_set1 (Full_set nat)) s ->
  In (Power_set1 bbN) (Ensemble_bbN_of_nat s).
Proof.
intros s H.
induction H; split.
apply Power_set_Ensemble_bbN_of_nat; assumption.
intros H1; apply H0.
inversion H1.
rewrite <- Ensemble_bbN_of_nat_empty_coercion in H3.
assert ( Empty_set nat = s ).
apply Ensemble_bbN_of_nat_injective; assumption.
rewrite <- H2; intuition.
Qed.

Theorem Power_finite1_Ensemble_bbN_of_nat: forall (s: Ensemble nat), In (Power_finite1 (Full_set nat)) s ->
  In (Power_finite1 bbN) (Ensemble_bbN_of_nat s).
Proof.
intros s H.
induction H.
induction H.
constructor.
constructor.
apply finite_Ensemble_bbN_of_nat; assumption.
red; intros.
induction H2.
induction H2.
rewrite H3; apply bbN_of_nat_in_bbN.
intros H2; apply H0.
induction H.
intuition.
rewrite Ensemble_bbN_of_nat_add_coercion in H2.
absurd (In (Singleton (Empty_set (Ensemble BIG_type))) (Add (Ensemble BIG_type) (Ensemble_bbN_of_nat A) (bbN_of_nat x))).
assert ((Add (Ensemble BIG_type) (Ensemble_bbN_of_nat A) (bbN_of_nat x)) <> (Empty_set (Ensemble BIG_type))).
apply Add_not_Empty.
intros H5; apply H4.
induction H5; reflexivity.
assumption.
Qed.

Theorem Power_set1_singleton_nat: forall (a: nat), In (Power_set1 (Full_set nat)) (Singleton a).
Proof.
intros a.
constructor.
constructor.
red; intros; constructor.
intros H.
inversion H.
symmetry in H1; generalize H1; apply singleton003.
Qed.

Theorem Power_finite1_singleton_nat: forall (a: nat), In (Power_finite1 (Full_set nat)) (Singleton a).
Proof.
intros a.
constructor.
constructor.
apply Singleton_is_finite.
red; intros; constructor.
intros H.
generalize (Singleton_inv _ (Empty_set _) (Singleton a) H).
intros.
assert (Same_set _ (Empty_set nat) (Singleton a)).
rewrite H0; intuition.
induction H1.
generalize (H2 a); intros.
assert (In (Singleton a) a); [ intuition | ].
generalize (H3 H4); intros; contradiction.
Qed.

Theorem valid_Ensemble_bbN_of_nat_singleton: forall (a: nat), Singleton (bbN_of_nat a) = Ensemble_bbN_of_nat (Singleton a).
Proof.
intros a.
apply Extensionality_Ensembles.
split; red; intros.

induction H.
apply valid_Ensemble_nat_belonging_l; intuition.

induction H.
induction H.
induction H.
rewrite H0; intuition.
Qed.

Theorem Power_set1_union_nat: forall (a b: Ensemble nat), In (Power_set1 (Full_set nat)) a -> In (Power_set1 (Full_set nat)) b ->
  In (Power_set1 (Full_set nat)) (Union a b).
Proof.
intros a b H H0.
induction H; induction H0.
split.
constructor; red; intros.
induction H3.
induction H; apply H; assumption.
induction H0; apply H0; assumption.
intros H3.
inversion H3.
assert (Same_set _ (Empty_set nat) (Union a b)).
rewrite H5; intuition.
induction H4.
assert (Inhabited _ a); [ apply not_empty_Inhabited; intuition | ].
induction H7.
generalize (H6 x); intros.
assert (In (Union a b) x); [ left; assumption | ].
generalize (H6 _ H9); intros; contradiction.
Qed.

Theorem Power_finite1_union_nat: forall (a b: Ensemble nat), In (Power_finite1 (Full_set nat)) a -> In (Power_finite1 (Full_set nat)) b ->
  In (Power_finite1 (Full_set nat)) (Union a b).
Proof.
intros a b H H0.
constructor.
constructor.
apply Union_preserves_Finite.
do 2 (induction H); assumption.
do 2 (induction H0); assumption.
red; intros; constructor.
intros H1.
induction H; induction H0.
assert ((Union a b) = (Empty_set _)).
induction H1; intuition.
assert (Same_set _ (Union a b) (Empty_set _)).
rewrite H4; intuition.
induction H5.
assert (Inhabited _ a); [ apply not_empty_Inhabited; intuition | ].
induction H7.
generalize (H5 x); intros.
assert (In (Union a b) x); [ left; assumption | ].
generalize (H8 H9); intros; contradiction.
Qed.

Theorem valid_Ensemble_bbN_of_nat_Union: forall (s t: Ensemble nat),
  Ensemble_bbN_of_nat (Union s t) =
  Union (Ensemble_bbN_of_nat s) (Ensemble_bbN_of_nat t).
Proof.
intros s t.
apply (Extensionality_Ensembles _
  (Ensemble_bbN_of_nat (Union s t)) (Union (Ensemble_bbN_of_nat s) (Ensemble_bbN_of_nat t))).
split; red; intros.

induction H.
induction H.
induction H.
left.
rewrite H0.
apply valid_Ensemble_nat_belonging_l; assumption.
right.
rewrite H0.
apply valid_Ensemble_nat_belonging_l; assumption.

induction H; induction H; induction H.
rewrite H0.
apply valid_Ensemble_nat_belonging_l.
left; assumption.
rewrite H0.
apply valid_Ensemble_nat_belonging_l.
right; assumption.
Qed.

Theorem valid_Union_Ensemble_nat_of_bbN: forall (s t: Ensemble nat),
  Union s t =
  Ensemble_nat_of_bbN (Union (Ensemble_bbN_of_nat s) (Ensemble_bbN_of_nat t)).
Proof.
intros s t.
rewrite (valid_Ensemble_bbN_of_nat (Union s t)).
rewrite <- valid_Ensemble_bbN_of_nat_Union.
reflexivity.
Qed.
