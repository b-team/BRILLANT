/*? **************************** DIGILOG ************************************
 * Date : @(#) sccs get 6/7/96
 *
 * File : Service_B_1.mch
 *
 * Version : 1.8
 *
 * Directory : /home/IPSN/ebt/DEMON/Control/spec/SCCS/s.Service_B_1.mch
 *
 * Type	:	MACHINE
 *
 * Object : Service which tests the water level and 
 *			determines open/close commands
 *
 * Copyright : (C) 1996 Jean-Raymond ABRIAL / DIGILOG - STERIA Mediterranee
 *
 *   Permission is granted to make and distribute verbatim copies of
 *   this file provided the copyright notice and this permission notice
 *   are preserved on all copies.
 *
 *   Permission is granted to copy and distribute modified versions of
 *   this file under the conditions for verbatim copying, provided also
 *   that the entire resulting derived work is distributed under the terms
 *   of a permission notice identical to this one.
 *
 *   Permission is granted to copy and distribute translations of this
 *   manual into another language, under the above conditions for modified
 *   versions
 *
 *	 This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *   This program can be redistributed or modified under the terms of
 *   the GNU General Public License. The General Public License can be
 *	 found in the file COPYING tha accompanies the source code.
 *
 *---------------------------------------------------------------------------
 *
 * Functions :
 *		functional_level_test
 *		level_safe_test
 *---------------------------------------------------------------------------
 *
 * Modifications :
 *
 ************************************************************************* ?*/

MACHINE
	Service_B_1

SEES
	Cycle_L_1, Constants_L_1

VISIBLE_VARIABLES
	pop, pcl, lvs

INVARIANT
	pop : BOOL &
	pcl : BOOL &
	lvs : BOOL

INITIALISATION
	pop :: BOOL ||
	pcl :: BOOL ||
	lvs :: BOOL

OPERATIONS
	functional_level_test =
		BEGIN
			/* No need to wait that the adjusted water level are outside
			   the limits.
			   We could anticipate by comparing the calculated level
			   with the nominal functionning levels 
			*/
			pop := bool (qc2 <= N2 & qc1 < N1) ||
			pcl := bool (qc2 > N2 & qc1 >= N1) 
		END;

	level_safe_test =
		BEGIN
			lvs := bool (qa1 >= M1 & qa2 <= M2)
		END
END

