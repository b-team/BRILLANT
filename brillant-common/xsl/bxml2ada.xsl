<!-- $Id$ -->

<!-- BRILLANT project -->
<!-- https://gna.org/projects/brillant/ -->



<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns="http://www.w3.org/TR/REC-html40">

<xsl:output method="text" omit-xml-declaration="yes"/>

<xsl:template match="AMN">
  <xsl:apply-templates/>
</xsl:template>

<!-- MACHINE ou REFINEMENT ou IMPLEMENTATION-->

<xsl:template match="MACHINE | REFINEMENT | IMPLEMENTATION | EmptyTree">
  <xsl:choose>
    <xsl:when test="self::MACHINE">
    </xsl:when>
    <xsl:when test="self::REFINEMENT">
      <xsl:text>
-- generated from REFINEMENT </xsl:text>
    </xsl:when>
    <xsl:when test="self::IMPLEMENTATION">
      <xsl:text>
-- generated from IMPLEMENTATION </xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>
        ARBRE VIDE
      </xsl:text>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:apply-templates select="Head"/>
  <xsl:apply-templates select="ClausesList"/>
  <xsl:text>
    
    private
  </xsl:text>
  <xsl:apply-templates select="ClausesList/CVARIABLES" mode="typedefinition"/>  
  <xsl:text>
    type  ABSTRACT_TYPE_</xsl:text>
    <xsl:apply-templates select="Head/id"/>
  <xsl:text> is 
    record 
    initialised : Boolean;
</xsl:text>
<xsl:apply-templates select="//Head/HeadPar" mode="withtype"/>
    <xsl:apply-templates select="ClausesList/CVARIABLES"/>
    <xsl:text>
      end record;
    </xsl:text>
    <xsl:text> end </xsl:text>   
    <xsl:apply-templates select="Head/id"/>
    <xsl:text>; </xsl:text>   
  </xsl:template>
  
<xsl:template match="Head">
  <xsl:text>package </xsl:text>   
  <xsl:value-of select="id"/>
  <xsl:text> is 
  
  type ABSTRACT_TYPE_</xsl:text>
  <xsl:apply-templates select="id"/>
  <xsl:text> is private ;</xsl:text>   
</xsl:template>

  
<xsl:template match="HeadPar">
</xsl:template>

<xsl:template match="HeadPar" mode="withtype">
    <xsl:apply-templates select="." mode="typesimple"/>
</xsl:template>
  
  
<xsl:template match="IdentListPar">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
  </xsl:for-each>
</xsl:template>
  
<xsl:template match="IdentTypedListPar">
  <xsl:apply-templates select="IdentTypedPar"/>
</xsl:template>
  
<xsl:template match="IdentTypedPar">
  <xsl:apply-templates select="id"/>
  <xsl:apply-templates select="Type"/>
</xsl:template>

<xsl:template match="Type">
  <xsl:choose>
    <xsl:when test="child::ExprNatRange">
      <xsl:apply-templates select="ExprNatRange"/>
    </xsl:when>
    <xsl:when test="child::PredType">
	<!-- Pas trouve dans la DTD-->
    </xsl:when>
    <xsl:when test="child::Record">
      <xsl:apply-templates select="Record"/>
    </xsl:when>
    <xsl:when test="child::TypeIdentifier">
      <xsl:apply-templates select="id"/>
    </xsl:when>
  </xsl:choose>
</xsl:template>
  
<xsl:template match="ExprNatRange">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>
  
<!--   les clauses  -->

<xsl:template match="ClausesList">
  <xsl:call-template name="clauses"/>
</xsl:template>
  
<!-- Clause SETS -->

<xsl:template match="SETS">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position() != last()">
      <xsl:text> ; </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>
  
<xsl:template match="SetEnumDec">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> = { </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>}</xsl:text>
</xsl:template>
  
<xsl:template match="SetAbstract">
  <xsl:apply-templates select="id"/>
</xsl:template>
  
<!-- INITIALISATION -->

<xsl:template match="INITIALISATION">
  <xsl:apply-templates/>
</xsl:template>
  
<!-- Clause VALUES -->

<xsl:template match="VALUES">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position()!=last()">
      <xsl:text> ; </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>
  
<xsl:template match="Valuation">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> = </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- Clause REFINES -->

<xsl:template match="REFINES">
  <xsl:apply-templates select="*"/>
</xsl:template>

<!-- Clause SEES -->

<xsl:template match="SEES">
  <xsl:for-each select="*">
    <xsl:value-of select="."/>
    <xsl:if test="position() != last()">
      <xsl:text>,
       </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<!-- Clauses CONSTANTS, PROMOTES, USES, var, TypeIdentifier -->

<xsl:template match="CONSTANTS | var | TypeIdentifier">
  <xsl:apply-templates select="id"/>
</xsl:template>

<xsl:template match="PROMOTES | USES">
  <xsl:for-each select="*">
    <xsl:value-of select="."/>
    <xsl:if test="position() != last()">
      <xsl:text>,
       </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<!-- Clauses avec une liste d'identifiant -->

<xsl:template match="VARIABLES | CVARIABLES | AVARIABLES | HVARIABLES | HCONSTANTS | CCONSTANTS | ACONSTANTS">
  <xsl:apply-templates select="idList" mode="typedeclaration"/>
</xsl:template>

<xsl:template match="CVARIABLES" mode="typedefinition">
  <xsl:apply-templates select="idList" mode="typedefinition"/>
</xsl:template>

<!-- clause PROPERTIES, CONSTRAINTS, INVARIANT, ASSERTIONS -->

<xsl:template match="PROPERTIES">
  <xsl:apply-templates select="Predicate"/>
</xsl:template>

<xsl:template match="CONSTRAINTS | INVARIANT">
  <xsl:apply-templates select="Predicate"/>
</xsl:template>

<xsl:template match="ASSERTIONS">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position() != last()">
      <xsl:text>; </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>


<!-- clause EXTENDS, INCLUDES, IMPORTS -->

<xsl:template match="EXTENDS | INCLUDES | IMPORTS">
  <xsl:for-each select="*">
    <xsl:call-template name="instance"/>
    <xsl:if test="position() != last()">
      <xsl:text>,
      </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template name="instance">
  <xsl:for-each select="*">
    <xsl:choose>
      <xsl:when test="self::id">
        <xsl:apply-templates select="../id"/>
      </xsl:when>
      <xsl:when test="self::ExprList">
        <xsl:if test="count(*)!=0">
        <xsl:text>(</xsl:text>
        <xsl:apply-templates select="../ExprList"/>
        <xsl:text>)</xsl:text>
        </xsl:if>
      </xsl:when>
    </xsl:choose>
  </xsl:for-each>
</xsl:template>


<!-- Predicats -->

<xsl:template match="Predicate">
  <xsl:apply-templates select="*"/>
</xsl:template>

<!-- 4.1 -->
<!-- Propositions -->

<xsl:template match="PredParen">
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="*"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Neg">
  <xsl:text> not </xsl:text>
  <xsl:apply-templates select="*"/>
</xsl:template>

<xsl:template match="And">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:choose>
    <xsl:when test="parent::ExprParen">
    <!-- xxxxxxxxxx -->
    </xsl:when>
    <xsl:otherwise>
    <!-- xxxxxxxxxx -->
    </xsl:otherwise>
  </xsl:choose>
  <xsl:text disable-output-escaping="yes"> &amp; </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Or">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> or </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Implies">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> =&gt; </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Equiv">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping = "yes"> &lt;=&gt; </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 4.2 -->
<!-- Predicats Quantifies -->

<xsl:template match="Exists">
  <xsl:text> # </xsl:text>
  <xsl:apply-templates select="idList"/>
  <xsl:text>.(</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="ForAll">
  <xsl:text> ! </xsl:text>
  <xsl:apply-templates select="idList"/>
  <xsl:text> . ( </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<!-- 4.3 -->
<!-- Predicats egalites -->

<xsl:template match="Equal">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> = </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="NotEqual">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> /= </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 4.4 -->
<!-- Predicats d'appartenance -->

<xsl:template match="In">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> : </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="NotIn">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> /: </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 4.5 -->
<!-- Predicats d'inclusions -->

<xsl:template match="SubSet">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> &lt;: </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="StrictSubSet">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> &lt;&lt;: </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="NotsubSet">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> /&lt;: </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="NotStrictSubSet">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> /&lt;&lt;: </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 4.6 -->
<!-- Predicats de comparaison -->

<xsl:template match="LessEqual">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> &lt;= </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Less">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> &lt; </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="GreaterEqual">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> &gt;= </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Greater">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> &gt; </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- Expressions -->

<xsl:template match="Expr">
  <xsl:apply-templates select="*"/>
</xsl:template>

<xsl:template match="ExprParen">
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="*"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<!-- Expressions arithmetiques -->

<xsl:template match="MAXINT">
  <xsl:text> MAXINT </xsl:text>
</xsl:template>

<xsl:template match="MININT">
  <xsl:text> MININT </xsl:text>
</xsl:template>

<xsl:template match="Minus">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> - </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="UMinus">
  <xsl:text>-</xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="Plus">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> + </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Mul">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> * </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Div">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> / </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Modulo">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> mod </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Puissance">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> ** </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Succ">
  <xsl:apply-templates select="Expr"/> + 1
</xsl:template>

<!-- 5.4 -->

<xsl:template match="Min">
  <xsl:text> min(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Max">
  <xsl:text> max(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Card">
  <xsl:text> card(</xsl:text>
  <xsl:apply-templates select="Expr"/>
<xsl:text>)</xsl:text>
</xsl:template>


<!-- 5.6 -->
<!-- Ensembles Predefinis -->

<xsl:template match="SetPredefined">
  <xsl:apply-templates select="child::*[position()=1]"/>
</xsl:template>

<xsl:template match="NAT">
  <xsl:text>NAT </xsl:text>
</xsl:template>

<xsl:template match="INT">
  <xsl:text>INT </xsl:text>
</xsl:template>

<xsl:template match="BOOL">
  <xsl:text>BOOL </xsl:text>
</xsl:template>

<xsl:template match="INTEGER">
  <xsl:text>INTEGER </xsl:text>
</xsl:template>

<xsl:template match="NATURAL">
  <xsl:text>NATURAL </xsl:text>
</xsl:template>

<xsl:template match="NAT1">
  <xsl:text>NAT1 </xsl:text>
</xsl:template>

<xsl:template match="NATURAL1">
  <xsl:text>NATURAL1 </xsl:text>
</xsl:template>

<xsl:template match="SetEmpty">
  <xsl:text> {} </xsl:text>
</xsl:template>

<xsl:template match="STRING">
  <xsl:text> "</xsl:text>
  <xsl:value-of select="."/>
  <xsl:text>"</xsl:text>
</xsl:template>


<!-- 5.8 -->
<!-- Expressions ensemblistes -->

<xsl:template match="InterSets">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> /\ </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="UnionSets">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> \/ </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- Intersection et union Quantifiee -->

<xsl:template match="InterQ">
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> | </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> ) = inter{</xsl:text>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="UnionQ">
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> | </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> ) = union{</xsl:text>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <xsl:text>}</xsl:text>
</xsl:template>

<!-- intersection et union generalisee -->

<xsl:template match="UnionG">
  <xsl:text> union{</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="InterG">
  <xsl:text> inter{</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>}</xsl:text>
</xsl:template>

<!-- 5.9 -->
<!-- Expressions de records -->

<xsl:template match="Records">
    <xsl:text> rec(</xsl:text>
    <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position()!=last()">
      <xsl:text>,</xsl:text>
    </xsl:if>
  </xsl:for-each>
    <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="RecordAccess">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>'</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="RecWithFields">
  <xsl:text>
struct(
  </xsl:text>
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position()!=last()">
      <xsl:text>,</xsl:text>
    </xsl:if>
  </xsl:for-each>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="RecordItem">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> : </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Field">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> : </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="SetEqualRecord">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> := </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 5.10 -->
<!-- Ensemble de Relations -->

<xsl:template match="RelSet">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> &lt;-&gt; </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 5.11 -->
<!-- Expressions de relations -->

<xsl:template match="Identity">
  <xsl:text> id(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Tilde">
  <xsl:apply-templates select="Expr"/>
  <xsl:text> ~ </xsl:text>
</xsl:template>

<xsl:template match="PrjOne">
  <xsl:text> prj1 </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="PrjTwo">
  <xsl:text> prj2 </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="RelSeqComp">
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>; </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="ParallelComp">
  <xsl:text> ParallelComp </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> ||
  </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="RelProd">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> &gt;&lt; </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 5.12 -->
<!-- Expressions de relations -->

<xsl:template match="RelIter">
  <xsl:text> RelIter </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>,</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- fermeture -->

<xsl:template match="Closure">
  <xsl:text> Closure </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<!-- fermeture reflexive transitive -->

<xsl:template match="ClosureOne">
  <xsl:text> closure1 </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<!-- 5.13 -->
<!-- Expressions de relations -->

<xsl:template match="Dom"> <!-- Domaine -->
  <xsl:text> dom(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Ran"> <!-- Image -->
  <xsl:text> ran(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<!-- 5.14 -->
<!-- Expressions de relations -->

<xsl:template match="DomSubstract">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> &lt;&lt;| </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="RanSubstract">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> |&gt;&gt; </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="DomRestrict">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> &lt;| </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="RanRestrict">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> |&gt; </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 5.15 -->
<!-- Ensembles de fonctions -->

<xsl:template match="PartialFunc">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> +-&gt; </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="TotalFunc">
  <xsl:text disable-output-escaping="yes"> --&gt; </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> --&gt; </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="PartialInj">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> &gt;+&gt; </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="TotalInj">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> &gt;-&gt; </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="PartialSurj">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> +-&gt;&gt; </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="TotalSurj">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> --&gt;&gt; </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="PartialBij">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> &gt;+-&gt;&gt; </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="TotalBij">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> &gt;-&gt;&gt; </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>


<!-- 5.16 -->
<!-- Expressions de fonctions -->

<xsl:template match="Lambda">
  <xsl:text> % </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>.(</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> | </xsl:text>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="ExprFunCall">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:if test="child::ExprList and string(child::ExprList)">
    <xsl:text>(</xsl:text>
    <xsl:apply-templates select="child::*[position()=2]"/>
    <xsl:text>)</xsl:text>
  </xsl:if>
</xsl:template>

<xsl:template match="RelFnc">
  <xsl:text> RelFnc </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="FncRel">
  <xsl:text> FncRel </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<!-- 5.17 -->
<!-- Ensembles de sequences -->

<xsl:template match="Seq">
  <xsl:text> seq(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>) </xsl:text>
</xsl:template>

<xsl:template match="SeqOne">
  <xsl:text> seq1(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>) </xsl:text>
</xsl:template>

<xsl:template match="ISeq">
  <xsl:text> iseq(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>) </xsl:text>
</xsl:template>

<xsl:template match="ISeqOne">
  <xsl:text> iseq1(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>) </xsl:text>
</xsl:template>

<xsl:template match="Perm">
  <xsl:text> perm </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<!-- 5.18 -->
<!-- Expressions de sequences -->

<xsl:template match="Size">
  <xsl:text> size(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>) </xsl:text>
</xsl:template>

<xsl:template match="First">
  <xsl:text> first(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>) </xsl:text>
</xsl:template>

<xsl:template match="Last">
  <xsl:text> last(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>) </xsl:text>
</xsl:template>

<xsl:template match="Tail">
  <xsl:text> tail(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>) </xsl:text>
</xsl:template>

<xsl:template match="Front">
  <xsl:text> front(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>) </xsl:text>
</xsl:template>

<xsl:template match="Rev">
  <xsl:text> rev(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>) </xsl:text>
</xsl:template>

<!-- 5.19 -->
<!-- Expressions de sequences -->

<xsl:template match="AppendSeq">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> &lt;- </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="ConcatSeq">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> ^ </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="PrependSeq">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> -&gt; </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="PrefixSeq">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> /|\ </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="SuffixSeq">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> \|/ </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Conc">
  <xsl:text> conc </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<!-- 5.20 -->
<!-- Ensembles d'arbres -->

<xsl:template match="Tree">
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="BTree">
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<!-- 5.21 -->
<!-- Expressions d'arbres -->

<xsl:template match="Const">
  <xsl:text> const </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> , </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Top">
  <xsl:text> top </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="Sons">
  <xsl:text> sons </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="Prefix">
  <xsl:text> prefix </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="Postfix">
  <xsl:text> postfix </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="SizeT">
  <xsl:text> sizet </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="Mirror">
  <xsl:text> mirror </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<!-- 5.22 -->
<!-- Expressions de noeuds d'arbres -->

<xsl:template match="Rank">
  <xsl:text> rank </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> , </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Father">
  <xsl:text> father </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> , </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Son">
  <xsl:text> son </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> , </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Subtree">
  <xsl:text> subtree </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> , </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Arity">
  <xsl:text> arity </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> , </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 5.23 -->
<!-- Expressions d'arbre binaires -->

<xsl:template match="Bin">
  <xsl:text> bin </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> , </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> , </xsl:text>
  <xsl:apply-templates select="child::*[position()=3]"/>
</xsl:template>

<xsl:template match="Left">
  <xsl:text> left </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="Right">
  <xsl:text> right </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="Infix">
  <xsl:text> infix </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="ExprSequence">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>,</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="NatSetRange">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>..</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="OverRideFwd">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>&lt;+</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="OverRideBck">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="ExprId">
  <xsl:apply-templates select="id"/>
</xsl:template>

<xsl:template match="ExprList">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position()!=last()">
      <xsl:text>,</xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="Assign">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- OPERATIONS -->

<xsl:template match="OPERATIONS">
  <xsl:apply-templates select="Operation"/>
</xsl:template>

<xsl:template match="Operation">
  <xsl:apply-templates select="child::*[position()=1]"/>
</xsl:template>

<xsl:template match="OperHead" mode="endoperation">
  <xsl:text> 
end </xsl:text>  
  <xsl:apply-templates select="OperName" mode="onlyname"/>
  </xsl:template>
  

<xsl:template match="OperHead">
    <xsl:choose>
      <xsl:when test="child::OperOut and string(child::OperOut)">
            <xsl:text>
      function  </xsl:text>
      </xsl:when>
      <xsl:otherwise>
    <xsl:text>
      procedure  </xsl:text>        
      </xsl:otherwise>
    </xsl:choose>
      <xsl:apply-templates select="OperName" />
    <xsl:if test="child::OperIn and string(child::OperIn)">
      <!-- inutile car compris dans la decompilation du this...    <xsl:text>(</xsl:text>-->
      <xsl:apply-templates select="OperIn"/>
    </xsl:if>
    <xsl:text>)</xsl:text>
    <xsl:if test="child::OperOut and string(child::OperOut)">    
    <xsl:text> return </xsl:text>
    <xsl:apply-templates select="OperOut" mode="onlytype"/>  
  </xsl:if> 
  <xsl:text>; </xsl:text>
</xsl:template>

<xsl:template match="OperName">
  <xsl:apply-templates select="id"/>   
  <xsl:choose>
    <xsl:when test="../ModifyComponentState">
        <xsl:text>(this : in out ABSTRACT_TYPE_stack </xsl:text>
    </xsl:when>
    <xsl:otherwise>
        <xsl:text>(this : in ABSTRACT_TYPE_stack </xsl:text>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="OperName" mode="onlyname">
  <xsl:apply-templates select="id"/>   
</xsl:template>

<xsl:template match="OperOut">
  <xsl:apply-templates select="idList" mode="OperOut"/>
</xsl:template>

<xsl:template match="OperOut" mode="onlytype">
  <xsl:apply-templates select="idList/idtyped/typesimple" />
</xsl:template>


<xsl:template match="OperIn">
  <xsl:apply-templates select="idList" mode="OperIn"/>
</xsl:template>


<xsl:template match="idList" mode="OperOut">
  <xsl:for-each select="*">
    <xsl:value-of select="."/>
    <xsl:if test="position() != last()">
      <xsl:text>;</xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="idList" mode="OperIn">
  <xsl:for-each select="*">
      <xsl:text>; </xsl:text>
      <xsl:apply-templates select="."/>
  </xsl:for-each>
</xsl:template>

<xsl:template match="idtyped">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> : </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="idtyped" mode="typesimple">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> : </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]" mode="typesimple"/>
</xsl:template>

<xsl:template match="idtyped" mode="typedefinition">
  <xsl:apply-templates select="child::*[position()=2]" mode="typedefinition"/>
</xsl:template>

<xsl:template match="typesimple">
  <xsl:apply-templates select="child::*" mode="typesimple"/>
</xsl:template>


<xsl:template match="NAT" mode="typesimple">
  <xsl:text>Integer </xsl:text>
</xsl:template>


<xsl:template match="BOOL" mode="typesimple">
  <xsl:text>boolean </xsl:text>
</xsl:template>

<xsl:template match="TotalFunc" mode="typesimple">
  <xsl:apply-templates select="../../*[position()=1]"/>
  <xsl:text>_data_type </xsl:text> 
</xsl:template>

<xsl:template match="TotalFunc" mode="typedefinition">
  <xsl:text>type  </xsl:text> 
  <xsl:apply-templates select="../../*[position()=1]"/>
  <xsl:text>_data_type</xsl:text> 
  <xsl:text> is array (</xsl:text>
  <xsl:apply-templates select="child::*[position()=1]" mode="typedefinition"/>
  <xsl:text>) of </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]" mode="typesimple"/>
</xsl:template>

<xsl:template match="NatSetRange" mode="typesimple">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>..</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="NatSetRange" mode="typedefinition">
  <xsl:apply-templates select="child::*[position()=1]" mode="typedefinition"/>
  <xsl:text>..</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]" mode="typedefinition"/>
</xsl:template>

<xsl:template match="Expr" mode="typedefinition">
  <xsl:apply-templates select="*" mode="typedefinition"/>
</xsl:template>


<xsl:template match="Number" mode="typedefinition" >
  <xsl:value-of select="."/>
</xsl:template>
  
<xsl:template match="id" mode="typedefinition">
  <xsl:text>Integer'last</xsl:text>
</xsl:template>


<xsl:template match="OperBody">
  <xsl:text> 
begin
</xsl:text>
  <xsl:apply-templates select="*"/>
</xsl:template>

<xsl:template match="idList" mode="typedeclaration">
  <xsl:for-each select="*">
    <xsl:text>;
</xsl:text>
    <xsl:apply-templates select="."/>
  </xsl:for-each>
    <xsl:text>;
</xsl:text>
</xsl:template>

<xsl:template match="idList" mode="typedefinition">
  <xsl:for-each select="*">
    <xsl:apply-templates select="." mode="typedefinition"/>
    <xsl:if test="position() != last()">
      <xsl:text>;</xsl:text>      
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="idList">
  <xsl:for-each select="*">
    <xsl:value-of select="."/>
    <xsl:if test="position() != last()">
      <xsl:text>,</xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<!-- 6 -->
<!-- Substitutions -->
<!-- 6.1 -->

<xsl:template match="Block">
  <xsl:text>
    begin
  </xsl:text>
  <xsl:apply-templates select="*"/>
  <xsl:text>
    end; 
  </xsl:text>
</xsl:template>

<!-- 6.2 -->

<xsl:template match="Skip">
  <xsl:text> () </xsl:text>
</xsl:template>

<!-- 6.3 -->

<xsl:template match="SetEqualIds">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> := </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="SetEqualFun">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> := </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 6.4 -->
<!-- Precondition substitution -->

<xsl:template match="Precondition">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="PRE">
  <xsl:text>
--    PRE Condition :   </xsl:text>
  <xsl:apply-templates select="Predicate"/>
</xsl:template>

<xsl:template match="THEN">
  <xsl:apply-templates select="*"/>
</xsl:template>

<!-- 6.7 -->
<!-- IF Substitution -->

<xsl:template match="IF">
  <xsl:for-each select="*">
    <xsl:choose>
      <xsl:when test="self::IfThen and position()=1">
        <xsl:call-template name="IfThen1"/>
      </xsl:when>
      <xsl:when test="self::IfThen and position()!=1">
        <xsl:call-template name="IfThen2"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>
    else
        </xsl:text>
        <xsl:apply-templates select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:for-each>
</xsl:template>

<xsl:template name="IfThen1">
    <xsl:text>
    if
    </xsl:text>
    <xsl:apply-templates select="child::*[position()=1]"/>
    <xsl:text>
    then 
    </xsl:text>
    <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template name="IfThen2">
    <xsl:text>
    else if 
    </xsl:text>
    <xsl:apply-templates select="child::*[position()=1]"/>
    <xsl:text>
    then 
    </xsl:text>
    <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>


<xsl:template match="WhenPart">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>
    THEN
  </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 6.9 -->
<!-- CASE Substitution -->

<xsl:template match="CASE">
  <xsl:text>
    match 
  </xsl:text>
  <xsl:for-each select="*">
    <xsl:choose>
      <xsl:when test="position()=1">
        <xsl:apply-templates select="."/>
      </xsl:when>
      <xsl:when test="position()=2">
        <xsl:text>
    with 
        </xsl:text>
        <xsl:apply-templates select="."/>
      </xsl:when>
      <xsl:when test="position()=3">
        <xsl:text>
    | _ -> 
        </xsl:text>
        <xsl:apply-templates select="."/>
      </xsl:when>
    </xsl:choose>
  </xsl:for-each>
</xsl:template>

<xsl:template match="OrPartList">
  <xsl:for-each select="*">
    <xsl:choose>
      <xsl:when test="self::OrPart and position()=1">
        <xsl:call-template name="OrPart1"/>
      </xsl:when>
      <xsl:when test="self::OrPart and position()!=1 ">
        <xsl:call-template name="OrPart2"/>
      </xsl:when>
      <xsl:otherwise/>
    </xsl:choose>
  </xsl:for-each>
</xsl:template>

<xsl:template name="OrPart1">
  <xsl:text> EITHER </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> THEN </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template name="OrPart2">
  <xsl:text> OR </xsl:text>
  <xsl:call-template name="ExprOrPart"/>
  <xsl:text> THEN </xsl:text>
  <xsl:apply-templates select="child::*[position()=last()]"/>
</xsl:template>

<xsl:template name="ExprOrPart">
  <xsl:for-each select="Expr">
    <xsl:apply-templates/>
    <xsl:if test="position()!=last()">
      <xsl:text>,</xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>



<!-- 6.11 -->
<!-- LET Substitution -->

<xsl:template match="LET">
  <xsl:text>
    let 
  </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>
    =
  </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>
    in
  </xsl:text>
  <xsl:apply-templates select="child::*[position()=3]"/>
</xsl:template>

<!-- 6.12 -->

<xsl:template match="SetIn">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> :: </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 6.13 -->

<xsl:template match="BecomeSuch">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> :(</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<!-- 6.14 -->
<!-- VAR Substitution -->

<xsl:template match="VAR">
  <xsl:text>
    let 
  </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>
    =
  </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 6.15 -->

<xsl:template match="Sequence">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position() != last()">
      <xsl:text>; </xsl:text>
    </xsl:if>
    </xsl:for-each>
</xsl:template>


<!-- 6.16 -->

<xsl:template match="OperCall">
  <xsl:for-each select="*">
    <xsl:choose>
      <xsl:when test="self::idList and string(self::idList)">
        <xsl:apply-templates select="."/>
	<xsl:text disable-output-escaping="yes"> &lt;-- </xsl:text>
      </xsl:when>
      <xsl:when test="self::Expr">
        <xsl:apply-templates select="."/>
      </xsl:when>
      <xsl:when test="self::ExprList and count(child::*)!=0">
        <xsl:text>(</xsl:text>
        <xsl:apply-templates select="."/>
        <xsl:text>)</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:for-each>
</xsl:template>
  
<!-- 6.17 -->
<xsl:template match="WHILE">
  <xsl:text>
      WHILE
  </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>
      DO
  </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>
      INVARIANT
  </xsl:text>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <xsl:text>
      VARIANT
  </xsl:text>
  <xsl:apply-templates select="child::*[position()=4]"/>
  <xsl:text>
      END /* WHILE */
  </xsl:text>
</xsl:template>

<xsl:template match="WhileTest">
  <xsl:apply-templates select="Predicate"/>
</xsl:template>
  
<xsl:template match="WhileInvariant">
  <xsl:apply-templates select="Predicate"/>
</xsl:template>
  
<xsl:template match="WhileVariant">
  <xsl:apply-templates select="Expr"/>
</xsl:template>
  
<xsl:template match="WhileBody">
  <xsl:apply-templates select="*"/>
</xsl:template>
  
<!-- 6.18 -->
<xsl:template match="Parallel">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> ||
      </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>
  
<!-- Afficher l'identifiant -->
<xsl:template match="id">
    <xsl:value-of select="."/>
</xsl:template>
  
<xsl:template match="idrename">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>
  
<xsl:template match="Constant">
  <xsl:choose>
    <xsl:when test="child::number">
      <xsl:apply-templates select="number"/>
    </xsl:when>
    <xsl:when test="child::string">
      <xsl:apply-templates select="string"/>
    </xsl:when>
  </xsl:choose>
</xsl:template>
  
<xsl:template match="string">
  <xsl:value-of select="."/>
</xsl:template>
  
<xsl:template match="SeqEnum">
  <xsl:text>[</xsl:text>
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position()!=last()">
      <xsl:text>,</xsl:text>
    </xsl:if>
  </xsl:for-each>
  <xsl:text>]</xsl:text>
</xsl:template>
  
<xsl:template match="SeqEmpty">
  <xsl:text> [] </xsl:text>
</xsl:template>
  
<xsl:template match="Set">
  <xsl:apply-templates select="*"/>
</xsl:template>
  
<xsl:template match="SetRange">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>
  
<xsl:template match="SetEnum">
  <xsl:apply-templates select="Expr"/>
</xsl:template>
  
<xsl:template match="SetCompr">
  <xsl:text>{</xsl:text>
  <xsl:for-each select="*">
    <xsl:apply-templates/>
    <xsl:if test="position() != last()">
      <xsl:text>, </xsl:text>
    </xsl:if>
  </xsl:for-each>
  <xsl:text>}</xsl:text>
</xsl:template>
  
<xsl:template match="SetComprPred">
  <xsl:text> {</xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> | </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>} </xsl:text>
</xsl:template>
  
<xsl:template match="INTER">
 <xsl:apply-templates select="child::*[position()=1]"/>
 <xsl:apply-templates select="child::*[position()=2]"/>
 <xsl:apply-templates select="child::*[position()=3]"/>
</xsl:template>
  
<xsl:template match="BoolEvaluation">
 <xsl:text> bool(</xsl:text>
 <xsl:apply-templates select="*"/>
 <xsl:text>) </xsl:text>
</xsl:template>
  
<xsl:template match="TRUE">
  <xsl:text>TRUE</xsl:text>
</xsl:template>
  
<xsl:template match="FALSE">
  <xsl:text>FALSE</xsl:text>
</xsl:template>
  
<xsl:template match="Number">
  <xsl:value-of select="."/>
</xsl:template>
  
<xsl:template match="number">
  <xsl:value-of select="."/>
</xsl:template>
  
    
<!-- Entite clause -->

<xsl:template name="clauses">
  <xsl:for-each select="*">
    <xsl:choose>
      <xsl:when test="self::SEES">
        <xsl:text>
open 
      </xsl:text>
        <xsl:apply-templates select="//SEES"/>
      </xsl:when>
      <xsl:when test="self::DEFINITIONS">
        <xsl:text>
DEFINITIONS
      </xsl:text>
        <xsl:apply-templates select="//DEFINITIONS"/>
      </xsl:when>
      <xsl:when test="self::CONSTRAINTS">
      </xsl:when>
      <xsl:when test="self::AVARIABLES">
        <xsl:text>
-- VARIABLES CLAUSES 
      </xsl:text>
	<xsl:apply-templates select="//AVARIABLES"/>
      </xsl:when>
      <xsl:when test="self::INVARIANT">	  
      </xsl:when>
      <xsl:when test="self::SETS">	  
        <xsl:text>
-- SETS
      </xsl:text>
        <xsl:apply-templates select="//SETS"/>
      </xsl:when>
      <xsl:when test="self::INITIALISATION">
        <xsl:text>
procedure initialisation (this : in out ABSTRACT_TYPE_stack);
      </xsl:text>
      </xsl:when>
      <xsl:when test="self::CONSTANTS">	  
        <xsl:text>
-- CONSTANTS 
      </xsl:text>
        <xsl:apply-templates select="//CONSTANTS"/>
      </xsl:when>
      <xsl:when test="self::CCONSTANTS">
        <xsl:text>
-- CONSTANTS 
      </xsl:text>
        <xsl:apply-templates select="//CCONSTANTS"/>
      </xsl:when>
      <xsl:when test="self::ACONSTANTS">	  
        <xsl:text>
-- ABSTRACT_CONSTANTS 
      </xsl:text>
        <xsl:apply-templates select="//ACONSTANTS"/>
      </xsl:when>
      <xsl:when test="self::HCONSTANTS">
        <xsl:text>
-- HIDDEN_CONSTANTS 
      </xsl:text>
        <xsl:apply-templates select="//HCONSTANTS"/>
      </xsl:when>
      <xsl:when test="self::PROPERTIES">	  
        <xsl:text>
-- PROPERTIES
      </xsl:text>
        <xsl:apply-templates select="//PROPERTIES"/>
      <xsl:text>
-- END PROPERTIES </xsl:text>
      </xsl:when>
      <xsl:when test="self::VARIABLES">
        <xsl:text>
-- VARIABLES 
      </xsl:text>
        <xsl:apply-templates select="//VARIABLES"/>
      </xsl:when>
      <xsl:when test="self::CVARIABLES">
      </xsl:when>
      <xsl:when test="self::HVARIABLES">
        <xsl:text>
-- HIDDEN VARIABLES 
      </xsl:text>
        <xsl:apply-templates select="//HVARIABLES"/>
      <xsl:text>
-- END HIDDEN VARIABLES  </xsl:text>
      </xsl:when>
      <xsl:when test="self::PROMOTES">
        <xsl:text>
-- PROMOTES 
      </xsl:text>
        <xsl:apply-templates select="//PROMOTES"/>
      <xsl:text>
-- END PROMOTES  </xsl:text>
      </xsl:when>
      <xsl:when test="self::EXTENDS">	  
        <xsl:text>
-- EXTENDS 
      </xsl:text>
        <xsl:apply-templates select="//EXTENDS"/>
      <xsl:text>
-- END EXTENDS  </xsl:text>
      </xsl:when>
      <xsl:when test="self::ASSERTIONS">	  
        <xsl:text>
-- ASSERTIONS
      </xsl:text>
        <xsl:apply-templates select="//ASSERTIONS"/>
         <xsl:text>
-- END ASSERTIONS  </xsl:text>
      </xsl:when>
      <xsl:when test="self::OPERATIONS">
        <xsl:apply-templates select="//OPERATIONS"/>
      </xsl:when>
      <xsl:when test="self::USES">
        <xsl:text>
-- USES
      </xsl:text>
        <xsl:apply-templates select="//USES"/>
      </xsl:when>
      <xsl:when test="self::INCLUDES">
        <xsl:text>
-- INCLUDES 
      </xsl:text>
        <xsl:apply-templates select="//INCLUDES"/>
      </xsl:when>
      <xsl:when test="self::REFINES">
        <xsl:text>
-- REFINES 
      </xsl:text>
        <xsl:apply-templates select="//REFINES"/>
      </xsl:when>
      <xsl:when test="self::IMPORTS">	  
        <xsl:text>
-- IMPORTS  
open 
      </xsl:text>
        <xsl:apply-templates select="//IMPORTS"/>
      </xsl:when>
      <xsl:when test="self::VALUES">
        <xsl:text>
-- VALUES  
      </xsl:text>
        <xsl:apply-templates select="//VALUES"/>
      </xsl:when>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>