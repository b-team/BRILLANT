(*==================================================================================================
  Project : Study of the concept of refinement
  Module : subset_gen (Generic notions about subsets)
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/ANSSI & UPMC/LIP6 - St: February 2007, Md: March 2008
  Provided under licence CeCILL-B
  ================================================================================================*)

Require Export List.

(* Subset definitions ----------------------------------------------------------------------------*)

Definition Subset(A:Set):=A->Prop. (* Subset is a unary predicate, possibly not decidable *)
Notation "'⇑' A":=(Subset A) (no associativity, at level 50).

Inductive SEmp(A:Set):⇑A:=SEmp_:forall (a:A), False->(SEmp A) a. (* Constant empty subset *)
Notation "'∅(' A ')'":=(SEmp A).

Inductive SFul(A:Set):⇑A:=SFul_:forall (a:A), (SFul A) a. (* Constant full subset *)
Notation "'⊛(' A ')'":=(SFul A).

Inductive SSin(A:Set)(a:A):⇑A:=SSin_:(SSin A a) a. (* Singleton *)
Notation "'⊙(' a ')'":=(SSin _ a).

Inductive SCmp(A:Set)(S:⇑A):⇑A:=SCmp_:forall (a:A), ~(S a)->(SCmp A S) a. (* Complementary subset *)
Notation "'∁(' S ')'":=(SCmp _ S).

Inductive SInt(A:Set)(S1 S2:⇑A):⇑A:=SInt_:forall (a:A), S1 a->S2 a->(SInt A S1 S2) a. (* inter. *)
Notation "S1 '∩' S2":=(SInt _ S1 S2) (left associativity, at level 25).

Inductive SUni(A:Set)(S1 S2:⇑A):⇑A:= (* Union *)
 | SUni_l:forall (a:A), S1 a->(SUni A S1 S2) a
 | SUni_r:forall (a:A), S2 a->(SUni A S1 S2) a.
Notation "S1 '∪' S2":=(SUni _ S1 S2) (left associativity, at level 25).

Fixpoint SMint(A:Set)(L:list (⇑A)){struct L}:⇑A:=
 match L with nil => ⊛(A) | s::T => s∩(SMint A T) end.
Notation "'⋒(' L ')'":=(SMint _ L).

(* Subset predicates -----------------------------------------------------------------------------*)

Definition SInc(A:Set)(S1 S2:⇑A):Prop:=forall (a:A), S1 a->S2 a. (* Inclusion *)
Notation "S1 '⊆' S2":=(SInc _ S1 S2) (no associativity, at level 50).

Definition SEqu(A:Set)(S1 S2:⇑A):Prop:=(S1⊆S2) /\ (S2⊆S1). (* Equivalence *)
Notation "S1 '≡' S2":=(SEqu _ S1 S2) (no associativity, at level 50).

Definition SDec(A:Set)(S:⇑A):=forall (a:A), {S a}+{~S a}. (* Decidability *)
Notation "'∂(' S ')'":=(SDec _ S).

(* Subset results --------------------------------------------------------------------------------*)

Lemma SSin_spec:forall (A:Set)(a a':A), ⊙(a) a'->a=a'.
Proof.
 intros A a a' Ha; inversion_clear Ha; apply refl_equal.
Qed.

Lemma SEmp_SInc:forall (A:Set)(S:⇑A), ∅(A)⊆S.
Proof.
 intros A S; unfold SInc; intros a HEmp; inversion HEmp as [a' HFalse Ha']; destruct HFalse.
Qed.

Lemma SFul_SInc:forall (A:Set)(S:⇑A), S⊆⊛(A).
Proof.
 intros A S; unfold SInc; intros a _; apply SFul_.
Qed.

Lemma S_SCmp_conj:forall (A:Set)(S:⇑A)(a:A), S a->∁(S) a->False.
Proof.
 intros A S a HS HC; destruct HC as [a HS']; apply HS'; apply HS.
Qed.

Lemma SCmp_SDec:forall (A:Set)(S:⇑A), ∂(S)->∂(∁(S)).
Proof.
 intros A S; unfold SDec; intros HS a; destruct (HS a) as [HS' | HS'].
  right; intros HC; apply (S_SCmp_conj _ _ _ HS' HC).
  left; apply SCmp_; apply HS'.
Qed.

Lemma SCmp_SCmp_SInc:forall (A:Set)(S:⇑A), S⊆∁(∁(S)).
Proof.
 intros A S; unfold SInc; intros a Ha; apply SCmp_; intros Ha'; destruct Ha' as [a Ha']; apply Ha';
  apply Ha.
Qed.

Lemma SCmp_idem:forall (A:Set)(S:⇑A), ∂(S)->S≡∁(∁(S)).
Proof.
 intros A S HS; unfold SEqu; split.
  apply SCmp_SCmp_SInc.
  unfold SInc; intros a Ha.
  destruct Ha as [a Ha]; destruct (HS a) as [HS' | HS']; [ | destruct Ha; apply SCmp_]; apply HS'.
Qed.

Lemma SInt_def_l:forall (A:Set)(S1 S2:⇑A)(a:A), (S1∩S2) a->S1 a.
Proof.
 intros A S1 S2 a HInt;  destruct HInt as [a HS1 HS2]; apply HS1.
Qed.

Lemma SInt_def_r:forall (A:Set)(S1 S2:⇑A)(a:A), (S1∩S2) a->S2 a.
 intros A S1 S2 a HInt;  destruct HInt as [a HS1 HS2]; apply HS2.
Qed.

Lemma SInt_SInc_l:forall (A:Set)(S1 S2:⇑A), S1∩S2⊆S1.
Proof.
 intros A S1 S2; unfold SInc; apply SInt_def_l.
Qed.

Lemma SInt_SInc_r:forall (A:Set)(S1 S2:⇑A), S1∩S2⊆S2.
Proof.
 intros A S1 S2; unfold SInc; apply SInt_def_r.
Qed.

Lemma SUni_SInc_l:forall (A:Set)(S1 S2:⇑A), S1⊆S1∪S2.
Proof.
 intros A S1 S2; unfold SInc; intros a Ha; apply SUni_l; apply Ha.
Qed.

Lemma SUni_SInc_r:forall (A:Set)(S1 S2:⇑A), S2⊆(S1∪S2).
Proof.
 intros A S1 S2; unfold SInc; intros a Ha; apply SUni_r; apply Ha.
Qed.

Lemma SCmp_SInt:forall (A:Set)(S:⇑A), S∩∁(S)≡∅(A).
Proof.
 intros A S; unfold SEqu; unfold SInc; split; intros a Ha.
  apply SEmp_; apply (S_SCmp_conj _ _ _ (SInt_def_l _ _ _ _ Ha) (SInt_def_r _ _ _ _ Ha)).
  destruct Ha as [a HF]; destruct HF.
Qed.

Lemma SCmp_SUni:forall (A:Set)(S:⇑A), ∂(S)->S∪∁(S)≡⊛(A).
Proof.
 intros A S; unfold SEqu; unfold SInc; split; intros a Ha.
  apply SFul_.
  clear Ha; destruct (H a) as [Ha | Ha]; [apply SUni_l | apply SUni_r; apply SCmp_]; apply Ha.
Qed.

Lemma SInc_refl:forall (A:Set)(S:⇑A), S⊆S.
Proof.
 intros A S; unfold SInc; intros a Ha; apply Ha.
Qed.

Lemma SInc_trans:forall (A:Set)(S1 S2 S3:⇑A), S1⊆S2->S2⊆S3->S1⊆S3.
Proof.
 intros A S1 S2 S3; unfold SInc; intros H12 H23 a Ha; apply H23; apply H12; apply Ha.
Qed.

Lemma SEqu_refl:forall (A:Set)(S:⇑A), S≡S.
Proof.
 intros A S; unfold SEqu; split; apply SInc_refl.
Qed.

Lemma SEqu_sym:forall (A:Set)(S1 S2:⇑A), S1≡S2->S2≡S1.
Proof.
 intros A S1 S2; unfold SEqu; intros H12; destruct H12 as [H12 H21]; split; [apply H21 | apply H12].
Qed.

Lemma SEqu_trans:forall (A:Set)(S1 S2 S3:⇑A), S1≡S2->S2≡S3->S1≡S3.
Proof.
 intros A S1 S2 S3; unfold SEqu; intros H12 H23; destruct H12 as [H12 H21];
  destruct H23 as [H23 H32]; split;
  [apply (SInc_trans _ _ _ _ H12 H23) | apply (SInc_trans _ _ _ _ H32 H21)].
Qed.

Lemma SInc_SEqu_trans:forall (A:Set)(S1 S2 S3:⇑A), S1⊆S2->S2≡S3->S1⊆S3.
Proof.
 intros A S1 S2 S3 H12 [H23 _]; apply (SInc_trans _ _ _ _ H12 H23).
Qed.

Lemma SEqu_SInc_trans:forall (A:Set)(S1 S2 S3:⇑A), S1≡S2->S2⊆S3->S1⊆S3.
Proof.
 intros A S1 S2 S3 [H12 _] H23; apply (SInc_trans _ _ _ _ H12 H23).
Qed.

Lemma SInt_comm:forall (A:Set)(S1 S2:⇑A), S1∩S2≡S2∩S1.
Proof.
 intros A S1 S2; unfold SEqu; unfold SInc; split; intros a Ha; inversion_clear Ha; apply SInt_;
  [ apply H0 | apply H | apply H0 | apply H].
Qed.

Lemma SUni_comm:forall (A:Set)(S1 S2:⇑A), S1∪S2≡S2∪S1.
Proof.
 intros A S1 S2; unfold SEqu; unfold SInc; split; intros a Ha; inversion_clear Ha;
  [apply SUni_r | apply SUni_l | apply SUni_r | apply SUni_l]; apply H.
Qed.

Lemma SMint_SInc:forall (A:Set)(S:⇑A)(L:list (⇑A)), In S L->⋒(L)⊆S.
Proof.
 intros A S; induction L as [ | S' L]; simpl; intros HS.
  destruct HS.
  destruct HS as [HS | HS].
   rewrite HS in *; clear S' HS; apply SInt_SInc_l.
   apply (SInc_trans _ _ _ _ (SInt_SInc_r A S' (⋒(L))) (IHL HS)).
Qed.

Lemma SMint_in:forall (A:Set)(a:A)(L:list (⇑A)), ⋒(L) a->forall (S:⇑A), In S L->S a.
Proof.
 intros A a; induction L as [ | S L]; simpl; intros Ha S' HS.
  destruct HS.
  inversion_clear Ha; destruct HS as [HS | HS].
   rewrite <- HS; clear S' HS; apply H.
   apply (IHL H0 S' HS).
Qed.

(* Congruence rules ------------------------------------------------------------------------------*)

Definition SEqu_congr_prop(A:Set)(P:⇑A->Type):=forall (S1 S2:⇑A), S1≡S2->P S1->P S2.
Implicit Arguments SEqu_congr_prop [A].

Definition SEqu_congr_fun(A B:Set)(f:⇑A->⇑B):=forall (S1 S2:⇑A), S1≡S2->f(S1)≡f(S2).
Implicit Arguments SEqu_congr_fun [A B].

Theorem SEqu_SCmp:forall (A:Set), SEqu_congr_fun (SCmp A).
Proof.
 intros A; unfold SEqu_congr_fun; intros S1 S2; unfold SEqu; unfold SInc; intros [H12 H21]; split;
  intros a Ha; inversion_clear Ha; apply SCmp_; intros Ha; apply H; [apply H21 | apply H12];
  apply Ha.
Qed.

Theorem SEqu_SInt:forall (A:Set)(S:⇑A), SEqu_congr_fun (SInt A S).
Proof.
 intros A S; unfold SEqu_congr_fun; intros S1 S2; unfold SEqu; unfold SInc; intros [H12 H21]; split;
  intros a Ha; inversion_clear Ha; apply SInt_;
   [apply H | apply H12; apply H0 | apply H | apply H21; apply H0].
Qed.

Theorem SEqu_SUni:forall (A:Set)(S:⇑A), SEqu_congr_fun (SUni A S).
Proof.
 intros A S; unfold SEqu_congr_fun; intros S1 S2; unfold SEqu; unfold SInc; intros [H12 H21]; split;
  intros a Ha; inversion_clear Ha;
  [apply SUni_l | apply SUni_r; apply H12 | apply SUni_l | apply SUni_r; apply H21]; apply H.
Qed.

Theorem SEqu_SInc_l:forall (A:Set)(S:⇑A), SEqu_congr_prop (SInc A S).
Proof.
 intros A S; unfold SEqu_congr_fun; intros S1 S2; unfold SEqu; intros [H12 H21] HInc; unfold SInc;
  intros a Ha; apply H12; apply HInc; apply Ha.
Qed.

Theorem SEqu_SInc_r:forall (A:Set)(S:⇑A), SEqu_congr_prop (fun S':⇑A=>SInc A S' S).
Proof.
 intros A S; unfold SEqu_congr_fun; intros S1 S2; unfold SEqu; intros [H12 H21] HInc; unfold SInc;
  intros a Ha; apply HInc; apply H21; apply Ha.
Qed.

Theorem SEqu_SDec:forall (A:Set), SEqu_congr_prop (SDec A).
Proof.
 intros A; unfold SEqu_congr_fun; intros S1 S2; unfold SEqu; unfold SDec; intros [H12 H21] H1;
  unfold SInc; intros a; destruct (H1 a) as [Ha | Ha].
  left; apply H12; apply Ha.
  right; intros Ha'; apply Ha; apply H21; apply Ha'.
Qed.

(*================================================================================================*)