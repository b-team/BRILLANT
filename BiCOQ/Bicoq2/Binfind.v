(*☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   Project: BiCoq, formalisation of the B theory into the Coq system
   Module: Binfind, induction principle for B proofs
   The definition of the B proofs as a inductive type allolws the definition of
   induction principles.
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : July 2005 - October 2006
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Require Export Pinfer.

(*Proof depth function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Fixpoint fidepth(G:☐)(P:☊)(H:G⊣P){struct H}:☈:=
  match H with
  | biin _ _ _ => ☦
  | biinc _ G2 P' H' _ => ☠(fidepth G2 P' H')
  | biandi G' PL PR HL HR => ☠((fidepth G' PL HL)☰☉(fidepth G' PR HR))
  | biandl G' PL PR H' => ☠(fidepth G' (PL∧PR) H')
  | biandr G' PL PR H' => ☠(fidepth G' (PL∧PR) H')
  | biimpi G' PL PR H' => ☠(fidepth (G'☑★PL) PR H')
  | biimpe G' Q' P' H' => ☠(fidepth G' (Q'☲P') H')
  | biabsn G' Q' P' H1 H2 => ☠((fidepth (G'☑★¬P') Q' H1)☰☉(fidepth (G'☑★¬P') (¬Q') H2))
  | biabsp G' Q' P' H1 H2 => ☠((fidepth (G'☑★P') Q' H1)☰☉(fidepth (G'☑★P') (¬Q') H2))
  | bifori G' _ P' _ H' => ☠(fidepth G' P' H')
  | bifore G' f' P' _ H' => ☠(fidepth G' ☬☢(f'∙P') H')
  | biequi _ _ => ☦
  | bieque G' f' P' E' F' H1 H2 => ☠((fidepth G' (E'♃F') H1)☰☉(fidepth G' 〈f'♇☍E'@☋P'〉 H2))
  | bipro G' f1 f2 E' S' T' _ _ _ => ☦
  | bipow G' f' S' T' _ _ => ☦
  | bicmp G' f' P' E' S' => ☦
  | bieqsi G' S' T' H1 H2 => ☠((fidepth G' (S'∈(☬T')) H1)☰☉(fidepth G' (T'∈(☬S')) H2))
  | bichsi G' f' S' _ H' => ☠(fidepth G' ☬☣(f'∙χ(f')∈S') H')
  | bibigi _ _ => ☦
  | bibied _ _ _ _ => ☦
  | bicpll G' E1 F1 E2 F2 H' => ☠(fidepth G' ((E1☵F1)♃(E2☵F2)) H')
  | bicplr G' E1 F1 E2 F2 H' => ☠(fidepth G' ((E1☵F1)♃(E2☵F2)) H')
  end.
 Notation "'?☓∣' I '∣'":=(@fidepth _ _ I).

(* Proof depth induction principle ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem Bdi_ind:forall (PH:forall (G:☐)(P:☊), G⊣P->Set),
                               (forall (G:☐)(P:☊)(H:G⊣P),
                                (forall  (G':☐)(P':☊)(H':G'⊣P'), ?☓∣H'∣<☉?☓∣H∣->PH G' P' H')->PH G P H)->
                               forall (G:☐)(P:☊)(H:G⊣P), PH G P H.
 Proof.
  intros PH Hind G P H; assert (Ha:forall (i:☈)(G':☐)(P':☊)(H':G'⊣P'), ?☓∣H'∣<☉i->PH G' P' H').
   induction i; intros G' P' H' Ha.
    inversion Ha.
    apply Hind; intros G'' P'' H'' Hb; apply IHi; inversion_clear Ha;
     apply (transigtige _ _ _ Hb H0).
   apply Hind; intros G' P' H' Hb; apply (Ha ?☓∣H∣); apply Hb.
 Qed.
 (* Nota: this is not a simple application of WFIND1, as WFIND1 is only applicable to a   *)
 (* proposal P:S->Prop where S is a Set... Direct application of WFIND1 provides a less   *)
 (* interesting result, allowing induction only on different proofs of a unique G⊣P.     *)

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)