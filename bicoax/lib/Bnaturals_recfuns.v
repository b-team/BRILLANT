(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter2.
Require Import Powerset_facts.
Require Import Bfixpoints.
Require Import Bnaturals_basics.

Open Local Scope nat_scope.

(**
  * Recursive functions: definitions
  *)

Definition Bpred_nat (c: nat * nat) := match c with | (x,y) => x = (S y) end.

Section N_recursive_functions.

Variable S: Type.
Variable s: Ensemble S.
Variable a: S.
Variable g: Ensemble (S*S).
Variable a_in_s: In s a.
Variable g_total: In (total_function s s) g.


(**
  genf, BBook: section 3.5.6, p.159, array 1
  *)
Definition bbN_genf :=
  lambda (Power_set (times bbN s))
  (fun h => (Union (Singleton (bbN_0, a)) (composition bbN_pred (composition h g)))).

(**
  The same definition using nat instead of bbN. This is more
  complicated because "pred" is not a function in the sense of the
  BBook (pred(0) = 0, while in the BBook pred(0) is not defined).
  *)
Definition nat_genf :=
  lambda (Power_set (times (Full_set nat) s))
  (fun h => (Union (Singleton (0, a)) (composition Bpred_nat (composition h g)))).

Definition subgenf_nat_of_bbN (f: Ensemble (Ensemble BIG_type * S)) :=
  fun cpl => match cpl with
  | (n, sub_n) => In f (bbN_of_nat n, sub_n)
  end.

Definition subgenf_bbN_of_nat (f: Ensemble (nat * S)) :=
  fun cpl => match cpl with
  | (n, sub_n) => In bbN n /\ In f (nat_of_bbN n, sub_n)
  end.

Theorem bbN_genf_is_total:
  In (total_function (Power_set (times bbN s)) (Power_set (times bbN s)))
  bbN_genf.
Proof.
constructor.
constructor.
constructor.
constructor.

intros x H.
induction H.
constructor.
assumption.
rewrite H0; constructor.
intros z H1.
induction H1.
induction H1.
constructor.
apply bbN_0_in_bbN.
exact a_in_s.
induction H1.
induction H1.
induction H1.
constructor.
inversion H1.
inversion H5.
inversion H8.
replace a0 with (app bbN_succ x0 (bbN_succ_y x0 H6)).
apply bbN_succ_in_bbN.
rewrite H13; unfold bbN_succ; apply abstract_lambda; reflexivity.
inversion H2.
induction H4.
induction H4.
assert (In (total_function s s) g).
assumption.
repeat (induction H7).
generalize (H7 _ H6); intros.
inversion H10; assumption.

intros.
inversion H; inversion H0.
rewrite H4; rewrite H8; reflexivity.

apply Extensionality_Ensembles; split; intros x H.
induction H.
induction H.
inversion H.
assumption.
constructor; exists (Union (Singleton (bbN_0, a)) (composition bbN_pred (composition x g))).
constructor; [ assumption | reflexivity ].
Qed.

Theorem bbN_genf_y: forall (x: Ensemble ((Ensemble BIG_type) * S)) (H: In (Power_set (times bbN s)) x),
    In (partial_function (Power_set (times bbN s)) (Power_set (times bbN s))) bbN_genf
/\  In (domain bbN_genf) x.
Proof.
intros.
split.
generalize bbN_genf_is_total; intros.
induction H0.
assumption.
constructor; exists (Union (Singleton (bbN_0, a)) (composition bbN_pred (composition x g))).
constructor; [ assumption | reflexivity ].
Qed.

Theorem bbN_genf_is_monotonic: forall (x y: Ensemble ((Ensemble BIG_type) * S))
  (x_ok: In (Power_set (times bbN s)) x) (y_ok: In (Power_set (times bbN s)) y),
  Included x y ->
  Included
    (app bbN_genf x (bbN_genf_y x x_ok))
    (app bbN_genf y (bbN_genf_y y y_ok)).
Proof.
intros.
unfold bbN_genf.
do 2 (apply abstract_lambda).
intros z H0.
induction H0.
left; assumption.
right.
induction H0; induction H0.
constructor; exists x0.
induction H0.
split; [ assumption | ].
induction H1; induction H1.
constructor; exists x1.
induction H1.
split; [ | assumption ].
apply H; assumption.
Qed.

Theorem nat_genf_is_total:
  In (total_function (Power_set (times (Full_set nat) s)) (Power_set (times (Full_set nat) s)))
  nat_genf.
Proof.
constructor.
constructor.
constructor.
constructor.

intros x H.
induction H.
constructor.
assumption.
rewrite H0; constructor.
intros z H1.
induction H1.
induction H1.
constructor.
constructor.
exact a_in_s.
induction H1.
induction H1.
induction H1.
constructor.
constructor.
inversion H2.
induction H4.
induction H4.
assert (In (total_function s s) g); [ assumption | ].
repeat (induction H7).
generalize (H7 _ H6); intros.
inversion H10; assumption.

intros.
inversion H; inversion H0.
rewrite H4; rewrite H8; reflexivity.

apply Extensionality_Ensembles; split; intros x H.
induction H.
induction H.
inversion H.
assumption.
constructor; exists (Union (Singleton (0, a)) (composition Bpred_nat (composition x g))).
constructor; [ assumption | reflexivity ].
Qed.

Theorem nat_genf_applicable: forall (x: Ensemble (nat * S)) (H: In (Power_set (times (Full_set nat) s)) x),
    In (partial_function (Power_set (times (Full_set nat) s)) (Power_set (times (Full_set nat) s))) nat_genf
/\  In (domain nat_genf) x.
Proof.
intros.
split.
generalize nat_genf_is_total; intros.
induction H0.
assumption.
constructor; exists (Union (Singleton (0, a)) (composition Bpred_nat (composition x g))).
constructor; [ assumption | reflexivity ].
Qed.

Theorem nat_genf_is_monotonic: forall (x y: Ensemble (nat * S))
  (x_ok: In (Power_set (times (Full_set nat) s)) x) (y_ok: In (Power_set (times (Full_set nat) s)) y),
  Included x y ->
  Included
    (app nat_genf x (nat_genf_applicable x x_ok))
    (app nat_genf y (nat_genf_applicable y y_ok)).
Proof.
intros.
unfold nat_genf.
do 2 (apply abstract_lambda).
intros z H0.
induction H0.
left; assumption.
right.
induction H0; induction H0.
constructor; exists x0.
induction H0.
split; [ assumption | ].
induction H1; induction H1.
constructor; exists x1.
induction H1.
split; [ | assumption ].
apply H; assumption.
Qed.

Theorem In_nat_s_noB: forall (h: Ensemble ((Ensemble BIG_type)*S)), In (Power_set (times bbN s)) h ->
  In (Power_set (times (Full_set nat) s)) (subgenf_nat_of_bbN h).
Proof.
intros h H.
constructor; red; intros.
induction x.
split.
constructor.
do 2 (red in H0).
induction H.
generalize (H _ H0); intros.
inversion H1.
assumption.
Qed.


(**
  * Recursive functions: validity
  *)

Theorem valid_bbN_genf:forall (h: Ensemble ((Ensemble BIG_type)*S)) (H: In (Power_set (times bbN s)) h),
  app bbN_genf h (bbN_genf_y h H) =
  subgenf_bbN_of_nat (app nat_genf (subgenf_nat_of_bbN h) (nat_genf_applicable (subgenf_nat_of_bbN h) (In_nat_s_noB h H))).
Proof.
intros h H.
unfold bbN_genf; apply abstract_lambda.
unfold nat_genf; apply abstract_lambda.
apply Extensionality_Ensembles; split; red; intros.

induction H0.
induction H0.
do 2 red.
split; [ apply bbN_0_in_bbN | ].
left.
rewrite <- nat_zero_bbN_0; intuition.
induction H0.
inversion H0.
do 2 red.
induction H1.
inversion H1.
inversion H5.
inversion H8.
split.
replace a0 with (app bbN_succ x (bbN_succ_y x H6)).
apply bbN_succ_in_bbN.
unfold bbN_succ; apply abstract_lambda; rewrite H13; reflexivity.
right.
constructor; exists (nat_of_bbN x).
split.
rewrite H13.
replace x with (bbN_of_nat (nat_of_bbN x)).
rewrite <- nat_S_bbN_succ_nondependent.
rewrite nat_2coercions_identity.
do 2 red; reflexivity.
rewrite bbN_of_nat_of_bbN; [ reflexivity | assumption ].
inversion H2.
induction H15.
constructor; exists x2.
induction H15.
split.
do 2 red.
rewrite bbN_of_nat_of_bbN; [ assumption | assumption ].
assumption.

induction x.
do 2 (red in H0).
induction H0.
inversion H1.
left.
inversion H2.
assert (a0 = bbN_0).
generalize (valid_nat_eq_l  _ _ H5); intros.
rewrite bbN_of_nat_of_bbN in H4.
rewrite <- bbN_0_nat_zero in H4.
symmetry; assumption.
assumption.
rewrite H4; intuition.
right.
inversion H2.
inversion H5.
induction H7.
constructor; exists (bbN_of_nat x0).
split.
inversion H7.
replace a0 with (bbN_of_nat (Datatypes.S x0)).
constructor.
constructor.
assert (In bbN (bbN_of_nat x0)).
apply bbN_of_nat_in_bbN.
replace (bbN_of_nat (Datatypes.S x0))
  with (app bbN_succ (bbN_of_nat x0) (bbN_succ_y (bbN_of_nat x0) H9)).
apply app_trivial_property.
simpl.
unfold bbN_succ; apply abstract_lambda.
rewrite Union_commutative; reflexivity.
apply bbN_of_nat_in_bbN.
symmetry.
assert (In bbN (bbN_of_nat (Datatypes.S x0))).
apply bbN_of_nat_in_bbN.
apply valid_bbN_eq_r.
assumption.
assumption.
rewrite nat_2coercions_identity; assumption.
inversion H8.
induction H10.
induction H10.
constructor; exists x1.
do 2 (red in H10).
split; assumption.
Qed.

Theorem bbN_s_subgenf_bbNofnat: forall (h: Ensemble (nat*S)), In (Power_set (times (Full_set nat) s)) h ->
  In (Power_set (times bbN s)) (subgenf_bbN_of_nat h).
Proof.
intros h H.
constructor; red; intros.
induction x.
do 2 (red in H0).
induction H0.
split.
assumption.
induction H.
generalize (H _ H1); intros.
inversion H2.
assumption.
Qed.

Theorem valid_nat_genf:forall (h: Ensemble (nat*S)) (H: In (Power_set (times (Full_set nat) s)) h),
  app nat_genf h (nat_genf_applicable h H) =
  subgenf_nat_of_bbN (app bbN_genf (subgenf_bbN_of_nat h) (bbN_genf_y (subgenf_bbN_of_nat h) (bbN_s_subgenf_bbNofnat h H))).
Proof.
intros h H.
unfold nat_genf; apply abstract_lambda.
unfold bbN_genf; apply abstract_lambda.
apply Extensionality_Ensembles; split; red; intros.

induction H0.
induction H0.
do 2 red.
left.
rewrite bbN_0_nat_zero; intuition.
induction H0.
induction H0.
do 2 red.
right.
induction H0.
constructor; exists (bbN_of_nat x).
split.
inversion H0.
constructor.
constructor.
assert (In bbN (bbN_of_nat x)).
apply bbN_of_nat_in_bbN.
replace (bbN_of_nat (Datatypes.S x))
  with (app bbN_succ (bbN_of_nat x) (bbN_succ_y (bbN_of_nat x) H3)).
apply app_trivial_property.
simpl.
unfold bbN_succ; apply abstract_lambda.
rewrite Union_commutative; reflexivity.
apply bbN_of_nat_in_bbN.
inversion H1.
induction H3.
induction H3.
constructor; exists x0.
split.
do 2 red.
split.
apply bbN_of_nat_in_bbN.
rewrite nat_2coercions_identity; assumption.
assumption.

induction x.
inversion H0.
left.
inversion H1.
replace a0 with 0; [ intuition | ].
apply valid_nat_eq_r.
rewrite  nat_zero_bbN_0.
rewrite bbN_of_nat_of_bbN.
assumption.
apply bbN_0_in_bbN.
inversion H1.
induction H4.
induction H4.
right.
constructor; exists (nat_of_bbN x0).
split.
inversion H4.
inversion H9.
do 2 red.
inversion H12.
apply valid_nat_eq_r.
simpl.
rewrite bbN_of_nat_of_bbN.
rewrite Union_commutative; assumption.
assumption.
inversion H6.
induction H8.
induction H8.
constructor; exists x1.
split.
inversion H8.
assumption.
assumption.
Qed.

(**
  Definition of a recursive function over natural numbers, BBook: section 3.5.6, p.159
  *)
Definition nat_f := Bfix (times (Full_set nat) s) nat_genf.
Definition bbN_f := Bfix (times bbN s) bbN_genf.

(**
  property 3.5.12, BBook: section 3.5.6, p.159
  *)
Theorem nat_f_lfp: nat_f = Union (Singleton (0,a)) (composition Bpred_nat (composition nat_f g)).
Proof.
unfold nat_f at 1.
generalize (fix_Knaster_Tarski _ (times (Full_set nat) s) nat_genf nat_genf_is_total).
intros.
rewrite <- H.
unfold nat_genf; apply abstract_lambda.
reflexivity.
intros.
induction H0.
inversion H0.
exists (nat_genf_applicable x H4).
exists (nat_genf_applicable y H5).
apply nat_genf_is_monotonic; assumption.
Qed.

(**
  property 3.5.12, BBook: section 3.5.6, p.159
  *)
Theorem bbN_f_lfp: bbN_f = Union (Singleton (bbN_0,a)) (composition bbN_pred (composition bbN_f g)).
Proof.
unfold bbN_f at 1.
generalize (fix_Knaster_Tarski _ (times bbN s) bbN_genf bbN_genf_is_total).
intros.
rewrite <- H.
unfold bbN_genf; apply abstract_lambda.
reflexivity.
intros.
induction H0.
inversion H0.
exists (bbN_genf_y x H4).
exists (bbN_genf_y y H5).
apply bbN_genf_is_monotonic; assumption.
Qed.

Theorem nat_f_is_functional: forall (x: nat), forall (y: S), In nat_f (x,y) -> forall (z: S), In nat_f (x,z) -> y = z.
Proof.
intros x.
induction x.
intros.
assert (forall (s: S), In nat_f (0,s) -> s = a).
intros.
rewrite nat_f_lfp in H1.
inversion H1.
inversion H2.
reflexivity.
inversion H2.
induction H5; induction H5.
inversion H5.
generalize (H1 y H); generalize (H1 z H0); intros.
rewrite H2; rewrite H3; reflexivity.

intros.
rewrite nat_f_lfp in H, H0.
inversion H.
inversion H1.
inversion H0.
inversion H3.
inversion H1; inversion H3.
induction H6; induction H9.
induction H6; induction H9.
assert (x2 = x); [ inversion H6; reflexivity | ].
assert (x3 = x); [ inversion H9; reflexivity | ].
rewrite H13 in H11; rewrite H14 in H12.
inversion H11; inversion H12.
induction H16; induction H19.
induction H16; induction H19.
generalize (IHx _ H16 _ H19); intros.
rewrite H23 in H21.
assert (In (total_function s s) g); [ assumption  | ].
induction H24.
induction H24.
apply H26 with x5; assumption.
Qed.

(**
  Theorem 3.5.3, BBook: section 3.5.6, p.160

  Note: this is another version of the expression that (nat_)f is total. The true
  theorem 3.5.3 relies on a property that is false, hence we do the full proof of
  totality.
  *)
Theorem nat_f_is_total: In (total_function (Full_set nat) s) nat_f.
Proof.
constructor.
constructor.

constructor.
constructor.
red; intros.
induction H.
induction H.
apply H.
do 2 red.
assert (In (Power_set (times (Full_set nat) s)) (times (Full_set nat) s)).
constructor; intuition.
exists (nat_genf_applicable (times (Full_set nat) s) H0).
unfold nat_genf; apply abstract_lambda.
red; intros.
induction x0.
induction H1.
induction H1.
constructor; [ constructor | exact a_in_s ].
inversion H1.
constructor.
constructor.
induction H2.
induction H2.
inversion H4.
induction H6.
induction H6.
assert (In (total_function s s) g); [ assumption | ].
repeat (induction H9).
generalize (H9 _ H8); intros.
inversion H12; assumption.

apply nat_f_is_functional.

apply Extensionality_Ensembles; split; red; intros.
constructor.
induction x.
constructor; exists a.
do 2 constructor.
intros.
inversion H0.
apply H1.
unfold nat_genf; apply abstract_lambda.
left; intuition.
assert (In (Full_set nat) x); [ constructor | ].
generalize (IHx H0); intros.
inversion H1.
induction H2.
rewrite nat_f_lfp in H2.
inversion H2.
assert (In (partial_function s s) g /\ In (domain g) a).
assert (In (total_function s s) g); [ assumption | ].
induction H6.
split; [ assumption | rewrite H7; apply a_in_s ].
constructor; exists (app g a H6).
do 2 constructor.
intros.
induction H7.
apply H7.
unfold nat_genf; apply abstract_lambda.
right.
constructor.
exists x.
split.
constructor.
constructor.
exists a.
split.
apply H7.
unfold nat_genf; apply abstract_lambda.
left.
inversion H4; intuition.
apply app_trivial_property.
assert (In (partial_function s s) g /\ In (domain g) x0).
assert (In (total_function s s) g); [ assumption | ].
inversion H6.
split; [ assumption | ].
inversion H4.
induction H11; induction H11.
inversion H13.
induction H15; induction H15.
rewrite H8.
repeat (induction H7).
generalize (H7 _ H17); intros.
inversion H19; assumption.
constructor; exists (app g x0 H6).
do 2 constructor.
intros.
induction H7.
apply H7.
unfold nat_genf; apply abstract_lambda.
right.
constructor.
exists x.
split.
constructor.
constructor.
exists x0.
split.
apply H7.
unfold nat_genf; apply abstract_lambda.
right.
inversion H4.
induction H9; induction H9.
constructor; exists x3.
split; [ assumption | ].
inversion H11.
induction H13; induction H13.
constructor; exists x4.
split.
induction H13.
induction H13.
apply H13.
exists x2.
assumption.
assumption.
apply app_trivial_property.
Qed.

(**
  Theorem 3.5.3, BBook: section 3.5.6, p.160

  Note: this is another version of the expression that f is total. The true
  theorem 3.5.3 relies on a property that is false, hence we do the full proof of
  totality.
  *)
Theorem bbN_f_is_total_BBook: forall (n: Ensemble BIG_type), In bbN n ->
  exists y, In s y /\ In bbN_f (n,y) /\ forall (z: S), (In s z /\ In bbN_f (n,z) -> z = y).
Proof.
intros n H.
pattern n.
apply BBook_principle_of_mathematical_induction; [ | | assumption ].

assert (forall (s: S), In bbN_f (bbN_0,s) <-> s = a).
intros.
split; intros.
rewrite bbN_f_lfp in H0.
inversion H0.
inversion H1.
reflexivity.
inversion H1.
induction H4; induction H4.
inversion H4.
inversion H9.
inversion H12.
absurd (app bbN_succ x0 (bbN_succ_y x0 H10) = bbN_0).
apply bbN_succ_is_not_bbN_0.
unfold bbN_succ; apply abstract_lambda.
rewrite H17; reflexivity.
rewrite H0.
do 2 constructor.
intros.
induction H1.
apply H1.
unfold bbN_genf; apply abstract_lambda.
left; intuition.
(* End of "small" assertion *)

exists a.
split.
apply a_in_s.
split.
do 2 constructor.
intros.
induction H1.
apply H1.
unfold bbN_genf; apply abstract_lambda.
left; intuition.
intros z H1.
induction H1.
apply (proj1 (H0 z)); assumption.

(* Induction step *)
intros n0 H0 H1.
induction H1.
decompose [and] H1.
assert (In (partial_function s s) g /\ In (domain g) x).
assert (In (total_function s s) g); [ assumption | ].
induction H3.
split; [ assumption | rewrite H6; assumption ].
exists (app g x H3).
split.
apply application_in_codomain.
split.
rewrite bbN_f_lfp.
right.
constructor; exists n0.
split.
constructor.
constructor; apply app_trivial_property.
assumption.
constructor; exists x.
split; [ assumption | apply app_trivial_property ].
intros z H6.
induction H6.
rewrite bbN_f_lfp in H7.
inversion H7.
inversion H8.
absurd (app bbN_succ n0 (bbN_succ_y n0 H0) = bbN_0).
apply bbN_succ_is_not_bbN_0.
rewrite H11; reflexivity.
inversion H8.
induction H11; induction H11.
assert (x1 = n0).
inversion H11.
inversion H16.
assert (In bbN_succ (x1, app bbN_succ x1 (bbN_succ_y x1 H17))).
apply app_trivial_property.
apply bbN_succ_injective with H17 H0.
inversion H19.
inversion H21.
rewrite H25; rewrite H29; reflexivity.
rewrite H14 in H13.
inversion H13.
induction H16; induction H16.
assert (In s x2).
rewrite bbN_f_lfp in H16.
inversion H16.
inversion H19.
rewrite <- H23; apply a_in_s.
inversion H19.
induction H22; induction H22.
inversion H24.
induction H26; induction H26.
inversion H3.
repeat (induction H29).
generalize (H29 _ H28); intros.
inversion H32; assumption.
assert (In s x2 /\ In bbN_f (n0,x2)).
split; assumption.
generalize (H5 _ H20); intros.
apply identifying_app.
rewrite <- H21.
assumption.
Qed.

(**
  Theorem 3.5.3, BBook: section 3.5.6, p.160

  The streamlined (and more readable) version of the theorem, or at
  least what it is supposed to be used for.
  *)
Theorem bbN_f_is_total: In (total_function bbN s) bbN_f.
Proof.
constructor.
constructor.

constructor.
constructor; red; intros.
induction H.
induction H.
apply H.
do 2 red.
assert (In (Power_set (times bbN s)) (times bbN s)).
constructor; intuition.
exists (bbN_genf_y (times bbN s) H0).
unfold bbN_genf; apply abstract_lambda.
red; intros.
induction x0.
induction H1.
induction H1.
constructor; [ apply bbN_0_in_bbN | exact a_in_s ].
inversion H1.
induction H2; induction H2.
split.
inversion H2.
inversion H7.
inversion H10.
replace a1 with (app bbN_succ x1 (bbN_succ_y x1 H8)).
apply bbN_succ_in_bbN.
unfold bbN_succ; apply abstract_lambda; rewrite H15; reflexivity.
inversion H4.
induction H6; induction H6.
assert (In (total_function s s) g); [ assumption | ].
repeat (induction H9).
generalize (H9 _ H8); intros.
inversion H12; assumption.

intros x.
intros y H.
assert (In bbN x).
rewrite bbN_f_lfp in H.
inversion H.
inversion H0.
apply bbN_0_in_bbN.
inversion H0.
induction H3; induction H3.
inversion H3.
inversion H8.
inversion H11.
replace x with (app bbN_succ x1 (bbN_succ_y x1 H9)).
apply bbN_succ_in_bbN.
unfold bbN_succ; apply abstract_lambda; rewrite H16; reflexivity.
generalize (bbN_f_is_total_BBook x H0); intros.
induction H1.
decompose [and] H1.
assert (In s y /\ In bbN_f (x,y)).
split; [ | assumption ].
rewrite bbN_f_lfp in H.
inversion H.
inversion H4.
rewrite <- H10; apply a_in_s.
inversion H4.
induction H9; induction H9.
inversion H11.
induction H13; induction H13.
assert (In (total_function s s) g); [ assumption | ].
repeat (induction H16).
generalize (H16 _ H15); intros.
inversion H19; assumption.
generalize (H6 _ H4); intros.
rewrite H7; symmetry.
apply H6.
split; [ | assumption ].
rewrite bbN_f_lfp in H2.
inversion H2.
inversion H8.
rewrite <- H12; apply a_in_s.
inversion H8.
induction H11; induction H11.
inversion H13.
induction H15; induction H15.
assert (In (total_function s s) g); [ assumption | ].
repeat (induction H18).
generalize (H18 _ H17); intros.
inversion H21; assumption.

apply Extensionality_Ensembles; split; red; intros.
induction H.
induction H.
rewrite bbN_f_lfp in H.
inversion H.
inversion H0.
apply bbN_0_in_bbN.
inversion H0.
induction H3; induction H3.
inversion H3.
inversion H8.
inversion H11.
replace a0 with (app bbN_succ x1 (bbN_succ_y x1 H9)).
apply bbN_succ_in_bbN.
unfold bbN_succ; apply abstract_lambda; rewrite H16; reflexivity.
generalize (bbN_f_is_total_BBook x H); intros.
induction H0.
decompose [and] H0.
constructor; exists x0; assumption.
Qed.

End N_recursive_functions.

