(* $Id$ *)

(* 
 Traitement des expressions d�pourvues de sens
 Main reference : Lilian Burdy PhD Thesis (mai 2000)
*)

open Project
open Po_adt
open Beds_lib
open Gop_lib

(*open Blast_lib2*)
    
exception NotDefined of string 

let rec deltaE expr = 
  match 
    expr 
  with     
(*GM    | Blast.ExprPred p -> (Blast.ExprPred(deltaP p)) *)
    | Blast.ExprParen e -> deltaE e
    | Blast.ExprBin (op2 , expr1 , expr2) -> (* Binary Expressions *)
	(
	  match op2 with
	    (* *)
	    | Bbop.Arity
	    | Bbop.SubTree
	    | Bbop.Father
	    | Bbop.Rank
	    | Bbop.Prj2
	    | Bbop.Prj1
	    | Bbop.Image
	    | Bbop.SetMinus
	    | Bbop.CartesianProd -> raise (NotDefined "delta E non d�finie?")

	    | Bbop.Minus ->  mkTrue
	    | Bbop.NatSetRange->  mkTrue
	    | Bbop.Puissance->  mkTrue
	    | Bbop.Mul->  mkTrue
	    | Bbop.Div->  mkTrue
	    | Bbop.Mod->  mkTrue
	    | Bbop.Plus ->  mkTrue
	    | Bbop.PartialFunc->  mkTrue
	    | Bbop.TotalFunc->  mkTrue
	    | Bbop.PartialInj->  mkTrue
	    | Bbop.TotalInj->  mkTrue
	    | Bbop.PartialSurj->  mkTrue
	    | Bbop.TotalSurj->  mkTrue
	    | Bbop.PartialBij->  mkTrue
	    | Bbop.TotalBij->  mkTrue
		
	    | Bbop.OverRideFwd->  mkTrue
	    | Bbop.OverRideBck->  mkTrue	
		
	    | Bbop.AppendSeq->  mkTrue
	    | Bbop.PrependSeq->  mkTrue
	    | Bbop.PrefixSeq->  mkTrue
	    | Bbop.SuffixSeq->  mkTrue

	    | Bbop.DomRestrict->  mkTrue
	    | Bbop.RanRestrict->  mkTrue
	    | Bbop.DomSubstract->  mkTrue
	    | Bbop.RanSubstract->  mkTrue
		
	    | Bbop.InterSets->  mkTrue
	    | Bbop.UnionSets->  mkTrue
		
	    | Bbop.RelProd->  mkTrue
	    | Bbop.ParallelComp->  mkTrue

	    | Bbop.ConcatSeq->  
		let e1 = mk_PredIn expr1 (mk_Seq (mk_Idvar)) in
		let e2 =  mk_PredIn expr2  (mk_Seq (mk_Idvar)) in
		  Blast.ExprPred ( mk_PredAnd e1 e2 )
		    

	)
  | Blast.ExprUn (op1 , expr) (* Unary Expressions *)
    -> (
      match op1 with 
	    (*
	      | Bbop.Perm ->
              mk_pred_to_expr 
	      (mk_PredIn expr 
	      (Blast.ExprUn(Bbop.Fin , Blast_mk.mk_INTEGER )))
	    *)
	| Bbop.Mirror 
	| Bbop.SizeT 
	| Bbop.Postfix 
	| Bbop.Prefix 
	| Bbop.Sons 
	| Bbop.Top 
	| Bbop.Infix 
	| Bbop.Right 
	| Bbop.Left 
	| Bbop.Btree 
	| Bbop.Tree 
	| Bbop.Perm -> raise (NotDefined "delta E non definie ?")
	| Bbop.Min 
	| Bbop.Max 
	| Bbop.Card
	| Bbop.Pow 
	| Bbop.PowOne
	| Bbop.Fin 
	| Bbop.FinOne 
	| Bbop.Identity 
	| Bbop.Dom 
	| Bbop.Ran 
	| Bbop.UnionGen 
	| Bbop.InterGen 
	| Bbop.Seq 
	| Bbop.SeqOne 
	| Bbop.ISeq
	| Bbop.ISeqOne 
	| Bbop.RelFnc 
	| Bbop.FncRel 
	| Bbop.Bool 
	| Bbop.Pred
	| Bbop.Succ 
	| Bbop.Tilde
	| Bbop.UMinus
	| Bbop.Closure
	| Bbop.ClosureOne 
	| Bbop.Cod ->  mkTrue
	| Bbop.Size ->   mk_In  expr (mk_Seq mk_Idvar)
	| Bbop.First ->  mk_In  expr (mk_Seq mk_Idvar)
	| Bbop.Last ->   mk_In  expr (mk_Seq mk_Idvar)
	| Bbop.Front ->  mk_In  expr (mk_Seq mk_Idvar)
	| Bbop.Tail ->   mk_In  expr (mk_Seq mk_Idvar)
	| Bbop.Rev ->    mk_In  expr (mk_Seq mk_Idvar)
	| Bbop.Conc ->   mk_In  expr (mk_Seq (mk_Seq mk_Idvar))

    )   
  |  _ ->  mkTrue
       
and 
    
    (* \section{$\Delta$P : Bonne d�finition des pr�dicats} *)
    
    deltaP pred = 
  match pred with 
      Blast.PredParen(pred) 
      -> deltaP pred
    | Blast.PredNegation(pred) 
      -> deltaP pred
    | Blast.PredExists(ids,pred) 
      -> Blast.PredForAll(ids,deltaP pred)
    | Blast.PredForAll(ids,pred) 
      -> Blast.PredForAll(ids,deltaP pred)        
    | Blast.PredBin (op,pred1,pred2) ->
	(
	  match op with
	      
	    (* Burdy2000 Def. 4.7 *)		  

	    | Bbop.And -> (* P1 & P2 *)
		let p1 = (deltaP pred1) in
		let p2 = (mk_PredImplies pred1 (deltaP pred2)) in
		  mk_PredAnd p1 p2
	    | Bbop.Ou -> 
		(* d(P1 \/ P2) = d(P1) & (-P1 => d(P2)) *)
		Blast.PredBin(Bbop.And,
			      (deltaP pred1),
			      Blast.PredBin(Bbop.Implies,
					    Blast.PredNegation(pred1),
					    (deltaP pred2)))

	    | Bbop.Implies -> 
		Blast.PredBin(
		  Bbop.And,
		  (deltaP pred1),
		  Blast.PredBin(
		    Bbop.Implies,
		    pred1,
		    (deltaP pred2)))


	    | Bbop.Equiv
	    | Bbop.Equal 
	    | Bbop.NotEqual (* ? *)
	    | Bbop.Less 
	    | Bbop.LessEqual
	    | Bbop.Greater 
	    | Bbop.GreaterEqual 

	    | Bbop.SubSet 
	    | Bbop.StrictSubSet
	    | Bbop.NotSubSet (* ? *)
	    | Bbop.NotStrictSubSet (* ? *)
	    | Bbop.In 
	    | Bbop.NotIn (* ? *)-> 
		let e1 = deltaP pred1 in
		let e2 = deltaP pred2 in  mk_PredAnd e1 e2
	)
    | _ -> pred
	
	
(* \section{$\Delta$S : Bonne d�finition des substitutions} *)
(*i 4.2.4 p93 d�finition 4.17 i*)
(* 
	
let rec deltaS subst = 
  match subst with
   | Gsl_affect(id,expr) -> deltaE (expr) 
   | Gsl_precondition(expr,gsl) -> 
   mkPredAnd	(deltaP expr)
   (mkPredImplies  expr    (deltaS gsl) )
   
   | Gsl_bChoice(gsl1,gsl2) -> mkPredAnd (deltaS gsl1) (deltaS gsl2)
   | Gsl_guard(expr,gsl) -> mkPredAnd (deltaP expr) (mkPredImplies expr (deltaS gsl) ) 
   | Gsl_uChoice(ids,gsl) -> mkPredForAll ids  (deltaS gsl)
  | Gsl_parallel(gsl1,gsl2) -> mkPredAnd (deltaS gsl1) (deltaS gsl2)
  | Gsl_sequence(Gsl_parallel(gsl1,gsl2),gsl3) 
    -> 
      let s1 = Gsl_sequence(gsl1,gsl3) in
      let s2 = Gsl_sequence(gsl2,gsl3) in
      mkPredAnd ( deltaS  s1)  ( deltaS s2 )
  | Gsl_sequence(Gsl_affect(id,expr),gsl) -> 
      let p1 = deltaE expr in 		
      let p2 = Gsl.apply (Gsl_affect(id,expr)) (deltaS gsl) in
      (mkPredAnd p1 p2)
  | Gsl_sequence(gsl1,gsl2) -> mkPredFalse ()
  | Gsl_while(exprP,gslS,exprI,exprV) -> mkPredFalse()
  | Gsl_skip -> mkPredTrue ()
	;;	

*)
    
(* fonction qui genere les EDS � partir d'un pr�dicat et
   prend une description de cette g�n�ration *)

(*MG  let eds_generate_properties project mchid= *)
  
let properties project mchid = 
  let machine=(snd (Project.find project mchid)).machine in
    
  let mchgoal=
    try Po_acc.get_component (Po_adt.Properties(mchid)) machine
    with Not_found -> raise No_po
  in 
  let mchreq=
    fun id -> [Parameters(id); Po_adt.Sets(id); Constraints(id); Properties(id)] 
  in
  let seenhyps=Gop_lib.seen_hypos project mchid in
  let hyprequest=(mchreq mchid)@seenhyps in
  let finalpo=
    { hypos = Po_acc.mkHypothesis hyprequest project mchid;
      goal = (deltaP mchgoal);
      filename = (Id_acc.name_of mchid) ^ ".eds";
      trace= "EDS properties"; 
    } 
  in [finalpo]
       
(* fonction qui genere les EDS � partir d'un pr�dicat et
   prend une description de cette g�n�ration *)
let initialization project mchid=

  let machine=(snd (Project.find project mchid)).machine in
    let g=
      begin
	let inv=try 
	  Po_acc.get_component (Invariant(mchid)) machine 
	with  Not_found -> Blast_mk.mk_PredTrue ()
	in
	  try 
	      (Blast_mk.mk_SubstApply 
	        (Clause_acc.get_Initialisation 
		   (Blast_acc.get_clause_list machine)
	        )
	      inv)
	  with Not_found -> inv
      end
    in
    let mchreq=
      fun id -> [Parameters(id); Sets(id); Constraints(id); Properties(id)] 
    in
    let seenhyps=Gop_lib.seen_hypos project mchid in
    let hyprequest=(mchreq mchid)@seenhyps in
    let finalpo=
      { 
	hypos = Po_acc.mkHypothesis hyprequest project mchid;
	goal = deltaP g;      
	trace = "Initialization";
	filename = (Id_acc.name_of mchid) ^ ".eds";
      }
    in [finalpo]

let invariant project mchid=

  let machine=(snd (Project.find project mchid)).machine in
	let inv=try 
	  Po_acc.get_component (Invariant(mchid)) machine 
	with  Not_found -> Blast_mk.mk_PredTrue ()

    in
    let mchreq=
      fun id -> [Parameters(id); Sets(id); Constraints(id); Properties(id)] 
    in
    let seenhyps=Gop_lib.seen_hypos project mchid in
    let hyprequest=(mchreq mchid)@seenhyps in
    let finalpo=
      { 
	hypos = Po_acc.mkHypothesis hyprequest project mchid;
	goal = deltaP inv;      
	trace = "Invariants";
	filename = (Id_acc.name_of mchid) ^ ".eds";
      }
    in [finalpo]

let assertion project mchid=
  let machine=(snd (Project.find project mchid)).machine in
    (*if not (gop_mch.candidate_simple_po machine) then raise No_po;*)
    let mchreq=
      fun id -> [Parameters(id); Sets(id); Constraints(id); Properties(id); Invariant(id)] 
    in
    let seenhyps=Gop_lib.seen_hypos project mchid in
    let hyprequest=(mchreq mchid)@seenhyps in
    let finalgoal=
      try Po_acc.get_component (Assertions(mchid)) machine 
      with Not_found -> raise No_po
    in
    let finalpo=
      { hypos = Po_acc.mkHypothesis hyprequest project mchid;
	goal = deltaP finalgoal;
	trace= "Assertions";
	filename = (Id_acc.name_of mchid) ^ ".eds";
      } 
    in
      [finalpo]

let precondition project mchid =
  let machine=(snd (Project.find project mchid)).machine in
    let mchgoal=
      try Po_acc.get_component (Invariant(mchid)) machine
      with Not_found -> Blast_mk.mk_PredTrue ()
    in 
    let mchreq=
      fun id -> [Parameters(id); Sets(id); Constraints(id); Properties(id); Invariant(id); Assertions(id)] 
    in
    let seenhyps=Gop_lib.seen_hypos project mchid in
    let hyprequest=(mchreq mchid)@seenhyps in

    let all_ops=
      Clause_acc.get_Operations 
	(Blast_acc.get_clause_list machine) in
    let all_opcomps=List.map Gop_lib.decompose_operation all_ops in

  let basepo=
      { hypos = Po_acc.mkHypothesis hyprequest project mchid;
	goal = deltaP mchgoal;
	trace= "Operations"; 
	filename = (Id_acc.name_of mchid) ^ ".mch";
      } 
    in

  let newgoal goal (n,q,v) =
    match q with
      | None 
	-> goal
      | Some(comp) 
	-> add_to_po_goals comp goal
  in
  let finalgoal = List.map (newgoal mchgoal) all_opcomps in

let all_mypos=
  List.map (Gop_lib.complete_goal_with_operation basepo) all_opcomps in
  if all_mypos=[]
  then raise No_po
  else all_mypos

let operations project mchid =
  let machine=(snd (Project.find project mchid)).machine in
    let mchgoal=
      try Po_acc.get_component (Invariant(mchid)) machine
      with Not_found -> Blast_mk.mk_PredTrue ()
    in 
    let mchreq=
      fun id -> [Parameters(id); Sets(id); Constraints(id); Properties(id); Invariant(id); Assertions(id)] 
    in
    let seenhyps=Gop_lib.seen_hypos project mchid in
    let hyprequest=(mchreq mchid)@seenhyps in
    let basepo=
      { hypos = Po_acc.mkHypothesis hyprequest project mchid;
	goal = deltaP mchgoal;
	trace= "Operations"; 
	filename = (Id_acc.name_of mchid) ^ ".mch";
      } 
    in
    let all_ops=
      Clause_acc.get_Operations 
	(Blast_acc.get_clause_list machine) in
    let all_opcomps=List.map Gop_lib.decompose_operation all_ops in
    let all_mypos=
      List.map (Gop_lib.complete_po_with_operation basepo) all_opcomps in
      if all_mypos=[]
      then raise No_po
      else all_mypos



let eds_ref_params project mchid=
  let machine=(snd (Project.find project mchid)).machine in

    let mchgoal=
      try [Po_acc.get_component (Properties(mchid)) machine]
      with Not_found -> []
    in 
    let mchreq=
      fun id -> [Parameters(id); Sets(id); Constraints(id); Properties(id)] 
    in
    let seenhyps=Gop_lib.seen_hypos project mchid in
    let hyprequest=(mchreq mchid)@seenhyps@[Properties(mchid)] in

    let refid_sequence=Gop_lib.create_refid_sequence project mchid in
    let reverse_refids=List.rev refid_sequence in
    let firstmchid=List.hd reverse_refids in

    let hypACreq=(fun id -> [Parameters(id); Constraints(id)]) in
    let coregoal= (hypACreq firstmchid) in

    let finalgoal=
      match (Po_acc.mkPoPredicate coregoal project mchid) with
	| None -> raise No_po
	| Some(pred) -> pred 
    in
    let basepo=
      { hypos = Po_acc.mkHypothesis hyprequest project mchid ;
	goal = finalgoal;
	filename = (Id_acc.name_of mchid) ^ ".eds";
	trace= "Refines_params_eds"; 
      } 
    in [basepo]

let eds_ref_initialization project mchid=
  let machine=(snd (Project.find project mchid)).machine in

    let invgoal=
      try Po_acc.get_component (Invariant(mchid)) machine
      with Not_found -> Blast_mk.mk_PredTrue ()
    in 
 
    let refid_sequence=Gop_lib.create_refid_sequence project mchid in
    let reverse_refids=List.rev refid_sequence in
    let firstmchid=List.hd reverse_refids in

    let hypACreq=(fun id -> [Parameters(id); Constraints(id)]) in
    let seqreq=(fun id -> [Sets(id); Properties(id)]) in

    let seenhyps=Gop_lib.seen_hypos project mchid in
    let wholeseqreq=List.flatten (List.map seqreq reverse_refids) in
    let hyprequest=(hypACreq mchid)@wholeseqreq@seenhyps in


    let not_invgoal=Blast_mk.mk_PredNegation invgoal in
    let previnit_notinv=
      try
	let beforelastid=List.hd (List.tl refid_sequence) in
	let prevmch=(snd (Project.find project beforelastid)).machine in
	let prevmchinit=
	  Clause_acc.get_Initialisation (Blast_acc.get_clause_list prevmch)
	in
	  Blast_mk.mk_SubstApply prevmchinit not_invgoal
      with
	  Invalid_argument "get_Initialisation" -> not_invgoal
    in

    let npini_goal=Blast_mk.mk_PredNegation previnit_notinv in

(* SC: il peut ne pas y avoir d'init... *)
    let mchinit=Gop_lib.get_initialization project mchid in

    let increq=fun id -> [Sets(id); Properties(id)] in
    let allinstanciations=
      Gop_lib.initializations_instanciations project (Blast.Includes([])) mchid
    in

    let build_po id=
      { hypos = Po_acc.mkHypothesis (hyprequest@(increq id)) project mchid ;
	goal = Blast_mk.mk_SubstApply
		 (Blast_mk.mk_SubstSequence
		    (List.assoc id allinstanciations)
		    mchinit
		 )
		 npini_goal ;
	filename = (Id_acc.name_of mchid) ^ ".eds";
	trace="Refines_invariant";
      } 
    in
    let allpos=List.map build_po (fst (List.split allinstanciations)) in
      if allpos=[] then (* there are no included machines *)
	[{ hypos = Po_acc.mkHypothesis hyprequest project mchid ;
	  goal = deltaP (Blast_mk.mk_SubstApply mchinit npini_goal) ;
	  filename = (Id_acc.name_of mchid) ^ ".eds";
	  trace="Refines_invariant_EDS";
	}] 
      else allpos

let eds_ref_invariant project mchid=
  let machine=(snd (Project.find project mchid)).machine in

    let invgoal=
      try Po_acc.get_component (Invariant(mchid)) machine
      with Not_found -> Blast_mk.mk_PredTrue ()
    in 
 
    let refid_sequence=Gop_lib.create_refid_sequence project mchid in
    let reverse_refids=List.rev refid_sequence in
    let firstmchid=List.hd reverse_refids in

    let hypACreq=(fun id -> [Parameters(id); Constraints(id)]) in
    let seqreq=(fun id -> [Sets(id); Properties(id)]) in

    let seenhyps=Gop_lib.seen_hypos project mchid in
    let wholeseqreq=List.flatten (List.map seqreq reverse_refids) in
    let hyprequest=(hypACreq mchid)@wholeseqreq@seenhyps in


    let not_invgoal=Blast_mk.mk_PredNegation invgoal in
    let previnit_notinv=
      try
	let beforelastid=List.hd (List.tl refid_sequence) in
	let prevmch=(snd (Project.find project beforelastid)).machine in
	let prevmchinit=
	  Clause_acc.get_Initialisation (Blast_acc.get_clause_list prevmch)
	in
	  Blast_mk.mk_SubstApply prevmchinit not_invgoal
      with
	  Invalid_argument "get_Initialisation" -> not_invgoal
    in

    let npini_goal=Blast_mk.mk_PredNegation previnit_notinv in

(* SC: il peut ne pas y avoir d'init... *)
    let mchinit=Gop_lib.get_initialization project mchid in

    let increq=fun id -> [Sets(id); Properties(id)] in
    let allinstanciations=
      Gop_lib.initializations_instanciations project (Blast.Includes([])) mchid
    in

    let build_po id=
      { hypos = Po_acc.mkHypothesis (hyprequest@(increq id)) project mchid ;
	goal = Blast_mk.mk_SubstApply
		 (Blast_mk.mk_SubstSequence
		    (List.assoc id allinstanciations)
		    mchinit
		 )
		 npini_goal ;
	filename = (Id_acc.name_of mchid) ^ ".ref";
	trace="Refines_invariant";
      } 
    in
    let allpos=List.map build_po (fst (List.split allinstanciations)) in
      if allpos=[] then (* there are no included machines *)
	[{ hypos = Po_acc.mkHypothesis hyprequest project mchid ;
	  goal = deltaP (Blast_mk.mk_SubstApply mchinit npini_goal) ;
	  filename = (Id_acc.name_of mchid) ^ ".ref";
	  trace="Refines_invariant";
	}] 
      else allpos

let eds_ref_operation project mchid=
  let machine=(snd (Project.find project mchid)).machine in 
    let invgoal=
      try Po_acc.get_component (Invariant(mchid)) machine
      with Not_found -> Blast_mk.mk_PredTrue ()
    in 
    let refid_sequence=Gop_lib.create_refid_sequence project mchid in
    let reverse_refids=List.rev refid_sequence in
    let firstmchid=List.hd reverse_refids in

    let hypACreq=(fun id -> [Parameters(id); Constraints(id)]) in
    let allmchreq=
      fun id -> [Sets(id); Properties(id); Invariant(id); Assertions(id)] 
    in
    let seenhyps=Gop_lib.seen_hypos project mchid in
    let increq=
      fun id -> [Sets(id); Properties(id); Invariant(id); Assertions(id)] 
    in
    let incedges=Bgraph.filter_sourceDep project.graph Bgraph.Includes mchid in
    let includedreqs=List.flatten (List.map increq (List.map Bgraph.target incedges)) in

    let hyprequest=(hypACreq firstmchid) @ 
		   (List.flatten (List.map allmchreq reverse_refids)) @ 
		   seenhyps @ includedreqs
    in
    let base_hypos=Po_acc.mkHypothesis hyprequest project mchid in
    
    let basepo=
      { hypos = base_hypos;
	goal = (deltaP invgoal);
	filename=(Id_acc.name_of mchid) ^ ".ref";
	trace="Refines_operations";
      } 
    in
    let all_opids=List.map 
		    Clause_acc.get_operation_id
		    (Clause_acc.get_Operations 
		       (Blast_acc.get_clause_list machine)) 
    in
    let all_mypos=List.map
		    (fun id -> Gop_lib.complete_po_with_refined_operation basepo id project mchid)
		    all_opids
    in
      if all_mypos=[]
      then raise No_po
      else all_mypos
