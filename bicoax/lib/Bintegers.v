(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Export Bintegers_basics.
Require Export Bintegers_ensembles.
Require Export Bintegers_min.
Require Export Bintegers_max.
Require Export Bintegers_arithmetic.
Require Export Bintegers_arithmetic_inv.
Require Export Bintegers_arithmetic_Z.
Require Export Bintegers_arithmetic_inv_Z.
Require Export Bintegers_iterate.
Require Export Bintegers_iterz.
Require Export Bintegers_cardinal.
Require Export Bintegers_props.

(*
  List of operators to convert from N to Z (also in the BBook):
  N elements
  comparisons (<=, <, >=, >)
  ensembles
  min & max
  plus, mult, exp
  minus, div (mod ?)
List of operators to convert from nat to BN:
  iterate (note: it could be made that iterate (-n) is ~(iterate n), it seems natural
  Bcardinal
*)
