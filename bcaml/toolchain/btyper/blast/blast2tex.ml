(*************************************************************)
(*                PROJET BCAML                               *)
(*                                                           *)
(* http://www3.inrets.fr/Public/ESTAS/Mariano.Georges/Bcaml/ *)
(*                                                           *)
(*                   IRIT                                    *)
(*                                                           *)
(*   source genere le 14-05-2001  09:04:43                   *)
(*************************************************************)

open Blast

let string2tex env s = s
let bool2tex env = function true -> "true" | false -> "false"
let int2tex env = string_of_int
let option2tex f env = function Some x -> f env x | None -> ""
let seq2tex f env l = if l = [] then "" else "\n\\begin{"^env^"}\n"^(String.concat ("\\next@"^env)
           (List.map (fun x -> " "^ (f (env^"@el") x) ^ "\n") l))^"\\end{"^env^"}"
let rec amn2tex env = let env = "amn" in
  (function
  Machine(_x1, _x2) -> let env = "Machine" in
  "\\Machine{"^(head2tex (env^"@head") _x1)^"}{"^((seq2tex clause2tex) (env^"@clauseList") _x2)^"}"
| Refinement(_x1, _x2) -> let env = "Refinement" in
  "\\Refinement{"^(head2tex (env^"@head") _x1)^"}{"^((seq2tex clause2tex) (env^"@clauseList") _x2)^"}"
| Implementation(_x1, _x2) -> let env = "Implementation" in
  "\\Implementation{"^(head2tex (env^"@head") _x1)^"}{"^((seq2tex clause2tex) (env^"@clauseList") _x2)^"}"
| EmptyTree -> let env = "EmptyTree" in
  "\\EmptyTree{}")
and head2tex env (_x1, _x2) =
  let env = "head" in
  "\\head{"^(id2tex (env^"@id") _x1)^"}{"^(ids2tex (env^"@ids") _x2)^"}"
and clause2tex env = let env = "clause" in
  (function
  Definitions(_x1) -> let env = "Definitions" in
  "\\Definitions{"^((seq2tex def2tex) (env^"@defList") _x1)^"}"
| Constraints(_x1) -> let env = "Constraints" in
  "\\Constraints{"^(predicate2tex (env^"@predicate") _x1)^"}"
| Invariant(_x1) -> let env = "Invariant" in
  "\\Invariant{"^(predicate2tex (env^"@predicate") _x1)^"}"
| Sets(_x1) -> let env = "Sets" in
  "\\Sets{"^((seq2tex set2tex) (env^"@setList") _x1)^"}"
| Initialisation(_x1) -> let env = "Initialisation" in
  "\\Initialisation{"^(substitution2tex (env^"@substitution") _x1)^"}"
| ConstantsConcrete(_x1) -> let env = "ConstantsConcrete" in
  "\\ConstantsConcrete{"^(ids2tex (env^"@ids") _x1)^"}"
| ConstantsAbstract(_x1) -> let env = "ConstantsAbstract" in
  "\\ConstantsAbstract{"^(ids2tex (env^"@ids") _x1)^"}"
| ConstantsHidden(_x1) -> let env = "ConstantsHidden" in
  "\\ConstantsHidden{"^(ids2tex (env^"@ids") _x1)^"}"
| Properties(_x1) -> let env = "Properties" in
  "\\Properties{"^(predicate2tex (env^"@predicate") _x1)^"}"
| Values(_x1) -> let env = "Values" in
  "\\Values{"^((seq2tex (fun env (_x1, _x2) -> "\\"^env^"{"^(id2tex (env^"@A") _x1) ^"}{"^ (expr2tex (env^"@B") _x2)^"}")) (env^"@id@exprList") _x1)^"}"
| VariablesConcrete(_x1) -> let env = "VariablesConcrete" in
  "\\VariablesConcrete{"^(ids2tex (env^"@ids") _x1)^"}"
| VariablesAbstract(_x1) -> let env = "VariablesAbstract" in
  "\\VariablesAbstract{"^(ids2tex (env^"@ids") _x1)^"}"
| VariablesHidden(_x1) -> let env = "VariablesHidden" in
  "\\VariablesHidden{"^(ids2tex (env^"@ids") _x1)^"}"
| Promotes(_x1) -> let env = "Promotes" in
  "\\Promotes{"^(ids2tex (env^"@ids") _x1)^"}"
| Assertions(_x1) -> let env = "Assertions" in
  "\\Assertions{"^((seq2tex predicate2tex) (env^"@predicateList") _x1)^"}"
| Operations(_x1) -> let env = "Operations" in
  "\\Operations{"^((seq2tex operation2tex) (env^"@operationList") _x1)^"}"
| Sees(_x1) -> let env = "Sees" in
  "\\Sees{"^(ids2tex (env^"@ids") _x1)^"}"
| Uses(_x1) -> let env = "Uses" in
  "\\Uses{"^(ids2tex (env^"@ids") _x1)^"}"
| Extends(_x1) -> let env = "Extends" in
  "\\Extends{"^((seq2tex instance2tex) (env^"@instanceList") _x1)^"}"
| Includes(_x1) -> let env = "Includes" in
  "\\Includes{"^((seq2tex instance2tex) (env^"@instanceList") _x1)^"}"
| Imports(_x1) -> let env = "Imports" in
  "\\Imports{"^((seq2tex instance2tex) (env^"@instanceList") _x1)^"}"
| Refines(_x1) -> let env = "Refines" in
  "\\Refines{"^(id2tex (env^"@id") _x1)^"}")
and def2tex env = let env = "def" in
  (function
  Def(_x1, _x2, _x3) -> let env = "Def" in
  "\\Def{"^(id2tex (env^"@id") _x1)^"}{"^(ids2tex (env^"@ids") _x2)^"}{"^(defBody2tex (env^"@defBody") _x3)^"}"
| FileDef(_x1) -> let env = "FileDef" in
  "\\FileDef{"^(string2tex (env^"@string") _x1)^"}")
and defBody2tex env = let env = "defBody" in
  (function
  ExprDefBody(_x1) -> let env = "ExprDefBody" in
  "\\ExprDefBody{"^(expr2tex (env^"@expr") _x1)^"}"
| SubstDefBody(_x1) -> let env = "SubstDefBody" in
  "\\SubstDefBody{"^(substitution2tex (env^"@substitution") _x1)^"}")
and set2tex env = let env = "set" in
  (function
  SetAbstDec(_x1) -> let env = "SetAbstDec" in
  "\\SetAbstDec{"^(id2tex (env^"@id") _x1)^"}"
| SetEnumDec(_x1, _x2) -> let env = "SetEnumDec" in
  "\\SetEnumDec{"^(id2tex (env^"@id") _x1)^"}{"^(ids2tex (env^"@ids") _x2)^"}"
| SetRecordsDec(_x1, _x2) -> let env = "SetRecordsDec" in
  "\\SetRecordsDec{"^(id2tex (env^"@id") _x1)^"}{"^((seq2tex recordsItem2tex) (env^"@recordsItemList") _x2)^"}")
and instance2tex env (_x1, _x2) =
  let env = "instance" in
  "\\instance{"^(id2tex (env^"@id") _x1)^"}{"^((seq2tex expr2tex) (env^"@exprList") _x2)^"}"
and type_B2tex env = let env = "type_B" in
  (function
  TypeNatSetRange(_x1, _x2) -> let env = "TypeNatSetRange" in
  "\\TypeNatSetRange{"^(expr2tex (env^"@expr") _x1)^"}{"^(expr2tex (env^"@expr") _x2)^"}"
| PredType(_x1) -> let env = "PredType" in
  "\\PredType{"^(expr2tex (env^"@expr") _x1)^"}"
| RecordsType(_x1) -> let env = "RecordsType" in
  "\\RecordsType{"^((seq2tex field2tex) (env^"@fieldList") _x1)^"}"
| TypeIdentifier(_x1) -> let env = "TypeIdentifier" in
  "\\TypeIdentifier{"^(id2tex (env^"@id") _x1)^"}"
| PFunType(_x1, _x2) -> let env = "PFunType" in
  "\\PFunType{"^(type_B2tex (env^"@type_B") _x1)^"}{"^(type_B2tex (env^"@type_B") _x2)^"}"
| FunType(_x1, _x2) -> let env = "FunType" in
  "\\FunType{"^(type_B2tex (env^"@type_B") _x1)^"}{"^(type_B2tex (env^"@type_B") _x2)^"}"
| ProdType(_x1, _x2) -> let env = "ProdType" in
  "\\ProdType{"^(type_B2tex (env^"@type_B") _x1)^"}{"^(type_B2tex (env^"@type_B") _x2)^"}"
| Sequence(_x1) -> let env = "Sequence" in
  "\\Sequence{"^(type_B2tex (env^"@type_B") _x1)^"}"
| SubType(_x1, _x2) -> let env = "SubType" in
  "\\SubType{"^(type_B2tex (env^"@type_B") _x1)^"}{"^(expr2tex (env^"@expr") _x2)^"}"
| Fin(_x1) -> let env = "Fin" in
  "\\Fin{"^(type_B2tex (env^"@type_B") _x1)^"}"
| Untyped -> let env = "Untyped" in
  "\\Untyped{}")
and field2tex env (_x1, _x2) =
  let env = "field" in
  "\\field{"^(id2tex (env^"@id") _x1)^"}{"^(type_B2tex (env^"@type_B") _x2)^"}"
and predicate2tex env = let env = "predicate" in
  (function
  PredParen(_x1) -> let env = "PredParen" in
  "\\PredParen{"^(predicate2tex (env^"@predicate") _x1)^"}"
| PredNegation(_x1) -> let env = "PredNegation" in
  "\\PredNegation{"^(predicate2tex (env^"@predicate") _x1)^"}"
| PredExists(_x1, _x2) -> let env = "PredExists" in
  "\\PredExists{"^((seq2tex id2tex) (env^"@idList") _x1)^"}{"^(predicate2tex (env^"@predicate") _x2)^"}"
| PredForAll(_x1, _x2) -> let env = "PredForAll" in
  "\\PredForAll{"^((seq2tex id2tex) (env^"@idList") _x1)^"}{"^(predicate2tex (env^"@predicate") _x2)^"}"
| PredBin(_x1, _x2, _x3) -> let env = "PredBin" in
  "\\PredBin{"^(Bbop.bop22tex (env^"@Bbop@bop2") _x1)^"}{"^(predicate2tex (env^"@predicate") _x2)^"}{"^(predicate2tex (env^"@predicate") _x3)^"}"
| PredAtom(_x1, _x2, _x3) -> let env = "PredAtom" in
  "\\PredAtom{"^(Bbop.bop22tex (env^"@Bbop@bop2") _x1)^"}{"^(expr2tex (env^"@expr") _x2)^"}{"^(expr2tex (env^"@expr") _x3)^"}")
and operation2tex env (_x1, _x2, _x3, _x4) =
  let env = "operation" in
  "\\operation{"^(id2tex (env^"@id") _x1)^"}{"^(ids2tex (env^"@ids") _x2)^"}{"^(ids2tex (env^"@ids") _x3)^"}{"^(substitution2tex (env^"@substitution") _x4)^"}"
and expr2tex env = let env = "expr" in
  (function
  ExprSequence(_x1, _x2) -> let env = "ExprSequence" in
  "\\ExprSequence{"^(expr2tex (env^"@expr") _x1)^"}{"^(expr2tex (env^"@expr") _x2)^"}"
| ExprParen(_x1) -> let env = "ExprParen" in
  "\\ExprParen{"^(expr2tex (env^"@expr") _x1)^"}"
| ExprId(_x1) -> let env = "ExprId" in
  "\\ExprId{"^(id2tex (env^"@id") _x1)^"}"
| ExprFunCall(_x1, _x2) -> let env = "ExprFunCall" in
  "\\ExprFunCall{"^(expr2tex (env^"@expr") _x1)^"}{"^((seq2tex expr2tex) (env^"@exprList") _x2)^"}"
| ExprBin(_x1, _x2, _x3) -> let env = "ExprBin" in
  "\\ExprBin{"^(Bbop.op22tex (env^"@Bbop@op2") _x1)^"}{"^(expr2tex (env^"@expr") _x2)^"}{"^(expr2tex (env^"@expr") _x3)^"}"
| ExprUn(_x1, _x2) -> let env = "ExprUn" in
  "\\ExprUn{"^(Bbop.op12tex (env^"@Bbop@op1") _x1)^"}{"^(expr2tex (env^"@expr") _x2)^"}"
| ExprSeq(_x1) -> let env = "ExprSeq" in
  "\\ExprSeq{"^(exprSeq2tex (env^"@exprSeq") _x1)^"}"
| ExprSet(_x1) -> let env = "ExprSet" in
  "\\ExprSet{"^(exprSet2tex (env^"@exprSet") _x1)^"}"
| RelSet(_x1, _x2) -> let env = "RelSet" in
  "\\RelSet{"^(expr2tex (env^"@expr") _x1)^"}{"^(expr2tex (env^"@expr") _x2)^"}"
| RelSeqComp(_x1) -> let env = "RelSeqComp" in
  "\\RelSeqComp{"^((seq2tex expr2tex) (env^"@exprList") _x1)^"}"
| ExprNumber(_x1) -> let env = "ExprNumber" in
  "\\ExprNumber{"^(number2tex (env^"@number") _x1)^"}"
| ExprBool(_x1) -> let env = "ExprBool" in
  "\\ExprBool{"^(bool2tex (env^"@bool") _x1)^"}"
| ExprString(_x1) -> let env = "ExprString" in
  "\\ExprString{"^(string2tex (env^"@string") _x1)^"}"
| ExprRecords(_x1) -> let env = "ExprRecords" in
  "\\ExprRecords{"^(exprRecords2tex (env^"@exprRecords") _x1)^"}"
| ExprPred(_x1) -> let env = "ExprPred" in
  "\\ExprPred{"^(predicate2tex (env^"@predicate") _x1)^"}"
| ExprNuplet(_x1) -> let env = "ExprNuplet" in
  "\\ExprNuplet{"^((seq2tex expr2tex) (env^"@exprList") _x1)^"}"
| ExprSIGMA(_x1, _x2, _x3) -> let env = "ExprSIGMA" in
  "\\ExprSIGMA{"^((seq2tex id2tex) (env^"@idList") _x1)^"}{"^(predicate2tex (env^"@predicate") _x2)^"}{"^(expr2tex (env^"@expr") _x3)^"}"
| ExprPI(_x1, _x2, _x3) -> let env = "ExprPI" in
  "\\ExprPI{"^((seq2tex id2tex) (env^"@idList") _x1)^"}{"^(predicate2tex (env^"@predicate") _x2)^"}{"^(expr2tex (env^"@expr") _x3)^"}"
| ExprLambda(_x1, _x2, _x3) -> let env = "ExprLambda" in
  "\\ExprLambda{"^((seq2tex id2tex) (env^"@idList") _x1)^"}{"^(predicate2tex (env^"@predicate") _x2)^"}{"^(expr2tex (env^"@expr") _x3)^"}")
and exprRecords2tex env = let env = "exprRecords" in
  (function
  RecordsWithField(_x1) -> let env = "RecordsWithField" in
  "\\RecordsWithField{"^((seq2tex recordsItem2tex) (env^"@recordsItemList") _x1)^"}"
| Records(_x1) -> let env = "Records" in
  "\\Records{"^((seq2tex expr2tex) (env^"@exprList") _x1)^"}"
| RecordsAccess(_x1, _x2) -> let env = "RecordsAccess" in
  "\\RecordsAccess{"^(expr2tex (env^"@expr") _x1)^"}{"^(id2tex (env^"@id") _x2)^"}"
| RecordsSet(_x1) -> let env = "RecordsSet" in
  "\\RecordsSet{"^((seq2tex recordsItem2tex) (env^"@recordsItemList") _x1)^"}")
and recordsItem2tex env (_x1, _x2) =
  let env = "recordsItem" in
  "\\recordsItem{"^(id2tex (env^"@id") _x1)^"}{"^(expr2tex (env^"@expr") _x2)^"}"
and exprSeq2tex env = let env = "exprSeq" in
  (function
  SeqEnum(_x1) -> let env = "SeqEnum" in
  "\\SeqEnum{"^((seq2tex expr2tex) (env^"@exprList") _x1)^"}"
| SeqEmpty -> let env = "SeqEmpty" in
  "\\SeqEmpty{}")
and exprSet2tex env = let env = "exprSet" in
  (function
  SetRange(_x1, _x2) -> let env = "SetRange" in
  "\\SetRange{"^(expr2tex (env^"@expr") _x1)^"}{"^(expr2tex (env^"@expr") _x2)^"}"
| SetEnum(_x1) -> let env = "SetEnum" in
  "\\SetEnum{"^((seq2tex expr2tex) (env^"@exprList") _x1)^"}"
| SetCompr(_x1) -> let env = "SetCompr" in
  "\\SetCompr{"^((seq2tex expr2tex) (env^"@exprList") _x1)^"}"
| SetComprPred(_x1, _x2) -> let env = "SetComprPred" in
  "\\SetComprPred{"^((seq2tex id2tex) (env^"@idList") _x1)^"}{"^(predicate2tex (env^"@predicate") _x2)^"}"
| SetEmpty -> let env = "SetEmpty" in
  "\\SetEmpty{}"
| SetUnionQuantify(_x1, _x2, _x3) -> let env = "SetUnionQuantify" in
  "\\SetUnionQuantify{"^((seq2tex id2tex) (env^"@idList") _x1)^"}{"^(predicate2tex (env^"@predicate") _x2)^"}{"^(expr2tex (env^"@expr") _x3)^"}"
| SetInterQuantify(_x1, _x2, _x3) -> let env = "SetInterQuantify" in
  "\\SetInterQuantify{"^((seq2tex id2tex) (env^"@idList") _x1)^"}{"^(predicate2tex (env^"@predicate") _x2)^"}{"^(expr2tex (env^"@expr") _x3)^"}")
and number2tex env = let env = "number" in
  (function
  MinNumber -> let env = "MinNumber" in
  "\\MinNumber{}"
| Number(_x1) -> let env = "Number" in
  "\\Number{"^(int2tex (env^"@int") _x1)^"}"
| MaxNumber -> let env = "MaxNumber" in
  "\\MaxNumber{}")
and substitution2tex env = let env = "substitution" in
  (function
  SubstOperCall(_x1, _x2, _x3) -> let env = "SubstOperCall" in
  "\\SubstOperCall{"^(ids2tex (env^"@ids") _x1)^"}{"^(id2tex (env^"@id") _x2)^"}{"^((seq2tex expr2tex) (env^"@exprList") _x3)^"}"
| SubstBlock(_x1) -> let env = "SubstBlock" in
  "\\SubstBlock{"^(substitution2tex (env^"@substitution") _x1)^"}"
| SubstPrecondition(_x1, _x2) -> let env = "SubstPrecondition" in
  "\\SubstPrecondition{"^(predicate2tex (env^"@predicate") _x1)^"}{"^(substitution2tex (env^"@substitution") _x2)^"}"
| SubstAssertion(_x1, _x2) -> let env = "SubstAssertion" in
  "\\SubstAssertion{"^(predicate2tex (env^"@predicate") _x1)^"}{"^(substitution2tex (env^"@substitution") _x2)^"}"
| SubstChoice(_x1) -> let env = "SubstChoice" in
  "\\SubstChoice{"^((seq2tex substitution2tex) (env^"@substitutionList") _x1)^"}"
| SubstIf(_x1, _x2) -> let env = "SubstIf" in
  "\\SubstIf{"^((seq2tex (fun env (_x1, _x2) -> "\\"^env^"{"^(predicate2tex (env^"@A") _x1) ^"}{"^ (substitution2tex (env^"@B") _x2)^"}")) (env^"@predicate@substitutionList") _x1)^"}{"^((option2tex substitution2tex) (env^"@substitution") _x2)^"}"
| SubstSelect(_x1, _x2) -> let env = "SubstSelect" in
  "\\SubstSelect{"^((seq2tex (fun env (_x1, _x2) -> "\\"^env^"{"^(predicate2tex (env^"@A") _x1) ^"}{"^ (substitution2tex (env^"@B") _x2)^"}")) (env^"@predicate@substitutionList") _x1)^"}{"^((option2tex substitution2tex) (env^"@substitution") _x2)^"}"
| SubstCase(_x1, _x2, _x3) -> let env = "SubstCase" in
  "\\SubstCase{"^(expr2tex (env^"@expr") _x1)^"}{"^((seq2tex (fun env (_x1, _x2) -> "\\"^env^"{"^((seq2tex expr2tex) (env^"@A") _x1) ^"}{"^ (substitution2tex (env^"@B") _x2)^"}")) (env^"@exprList@substitutionList") _x2)^"}{"^((option2tex substitution2tex) (env^"@substitution") _x3)^"}"
| SubstAny(_x1, _x2, _x3) -> let env = "SubstAny" in
  "\\SubstAny{"^((seq2tex id2tex) (env^"@idList") _x1)^"}{"^(predicate2tex (env^"@predicate") _x2)^"}{"^(substitution2tex (env^"@substitution") _x3)^"}"
| SubstLet(_x1, _x2, _x3) -> let env = "SubstLet" in
  "\\SubstLet{"^((seq2tex id2tex) (env^"@idList") _x1)^"}{"^(predicate2tex (env^"@predicate") _x2)^"}{"^(substitution2tex (env^"@substitution") _x3)^"}"
| SubstVar(_x1, _x2) -> let env = "SubstVar" in
  "\\SubstVar{"^((seq2tex id2tex) (env^"@idList") _x1)^"}{"^(substitution2tex (env^"@substitution") _x2)^"}"
| SubstWhile(_x1, _x2, _x3, _x4) -> let env = "SubstWhile" in
  "\\SubstWhile{"^(predicate2tex (env^"@cond@predicate") _x1)^"}{"^(substitution2tex (env^"@substitution") _x2)^"}{"^(expr2tex (env^"@variant@expr") _x3)^"}{"^(predicate2tex (env^"@inv@predicate") _x4)^"}"
| SubstSkip -> let env = "SubstSkip" in
  "\\SubstSkip{}"
| SubstSequence(_x1, _x2) -> let env = "SubstSequence" in
  "\\SubstSequence{"^(substitution2tex (env^"@substitution") _x1)^"}{"^(substitution2tex (env^"@substitution") _x2)^"}"
| SubstParallel(_x1, _x2) -> let env = "SubstParallel" in
  "\\SubstParallel{"^(substitution2tex (env^"@substitution") _x1)^"}{"^(substitution2tex (env^"@substitution") _x2)^"}"
| SubstSetEqualIds(_x1, _x2) -> let env = "SubstSetEqualIds" in
  "\\SubstSetEqualIds{"^(ids2tex (env^"@ids") _x1)^"}{"^((seq2tex expr2tex) (env^"@exprList") _x2)^"}"
| SubstSetEqualFun(_x1, _x2, _x3) -> let env = "SubstSetEqualFun" in
  "\\SubstSetEqualFun{"^(expr2tex (env^"@expr") _x1)^"}{"^((seq2tex expr2tex) (env^"@exprList") _x2)^"}{"^(expr2tex (env^"@expr") _x3)^"}"
| SubstSetEqualRecords(_x1, _x2) -> let env = "SubstSetEqualRecords" in
  "\\SubstSetEqualRecords{"^(exprRecords2tex (env^"@exprRecords") _x1)^"}{"^(expr2tex (env^"@expr") _x2)^"}"
| SubstBecomeSuch(_x1, _x2) -> let env = "SubstBecomeSuch" in
  "\\SubstBecomeSuch{"^(ids2tex (env^"@ids") _x1)^"}{"^(predicate2tex (env^"@predicate") _x2)^"}"
| SubstSetIn(_x1, _x2) -> let env = "SubstSetIn" in
  "\\SubstSetIn{"^(ids2tex (env^"@ids") _x1)^"}{"^(expr2tex (env^"@expr") _x2)^"}")
and ids2tex env (_x1) =
  let env = "ids" in
  "\\ids{"^((seq2tex id2tex) (env^"@idList") _x1)^"}"
and origine2tex env = let env = "origine" in
  (function
  Local -> let env = "Local" in
  "\\Local{}"
| Remote(_x1) -> let env = "Remote" in
  "\\Remote{"^((seq2tex (fun env (_x1, _x2) -> "\\"^env^"{"^(string2tex (env^"@A") _x1) ^"}{"^ (string2tex (env^"@B") _x2)^"}")) (env^"@string@stringList") _x1)^"}"
| OrgRefine(_x1, _x2) -> let env = "OrgRefine" in
  "\\OrgRefine{"^(string2tex (env^"@string") _x1)^"}{"^((seq2tex (fun env (_x1, _x2) -> "\\"^env^"{"^(string2tex (env^"@A") _x1) ^"}{"^ (string2tex (env^"@B") _x2)^"}")) (env^"@string@stringList") _x2)^"}"
| Prefix(_x1) -> let env = "Prefix" in
  "\\Prefix{"^((seq2tex string2tex) (env^"@stringList") _x1)^"}")
and id2tex env = let env = "id" in
  (function
  Id(_x1, _x2, _x3, _x4) -> let env = "Id" in
  "\\Id{"^(origine2tex (env^"@origine") _x1)^"}{"^(string2tex (env^"@string") _x2)^"}{"^((option2tex type_B2tex) (env^"@type_B") _x3)^"}{"^(Bbop.kind2tex (env^"@Bbop@kind") _x4)^"}")
