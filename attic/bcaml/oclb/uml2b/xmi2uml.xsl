<?xml version="1.0"?>


<!-- $Id$ -->

<!-- ==============================
Transformation d'un XMI de Poseidon
  en un fichier reconnu par ioXML
=============================== -->





<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:UML="org.omg.xmi.namespace.UML"
	exclude-result-prefixes="UML">

  <xsl:output method="xml" omit-xml-declaration="yes" indent="yes"/>

  <!-- instruction attendue par XSLTProc -->
  <xsl:namespace-alias stylesheet-prefix="UML" result-prefix="xsl"/>

  <!-- instruction attendue par Xalan -->
  <!--
  <xsl:namespace-alias stylesheet-prefix="UML" result-prefix="#default"/>
  -->



  <!-- noeud XMI -->
  <xsl:template match="XMI">
    <xsl:apply-templates select="XMI.content"/>
  </xsl:template>


  <!-- noeud XMI.content -->
  <xsl:template match="/XMI/XMI.content">
    <xsl:apply-templates select="UML:Model"/>
  </xsl:template>


  <!-- noeud UML:Model -->
  <xsl:template match="UML:Model">
    <Model>
      <tuple>
        <ti>
          <!-- nom du modele -->
          "<xsl:value-of select="@name"/>"
        </ti>
        <xsl:apply-templates select="UML:Namespace.ownedElement"/>
      </tuple>
    </Model>
  </xsl:template>


  <!-- noeud UML:Namespace.ownedElement -->
  <xsl:template match="UML:Namespace.ownedElement">
    <ti>
      <!-- ajout des classes -->
      <list>
        <xsl:apply-templates
          select="UML:Class | UML:AssociationClass | UML:Package">
          <xsl:with-param name="entity" select="'class'"/>
        </xsl:apply-templates>
      </list>
    </ti>
    <ti>
      <!-- ajout des associations -->
      <list>
        <xsl:apply-templates select="UML:Association | UML:Package">
          <xsl:with-param name="entity" select="'assoc'"/>
        </xsl:apply-templates>
      </list>
    </ti>
  </xsl:template>


  <!-- Traitement des classes ou des associations d'un package
       (suivant la valeur du parametre 'entity') -->
  <xsl:template match="UML:Package">
    <xsl:param name="entity"/>
    <xsl:variable name="uno" select="UML:Namespace.ownedElement"/>
    <xsl:choose>
      <xsl:when test="$entity='class'">
        <xsl:apply-templates
          select="$uno/UML:Class | $uno/UML:AssociationClass
            | $uno/UML:Package">
          <xsl:with-param name="entity" select="'class'"/>
        </xsl:apply-templates>
      </xsl:when>
      <xsl:when test="$entity='assoc'">
        <xsl:apply-templates
          select="$uno/UML:Association | $uno/UML:Package">
          <xsl:with-param name="entity" select="'assoc'"/>
        </xsl:apply-templates>
      </xsl:when>
    </xsl:choose>
  </xsl:template>



<!-- ##################################################
           TRAITEMENT DES CLASSES DU MODELE
(les classes-associations sont vues commes des classes)
################################################### -->

  <xsl:template match="UML:Class | UML:AssociationClass">

    <xsl:variable name="className" select="string(@name)"/>

    <!-- classes anonymes non prises en compte -->
    <xsl:if test="string-length($className)!=0">

      <!-- recuperation du xmi.id de la classe -->
      <xsl:variable name="classId" select="string(@xmi.id)"/>

      <li>
        <Class>
          <tuple>
            <ti>
              <!-- nom de la classe -->
              "<xsl:value-of select="$className"/>"
            </ti>
            <ti>
              <!-- ajout de la multiplicite de classe -->
              <Multiplicity>
                <Range>
                  <tuple>
                    <ti>""</ti>
                    <ti>""</ti>
                  </tuple>
                </Range>
              </Multiplicity>
            </ti>
            <ti>
              <!-- ajout des associations liees a la classe-association
                   (liste vide s'il s'agit d'une classe ordinaire) -->
              <list>
                <xsl:if test="name(.)='UML:AssociationClass'">
                  <xsl:call-template name="get_assoc"/>
                </xsl:if>
              </list>
            </ti>
            <ti>
              <!-- ajout des super-classes dont cette classe herite -->
              <list>
                <xsl:apply-templates select="../UML:Generalization">
                  <xsl:with-param name="id" select="$classId"/>
                </xsl:apply-templates>
              </list>
            </ti>
            <ti>
              <!-- ajout des attributs de la classe -->
              <list>
                <xsl:apply-templates select="UML:Classifier.feature">
                  <xsl:with-param name="type" select="'attr'"/>
                </xsl:apply-templates>
              </list>
            </ti>
            <ti>
              <!-- ajout des operations de la classe -->
              <list>
                <xsl:apply-templates select="UML:Classifier.feature">
                  <xsl:with-param name="type" select="'oper'"/>
                </xsl:apply-templates>
              </list>
            </ti>
            <ti>
              <!-- ajout des contraintes OCL relatives a cette classe -->
              <list>
                <xsl:apply-templates select="../UML:Comment">
                  <xsl:with-param name="id" select="$classId"/>
                </xsl:apply-templates>
                <xsl:apply-templates select="../UML:Constraint">
                  <xsl:with-param name="id" select="$classId"/>
                </xsl:apply-templates>
              </list>
            </ti>
            <ti>
              <!-- ajout des diagrammes d'etats-transitions de la classe -->
              <list>
                <xsl:apply-templates select="../../../UML:StateMachine">
                  <xsl:with-param name="id" select="$classId"/>
                </xsl:apply-templates>
              </list>
            </ti>
          </tuple>
        </Class>
      </li>

    </xsl:if>

  </xsl:template>


  <!-- Debranchement sur les attributs ou sur les operations
       (suivant la valeur du parametre 'type') -->
  <xsl:template match="UML:Classifier.feature">
    <xsl:param name="type"/>
    <xsl:choose>
      <xsl:when test="$type='attr'">
        <xsl:apply-templates select="UML:Attribute"/>
      </xsl:when>
      <xsl:when test="$type='oper'">
        <xsl:apply-templates select="UML:Operation"/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>



<!-- ################################
TRAITEMENT DES ATTRIBUTS D'UNE CLASSE
################################# -->

  <xsl:template match="UML:Attribute">

    <xsl:variable name="attrName" select="string(@name)"/>

    <!-- attributs anonymes non pris en compte -->
    <xsl:if test="string-length($attrName)!=0">
      <li>
        <Attribute>
          <tuple>
            <ti>
              <!-- nom de l'attribut -->
              "<xsl:value-of select="$attrName"/>"
            </ti>
            <ti>
              <!-- ajout de la multiplicite de l'attribut -->
              <xsl:choose>
                <!-- valeur par defaut de la multiplicite d'un attribut -->
                <xsl:when test="count(./UML:StructuralFeature.multiplicity)=0">
                  <Multiplicity>
                    <Range>
                      <tuple>
                        <ti>"1"</ti>
                        <ti>"1"</ti>
                      </tuple>
                    </Range>
                  </Multiplicity>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:apply-templates
                    select="UML:StructuralFeature.multiplicity"/>
                </xsl:otherwise>
              </xsl:choose>
            </ti>
            <ti>
              <!-- ajout du type de l'attribut -->
                <xsl:apply-templates select="UML:StructuralFeature.type">
                  <xsl:with-param name="basePath" select="../.."/>
                </xsl:apply-templates>
            </ti>
            <ti>
              <!-- ajout de la valeur initiale de l'attribut -->
              "<xsl:apply-templates select="UML:Attribute.initialValue"/>"
            </ti>
          </tuple>
        </Attribute>
      </li>
    </xsl:if>

  </xsl:template>


<!-- Traitement de la multiplicite d'un attribut -->
  <xsl:template match="UML:StructuralFeature.multiplicity">
    <xsl:apply-templates select="UML:Multiplicity"/>
  </xsl:template>


<!-- Traitement de la valeur initiale de l'attribut -->
  <xsl:template match="UML:Attribute.initialValue">
    <xsl:apply-templates select="UML:Expression"/>
  </xsl:template>



<!-- #################################
TRAITEMENT DES OPERATIONS D'UNE CLASSE
################################## -->

  <xsl:template match="UML:Operation">

    <xsl:variable name="operName" select="string(@name)"/>

    <!-- operations anonymes non prises en compte -->
    <xsl:if test="string-length($operName)!=0">
      <li>
        <Operation>
          <tuple>
            <ti>
              <!-- nom de l'operation -->
              "<xsl:value-of select="$operName"/>"
            </ti>
            <ti>
              <list>
                <xsl:apply-templates select="UML:BehavioralFeature.parameter">
                  <xsl:with-param name="kind" select="'arg'"/>
                </xsl:apply-templates>
              </list>
            </ti>
            <ti>
              <list>
                <xsl:apply-templates select="UML:BehavioralFeature.parameter">
                  <xsl:with-param name="kind" select="'return'"/>
                </xsl:apply-templates>
              </list>
            </ti>
          </tuple>
        </Operation>
      </li>
    </xsl:if>

  </xsl:template>


<!-- Traitement des arguments et des valeurs de retour de l'operation
       (selon la valeur du parametre 'kind') -->
  <xsl:template match="UML:BehavioralFeature.parameter">
    <xsl:param name="kind"/>
    <xsl:apply-templates select="UML:Parameter">
      <xsl:with-param name="kind2" select="$kind"/>
    </xsl:apply-templates>
  </xsl:template>


  <xsl:template match="UML:Parameter">
    <xsl:param name="kind2"/>

    <xsl:variable name="paramName" select="string(@name)"/>
    <xsl:variable name="paramKind" select="string(@kind)"/>

    <!-- parametres anonymes non pris en compte -->
    <xsl:if test="string-length($paramName)!=0">
      <xsl:if
        test="($kind2='arg' and $paramKind!='return')
          or ($kind2='return' and $paramKind='return')">
        <li>
          <Parameter>
            <tuple>
              <ti>
                <!-- nom du parametre -->
                "<xsl:value-of select="$paramName"/>"
              </ti>
              <ti>
                <!-- ajout du type du parametre -->
                <xsl:apply-templates select="UML:Parameter.type">
                  <xsl:with-param name="basePath" select="../../../.."/>
                </xsl:apply-templates>
              </ti>
            </tuple>
          </Parameter>
        </li>
      </xsl:if>
    </xsl:if>

  </xsl:template>



<!-- ##############################################
TRAITEMENT DES SUPER-CLASSES DONT UNE CLASSE HERITE
############################################### -->

  <xsl:template match="UML:Generalization">
    <xsl:param name="id"/>

    <xsl:variable
      name="classIdref"
      select="./UML:Generalization.child/UML:Class/@xmi.idref
        | ./UML:Generalization.child/UML:AssociationClass/@xmi.idref"/>

    <xsl:if test="$classIdref=$id">
      <xsl:variable
        name="parentIdref"
        select="./UML:Generalization.parent/UML:Class/@xmi.idref
          | ./UML:Generalization.parent/UML:AssociationClass/@xmi.idref"/>

      <xsl:for-each select="../UML:Class | ../UML:AssociationClass">
        <xsl:if test="@xmi.id=$parentIdref">
          <xsl:variable name="parentName" select="@name"/>
          <xsl:if test="string-length($parentName)!=0">
            <li>
              <Generalization>
                <Parent>
                  "<xsl:value-of select="$parentName"/>"
                </Parent>
              </Generalization>
            </li>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>

    </xsl:if>

  </xsl:template>



<!-- ######################################
TRAITEMENT DES CONTRAINTES OCL D'UNE CLASSE
####################################### -->

  <!-- Traitement des commentaires attaches a une classe -->
  <xsl:template match="UML:Comment">
    <xsl:param name="id"/>

    <xsl:variable
      name="classIdref"
      select="./UML:Comment.annotatedElement/UML:Class/@xmi.idref
        | ./UML:Comment.annotatedElement/UML:AssociationClass/@xmi.idref"/>
    <xsl:variable name="oclConstraint" select="@body"/>

    <xsl:if test="$classIdref=$id and string-length($oclConstraint)!=0">
      <li>
        "<xsl:value-of select="$oclConstraint"/>"
      </li>
    </xsl:if>

  </xsl:template>


  <!-- Traitement des contraintes relatives a une classe -->
  <xsl:template match="UML:Constraint">
    <xsl:param name="id"/>

    <xsl:variable
      name="classIdref"
      select="./UML:Constraint.constrainedElement/UML:Class/@xmi.idref
        | ./UML:Constraint.constrainedElement/UML:AssociationClass/@xmi.idref"/>
    <xsl:variable
      name="oclConstraint"
      select="./UML:Constraint.body/UML:BooleanExpression/@body"/>

    <xsl:if test="$classIdref=$id and string-length($oclConstraint)!=0">
      <li>
        "<xsl:value-of select="$oclConstraint"/>"
      </li>
    </xsl:if>

  </xsl:template>



<!-- #####################################################
TRAITEMENT DES DIAGRAMMES D'ETATS-TRANSITIONS D'UNE CLASSE
###################################################### -->

  <xsl:template match="UML:StateMachine">
    <xsl:param name="id"/>

    <xsl:variable
      name="classIdref"
      select="./UML:StateMachine.context/UML:Class/@xmi.idref
        | ./UML:StateMachine.context/UML:AssociationClass/@xmi.idref"/>

    <xsl:if test="$classIdref=$id">
      <li>
        <State_machine>
          <tuple>
            <ti>
              <!-- nom du diagramme d'etats-transitions  -->
              "<xsl:value-of select="@name"/>"
            </ti>
            <ti>
              <!-- ajout des etats de l'automate -->
              <list>
                <xsl:apply-templates select="UML:StateMachine.top"/>
              </list>
            </ti>
            <ti>
              <!-- ajout des transitions -->
              <list>
                <xsl:apply-templates select="UML:StateMachine.transitions">
                  <xsl:with-param
                    name="statesNode"
                    select="./UML:StateMachine.top"/>
                </xsl:apply-templates>
              </list>
            </ti>
          </tuple>
        </State_machine>
      </li>
    </xsl:if>

  </xsl:template>


  <!-- Traitement des etats du diagramme -->
  <xsl:template match="UML:StateMachine.top">
    <xsl:for-each
      select="//UML:Pseudostate | //UML:SimpleState | //UML:FinalState">
      <xsl:variable name="stateName" select="@name"/>
      <xsl:if test="string-length($stateName)!=0">
        <li>
          <xsl:call-template name="get_state">
            <xsl:with-param name="sName" select="$stateName"/>
            <xsl:with-param name="sType" select="name(.)"/>
          </xsl:call-template>
        </li>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>


  <!-- traitement d'un etat particulier
       (en fonction de son nom 'sName' et de son type 'sType') -->
  <xsl:template name="get_state">
    <xsl:param name="sName"/>
    <xsl:param name="sType"/>
    <xsl:choose>
      <xsl:when test="$sType='UML:Pseudostate'">
        <!-- ajout d'un etat initial -->
        <Pseudostate>
          "<xsl:value-of select="$sName"/>"
        </Pseudostate>
      </xsl:when>
      <xsl:when test="$sType='UML:SimpleState'">
        <!-- ajout d'un etat ordinaire -->
        <Simplestate>
          "<xsl:value-of select="$sName"/>"
        </Simplestate>
      </xsl:when>
      <xsl:when test="$sType='UML:FinalState'">
        <!-- ajout d'un etat final -->
        <Finalstate>
          "<xsl:value-of select="$sName"/>"
        </Finalstate>
      </xsl:when>
    </xsl:choose>
  </xsl:template>



  <!-- Traitement des transitions du diagramme -->
  <xsl:template match="UML:StateMachine.transitions">
    <xsl:param name="statesNode"/>
    <xsl:apply-templates select="UML:Transition">
      <xsl:with-param name="statesNode" select="$statesNode"/>
    </xsl:apply-templates>
  </xsl:template>


  <!-- traitement d'une transition particuliere -->
  <xsl:template match="UML:Transition">
    <xsl:param name="statesNode"/>

    <!-- transitions non prises en compte
         si l'etat de depart ou d'arrivee est un super-etat -->
    <xsl:variable name="source" select="name(./UML:Transition.source/*)"/>
    <xsl:variable name="target" select="name(./UML:Transition.target/*)"/>
    <xsl:if
      test="($source='UML:Pseudostate'
          or $source='UML:SimpleState' or $source='UML:FinalState')
        and ($target='UML:Pseudostate'
          or $target='UML:SimpleState' or $target='UML:FinalState')">

      <li>
        <Transition>
          <tuple>
            <ti>
              <!-- nom de la transition -->
              "<xsl:value-of select="@name"/>"
            </ti>
            <ti>
              <!-- ajout de l'etat de depart -->
              <Source>
                <xsl:apply-templates select="UML:Transition.source">
                  <xsl:with-param name="statesNode" select="$statesNode"/>
                </xsl:apply-templates>
              </Source>
            </ti>
            <ti>
              <!-- ajout de l'etat d'arrivee -->
              <Target>
                <xsl:apply-templates select="UML:Transition.target">
                  <xsl:with-param name="statesNode" select="$statesNode"/>
                </xsl:apply-templates>
              </Target>
            </ti>
            <ti>
              <!-- ajout de l'evenement de la transition (declencheur) -->
              "<xsl:apply-templates select="UML:Transition.trigger"/>"
            </ti>
            <ti>
              <!-- ajout de la condition de declenchement de la transition -->
              "<xsl:apply-templates select="UML:Transition.guard"/>"
            </ti>
            <ti>
              <!-- ajout de l'operation accompagnant le declenchement -->
              "<xsl:apply-templates select="UML:Transition.effect"/>"
            </ti>
          </tuple>
        </Transition>
      </li>

    </xsl:if>
  </xsl:template>


  <!-- Traitement de l'etat de depart ou d'arrivee d'une transition -->
  <xsl:template match="UML:Transition.source | UML:Transition.target">
    <xsl:param name="statesNode"/>
    <xsl:variable
      name="stateIdref"
      select="UML:Pseudostate/@xmi.idref
        | UML:SimpleState/@xmi.idref | UML:FinalState/@xmi.idref"/>
    <xsl:for-each
      select="$statesNode//UML:Pseudostate
        | $statesNode//UML:SimpleState | $statesNode//UML:FinalState">
      <xsl:if test="@xmi.id=$stateIdref">
          <xsl:call-template name="get_state">
            <xsl:with-param name="sName" select="@name"/>
            <xsl:with-param name="sType" select="name(.)"/>
         </xsl:call-template>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>


  <!-- Traitement du declencheur  d'une transition -->
  <xsl:template match="UML:Transition.trigger">
    <xsl:variable name="eventIdref" select="./UML:CallEvent/@xmi.idref"/>
    <xsl:for-each
      select="/XMI/XMI.content/UML:Model
        /UML:Namespace.ownedElement/UML:CallEvent">
      <xsl:if test="@xmi.id=$eventIdref">
        <xsl:value-of select="@name"/>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>


  <!-- Traitement de la garde d'une transition -->
  <xsl:template match="UML:Transition.guard">
    <xsl:value-of select="./UML:Guard/@name"/>
  </xsl:template>


  <!-- Traitement de l'action associee a une transition -->
  <xsl:template match="UML:Transition.effect">
    <xsl:value-of select="./UML:CallAction/@name"/>
  </xsl:template>




<!-- #############################################################
              TRAITEMENT DES ASSOCIATIONS DU MODELE
(les classes-associations ne sont pas vues comme des associations)
############################################################## -->

  <xsl:template match="UML:Association" name="get_assoc">
    <xsl:apply-templates select="UML:Association.connection">
      <xsl:with-param name="roleType1" select="'???undefined???'"/>
      <xsl:with-param name="roleType2" select="'???undefined???'"/>
      <xsl:with-param name="assocPath" select="./UML:Association.connection"/>
    </xsl:apply-templates>
  </xsl:template>


  <!-- noeud UML:Association.connection
       (comporte deux appels recursifs) -->
  <xsl:template match="UML:Association.connection" name="assoc">
    <xsl:param name="roleType1"/>
    <xsl:param name="roleType2"/>
    <xsl:param name="assocPath"/>

    <xsl:choose>

      <!-- identification de la classe associee au premier role
           (premier appel du template) -->
      <xsl:when test="$roleType1='???undefined???'">
        <xsl:variable
          name="r1Idref"
          select="$assocPath/UML:AssociationEnd[position()=1]
            /UML:AssociationEnd.participant/UML:Class/@xmi.idref
            | $assocPath/UML:AssociationEnd[position()=1]
            /UML:AssociationEnd.participant/UML:AssociationClass/@xmi.idref"/>
        <xsl:for-each
          select="$assocPath/../..//UML:Class
            | $assocPath/../..//UML:AssociationClass">
          <xsl:if test="@xmi.id=$r1Idref">
            <xsl:call-template name="assoc">
              <xsl:with-param name="roleType1" select="@name"/>
              <xsl:with-param name="roleType2" select="'???undefined???'"/>
              <xsl:with-param name="assocPath" select="$assocPath"/>
            </xsl:call-template>
          </xsl:if>
        </xsl:for-each>
      </xsl:when>


      <!-- identification de la classe associee au second role
           (deuxieme appel du template = premier appel recursif) -->
      <xsl:when test="$roleType2='???undefined???'">
        <xsl:variable
          name="r2Idref"
          select="$assocPath/UML:AssociationEnd[position()=2]
            /UML:AssociationEnd.participant/UML:Class/@xmi.idref
            | $assocPath/UML:AssociationEnd[position()=2]
            /UML:AssociationEnd.participant/UML:AssociationClass/@xmi.idref"/>
        <xsl:for-each
          select="$assocPath/../..//UML:Class
          | $assocPath/../..//UML:AssociationClass">
          <xsl:if test="@xmi.id=$r2Idref">
            <xsl:call-template name="assoc">
              <xsl:with-param name="roleType1" select="$roleType1"/>
              <xsl:with-param name="roleType2" select="@name"/>
              <xsl:with-param name="assocPath" select="$assocPath"/>
            </xsl:call-template>
          </xsl:if>
        </xsl:for-each>
      </xsl:when>


      <!-- fin du traitement de l'association
           (troisieme appel du template = second appel recursif) -->
      <xsl:otherwise>
        <!-- associations non prises en compte
             si l'une des classes associees est anonyme -->
        <xsl:if
          test="string-length($roleType1)!=0 and string-length($roleType2)!=0">
          <xsl:variable name="assocName" select="string($assocPath/../@name)"/>
          <li>
            <Association>
              <tuple>
                <ti>
                  <!-- nom de l'association -->
                  <xsl:choose>
                    <!-- reconstitution du nom d'une association anonyme -->
                    <xsl:when test="string-length($assocName)=0">
                      "<xsl:value-of
                         select="concat(
                           'assoc_', $roleType1, '_', $roleType2)"/>"
                    </xsl:when>
                    <xsl:otherwise>
                      "<xsl:value-of select="$assocName"/>"
                    </xsl:otherwise>
                  </xsl:choose>
                </ti>
                <ti>
                  <!-- ajout des roles de l'association -->
                  <list>
                    <!-- choix d'un sens pour les roles
                         en fonction des possibilites de navigation -->
                    <xsl:choose>
                      <xsl:when
                        test="$assocPath/UML:AssociationEnd
                          /@isNavigable!='true'">
                        <xsl:apply-templates
                          select="$assocPath/UML:AssociationEnd[position()=1]">
                          <xsl:with-param name="roleType" select="$roleType1"/>
                        </xsl:apply-templates>
                        <xsl:apply-templates
                          select="$assocPath/UML:AssociationEnd[position()=2]">
                          <xsl:with-param name="roleType" select="$roleType2"/>
                        </xsl:apply-templates>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:apply-templates
                          select="$assocPath/UML:AssociationEnd[position()=2]">
                          <xsl:with-param name="roleType" select="$roleType2"/>
                        </xsl:apply-templates>
                        <xsl:apply-templates
                          select="$assocPath/UML:AssociationEnd[position()=1]">
                          <xsl:with-param name="roleType" select="$roleType1"/>
                        </xsl:apply-templates>
                      </xsl:otherwise>
                    </xsl:choose>
                  </list>
                </ti>
              </tuple>
            </Association>
          </li>
        </xsl:if>
      </xsl:otherwise>

    </xsl:choose>
  </xsl:template>


<!-- Traitement des roles d'une association -->
  <xsl:template match="UML:AssociationEnd">
    <xsl:param name="roleType"/>
    <li>
      <Role>
        <tuple>
          <ti>
            <!-- nom du role -->
            "<xsl:value-of select="@name"/>"
          </ti>
          <ti>
            <!-- ajout de l'agregation -->
            <xsl:variable name="aggType" select="@aggregation"/>
            <xsl:choose>
              <xsl:when test="$aggType='aggregate'">
                <Aggregate/>
              </xsl:when>
              <xsl:when test="$aggType='composite'">
                <Composite/>
              </xsl:when>
              <xsl:otherwise>
                <Ass_end/>
              </xsl:otherwise>
            </xsl:choose>
          </ti>
          <ti>
            <!-- ajout de la multiplicite -->
            <xsl:apply-templates select="UML:AssociationEnd.multiplicity"/>
          </ti>
          <ti>
            <!-- ajout de la classe associee au role -->
            <Role_type>
              "<xsl:value-of select="$roleType"/>"
            </Role_type>
          </ti>
        </tuple>
      </Role>
    </li>
  </xsl:template>


<!-- Traitement de la multiplicite d'un role -->
  <xsl:template match="UML:AssociationEnd.multiplicity">
    <xsl:apply-templates select="UML:Multiplicity"/>
  </xsl:template>



<!-- #######################
TRAITEMENT DES MULTIPLICITES
######################## -->

  <!-- noeud UML:Multiplicity -->
  <xsl:template match="UML:Multiplicity">
    <xsl:apply-templates select="UML:Multiplicity.range"/>
  </xsl:template>


  <!-- noeud UML:Multiplicity.range -->
  <xsl:template match="UML:Multiplicity.range">
    <xsl:apply-templates select="UML:MultiplicityRange"/>
  </xsl:template>


  <!-- noeud UML:MultiplicityRange -->
  <xsl:template match="UML:MultiplicityRange">
    <Multiplicity>
      <Range>
        <tuple>
          <ti>
            <!-- ajout de la borne inferieure -->
            <xsl:call-template name="conv_mult">
              <xsl:with-param name="mult" select="@lower"/>
            </xsl:call-template>
          </ti>
          <ti>
            <!-- ajout de la borne superieure -->
            <xsl:call-template name="conv_mult">
              <xsl:with-param name="mult" select="@upper"/>
            </xsl:call-template>
          </ti>
        </tuple>
      </Range>
    </Multiplicity>
  </xsl:template>


  <!-- conversion de -1 en * dans la multiplicite -->
  <xsl:template name="conv_mult">
    <xsl:param name="mult"/>
    <xsl:choose>
      <xsl:when test="$mult=-1">
        "*"
      </xsl:when>
      <xsl:otherwise>
        "<xsl:value-of select="$mult"/>"
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>



<!-- #################################
RECUPERATION DU CORPS D'UNE EXPRESSION
################################## -->

  <xsl:template match="UML:Expression">
    <xsl:value-of select="@body"/>
  </xsl:template>



<!-- ###########################################################
RECUPERATION DU TYPE D'UN ATTRIBUT OU D'UN PARAMETRE D'OPERATION
############################################################ -->

  <xsl:template match="UML:StructuralFeature.type | UML:Parameter.type">
    <xsl:param name="basePath"/>

    <xsl:variable
      name="typeIdref"
      select="./UML:DataType/@xmi.idref
        | ./UML:Class/@xmi.idref | ./UML:AssociationClass/@xmi.idref"/>

    <!-- recherche d'un type donnee
         (type de base: char, int, ... ou type complexe: String, etc...) -->
    <xsl:variable
      name="packagePath"
      select="$basePath/../UML:Package/UML:Namespace.ownedElement
        /UML:Package/UML:Namespace.ownedElement"/>
    <xsl:for-each
      select="$basePath/UML:Namespace.ownedElement/UML:DataType
        | $packagePath/UML:DataType
        | $packagePath/UML:Class | $packagePath/UML:AssociationClass">
      <xsl:if test="@xmi.id=$typeIdref">
        <xsl:variable name="xmiType" select="@name"/>
        <xsl:choose>
          <xsl:when test="$xmiType='boolean' or $xmiType='Boolean'">
            <Boolean/>
          </xsl:when>
          <xsl:when test="$xmiType='char' or $xmiType='Character'">
            <Char/>
          </xsl:when>
          <xsl:when test="$xmiType='int' or $xmiType='Integer'">
            <Integer/>
          </xsl:when>
          <xsl:when test="$xmiType='float' or $xmiType='Float'">
            <Real/>
          </xsl:when>
          <xsl:when test="$xmiType='double' or $xmiType='Double'">
            <Real/>
          </xsl:when>
          <xsl:when test="$xmiType='String'">
            <Striing/>
          </xsl:when>
          <xsl:otherwise>
            <Undefined/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
    </xsl:for-each>

    <!-- recherche d'un type objet (une classe du diagramme) -->
    <xsl:for-each
      select="$basePath/../UML:Class | $basePath/../UML:AssociationClass">
      <xsl:if test="@xmi.id=$typeIdref">
        <Att_type>
          "<xsl:value-of select="@name"/>"
        </Att_type>
      </xsl:if>
    </xsl:for-each>

  </xsl:template>



</xsl:stylesheet>

