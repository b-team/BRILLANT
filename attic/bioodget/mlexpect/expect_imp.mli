(*Copyright (C) 1998 Ian Zimmerman <itz@rahul.net>*)
(*$Id: expect_imp.mli,v 1.1 2001/04/27 11:00:11 lefessan Exp $*)

(*Module Expect: the expect function and its friends, types, flags etc.*)

val trace: bool ref
(* If !TRACE is set, the expect function will report details of the*)
(* pattern matches on stderr in the case of non-precompiled*)
(* patterns.  The reports look like PATTERN ~ STRING -> RESULT where*)
(* RESULT is true or false.*)    

val timeout: float ref

exception Timeout
(* If none of the alternatives passed to expect match within !TIMEOUT*)
(* seconds, this exception is raised.*)

type expect_condition =
    Eof
  | Regexp of string
  | Exact of string

type 't expect_clause = 't * Spawn_imp.id * expect_condition

val expect: 't expect_clause list -> 't expect_clause
(* expect CLAUSES repeatedly tries to match the patterns specified by*)
(* CLAUSES with the output on the connections in CLAUSES.  The first*)
(* CLAUSE to match is returned as result.  Each CLAUSE should look*)
(* like (TAG, SPAWN_ID, PATTERN) where TAG is an enumeration value*)
(* used for matching the result and PATTERN is one of three types: Eof*)
(* to match the end-of-file condition on SPAWN_ID, Regexp(REGEXP_STRING)*)
(* to match the output of SPAWN_ID with a regexp in the normal string*)
(* notation, or Exact(STRING) to match with a fixed string.*)

