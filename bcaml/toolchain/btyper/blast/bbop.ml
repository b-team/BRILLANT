
type kind = 
    StateVar | LocVar | FieldName | Set | Cst 
  | OpName | MachineName | UnknownId

type bop2 = 
  | Ou 
  | And 
  | Implies
  | Equiv
  | Equal
  | NotEqual
      
  | Less 
  | LessEqual 
  | Greater 
  | GreaterEqual 
      
  | SubSet
  | NotSubSet
  | StrictSubSet
  | NotStrictSubSet

  | In
  | NotIn

let bop2_name = function
    Ou -> "Or"
  | And -> "And"
  | Implies -> "Implies"
  | Equiv -> "Equiv"
  | Equal -> "Equal"
  | NotEqual -> "NotEqual"
  | Less -> "Less"
  | LessEqual -> "LessEqual"
  | Greater -> "Greater"
  | GreaterEqual -> "GreaterEqual"
  | SubSet -> "SubSet"
  | NotSubSet -> "NotsubSet"
  | StrictSubSet -> "StrictSubSet"
  | NotStrictSubSet -> "NotStrictSubSet"
  | In -> "In"
  | NotIn -> "NotIn"
        
let bop2_ascii = function
    Ou -> "or"
  | And -> "&"
  | Implies -> "=>"
  | Equiv -> "<=>"
  | Equal -> "="
  | NotEqual -> "/="
  | Less -> "<"
  | LessEqual -> "<="
  | Greater -> ">"
  | GreaterEqual -> ">="
  | SubSet -> "<:"
  | NotSubSet ->  "/<:"
  | StrictSubSet -> "<<:"
  | NotStrictSubSet -> "/<<:"
  | In -> ":"
  | NotIn -> "/:"

type op1 =
  | Closure 
  | ClosureOne 
  | Cod 
  | Perm
  | Min
  | Max
  | Card
  | Pow
  | PowOne
  | Fin
  | FinOne
  | Identity
  | Dom
  | Ran
  | Union
  | Inter
  | Seq
  | SeqOne
  | ISeq
  | ISeqOne
  | Size
  | First
  | Last
  | Front
  | Tail
  | Rev
  | Conc
  | RelFnc
  | FncRel
  | Bool
  | Pred
  | Succ
  | Tilde
  | UMinus

type op2 = 
    Minus 
  | NatSetRange
  | Puissance
  | Mul
  | Div
  | Mod
  | Plus 
  | SetMinus
  | SetMul
(* FunName2 *)
      
  | PartialFunc
  | TotalFunc
  | PartialInj
  | TotalInj
  | PartialSurj
  | TotalSurj
  | PartialBij
  | TotalBij
      
  | OverRideFwd
  | OverRideBck	
      
  | AppendSeq
  | PrependSeq
  | PrefixSeq
  | SuffixSeq
  | ConcatSeq
      
  | DomRestrict
  | RanRestrict
  | DomSubstract
  | RanSubstract

  | InterSets
  | UnionSets

  | RelProd
  | ParallelComp
  | Image

  | Prj1
  | Prj2
      ;;

(*
let affixe op =
   match op with
	Card | Front | Tail | Rev | RelFnc | FncRel |
	Dom | Ran | Pow | PowOne | Fin | FinOne | Min | Max
	-> Affix.Prefix
   | _ -> Affix.Infix
;;
*)

let op1_ascii = function
  | Closure -> "closure"
  | ClosureOne -> "closure1"
  | Cod -> "cod"
  | Perm -> "perm"
  | Min -> "min"
  | Max -> "max"
  | Card -> "card"
  | Pow -> "POW"
  | PowOne -> "POW1"
  | Fin -> "FIN"
  | FinOne -> "FIN1"
  | Identity -> "id"
  | Dom -> "dom"
  | Ran -> "ran"
  | Union -> "union"
  | Inter -> "inter"
  | Seq -> "seq"
  | SeqOne -> "seq1"
  | ISeq -> "iseq"
  | ISeqOne -> "iseq1"
  | Size -> "size"
  | First -> "first"
  | Last -> "last"
  | Front -> "front"
  | Tail -> "tail"
  | Rev -> "rev"
  | Conc -> "conc"
        
  | RelFnc -> "rel"
  | FncRel -> "fnc"
  | Bool -> "bool"
  | Pred -> "pred"
  | Succ -> "succ"
  | Tilde -> "~"
  | UMinus -> "-"

let op2_ascii = function
    Minus -> "-"
  | SetMinus -> "-"
  | Plus -> "+"
  | Puissance -> "**"
  | Mul -> "*"
  | SetMul -> "*"
  | Div -> "/"
  | Mod -> "mod"
  | NatSetRange -> ".."
(* FunName2 *)
  | PartialFunc -> "+->"
  | TotalFunc -> "-->"
  | PartialInj -> ">+>"
  | TotalInj -> ">->"
  | TotalSurj -> "-->>"
  | PartialSurj -> "+->>"
  | PartialBij -> ">+>>"
  | TotalBij -> ">->>"
        
  | OverRideFwd -> "<+"
  | OverRideBck -> "+>"  (* non d�fini dans le B-book ! *)
        
  | AppendSeq -> "<-"
  | PrependSeq -> "->"
  | PrefixSeq -> "/|\\"
  | SuffixSeq -> "\\|/"
  | ConcatSeq -> "^"
        
        
  | DomRestrict -> "<|"
  | RanRestrict -> "|>"
  | DomSubstract -> "<<|"
  | RanSubstract -> "|>>"
        
  | InterSets -> "/\\"
  | UnionSets -> "\\/"

  | RelProd -> "><"
  | ParallelComp -> "||"
  | Image -> "Im"

  | Prj1 -> "prj1"
  | Prj2 -> "prj2"
        ;;
        
let op1_name = function
  | Closure -> "Closure"
  | ClosureOne -> "Closure1"
  | Cod -> "Cod"
  | Perm -> "Perm"
  | Min -> "Min"
  | Max -> "Max"
  | Card -> "Card"
  | Pow -> "Pow"
  | PowOne -> "PowOne"
  | Fin -> "Fin"
  | FinOne -> "FinOne"
  | Identity -> "Identity"
  | Dom -> "Dom"
  | Ran -> "Ran"
  | Union -> "Union"
  | Inter -> "Inter"
  | Seq -> "Seq"
  | SeqOne -> "SeqOne"
  | ISeq -> "ISeq"
  | ISeqOne -> "ISeqOne"
  | Size -> "Size"
  | First -> "First"
  | Last -> "Last"
  | Front -> "Front"
  | Tail -> "Tail"
  | Rev -> "Rev"
  | Conc -> "Conc"
        
  | RelFnc -> "RelFnc"
  | FncRel -> "FncRel"
  | Bool -> "Bool"
  | Pred -> "Pred"
  | Succ -> "Succ"
  | Tilde -> "Tilde"
  | UMinus -> "UMinus"

let op2_name = function
    Minus -> "Minus"
  | SetMinus -> "SetMinus"
  | NatSetRange -> "NatSetRange"
  | Plus -> "Plus"
  | Puissance -> "Puissance"
  | Mul -> "Mul"
  | SetMul -> "SetMul"
  | Div -> "Div"
  | Mod -> "Modulo"
(* FunName2 *)
  | PartialFunc -> "PartialFunc"
  | TotalFunc -> "TotalFunc"
  | PartialInj -> "PartialInj"
  | TotalInj -> "TotalInj"
  | PartialSurj -> "PartialSurj"
  | TotalSurj -> "TotalSurj"
  | PartialBij -> "PartialBij"
  | TotalBij -> "TotalBij"
        
  | OverRideFwd -> "OverRideFwd"
  | OverRideBck -> "OverRideBck"
        
  | AppendSeq -> "AppendSeq"
  | PrependSeq -> "PrependSeq"
  | PrefixSeq -> "PrefixSeq"
  | SuffixSeq -> "SuffixSeq"
  | ConcatSeq -> "ConcatSeq"
        
  | DomRestrict -> "DomRestrict"
  | RanRestrict -> "RanRestrict"
  | DomSubstract -> "DomSubstract"
  | RanSubstract -> "RanSubstract"

  | InterSets ->  "InterSets"
  | UnionSets ->  "UnionSets"

  | RelProd -> "RelProd"
  | ParallelComp -> "ParallelComp"
  | Image -> "Image"

  | Prj1 -> "Prj1"
  | Prj2 -> "Prj2"

let kind_name = function 
    StateVar -> "StatVar"
  | LocVar -> "LocVar"
  | Set -> "Set"
  | Cst -> "Cst"
  | OpName -> "OpName"
  | MachineName -> "MachineName"
  | FieldName -> "FieldName"
  | UnknownId -> "UnknownId"

(*MG Xml inactiv�
let p_xml_bop2 s = [Xml.XBalise ((bop2_name s),[])]

let p_xml_op1 s = [Xml.XBalise ((op1_name s),[])]

let p_xml_op2 s = [Xml.XBalise ((op2_name s),[])]

let p_xml_kind s = [Xml.XBalise ((kind_name s),[])]

let xmlparse_bop2 = function
  | [] -> raise (Xml.InvalidXml "Bbop.bop2")
  | (s::l) -> Ou,l (* a completer *)

let xmlparse_op1 = function
  | [] -> raise (Xml.InvalidXml "Bbop.op1")
  | (s::l) -> Card,l (* a completer *)

let xmlparse_op2 = function
  | [] -> raise (Xml.InvalidXml "Bbop.op2")
  | (s::l) -> Minus,l (* a completer *)

let xmlparse_kind = function
  | [] -> raise (Xml.InvalidXml "Bbop.kind")
  | (s::l) -> StateVar,l (* a completer *)

*)
let bop22tex _ op = "\\@"^(bop2_name op)

let op12tex _ op = "\\@"^(op1_name op)

let op22tex _ op = "\\@"^(op2_name op)

let kind2tex _ op = "\\@"^(kind_name op)

