(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

(* Library of misc. theorems, at the moment to test the integrability of libraries *)

Require Import ZArith.
Require Import Bchapter2.
Require Import Constructive_sets.
Require Import Classical_sets.
Require Import Bchapter3.

Open Local Scope Z_scope.

Theorem all_bN_positive: forall x, In BN x -> x >= 0.
Proof.
intros.
do 3 (red in H); assumption.
Qed.

(* Trying to see about empty datatypes *)

Theorem empty_datatype: forall (x y: Z), x > y -> Included (interval x y) (Empty_set _).
Proof.
intros.
unfold Included.
intros.
elim H0; intros.
absurd (x <= y).
intuition.
intuition.
Qed.


Theorem empty_set_datatype: forall x y, x > y -> (interval x y)= (Empty_set _).
Proof.
intros.
apply Extensionality_Ensembles.
split.
apply empty_datatype; intuition.
apply Included_Empty.
Qed.

(**
  An example of proof obligations resolutions for the following well-known "toy" B
  project (displayed here with a mix of Unicode and ASCII characters):
<<
MACHINE LittleExample
VARIABLES y
INVARIANT y ∈  FIN(ℕ1)
INITIALISATION y := ∅
OPERATIONS
  read(n) =
     PRE n  ∈ ℕ1
     THEN y :=y ∪{n}
     END;
m <-- maximum =
     PRE  y /= ∅
     THEN m :=max(y)
     END

REFINEMENT LittleExample1
REFINES LittleExample
VARIABLES z
INVARIANT z = max(y ∪ {0})
INITIALISATION z :=0
OPERATIONS
  read(n) =
     PRE n  ∈ ℕ1
     THEN z :=max({z,n})
     END;
m <-- maximum =
     PRE z /= 0
     THEN m :=z
     END
>>

Granted, the proof is not especially top-notch but it shows it is feasible and it gives
an idea of what theorems or tactics might be needed.
  *)

Theorem LittleExample_init: In (Power_finite BN1) (Empty_set _).
Proof.
split.
left.
red; intros; contradiction.
Qed.

Theorem LittleExample_Op_read: forall n y,
  In (Power_finite BN1) y /\ In BN1 n -> In (Power_finite BN1) (Union y (Singleton n)).
Proof.
intros.
rewrite commutativity_1; apply augmented_set_in_finite_sets.
constructor; intuition.
Qed.

Theorem LittleExample_Op_maximum: forall y,
  In (Power_finite BN1) y /\ y <> (Empty_set _) -> In (Power_finite BN1) y.
Proof.
intuition.
Qed.

Theorem LittleExample1_init: ~~(0 = Bmax (Union (Empty_set _) (Singleton 0))).
Proof.
intros H; apply H.
rewrite commutativity_1; rewrite neutral_element_1.
symmetry; apply Bmax_singleton.
Qed.

Theorem LittleExample1_Op_read: forall y z n,
  In (Power_finite BN1) y /\ z = Bmax (Union y (Singleton 0))
  /\ In BN1 n
->
  In BN1 n /\
  ~~((Bmax ({= z, n =})) = Bmax (Union (Union y (Singleton n)) (Singleton 0))).
Proof.
intros.
split; [ intuition | ].
intros H0; apply H0.
decompose [and] H.
rewrite associativity_1.
rewrite commutativity_1 with Z (Singleton n) (Singleton 0).
rewrite <- associativity_1.
rewrite Bmax_union with (Union y (Singleton 0)) (Singleton n).
rewrite <- H3.
rewrite Bmax_singleton; reflexivity.
intros H5.
assert (Same_set Z (Union y (Singleton 0)) (Empty_set Z)).
rewrite H5; intuition.
destruct H2.
assert (In (Union y (Singleton 0)) 0).
right; intuition.
specialize (H2 0 H7); contradiction.
intros H5.
assert (Same_set Z (Singleton n) (Empty_set Z)).
rewrite H5; intuition.
destruct H2.
assert (In (Singleton n) n).
intuition.
specialize (H2 n H7); contradiction.
rewrite neutral_element_2.
rewrite commutativity_1; apply augmented_set_in_finite_sets.
constructor.
constructor.
destruct H1; constructor.
assumption.
red; intros; constructor.
red; intros.
destruct H2.
destruct H1.
assert (Included BN1 BN).
red; intros.
do 3 (red in H6).
do 3 red; intuition.
apply H6; apply H5; assumption.
destruct H2; do 3 red; intuition.
rewrite neutral_element_2.
constructor.
replace (Singleton n) with (Add Z (Empty_set Z) n).
right.
left.
intros H5; contradiction.
unfold Add; rewrite commutativity_1; apply neutral_element_1.
red; intros; constructor.
red; intros x H2; destruct H2.
do 3 (red in H4).
do 3 red; intuition.
Qed.


Theorem LittleExample1_Op_maximum: forall y z,
  In (Power_finite BN1) y /\ z = Bmax (Union y (Singleton 0))
  /\ y <> (Empty_set _)
-> z <> 0 /\
  ~~(z = Bmax (Union y (Singleton 0)) /\ Bmax y = z).
Proof.
intros.
decompose [and] H.
split.
assert (Inhabited Z y).
apply not_empty_Inhabited; assumption.
destruct H1.
assert (0 <> z).
apply Zlt_not_eq.
assert (In y (Bmax y)).
apply Bmax_in_its_argument.
assumption.
destruct H0.
rewrite neutral_element_2.
constructor.
assumption.
red; intros; constructor.
assert (Included BN1 BN).
red; intros.
do 3 (red in H5).
do 3 red; intuition.
red; intros; apply H5; apply H4; intuition.
destruct H0.
specialize (H5 _ H4).
do 3 (red in H5).
apply Zlt_le_trans with (Bmax y).
intuition.
rewrite Bmax_union in H2.
rewrite Bmax_doubleton1 in H2.
rewrite H2; intuition.
rewrite Bmax_singleton; intuition.
assumption.
intros H6.
assert (Same_set Z (Singleton 0) (Empty_set Z)).
rewrite H6; intuition.
destruct H7.
assert (In (Singleton 0) 0).
intuition.
specialize (H7 0 H9); contradiction.
rewrite neutral_element_2.
constructor; [ assumption | red; intros; constructor ].
destruct H.
destruct H.
red; intros.
specialize (H7 x0 H8).
do 3 (red in H7); do 3 red; intuition.
rewrite neutral_element_2.
constructor; [ | red; intros; constructor ].
replace (Singleton 0) with (Add Z (Empty_set Z) 0).
right.
left.
intros H6; contradiction.
unfold Add; rewrite commutativity_1; apply neutral_element_1.
red; intros x0 H6; destruct H6.
do 3 red; intuition.
intuition.
intros H4; apply H4.
split; [ assumption | ].
rewrite H2.
rewrite Bmax_union.
rewrite Bmax_singleton.
rewrite Bmax_doubleton1.
reflexivity.
destruct H0.
assert (Inhabited Z y).
apply not_empty_Inhabited; assumption.
destruct H5.
apply Zge_trans with x.
apply Bmax_is_maximal.
rewrite neutral_element_2.
constructor; [ assumption | red; intros; constructor ].
red; intros.
generalize (H1 x0 H6) ;intros H7.
do 3 (red in H7); do 3 red; intuition.
assumption.
specialize (H1 x H5); do 3 (red in H1); intuition.
assumption.
intros H1.
assert (Same_set Z (Singleton 0) (Empty_set Z)).
rewrite H1; intuition.
destruct H5.
assert (In (Singleton 0) 0).
intuition.
specialize (H5 0 H7); contradiction.
destruct H0.
rewrite neutral_element_2.
constructor; [ assumption | red; intros; constructor ].
red; intros x H5; specialize (H1 x H5); do 3 (red in H1); do 3 red; intuition.
rewrite neutral_element_2.
constructor; [ | red; intros; constructor ].
replace (Singleton 0) with (Add Z (Empty_set Z) 0).
right.
left.
intros H5; contradiction.
unfold Add; rewrite commutativity_1; apply neutral_element_1.
red; intros x H1; destruct H1; do 3 red; intuition.
Qed.

(** Another example, a stack:
<<
MACHINE stack(stack_size)
DEFINITIONS empty == stack_top = 0
CONSTRAINTS
  stack_size : NAT & stack_size >= 1 & stack_size <= MAXINT
VISIBLE_VARIABLES
  the_stack, stack_top
INVARIANT
  the_stack : (1..stack_size)-->NAT &
  stack_top : NAT & stack_top >= 0 & stack_top <= stack_size
INITIALISATION
  the_stack :: (1..stack_size) --> NAT || stack_top := 0
OPERATIONS
  flag <-- is_empty = flag := bool(empty);

  push(addval) =
  PRE stack_top < stack_size & addval : NAT
  THEN
  stack_top := stack_top + 1 ||
  the_stack(stack_top + 1) := addval
  END;

  pop =
  PRE not (empty)
  THEN
  stack_top := stack_top -1
  END;

  result <-- top =
  PRE not (empty)
  THEN
  result := the_stack(stack_top)
  END
END

IMPLEMENTATION stack_1(stack_size)
DEFINITIONS empty == stack_top = 0
REFINES stack
INITIALISATION
  the_stack := (1..stack_size)*{0};
  stack_top := 0
OPERATIONS
  flag <-- is_empty = flag := bool(empty);

  push(addval) =
  BEGIN
  stack_top := stack_top + 1 ;
  the_stack(stack_top) := addval
  END;

  pop =
  BEGIN
  stack_top := stack_top -1
  END;
  result <-- top =
  BEGIN
  result := the_stack(stack_top )
  END
>>
  *)

Theorem stack_init: forall stack_size,
  (In BN stack_size /\ stack_size >= 1 /\ stack_size <= MAXINT) ->
  (forall the_stack', In (total_function (interval 1 stack_size) BN) the_stack' ->
  In (total_function (interval 1 stack_size) BN) the_stack' /\
  In BN 0 /\ 0 >= 0 /\ 0 <= stack_size).
Proof.
intros.
intuition.
do 3 red; intuition.
Qed.

Theorem stack_Op_is_empty: forall stack_size the_stack stack_top,
  In BN stack_size /\ stack_size >= 1 /\ stack_size <= MAXINT
/\ In (total_function (interval 1 stack_size) BN) the_stack
/\ In BN stack_top /\ stack_top >= 0 /\ stack_top <= stack_size
->
In (total_function (interval 1 stack_size) BN) the_stack
/\ In BN stack_top /\ stack_top >= 0 /\ stack_top <= stack_size.
Proof.
intuition.
Qed.

Theorem stack_Op_push: forall stack_size the_stack stack_top addval,
  In BN stack_size /\ stack_size >= 1 /\ stack_size <= MAXINT
/\ In (total_function (interval 1 stack_size) BN) the_stack
/\ In BN stack_top /\ stack_top >= 0 /\ stack_top <= stack_size
/\ stack_top < stack_size /\ In BN addval
->
  In (total_function (interval 1 stack_size) BN) (override the_stack (Singleton (stack_top+1, addval)))
/\ In BN (stack_top + 1) /\ (stack_top + 1) >= 0 /\ (stack_top + 1) <= stack_size.
Proof.
intros.
decompose [and] H.
split.
apply Membership_laws_26.
split; [ assumption | ].
apply Membership_laws_50.
split; [ | assumption ].
do 3 red; intuition.
split.
do 3 red; intuition.
intuition.
Qed.

Theorem stack_Op_pop: forall stack_size the_stack stack_top,
  In BN stack_size /\ stack_size >= 1 /\ stack_size <= MAXINT
/\ In (total_function (interval 1 stack_size) BN) the_stack
/\ In BN stack_top /\ stack_top >= 0 /\ stack_top <= stack_size
/\ ~(stack_top = 0)
->In (total_function (interval 1 stack_size) BN) the_stack
/\ In BN (stack_top-1) /\ (stack_top-1) >= 0 /\ (stack_top-1) <= stack_size.
Proof.
intros.
decompose [and] H.
split; [ intuition | ].
split; [ | intuition ].
do 3 red; intuition.
Qed.


Theorem stack_Op_result: forall stack_size the_stack stack_top,
  In BN stack_size /\ stack_size >= 1 /\ stack_size <= MAXINT
/\ In (total_function (interval 1 stack_size) BN) the_stack
/\ In BN stack_top /\ stack_top >= 0 /\ stack_top <= stack_size
/\ ~(stack_top = 0)
->In (total_function (interval 1 stack_size) BN) the_stack
/\ In BN stack_top /\ stack_top >= 0 /\ stack_top <= stack_size.
Proof.
intuition.
Qed.

(** Here I assumed the unmentioned invariant is equivalent to TRUE *)
Theorem stack_1_init: forall stack_size,
  In BN stack_size /\ stack_size >= 1 /\ stack_size <= MAXINT
->
  ~
  (forall the_stack', In (total_function (interval 1 stack_size) BN) the_stack' ->
  (~True)).
Proof.
intros.
apply ex_not_not_all.
exists (times (interval 1 stack_size) (Singleton 0)).
intros H0; apply H0.
constructor.
constructor.
do 2 constructor.
red; intros x H1; induction H1.
constructor; [ assumption | ].
destruct H2; do 3 red; intuition.
intros.
inversion H1; inversion H2.
destruct H6; destruct H10; reflexivity.
apply Equality_laws_dom_18.
intros H1.
assert (Same_set Z (Singleton 0) (Empty_set Z)); [ rewrite H1; intuition | ].
destruct H2.
assert (In (Singleton 0) 0); [ intuition | ].
specialize (H2 0 H4); contradiction.
tauto.
Qed.

(*
  In BN stack_size /\ stack_size >= 1 /\ stack_size <= MAXINT
/\ In (total_function (interval 1 stack_size) BN) the_stack
/\ In BN stack_top /\ stack_top >= 0 /\ stack_top <= stack_size
/\ True
/\
->
...
/\
[Vn']
~[Vn-1]
(~(True /\ u = u'))
*)

Theorem stack_1_Op_is_empty: forall stack_size the_stack stack_top,
  In BN stack_size /\ stack_size >= 1 /\ stack_size <= MAXINT
/\ In (total_function (interval 1 stack_size) BN) the_stack
/\ In BN stack_top /\ stack_top >= 0 /\ stack_top <= stack_size
/\ True (* absent In *)
/\ True (* absent Q1 *)
->
True (* absent Qn *)
/\ (
  stack_top = 0 ->
  ~(stack_top = 0 -> (~(True /\ true = true)) /\ ~(stack_top = 0) -> (~(True /\ false = true)))
/\
  ~(stack_top = 0) ->
  ~(stack_top = 0 -> (~(True /\ true = false)) /\ ~(stack_top = 0) -> (~(True /\ false = false)))
).
Proof.
intuition.
Qed.

Theorem stack_1_Op_push: forall stack_size the_stack stack_top addval,
  In BN stack_size /\ stack_size >= 1 /\ stack_size <= MAXINT
/\ In (total_function (interval 1 stack_size) BN) the_stack
/\ In BN stack_top /\ stack_top >= 0 /\ stack_top <= stack_size
/\ True
/\ stack_top < stack_size /\ In BN addval
->
True
/\ ~~True.
Proof.
intros.
intuition.
Qed.

Theorem stack_1_Op_pop: forall stack_size the_stack stack_top,
  In BN stack_size /\ stack_size >= 1 /\ stack_size <= MAXINT
/\ In (total_function (interval 1 stack_size) BN) the_stack
/\ In BN stack_top /\ stack_top >= 0 /\ stack_top <= stack_size
/\ True
/\ ~(stack_top = 0)
->
True
/\ ~~True.
Proof.
intros.
intuition.
Qed.

(**
  This PO, although simple, illustrates the modification brought by our definition
  of functional application. Here the domain and codomain of H are exactly the ones
  we need, but it remains to see if typechecking and proof obligations generation will
  allow us to be as precise. For instance, (In (partial_function BZ BZ) the_stack) could
  have been generated. It would have been correct, but a bit more difficult to prove. Tactics
  for functional application would help in this context.
  *)
Theorem stack_1_Op_result: forall stack_size the_stack stack_top,
  In BN stack_size /\ stack_size >= 1 /\ stack_size <= MAXINT
/\ In (total_function (interval 1 stack_size) BN) the_stack
/\ In BN stack_top /\ stack_top >= 0 /\ stack_top <= stack_size
/\ True
/\ ~(stack_top = 0)
->
exists H:(In (partial_function (interval 1 stack_size) BN) the_stack /\ In (domain the_stack) stack_top),
( True
/\ ~(~(True /\ app the_stack stack_top H = app the_stack stack_top H))).
Proof.
intros.
assert (In (partial_function (interval 1 stack_size) BN) the_stack /\ In (domain the_stack) stack_top).
decompose [and] H.
destruct H3.
split; [ assumption | ].
rewrite H8; do 3 red; intuition.
exists H0.
intuition.
Qed.
