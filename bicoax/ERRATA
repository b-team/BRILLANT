This file gathers the BBook errors or unclear things we are aware of:

- overriding, BBook: section 2.4.2, p.80, array 1, row 2

  Replace "p" with "r" or replace all "r" with "p"

- image, BBook: section 2.4.2, p.80, array 2, row 1

  replace "x" with "a" and "y" with "b", or "a" with "x" and "b" with "y" 

- property 2.5.1, section 2.5.1, p.87

  The right to left implication is false.
  Counterexample:
  Let f = {0|-1, 0|-2, 1|-2} and s = {1} and t = {0, 1, 2, ...}.
  The hypothesis is true:
  1 ∈ s. {1}◁f ∈ {1} → t ?
  dom({1}◁f) = {1} hence it is total.
  ({1}◁f)~; {1}◁f = {2|-1};{1|-2} = {2|-2} ⊆ (id t) hence it is functional
  {1}◁f ∈ {1} ↔ t ? i.e.
  {1}◁f ⊆ {1}×t ? {1|-2} ⊆ {1|-0, 1|-1, 1|-2,...} ? Yes
  Hence we proved that the hypothesis is true. Now, do we have:
  f ∈ s→ t ?
  dom(f) = {0,1} ≠ {1} = s. First problem.
  f~;f = {1|-0, 2|-0, 2|-1};f = {1|-1, 1|-2, 2|-1, 2|-2} ⊈ (id t). Second problem.
  f ∈ s↔ t ? i.e.
  {0|-1, 0|-2, 1|-2} ⊆ {1|-0, 1|-1,...} ? No, third problem.

  This error was not seen in the BBook because the proof forgets the
  (f ∈ s↔ t) part of the definition of a partial function. This has
  consequences later on as this property is used.

- direct product of total surjections, section 2.6.1, p.96, array 1, row 5

  this theorem is false.

  Counterexample:

  f={(1,a), (2,b), (3, c), (4,c)}
  g={{(1,alpha), (2,alpha), (3, beta), (4,gamma)}
  f is a total surjection of 1..4 -> a..c
  g is a total surjection of 1..4 -> alpha..gamma
  Their direct product:
  {1 |-> (a,alpha), 2 |-> (b,alpha), 3 |-> (c,beta), 4 |-> (c,gamma) }
  is not a surjection over a..c >< alpha..gamma.
  Indeed, for instance the couple (a,gamma) is in the aforementioned product set,
  but has no antecedent in 1..4.

  This error looks like a copy/paste mistake.

- direct product of total bijections, section 2.6.1, p.96, array 1, row 6

  error: this theorem is false.
 
  See the point above, the counterexample would be similar
  This error looks like a copy/paste mistake

- equality law for right composition with a singleton, section 2.6.4, p.104, array 1, row 14

  replace "x ∈ s" with "x ∈ t"

- equality law for image by a domain subtraction, section 2.6.4, p.111, array 1, row 6

  replace "r[u \ v]" with "r[v \ u]"

- equality law for evaluation of what is actually prj2, section 2.6.4, p.115, array 1, row 12

  replace "prj1" with "prj2"

- Peano 4, section 3.5.2, p.148, array 1, row 4

  a universal quantification for m is missing

- Theorem 3.5.3, section 3.5.6, p.160

  This theorem is true, but as a consequence of the totality of
  "f". It can not be used alone for proving that f is total by way of
  property 2.5.1 as this property is false. Hence for this theorem we
  went the full way for proving the totality of "f".

- definition of plus(m), section 3.5.7, p.161, array 1, row 1

  This definition is different. Let Bigs be the set of finite subsets of BIG. By definition,
  succ ∈ Bigs → Bigs (and *not* ℕ → ℕ). This means that the "g" (see section 3.5.6) is a
  total function Bigs → Bigs. We have "a" (i.e. m in this case) ∈ ℕ but also a ∈ Bigs.
  Hence the built "f" function is: f ∈ ℕ → Bigs. Hence plus(m) ∈ ℕ → Bigs, hence
  plus ∈ ℕ →(ℕ → Bigs), at best, which contradicts the desired property for "plus".

  There are two possible fixes for this:
  - Assess that the codomain of "f" is actually a subset of the definition set of "g", but I
    think the section 3.5.6 is too general for that. An alternative would be to realize this
    proof for plus only and from there it should be okay.
  - Build "plus" upon "ℕ ◁ succ", which is what we did here, hence we can reuse directly
    the results of section 3.5.6.
