<!-- BXML vers BHTML (A. Doniec) -->

<xsl:stylesheet version="1.0"
		xmlns:xsl = "http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/TR/REC-html40">

<xsl:output method = "html"
	    omit-xml-declaration = "yes"
	    indent = "yes"
	    doctype-public = "-//w3c//dtd html 4.0 transitional//en"/>

<xsl:template match = "AMN">
  <HTML>
  <HEAD>
  <TITLE></TITLE>
  <META NAME = "Author" CONTENT = "Mariano Georges"/>
  <META NAME = "Copyright" CONTENT = "INRETS-ESTAS 2001"/>
  <META NAME = "E-mail" CONTENT = "georges.mariano@inrets.fr"/>
  <META NAME = "Keywords" CONTENT = "m�thode B methode formelle"/>
  </HEAD>
  <BODY BGCOLOR = "white" VLINK = "blue" LANG = "EN">
  <BR/>
  <xsl:apply-templates/>
  <BR/>
  <HR/>
  Derni�re modification:
  <script language="JavaScript">
  document.writeln(document.lastModified)
  </script>
  -- Contact:
  <A HREF="mailto:georges.mariano@inrets.fr">
  georges.mariano@inrets.fr
  </A>
  <HR/>
  </BODY>
</HTML>
</xsl:template>

<xsl:template match = "MACHINE | REFINEMENT | IMPLEMENTATION | EmptyTree">
  <xsl:choose>
    <xsl:when test = "self::MACHINE">
    <BR/><B><BIG>MACHINE</BIG></B><BR/>
    </xsl:when>
    <xsl:when test = "self::REFINEMENT">
    <BR/><B><BIG>REFINEMENT</BIG></B><BR/>
    </xsl:when>
    <xsl:when test = "self::IMPLEMENTATION">
    <BR/><B><BIG>IMPLEMENTATION</BIG></B><BR/>
    </xsl:when>
    <xsl:otherwise>
    <BR/><B><BIG>ARBRE VIDE</BIG></B><BR/>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:apply-templates select = "Head"/>
  <xsl:apply-templates select = "ClausesList"/>
  <BR/><B><BIG>END</BIG></B><BR/>
</xsl:template>

<xsl:template match="Head">
  <BLOCKQUOTE>
  <xsl:value-of select="id"/>
  <xsl:if test="child::HeadPar and string(child::HeadPar)">
    <xsl:apply-templates select="HeadPar"/>
  </xsl:if>
  </BLOCKQUOTE>
</xsl:template>

<xsl:template match="HeadPar">
  <xsl:text> ( </xsl:text>
    <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position() != last()">
      <xsl:text> , </xsl:text>
    </xsl:if>
    </xsl:for-each>
  <xsl:text> ) </xsl:text>
</xsl:template>
  
<xsl:template match="IdentListPar">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
  </xsl:for-each>
</xsl:template>
  
<xsl:template match="IdentTypedListPar">
  <xsl:apply-templates select="IdentTypedPar"/>
</xsl:template>
  
<xsl:template match="IdentTypedPar">
  <xsl:apply-templates select="id"/>
  <xsl:apply-templates select="Type"/>
</xsl:template>

<xsl:template match="Type">
  <xsl:choose>
    <xsl:when test="child::ExprNatRange">
      <xsl:apply-templates select="ExprNatRange"/>
    </xsl:when>
    <xsl:when test="child::PredType">
	<!-- Pas trouve dans la DTD-->
    </xsl:when>
    <xsl:when test="child::Record">
      <xsl:apply-templates select="Record"/>
    </xsl:when>
    <xsl:when test="child::TypeIdentifier">
      <xsl:apply-templates select="id"/>
    </xsl:when>
  </xsl:choose>
</xsl:template>
  
<xsl:template match="ExprNatRange">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>
  
<!--   les clauses  -->

<xsl:template match="ClausesList">
  <xsl:call-template name="clauses"/>
</xsl:template>

<!-- Clause SETS -->

<xsl:template match="SETS">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position() != last()">
      <xsl:text> ; <BR/> </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="SetEnumDec">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> = { </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> } </xsl:text>
</xsl:template>
  
<xsl:template match="SetAbstract">
  <xsl:apply-templates select="id"/>
</xsl:template>

<!-- INITIALISATION -->

<xsl:template match="INITIALISATION">
  <xsl:apply-templates/>
</xsl:template>
  
<!-- Clause VALUES -->

<xsl:template match="VALUES">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position()!=last()">
      <xsl:text> ; <BR/> </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="Valuation">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> = </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- Clause DEFINITIONS -->

<xsl:template match="DEFINITIONS">
    <xsl:apply-templates select="Definition"/>
</xsl:template>

<xsl:template match="Definition">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> == </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:if test="position()!=last()">
    <xsl:text> ; <BR/> </xsl:text>
  </xsl:if>
</xsl:template>

<xsl:template match="DefFile">
  <xsl:apply-templates select="STRING"/>
  <xsl:text> ; <BR/> </xsl:text>
</xsl:template>

<xsl:template match="DefHead">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:if test="child::idList and string(child::idList)">
    <xsl:text> ( </xsl:text>
    <xsl:apply-templates select="child::*[position()=2]"/>
    <xsl:text> ) </xsl:text>
  </xsl:if>
</xsl:template>

<xsl:template match="DefBody">
  <xsl:apply-templates select="*"/>
</xsl:template>

<xsl:template match="DefBodyExpr | DefBodyPred">
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<!-- Clause REFINES -->

<xsl:template match="REFINES">
  <xsl:apply-templates select="*"/>
</xsl:template>

<!-- Clause SEES -->

<xsl:template match="SEES">
  <xsl:for-each select="*">
    <xsl:value-of select="."/>
    <xsl:if test="position() != last()">
      <xsl:text> , </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<!-- Clauses CONSTANTS, PROMOTES, USES, var, TypeIdentifier -->

<xsl:template match="CONSTANTS | var | TypeIdentifier">
  <xsl:apply-templates select="id"/>
</xsl:template>

<xsl:template match="PROMOTES | USES">
  <xsl:for-each select="*">
    <xsl:value-of select="."/>
    <xsl:if test="position() != last()">
      <xsl:text> , </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<!-- Variable typee -->

<xsl:template match="varTyped">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- Clauses avec une liste d'identifiant -->

<xsl:template match="VARIABLES | CVARIABLES | AVARIABLES | HVARIABLES | HCONSTANTS | CCONSTANTS | ACONSTANTS">
  <xsl:apply-templates select="idList"/>
</xsl:template>

<!-- clause PROPERTIES, CONSTRAINTS, INVARIANT, ASSERTIONS -->

<xsl:template match="PROPERTIES">
  <xsl:apply-templates select="Pred"/>
</xsl:template>

<xsl:template match="CONSTRAINTS | INVARIANT">
  <xsl:apply-templates select="Pred"/>
</xsl:template>

<xsl:template match="ASSERTIONS">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position() != last()">
      <xsl:text> ; <BR/> </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<!-- clause EXTENDS, INCLUDES, IMPORTS -->

<xsl:template match="EXTENDS | INCLUDES | IMPORTS">
  <xsl:for-each select="*">
    <xsl:call-template name="instance"/>
    <xsl:if test="position() != last()">
      <xsl:text> , </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template name="instance">
  <xsl:for-each select="*">
    <xsl:choose>
      <xsl:when test="self::id">
        <xsl:apply-templates select="../id"/>
      </xsl:when>
      <xsl:when test="self::ExprList">
        <xsl:if test="count(*)!=0">
        <xsl:text> ( </xsl:text>
        <xsl:apply-templates select="../ExprList"/>
        <xsl:text> ) </xsl:text>
        </xsl:if>
      </xsl:when>
    </xsl:choose>
  </xsl:for-each>
</xsl:template>

<!-- Predicats -->

<xsl:template match="Pred">
  <xsl:apply-templates select="*"/>
</xsl:template>

<!-- 4.1 -->
<!-- Propositions -->

<xsl:template match="PredParen">
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="*"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="Neg">
  <FONT COLOR="#3333FF"> not </FONT>
  <xsl:apply-templates select="*"/>
</xsl:template>

<xsl:template match="And">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <BR/><FONT COLOR="#3333FF"> &amp; </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Or">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> or </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Implies">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> =&gt; </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Equiv">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> &lt;=&gt; </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 4.2 -->
<!-- Predicats Quantifies -->

<xsl:template match="Exists">
  <FONT COLOR="#3333FF"> # </FONT>
  <xsl:apply-templates select="idList"/>
  <FONT COLOR="#3333FF"> . </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="ForAll">
  <FONT COLOR="#3333FF"> ! </FONT>
  <xsl:apply-templates select="idList"/>
  <FONT COLOR="#3333FF"> . </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<!-- 4.3 -->
<!-- Predicats egalites -->

<xsl:template match="Equal">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> = </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="NotEqual">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> /= </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 4.4 -->
<!-- Predicats d'appartenance -->

<xsl:template match="In">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> : </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="NotIn">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> /: </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 4.5 -->
<!-- Predicats d'inclusions -->

<xsl:template match="SubSet">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> &lt;: </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="StrictSubSet">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> &lt;&lt;: </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="NotsubSet">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> /&lt;: </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="NotStrictSubSet">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> /&lt;&lt;: </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 4.6 -->
<!-- Predicats de comparaison -->

<xsl:template match="LessEqual">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> &lt;= </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Less">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> &lt; </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="GreaterEqual">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> &gt;= </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Greater">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> &gt; </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- Expressions -->

<xsl:template match="Expr">
  <xsl:apply-templates select="*"/>
</xsl:template>

<xsl:template match="ExprParen">
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="*"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<!-- Expressions arithmetiques -->

<xsl:template match="MAXINT">
  <FONT COLOR="#3333FF"> MAXINT </FONT>
</xsl:template>

<xsl:template match="MININT">
  <FONT COLOR="#3333FF"> MININT </FONT>
</xsl:template>

<xsl:template match="Minus">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> - </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="UMinus">
  <FONT COLOR="#3333FF"> - </FONT>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="Plus">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> + </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Mul">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> * </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Div">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> / </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Modulo">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> mod </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Puissance">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> ** </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Succ">
  <xsl:apply-templates select="Expr"/>
  <FONT COLOR="#3333FF"> + </FONT>
  <xsl:text> 1 </xsl:text>
</xsl:template>

<!-- 5.4 -->

<xsl:template match="Min">
  <FONT COLOR="#3333FF"> min </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="Max">
  <FONT COLOR="#3333FF"> max </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="Card">
  <FONT COLOR="#3333FF"> card </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="Expr"/>
<xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="PI">
  <FONT COLOR="#3333FF"> PI </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> ) </xsl:text>
  <FONT COLOR="#3333FF"> . </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <FONT COLOR="#3333FF"> | </FONT>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="SIGMA">
  <FONT COLOR="#3333FF"> SIGMA </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> ) </xsl:text>
  <FONT COLOR="#3333FF"> . </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <FONT COLOR="#3333FF"> | </FONT>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="LAMBDA">
  <FONT COLOR="#3333FF"> LAMBDA </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> ) </xsl:text>
  <FONT COLOR="#3333FF"> . </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <FONT COLOR="#3333FF"> | </FONT>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<!-- 5.5 -->
<!-- Expressions de couples -->

<xsl:template match="Maplet">
  <xsl:for-each select="*">
    <xsl:choose>
      <xsl:when test="position()=1">
        <xsl:apply-templates select="."/>
      </xsl:when>
      <xsl:otherwise>
        <FONT COLOR="#3333FF"> |-&gt; </FONT>
        <xsl:apply-templates select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:for-each>
</xsl:template>

<!-- 5.6 -->
<!-- Ensembles Predefinis -->

<xsl:template match="SetPredefined">
  <xsl:apply-templates select="child::*[position()=1]"/>
</xsl:template>

<xsl:template match="NAT">
  <FONT COLOR="#993399"> NAT </FONT>
</xsl:template>

<xsl:template match="INT">
  <FONT COLOR="#993399"> INT </FONT>
</xsl:template>

<xsl:template match="BOOL">
  <FONT COLOR="#993399"> BOOL </FONT>
</xsl:template>

<xsl:template match="INTEGER">
  <FONT COLOR="#993399"> INTEGER </FONT>
</xsl:template>

<xsl:template match="NATURAL">
  <FONT COLOR="#993399"> NATURAL </FONT>
</xsl:template>

<xsl:template match="NAT1">
  <FONT COLOR="#993399"> NAT1 </FONT>
</xsl:template>

<xsl:template match="NATURAL1">
  <FONT COLOR="#993399"> NATURAL1 </FONT>
</xsl:template>

<xsl:template match="SetEmpty">
  <FONT COLOR="#993399"> {} </FONT>
</xsl:template>

<xsl:template match="STRING">
  <FONT COLOR="#00FFCE"> " </FONT>
  <FONT COLOR="#00FFCE"> <xsl:value-of select="."/> </FONT>
  <FONT COLOR="#00FFCE"> " </FONT>
</xsl:template>

<!-- 5.7 -->
<!-- Expressions ensemblistes -->

<xsl:template match="Pow">
  <FONT COLOR="#3333FF"> POW </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="PowOne">
  <FONT COLOR="#3333FF"> POWONE </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="Fin">
  <FONT COLOR="#3333FF"> FIN </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="FinOne">
  <FONT COLOR="#3333FF"> FINONE </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<!-- 5.8 -->
<!-- Expressions ensemblistes -->

<xsl:template match="InterSets">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> /\ </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="UnionSets">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> \/ </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- Intersection et union Quantifiee -->

<xsl:template match="InterQ">
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> | </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> ) </xsl:text>
  <FONT COLOR="#3333FF"> = inter { </FONT>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <FONT COLOR="#3333FF"> } </FONT>
</xsl:template>

<xsl:template match="UnionQ">
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> | </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> ) </xsl:text>
  <FONT COLOR="#3333FF"> = union { </FONT>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <FONT COLOR="#3333FF"> } </FONT>
</xsl:template>

<!-- intersection et union generalisee -->

<xsl:template match="UnionG">
  <FONT COLOR="#3333FF"> union { </FONT>
  <xsl:apply-templates select="Expr"/>
  <FONT COLOR="#3333FF"> } </FONT>
</xsl:template>

<xsl:template match="InterG">
  <FONT COLOR="#3333FF"> inter { </FONT>
  <xsl:apply-templates select="Expr"/>
  <FONT COLOR="#3333FF"> } </FONT>
</xsl:template>

<!-- 5.9 -->
<!-- Expressions de records -->

<xsl:template match="Records">
  <FONT COLOR="#3333FF"> rec </FONT>
    <xsl:text> ( </xsl:text>
    <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position()!=last()">
      <xsl:text> , </xsl:text>
    </xsl:if>
  </xsl:for-each>
    <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="RecordAccess">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> ' </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="RecWithFields">
  <FONT COLOR="#3333FF"> struct </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position()!=last()">
      <xsl:text> , </xsl:text>
    </xsl:if>
  </xsl:for-each>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="RecordItem">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> : </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Field">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> : </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="SetEqualRecord">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> := </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 5.10 -->
<!-- Ensemble de Relations -->

<xsl:template match="RelSet">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> &lt;-&gt; </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 5.11 -->
<!-- Expressions de relations -->

<xsl:template match="Identity">
  <FONT COLOR="#3333FF"> id </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="Tilde">
  <xsl:apply-templates select="Expr"/>
  <FONT COLOR="#3333FF"> ~ </FONT>
</xsl:template>

<xsl:template match="PrjOne">
  <FONT COLOR="#3333FF"> prj1 </FONT>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="PrjTwo">
  <FONT COLOR="#3333FF"> prj2 </FONT>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="RelSeqComp">
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> ; </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="ParallelComp">
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> || </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="RelProd">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> &gt;&lt; </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 5.12 -->
<!-- Expressions de relations -->

<xsl:template match="RelIter">
  <FONT COLOR="#3333FF"> RelIter </FONT>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> , </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- fermeture -->

<xsl:template match="Closure">
  <FONT COLOR="#3333FF"> Closure </FONT>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<!-- fermeture reflexive transitive -->

<xsl:template match="ClosureOne">
  <FONT COLOR="#3333FF"> closure1 </FONT>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<!-- 5.13 -->
<!-- Expressions de relations -->

<xsl:template match="Dom"> <!-- Domaine -->
  <FONT COLOR="#3333FF"> dom </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="Ran"> <!-- Image -->
  <FONT COLOR="#3333FF"> ran </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<!-- 5.14 -->
<!-- Expressions de relations -->

<xsl:template match="DomSubstract">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> &lt;&lt;| </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="RanSubstract">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> |&gt;&gt; </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="DomRestrict">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> &lt;| </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="RanRestrict">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> |&gt; </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 5.15 -->
<!-- Ensembles de fonctions -->

<xsl:template match="PartialFunc">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> +-&gt; </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="TotalFunc">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> --&gt; </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="PartialInj">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> &gt;+&gt; </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="TotalInj">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> &gt;-&gt; </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="PartialSurj">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> +-&gt;&gt; </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="TotalSurj">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> --&gt;&gt; </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="PartialBij">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> &gt;+-&gt;&gt; </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="TotalBij">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> &gt;-&gt;&gt; </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 5.16 -->
<!-- Expressions de fonctions -->

<xsl:template match="Lambda">
  <FONT COLOR="#3333FF"> % </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> ) </xsl:text>
  <FONT COLOR="#3333FF"> . </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <FONT COLOR="#3333FF"> | </FONT>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="ExprFunCall">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:if test="child::ExprList and string(child::ExprList)">
    <xsl:text> ( </xsl:text>
    <xsl:apply-templates select="child::*[position()=2]"/>
    <xsl:text> ) </xsl:text>
  </xsl:if>
</xsl:template>

<xsl:template match="RelFnc">
  <FONT COLOR="#3333FF"> RelFnc </FONT>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="FncRel">
  <FONT COLOR="#3333FF"> FncRel </FONT> 
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<!-- 5.17 -->
<!-- Ensembles de sequences -->

<xsl:template match="Seq">
  <FONT COLOR="#3333FF"> seq </FONT> 
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="SeqOne">
  <FONT COLOR="#3333FF"> seq1 </FONT> 
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="ISeq">
  <FONT COLOR="#3333FF"> iseq </FONT> 
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="ISeqOne">
  <FONT COLOR="#3333FF"> iseq1 </FONT> 
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="Perm">
  <FONT COLOR="#3333FF"> perm </FONT> 
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<!-- 5.18 -->
<!-- Expressions de sequences -->

<xsl:template match="Size">
  <FONT COLOR="#3333FF"> size </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="First">
  <FONT COLOR="#3333FF"> first </FONT> 
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="Last">
  <FONT COLOR="#3333FF"> last </FONT> 
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="Tail">
  <FONT COLOR="#3333FF"> tail </FONT> 
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="Front">
  <FONT COLOR="#3333FF"> front </FONT> 
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="Rev">
  <FONT COLOR="#3333FF"> rev </FONT> 
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<!-- 5.19 -->
<!-- Expressions de sequences -->

<xsl:template match="AppendSeq">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> &lt;- </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="ConcatSeq">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> ^ </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="PrependSeq">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> -&gt; </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="PrefixSeq">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> /|\ </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="SuffixSeq">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> \|/ </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Conc">
  <FONT COLOR="#3333FF"> conc </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<!-- 5.20 -->
<!-- Ensembles d'arbres -->

<xsl:template match="Tree">
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="BTree">
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<!-- 5.21 -->
<!-- Expressions d'arbres -->

<xsl:template match="Const">
  <FONT COLOR="#3333FF"> const </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> , </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="Top">
  <FONT COLOR="#3333FF"> top </FONT>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="Sons">
  <FONT COLOR="#3333FF"> sons </FONT>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="Prefix">
  <FONT COLOR="#3333FF"> prefix </FONT>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="Postfix">
  <FONT COLOR="#3333FF"> postfix </FONT>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="SizeT">
  <FONT COLOR="#3333FF"> sizet </FONT>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="Mirror">
  <FONT COLOR="#3333FF"> mirror </FONT>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<!-- 5.22 -->
<!-- Expressions de noeuds d'arbres -->

<xsl:template match="Rank">
  <FONT COLOR="#3333FF"> rank </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> , </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="Father">
  <FONT COLOR="#3333FF"> father </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> , </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="Son">
 <FONT COLOR="#3333FF"> son </FONT> 
  <xsl:text> ) </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> , </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="Subtree">
  <FONT COLOR="#3333FF"> subtree </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> , </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="Arity">
  <FONT COLOR="#3333FF"> arity </FONT> 
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> , </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<!-- 5.23 -->
<!-- Expressions d'arbre binaires -->

<xsl:template match="Bin">
  <FONT COLOR="#3333FF"> bin </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> , </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> , </xsl:text>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="Left">
  <FONT COLOR="#3333FF"> left </FONT>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="Right">
  <FONT COLOR="#3333FF"> right </FONT>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="Infix">
  <FONT COLOR="#3333FF"> infix </FONT>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="ExprSequence">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> , </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="NatSetRange">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> .. </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="OverRideFwd">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> &lt;+ </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="OverRideBck">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="ExprId">
  <xsl:apply-templates select="id"/>
</xsl:template>

<xsl:template match="ExprList">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position()!=last()">
      <xsl:text> , </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="Assign">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- OPERATIONS -->

<xsl:template match="OPERATIONS">
  <xsl:apply-templates select="Operation"/>
</xsl:template>

<xsl:template match="Operation">
  <TABLE BORDER="2" WIDTH="90%" BGCOLOR="#FFFFCC">
  <TR>
  <TD>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <BLOCKQUOTE>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:if test="position()!=last()">
    <xsl:text> ; <BR/> </xsl:text>
  </xsl:if>
  </BLOCKQUOTE>
  </TD>
  </TR>	
  </TABLE>
  <BR/>
</xsl:template>

<xsl:template match="OperHead">
  <xsl:if test="child::OperOut and string(child::OperOut)">
    <xsl:apply-templates select="OperOut"/>
    <FONT COLOR="#3333FF"> &lt;-- </FONT>    
  </xsl:if>
  <xsl:apply-templates select="OperName" />
  <xsl:if test="child::OperIn and string(child::OperIn)">
    <xsl:text> ( </xsl:text>
    <xsl:apply-templates select="OperIn"/>
    <xsl:text> ) </xsl:text>
  </xsl:if>
    <FONT COLOR="#3333FF"> = </FONT>
</xsl:template>

<xsl:template match="OperName">
  <xsl:call-template name="link_op"/>
  <!-- xsl:apply-templates select="id"/-->
</xsl:template>

<xsl:template name="link_op">
  <A>
   <xsl:attribute name="NAME">
     <xsl:apply-templates select="id"/>
   </xsl:attribute>
   <xsl:apply-templates select="id"/>
  </A>
</xsl:template>

<xsl:template match="OperOut">
  <xsl:apply-templates select="idList"/>
</xsl:template>

<xsl:template match="OperIn">
  <xsl:apply-templates select="idList"/>
</xsl:template>

<xsl:template match="OperBody">
  <xsl:apply-templates select="*"/>
</xsl:template>

<xsl:template match="idList">
  <xsl:for-each select="*">
    <xsl:value-of select="."/>
    <xsl:if test="position() != last()">
      <xsl:text> , </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<!-- 6 -->
<!-- Substitutions -->
<!-- 6.1 -->

<xsl:template match="Block">
  <FONT COLOR="#CC6600"> BEGIN </FONT><BR/>
  <BLOCKQUOTE>
  <xsl:apply-templates select="*"/>
  </BLOCKQUOTE>
  <FONT COLOR="#CC6600"> END </FONT>
</xsl:template>

<!-- 6.2 -->

<xsl:template match="Skip">
  <FONT COLOR="#33CC66"> skip </FONT>
</xsl:template>

<!-- 6.3 -->

<xsl:template match="SetEqualIds">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> := </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="SetEqualFun">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> := </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 6.4 -->
<!-- Precondition substitution -->

<xsl:template match="Precondition">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="PRE">
  <FONT COLOR="#33CC66"> PRE </FONT>
  <BLOCKQUOTE>
  <xsl:apply-templates select="Pred"/>
  </BLOCKQUOTE>
</xsl:template>

<xsl:template match="THEN">
  <FONT COLOR="#33CC66"> THEN </FONT>
  <BLOCKQUOTE>
  <xsl:apply-templates select="*"/>
  </BLOCKQUOTE>
  <FONT COLOR="#33CC66"> END </FONT>
</xsl:template>

<!-- 6.5 -->
<!-- ASSERT Substitution -->

<xsl:template match="Assertion">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 6.6 -->
<!-- CHOICE Substitution -->

<xsl:template match="CHOICE">
  <FONT COLOR="#33CC66"> CHOICE </FONT>
  <xsl:for-each select="*">
    <BLOCKQUOTE>
    <xsl:apply-templates select="."/>
    </BLOCKQUOTE>
    <xsl:if test="position()!=last()">
      <FONT COLOR="#33CC66"> OR </FONT>
    </xsl:if>
  </xsl:for-each>
  <FONT COLOR="#33CC66"> END </FONT>
</xsl:template>

<!-- 6.7 -->
<!-- IF Substitution -->

<xsl:template match="IF">
  <xsl:for-each select="*">
    <xsl:choose>
      <xsl:when test="self::IfThen and position()=1">
        <xsl:call-template name="IfThen1"/>
      </xsl:when>
      <xsl:when test="self::IfThen and position()!=1">
        <xsl:call-template name="IfThen2"/>
      </xsl:when>
      <xsl:otherwise>
        <FONT COLOR="#33CC66"> ELSE </FONT>
        <BLOCKQUOTE>
        <xsl:apply-templates select="."/>
        </BLOCKQUOTE>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:for-each>
  <FONT COLOR="#33CC66"> END </FONT>
</xsl:template>

<xsl:template name="IfThen1">
    <FONT COLOR="#33CC66"> IF </FONT>
    <BLOCKQUOTE>
    <xsl:apply-templates select="child::*[position()=1]"/>
    </BLOCKQUOTE>
    <FONT COLOR="#33CC66"> THEN </FONT>
    <BLOCKQUOTE>
    <xsl:apply-templates select="child::*[position()=2]"/>
    </BLOCKQUOTE>
</xsl:template>

<xsl:template name="IfThen2">
    <FONT COLOR="#33CC66"> ELSIF </FONT>
    <BLOCKQUOTE>
    <xsl:apply-templates select="child::*[position()=1]"/>
    </BLOCKQUOTE>
    <BR/><FONT COLOR="#33CC66"> THEN </FONT>
    <BLOCKQUOTE>
    <xsl:apply-templates select="child::*[position()=2]"/>
    </BLOCKQUOTE>
</xsl:template>


<!-- 6.8 -->
<!-- SELECT Substitution -->

<xsl:template match="SELECT">
  <xsl:for-each select="*">
    <xsl:choose>
      <xsl:when test="self::WhenPart and position()=1">
        <FONT COLOR="#33CC66"> SELECT </FONT>
        <xsl:apply-templates select="."/>
      </xsl:when>
      <xsl:when test="self::WhenPart">
        <BR/><FONT COLOR="#33CC66"> WHEN </FONT>
        <BLOCKQUOTE>
        <xsl:apply-templates select="."/>
        </BLOCKQUOTE>
      </xsl:when>
      <xsl:otherwise>
        <FONT COLOR="#33CC66"> ELSE </FONT><BR/>
        <BLOCKQUOTE>
        <xsl:apply-templates select="."/>
        </BLOCKQUOTE>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:for-each>
  <FONT COLOR="#33CC66"> END </FONT><BR/>
</xsl:template>

<xsl:template match="WhenPart">
  <BLOCKQUOTE>
  <xsl:apply-templates select="child::*[position()=1]"/>
  </BLOCKQUOTE>
  <FONT COLOR="#33CC66"> THEN </FONT>
  <BLOCKQUOTE>
  <xsl:apply-templates select="child::*[position()=2]"/>
  </BLOCKQUOTE>
</xsl:template>

<!-- 6.9 -->
<!-- CASE Substitution -->

<xsl:template match="CASE">
  <FONT COLOR="#33CC66"> CASE </FONT>
  <xsl:for-each select="*">
    <xsl:choose>
      <xsl:when test="position()=1">
        <BLOCKQUOTE>
        <xsl:apply-templates select="."/>
        </BLOCKQUOTE>
      </xsl:when>
      <xsl:when test="position()=2">
        <FONT COLOR="#33CC66"> OF </FONT>
        <BLOCKQUOTE>
        <xsl:apply-templates select="."/>
        </BLOCKQUOTE>
      </xsl:when>
      <xsl:when test="position()=3">
        <FONT COLOR="#33CC66"> ELSE </FONT>
        <BLOCKQUOTE>
        <xsl:apply-templates select="."/>
        </BLOCKQUOTE>
      </xsl:when>
    </xsl:choose>
  </xsl:for-each>
  <FONT COLOR="#33CC66"> END <BR/> END </FONT>
</xsl:template>

<xsl:template match="OrPartList">
  <xsl:for-each select="*">
    <xsl:choose>
      <xsl:when test="self::OrPart and position()=1">
        <xsl:call-template name="OrPart1"/>
      </xsl:when>
      <xsl:when test="self::OrPart and position()!=1 ">
        <xsl:call-template name="OrPart2"/>
      </xsl:when>
      <xsl:otherwise/>
    </xsl:choose>
  </xsl:for-each>
</xsl:template>

<xsl:template name="OrPart1">
  <FONT COLOR="#33CC66"> EITHER </FONT>
  <BLOCKQUOTE>
  <xsl:apply-templates select="child::*[position()=1]"/>
  </BLOCKQUOTE>
  <FONT COLOR="#33CC66"> THEN </FONT>
  <BLOCKQUOTE>
  <xsl:apply-templates select="child::*[position()=2]"/>
  </BLOCKQUOTE>
</xsl:template>

<xsl:template name="OrPart2">
  <FONT COLOR="#33CC66"> OR </FONT>
  <BLOCKQUOTE>
  <xsl:call-template name="ExprOrPart"/>
  </BLOCKQUOTE>
  <FONT COLOR="#33CC66"> THEN </FONT>
  <BLOCKQUOTE>
  <xsl:apply-templates select="child::*[position()=last()]"/>
  </BLOCKQUOTE>
</xsl:template>

<xsl:template name="ExprOrPart">
  <xsl:for-each select="Expr">
    <xsl:apply-templates/>
    <xsl:if test="position()!=last()">
      <xsl:text> , </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<!-- 6.10 -->
<!-- ANY Substitution -->

<xsl:template match="ANY">
  <FONT COLOR="#33CC66"> ANY </FONT>
  <BLOCKQUOTE>
  <xsl:apply-templates select="child::*[position()=1]"/>
  </BLOCKQUOTE>
  <FONT COLOR="#33CC66"> WHERE </FONT>
  <BLOCKQUOTE>
  <xsl:apply-templates select="child::*[position()=2]"/>
  </BLOCKQUOTE>
  <FONT COLOR="#33CC66"> THEN </FONT>
  <BLOCKQUOTE>
  <xsl:apply-templates select="child::*[position()=3]"/>
  </BLOCKQUOTE>
  <FONT COLOR="#33CC66"> END </FONT>
</xsl:template>

<!-- 6.11 -->
<!-- LET Substitution -->

<xsl:template match="LET">
  <FONT COLOR="#33CC66"> LET </FONT>
  <BLOCKQUOTE>
  <xsl:apply-templates select="child::*[position()=1]"/>
  </BLOCKQUOTE>
  <FONT COLOR="#33CC66"> BE </FONT>
  <BLOCKQUOTE>
  <xsl:apply-templates select="child::*[position()=2]"/>
  </BLOCKQUOTE>
  <FONT COLOR="#33CC66"> IN </FONT>
  <BLOCKQUOTE>
  <xsl:apply-templates select="child::*[position()=3]"/>
  </BLOCKQUOTE>
  <FONT COLOR="#33CC66"> END </FONT>
</xsl:template>

<!-- 6.12 -->

<xsl:template match="SetIn">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> :: </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 6.13 -->

<xsl:template match="BecomeSuch">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#3333FF"> : </FONT>
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="ASSERT">
  <FONT COLOR="#33CC66"> ASSERT </FONT>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <FONT COLOR="#33CC66"> THEN </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <FONT COLOR="#33CC66"> END </FONT><BR/>
</xsl:template>

<!-- 6.14 -->
<!-- VAR Substitution -->

<xsl:template match="VAR">
  <FONT COLOR="#33CC66"> VAR </FONT>
  <BLOCKQUOTE>
  <xsl:apply-templates select="child::*[position()=1]"/>
  </BLOCKQUOTE>
  <FONT COLOR="#33CC66"> IN </FONT>
  <BLOCKQUOTE>
  <xsl:apply-templates select="child::*[position()=2]"/>
  </BLOCKQUOTE>
  <FONT COLOR="#33CC66"> END </FONT>
</xsl:template>

<!-- 6.15 -->

<xsl:template match="Sequence">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position() != last()">
      <xsl:text> ; </xsl:text><BR/>
    </xsl:if>
    </xsl:for-each>
</xsl:template>


<!-- 6.16 -->

<xsl:template match="OperCall">
  <xsl:for-each select="*">
    <xsl:choose>
      <xsl:when test="self::idList and string(self::idList)">
        <xsl:apply-templates select="."/>
        <FONT COLOR="#3333FF"> &lt;-- </FONT>
      </xsl:when>
      <xsl:when test="self::Expr">
        <xsl:apply-templates select="."/>
      </xsl:when>
      <xsl:when test="self::ExprList and count(child::*)!=0">
        <xsl:text> ( </xsl:text>
        <xsl:apply-templates select="."/>
        <xsl:text> ) </xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:for-each>
</xsl:template>
  
<!-- 6.17 -->
<xsl:template match="WHILE">
  <FONT COLOR="#33CC66"> WHILE </FONT>
  <BLOCKQUOTE>
  <xsl:apply-templates select="child::*[position()=1]"/>
  </BLOCKQUOTE>
  <FONT COLOR="#33CC66"> DO </FONT>
  <BLOCKQUOTE>
  <xsl:apply-templates select="child::*[position()=2]"/>
  </BLOCKQUOTE>
  <FONT COLOR="#33CC66"> INVARIANT </FONT>
  <BLOCKQUOTE>
  <xsl:apply-templates select="child::*[position()=3]"/>
  </BLOCKQUOTE>
  <FONT COLOR="#33CC66"> VARIANT </FONT>
  <BLOCKQUOTE>
  <xsl:apply-templates select="child::*[position()=4]"/>
  </BLOCKQUOTE>
  <FONT COLOR="#33CC66"> END </FONT>
</xsl:template>

<xsl:template match="WhileTest">
  <xsl:apply-templates select="Pred"/>
</xsl:template>
  
<xsl:template match="WhileInvariant">
  <xsl:apply-templates select="Pred"/>
</xsl:template>
  
<xsl:template match="WhileVariant">
  <xsl:apply-templates select="Expr"/>
</xsl:template>
  
<xsl:template match="WhileBody">
  <xsl:apply-templates select="*"/>
</xsl:template>

<!-- 6.18 -->

<xsl:template match="Parallel">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <BR/><FONT COLOR="#3333FF"> || </FONT>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>
  
<!-- Afficher l'identifiant -->
<xsl:template match="id">
  <xsl:for-each select="self::id">
    <xsl:value-of select="."/>
    <xsl:if test="position()!=last()">
      <xsl:text> , </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>
  
<xsl:template match="idrename">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>
  
<xsl:template match="Constant">
  <xsl:choose>
    <xsl:when test="child::number">
      <xsl:apply-templates select="number"/>
    </xsl:when>
    <xsl:when test="child::string">
      <xsl:apply-templates select="string"/>
    </xsl:when>
  </xsl:choose>
</xsl:template>
  
<xsl:template match="string">
  <xsl:value-of select="."/>
</xsl:template>
  
<xsl:template match="SeqEnum">
  <xsl:text> [ </xsl:text>
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position()!=last()">
      <xsl:text> , </xsl:text>
    </xsl:if>
  </xsl:for-each>
  <xsl:text> ] </xsl:text>
</xsl:template>
  
<xsl:template match="SeqEmpty">
  <xsl:text> [] </xsl:text>
</xsl:template>
  
<xsl:template match="Set">
  <xsl:apply-templates select="*"/>
</xsl:template>
  
<xsl:template match="SetRange">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>
  
<xsl:template match="SetEnum">
  <xsl:apply-templates select="Expr"/>
</xsl:template>
  
<xsl:template match="SetCompr">
  <xsl:text> { </xsl:text>
  <xsl:for-each select="*">
    <xsl:apply-templates/>
    <xsl:if test="position() != last()">
      <xsl:text> , </xsl:text>
    </xsl:if>
  </xsl:for-each>
  <xsl:text> } </xsl:text>
</xsl:template>
  
<xsl:template match="SetComprPred">
  <xsl:text> { </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> | </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> } </xsl:text>
</xsl:template>
  
<xsl:template match="INTER">
 <xsl:apply-templates select="child::*[position()=1]"/>
 <xsl:apply-templates select="child::*[position()=2]"/>
 <xsl:apply-templates select="child::*[position()=3]"/>
</xsl:template>
  
<xsl:template match="BoolEvaluation">
 <FONT COLOR="#3333FF"> bool </FONT>
 <xsl:text> ( </xsl:text>
 <xsl:apply-templates select="*"/>
 <xsl:text> ) </xsl:text>
</xsl:template>
  
<xsl:template match="TRUE">
  <FONT COLOR="#3333FF"> TRUE </FONT>
</xsl:template>
  
<xsl:template match="FALSE">
  <FONT COLOR="#3333FF"> FALSE </FONT>
</xsl:template>
  
<xsl:template match="Number">
  <xsl:value-of select="."/>
</xsl:template>
  
<xsl:template match="number">
  <xsl:value-of select="."/>
</xsl:template>
  
<!-- Entite clause -->
     <xsl:template name="clauses">
       <xsl:for-each select="*">
        <xsl:choose>
        <xsl:when test="self::SEES">
                  <BR/><B>SEES</B><BR/>
                  <BLOCKQUOTE>
                  <xsl:apply-templates select="//SEES"/>
                  </BLOCKQUOTE>
        </xsl:when>
        <xsl:when test="self::DEFINITIONS">
                  <BR/><B>DEFINITIONS</B><BR/>
                  <BLOCKQUOTE>
                  <xsl:apply-templates select="//DEFINITIONS"/>
                  </BLOCKQUOTE>
        </xsl:when>
        <xsl:when test="self::CONSTRAINTS">
                  <BR/><B>CONSTRAINTS</B><BR/>
                  <BLOCKQUOTE>
                  <xsl:apply-templates select="//CONSTRAINTS"/>
                  </BLOCKQUOTE>
        </xsl:when>
        <xsl:when test="self::AVARIABLES">
                  <BR/><B>ABSTRACT_VARIABLES</B><BR/>
                  <BLOCKQUOTE>
                  <xsl:apply-templates select="//AVARIABLES"/>
                  </BLOCKQUOTE>
         </xsl:when>
        <xsl:when test="self::INVARIANT">
                  <BR/><B>INVARIANT</B><BR/>
                  <BLOCKQUOTE>
                  <xsl:apply-templates select="//INVARIANT"/>
                  </BLOCKQUOTE>
        </xsl:when>
        <xsl:when test="self::SETS">
                  <BR/><B>SETS</B><BR/>
                  <BLOCKQUOTE>
                  <xsl:apply-templates select="//SETS"/>
                  </BLOCKQUOTE>
         </xsl:when>
        <xsl:when test="self::INITIALISATION">
                  <BR/><B>INITIALISATION</B><BR/>
                  <BLOCKQUOTE>
                  <xsl:apply-templates select="//INITIALISATION"/>
                  </BLOCKQUOTE>
        </xsl:when>
        <xsl:when test="self::CONSTANTS">
                  <BR/><B>CONSTANTS</B><BR/>
                  <BLOCKQUOTE>
                  <xsl:apply-templates select="//CONSTANTS"/>
                  </BLOCKQUOTE>
         </xsl:when>
        <xsl:when test="self::CCONSTANTS">
                  <BR/><B>CONCRETE_CONSTANTS</B><BR/>
                  <BLOCKQUOTE>
                  <xsl:apply-templates select="//CCONSTANTS"/>
                  </BLOCKQUOTE>
         </xsl:when>
        <xsl:when test="self::ACONSTANTS">
                  <BR/><B>ABSTRACT_CONSTANTS</B><BR/>
                  <BLOCKQUOTE>
                  <xsl:apply-templates select="//ACONSTANTS"/>
                  </BLOCKQUOTE>
         </xsl:when>
        <xsl:when test="self::HCONSTANTS">
                  <BR/><B>HCONSTANTS</B><BR/>
                  <BLOCKQUOTE>
                  <xsl:apply-templates select="//HCONSTANTS"/>
                  </BLOCKQUOTE>
         </xsl:when>
        <xsl:when test="self::PROPERTIES">
                  <BR/><B>PROPERTIES</B><BR/>
                  <BLOCKQUOTE>
                  <xsl:apply-templates select="//PROPERTIES"/>
                  </BLOCKQUOTE>
         </xsl:when>
        <xsl:when test="self::VARIABLES">
                  <BR/><B>VARIABLES</B><BR/>
                  <BLOCKQUOTE>
                  <xsl:apply-templates select="//VARIABLES"/>
                  </BLOCKQUOTE>
         </xsl:when>
        <xsl:when test="self::CVARIABLES">
                   <BR/><B>CONCRETE_VARIABLES</B><BR/>
                  <BLOCKQUOTE>
                  <xsl:apply-templates select="//CVARIABLES"/>
                  </BLOCKQUOTE>
         </xsl:when>
        <xsl:when test="self::HVARIABLES">
                  <BR/><B>HVARIABLES</B><BR/>
                  <BLOCKQUOTE>
                  <xsl:apply-templates select="//HVARIABLES"/>
                  </BLOCKQUOTE>
         </xsl:when>
        <xsl:when test="self::PROMOTES">
                  <BR/><B>PROMOTES</B><BR/>
                  <BLOCKQUOTE>
                  <xsl:apply-templates select="//PROMOTES"/>
                  </BLOCKQUOTE>
        </xsl:when>
        <xsl:when test="self::EXTENDS">
                  <BR/><B>EXTENDS</B><BR/>
                  <BLOCKQUOTE>
                  <xsl:apply-templates select="//EXTENDS"/>
                  </BLOCKQUOTE>
        </xsl:when>
        <xsl:when test="self::ASSERTIONS">
                  <BR/><B>ASSERTIONS</B><BR/>
                  <BLOCKQUOTE>
                  <xsl:apply-templates select="//ASSERTIONS"/>
                  </BLOCKQUOTE>
         </xsl:when>
        <xsl:when test="self::OPERATIONS">
                  <BR/>
                  <TABLE BORDER="2" WIDTH="75%" BGCOLOR="#99FFCC">
                  <TR>
                  <TD>
                  <B>OPERATIONS</B><BR/>
                  <BLOCKQUOTE>
                  <xsl:apply-templates select="//OPERATIONS"/>
                  </BLOCKQUOTE>
                  </TD>
                  </TR>
                  </TABLE>
         </xsl:when>
        <xsl:when test="self::USES">
                   <BR/><B>USES</B><BR/>
                  <BLOCKQUOTE>
                  <xsl:apply-templates select="//USES"/>
                  </BLOCKQUOTE>
         </xsl:when>
        <xsl:when test="self::INCLUDES">
                  <BR/><B>INCLUDES</B><BR/>
                  <BLOCKQUOTE>
                  <xsl:apply-templates select="//INCLUDES"/>
                  </BLOCKQUOTE>
         </xsl:when>
        <xsl:when test="self::REFINES">
                  <BR/><B>REFINES</B><BR/>
                  <BLOCKQUOTE>
                  <xsl:apply-templates select="//REFINES"/>
                  </BLOCKQUOTE>
         </xsl:when>
        <xsl:when test="self::IMPORTS">
                  <BR/><B>IMPORTS</B><BR/>
                  <BLOCKQUOTE>
                  <xsl:apply-templates select="//IMPORTS"/>
                  </BLOCKQUOTE>
         </xsl:when>
        <xsl:when test="self::VALUES">
                  <BR/><B>VALUES</B><BR/>
                  <BLOCKQUOTE>
                  <xsl:apply-templates select="//VALUES"/>
                  </BLOCKQUOTE>
        </xsl:when>
        </xsl:choose>
       </xsl:for-each>
     </xsl:template>
</xsl:stylesheet>

