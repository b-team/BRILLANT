(*☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   Project: BiCoq, formalisation of the B theory into the Coq system
   Module: Bprop, propositional calculus
   The automated proof method proposed by Abrial is implemented as a prover function.
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : October 2005 - October 2006
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Require Export Bprop.

(* Prover functions for simple theorems ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Replace an ☲ goal by its contraposition *)
 Definition fbkcontrap(S:☒):[☒*]:=∅☓ ☓★ (match S with ☬☓(G,P☲Q) => ☬☓(G,#(Q)☲#(P)) | _ => S end).

 Theorem corrfbkcontrap:forall (S:☒), ∧☓(fbkcontrap S)->☭☓(S).
 Proof.
  induction S; unfold fbkcontrap; intros H; destruct b0;
   try (destruct (lsinfins _ _ H); apply s); destruct b0_2; destruct b0_1; simpl;
   try (apply contrapn; destruct (lsinfins _ _ H); apply s);
   try (apply contrapn'; destruct (lsinfins _ _ H); apply s);
   try (apply contrapp; destruct (lsinfins _ _ H); apply s);
   try (apply contrapp'; destruct (lsinfins _ _ H); apply s).
  Qed.

(* Propositional calculus strategy prover functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Application of  bbbs1, bbbs2, bbdb1, or bbdb2 to conclude *)
 Definition fpropconc(S:☒):[☒*]:=
  match S with
  | ☬☓(G,P) => match P ∈☑☂ G with
                   | ⊤ => ∅☓
                   | ⊥ => match P with
                             | Q☲R => match R ∈☑☂ G with
                                            | ⊤ => ∅☓
                                            | ⊥ => match #(Q) ∈☑☂ G with ⊤ => ∅☓ | ⊥ => ∅☓ ☓★ S end
                                            end
                             | _ => ∅☓ ☓★ S
                             end
                   end
  end.

 Theorem corrfpropconc:forall (S:☒), ∧☓(fpropconc S)->☭☓(S).
 Proof.
  induction S; unfold fpropconc; intros H; destruct (decgin b0 b).
   simpl; apply biin; apply l.
   rewrite <- (sym_id (ginrf _ _ e)) in H; destruct b0;
    try (destruct (lsinfins _ _ H); apply s); destruct (decgin b0_2 b).
     simpl; apply biimpi; apply biins; apply biin; apply l.
     rewrite <- (sym_id (ginrf _ _ e0)) in H; destruct (decgin #(b0_1) b).
      destruct b0_1; simpl; try (apply bbdb2; apply l).
       apply bbdb1; apply l.
    rewrite <- (sym_id (ginrf _ _ e1)) in H; destruct (lsinfins _ _ H); apply s.
 Qed.

 Theorem concfpropconc:forall (S:☒), (fpropconc S≡∅☓)+(fpropconc S≡∅☓☓★S).
 Proof.
  induction S; unfold fpropconc; destruct (b0 ∈☑☂ b); try (left; apply refl_id);
   try (right; apply refl_id).
   destruct b0; try (left; apply refl_id); try (right; apply refl_id).
    destruct (b0_2 ∈☑☂ b); try (left; apply refl_id); try (right; apply refl_id).
     destruct (#(b0_1) ∈☑☂ b); try (left; apply refl_id); try (right; apply refl_id).
 Qed.

 (* Application of  bbdr1 to bbdr8, followed by cnj and ded, to progress *)
 Definition fpropprog(S:☒):[☒*]:=
  match S with
  | ☬☓(G,¬P) => match P with
                     | ¬P' => ∅☓ ☓★ ☬☓(G,P') (* DR1 *)
                     | P'☲Q' => ∅☓ ☓★ ☬☓(G,P') ☓★ ☬☓(G,¬Q') (* DR2 *)
                     | P'∧Q' => ∅☓ ☓★ ☬☓(G,P'☲¬Q') (* DR3 *)
                     | _ => ∅☓ ☓★ S (* FAIL *)
                     end
  | ☬☓(G,P☲Q) => match P with
                        | ¬¬P' => ∅☓ ☓★ ☬☓(G,P'☲Q) (* DR4 *)
                        | ¬(P'☲Q') => ∅☓ ☓★ ☬☓(G,P'☲(¬Q'☲Q)) (* DR5 *)
                        | ¬(P'∧Q') => ∅☓ ☓★ ☬☓(G,¬P'☲Q) ☓★ ☬☓(G,¬Q'☲Q) (* DR6 *)
                        | P'☲Q' => ∅☓ ☓★ ☬☓(G,¬P'☲Q) ☓★ ☬☓(G,Q'☲Q) (* DR7 *)
                        | P'∧Q' => ∅☓ ☓★ ☬☓(G,P'☲(Q'☲Q)) (* DR8 *)
                        | _ => ∅☓ ☓★ ☬☓(G ☑★ P,Q) (* DED *)
                        end
  | ☬☓(G,P∧Q) => ∅☓ ☓★ ☬☓(G,P) ☓★ ☬☓(G,Q) (* CNJ *)
  | _ => ∅☓ ☓★ S (* FAIL *)
  end.

 Theorem corrfpropprog:forall (S:☒), ∧☓(fpropprog S)->☭☓(S).
 Proof.
  induction S; unfold fpropprog; intros H; destruct b0;
   try (destruct (lsinfins _ _ H); apply s); simpl.
   destruct b0; try (destruct (lsinfins _ _ H); apply s); simpl.
    apply bbdr1; destruct (lsinfins _ _ H); apply s.
    apply bbdr3; destruct (lsinfins _ _ H); apply s.
    apply bbdr2; destruct (lsinfins _ _ H); clear H; destruct (lsinfins _ _ l); clear l l0;
     [apply s0 | apply s].
   apply cnj; destruct (lsinfins _ _ H); clear H; destruct (lsinfins _ _ l);
    clear l l0; [apply s0 | apply s].
   destruct b0_1; try (apply biimpi; destruct (lsinfins _ _ H); apply s).
    destruct b0_1; try (apply biimpi; destruct (lsinfins _ _ H); apply s).
     apply bbdr4; destruct (lsinfins _ _ H); apply s.
     apply bbdr6; destruct (lsinfins _ _ H); clear H; destruct (lsinfins _ _ l); clear l l0;
     [apply s0 | apply s].
     apply bbdr5; destruct (lsinfins _ _ H); apply s.
     apply bbdr8; destruct (lsinfins _ _ H); apply s.
     apply bbdr7; destruct (lsinfins _ _ H); clear H; destruct (lsinfins _ _ l); clear l l0;
     [apply s0 | apply s].
 Qed.

 (* One step application for propositional calculus *)
 Definition fpropstep(S:☒):[☒*]:=
  let t:=fpropconc S in match t with ∅☓ => ∅☓ | _ ☓★ _ => fpropprog S end.

 Theorem corrfpropstep:forall (S:☒), ∧☓(fpropstep S)->☭☓(S).
 Proof.
  intros S; unfold fpropstep; intros H; apply corrfpropconc; destruct (concfpropconc S);
   rewrite i;
   [apply lsinfemp |
     rewrite <- (sym_id i) in H; apply inslsinf; split; [apply lsinfemp
                                                                                             | apply corrfpropprog; apply H]].
 Qed.

 (* Application of one step to all sequents of a list *)
 Fixpoint fpropsl(L:[☒*]){struct L}:[☒*]:=
  match L with
  | ∅☓ => ∅☓
  | L' ☓★ S' => flsconc (fpropsl L') (fpropstep S')
  end.

 Theorem corrfpropsl:forall (L:[☒*]), ∧☓(fpropsl L)->∧☓(L).
 Proof.
  induction L; intros H;
   [apply lsinfemp | simpl in H; destruct (lsinfconc _ _ H); apply inslsinf; split;
                                   [apply IHL; apply l | apply corrfpropstep; apply l0]].
 Qed.

 (* Application of several steps to all sequents of a list *)
 Fixpoint fpropjump(L:[☒*])(c:☈){struct c}:[☒*]:=
  match c with
  | ☦ => L
  | ☠c' => fpropjump (fpropsl L) c'
  end.
 (* Nota: the value c avoid having to identify a variant for a fixpoint, as there is no    *)
 (* proof that recursive calls to fpropsl will terminate. It is also possible to             *)
 (* define a variant, based on a well-funded order on the sequents...                                    *)

 Theorem corrfpropjump:forall (c:☈)(L:[☒*]), ∧☓(fpropjump L c)->∧☓(L).
 Proof.
  induction c; simpl; intros L H; [apply H | apply corrfpropsl; apply (IHc _ H)].
 Qed.
 (* Nota: this proves the correctness of the propositional calculus automated prover,  *)
 (* but we should also prove the completion, i.e. the fact that under the assumption of   *)
 (* the existence of a proof, this prover would provide a proof.                                            *)

 Theorem compfpropjump:forall (c:☈)(G:☐)(P:☊), ∧☓(fpropjump (∅☓ ☓★ ☬☓(G,P)) c)->G⊣P.
 Proof.
  intros c G P H; assert (H0:∧☓(∅☓ ☓★ ☬☓(G,P)) -> G⊣P).
   intros H0; destruct (lsinfins _ _ H0); apply s.
   apply H0; apply corrfpropjump with (c:=c); apply H.
 Qed.
 (* Nota: this results allow to replace proof using BTprop tactic by computation using *)
 (* fpropjump function ; an example is provided thereafter.                                                   *)

 Theorem demo1:forall (P Q:☊), ⊣P☲Q☲(P∧Q).
 Proof.
  intros P Q; apply biimpi; apply biimpi; apply biandi; BThyp.
 Qed.

 Theorem demo2:forall (P Q:☊), ⊣P☲Q☲(P∧Q).
 Proof.
  BTprop.
 Qed.

 Theorem demo3:⊣(π ☦) ☲ (π ☧) ☲ ((π ☦) ∧ (π ☧)).
 Proof.
  apply compfpropjump with (c:=☠☠☠☠☦); simpl; apply lsinfemp.
 Qed.

(* B-Book proofs based on the propositional calculus tactic ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* BB p25 §1.2.4 Th1.2.9 *)
 Eval compute in (fpropjump (∅☓ ☓★ ☬☓(∅☑,(¬(π ☦) ☲(π ☧)) ∧(π ☧☦) ☲(¬((π ☦) ∧(π ☧☦)) ☲((π ☧) ∧(π ☧☦))))) ☧☦☦☦).

 (* BB p26 §1.2.5 Th1.2.10 *)
 Eval compute in (fpropjump (∅☓ ☓★ ☬☓(∅☑,((π ☦) ☲ (π ☧)) ∧ ((π ☧☦) ☲ (π ☧)) ∧ ((π ☦) ∨ (π ☧☦)) ☲ (π ☧))) ☧☦☦☦).

(* Case tactic prover function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Case tactic prover function *)
 Definition fbkcase(S:☒)(P Q:☊):[☒*]:=
  match S with ☬☓(G,R) => ∅☓ ☓★ ☬☓(G,P∨Q) ☓★ ☬☓(G ☑★ P,R) ☓★ ☬☓(G ☑★ Q,R) end.

 Theorem corrfbkcase:forall (S:☒)(P Q:☊), ∧☓(fbkcase S P Q)->☭☓(S).
 Proof.
  induction S; intros P Q; unfold fbkcase; intros H; destruct (lsinfins _ _ H); clear H;
   destruct (lsinfins _ _ l); clear l; destruct (lsinfins _ _ l0); clear l l0;
   apply (case _ _ _ _ s1 s0 s).
 Qed.

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)
