(*==================================================================================================
  Project : Study of the concept of refinement
  Module : data_ref
  We study the concept of data-refinement, a form of simulation of a program A by a program C while
  both programs do not use the same representations for their data. In here, a program is
  considered as a function (in Coq, this means a total and computable one), the refinement
  relationship is defined as modulo interpretations, that is relations between the representations.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/ANSSI & UPMC/LIP6 - St: February 2007, Md: May 2008
  Provided under licence CeCILL-B
  ================================================================================================*)

Require Export relation_gen.

(* Program definitions ---------------------------------------------------------------------------*)

Record Prog:Type:=Prog_ {dom:Set;ran:Set;fnc:dom->ran}.
Notation "'§(' f ')'":=(Prog_ _ _ f).
(* Nota: Partial functions are representable using a domain of the form {x:D & pre(x)} where pre(x)
   is the precondition for fnc. *)

(* Data-refinement definition --------------------------------------------------------------------*)

Definition DatRef(Pa Pc:Prog)(Rd:Pa.(dom)↔Pc.(dom))(Rr:Pa.(ran)↔Pc.(ran)):=
 forall (xa:Pa.(dom))(xc:Pc.(dom)), Rd(xa,xc)->Rr(Pa.(fnc) xa,Pc.(fnc) xc).
Notation "Pa '⇝' Pc '[' Rd ','  Rr ']'":=(DatRef Pa Pc Rd Rr) (no associativity, at level 90).
(* Nota:Very generic, e.g. always true if Rdom empty or Rran full. Yet one can argue that such
   interpretations are still valid in extreme cases (absence of specification, or representing
   unit, etc. *)

(* Data-refinement properties --------------------------------------------------------------------*)

Theorem DatRef_refl:forall (P:Prog), P⇝P[Ⅰ(P.(dom)),Ⅰ(P.(ran))]. (* Reflexivity *)
Proof.
 intros P; unfold DatRef; intros xa xr Hdom; inversion_clear Hdom; apply RId_.
Qed.

Theorem DatRef_sym:forall (Pa Pc:Prog)(Rd:Pa.(dom)↔Pc.(dom))(Rr:Pa.(ran)↔Pc.(ran)),
                   Pa⇝Pc[Rd,Rr]->Pc⇝Pa[↩Rd,↩Rr]. (* Symmetry *)
Proof.
 intros Pa Pc Rd Rr Href; unfold DatRef; intros c a Hdom; inversion_clear Hdom; apply RInv_;
  apply Href; apply H.
Qed.

Theorem DatRef_tran:forall (Pa Pi:Prog)
                           (Rd1:Pa.(dom)↔Pi.(dom))
                           (Rr1:Pa.(ran)↔Pi.(ran)), Pa⇝Pi[Rd1,Rr1]->
                    forall (Pc:Prog)
                           (Rd2:Pi.(dom)↔Pc.(dom))
                           (Rr2:Pi.(ran)↔Pc.(ran)), Pi⇝Pc[Rd2,Rr2]->
                    Pa⇝Pc[Rd2∘Rd1,Rr2∘Rr1].
Proof.
 unfold DatRef; intros Pa Pi Rd1 Rr1 Hr1 Pc Rd2 Rr2 Hr2 a c Hac; inversion_clear Hac;
  apply RCmp_ with (b:=fnc Pi b).
 apply Hr1; apply H.
 apply Hr2; apply H0.
Qed.
(* Nota: Rd1 and Rd2 can be non-empty yet Rd2∘Rd1 being empty - and the refinement being trivially
   true in this case. The intuition is that if Rd2∘Rd1 is indeed empty, then they are not compatible
   that is there is no representation of values of the first (abstract domain)  in the third
   (concrete) domain. *)

Theorem DatRef_mon:forall (Da Ia Ra Dc Ic Rc:Set) (* Domain/intermediate/range, abstract/concrete *)
                          (fa1:Da->Ia)(fa2:Ia->Ra)(fc1:Dc->Ic)(fc2:Ic->Rc)
                          (Rd:Da↔Dc)(Ri1 Ri2:Ia↔Ic)(Rr:Ra↔Rc),
                          §(fa1)⇝§(fc1)[Rd,Ri1]->
                          §(fa2)⇝§(fc2)[Ri2,Rr]->
                          Ri1⊆Ri2->
                          §(fun x=>fa2 (fa1 x))⇝§(fun x=>fc2 (fc1 x))[Rd,Rr]. (* Composition *)
Proof.
 intros Da Ia Ra Dc Ic Rc fa1 fa2 fc1 fc2 Rd Ri1 Ri2 Rr Hai Hic Hcomp; unfold DatRef; simpl;
  intros a c Hac; apply Hic; apply Hcomp; apply Hai; apply Hac.
Qed.
(* Nota: This allow to decompose a program into several modules, to refine each one before composing
   them to obtain a refinement of the initial program. The condition Ri1⊆Ri2 in this context just
   expresses that the two interpretations of the intermediate support are consistent. *)

(* Illustration 1: Booleans represented through naturals -----------------------------------------*)
(* In this example, the abstract program is "not" on booleans, and the concrete representation is
   nat. The interpretation (for domain and range) is that any odd natural values represent false
   and even natural values represent true. *)

Section Bool_as_nat.
 Require Import Bool.
 Let not_bool:=§(negb).
 Let succ_nat:=§(S).
 Inductive bool_as_nat:bool↔nat:=
 | ban_0:bool_as_nat(false,0)
 | ban_false:forall (n:nat), bool_as_nat(true,n)->bool_as_nat(false,S n)
 | ban_true:forall (n:nat), bool_as_nat(false,n)->bool_as_nat(true,S n).
 Theorem succ_ref_not:not_bool⇝succ_nat[bool_as_nat,bool_as_nat].
 Proof.
  unfold DatRef; simpl; intros b n Hbn; inversion_clear Hbn.
   apply ban_true; apply ban_0.
   apply ban_true; apply ban_false; apply H.
   apply ban_false; apply ban_true; apply H.
 Qed.
End Bool_as_nat.

(* Illustration 2: Naturals represented through lists of booleans --------------------------------*)
(* In this example, the abstract program is "succ" on (Peano's) naturals, and the concrete
   representation is list of booleans. The interpretation (for domain and range) is the standard
   binary encoding, with false being 0, true being 1, and the LSB being at the head of the list. *)

Section Nat_as_listbool.
 Require Import List.
 Let succ_nat:=§(S). 
 Let inc_word:=
  §(fix inc(b:list bool){struct b}:list bool:=
     match b with nil => true::nil | false::T => true::T | true::T => false::(inc T) end).
 Inductive nat_as_listbool:nat↔(list bool):=
 | nal_nil:nat_as_listbool(0,nil)
 | nal_twice:forall (b:list bool)(n:nat), nat_as_listbool(n,b)->nat_as_listbool(n+n,false::b)
 | nal_succ:forall (b:list bool)(n:nat), nat_as_listbool(n,false::b)->nat_as_listbool(S n,true::b).
 Lemma even_divide:forall (b:list bool)(n:nat), nat_as_listbool (n,false::b)->exists m:nat, n=m+m.
 Proof.
  intros b n Href; inversion_clear Href; exists n0; apply refl_equal.
 Qed.
 Lemma succ_divide:forall (n:nat), S(S(n+n))=(S n)+(S n).
 Proof.
  induction n as [ | n]; [apply refl_equal | simpl; auto].
 Qed.
 Lemma twice_inj:forall (n m:nat), n+n=m+m->n=m.
 Proof.
  induction n as [ | n Hind]; intros m; intros H.
   induction m as [ | m]; [apply refl_equal | discriminate H].
   induction m as [ | m].
    discriminate H.
    rewrite (Hind m).
     apply refl_equal.
     simpl in H; inversion H; repeat (rewrite <- (plus_n_Sm) in H1);
      inversion_clear H1; apply refl_equal.
 Qed.
 Theorem inc_ref_succ:succ_nat⇝inc_word[nat_as_listbool,nat_as_listbool].
 Proof.
  unfold DatRef; intros n b; generalize b n; clear n b; induction b;
   intros n H.
   inversion_clear H; simpl; apply nal_succ; apply (nal_twice nil 0);
    apply nal_nil.
   destruct a.
    simpl; inversion H; destruct (even_divide _ _ H1); rewrite H3; rewrite succ_divide;
     apply nal_twice; apply IHb; rewrite H3 in H1; inversion H1; rewrite <- (twice_inj _ _ H4);
     apply H5.
    simpl; inversion H; apply nal_succ; apply nal_twice; apply H1.
 Qed.
End Nat_as_listbool.

(* Existence of a refinement ---------------------------------------------------------------------*)

Section DatRef_Bd.

 Variables Da Ra Dc Rc:Set. (* Domain/range, abstract/concrete *)
 Variable fa:Da->Ra. (* Abstract program *)
 Variable Id:Da↔Dc. (* Domain interpretation *)
 Variable Ir:Ra↔Rc. (* Range interpretation *)

 Inductive rng1:Dc↔Rc:= (* Magic operator that identify acceptable images for a value of Dc *)
  rng1_:forall (xc:Dc)(yc:Rc), (forall (xa:Da), Id(xa,xc)->(Ir@⊙(fa xa)) yc)->rng1(xc,yc).

 Theorem rng1_ref:forall (fc:Dc->Rc), §(fa)⇝§(fc)[Id,Ir]<->forall (xc:Dc), rng1(xc,fc xc).
 Proof.
  intros fc; split.
   intros Href xc; apply rng1_; intros xa Hxa; apply RImg_ with (a:=fa xa);
    [apply SSin_ | apply Href; apply Hxa].
   intros fc_spec; unfold DatRef; simpl; intros xa xc Hac; generalize (fc_spec xc); intros Hfc;
    inversion_clear Hfc; generalize (H xa Hac); intros H0; inversion_clear H0; inversion H1;
    rewrite H0; apply H2.
 Qed.

 Inductive rng2:Dc↔Rc:= (* Another magic operator, more computable *)
  rng2_:forall (xc:Dc)(yc:Rc), (forall (xa:Da), ((↩Id)@⊙(xc)) xa->(Ir@⊙(fa xa)) yc)->rng2(xc,yc).

 Theorem rng1_rng2:forall (xc:Dc)(yc:Rc), rng1(xc,yc)<->rng2(xc,yc).
 Proof.
  intros xc yc; split; intros Hrng; inversion_clear Hrng.
   apply rng2_; intros xa Hxa; apply H; inversion_clear Hxa; inversion_clear H1; inversion_clear H0;
    apply H2.
   apply rng1_; intros xa Hxa; apply H; apply RImg_ with (a:=xc).
    apply SSin_.
    apply RInv_; apply Hxa.
 Qed.

 Variable list_Da:⇑Da->list Da. (* enum list all elements of any subset of  (finite) Da *)
 Variable list_Da_left:forall (A:⇑Da)(a:Da), A a->In a (list_Da A).
 Variable list_Da_right:forall (A:⇑Da)(a:Da), In a (list_Da A)->A a.
 Inductive rng3:Dc↔Rc:= (* Another magic operator, still more computable *)
  rng3_:forall (xc:Dc)(yc:Rc),
        (forall (xa:Da), In xa (list_Da((↩Id)@⊙(xc)))->(Ir@⊙(fa xa)) yc)->rng3(xc,yc).

 Theorem rng2_rng3:forall (xc:Dc)(yc:Rc), rng2(xc,yc)<->rng3(xc,yc).
 Proof.
  intros xc yc; split; intros Hrng; inversion_clear Hrng.
   apply rng3_; intros xa Hxa; apply H; apply list_Da_right; apply Hxa.
   apply rng2_; intros xa Hxa; apply H; apply list_Da_left; apply Hxa.
 Qed.

 Fixpoint inter_rng4(L:list Da){struct L}:⇑Rc:=
  match L with nil => ⊛(Rc) | xa::L' => (Ir@⊙(fa xa))∩(inter_rng4 L') end.
 Theorem inter_rng4_in:forall (xc:Rc)(L:list Da),
                       (inter_rng4 L) xc<->(forall (xa:Da), In xa L->(Ir@⊙(fa xa)) xc).
 Proof.
  intros xc; induction L as [ | xa L]; simpl; split; intros Hxc.
   intros xa HF; destruct HF.
   apply SFul_.
   intros xa' H; destruct H as [Heq | HIn].
    rewrite <- Heq in *; clear xa' Heq; apply (SInt_def_l _ _ _ _ Hxc).
    apply ((proj1 IHL) (SInt_def_r _ _ _ _ Hxc) _ HIn).
   apply SInt_.
    apply (Hxc xa); left; apply refl_equal.
    apply (proj2 IHL); intros xa' H; apply (Hxc xa'); right; apply H.
 Qed.
 Inductive rng4:Dc↔Rc:= (* Yet another... Well, you got the idea *)
  rng4_:forall (xc:Dc)(yc:Rc), (inter_rng4 (list_Da((↩Id)@⊙(xc)))) yc->rng4(xc,yc).
 Theorem rng3_rng4:forall (xc:Dc)(yc:Rc), rng3(xc,yc)<->rng4(xc,yc).
 Proof.
  intros xc yc; split; intros Hrng; inversion_clear Hrng.
   apply rng4_; apply (proj2 (inter_rng4_in yc (list_Da ((↩Id) @ ⊙(xc))))); apply H.
   apply rng3_; apply (proj1 (inter_rng4_in yc (list_Da ((↩Id) @ ⊙(xc)))) H).
 Qed.

 Theorem fc_spec:forall (fc:Dc->Rc),
                 §(fa)⇝§(fc)[Id,Ir]<->forall (xc:Dc), (inter_rng4 (list_Da((↩Id)@⊙(xc))))(fc xc).
 Proof.
  intros fc; split.
   intros Href; intros xc; generalize (proj1 (rng1_ref fc) Href); intros H1.
   generalize (proj1 (rng1_rng2 xc (fc xc)) (H1 xc)); intros H2.
   generalize (proj1 (rng2_rng3 xc (fc xc)) H2); intros H3.
   generalize (proj1 (rng3_rng4 xc (fc xc)) H3); intros H4.
   inversion H4; apply H0.
   intros Hfc; apply (proj2 (rng1_ref fc)); intros xc.
   apply (proj2 (rng1_rng2 xc (fc xc))).
   apply (proj2 (rng2_rng3 xc (fc xc))).
   apply (proj2 (rng3_rng4 xc (fc xc))).
   apply rng4_; apply Hfc.
 Qed.

 Variable choice_Rc:⇑Rc->Rc. (* Choice returns an element belonging to any subset of Rc *)
 Variable choice_Rc_spec:forall (C:⇑Rc), (exists c:Rc, C c)->C(choice_Rc(C)).
 Definition DatRef_ex:=forall (xc:Dc), exists c : Rc, inter_rng4 (list_Da ((↩Id) @ ⊙(xc))) c.
 Theorem fc_valid:DatRef_ex->
                  §(fa)⇝§(fun xc:Dc=>choice_Rc(inter_rng4 (list_Da((↩Id)@⊙(xc)))))[Id,Ir].
 Proof.
  intros Hex; apply (proj2 (fc_spec (fun xc:Dc=>choice_Rc(inter_rng4 (list_Da((↩Id)@⊙(xc))))))).
  intros xc; apply choice_Rc_spec; apply Hex.
 Qed.
 (* Nota: Various other lemmas about sufficient conditions can be derived; e.g. for a refinement to
    exists it is sufficient that RInj Id /\ ∏2(fa)⊆∏1(Ir). *)

End DatRef_Bd.

(* Refining identity -----------------------------------------------------------------------------*)

Theorem DatRef_id:forall (Sa Dc Rc:Set)(g:Dc->Rc)(Id:Sa↔Dc)(Ir:Sa↔Rc),
                  §(fun x:Sa=>x)⇝§(g)[Id,Ir]<->((⇔g)∘Id⊆Ir).
Proof.
 unfold DatRef; simpl; intros Sa Dc Rc g Id Ir; split.
  intros HRef [xa yc] H; inversion_clear H; rename b into xc; inversion_clear H1; apply HRef;
   apply H0.
  intros HInc xa xc H; apply HInc; apply RCmp_ with (b:=xc); [apply H | apply RofF_].
Qed.
(* Nota: this theorem shed light on side conditions related to the monotony of refinement w.r.t.
   composition. In the absence of glue (function g) one can read this theorem as providing the
   compatibility condition with g=id, that is Id⊆Ir. When there is a glue, the condition is for this
   glue to refine identity. *)

Theorem DatRef_id_r:forall (A C:Set)(g:A->C)(I:A↔C),
                    §(fun x:A=>x)⇝§(g)[Ⅰ(A),I]<->(forall a:A, I(a,g a)).
Proof.
 unfold DatRef; simpl; intros A C g I; split.
  intros HRef a; apply HRef; apply RId_.
  intros HI a c H; inversion_clear H; apply HI.
Qed.
(* Nota: this theorem is about reifications, i.e. translations from abstract to concrete. For a
   reification to exist, I has to include the projection (and to be total). *)

Theorem DatRef_id_l:forall (A C:Set)(g:C->A)(I:A↔C), §(fun x:A=>x)⇝§(g)[I,Ⅰ(A)]<->I⊆↩(⇔g).
Proof.
 unfold DatRef; simpl; intros A C g I; split.
  intros HRef [a c] Hac; generalize (HRef _ _ Hac); intros H; inversion_clear H; apply RInv_;
   apply RofF_.
  intros HI a c Hac; generalize (HI _ Hac); intros H; inversion_clear H; inversion_clear H0;
   apply RId_.
Qed.
(* Nota: this theorem is about interpretations, i.e. translations from concrete to abstract. For an
   interpretation to exist, I has to be included in the interpretation (and to be injective). *)

(*================================================================================================*)