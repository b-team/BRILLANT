(* Copyright (C) 1998 Ian Zimmerman <itz@rahul.net> *)
(* $Id: expect_lib.mli,v 1.1 2001/04/27 11:00:12 lefessan Exp $ *)

(* Public interface for Expect library - composed from submodule*)
(* interfaces *)

module type PTY = sig

  exception None_available

  val open_master: unit -> (Unix.file_descr * string)
  (* Attempt to allocate a master pty device.  Return a pair*)
  (* (MASTER_DESCRIPTOR, SLAVE_DEVICE_NAME) if successful; otherwise raise*)
  (* None_available *)

  val open_slave: Unix.file_descr -> string -> Unix.file_descr
  (* open_slave MASTER_DESCRIPTOR SLAVE_NAME opens the slave side of the*)
  (* pty connection after the master side has been opened by ptym_open.*)
  (* Returns the slave file descriptor.*)

  type init_params =
      No_tty_copy                    (*do not copy the modes of current tty*)
    | No_tty_init                    (*do not initialize into sane state*)
    | Stty_init of (Unix.terminal_io * Unix.terminal_io)

  val fork: init_params list -> (int * Unix.file_descr * string)
  (* fork is a higher level function that uses open_master and*)
  (* open_slave.  It forks a new process and returns a triple *)
  (* (CHILD_PID, MASTER_DESCRIPTOR, SLAVE_NAME) to the parent.*)
  (* The new child's stdin and stdout are connected to the slave *)
  (* side of a pty whose master side is MASTER_DESCRIPTOR.  In the*)
  (* child, the triple (0, Unix.stdout, SLAVE_NAME) is returned.*)

  (* In addition, fork performs a three-stage initialization of the*)
  (* termios modes on the pty device, as directed by the list argument.*)
  (* Unless No_tty_copy is specified, the mode of the current*)
  (* controlling terminal are first copied onto the pty.  Unless*)
  (* No_tty_init is specified, the pty is then subjected to the*)
  (* equivalent of "stty sane".  Lastly, if any *)
  (* Stty_init(MODES_TO_SET, MODES_TO_CLEAR) items are in the argument*)
  (* list, the true and nonzero members of MODES_TO_SET are copied onto*)
  (* the pty, and the true members of MODES_TO_CLEAR are cleared on the*)
  (* pty.  Only the boolean members of MODES_TO_CLEAR are significant.*)

end

module Pty: PTY

module type SPAWN = sig

  val bufsize: int ref
  (* size of newly created buffers, default: 8092 *)

  val trace: bool ref

  (* If !TRACE is set, the expect function will report details of the*)
  (* sent commands on stderr.  The reports look like*)
  (* Sending: command*)

  type id
  (* expect and interact connections are identified by this *)

  exception Wrong_kind of string

  val to_descr: id -> Unix.file_descr
  val to_pid: id -> int

  val spawn_file: Unix.file_descr -> bool -> id
  (* Open connection to a file.  Argument tells if file descriptor*)
  (* should be left open when connection is closed.*)

  val spawn_process: string -> string array -> Pty_imp.init_params list -> id
  (* spawn_process PROG ARGS MODES forks a new process using*)
  (* Pty.pty_fork MODES, executes PROG with ARGS in the child and opens*)
  (* a connection to the master pty.*)

  val close: id -> unit
  (* Closes the connection identified by SPAWN_ID.  If the connection is*)
  (* of File type and LEAVEOPEN has been specified, closes the*)
  (* associated file descriptor as well.*)
      
  val send: id -> string -> unit
  (* Sends a string to the connection.  send SPAWN_ID STR is essentially*)
  (* equivalent to Unix.write (to_descr SPAWN_ID) STR (String.length STR),*)

  val matched_string: id -> string
  (* Returns the string that was actually matched by last successful*)
  (* invocation of expect.*)

  val group_beginning: id -> int -> int
  (* group_beginning id N returns the offset of the substring matched by*)
  (* the Nth parenthesized group from the beginning of the string*)
  (* returned by matched_string.  Note this is a different convention*)
  (* from Str.group_beginning.*)

  val group_end: id -> int -> int
  (* Same as group_beginning for the end of the matched substring.*)
end

module Spawn: SPAWN

module type EXPECT = sig

  val trace: bool ref

  (* If !TRACE is set, the expect function will report details of the*)
  (* pattern matches on stderr.  The reports look like*)
  (* PATTERN ~ STRING -> RESULT where RESULT is true or false.*)

  val timeout: float ref

  exception Timeout
  (* If none of the alternatives passed to expect match within !TIMEOUT*)
  (* seconds, this exception is raised.*)

  type expect_condition =
      Eof
    | Regexp of string
    | Exact of string

  type 't expect_clause = 't * Spawn.id * expect_condition

  val expect: 't expect_clause list -> 't expect_clause
  (* expect CLAUSES repeatedly tries to match the patterns specified by*)
  (* CLAUSES with the output on the connections in CLAUSES.  The first*)
  (* CLAUSE to match is returned as result.  Each CLAUSE should look*)
  (* like (TAG, SPAWN_ID, PATTERN) where TAG is an enumeration value*)
  (* used for matching the result and PATTERN is one of three types: Eof*)
  (* to match the end-of-file condition on SPAWN_ID, Regexp(REGEXP_STRING)*)
  (* to match the output of SPAWN_ID with a regexp in the normal string*)
  (* notation, or Exact(STRING) to match with a fixed string.*)
end

module Expect: EXPECT
