(*==================================================================================================
  Project : LambdaTdB
  Module : tech.v
  We study here in the details the technical properties of the calculus defined in term.v
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/ANSSI & UPMC/LIP6 - St: Aug 2009, Md: Sep 2009
  Provided under licence CeCILL-B
  ================================================================================================*)

Require Export beta.

(* Commutation of liftings -----------------------------------------------------------------------*)

Theorem lift_lift_:forall (t1 t2:Τ)(l:Λ)(m1 m2:Μ), m1 t2<=m2 t2->
                   lift m1 t1 (lift m2 t2 l)=lift (minc m2 t1) t2 (lift m1 t1 l).
Proof.
 intros t1 t2; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros m1 m2 Hm.
  unfold dangling; destruct (LTyp_eq t1 t) as [Ht1t | Ht1t]; [rewrite Ht1t in * | ];
   destruct (LTyp_eq t2 t) as [Ht2t | Ht2t]; try (rewrite Ht2t in *); my_simpl;
   try (apply refl_equal); repeat (rewrite (minc_eq)).
   destruct (LIdx_le (m2 t) i) as [Hm2ti | Hm2ti]; my_simpl.
    destruct (LIdx_le (m1 t) i) as [Hm1ti | Hm1ti]; my_simpl.
     apply refl_equal.
     destruct ((Lt.lt_not_le _ _ (Lt.le_lt_trans _ _ _ Hm2ti Hm1ti)) Hm).
    destruct (LIdx_le (m1 t) i) as [Hm1ti | Hm1ti]; my_simpl; apply refl_equal.
  rewrite Hl; [ | apply minc_le; apply Hm].
   rewrite (minc_ext (minc_comm m2 t1 t)); apply refl_equal.
  rewrite Hl1; [ | apply Hm]; rewrite Hl2; [ | apply Hm]; apply refl_equal.
Qed.

Theorem lift_lift_eq:forall (t1 t2:Τ)(l:Λ)(m:Μ),
                     lift (minc m t2) t1 (lift m t2 l)=lift (minc m t1) t2 (lift m t1 l).
Proof.
 intros t1 t2; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros m.
  unfold dangling; destruct (LTyp_eq t1 t) as [Ht1t | Ht1t]; [rewrite Ht1t in * | ];
   destruct (LTyp_eq t2 t) as [Ht2t | Ht2t]; try (rewrite Ht2t in *); my_simpl;
   try (apply refl_equal).
 rewrite (minc_ext (minc_comm m t t2)); rewrite (minc_ext (minc_comm m t t1)); rewrite Hl;
  apply refl_equal.
 rewrite Hl1; rewrite Hl2; apply refl_equal.
Qed.

(* Commutation of lifting and elimination --------------------------------------------------------*)

Theorem lift_elim_:forall (te tl:Τ)(l le:Λ)(me ml:Μ), me tl<=ml tl->
                   lift ml tl (elim me te le l)=elim me te (lift ml tl le) (lift (minc ml te) tl l).
Proof.
 intros te tl; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros le me ml Hm.
  unfold dangling; destruct (LTyp_eq te t) as [Htet | Htet]; [rewrite Htet in * | ];
   destruct (LTyp_eq tl t) as [Htlt | Htlt]; try (rewrite Htlt in *); my_simpl; simpl;
   unfold dangling; my_simpl; try (apply refl_equal).
   destruct (LIdx_le (me t) i) as [Hmeti | Hmeti].
    destruct (LIdx_eq (me t) i) as [H'meti | H'meti]; [rewrite H'meti in * | ].
     rewrite minc_eq; my_simpl; destruct (LIdx_le (S (ml t)) i) as [HSmlti | HSmlti].
      destruct (Le.le_Sn_n _ (Le.le_trans _ _ _ HSmlti Hm)).
      my_simpl; apply refl_equal.
     rewrite minc_eq; my_simpl; destruct (LIdx_le (S (ml t)) i) as [HSmlti | HSmlti].
      my_simpl; destruct (LIdx_eq (me t) (S i)) as [HmetSi | HmetSi].
       rewrite HmetSi in Hmeti; destruct (Le.le_Sn_n _ Hmeti).
       simpl; unfold dangling; my_simpl; destruct i as [| i].
        inversion Hmeti; destruct H'meti; apply H0.
        simpl; my_simpl; apply refl_equal.
      my_simpl; simpl; unfold dangling; my_simpl; destruct i as [| i].
        inversion Hmeti; destruct H'meti; apply H0.
        simpl; my_simpl; rewrite (gt_simpl (Lt.lt_S_n _ _ HSmlti)); apply refl_equal.
    rewrite minc_eq; destruct (LIdx_le (S (ml t)) i) as [HSmlti | HSmlti].
     destruct ((Lt.lt_not_le _ _ (Lt.lt_trans _ _ _ HSmlti Hmeti)) Hm).
     my_simpl; simpl; unfold dangling; my_simpl; apply refl_equal.
   destruct (LIdx_le (me t) i) as [Hmeti | Hmeti].
    destruct (LIdx_eq (me t) i) as [H'meti | H'meti].
     apply refl_equal.
     simpl; unfold dangling; my_simpl; apply refl_equal.
    simpl; unfold dangling; my_simpl; apply refl_equal.
  rewrite Hl; [ | apply minc_le; apply Hm]; rewrite <- lift_lift_; [ | apply Hm];
   rewrite (minc_ext (minc_comm ml t te)); apply refl_equal.
  rewrite Hl1; [ | apply Hm]; rewrite Hl2; [ | apply Hm]; apply refl_equal.
Qed.

Theorem lift_elim'_lemma:forall (t:Τ)(l:Λ)(m1 m2:Μ), m1 t=m2 t->lift m1 t l=lift m2 t l.
Proof.
 intros tl; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros m1 m2 Ht.
  unfold dangling; rewrite Ht; apply refl_equal.
  rewrite (Hl _ _ (minc_eq_eq Ht t)); apply refl_equal.
  rewrite (Hl1 _ _ Ht); rewrite (Hl2 _ _ Ht); apply refl_equal.
Qed.

Theorem lift_elim'_:forall (te tl:Τ)(l le:Λ)(me ml:Μ), (forall (t':Τ), ml t'<=me t')->
                    valid ml me le=true->
                    elim (minc me tl) te (lift me tl le) (lift ml tl l)=
                    lift ml tl (elim me te le l).
Proof.
 intros te tl; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros le me ml Hm Hle.
  unfold dangling; destruct (LTyp_eq te t) as [Htet | Htet]; [rewrite Htet in * | ];
   destruct (LTyp_eq tl t) as [Htlt | Htlt]; try (rewrite Htlt in * ); my_simpl; simpl;
   unfold dangling; my_simpl; try (apply refl_equal).
   rewrite minc_eq; destruct (LIdx_le (me t) i) as [Hmeti | Hmeti]; my_simpl.
    generalize (Hm t); intros Hmlti; my_simpl;
     destruct (LIdx_eq (me t) i) as [H'meti | H'meti]; [rewrite H'meti in * | ]; my_simpl.
      apply sym_equal; apply lift_valid_; [apply Hm | apply Hle].
      simpl; destruct i as [| i]; simpl.
       inversion Hmeti; destruct H'meti; apply H0.
       simpl in *; destruct (Lt.le_lt_or_eq _ _ Hmeti) as [H''meti | H''meti]; unfold dangling;
        my_simpl.
        rewrite (le_simpl (Le.le_trans _ _ _ Hmlti (Le.le_S_n _ _ H''meti))); apply refl_equal.
        destruct H'meti; apply H''meti.
    destruct (LIdx_le (ml t) i) as [Hmlti | Hmlti]; my_simpl; simpl; unfold dangling;
     my_simpl; apply refl_equal.
   destruct (LIdx_le (me t) i) as [Hmeti | Hmeti].
    destruct (LIdx_eq (me t) i) as [H'meti | H'meti]; [rewrite H'meti in * | ].
     apply sym_equal; apply lift_valid_; [apply Hm | apply Hle].
     simpl; unfold dangling; destruct i as [| i].
      inversion Hmeti; destruct H'meti; apply H0.
      my_simpl; apply refl_equal.
    simpl; unfold dangling; my_simpl; apply refl_equal.
  rewrite (minc_ext (minc_comm me t tl)).
  rewrite <- (Hl (lift me t le) (minc me t) (minc ml t)).
   rewrite <- lift_lift_eq; apply refl_equal.
   intros t'; apply minc_le; apply Hm.
   apply valid_lift_; [apply Hm | apply Hle].
  rewrite Hl1; [ | apply Hm | apply Hle]; rewrite Hl2; [ | apply Hm | apply Hle]; apply refl_equal.
Qed.

Theorem elim_lift'_:forall (te:Τ)(l le:Λ)(m1 m2:Μ),
                    m1 te<=m2 te->valid m1 m2 l=true->elim m1 te le (lift m2 te l)=l.
Proof.
 intros te; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros le m1 m2 Hm Hval.
  unfold dangling; destruct (LTyp_eq te t) as [Htet | Htet]; [rewrite Htet in * | ]; my_simpl;
  [ | apply refl_equal].
   destruct (LIdx_le (m2 t) i) as [Hm2ti | Hm2ti]; my_simpl.
    generalize (Le.le_trans _ _ _ Hm Hm2ti); intros Hm1ti; my_simpl;
     destruct (LIdx_eq (m1 t) (S i)) as [Hm1tSi | Hm1tSi].
      rewrite Hm1tSi in Hm1ti; destruct (Le.le_Sn_n _ Hm1ti).
      apply refl_equal.
    destruct (LIdx_le (m1 t) i) as [Hm1ti | Hm1ti]; my_simpl.
     inversion Hval.
     apply refl_equal.
  rewrite Hl; [apply refl_equal | apply minc_le; apply Hm | apply Hval].
  destruct (andb_prop _ _ Hval) as [Hval1 Hval2]; rewrite (Hl1 le _ _ Hm Hval1);
   rewrite (Hl2 le _ _ Hm Hval2); apply refl_equal.
Qed.
(* Nota: It is sufficient for this result to have l valid for type t only; yet it is a meaningless
   condition as m1 and m2 records a context for all types and that l has been lifted accordingly. *)

(* Commutation of eliminations -------------------------------------------------------------------*)

Theorem elim_elim_:forall (t1 t2:Τ)(l le1 le2:Λ)(m1 m2:Μ),
                   (forall (t':Τ), m2 t'<=m1 t')->
                   valid m2 m1 le1=true->
                   elim m1 t1 le1 (elim m2 t2 le2 l)=
                   elim m2 t2 (elim m1 t1 le1 le2) (elim (minc m1 t2) t1 (lift m1 t2 le1) l).
Proof.
 intros t1 t2; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros le1 le2 m1 m2 Hm Hval.
  unfold dangling; destruct (LTyp_eq t1 t) as [Ht1t | Ht1t]; [rewrite Ht1t in * | ];
   destruct (LTyp_eq t2 t) as [Ht2t | Ht2t]; try (rewrite Ht2t in *); my_simpl.
   rewrite minc_eq; destruct (LIdx_le (m2 t) i) as [Hm2ti | Hm2ti].
    destruct (LIdx_eq (m2 t) i) as [H'm2ti | H'm2ti].
     destruct (LIdx_le (S (m1 t)) i) as [HSm1ti | HSm1ti].
      rewrite <- H'm2ti in HSm1ti; destruct (Le.le_Sn_n _ (Le.le_trans _ _ _ HSm1ti (Hm t))).
      simpl; unfold dangling; my_simpl; apply refl_equal.
     destruct (LIdx_le (S (m1 t)) i) as [HSm1ti | HSm1ti].
      destruct (LIdx_eq (S (m1 t)) i) as [H'Sm1ti | H'Sm1ti].
       simpl; unfold dangling; my_simpl; destruct i as [| i].
        inversion HSm1ti.
        simpl in *; injection H'Sm1ti; intros Hm1ti; my_simpl; rewrite elim_lift'_;
        [apply refl_equal | apply Hm | apply Hval].
       simpl; unfold dangling; my_simpl; destruct i as [| i].
        inversion HSm1ti.
        simpl in *; my_simpl; destruct (LIdx_eq (m1 t) i) as [Hm1ti | Hm1ti].
         destruct H'Sm1ti; rewrite Hm1ti; apply refl_equal.
         destruct (LIdx_le (m2 t) i) as [H''m2ti | H''m2ti].
          destruct (LIdx_eq (m2 t) i) as [H'''m2ti | H'''m2ti].
           rewrite <- H'''m2ti in *; destruct Hm1ti;
            apply (Le.le_antisym _ _ (Le.le_S_n _ _ HSm1ti) (Hm t)).
           apply refl_equal.
          destruct (Lt.le_lt_or_eq _ _ H''m2ti) as [H'''m2ti | H'''m2ti].
           destruct (Lt.lt_irrefl _ (Lt.lt_le_trans _ _ _ H'''m2ti Hm2ti)).
           destruct H'm2ti; apply sym_equal; apply H'''m2ti.
      simpl; unfold dangling; my_simpl; destruct i as [| i].
       inversion Hm2ti; destruct H'm2ti; apply H0.
       simpl in *; rewrite (gt_simpl (Lt.lt_S_n _ _ HSm1ti)); apply refl_equal.
    generalize (Lt.lt_le_trans _ _ _ Hm2ti (Hm t)); intros Hm1ti; my_simpl; simpl; unfold dangling;
     my_simpl; apply refl_equal.
   simpl; unfold dangling; my_simpl; destruct (LIdx_le (m1 t) i) as [Hm1ti | Hm1ti].
    generalize (Le.le_trans _ _ _ (Hm t) Hm1ti); intros Hm2ti;
     destruct (LIdx_eq (m1 t) i) as [H'm1ti | H'm1ti].
     apply sym_equal; apply elim_lift'_; [apply Hm | apply Hval].
     simpl; unfold dangling; my_simpl; apply refl_equal.
    simpl; unfold dangling; my_simpl; apply refl_equal.
   destruct (LIdx_le (m2 t) i) as [Hm2ti | Hm2ti].
    destruct (LIdx_eq (m2 t) i) as [H'm2ti | H'm2ti].
     simpl; unfold dangling; my_simpl; apply refl_equal.
     simpl; unfold dangling; my_simpl; apply refl_equal.
    simpl; unfold dangling; my_simpl; apply refl_equal.
   simpl; unfold dangling; my_simpl; apply refl_equal.
  rewrite Hl; [ | intros t'; apply minc_le; apply Hm | apply valid_lift_; [apply Hm | apply Hval]];
   rewrite lift_elim'_; [ | apply Hm | apply  Hval]; rewrite (minc_ext (minc_comm m1 t t2));
   rewrite lift_lift_eq; apply refl_equal.
  rewrite Hl1; [ | apply Hm | apply Hval]; rewrite Hl2; [ | apply Hm | apply Hval];
   apply refl_equal.
Qed.

(* Commutation of lifting and beta ---------------------------------------------------------------*)

Theorem beta_lift_beta:forall (l l':Λ), l→βl'->forall (tl:Τ)(m:Μ), (lift m tl l)→β(lift m tl l').
Proof.
 intros l l' Hll'; induction Hll'; intros tl hl.
  simpl; rewrite lift_elim_; [apply beta_red | unfold root; apply Le.le_O_n].
  simpl; apply beta_lam; apply IHHll'.
  simpl; apply beta_app_l; apply IHHll'.
  simpl; apply beta_app_r; apply IHHll'.
Qed.

(* Commutation of elimination and beta -----------------------------------------------------------*)

Theorem beta_elim_le_:forall (te:Τ)(l le le':Λ)(me:Μ), le→βle'->
                      (elim me te le l)→*β(elim me te le' l).
Proof.
 intros te; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros le le' me Hbeta.
  unfold dangling; destruct (LTyp_eq te t) as [Htet | Htet]; [ | apply bs_refl].
   destruct (LIdx_le (me te) i) as [Hmetei | Hmetei]; [ | apply bs_refl].
    destruct (LIdx_eq (me te) i) as [H'metei | H'metei];
    [apply bs_beta; apply Hbeta | apply bs_refl].
  generalize (beta_lift_beta _ _ Hbeta t me); intros H'beta.
  generalize (Hl _ _ (minc me t) H'beta); intros Hind; induction Hind.
   apply bs_beta; apply beta_lam; apply b.
   apply bs_refl.
   apply bs_tran with (l2:=∆(t,l2)).
    apply IHHind.
    apply beta_lam; apply b.
  generalize (Hl1 _ _ me Hbeta); intros Hind1; induction Hind1.
   apply beta_star_tran with (l2:=l3@elim me te le l2).
    apply beta_app_l; apply b.
    apply beta_star_app_r; apply Hl2; apply Hbeta.
   apply beta_star_app_r; apply Hl2; apply Hbeta.
   apply beta_star_tran' with (l2:=l0@elim me te le' l2).
    apply beta_star_app_r; apply Hl2; apply Hbeta.
    apply beta_star_app_l; apply bs_tran with (l2:=l3); [apply Hind1 | apply b].
Qed.
(* Nota: provided le is reduced in one step into le', we need the reflexive closure because
   elimination can be without effect if the appropriate free variable does not appear in l.
   We also need the transitive closure because in an application, an eager evaluation will only
   reduce le once, whereas a lazy evaluation can duplicate le before reduction.                   *)

Theorem beta_elim_le_star_:forall (te:Τ)(l le le':Λ)(me:Μ), le→*βle'->
                           (elim me te le l)→*β(elim me te le' l).
Proof.
 intros te l le le' me Hbeta; induction Hbeta.
  apply beta_elim_le_; apply b.
  apply bs_refl.
  apply beta_star_tran' with (l2:=elim me te l2 l).
   apply IHHbeta.
   apply beta_elim_le_; apply b.
Qed.

Theorem beta_elim_l_:forall (te:Τ)(l l':Λ), l→βl'->
                     forall (le:Λ)(me:Μ), valid μ me le=true->(elim me te le l)→β(elim me te le l').
Proof.
 intros te l l' Hbeta; induction Hbeta; intros le me Hval; simpl.
  rewrite elim_elim_; [apply beta_red | intros t; unfold root; apply Le.le_O_n | apply Hval].
  apply beta_lam; apply IHHbeta; apply lift_valid_root; apply Hval.
  apply beta_app_l; apply IHHbeta; apply Hval.
  apply beta_app_r; apply IHHbeta; apply Hval.
Qed.

(*================================================================================================*)