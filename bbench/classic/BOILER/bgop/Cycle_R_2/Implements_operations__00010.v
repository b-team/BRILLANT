 

Require Import Blib.

Theorem op:
forall _CAP _DT _M1 _M2 _N1 _N2 _NB_PUMP _PNOMINAL _PUMP1 _PUMP2 _PUMP3 _PUMP4 _U1 _U2 _WMAX _acq_rfam _acq_rim _acq_rr _acq_rrm _pump_number _pump_number _rc _rfam _rfm _rim _rok _rok _rpcl_0 _rpop_0 _rr _rram _rrm _rtk _rtk, ( 
 (In BN _pump_number) -> 
(In BBOOL _rtk) -> 
(In BBOOL _rok) -> 
(In BN1 _N1) -> 
(In BN1 _N2) -> 
(In BN1 _M1) -> 
(In BN1 _M2) -> 
(In BN1 _CAP) -> 
(_N1 < _N2) -> 
(_M1 < _M2) -> 
(_M1 < _N1) -> 
(_N2 < _M2) -> 
(_M2 < _CAP) -> 
(In BN _NB_PUMP) -> 
(_NB_PUMP = 4) -> 
(In BN _PUMP1) -> 
(In BN _PUMP2) -> 
(In BN _PUMP3) -> 
(In BN _PUMP4) -> 
(_PUMP1 = 1) -> 
(_PUMP2 = 2) -> 
(_PUMP3 = 3) -> 
(_PUMP4 = 4) -> 
(In (interval 1 _NB_PUMP) _PUMP1) -> 
(In (interval 1 _NB_PUMP) _PUMP2) -> 
(In (interval 1 _NB_PUMP) _PUMP3) -> 
(In (interval 1 _NB_PUMP) _PUMP4) -> 
(In BN1 _WMAX) -> 
(In BN1 _U1) -> 
(In BN1 _U2) -> 
(In BN1 _DT) -> 
(In BN1 _PNOMINAL) -> 
(In BN1 (((_PNOMINAL + _PNOMINAL) + _PNOMINAL) + _PNOMINAL)) -> 
(In BN1 ((((_PNOMINAL + _PNOMINAL) + _PNOMINAL) + _PNOMINAL) * _DT)) -> 
(In BN1 (_U2 * _DT)) -> 
(In BN1 (_U1 * _DT)) -> 
(In BN1 ((_U1 * _DT) + _WMAX)) -> 
((_U2 * _DT) <= _WMAX) -> 
(In BN1 (2 * _WMAX)) -> 
(In BN1 (_WMAX * _DT)) -> 
(In BN1 ((_U1 * _DT) * _DT)) -> 
(In BN1 ((_U2 * _DT) * _DT)) -> 
(In BN1 ((((_U1 * _DT) * _DT) + 1) / 2)) -> 
(In BN1 ((((_U2 * _DT) * _DT) + 1) / 2)) -> 
(In BN1 ((4 * _PNOMINAL) + ((1 + ((_U2 * _DT) * _DT)) / 2))) -> 
(In BN1 ((_CAP + ((4 * _PNOMINAL) * _DT)) + ((1 + ((_U2 * _DT) * _DT)) / 2))) -> 
(In BZ (((1 + ((_U2 * _DT) * _DT)) / 2) - (_WMAX * _DT))) -> 
(In BN1 (_CAP + ((4 * _PNOMINAL) * _DT))) -> 
(In BZ (- ((_WMAX * _DT) + ((1 + ((_U2 * _DT) * _DT)) / 2)))) -> 
(In BBOOL _rim) -> 
(In BBOOL _rrm) -> 
(In BBOOL _rr) -> 
(In BBOOL _rpop_0) -> 
(In BBOOL _rpcl_0) -> 
(In BBOOL _rc) -> 
(In BBOOL _rfm) -> 
(In BBOOL _rram) -> 
(In BBOOL _rfam) -> 
(In BBOOL _rok) -> 
(In BBOOL _rtk) -> 
(In (interval 1 _NB_PUMP) _pump_number)
  -> (
 (((_rok = false) /\ ((app _acq_rrm (_pump_number) _) = true)) -> (~ ((((app _acq_rr (_pump_number) _) = (app _acq_rr (_pump_number) _)) /\ ((_rok = true) \/ ((app _acq_rrm (_pump_number) _) = true)))) -> (~ (((((app _acq_rr (_pump_number) _) <> (app _acq_rr (_pump_number) _)) /\ ((_rok = true) \/ ((app _acq_rrm (_pump_number) _) = true))) \/ ((_rfm = true) /\ ((app _acq_rfam (_pump_number) _) = false)))) -> (~ (((((app _acq_rim (_pump_number) _) = true) /\ ((_rok = false) \/ ((app _acq_rrm (_pump_number) _) = false))) /\ (((app _acq_rfam (_pump_number) _) = false) \/ (_rfm = true)))) -> ~ (forall _rok_fresh_0000, ((((In BBOOL _rok_fresh_0000) -> forall _rtk_fresh_0000, ((((In BBOOL _rtk_fresh_0000) -> ~ ((1 = 1))))))))))))) ) )
 .
try intuition 
   || unfold BBOOL ; intuition ; apply Full_intro
   || red ;  red ; red ; omega.
Qed.
 