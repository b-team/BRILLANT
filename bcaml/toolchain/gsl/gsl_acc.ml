(* $Id$ *)

(* Library for GSL constructors and accessors *)

open Gsl_adt
  
(* This module defines functions to build or unassemble constructions
  of the type defined in Gsl_adt, the same way it is done in the blast
  directory. The only difference is that, due to the little size of
  the type, it is more convenient to put all accessors and
  constructors in the same module *)

let mkGsl_Affect iexprlist =
  if iexprlist = []
  then raise (Invalid_argument "mkGsl_Affect")
  else Gsl_affect(iexprlist)


let mkGsl_Skip ()= 
  Gsl_skip


let mkGsl_Precondition condition subst =
  Gsl_precondition(condition, subst)


let mkGsl_bChoice subst1 subst2 =
    Gsl_bChoice(subst1, subst2)


let mkGsl_Guard condition subst =
  Gsl_guard(condition, subst)


let mkGsl_uChoice idents subst =
  Gsl_uChoice(idents, subst)


let mkGsl_Parallel subst1 subst2 =
  Gsl_parallel(subst1, subst2)


let mkGsl_Sequence subst1 subst2 =
  Gsl_sequence(subst1, subst2)


let mkGsl_While condition subst  variant invariant=
  Gsl_while(condition, subst, variant, invariant)


let gsl_from substgen=
  match substgen with
  | Gsl_precondition(_,s) -> s
  | Gsl_guard(_,s) -> s
  | Gsl_uChoice(_,s) -> s
  | Gsl_while(_,s,_,_) -> s
  | _ -> raise ( Invalid_argument "gsl_from" )


let cond_from substgen=
  match substgen with
  | Gsl_precondition(p,_) -> p
  | Gsl_guard(p,_) -> p
  | Gsl_while(p,_,_,_) -> p
  | _ -> raise ( Invalid_argument "cond_from" )


let allids_from substgen=
  match substgen with
  | Gsl_uChoice(v,_) -> v
  | _ -> raise ( Invalid_argument "allids_from" )


let affects_from substgen=
  match substgen with
  | Gsl_affect(l) -> l
  |  _ -> raise ( Invalid_argument "affects_from" )


let twosubsts_from substgen=
  match substgen with
  | Gsl_bChoice(s1, s2) -> (s1, s2)
  | Gsl_parallel(s1, s2) -> (s1, s2)
  | Gsl_sequence(s1, s2) -> (s1, s2)
  | _ -> raise ( Invalid_argument "twosubsts_from" )


let whilepreds_from substgen=
  match substgen with
  | Gsl_while(_,_,i,v) -> (i,v)
  | _ -> raise ( Invalid_argument "while_preds_from" )

