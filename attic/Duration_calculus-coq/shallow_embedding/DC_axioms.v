(*
    Copyright (c) 2002-2009 Samuel Colin

    This file is part of a double implementation of duration calculus
    in Coq as a shallow embedding and a deep embedding, which we shall
    refer to collectively as DC-coq.

    DC-coq is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DC-coq is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DC-coq.  If not, see <http://www.gnu.org/licenses/>.
*)


Require Import IL_base.
Require Import DC_base.
Require Import Common_functions.

Definition pr (S : DCState) := forall (x:R), #S#=x /\ (l= x) /\ (x>0)%R.
Notation "[[ s ]]":=(pr s) (at level 0, no associativity).

Open Scope states_scope.

Axiom DCA1 : #0#= 0.
Axiom DCA2 : forall (x:R), #1#= x <-> l=x.
Axiom DCA3 : forall s : DCState, #s#>= 0.
Axiom
  DCA4 :
    forall (s1 s2 : DCState) (x1 x2 and12 or12:R),
    #s1#= x1 -> #s2#=x2 -> #s1 /\ s2#=and12 -> #s1 \/ s2#=or12 ->
    (x1 + x2 = and12 + or12)%R.
Axiom
  DCA5 :
    forall (x y : R) (s : DCState),
    #s#=x ^^ #s#=y -> #s#= (x+y).
Axiom
  DCA6 :
    forall s1 s2 : DCState,
    (proplogic s1 <-> proplogic s2) -> (forall (x:R), #s1#=x <-> #s2#=x).

Close Scope states_scope.
Require Export List. 

Fixpoint disjunction (LS : list DCState) : DCState :=
  match LS with
  | state :: sequel =>
      match sequel with
      | nil => state
      | _ => SOr state (disjunction sequel)
      end
  | nil => SZero
  end.

Fixpoint chop_sharing_left (X : Prop) (LS : list DCState) {struct LS} :
 Prop :=
  match LS with
  | state :: sequel =>
      match sequel with
      | nil => chop X (pr state)
      | _ => chop X (pr state) \/ (chop_sharing_left X sequel)
      end
  | nil => False
  end.

Fixpoint chop_sharing_right (X : Prop) (LS : list DCState) {struct LS} :
 Prop :=
  match LS with
  | state :: sequel =>
      match sequel with
      | nil => chop (pr state) X
      | _ => chop (pr state) X \/ chop_sharing_right X sequel 
      end
  | nil => False
  end.

Axiom
  IR1 :
    forall (H : Prop -> Prop) (LS : list DCState),
    (proplogic (disjunction LS) <-> True) ->
    H point ->
    (forall X : Prop, H X -> H (X \/ chop_sharing_left X LS)) -> H True.

Axiom
  IR2 :
    forall (H : Prop -> Prop) (LS : list DCState),
    (proplogic (disjunction LS) <-> True) ->
    H point ->
    (forall X : Prop, H X -> H (X \/ chop_sharing_right X LS)) -> H True.

Require Import Classical.

Theorem IR1_simple :
 forall (H : Prop -> Prop) (S : DCState),
 H point ->
 (forall X : Prop, H X -> H (X \/ chop X (pr S) \/ chop X (pr (SNot S)))) ->
 H True.
intros H S Hpoint IR1hypo.
apply IR1 with (LS := S :: SNot S :: nil);
 [ simpl in |- *; split; [ tauto | intros; apply classic ]
 | assumption
 | assumption ].
Qed.

Theorem IR2_simple :
 forall (H : Prop -> Prop) (S : DCState),
 H point ->
 (forall X : Prop, H X -> H (X \/ chop (pr S) X \/ chop (pr (SNot S)) X)) ->
 H True.
intros H S Hpoint IR2hypo.
apply IR2 with (LS := S :: SNot S :: nil);
 [ simpl in |- *; split; [ tauto | intros; apply classic ]
 | assumption
 | assumption ].
Qed.
