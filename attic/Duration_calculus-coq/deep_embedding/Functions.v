(*
    Copyright (c) 2002-2009 Samuel Colin

    This file is part of a double implementation of duration calculus
    in Coq as a shallow embedding and a deep embedding, which we shall
    refer to collectively as DC-coq.

    DC-coq is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DC-coq is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DC-coq.  If not, see <http://www.gnu.org/licenses/>.
*)


Require Export Definitions.

Fixpoint proplogic (s:State_expr):Formula:=
match s with
 | SE0 => FFalse
 | SE1 => FTrue
 | (State_var name) => (FLetter name)
 | (SENot t) => (FNot (proplogic t))
 | (SEOr t u) => (FOr (proplogic t) (proplogic u))
end.

Fixpoint chop_free (f:Formula):Prop:=
match f with
| FTrue => True
| (FLetter _) => True
| (FNot g) => (chop_free g)
| (FOr g h) => (and (chop_free g) (chop_free h))
| (FExists z) => (chop_free (z (RVal R0)))
| (FChop _ _) => False
| (Flt u v) => True
| (Feq u v) => True
end.

Fixpoint rigid_state (s:State_expr):Prop:=
match s with
 | SE0 => True
 | SE1 => True
 | (State_var _) => False
 | (SENot p) => (rigid_state p)
 | (SEOr p q) => (and (rigid_state p) (rigid_state q))
end.

Fixpoint rigid_term (t:DCTerm):Prop:=
match t with
| (RVar y) => True
| length => False
| (RVal u) => True
| (RDur s) => (rigid_state s)
| (DCAdd u v) => (and (rigid_term u) (rigid_term v))
| (DCOpp u) => (rigid_term u) 
| (DCMult u v) => (and (rigid_term u) (rigid_term v))
| (DCInv u) => (rigid_term u) 
end.

Fixpoint rigid (f:Formula):Prop:=
match f with
| FTrue => True
| (FLetter _) => False
| (FNot g) => (rigid g)
| (FOr g h) => (and (rigid g) (rigid h))
| (FExists z) => (rigid (z (RVal R0)))
| (FChop g h) => (and (rigid g) (rigid h))
| (Flt u v) => (and (rigid_term u) (rigid_term v))
| (Feq u v) => (and (rigid_term u) (rigid_term v))
end.

