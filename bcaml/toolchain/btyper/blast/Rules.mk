FILES = bbop.ml \
 blast.ml \
 blast_lib.ml \
 bbop_free.ml \
 blast_it.ml \
 blast_itself.ml \
 blast_maps.ml \
 options.ml

DEPS = Btyperlib

NAME = Btyperblast
MAKELIB = yes
