(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter1.
Require Import Bbasic.
Require Import Bderived_constructs.
Require Import Brelations.
Require Import Bfunctions.

Open Scope eB_scope.

(**
  BBook: section 2.6.3, p.97, array 2, row 1
  *)
Theorem Inclusion_laws_01: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), ((s ⤖ t) ⊆ (s ↠ t)).
Proof.
intros S T s t.
intros x H.
induction H.
constructor.
intuition.
induction H0; intuition.
Qed.

(**
  BBook: section 2.6.3, p.97, array 2, row 2
  *)
Theorem Inclusion_laws_02: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), ((s ⤖ t) ⊆ (s ↣ t)).
Proof.
intros S T s t.
intros x H.
induction H.
constructor.
intuition.
induction H0; intuition.
Qed.

(**
  BBook: section 2.6.3, p.97, array 2, row 3
  *)
Theorem Inclusion_laws_03: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), ((s ↠ t) ⊆ (s ⤀ t)).
Proof.
intros S T s t.
intros x H.
induction H.
intuition.
Qed.

(**
  BBook: section 2.6.3, p.97, array 2, row 4
  *)
Theorem Inclusion_laws_04: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), ((s ↣ t) ⊆ (s ⤔ t)).
Proof.
intros S T s t.
intros x H.
induction H; intuition.
Qed.

(**
  BBook: section 2.6.3, p.98, array 1, row 1
  *)
Theorem Inclusion_laws_05: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), ((s ↠ t) ⊆ (s → t)).
Proof.
intros S T s t.
intros x H.
induction H; intuition.
Qed.

(**
  BBook: section 2.6.3, p.98, array 1, row 2
  *)
Theorem Inclusion_laws_06: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), ((s ↣ t) ⊆ (s → t)).
Proof.
intros S T s t.
intros x H.
induction H; intuition.
Qed.

(**
  BBook: section 2.6.3, p.98, array 1, row 3
  *)
Theorem Inclusion_laws_07: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), ((s ⤀ t) ⊆ (s ⇸ t)).
Proof.
intros S T s t.
intros x H.
induction H; intuition.
Qed.

(**
  BBook: section 2.6.3, p.98, array 1, row 4
  *)
Theorem Inclusion_laws_08: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), ((s ⤔ t) ⊆ (s ⇸ t)).
Proof.
intros S T s t.
intros x H.
induction H; intuition.
Qed.

(**
  BBook: section 2.6.3, p.98, array 1, row 5
  *)
Theorem Inclusion_laws_09: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), ((s → t) ⊆ (s ⇸ t)).
Proof.
intros S T s t.
intros x H.
induction H; intuition.
Qed.

(**
  BBook: section 2.6.3, p.98, array 1, row 6
  *)
Theorem Inclusion_laws_10: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), ((s ⇸ t) ⊆ (s ↔ t)).
Proof.
intros S T s t.
intros x H.
induction H; intuition.
Qed.

(**
  BBook: section 2.6.3, p.98, array 1, row 7
  *)
Theorem Inclusion_laws_11: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)), (r ∈ (s ↔ t)) ⇒(id(domain(r)) ⊆ (r;r∼)).
Proof.
intros S T s t r H.
intros x H0.
induction H0; constructor.
inversion H0.
induction H4; induction H4.
exists x.
split; [ intuition | constructor; rewrite <- H1; intuition ].
Qed.

(**
  BBook: section 2.6.3, p.98, array 1, row 8
  *)
Theorem Inclusion_laws_12: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)), (r ∈ (s ↔ t)) ⇒(id(range(r)) ⊆ (r∼;r)).
Proof.
intros S T s t r H.
intros x H0.
induction H0; constructor.
inversion H0.
induction H5; induction H5.
exists x.
split; [ constructor; rewrite  H1; intuition | intuition ].
Qed.

(**
  BBook: section 2.6.3, p.98, array 1, row 9
  *)
Theorem Inclusion_laws_13: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (r: Ensemble (S*T)), (u ⊆ s ∧ r ∈ (s ↔ t)) ⇒((u◁r) ⊆ r).
Proof.
intros S T s t u r H.
intros x H0.
induction H0.
intuition.
Qed.

(**
  BBook: section 2.6.3, p.98, array 1, row 10
  *)
Theorem Inclusion_laws_14: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (v: Ensemble T), (r ∈ (s ↔ t) ∧ v ⊆ t) ⇒((r▷v) ⊆ r).
Proof.
intros S T s t r v H.
intros x H0.
induction H0.
intuition.
Qed.

(**
  BBook: section 2.6.3, p.98, array 1, row 11
  *)
Theorem Inclusion_laws_15: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (u: Ensemble S), (u ⊆ s ∧ r ∈ (s ↔ t)) ⇒((u⩤r) ⊆ r).
Proof.
intros S T s t u r H.
intros x H0.
induction H0.
intuition.
Qed.

(**
  BBook: section 2.6.3, p.98, array 1, row 12
  *)
Theorem Inclusion_laws_16: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (v: Ensemble T), (r ∈ (s ↔ t) ∧ v ⊆ t) ⇒((r⩥v) ⊆ r).
Proof.
intros S T s t u r H.
intros x H0.
induction H0.
intuition.
Qed.

(**
  BBook: section 2.6.3, p.98, array 1, row 13
  *)
Theorem Inclusion_laws_17: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (r: Ensemble (S*T)) (p: Ensemble (T*U)) (q: Ensemble (T*U)), (r∈ (s ↔ t) ∧ p ∈ (t ↔ u) ∧ q ∈ (t ↔ u)) ⇒((r;(p∩q)) ⊆ ((r;p)∩(r;q))).
Proof.
intros S T U s t u r p q H.
intros x H0.
induction H0.
induction H0.
induction H0.
inversion H1.
constructor; constructor; exists x; intuition.
Qed.

(**
  BBook: section 2.6.3, p.98, array 1, row 14
  *)
Theorem Inclusion_laws_18: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (r: Ensemble (S*T)) (p: Ensemble (T*U)) (q: Ensemble (T*U)), (r∈ (s ↔ t) ∧ p ∈ (t ↔ u) ∧ q ∈ (t ↔ u)) ⇒(((r;p) ∖ (r;q)) ⊆ (r;(p ∖ q))).
Proof.
intros S T U s t u r p q H.
intros x H0.
induction H0.
inversion H0.
induction H2.
constructor; exists x0.
split; [ intuition |  constructor; [ intuition | ]].
intros H4; apply H1.
rewrite <- H3; constructor.
exists x0; intuition.
Qed.

(**
  BBook: section 2.6.3, p.98, array 1, row 15
  *)
Theorem Inclusion_laws_19: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (p: Ensemble (S*T)) (q: Ensemble (S*T)) (r: Ensemble (T*U)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t) ∧ r ∈ (t ↔ u)) ⇒(((p∩q);r) ⊆ ((p;r)∩(q;r))).
Proof.
intros S T U s t u r p q H.
intros x H0.
induction H0.
inversion H0.
induction H1.
inversion H1.
constructor; constructor; exists x; intuition.
Qed.

(**
  BBook: section 2.6.3, p.98, array 1, row 16
  *)
Theorem Inclusion_laws_20: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (p: Ensemble (S*T)) (q: Ensemble (S*T)) (r: Ensemble (T*U)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t) ∧ r ∈ (t ↔ u)) ⇒(((p;r) ∖ (q;r)) ⊆ ((p ∖ q);r)).
Proof.
intros S T U s t u r p q H.
intros x H0.
induction H0.
inversion H0.
induction H2.
constructor; exists x0.
split; [ constructor; [ intuition | ] | intuition ].
intros H4; apply H1.
rewrite <- H3; constructor.
exists x0; intuition.
Qed.

(**
  BBook: section 2.6.3, p.98, array 1, row 17
  *)
Theorem Inclusion_laws_21: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (v: Ensemble S) (r: Ensemble (S*T)), (r ∈ (s ↔ t) ∧ u ⊆ s ∧ v ⊆ s) ⇒((r[u ∩ v]) ⊆ ((r[u]) ∩ (r[v]))).
Proof.
intros S T s t u v r H.
intros x H0.
induction H0.
induction H0.
induction H0.
constructor; compute; apply image_intro with x; intuition.
Qed.

(**
  BBook: section 2.6.3, p.98, array 1, row 18
  *)
Theorem Inclusion_laws_22: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (v: Ensemble S) (r: Ensemble (S*T)), (r ∈ (s ↔ t) ∧ u ⊆ s ∧ v ⊆ s) ⇒(((r[u]) ∖ (r[v])) ⊆ (r[u ∖ v])).
Proof.
intros S T s t u v r H.
intros x H0.
induction H0.
inversion H0.
induction H2.
compute; apply image_intro with x0.
split; [ | assumption ].
split; [ assumption | ].
intros H5; apply H1.
compute; apply image_intro with x0; intuition.
Qed.

(**
  BBook: section 2.6.3, p.98, array 1, row 19
  *)
Theorem Inclusion_laws_23: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t)) ⇒((domain(p ∩ q)) ⊆ (domain(p) ∩ domain(q))).
Proof.
intros S T s t p q H.
intros x H0.
induction H0.
induction H0.
inversion H0.
split; constructor; exists x; assumption.
Qed.

(**
  property 2.6.1, BBook: section 2.6.3, p.98, array 1, row 20
  *)
Theorem Inclusion_laws_24: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t)) ⇒((domain(p) ∖ domain(q)) ⊆ (domain(p ∖ q))).
Proof.
intros S T s t p q H.
intros x H0.
induction H0.
induction H0.
inversion H0.
constructor.
exists x; constructor.
assumption.
intros H3; apply H1; constructor; exists x; assumption.
Qed.

(**
  BBook: section 2.6.3, p.98, array 1, row 21
  *)
Theorem Inclusion_laws_25: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t)) ⇒((range(p ∩ q)) ⊆ (range(p) ∩ range(q))).
Proof.
intros S T s t p q H.
intros x H0.
induction H0.
induction H0.
inversion H0.
split; constructor; exists x; assumption.
Qed.

(**
  BBook: section 2.6.3, p.98, array 1, row 22
  *)
Theorem Inclusion_laws_26: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t)) ⇒((range(p) ∖ range(q)) ⊆ (range(p ∖ q))).
Proof.
intros S T s t p q H.
intros x H0.
induction H0.
induction H0.
inversion H0.
constructor.
exists x; constructor.
assumption.
intros H3; apply H1; constructor; exists x; assumption.
Qed.

(**
  BBook: section 2.6.3, p.98, array 1, row 23
  *)
Theorem Inclusion_laws_27: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (p: Ensemble (S*T)) (q: Ensemble (S*T)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t) ∧ u ⊆ s) ⇒(((p ∩ q)[u]) ⊆ ((p[u]) ∩ (q[u]))).
Proof.
intros S T s t u p q H.
intros x H0.
induction H0.
induction H0; inversion H1.
constructor; compute; apply image_intro with x; intuition.
Qed.

(**
  BBook: section 2.6.3, p.98, array 1, row 24
  *)
Theorem Inclusion_laws_28: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (p: Ensemble (S*T)) (q: Ensemble (S*T)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t) ∧ u ⊆ s) ⇒(((p[u]) ∖ (q[u])) ⊆ ((p ∖ q)[u])).
Proof.
intros S T s t u p q H.
intros x H0.
induction H0.
inversion H0.
compute; apply image_intro with x0.
split; [ intuition | ].
split; [ intuition | ].
intros H4; apply H1.
compute; apply image_intro with x0; intuition.
Qed.


Close Scope eB_scope.
