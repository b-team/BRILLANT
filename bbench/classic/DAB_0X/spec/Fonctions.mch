/*? **************************************************************
 *
 * File : $Id: $
 *
 *
 * Object : Defines each function needed to use the cash dispenser
 *          machine.
 * 
 * ---------------------------------------------------------------
 * Copyright (c) 1996 - GEC ALSTHOM Transport and STERIA - 
 * ---------------------------------------------------------------
 *
 * History : $Log: $
 *
 *****************************************************************?*/


MACHINE
        Fonctions

SEES
        Etat , Date , Horloge

INCLUDES
        Messages , Carte

ABSTRACT_VARIABLES
        clients , comptes , interdits ,       /*? variables of machine Site_central ?*/
        caisse , corbeille , retraits ,       /*? variables of machine Distributeur ?*/
        carte , code_CB , date_validite ,       /*? data included in the card:
					   * number, code, expiration date
					   ?*/
        date_mesure , code_saisi , somme ,       /*? data coming from the outside:
					   * date, input code, inout sum
					   ?*/
        message ,                         /*? variable of machine Ecran ?*/
        essai_possible                          /*? variable of machine Clavier_code ?*/

,        code_demande         /*? equals TRUE if the input keyboard is active ?*/
,        somme_demandee         /*? equals TRUE if the code keyboard is active ?*/

CONCRETE_VARIABLES
        infos_lues         /*? equals TRUE if the data in the card are read;
			  * flag used to link accounts with the variable solde of the
			  * implementation ?*/

INVARIANT
        clients                : POW ( CARTE - { K0 , HS } )
&        comptes                : clients --> NAT
&        interdits        : POW ( clients )
&        caisse                : NAT
&        corbeille        : NAT
&        retraits        : NAT
&        carte                : CARTE
&        code_CB                : 0 .. 9999
&        date_validite        : DATE
&        date_mesure        : DATE
&        code_saisi        : 0 .. 9999
&        somme                : 100 .. 900
&        message                : MESSAGE
&        code_demande        : BOOL
&        somme_demandee        : BOOL
&        essai_possible        : BOOL
&        infos_lues        : BOOL
  /*? the variable sum is bound by the variable avance ?*/
&        caisse + corbeille + retraits : NAT

INITIALISATION
        ANY cl , co , in WHERE
                cl : POW ( CARTE - { K0 , HS } )
        &        co : cl --> NAT
        &        in : POW ( cl )
        THEN
                clients , comptes , interdits := cl , co , in
        END
||        caisse                := 0
||        corbeille        := 0
||        retraits        := 0
||        carte                := K0
||        code_CB                := 0
||        date_validite        := D_min
||        date_mesure        :: DATE
||        code_saisi        := 0
||        somme                :: 100 .. 900
||        message                := Vide
||        code_demande        := FALSE
||        somme_demandee        := FALSE
||        essai_possible        := TRUE
||        infos_lues        := FALSE

OPERATIONS

/*? 
 * the banker fills the cash register with avnc and empties out the basket.
 * The variable retraits is set to zero.
 ?*/
initialiser_distributeur ( avnc ) =
        PRE
                avnc : NAT
        &        900 <= avnc
        THEN
                caisse          := avnc
        ||        corbeille := 0
        ||        retraits  := 0
        END
;
 /*?
  * test all of the elements of the dab, tst_dab equals TRUE if an error occured
  ?*/
tst_dab <-- tester_dab =
        BEGIN
                tst_dab :: BOOL                                                           /*? E24 ?*/
        END
;
 /*?
  * test whether there is enough money in the cash register (cse_vde = FALSE)
  ?*/
cse_vde <-- tester_caisse_vide =
        BEGIN
                cse_vde := bool ( caisse < 900 )
        END
;
 /*?
  * the dab contains no card: carte is initialized to K0, and infos_lues is
  * initialized to FALSE;
  * variables linked with the card data are initialized to non significant values
  ?*/
initialiser_lecteur_dab =
        BEGIN
                carte              := K0
        ||        code_CB              := 0
        ||        date_validite := D_min
        ||        infos_lues    := FALSE
        END
;

 /*?
  * keyboard is disabled (code_demande initialized to FALSE)
  * no customer: variables essai_possible and code_saisi are initialized
  * to non significant values
  ?*/
initialiser_clavier_code_dab =
        BEGIN
                code_saisi     := 0
        ||        essai_possible := TRUE
        ||        code_demande   := FALSE
        END
;

 /*?
  * keyboard is disabled (code_demande initialized to FALSE)
  * no customer: initialized to 0
  ?*/
initialiser_clavier_somme_dab =
        BEGIN
                somme               :: 100 .. 900
        ||        somme_demandee := FALSE
        END
;

initialiser_ecran =
        BEGIN
                message := Vide
        END
;

 /*?
  * open the reader, which must be empty, and request the customer to put his card
  * inside
  ?*/
autoriser_entree_carte =
        PRE
                carte = K0
        /* respect of the invariant of Dab_imp */
        &        code_demande = FALSE
        &        somme_demandee = FALSE
        &        infos_lues = FALSE
        THEN
                message := Entrez_votre_carte                                           /*? E26 ?*/
        END
;
 /*?
  * wait to be used by a customer
  * return an event report
  ?*/
rslt <-- surveiller =
        PRE
                carte = K0
        THEN
                rslt :: { Valide , Hors_delai , Incident }
        END
;
 /*?
  * load the card
  ?*/
autoriser_chargement_carte =
        PRE
                carte = K0
        /* the card is not read yet, variable solde must not be linked to compte(carte)
	 */
        &        infos_lues = FALSE
        /* respect of the invariants of Dab_imp */
        &        code_demande = FALSE
        &        somme_demandee = FALSE
        THEN
                carte              :: CARTE                                                   /*? E14 ?*/
        ||        code_CB              :: 0 .. 9999
        ||        date_validite :: DATE
        ||        message              := Veuillez_patienter
        END
;
 /*?
  * check that the card has been successfully loaded
  * if so (rslt /= Illisible,Incident), the card is read (infos_lues = TRUE);
  * close the reader, then check that the customer is allowed to get some cash,
  * considering his card, the central site and the date.
  ?*/
rslt <-- controler_carte =
        PRE
        /* respect of the invariants of Dab_imp */
                infos_lues = FALSE
        &        somme_demandee = FALSE
        THEN
                ANY ctr , infl
                WHERE
                        ctr : { Valide , Invalide , Illisible , Interdite , Perimee ,
                          Incident , Epuisee }
                &        infl : BOOL
                &        ( (       ctr /: { Invalide , Illisible , Incident }
                         &        carte : clients - interdits       /* ctr /= Interdite */
                         &        comptes ( carte ) >= 100             /* ctr /= Epuisee */
                         &        date <= date_validite                /* ctr /= Perimee */
                          ) <=> ( ctr = Valide ) )
                &      ( ctr /: { Illisible , Incident } <=> ( infl = TRUE ) )
                &      ( ctr = Incident <=> ( carte = K0 ) )
                THEN
                        rslt            := ctr
                ||        infos_lues  := infl
                ||        date_mesure := date
                END
        END
;
 /*?
  * set up the keyboard according to the number of unsuccessfull attempts already done
  * activate the keyboard and ask the customer to enter his code
  * his card has to be valid (withdraw permission)
  * the message displayed depends on the previous state and on the transition
  * d_rslt of the automaton.
  ?*/
autoriser_saisie_code ( d_etat , d_rslt ) =
        PRE
                 carte : clients - interdits
        &         date_mesure <= date_validite
        &         100 <= comptes ( carte )
        /* respect of the preconditions of message_controler_code */
        &        d_etat : ETAT_AUTOMATE
        &        d_rslt : STATUT
        &        d_etat : { Traitement_carte , Traitement_code }
        &        ( d_etat = Traitement_code =>
                        d_rslt : { Nouvel , Dernier }
                 &      essai_possible = TRUE )
        &        ( d_etat = Traitement_carte => d_rslt = Valide )
        /* respect of the invariants of Dab_imp */
        &        code_demande = FALSE
        &        somme_demandee = FALSE
        &        infos_lues = TRUE

        THEN
                ANY msg , cds WHERE
                        msg : MESSAGE
                &        ( d_etat = Traitement_carte =>
                                msg = Entrez_votre_code )
                &        ( d_etat = Traitement_code =>
                                ( d_rslt = Nouvel => msg = Nouvel_essai )
                         &        ( d_rslt = Dernier => msg = Dernier_essai ) )
                &        cds : 0 .. 9999
                THEN
                        message , code_saisi := msg , cds
                END
        ||        code_demande   := TRUE
        ||        essai_possible := TRUE
        END
;
 /*?
  * check that the customer entered the right code and disable the keyboard
  ?*/
rslt <-- controler_code =
        PRE
        /* respect of the preconditions of called operations */
                code_demande = TRUE
        &        essai_possible = TRUE
        THEN
                ANY ctrl , cds , essaip WHERE
                        ctrl : { Valide , Nouvel , Dernier , Invalide , Hors_delai ,
                                Incident }
                &        cds : 0 .. 9999
                &        (        ( code_CB = cds
                         &      ctrl /: { Hors_delai , Incident } )
                         <=> ( ctrl = Valide ) )
                &        essaip : BOOL
                &        ( ctrl = Invalide => essaip = FALSE )
                &        ( ctrl : { Nouvel , Dernier } => essaip = TRUE )
                THEN
                        rslt , code_saisi := ctrl , cds
                ||        essai_possible := essaip
                END
        ||        code_demande := FALSE
        END

;
 /*?
  * compute the maximum amount that the customer is allowed to withdraw
  * activate the keyboard and ask the customer to enter the sum he wants
  * to withdraw
  * the card must be valid and the code must be the right code.
  ?*/
autoriser_saisie_somme =
        PRE
                carte : clients - interdits
        &        date_mesure <= date_validite
        &        comptes ( carte ) >= 100
        /* respect of the invariants of Dab_imp */
        &        somme_demandee = FALSE
        &        code_demande = FALSE
        &        infos_lues = TRUE
        THEN
                message               := Entrez_somme                                           /*? E31 ?*/
        ||        somme_demandee := TRUE
        END
;
 /*?
  * check that the customer entered a correct amount and disable the keyboard
  ?*/
rslt <-- controler_somme =
        PRE
        /* respect of the invariant of Dab_imp */
                carte : clients - interdits
        &        somme_demandee = TRUE
        &        infos_lues = TRUE
        THEN
                ANY som , ctr WHERE
                        som : 100 .. 900
                &        ctr : { Valide , Invalide , Hors_delai , Incident }
                &        (        ( ctr /: { Hors_delai , Incident }
                                 &        som : 100 .. 900
                                 &        som <= comptes ( carte )
                                 &        som <= caisse )
                        <=> ( ctr = Valide ) )
                THEN
                        somme := som
                ||        rslt  := ctr
                END
        ||        somme_demandee := FALSE
        END
;
 /*?
  * ask the customer to take his bills out
  ?*/
autoriser_distribution_billets =
        BEGIN
                message := Prenez_vos_billets                                           /*? E35 ?*/
        END
;
 /*?
  * check that the customer took his bills out, otherwise they are thrown into the basket
  * (an error occured before he could take them or he did not take them).
  * Whatever happened, the amount asked is deducted from the cash register and from
  * the account;
  * if an error occured, the dab does not have the right balance (infos_lues = FALSE) 
  * the amount must be valid
  ?*/
rslt <-- controler_distribution_billets =
        PRE
                carte : clients - interdits
        &        somme : 100 .. 900
        &        somme <= comptes ( carte )
        &        somme <= caisse
        THEN
                ANY ctr , rjt WHERE
                        ctr : { Valide , Hors_delai , Incident }
                &        rjt : { 0 , somme }
                &        ( ctr = Valide         => rjt = 0 )
                &        ( ctr = Hors_delai => rjt = somme )
                THEN
                        caisse               := caisse - somme
                ||        corbeille      := corbeille + rjt
                ||        retraits       := retraits + somme - rjt
                ||        comptes ( carte ) := comptes ( carte ) - somme
                ||        rslt               := ctr
                ||        infos_lues     := FALSE
                END
        END
;
 /*?
  * give back the card and ask the customer to take it back.
  * the message displayed depends on the previous state d_etat and on the transaction
  * d_rslt of the automaton.
  * data concerning the card and the account are forgotten (infos_lues = FALSE)
  ?*/
autoriser_restitution_carte ( d_etat , d_rslt ) =
        PRE
                 carte /= K0
        /* respect of the preconditions of message_restitution_carte */
        &        d_etat : ETAT_AUTOMATE
        &        d_rslt : STATUT
        &        d_etat : { Traitement_carte , Traitement_code , Traitement_somme , Distribution_billets }
        &        ( d_etat = Traitement_carte => d_rslt : { Perimee , Epuisee , Illisible , Invalide } )
        &        ( d_etat = Traitement_code => d_rslt : { Hors_delai , Incident } )
        &        ( d_etat = Traitement_somme => d_rslt : { Invalide , Hors_delai , Incident } )
        &        ( d_etat = Distribution_billets => d_rslt : { Valide , Hors_delai , Incident } )
        /* respect of the invariants of Dab_imp */
        &        code_demande = FALSE
        &        somme_demandee = FALSE
        THEN
                ANY msg WHERE
                        msg : MESSAGE
                &        ( d_etat = Traitement_carte =>
                                ( d_rslt = Perimee  => msg = Carte_perimee )
                         &        ( d_rslt = Epuisee  => msg = Carte_epuisee )
                         &        ( d_rslt = Illisible => msg = Carte_invalide )
                         &        ( d_rslt = Invalide => msg = Carte_invalide ) )
                &        ( d_etat = Traitement_code =>
                                ( d_rslt = Hors_delai => msg = Retirez_votre_carte )
                         &      ( d_rslt = Incident => msg = Retirez_votre_carte ) )
                &        ( d_etat = Traitement_somme =>
                                ( d_rslt = Invalide => msg = Caisse_insuffisante )
                         &      ( d_rslt = Hors_delai => msg = Retirez_votre_carte )
                         &      ( d_rslt = Incident => msg = Retirez_votre_carte ) )
                &        ( d_etat = Distribution_billets =>
                                ( d_rslt = Valide  => msg = Retirez_votre_carte )
                         &      ( d_rslt = Hors_delai => msg = Billets_confisques )
                         &      ( d_rslt = Incident => msg = Retirez_votre_carte )
                         )
                THEN
                        message           := msg
                ||        carte           := K0
                ||        infos_lues := FALSE
                END
        END
;
 /*?
  * check that the customer took the card back
  ?*/
rslt <-- controler_restitution_carte =
        PRE
                carte = K0
        THEN
                ANY msg , ctrl WHERE
                        ctrl   : { Valide , Hors_delai , Incident }
                &        msg    : MESSAGE
                &        ( ctrl = Valide  =>
                                msg = Merci_de_votre_visite
                        )
                &        ( ctrl : { Hors_delai , Incident } =>
                                msg = message
                        )
                THEN
                        rslt        := ctrl
                ||        message        := msg
                END
        END
;
 /*?
  * tell the customer that his card has been confiscated
  * the message displayed depends on the previous state d_etat and on the transaction
  * d_rslt of the automaton.
  * the data concerning the card and the linked account are forgotten (infos_lues=FALSE)
  ?*/
autoriser_confiscation_carte ( d_etat , d_rslt ) =
        PRE
        /* respect of the preconditions of message_confiscation_carte */
                d_etat : ETAT_AUTOMATE
        &        d_rslt : { Interdite , Incident , Invalide , Hors_delai }
        &        d_etat : { Traitement_carte , Traitement_code , Restitution_carte }
        /* respect of the invariants of Dab_imp */
        &        code_demande = FALSE
        &        somme_demandee = FALSE
        THEN
                ANY n_msg WHERE
                        n_msg : MESSAGE
                &        ( d_etat = Traitement_carte =>
                                 ( d_rslt = Interdite => n_msg = Carte_interdite ) )
                &        ( d_etat = Restitution_carte =>
                                 ( d_rslt = Hors_delai => n_msg = Carte_confisquee )
                         &        ( d_rslt = Incident  => n_msg = Carte_confisquee ) )
                &        ( d_etat = Traitement_code =>
                                 ( d_rslt = Invalide => n_msg = Code_invalide ) )
                THEN
                        message := n_msg
                END
        ||        infos_lues := FALSE
        END
;
 /*?
  * confiscate the card
  ?*/
rslt <-- controler_confiscation_carte =
        ANY ctrl WHERE
                ctrl : { Valide , Invalide }
        THEN
                carte := K0
        ||        rslt  :: { Valide , Invalide }
        END

;
 /*?
  * operation called before the dab is closed
  * give back or confiscate the card depending on the current state eta_cour of the
  * automaton
  * the data of the card are forgotten (infos_lues=FALSE)
  * if the card that has been given back is not withdrawn, then it is swallowed
  * and the customer is informed
  ?*/
traiter_carte ( eta_cour ) =
        PRE
                eta_cour : { Traitement_carte , Traitement_code , Traitement_somme , Distribution_billets , Restitution_carte , Confiscation_carte }
        /* respect of the preconditions of called operations */
        &        ( eta_cour : { Restitution_carte , Traitement_code , Traitement_somme , Distribution_billets } => carte /= K0 )
        &        ( eta_cour = Traitement_carte => carte = K0 )
        THEN
                CHOICE
                        message := Retirez_votre_carte
                OR
                        message := Carte_confisquee
                OR
                        skip
                END
        ||        carte           := K0
        ||        infos_lues := FALSE
        END
;
 /*?
  * tell the customer that the dab is inactive
  * close the reader to avoid any card insertion
  * and disable the keyboards
  ?*/
autoriser_mise_hors_service =
        BEGIN
                message               := En_panne                                           /*? E55 ?*/
        ||        somme_demandee := FALSE
        ||        code_demande   := FALSE
        ||        carte               := HS
        END
END
