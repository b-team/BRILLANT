 

Require Import Blib.

Theorem op:
forall Confiscation_carte Dernier Distribution_billets ETAT_AUTOMATE ETAT_DAB En_service Epuisee Hors_delai Hors_service Illisible Incident Interdite Invalide Nouvel Perimee Restitution_carte STATUT Traitement_carte Traitement_code Traitement_somme Valide Veille code_saisi essai_possible etat_clc, ( 
 (In (interval 0 9999) code_saisi) -> 
(In BBOOL etat_clc) -> 
(In BBOOL essai_possible) -> 
(In (Power_set1 BZ) ETAT_DAB) -> 
(ETAT_DAB = (fun o => ((o=Hors_service) \/ (o=En_service)))) -> 
(In (Power_set1 BZ) ETAT_AUTOMATE) -> 
(ETAT_AUTOMATE = (fun o => ((o=Traitement_carte) \/ (o=Traitement_code) \/ (o=Traitement_somme) \/ (o=Distribution_billets) \/ (o=Restitution_carte) \/ (o=Confiscation_carte) \/ (o=Veille)))) -> 
(In (Power_set1 BZ) STATUT) -> 
(STATUT = (fun o => ((o=Valide) \/ (o=Invalide) \/ (o=Illisible) \/ (o=Interdite) \/ (o=Perimee) \/ (o=Epuisee) \/ (o=Nouvel) \/ (o=Dernier) \/ (o=Hors_delai) \/ (o=Incident))))
  -> (
 (In BBOOL etat_clc) ) )
 .
intuition.
Qed.
 