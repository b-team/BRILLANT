(*☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   Project: BiCoq, formalisation of the B theory into the Coq system
   Module: Bgamma, proof environments
   A proof environment is a list (in the sense of Section Mlist) of predicates.
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : October 2005 - October 2006
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Load Mlist.
 Require Export Btrmind.

(* Proof environment definition from Blist ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Type *)
 Definition Bgam:=Blst ☊. Notation "☐":=Bgam.
 Definition gemp:=lemp ☊. Notation "∅☑":=(lemp ☊).
 Definition gins:=lins ☊. Notation "G '☑★' P":=(lins ☊ G P) (at level 50).

 (* Equality *)
 Definition fgequ:=flequ ☊ fpequ. Notation "G '=☑☂' H":=(flequ ☊ fpequ G H) (at level 50).
 Definition impgequ:=implequ ☊ fpequ imppequ.

 Definition decgequ:=decimp2 impgequ.
 Definition gequtp:=imp2tp impgequ.
 Definition gequfr:=imp2fr impgequ.
 Definition gequpt:=imp2pt impgequ.
 Definition gequrf:=imp2rf impgequ.

 (* Membership *)
 Definition giyes:=liyes ☊.
 Definition ginxt:=linxt ☊.
 Definition gin:=lin ☊. Notation "P '∈☑' G":=(lin ☊ P G) (at level 50).

 Ltac BTin:=repeat (apply giyes || apply ginxt).

 Definition ginemp:=linemp ☊.
 Definition casegin:=caselin ☊.

 (* Membership function *)
 Definition fgin:=flin ☊ fpequ. Notation "P '∈☑☂' G":=(flin ☊ fpequ P G) (at level 50).

 Definition impgin:=implin ☊ fpequ imppequ.
 Definition decgin:=declin ☊ fpequ imppequ.
 Definition ginpt:=linpt ☊ fpequ imppequ.
 Definition ginrf:=linrf ☊ fpequ imppequ.
 Definition gintp:=lintp ☊ fpequ imppequ.
 Definition ginfr:=linfr ☊ fpequ imppequ.
 
 (* Inclusion *)
 Definition ginc:=linc ☊. Notation "G '⊆☑' H":=(linc ☊ G H) (at level 50).

 Definition reflginc:=refllinc ☊.
 Definition transginc:=translinc ☊.
 Definition empginc:=emplinc ☊.
 Definition gincover:=lincover ☊.
 Definition gincemp:=lincemp ☊.
 Definition gincsub:=lincsub ☊.
 Definition gincdbl:=lincdbl ☊ fpequ imppequ.
 Definition gincswp:=lincswp ☊.
 Definition gincnot:=lincnot ☊ fpequ imppequ.

 (* Inclusion function *)
 Definition fginc:=flinc ☊ fpequ. Notation "G '⊆☑☂' H":=(flinc ☊ fpequ G H) (at level 50).

 Definition impginc:=implinc ☊ fpequ imppequ.
 Definition decginc:=declinc ☊ fpequ imppequ.
 Definition gincpt:=lincpt ☊ fpequ imppequ.
 Definition gincrf:=lincrf ☊ fpequ imppequ.
 Definition ginctp:=linctp ☊ fpequ imppequ.
 Definition gincfr:=lincfr ☊ fpequ imppequ.

 (* Remove element function *)
 Definition fgrem:=flrem ☊ fpequ. Notation "G '-☑' P":=(flrem ☊ fpequ G P) (at level 50).

 Definition fgremin:=flremin ☊ fpequ imppequ.
 Definition fgreminc:=flreminc ☊ fpequ imppequ.
 Definition infgrem:=inflrem ☊ fpequ imppequ.
 Definition ginceqrem:=linceqrem ☊ fpequ imppequ.

(* Majoration ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition gmaj:=lpmap2 ☊ ☈ pmaj.
 Infix ">☑":=gmaj (at level 70, right associativity).

 Theorem gmajinc:forall (i:☈)(G:☐)(P:☊), i>☑(G☑★P)->(i>☑G)*(i>☋P).
 Proof.
  intros i G P H; split;
   [unfold gmaj; intros P0 H0; apply H; apply ginxt; apply H0 | apply H; apply giyes].
 Qed.

 Theorem gmajdec:forall (G:☐)(i:☈)(P:☊), i>☋P->i>☑G->i>☑(G☑★P).
 Proof.
  intros G i P H H0; unfold gmaj; intros P0 H1; destruct (casegin _ _ _ H1);
   [rewrite i0; apply H | apply H0; apply l].
 Qed.

 Theorem gmajempty:forall (i:☈), i>☑∅☑.
 Proof.
  intros i; unfold gmaj; intros T H; invset_clear H.
 Qed.

 Theorem gmajige:forall (G:☐)(i j:☈), i>☑G->i≤☉j->j>☑G.
 Proof.
  intros G i j HG Hij; unfold gmaj; intros P H; apply (pmajige P _ _ Hij); apply HG; apply H.
 Qed.

(* Majoration function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition fgmaj:=lfmap2 ☊ ☈ fpmaj.
 Infix ">☑☂":=fgmaj (at level 70, right associativity).

 Definition impgmaj:=lmap2imp ☊ ☈ pmaj fpmaj imppmaj.
 Definition decgmaj:=decimp2 impgmaj.
 Definition gmajpt:=imp2pt impgmaj.
 Definition gmajrf:=imp2rf impgmaj.
 Definition gmajtp:=imp2tp impgmaj.
 Definition gmajfr:=imp2fr impgmaj.

(* Non-freeness ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition ginf:=lpmap2 ☊ ☈ pinf.
 Infix "⍀☑":=ginf (at level 70, right associativity).

 Theorem ginfinc:forall (i:☈)(G:☐)(P:☊), i⍀☑(G☑★P)->(i⍀☑G)*(i⍀☋P).
 Proof.
  intros i G P H; split;
   [unfold ginf; intros P0 H0; apply H; apply ginxt; apply H0 | apply H; apply giyes].
 Qed.

 Theorem ginfdec:forall (G:☐)(i:☈)(P:☊), i⍀☋P->i⍀☑G->i⍀☑(G☑★P).
 Proof.
  intros G i P H H0; unfold ginf; intros P0 H1; destruct (casegin _ _ _ H1);
   [rewrite i0; apply H | apply H0; apply l].
 Qed.

 Theorem ginfempty:forall (i:☈), i⍀☑∅☑.
 Proof.
  intros i; unfold ginf; intros T H; invset_clear H.
 Qed.

 Theorem gmajinf:forall (G:☐)(i:☈), i>☑G->i⍀☑G.
 Proof.
  intros G i H; unfold ginf; intros P H0; apply pmajinf; apply H; apply H0.
 Qed.

(* Non-freeness function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition fginf:=lfmap2 ☊ ☈ fpinf.
 Infix "⍀☑☂":=fginf (at level 70, right associativity).

 Definition impginf:=lmap2imp ☊ ☈ pinf fpinf imppinf.
 Definition decginf:=decimp2 impginf.
 Definition ginfpt:=imp2pt impginf.
 Definition ginfrf:=imp2rf impginf.
 Definition ginftp:=imp2tp impginf.
 Definition ginffr:=imp2fr impginf.

(* Fresh free index function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Fixpoint fgnewf(G:☐){struct G}:☈:=
  match G with
  | ∅☑ => ☦
  | G'☑★P' => ☰☋(P') ☰☉ (fgnewf G')
  end.
 Notation "'☰☑(' G ')'":=(fgnewf G).

 (* ☰☑ majors the ☰☋ of any term of the environment *)
 Theorem majfgnewf:forall (P:☊) (G:☐), P∈☑G->☰☋(P) ≤☉ ☰☑(G).
 Proof.
  intro P; induction G; intros H.
   invset H.
   unfold fgnewf; fold fgnewf; destruct (decpequ P d).
    rewrite i; apply fimaxgel.
    invset H.
     destruct e; rewrite H3; apply refl_id.
     generalize (IHG H2); intros H4; clear IHG H2 H1 H0 H3 d1 d2 L;
      destruct (decige ☰☋(d) ☰☑(G)).
      rewrite (invfimax _ _ i); apply H4.
      rewrite commfimax; rewrite (invfimax _ _ (asymige _ _ e0));
       apply (transige _ _ _ H4 (asymige _ _ e0)).
 Qed.

 Theorem fgnewfmaj:forall (G:☐), ☰☑(G)>☑G.
 Proof.
  intros G; unfold gmaj; intros P H; apply (pmajige P ☰☋(P));
   [apply majfgnewf; apply H | apply fpnewfmaj].
 Qed.

 (* The name provided by ☰☑ is fresh *)
 Theorem fgnewfinf:forall (G:☐), ☰☑(G) ⍀☑G.
 Proof.
  induction G; unfold ginf; intros P H.
   invset H.
   destruct (decpequ P d).
    rewrite i; rewrite <- (sym_id i) in H; simpl.
    generalize (pmajige _ _ _ (fimaxgel (fpnewf d) (☰☑(G))) (fpnewfmaj d)); apply pmajinf.
   invset H.
    destruct e; rewrite H3; apply refl_id.
    unfold fgnewf; fold fgnewf; clear H1 H0 H3 d1 d2 L H e; destruct (decige ☰☋(d) ☰☑(G)).
     rewrite (invfimax _ _ i); apply (IHG _ H2).
     rewrite commfimax; rewrite (invfimax _ _ (asymige _ _ e));
      generalize (pmajige _ _ _ (majfgnewf _ _ H2) (fpnewfmaj P)); intros H4;
      apply pmajinf; apply (pmajige _ _ _ (asymige _ _ e) H4).
 Qed.

 (* A technical lemmas allowing to anonymise fresh names... *)
 Lemma exfgnewf:forall (G:☐), {x:☈ & x⍀☑G}.
 Proof.
  intros G; exists ☰☑(G); apply fgnewfinf.
 Qed.

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)
