(* $Id$ *)

(* BCaml  IRIT                                   *)

type page =
    VBox of page list
  | HBox of page list
  | Word of string

let mk_VBox _x1 = VBox(_x1)
let mk_HBox _x1 = HBox(_x1)
let mk_Word _x1 = Word(_x1)
let mk_page _x = _x

let  mkXML hv _x1 content =   
  hv [Word("<"^_x1^">") ; content ; Word("</"^_x1^">")]
 
let mkBalise = mkXML  mk_HBox
let mkClause = mkXML  mk_VBox

(*
  HBox [Word("<"^_x1^">") ; content ; Word("</"^_x1^">")]

let mk_Clause _x1 content = 
  VBox [Word("<"^_x1^">") ; content ; Word("</"^_x1^">")]
*)


let rec butlast = function
    [x] -> [x]
  | x::l -> x::butlast l  
  | _ -> failwith "butlast"

let rec last = function
    [x] -> x
  | x::l -> last l  
  | _ -> failwith "last"

let hbox_list o c s lb = 
  match lb with
    [] -> HBox []
  | x::l -> 
      let ll = List.concat (List.map (fun y -> [Word s;y]) l) in
      HBox ((Word o)::x::(ll@[Word c]))


let vbox_list o c s lb = (* separateur et item sur des lignes diff.*)
  match lb with
    [] -> VBox []
  | x::l -> 
      let ll = 
	if s <> "" 
	then List.concat (List.map (fun y -> [Word s;y]) l) 
	else lb in
	VBox ((if o <> "" then [Word o] else [])@ 
	      (x::ll) @
	      (if c <> "" then [Word c] else []))

(* separateur sur la meme ligne que l'item *)
(*  item0 s *)
(*  item1 s *)
(*  ...     *)
(*  item_n  *)


let vbox_item o c s lb = 
  match lb with
      [] -> VBox []
    | _ -> 
	let ll = 
	  if s <> "" 
	  then 
	    List.concat 
	      (List.map
		 (*	       (fun y -> [mkClause s y ]) (butlast lb))   *)
		 (fun y -> [mkClause s y ]) lb )

	      (*	       (fun y -> [HBox [y ; (Word s)]]) (butlast lb))   *)
	  else lb 
	in
	  VBox (
	    (if o <> "" 
	     then [Word o] 
	     else [])
	    @ 
	    (ll) 
	    @ [last lb] 
	    @
	    (if c <> "" 
	     then [Word c] 
	     else [])
	  )
	    
let hbox_paren o c b =
  match b with
      HBox [] -> b
    | VBox [] -> b
    | _ -> HBox [Word o;b;Word c]
	
let rec flatten_hbox = function
    [] -> []
  | (HBox pl1)::pl2 -> (flatten_hbox pl1)@(flatten_hbox pl2)
  | x::pl -> x::(flatten_hbox pl)
      

