#!/bin/sh

parse="../../trunk/bcaml/toolchain/bparser/bparser_bc"
files=`find parser-watch/ -regex "^.*\(\.mch\|\.ref\|\.imp\)$"`

for i in $files
do
    file=`echo "$i" | sed -e 's!^.*/\([a-zA-Z0-9_]\+\(\.mch\|\.ref\|\.imp\)\)$!\1!g'`
    path=`echo "$i" | sed -e 's!^\(.*\)/\([a-zA-Z0-9_]\+\(\.mch\|\.ref\|\.imp\)\)$!\1!g'`
    xmlpath=`echo "$path" | sed -e 's!spec$!xml!'`
    if [ ! -d "$xmlpath" ]
    then
	mkdir -p "$xmlpath"
    fi
    rm -f "$xmlpath"/"$file".xml
    $parse "$i" >"$path"/"$file".xml
    if [ $? -eq 0 ]
    then
	xmllint --format "$path"/"$file".xml >"$xmlpath"/"$file".xml
    fi
    rm "$path"/"$file".xml
done
