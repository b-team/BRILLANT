(*==================================================================================================
  Project : BiCoq3
  Module : congr.v
  In this module we provide a few results related to congruence (replacement of related propositions
  in a term)
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Oct 2008, Md: Mar 2009
  ================================================================================================*)

Require Export raw_proof.

(* Lemmas and tactics for non-freeness -----------------------------------------------------------*)

Theorem Bfree_not_elim:forall (i:Ι)(p:Π), (i∖¬p)->(i∖p).
Proof.
 intros i p Hip; apply Hip.
Qed.

Theorem Bfree_not_intro:forall (i:Ι)(p:Π), (i∖p)->(i∖¬p).
Proof.
 intros i p Hip; apply Hip.
Qed.

Theorem Bfree_and_elim:forall (i:Ι)(p1 p2:Π), (i∖p1⋀p2)->(i∖p1) /\ (i∖p2).
Proof.
 intros i p1 p2 Hip1p2; destruct (orb_false_elim _ _ Hip1p2) as [Hip1 Hip2];
  fold Bp_free_ Be_free_ in *; split; [apply Hip1 | apply Hip2].
Qed.

Theorem Bfree_and_intro:forall (i:Ι)(p1 p2:Π), (i∖p1)->(i∖p2)->(i∖p1⋀p2).
Proof.
 intros i p1 p2 Hip1 Hip2; srewrite Hip1; srewrite Hip2; apply refl_equal.
Qed.

Theorem Bfree_imp_elim:forall (i:Ι)(p1 p2:Π), (i∖p1⇒p2)->(i∖p1) /\ (i∖p2).
Proof.
 intros i p1 p2 Hip1p2; destruct (orb_false_elim _ _ Hip1p2) as [Hip1 Hip2];
  fold Bp_free_ Be_free_ in *; split; [apply Hip1 | apply Hip2].
Qed.

Theorem Bfree_imp_intro:forall (i:Ι)(p1 p2:Π), (i∖p1)->(i∖p2)->(i∖p1⇒p2).
Proof.
 intros i p1 p2 Hip1 Hip2; srewrite Hip1; srewrite Hip2; apply refl_equal.
Qed.

Theorem Bfree_equ_elim:forall (i:Ι)(e1 e2:Ε), (i∖e1≡e2)->(i∖e1) /\ (i∖e2).
Proof.
 intros i e1 e2 Hie1e2; destruct (orb_false_elim _ _ Hie1e2) as [Hie1 Hie2];
  fold Bp_free_ Be_free_ in *; split; [apply Hie1 | apply Hie2].
Qed.

Theorem Bfree_equ_intro:forall (i:Ι)(e1 e2:Ε), (i∖e1)->(i∖e2)->(i∖e1≡e2).
Proof.
 intros i e1 e2 Hie1 Hie2; srewrite Hie1; srewrite Hie2; apply refl_equal.
Qed.

Theorem Bfree_ins_elim:forall (i:Ι)(e1 e2:Ε), (i∖e1∈e2)->(i∖e1) /\ (i∖e2).
Proof.
 intros i e1 e2 Hie1e2; destruct (orb_false_elim _ _ Hie1e2) as [Hie1 Hie2];
  fold Bp_free_ Be_free_ in *; split; [apply Hie1 | apply Hie2].
Qed.

Theorem Bfree_ins_intro:forall (i:Ι)(e1 e2:Ε), (i∖e1)->(i∖e2)->(i∖e1∈e2).
Proof.
 intros i e1 e2 Hie1 Hie2; srewrite Hie1; srewrite Hie2; apply refl_equal.
Qed.

Theorem Bfree_chc_elim:forall (i:Ι)(e:Ε), (i∖↓(e))->(i∖e).
Proof.
 intros i e Hie; apply Hie.
Qed.

Theorem Bfree_chc_intro:forall (i:Ι)(e:Ε), (i∖e)->(i∖↓(e)).
Proof.
 intros i e Hie; apply Hie.
Qed.

Theorem Bfree_pow_elim:forall (i:Ι)(e:Ε), (i∖↑(e))->(i∖e).
Proof.
 intros i e Hie; apply Hie.
Qed.

Theorem Bfree_pow_intro:forall (i:Ι)(e:Ε), (i∖e)->(i∖↑(e)).
Proof.
 intros i e Hie; apply Hie.
Qed.

Theorem Bfree_cpl_elim:forall (i:Ι)(e1 e2:Ε), (i∖e1↦e2)->(i∖e1) /\ (i∖e2).
Proof.
 intros i e1 e2 Hie1e2; destruct (orb_false_elim _ _ Hie1e2) as [Hie1 Hie2];
  fold Bp_free_ Be_free_ in *; split; [apply Hie1 | apply Hie2].
Qed.

Theorem Bfree_cpl_intro:forall (i:Ι)(e1 e2:Ε), (i∖e1)->(i∖e2)->(i∖e1↦e2).
Proof.
 intros i e1 e2 Hie1 Hie2; srewrite Hie1; srewrite Hie2; apply refl_equal.
Qed.

Theorem Bfree_pro_elim:forall (i:Ι)(e1 e2:Ε), (i∖e1×e2)->(i∖e1) /\ (i∖e2).
Proof.
 intros i e1 e2 Hie1e2; destruct (orb_false_elim _ _ Hie1e2) as [Hie1 Hie2];
  fold Bp_free_ Be_free_ in *; split; [apply Hie1 | apply Hie2].
Qed.

Theorem Bfree_pro_intro:forall (i:Ι)(e1 e2:Ε), (i∖e1)->(i∖e2)->(i∖e1×e2).
Proof.
 intros i e1 e2 Hie1 Hie2; srewrite Hie1; srewrite Hie2; apply refl_equal.
Qed.

Theorem Bgfree_add_elim:forall (i:Ι)(G:Γ)(p:Π), (i∖ˠG·p)->(i∖ˠG)/\(i∖p).
Proof.
 intros i G p HiGp; split; [apply Bg_free_inc with (1:=HiGp) | apply (Bg_free_mem _ _ HiGp)];
  Btac_gamma.
Qed.

Theorem Bgfree_add_intro:forall (G:Γ)(p:Π)(i:Ι), (i∖ˠG)->(i∖p)->(i∖ˠG·p).
Proof.
 intros G p i  Hg Hp; apply Bg_mem_free; simpl; intros p' Hp';
  destruct (orb_true_elim _ _ Hp') as [H'p' | H'p'].
  reflect_in Bp_eq_imp H'p'; rewrite H'p'; apply Hp.
  apply (Bg_free_mem _ _ Hg _ H'p').
Qed.

Theorem Bgfree_empty:forall (i:Ι), i∖ˠ∅ˠ.
Proof.
 intros i; apply refl_equal.
Qed.

Ltac Btac_free :=
 repeat (assumption || apply Bgfree_empty || apply Bp_forall_free || Btac_freeintro ||
         Btac_freehyp || apply Bp_forall_free_free || apply Be_cmpset_free ||
         apply Be_cmpset_free_free);
 repeat fold_all
with Btac_freeintro :=
 match goal with
 | |-(_∖ˠ_·_) => apply Bgfree_add_intro
 | |-(_∖_) => (apply Bfree_not_intro || apply Bfree_and_intro || apply Bfree_imp_intro ||
               apply Bfree_equ_intro || apply Bfree_ins_intro || apply Bfree_chc_intro ||
               apply Bfree_pow_intro || apply Bfree_cpl_intro || apply Bfree_pro_intro)
  end
with Btac_freehyp :=
 match goal with
 | Hfr:(_∖ˠ_·_)|-_ => generalize (Bgfree_add_elim _ _ _ Hfr); clear Hfr; intros Hfr; destruct Hfr
 | Hfr:(?i∖?t)|-_ => first [(* generalize (Bfree_not_elim i t Hfr); clear Hfr; intros Hfr | *)
                            destruct (Bfree_and_elim i _ _ Hfr); clear Hfr |
                            destruct (Bfree_imp_elim i _ _ Hfr); clear Hfr |
                            destruct (Bfree_equ_elim i _ _ Hfr); clear Hfr |
                            destruct (Bfree_ins_elim i _ _ Hfr); clear Hfr |
                            (* generalize (Bfree_chc_elim i t Hfr); clear Hfr; intros Hfr | *)
                            (* generalize (Bfree_pow_elim i t Hfr); clear Hfr; intros Hfr | *)
                            destruct (Bfree_cpl_elim i _ _ Hfr); clear Hfr |
                            destruct (Bfree_pro_elim i _ _ Hfr); clear Hfr]
 end
with fold_all :=
 match goal with
 | Hfr:(Be_free_ 0 ?i ?e=⊥)|-_ => change (Be_free_ 0 i e=⊥) with (i∖e) in Hfr
 | Hfr:(Bp_free_ 0 ?i ?p=⊥)|-_ => change (Bp_free_ 0 i p=⊥) with (i∖p) in Hfr
 | |-(Be_free_ 0 ?i ?e=⊥) => change (Be_free_ 0 i e=⊥) with (i∖e)
 | |-(Bp_free_ 0 ?i ?p=⊥) => change (Bp_free_ 0 i p=⊥) with (i∖p)
 end.

(* Auxiliary results -----------------------------------------------------------------------------*)

Theorem repl_esubst:forall (G:Γ)(p p':Π)(i:Ι)(e:Ε), G⊢p⇒p'->(i∖ˠG)->G⊢〈i←e@Π@p〉->G⊢〈i←e@Π@p'〉.
Proof.
 intros G p p' i e Himp HiG Hp; apply (Bpr_cutr _ _ 〈i←e@Π@p'〉 Hp); apply Bpr_impe;
  change (〈i←e@Π@p〉⇒〈i←e@Π@p'〉) with 〈i←e@Π@p⇒p'〉; apply Bpr_fore; apply (Bpr_fori _ _ _ Himp HiG).
Qed.

Theorem Bpr_pro_dbli:forall (G:Γ)(e e1 e2:Ε), G⊢e∈e1×e2->
                     forall (i1 i2:Ι), G·e≡∂(i1)↦∂(i2)⊢(∂(i1)↦∂(i2))∈e1×e2.
Proof.
 intros G e e1 e2 Hmem i1 i2; destruct (fresh (e1×e2)) as [i Hi];
  replace ((∂(i1)↦∂(i2))∈e1×e2) with 〈i←∂(i1)↦∂(i2)@Π@∂(i)∈e1×e2〉;
  destruct (orb_false_elim _ _ Hi) as [Hie1 Hie2].
  apply (Bpr_leib) with (1:=Bpr_axom G (e≡∂(i1)↦∂(i2))); apply Bpr_clear; simpl; nat_simpl;
   bool_simpl; simpl in Hi; irewrite (Bt_esubst_free e1 i Hie1 e);
   irewrite (Bt_esubst_free e2 i Hie2 e); DB_simpl; simpl; apply Hmem.
  simpl; nat_simpl; bool_simpl; irewrite (Bt_esubst_free e1 i Hie1 (∂(i1)↦∂(i2)));
   irewrite (Bt_esubst_free e2 i Hie2 (∂(i1)↦∂(i2))); DB_simpl; simpl; apply refl_equal.
Qed.

Theorem Bpr_prol:forall (G:Γ)(e1 e2 E1 E2:Ε), G⊢e1↦e2∈E1×E2->G⊢e1∈E1.
Proof.
 intros G e1 e2 E1 E2 Hmem; generalize (Bg_fshun_free (G·e1↦e2∈E1×E2) _ (le_n_Sn _) 0);
  set (i1:=¡(S ⋇ˠ(G·e1↦e2∈E1×E2),0)); intros Hi1; assert (Hi1es:i1∖e1↦e2∈E1×E2). Btac_free.
 simpl in Hi1es; destruct (orb_false_elim _ _ Hi1es) as [H'i1 H''i1];
  destruct (orb_false_elim _ _ H'i1) as [Hi1e1 Hi1e2];
  destruct (orb_false_elim _ _ H''i1) as [Hi1E1 Hi1E2]; clear Hi1es H'i1 H''i1.
 generalize (Bg_fshun_free (G·e1↦e2∈E1×E2) _ (le_S _ _ (le_n_Sn _)) 0);
  set (i2:=¡(S (S ⋇ˠ(G·e1↦e2∈E1×E2)),0)); intros Hi2; assert (Hi2es:i2∖e1↦e2∈E1×E2). Btac_free.
 simpl in Hi2es; destruct (orb_false_elim _ _ Hi2es) as [H'i2 H''i2];
  destruct (orb_false_elim _ _ H'i2) as [Hi2e1 Hi2e2];
  destruct (orb_false_elim _ _ H''i2) as [Hi2E1 Hi2E2]; clear Hi2es H'i2 H''i2.
 apply Bpr_cutr with (p1:=∃(i1∙∂(i1)∈E1⋀∃(i2∙∂(i2)∈E2⋀e1↦e2≡∂(i1)↦∂(i2)))).
  apply Bpr_proe.
   reflect DB_eq_imp; simpl; nat_simpl; apply refl_equal.
   Btac_free.
   Btac_free.
   apply Hmem.
  apply Bpr_impe; apply Bpr_ctnp; apply Bpr_impi; apply Bpr_fori.
   apply Bpr_impe; apply Bpr_ctnn; apply bbdr8; apply Bpr_impi; apply Bpr_ctnp; apply Bpr_impi;
    apply Bpr_fori.
    apply Bpr_impe; apply Bpr_ctnn; apply bbdr8; apply Bpr_impi; apply Bpr_impi;
     apply Bpr_cutr with (p1:=e1≡∂(i1)).
     apply Bpr_cpll with (e2:=e2)(e4:=∂(i2)); apply Bpr_axom.
     replace (e1∈E1) with (〈i1←e1@Π@∂(i1)∈E1〉).
      apply Bpr_leib with (e1:=∂(i1)).
       apply Bpr_equ_sym; Btac_hyp.
       replace 〈i1←∂(i1)@Π@∂(i1)∈E1〉 with (∂(i1)∈E1).
        Btac_hyp.
        irewrite (Bt_esubst_id (∂(i1)∈E1) i1); nat_simpl; bool_simpl; intros _; apply refl_equal.
      simpl; nat_simpl; bool_simpl; irewrite (Bt_esubst_free E1 i1 Hi1E1 e1); apply refl_equal.
    Btac_free; unfold Bt_free_,Be_free_; DB_simpl; simpl; nat_simpl; apply refl_equal.
   Btac_free.
Qed.

Theorem Bpr_pror:forall (G:Γ)(e1 e2 E1 E2:Ε), G⊢e1↦e2∈E1×E2->G⊢e2∈E2.
Proof.
 intros G e1 e2 E1 E2 Hmem; generalize (Bg_fshun_free (G·e1↦e2∈E1×E2) _ (le_n_Sn _) 0);
  set (i1:=¡(S ⋇ˠ(G·e1↦e2∈E1×E2),0)); intros Hi1; assert (Hi1es:i1∖e1↦e2∈E1×E2).
  apply Bg_free_mem with (G:=G·e1↦e2∈E1×E2); [apply Hi1 | Btac_gamma].
 simpl in Hi1es; destruct (orb_false_elim _ _ Hi1es) as [H'i1 H''i1];
  destruct (orb_false_elim _ _ H'i1) as [Hi1e1 Hi1e2];
  destruct (orb_false_elim _ _ H''i1) as [Hi1E1 Hi1E2]; clear Hi1es H'i1 H''i1.
 generalize (Bg_fshun_free (G·e1↦e2∈E1×E2) _ (le_S _ _ (le_n_Sn _)) 0);
  set (i2:=¡(S (S ⋇ˠ(G·e1↦e2∈E1×E2)),0)); intros Hi2; assert (Hi2es:i2∖e1↦e2∈E1×E2).
  apply Bg_free_mem with (G:=G·e1↦e2∈E1×E2); [apply Hi2 | Btac_gamma].
 simpl in Hi2es; destruct (orb_false_elim _ _ Hi2es) as [H'i2 H''i2];
  destruct (orb_false_elim _ _ H'i2) as [Hi2e1 Hi2e2];
  destruct (orb_false_elim _ _ H''i2) as [Hi2E1 Hi2E2]; clear Hi2es H'i2 H''i2.
 apply Bpr_cutr with (p1:=∃(i1∙∂(i1)∈E1⋀∃(i2∙∂(i2)∈E2⋀e1↦e2≡∂(i1)↦∂(i2)))).
  apply Bpr_proe.
   reflect DB_eq_imp; simpl; nat_simpl; apply refl_equal.
   apply Bg_free_mem with (G:=G·e1↦e2∈E1×E2); [apply Hi1 | Btac_gamma].
   apply Bg_free_mem with (G:=G·e1↦e2∈E1×E2); [apply Hi2 | Btac_gamma].
   apply Hmem.
  apply Bpr_impe; apply Bpr_ctnp; apply Bpr_impi; apply Bpr_fori.
   apply Bpr_impe; apply Bpr_ctnn; apply bbdr8; apply Bpr_impi; apply Bpr_ctnp; apply Bpr_impi;
    apply Bpr_fori.
    apply Bpr_impe; apply Bpr_ctnn; apply bbdr8; apply Bpr_impi; apply Bpr_impi;
     apply Bpr_cutr with (p1:=e2≡∂(i2)).
     apply Bpr_cplr with (e1:=e1)(e3:=∂(i1)); apply Bpr_axom.
     replace (e2∈E2) with (〈i2←e2@Π@∂(i2)∈E2〉).
      apply Bpr_leib with (e1:=∂(i2)).
       apply Bpr_equ_sym; Btac_hyp.
       replace 〈i2←∂(i2)@Π@∂(i2)∈E2〉 with (∂(i2)∈E2).
        Btac_hyp.
        irewrite (Bt_esubst_id (∂(i2)∈E2) i2); nat_simpl; bool_simpl; intros _; apply refl_equal.
      simpl; nat_simpl; bool_simpl; irewrite (Bt_esubst_free E2 i2 Hi2E2 e2); apply refl_equal.
    apply Bg_mem_free; simpl; intros p Hp; destruct (orb_true_elim _ _ Hp) as [H''p | H'p];
     clear Hp; [ | destruct (orb_true_elim _ _ H'p) as [H''p | H''p]; clear H'p].
     reflect_in Bp_eq_imp H''p; rewrite H''p; simpl; rewrite Hi2e2; rewrite Hi2E2; apply refl_equal.
     reflect_in Bp_eq_imp H''p; rewrite H''p; simpl; nat_simpl; bool_simpl; apply Hi2E1.
     apply (Bg_free_mem _ _ (Bg_free_inc _ _ _ Hi2 (Bg_inc_add _ _)) _ H''p).
   apply Bg_mem_free; simpl; intros p Hp; destruct (orb_true_elim _ _ Hp) as [H'p | H'p].
    reflect_in Bp_eq_imp H'p; rewrite H'p; simpl; rewrite Hi1e2; rewrite Hi1E2; apply refl_equal.
    apply (Bg_free_mem _ _ (Bg_free_inc _ _ _ Hi1 (Bg_inc_add _ _)) _ H'p).
Qed.

Theorem Bpr_prodec:forall (G:Γ)(e1 e2 E1 E2:Ε), G⊢e1∈E1->G⊢e2∈E2->G⊢e1↦e2∈E1×E2.
Proof.
 intros G e1 e2 E1 E2 He1E1 He2E2; generalize (Bt_fshun_free (e1↦e2∈E1×E2) _ (le_n_Sn _) 0 0);
  set (i1:=¡(S ⋇(e1↦e2∈E1×E2),0)); intros Hi1; simpl in Hi1;
  destruct (orb_false_elim _ _ Hi1) as [H'i1 H''i1];
  destruct (orb_false_elim _ _ H'i1) as [Hi1e1 Hi1e2];
  destruct (orb_false_elim _ _ H''i1) as [Hi1E1 Hi1E2]; clear H'i1 H''i1.
 generalize (Bt_fshun_free (e1↦e2∈E1×E2) _ (le_n_Sn _) 1 0);
  set (i2:=¡(S ⋇(e1↦e2∈E1×E2),1)); intros Hi2; simpl in Hi2;
  destruct (orb_false_elim _ _ Hi2) as [H'i2 H''i2];
  destruct (orb_false_elim _ _ H'i2) as [Hi2e1 Hi2e2];
  destruct (orb_false_elim _ _ H''i2) as [Hi2E1 Hi2E2]; clear H'i2 H''i2.
 assert (Hdiff:i1<>i2). reflect DB_eq_imp; simpl; nat_simpl; bool_simpl; apply refl_equal.
 apply (Bpr_proi G (e1↦e2) E1 E2 i1 i2 Hdiff Hi1 Hi2).
 apply Bpr_exsi with (e:=e1).
 unfold Bt_esubst_; unfold Bp_esubst_; fold Be_esubst_ Bp_esubst_; DB_simpl;
  rewrite (Bp_forall_esubst_diff (¬(∂(i2)∈E2⋀e1↦e2≡∂(i1)↦∂(i2))) e1 i2 i1 Hdiff Hi2e1);
  apply Bpr_andi.
  irewrite (Bt_esubst_free E1 i1 Hi1E1 e1); apply He1E1.
  replace 〈i1←e1@Π@¬(∂(i2)∈E2⋀e1↦e2≡∂(i1)↦∂(i2))〉 with (¬(∂(i2)∈E2⋀e1↦e2≡e1↦∂(i2))).
   apply Bpr_exsi with (e:=e2); simpl; nat_simpl; bool_simpl;
    irewrite (Bt_esubst_free E2 i2 Hi2E2 e2); irewrite (Bt_esubst_free e1 i2 Hi2e1 e2);
    irewrite (Bt_esubst_free e2 i2 Hi2e2 e2); apply Bpr_andi; [apply He2E2 | apply Bpr_refl].
   simpl; nat_simpl; bool_simpl; irewrite (Bt_esubst_free E2 i1 Hi1E2 e1);
    irewrite (Bt_esubst_free e1 i1 Hi1e1 e1); irewrite (Bt_esubst_free e2 i1 Hi1e2 e1);
    apply refl_equal.
Qed.

(* Elementary replacements -----------------------------------------------------------------------*)

Theorem repl_not:forall (G:Γ)(p p':Π), G⊢p⇒p'->G⊢¬p'->G⊢¬p.
Proof.
 intros G p p' Himp Hp'; apply (Bpr_absn _ _ _ (Bpr_impe _ _ _ Himp)); apply Bpr_clear; apply Hp'.
Qed.

Theorem repl_for:forall (G:Γ)(p p':Π)(i:Ι), G⊢p⇒p'->(i∖ˠG)->G⊢∀(i∙p)->G⊢∀(i∙p').
Proof.
 intros G p p' i Himp HiG Hp; apply (Bpr_fori G p' i) with (2:=HiG);
  apply (Bpr_cutr G p p' (Bpr_fore_id _ _ _ Hp)); apply Bpr_impe; apply Himp.
Qed.

Theorem repl_imp_l:forall (G:Γ)(p p' pr:Π), G⊢p⇒p'->G⊢p'⇒pr->G⊢p⇒pr.
Proof.
 intros G p p' pr Himp Hp'; apply Bpr_modp with (1:=Hp'); apply Bpr_modp with (1:=Himp); Btac_prop.
Qed.

Theorem repl_imp_r:forall (G:Γ)(p p' pl:Π), G⊢p⇒p'->G⊢pl⇒p->G⊢pl⇒p'.
Proof.
 intros G p p' pl Himp Hp; apply Bpr_modp with (1:=Hp); apply Bpr_modp with (1:=Himp); Btac_prop.
Qed.

Theorem repl_and_l:forall (G:Γ)(p p' pr:Π), G⊢p⇒p'->G⊢p⋀pr->G⊢p'⋀pr.
Proof.
 intros G p p' pr Himp Hp; apply Bpr_andi; [ | apply (Bpr_andr _ _ _ Hp)].
  apply (Bpr_cutr _ _ _ (Bpr_andl _ _ _ Hp) (Bpr_impe _ _ _ Himp)).
Qed.

Theorem repl_and_r:forall (G:Γ)(p p' pl:Π), G⊢p⇒p'->G⊢pl⋀p->G⊢pl⋀p'.
Proof.
 intros G p p' pl Himp Hp; apply Bpr_andi; [apply (Bpr_andl _ _ _ Hp) | ].
  apply (Bpr_cutr _ _ _ (Bpr_andr _ _ _ Hp) (Bpr_impe _ _ _ Himp)).
Qed.

Theorem repl_equ_l:forall (G:Γ)(e e' er:Ε), G⊢e≡e'->G⊢e≡er->G⊢e'≡er.
Proof.
 intros G e e' er Hequ He; apply (Bpr_equ_trans _ _ _ _ (Bpr_equ_sym _ _ _ Hequ) He).
Qed.

Theorem repl_equ_r:forall (G:Γ)(e e' el:Ε), G⊢e≡e'->G⊢el≡e->G⊢el≡e'.
Proof.
 intros G e e' el Hequ He; apply Bpr_equ_sym; apply (repl_equ_l _ _ _ el Hequ); apply Bpr_equ_sym;
  apply He.
Qed.

Theorem repl_ins_l:forall (G:Γ)(e e' er:Ε), G⊢e≡e'->G⊢e∈er->G⊢e'∈er.
Proof.
 intros G e e' er Hequ He; destruct (fresh er) as [i Hier]; replace (e'∈er) with 〈i←e'@Π@∂(i)∈er〉.
  apply (Bpr_leib G (∂(i)∈er) _ _ i Hequ); simpl; nat_simpl; simpl;
   irewrite (Bt_esubst_free _ _ Hier e); DB_simpl; apply He.
  simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ Hier e'); DB_simpl; apply refl_equal.
Qed.

Theorem repl_ins_r:forall (G:Γ)(e e' el:Ε), G⊢e⊆e'->G⊢el∈e->G⊢el∈e'.
Proof.
 intros G e e' el Hinc He; apply Bpr_cutr with (1:=He); apply Bpr_impe;
  destruct (fresh (e∈e')) as [i Hi]; simpl in Hi; destruct (orb_false_elim _ _ Hi) as [Hie Hie'];
  replace  (el∈e⇒el∈e') with 〈i←el@Π@∂(i)∈e⇒∂(i)∈e'〉.
  apply Bpr_fore; unfold binc in Hinc; apply (Bpr_powe G _ _ _ Hie Hie' Hinc).
  simpl; nat_simpl; simpl; irewrite (Bt_esubst_free e i Hie el);
   irewrite (Bt_esubst_free e' i Hie' el); DB_simpl; apply refl_equal.
Qed.

Theorem repl_chc:forall (G:Γ)(e e':Ε), G⊢e≡e'->G⊢↓(e)≡↓(e').
Proof.
 intros G e e' Hequ; destruct (fresh e) as [i Hi]; replace (↓(e)≡↓(e')) with 〈i←e'@Π@↓(e)≡↓(∂(i))〉.
  apply Bpr_leib with (1:=Hequ); simpl; nat_simpl; simpl; irewrite (Bt_esubst_free e i Hi e);
   DB_simpl; apply Bpr_refl.
  simpl; nat_simpl; simpl; irewrite (Bt_esubst_free e i Hi e'); DB_simpl; apply refl_equal.
Qed.

Theorem repl_pow:forall (G:Γ)(e e':Ε), G⊢e⊆e'->G⊢↑(e)⊆↑(e').
Proof.
 intros G e e'; unfold binc; intros Hinc; generalize (Bg_fshun_free (G·e∈e') _ (le_n_Sn _) 0);
  set (i:=¡(S ⋇ˠ(G·e∈e'),0)); intros Hi.
 assert (HiG:i∖ˠG). apply Bg_free_inc with (1:=Hi); Btac_gamma.
 assert (Hiee':i∖e∈e'). apply Bg_free_mem with (1:=Hi); Btac_gamma.
 simpl in Hiee'; destruct (orb_false_elim _ _ Hiee') as [Hie Hie'];
  apply (Bpr_powi G ↑(e) ↑(e') i Hie Hie').
 apply Bpr_fori with (2:=HiG); apply Bpr_impi.
 generalize (Bg_fshun_free (G·e∈e') _ (le_n_Sn _) 1); set (i':=¡(S ⋇ˠ(G·e∈e'),1)); intros Hi'.
 assert (Hi'G:i'∖ˠG). apply Bg_free_inc with (1:=Hi'); Btac_gamma.
 assert (Hi'ee':i'∖e∈e'). apply Bg_free_mem with (1:=Hi'); Btac_gamma.
 assert (Hi'e:i'∖e). simpl in Hi'ee'; destruct (orb_false_elim _ _ Hi'ee') as [H _]; apply H.
 assert (Hi'e':i'∖e'). simpl in Hi'ee'; destruct (orb_false_elim _ _ Hi'ee') as [_ H]; apply H.
 assert (Hi'i:i'∖∂(i)). simpl; nat_simpl; bool_simpl; apply refl_equal.
 apply (Bpr_powi (G·∂(i)∈↑(e)) ∂(i) e' i' Hi'i Hi'e').
 assert (Hi'Gie:i'∖ˠG·∂(i)∈↑(e)).
  apply Bg_mem_free; simpl; intros p Hp; destruct (orb_true_elim _ _ Hp) as [H'p | H'p].
   reflect_in Bp_eq_imp H'p; rewrite H'p; simpl; nat_simpl; bool_simpl; apply Hi'e.
   apply Bg_free_mem with (1:=Hi'G)(2:=H'p).
 apply Bpr_fori with (2:=Hi'Gie); apply Bpr_impi;
  apply (Bpr_cutr (G·∂(i)∈↑(e)·∂(i')∈∂(i)) (∂(i')∈e) (∂(i')∈e')).
  apply Bpr_impe; replace (∂(i')∈∂(i)⇒∂(i')∈e) with 〈i'←∂(i')@Π@∂(i')∈∂(i)⇒∂(i')∈e〉;
  [ | simpl; nat_simpl; bool_simpl; irewrite (Bt_esubst_free e i' Hi'e ∂(i')); apply refl_equal].
   apply Bpr_fore; apply Bpr_powe; [apply Hi'i | apply Hi'e | apply Bpr_axom].
  apply Bpr_impe; apply Bpr_clear; apply Bpr_clear;
  replace (∂(i')∈e⇒∂(i')∈e') with 〈i'←∂(i')@Π@∂(i')∈e⇒∂(i')∈e'〉;
  [ | simpl; nat_simpl; bool_simpl; irewrite (Bt_esubst_free e i' Hi'e ∂(i'));
      irewrite (Bt_esubst_free e' i' Hi'e' ∂(i')); apply refl_equal].
   apply Bpr_fore; apply Bpr_powe; [apply Hi'e | apply Hi'e' | apply Hinc].
Qed.

Theorem repl_cpl_l:forall (G:Γ)(e e' er:Ε), G⊢e≡e'-> G⊢e↦er≡e'↦er.
Proof.
 intros G e e' er Hequ; destruct (fresh (e↦er)) as [i Hi]; simpl in Hi;
  destruct (orb_false_elim _ _ Hi) as [Hie Hier]; replace (e↦er≡e'↦er) with 〈i←e'@Π@e↦er≡∂(i)↦er〉.
   apply Bpr_leib with (1:=Hequ); simpl; nat_simpl; simpl; irewrite (Bt_esubst_free e i Hie e);
    irewrite (Bt_esubst_free er i Hier e); DB_simpl; apply Bpr_refl.
   simpl; nat_simpl; simpl; irewrite (Bt_esubst_free e i Hie e');
    irewrite (Bt_esubst_free er i Hier e'); DB_simpl; apply refl_equal.
Qed.

Theorem repl_cpl_r:forall (G:Γ)(e e' el:Ε), G⊢e≡e'->G⊢el↦e≡el↦e'.
Proof.
 intros G e e' el Hequ; destruct (fresh (el↦e)) as [i Hi]; simpl in Hi;
  destruct (orb_false_elim _ _ Hi) as [Hiel Hie]; replace (el↦e≡el↦e') with 〈i←e'@Π@el↦e≡el↦∂(i)〉.
   apply Bpr_leib with (1:=Hequ); simpl; nat_simpl; simpl; irewrite (Bt_esubst_free el i Hiel e);
    irewrite (Bt_esubst_free e i Hie e); DB_simpl; apply Bpr_refl.
   simpl; nat_simpl; simpl; irewrite (Bt_esubst_free el i Hiel e');
    irewrite (Bt_esubst_free e i Hie e'); DB_simpl; apply refl_equal.
Qed.

Theorem repl_pro_l:forall (G:Γ)(e e' er:Ε), G⊢e⊆e'->G⊢e×er⊆e'×er.
Proof.
 intros G e e' er; unfold binc; intros Hinc; destruct (gfresh (G·e∈e'×er)) as [i Hi];
  assert (HiG:i∖ˠG).
  apply Bg_free_inc with (G1:=G·e∈e'×er); [apply Hi | Btac_gamma].
 assert (Hies:i∖e∈e'×er). apply Bg_free_mem with (G:=G·e∈e'×er); [apply Hi | Btac_gamma].
 simpl in Hies; destruct (orb_false_elim _ _ Hies) as [Hie H'ies];
  destruct (orb_false_elim _ _ H'ies) as [Hie' Hier]; clear Hies H'ies;
  apply (Bpr_powi G (e×er) (e'×er) i (orb_false_intro _ _ Hie Hier)
                                      (orb_false_intro _ _ Hie' Hier));
  apply Bpr_fori with (2:=HiG); apply Bpr_impi.
 destruct (gfresh (G·∂(i)×e∈e'×er)) as [i1 Hi1]; assert (Hi1G:i1∖ˠG).
  apply Bg_free_inc with (G1:=G·∂(i)×e∈e'×er); [apply Hi1 | Btac_gamma].
 assert (Hi1es:i1∖∂(i)×e∈e'×er).
  apply Bg_free_mem with (G:=G·∂(i)×e∈e'×er); [apply Hi1 | Btac_gamma].
 destruct (orb_false_elim _ _ Hi1es) as [H'i1es H''i1es];  fold Be_free_ Bp_free_ in H'i1es;
  fold Be_free_ Bp_free_ in H''i1es; destruct (orb_false_elim _ _ H'i1es) as [Hi1i Hi1e];
  destruct (orb_false_elim _ _ H''i1es) as [Hi1e' Hi1er]; clear Hi1es H'i1es H''i1es.
 destruct (gfresh (G·∂(i1)×∂(i)×e∈e'×er)) as [i2 Hi2]; assert (Hi2G:i2∖ˠG).
  apply Bg_free_inc with (G1:=G·∂(i1)×∂(i)×e∈e'×er); [apply Hi2 | Btac_gamma].
 assert (Hi2es:i2∖∂(i1)×∂(i)×e∈e'×er).
  apply Bg_free_mem with (G:=G·∂(i1)×∂(i)×e∈e'×er); [apply Hi2 | Btac_gamma].
 destruct (orb_false_elim _ _ Hi2es) as [H'i2es H''i2es]; fold Be_free_ Bp_free_ in H'i2es;
  fold Be_free_ Bp_free_ in H''i2es; destruct (orb_false_elim _ _ H'i2es) as [H'''i2es Hi2e];
  destruct (orb_false_elim _ _ H''i2es) as [Hi2e' Hi2er];
  destruct (orb_false_elim _ _ H'''i2es) as [Hi2i1 Hi2i]; clear Hi2es H'i2es H''i2es H'''i2es;
  DB_simpl; bool_simpl; Btac_free.
 apply (Bpr_proi (G·∂(i)∈e×er) ∂(i) e' er i2 i1 ((snd DB_eq_imp) _ _ Hi2i1)); Btac_free.
 apply Bpr_cutr with (p1:=¬∀(i2∙¬(∂(i2)∈e⋀¬∀(i1∙¬(∂(i1)∈er⋀∂(i)≡∂(i2)↦∂(i1)))))).
  apply (Bpr_proe (G·∂(i)∈e×er) ∂(i) e er _ _ ((snd DB_eq_imp) _ _ Hi2i1)); Btac_free; Btac_prop.
  apply Bpr_impe; apply Bpr_clear; apply Bpr_ctnn; apply Bpr_impi;
   assert (H'i2G:i2∖ˠG·∀(i2∙¬(∂(i2)∈e'⋀¬∀(i1∙¬(∂(i1)∈er⋀∂(i)≡∂(i2)↦∂(i1)))))).
   apply Bg_mem_free; simpl; intros p Hp; destruct (orb_true_elim _ _ Hp) as [Hpp' | HpG].
    reflect_in Bp_eq_imp Hpp'; rewrite Hpp';
     apply (Bp_forall_free i2 (¬(∂(i2)∈e'⋀¬∀(i1∙¬(∂(i1)∈er⋀∂(i)≡∂(i2)↦∂(i1)))))).
    apply (Bg_free_mem _ _ Hi2G _ HpG).
  apply Bpr_fori with (2:=H'i2G);
   apply Bpr_cutr with (p1:=¬(∂(i2)∈e'⋀¬∀(i1∙¬(∂(i1)∈er⋀∂(i)≡∂(i2)↦∂(i1))))).
   apply Bpr_fore_id with (i:=i2); apply Bpr_axom.
   apply Bpr_impe; apply Bpr_ctnn; apply Bpr_clear; apply Bpr_impi;
    apply repl_and_l with (p:=∂(i2)∈e); [ | apply Bpr_axom].
    apply Bpr_clear; apply Bpr_fore_id with (i:=i2); apply Bpr_powe with (3:=Hinc);
     [apply Hi2e | apply Hi2e'].
Qed.

Theorem repl_pro_r:forall (G:Γ)(e e' el:Ε), G⊢e⊆e'->G⊢el×e⊆el×e'.
Proof.
 intros G e e' el; unfold binc; intros Hinc; destruct (gfresh (G·e∈e'×el)) as [i Hi]; Btac_free.
 apply (Bpr_powi G (el×e) (el×e') i); Btac_free; apply Bpr_fori with (2:=H); apply Bpr_impi.
 destruct (gfresh (G·∂(i)×e∈e'×el)) as [i1 Hi1]; Btac_free.
 destruct (gfresh (G·∂(i1)×∂(i)×e∈e'×el)) as [i2 Hi2]; Btac_free.
 simpl in H10; DB_simpl; bool_simpl;
  apply (Bpr_proi (G·∂(i)∈el×e) ∂(i) el e' i2 i1 ((snd DB_eq_imp) _ _ H10)); Btac_free.
 apply Bpr_cutr with (p1:=¬∀(i2∙¬(∂(i2)∈el⋀¬∀(i1∙¬(∂(i1)∈e⋀∂(i)≡∂(i2)↦∂(i1)))))).
  apply (Bpr_proe (G·∂(i)∈el×e) ∂(i) el e _ _ ((snd DB_eq_imp) _ _ H10)); Btac_free; Btac_prop.
  apply Bpr_impe; apply Bpr_clear; apply Bpr_ctnn; apply Bpr_impi;
   assert (H'i2G:i2∖ˠG·∀(i2∙¬(∂(i2)∈el⋀¬∀(i1∙¬(∂(i1)∈e'⋀∂(i)≡∂(i2)↦∂(i1)))))).
   apply Bg_mem_free; simpl; intros p Hp; destruct (orb_true_elim _ _ Hp) as [Hpp' | HpG].
    reflect_in Bp_eq_imp Hpp'; rewrite Hpp';
     apply (Bp_forall_free i2 (¬(∂(i2)∈el⋀¬∀(i1∙¬(∂(i1)∈e'⋀∂(i)≡∂(i2)↦∂(i1)))))).
    apply (Bg_free_mem _ _ H5 _ HpG).
  apply Bpr_fori with (2:=H'i2G);
   apply Bpr_cutr with (p1:=¬(∂(i2)∈el⋀¬∀(i1∙¬(∂(i1)∈e'⋀∂(i)≡∂(i2)↦∂(i1))))).
   apply Bpr_fore_id with (i:=i2); apply Bpr_axom.
   apply Bpr_impe; apply Bpr_ctnn; apply Bpr_clear; Btac_prop; apply Bpr_impe; apply Bpr_clear;
    apply Bpr_ctnn; apply Bpr_impi; apply Bpr_fori.
    apply Bpr_cutr with (p1:=¬(∂(i1)∈e'⋀∂(i)≡∂(i2)↦∂(i1))).
     apply Bpr_fore_id with (i:=i1); apply Bpr_axom.
     apply Bpr_impe; apply Bpr_clear; apply Bpr_ctnn; Btac_prop; apply Bpr_clear; apply Bpr_impe;
      apply Bpr_fore_id with (i:=i1); apply (Bpr_powe _ _ _ _ H8 H4 Hinc).
    apply Bg_mem_free; simpl; intros p Hp; destruct (orb_true_elim _ _ Hp) as [H'p | H'p].
     reflect_in Bp_eq_imp H'p; rewrite H'p; Btac_free; apply Bp_forall_free.
     Btac_free; apply Bg_free_mem with (2:=H'p); apply H2.
Qed.

Theorem repl_cmp_l:forall (G:Γ)(p p':Π)(e:Ε)(i:Ι), G⊢p⇒p'->(i∖ˠG)->G⊢{i∊e∣p}⊆{i∊e∣p'}.
Proof.
 intros G p p' e i Himp HiG; unfold binc; destruct (gfresh (G·∂(i)∈{i∊e∣p}×{i∊e∣p'})) as [i' Hi'];
  Btac_free.
 apply (Bpr_powi G _ _ _ H0 H3); apply Bpr_fori with (2:=H); apply Bpr_impi; apply Bpr_cmpi.
  apply Bpr_cmpl with (i:=i)(p:=p); apply Bpr_axom.
  apply Bpr_cutr with (p1:=〈i←∂(i')@Π@p〉).
   apply Bpr_cmpr with (e2:=e); apply Bpr_axom.
   apply Bpr_impe; apply Bpr_clear; change (〈i←∂(i')@Π@p〉⇒〈i←∂(i')@Π@p'〉) with 〈i←∂(i')@Π@p⇒p'〉;
    apply (Bpr_sbsi _ _ ∂(i') _ Himp HiG).
Qed.

Theorem repl_cmp_r:forall (G:Γ)(p:Π)(e e':Ε)(i:Ι), G⊢e⊆e'->G⊢{i∊e∣p}⊆{i∊e'∣p}.
Proof.
 intros G p e e' i; unfold binc; intros Hinc;
  destruct (gfresh (G·∂(i)∈{i∊e∣p}×{i∊e'∣p})) as [i' Hi']; Btac_free.
 apply (Bpr_powi G _ _ _ H0 H3); apply Bpr_fori with (2:=H); apply Bpr_impi; apply Bpr_cmpi.
  apply Bpr_cutr with (p1:=∂(i')∈e).
   apply Bpr_cmpl with (i:=i)(p:=p); apply Bpr_axom.
   apply Bpr_impe; apply Bpr_clear; apply Bpr_fore_id with (i:=i'); apply Bpr_powe with (3:=Hinc);
    simpl in *; [apply (proj1 (orb_false_elim _ _ H0)) | apply (proj1 (orb_false_elim _ _ H3))].
  apply Bpr_cmpr with (e2:=e); apply Bpr_axom.
Qed.

(* Congruence-like results -----------------------------------------------------------------------*)

Theorem congr_not:forall (G:Γ)(p1 p2:Π), G⊢p1⇔p2->G⊢¬p1⇔¬p2.
Proof.
 intros G p1 p2 Hiff; apply (Bpr_cutr _ _ (¬p1⇔¬p2) Hiff); apply Bpr_impe; Btac_prop.
Qed.

Theorem congr_for:forall (G:Γ)(p1 p2:Π), G⊢p1⇔p2->forall (i:Ι), (i∖ˠG)->G⊢∀(i∙p1)⇔∀(i∙p2).
Proof.
 intros G p1 p2 Hiff i Hi; unfold biff; apply Bpr_andi; apply Bpr_impi.
  apply Bpr_fori; Btac_free; apply Bpr_cutr with (p1:=p1).
   pattern p1 at 2; replace p1 with 〈i←∂(i)@Π@p1〉;
   [apply Bpr_fore; apply Bpr_axom | irewrite (Bt_esubst_id p1 i); apply refl_equal].
   apply Bpr_impe; apply Bpr_clear; apply (Bpr_andl _ _ _ Hiff).
  apply Bpr_fori; Btac_free; apply Bpr_cutr with (p1:=p2).
   pattern p2 at 2; replace p2 with 〈i←∂(i)@Π@p2〉;
   [apply Bpr_fore; apply Bpr_axom | irewrite (Bt_esubst_id p2 i); apply refl_equal].
   apply Bpr_impe; apply Bpr_clear; apply (Bpr_andr _ _ _ Hiff).
Qed.

Theorem congr_for_raw:forall (G:Γ)(p1 p2:Π)(u n:nat), (¡(S u,n)∖ˠG·p1·p2)->
                      G⊢(Bp_app_ 0 p1 ∂(¡(S u,n)))⇔(Bp_app_ 0 p2 ∂(¡(S u,n)))->G⊢∀Δ(p1)⇔∀Δ(p2).
Proof.
 intros G p1 p2 u n Hfree Hiff; replace ∀Δ(p1) with ∀(¡(S u,n)∙Bp_app_ 0 p1 ∂(¡(S u,n))).
  replace ∀Δ(p2) with ∀(¡(S u,n)∙Bp_app_ 0 p2 ∂(¡(S u,n))).
   apply congr_for.
    apply Hiff.
    apply (Bg_free_inc _ G _ Hfree); reflect Bg_inc_imp; intros p Hp; Btac_gamma.
   apply sym_equal; apply (Bp_forall_inv p2 ¡(S u,n)); apply (Bg_free_mem _ _ Hfree); Btac_gamma.
  apply sym_equal; apply (Bp_forall_inv p1 ¡(S u,n)); apply (Bg_free_mem _ _ Hfree); Btac_gamma.
Qed.

Theorem congr_and:forall (G:Γ)(p1 p1' p2 p2':Π), G⊢p1⇔p1'->G⊢p2⇔p2'->G⊢(p1⋀p2)⇔(p1'⋀p2').
Proof.
 intros G p1 p1' p2 p2' Hiff1 Hiff2; apply (Bpr_cutr _ _ ((p1⋀p2)⇔(p1'⋀p2')) Hiff1); apply Bpr_impe;
  apply (Bpr_cutr _ _ ((p1⇔p1')⇒((p1⋀p2)⇔(p1'⋀p2'))) Hiff2); apply Bpr_impe; Btac_prop.
Qed.

Theorem congr_imp:forall (G:Γ)(p1 p1' p2 p2':Π), G⊢p1⇔p1'->G⊢p2⇔p2'->G⊢(p1⇒p2)⇔(p1'⇒p2').
Proof.
 intros G p1 p1' p2 p2' Hiff1 Hiff2; apply (Bpr_cutr _ _ ((p1⇒p2)⇔(p1'⇒p2')) Hiff1); apply Bpr_impe;
  apply (Bpr_cutr _ _ ((p1⇔p1')⇒((p1⇒p2)⇔(p1'⇒p2'))) Hiff2); apply Bpr_impe; Btac_prop.
Qed.

Theorem congr_equ:forall (G:Γ)(e1 e1' e2 e2':Ε), G⊢e1≡e1'->G⊢e2≡e2'->G⊢(e1≡e2)⇔(e1'≡e2').
Proof.
 intros G e1 e1' e2 e2' Hequ1 Hequ2; unfold biff; apply Bpr_andi; apply Bpr_impi.
  destruct (fresh (e1'≡e2')) as [i Hi]; replace (e1'≡e2') with 〈i←e1'@Π@∂(i)≡e2'〉.
   apply Bpr_leib with (e1:=e1).
    apply Bpr_clear; apply Hequ1.
    simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ Hi)) e1);
     DB_simpl; simpl; clear i Hi; destruct (fresh (e1≡e2')) as [i Hi];
     replace (e1≡e2') with 〈i←e2'@Π@e1≡∂(i)〉.
      apply Bpr_leib with (e1:=e2).
       apply  Bpr_clear; apply Hequ2.
       simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hi)) e2);
        DB_simpl; apply Bpr_axom.
      simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hi)) e2');
       DB_simpl; apply refl_equal.
     simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ Hi)) e1');
      DB_simpl; apply refl_equal.
  destruct (fresh (e1≡e2)) as [i Hi]; replace (e1≡e2) with 〈i←e1@Π@∂(i)≡e2〉.
   apply Bpr_leib with (e1:=e1').
    apply Bpr_clear; apply Bpr_equ_sym; apply Hequ1.
    simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ Hi)) e1');
     DB_simpl; simpl; clear i Hi; destruct (fresh (e1'≡e2)) as [i Hi];
     replace (e1'≡e2) with 〈i←e2@Π@e1'≡∂(i)〉.
      apply Bpr_leib with (e1:=e2').
       apply  Bpr_clear; apply Bpr_equ_sym; apply Hequ2.
       simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hi)) e2');
        DB_simpl; apply Bpr_axom.
      simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hi)) e2);
       DB_simpl; apply refl_equal.
     simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ Hi)) e1);
      DB_simpl; apply refl_equal.
Qed.

Theorem congr_ins:forall (G:Γ)(e1 e1' e2 e2':Ε), G⊢e1≡e1'->G⊢e2≡e2'->G⊢(e1∈e2)⇔(e1'∈e2').
Proof.
 intros G e1 e1' e2 e2' Hequ1 Hequ2; unfold biff; apply Bpr_andi; apply Bpr_impi.
  destruct (fresh (e1'∈e2')) as [i Hi]; replace (e1'∈e2') with 〈i←e1'@Π@∂(i)∈e2'〉.
   apply Bpr_leib with (e1:=e1).
    apply Bpr_clear; apply Hequ1.
    simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ Hi)) e1);
     DB_simpl; simpl; clear i Hi; destruct (fresh (e1∈e2')) as [i Hi];
     replace (e1∈e2') with 〈i←e2'@Π@e1∈∂(i)〉.
      apply Bpr_leib with (e1:=e2).
       apply  Bpr_clear; apply Hequ2.
       simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hi)) e2);
        DB_simpl; apply Bpr_axom.
      simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hi)) e2');
       DB_simpl; apply refl_equal.
     simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ Hi)) e1');
      DB_simpl; apply refl_equal.
  destruct (fresh (e1∈e2)) as [i Hi]; replace (e1∈e2) with 〈i←e1@Π@∂(i)∈e2〉.
   apply Bpr_leib with (e1:=e1').
    apply Bpr_clear; apply Bpr_equ_sym; apply Hequ1.
    simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ Hi)) e1');
     DB_simpl; simpl; clear i Hi; destruct (fresh (e1'∈e2)) as [i Hi];
     replace (e1'∈e2) with 〈i←e2@Π@e1'∈∂(i)〉.
      apply Bpr_leib with (e1:=e2').
       apply  Bpr_clear; apply Bpr_equ_sym; apply Hequ2.
       simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hi)) e2');
        DB_simpl; apply Bpr_axom.
      simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hi)) e2);
       DB_simpl; apply refl_equal.
     simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ Hi)) e1);
      DB_simpl; apply refl_equal.
Qed.

Theorem congr_chc:forall (G:Γ)(e1 e2:Ε), G⊢e1≡e2->G⊢↓(e1)≡↓(e2).
Proof.
 intros G e1 e2 Hequ; destruct (fresh (e1≡e2)) as [i Hi];
  replace (↓(e1)≡↓(e2)) with 〈i←e2@Π@↓(e1)≡↓(∂(i))〉.
   apply Bpr_leib with (e1:=e1).
    apply Hequ.
    simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hi)) e1);
     DB_simpl; apply Bpr_refl.
   simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hi)) e2);
    DB_simpl; apply refl_equal.
Qed.

Theorem congr_pow:forall (G:Γ)(e1 e2:Ε), G⊢e1≡e2->G⊢↑(e1)≡↑(e2).
Proof.
 intros G e1 e2 Hequ; destruct (fresh (e1≡e2)) as [i Hi];
  replace (↑(e1)≡↑(e2)) with 〈i←e2@Π@↑(e1)≡↑(∂(i))〉.
   apply Bpr_leib with (e1:=e1).
    apply Hequ.
    simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hi)) e1);
     DB_simpl; apply Bpr_refl.
   simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hi)) e2);
    DB_simpl; apply refl_equal.
Qed.

Theorem congr_cpl:forall (G:Γ)(e1 e1' e2 e2':Ε), G⊢e1≡e1'->G⊢e2≡e2'->G⊢(e1↦e2)≡(e1'↦e2').
Proof.
 intros G e1 e1' e2 e2' Hequ1 Hequ2; destruct (fresh ((e1↦e2)≡(e1'↦e2'))) as [i Hi];
  replace ((e1↦e2)≡(e1'↦e2')) with 〈i←e1'@Π@(e1↦e2)≡∂(i)↦e2'〉.
   apply Bpr_leib with (e1:=e1).
    apply Hequ1.
    simpl; nat_simpl; simpl;
     irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e1);
     irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e1);
     irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ (proj2 (Bt_free_sub _ _ Hi)))) e1);
     DB_simpl; simpl; clear i Hi; destruct (fresh ((e1↦e2)≡(e1↦e2'))) as [i Hi];
      replace ((e1↦e2)≡(e1↦e2')) with 〈i←e2'@Π@(e1↦e2)≡e1↦∂(i)〉.
      apply Bpr_leib with (e1:=e2).
       apply Hequ2.
       simpl; nat_simpl; simpl;
        irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e2);
        irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e2);
        DB_simpl; apply Bpr_refl.
      simpl; nat_simpl; simpl;
       irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e2');
       irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e2');
       DB_simpl; apply refl_equal.
  simpl; nat_simpl; simpl;
   irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e1');
   irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e1');
   irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ (proj2 (Bt_free_sub _ _ Hi)))) e1');
   DB_simpl; apply refl_equal.
Qed.

Theorem congr_pro:forall (G:Γ)(e1 e1' e2 e2':Ε), G⊢e1≡e1'->G⊢e2≡e2'->G⊢(e1×e2)≡(e1'×e2').
Proof.
 intros G e1 e1' e2 e2' Hequ1 Hequ2; destruct (fresh ((e1×e2)≡(e1'×e2'))) as [i Hi];
  replace ((e1×e2)≡(e1'×e2')) with 〈i←e1'@Π@(e1×e2)≡∂(i)×e2'〉.
   apply Bpr_leib with (e1:=e1).
    apply Hequ1.
    simpl; nat_simpl; simpl;
     irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e1);
     irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e1);
     irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ (proj2 (Bt_free_sub _ _ Hi)))) e1);
     DB_simpl; simpl; clear i Hi; destruct (fresh ((e1×e2)≡(e1×e2'))) as [i Hi];
      replace ((e1×e2)≡(e1×e2')) with 〈i←e2'@Π@(e1×e2)≡e1×∂(i)〉.
      apply Bpr_leib with (e1:=e2).
       apply Hequ2.
       simpl; nat_simpl; simpl;
        irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e2);
        irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e2);
        DB_simpl; apply Bpr_refl.
      simpl; nat_simpl; simpl;
       irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e2');
       irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e2');
       DB_simpl; apply refl_equal.
  simpl; nat_simpl; simpl;
   irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e1');
   irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e1');
   irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ (proj2 (Bt_free_sub _ _ Hi)))) e1');
   DB_simpl; apply refl_equal.
Qed.

Theorem congr_cmp:forall (G:Γ)(e e':Ε)(p p':Π), G⊢e≡e'->G⊢p⇔p'->
                  forall (i:Ι), (i∖ˠG)->(i∖e)->G⊢{i∊e∣p}≡{i∊e'∣p'}.
Proof.
 intros G e e' p p' Hequ Hiff i HiG Hie; destruct (fresh (∂(i)∈(e×e')⋀(p⋀p'))) as [i' Hi'];
  Btac_free; assert (Heq:{i∊e∣p}≡{i∊e'∣p'}=〈i'←e'@Π@{i∊e∣p}≡{i∊∂(i')∣p'}〉).
  simpl; DB_simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ H e');
   irewrite (Bt_esubst_free_ _ _ _ (Bt_abstr_free_free_ _ 0 _ i H1) ⇑Ε(e'));
   irewrite (Bt_esubst_free_ _ _ _ (Bt_abstr_free_free_ _ 0 _ i H2) ⇑Ε(e')); apply refl_equal.
 rewrite Heq; clear Heq; apply Bpr_leib with (e1:=e); [apply Hequ | ].
 simpl; DB_simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ H e);
  irewrite (Bt_esubst_free_ _ _ _ (Bt_abstr_free_free_ _ 0 _ i H1) ⇑Ε(e));
  irewrite (Bt_esubst_free_ _ _ _ (Bt_abstr_free_free_ _ 0 _ i H2) ⇑Ε(e)); apply Bpr_extn.
 fold (Be_cmpset i e p); fold (Be_cmpset i e p');
  apply (repl_cmp_l G p p' e i (Bpr_andl _ _ _ Hiff) HiG).
 fold (Be_cmpset i e p); fold (Be_cmpset i e p');
  apply (repl_cmp_l G p' p e i (Bpr_andr _ _ _ Hiff) HiG).
Qed.

(*================================================================================================*)