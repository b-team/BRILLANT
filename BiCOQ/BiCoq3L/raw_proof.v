(*==================================================================================================
  Project : BiCoq3
  Module : raw_proof.v
  In this module we provide so-called raw inference rules, that is inference rules expressed in the
  internal representation without masking De Bruijn levels.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Sep 2008, Md: Sep 2008
  ================================================================================================*)

Require Export swap.

Theorem Bpr_fori_raw:forall (G:Γ)(p:Π)(u n:nat), (¡(S u,n)∖ˠG·p)->G⊢Bp_app p ∂(¡(S u,n))->G⊢∀Δ(p).
Proof.
 intros G p u n HiGp Happ; assert (Hip:¡(S u,n)∖p). apply (Bg_free_mem _ _ HiGp p); Btac_gamma.
 assert (HiG:¡(S u,n)∖ˠG). apply (Bg_free_inc _ G _ HiGp (Bg_inc_add _ _)).
 rewrite (Bp_forall_inv p ¡(S u,n) Hip Hip); apply (Bpr_fori _ _ _ Happ HiG).
Qed.

Theorem Bpr_fore_raw:forall (G:Γ)(p:Π)(e:Ε), G⊢∀Δ(p)->G⊢Bp_app p e.
Proof.
 intros G p e Hp; destruct (gfresh (G·p) 1) as [n HnGp];
  assert (Hnp:¡(1,n)∖p). apply (Bg_free_mem _ _ HnGp p); Btac_gamma.
 assert (HnG:¡(1,n)∖ˠG). apply (Bg_free_inc _ G _ HnGp (Bg_inc_add _ _)).
 rewrite (Bp_forall_inv p ¡(1,n) Hnp Hnp) in Hp; generalize (Bpr_fore _ _ e _ Hp);
  irewrite (sym_equal (Bt_esubst_dec (Bp_app p ∂(¡(1,n))) e ¡(1,n)));
  irewrite (Bp_abstr_app _ _ Hnp Hnp); intros H'p; apply H'p.
Qed.

Theorem Bpr_cmpi_raw:forall (G:Γ)(p:Π)(e e':Ε)(u n:nat),
                     (¡(S u,n)∖p)->G⊢e'∈e->G⊢Bp_app p e'->G⊢e'∈ⅭΔ(e,p).
Proof.
 intros G p e e' u n Hip He'e Happ; rewrite (Be_cmpset_inv p e ¡(S u,n) Hip Hip);
  apply (Bpr_cmpi G (Bp_app p ∂(¡(S u,n))) _ _ ¡(S u,n) He'e);
  irewrite (sym_equal (Bt_esubst_dec (Bp_app p ∂(¡(S u,n))) e' ¡(S u,n)));
  irewrite (Bp_abstr_app _ _ Hip Hip); apply Happ.
Qed.

Theorem Bpr_cmpr_raw:forall (G:Γ)(p:Π)(e e':Ε), G⊢e'∈ⅭΔ(e,p)->G⊢Bp_app p e'.
Proof.
 intros G p e e' Hcmp; destruct (fresh p 1) as [n Hnp];
  rewrite (Be_cmpset_inv _ e _ Hnp Hnp) in Hcmp; generalize (Bpr_cmpr _ _ _ _ _ Hcmp);
  irewrite (sym_equal (Bt_esubst_dec (Bp_app p ∂(¡(1,n))) e' ¡(1,n)));
  irewrite (Bp_abstr_app _ _ Hnp Hnp); intros Happ; apply Happ.
Qed.

(*================================================================================================*)