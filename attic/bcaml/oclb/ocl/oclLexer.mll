(*** $Id$ ***)


(** {5 Fichier OcamlLex (g�n�rateur d'analyseur lexical)
@author 
@version 
*)





{
  open OclParser
  exception Eof
}
let ar_op = ['-' '+' '*' '%' '/']
let rel_op = ['=' '<' '>']
let name = ['a'-'z'] ['a'-'z' 'A'-'Z' '0'-'9' '_']*
let number = ['0'-'9']+
let typename = ['A'-'Z'] ['a'-'z' 'A'-'Z' '0'-'9' '_']*
(*
let string_const = ['\'' '"'] ['a'-'z' 'A'-'Z' '0'-'9' '_' ' ']* ['\'' '"']
*)
let string_const = ['\'' '"'] [' '-'�']* ['\'' '"']

rule token = parse
     
  | "->"		{ ARROW }
  | ".."		{ RANGE }
  | '.'			{ DOT }
  | "::"		{ DOUBLECOLON }
  | ':'			{ COLON }
  | '|'			{ BAR }
  | ';'			{ SEMICOLON }
  | ','			{ COMMA }
      
  | '['			{ OHOOK }
  | ']'			{ CHOOK }
  | '('			{ OPARENTHESIS }
  | ')'			{ CPARENTHESIS }
  | '{'			{ OACCOLADE }
  | '}'			{ CACCOLADE }
      
  | "<invariants>"	{ INVARIANTS }
  | "</invariants>"	{ ENDINVARIANTS }
  | "<features>"	{ FEATURES }
  | "</endfeatures>"	{ ENDFEATURES }
  | "<supertype>"	{ SUPERTYPE }
      
  | '-'			{ MINUS }
  | "not"		{ NOT }

  | '+'			{ PLUS }
  | '*'			{ MULTIPLY }
  | '/'			{ DIVIDE }

  | ">="		{ SUPEQUAL }
  | "<="		{ LOWEQUAL }
  | "<>"		{ DIFFERENT }
  | '='			{ EQUAL }
  | '>'			{ SUP }
  | '<'			{ LOW }

  | "Set"		{ SET }
  | "Bag"		{ BAG }
  | "Sequence"		{ SEQUENCE }
  | "Collection"	{ COLLECTION }
      
  | "and"		{ AND }
  | "or"		{ OR }
  | "xor"		{ XOR }
  | "implies"		{ IMPLIES }
  | "true"		{ TRUE }
  | "false"		{ FALSE }

  | '@'			{ TIME }
  | "let"		{ LET }
  | "in"		{ IN }
  | "def"		{ DEF }
  | "pre"		{ PRE }
  | "post"		{ POST }
  | "inv"		{ INV }
  | "package"		{ PACKAGE }
  | "endpackage"	{ ENDPACKAGE }
  | "context"		{ CONTEXT }
  | "allInstances()"	{ ALLINSTANCES }
      
  | "if"		{ IF }
  | "then"		{ THEN }
  | "else"		{ ELSE }
  | "endif"		{ ENDIF }

  | "select"		{ SELECT }
  | "collect"		{ COLLECT }
  | "reject"		{ REJECT }
  | "iterate"		{ ITERATE }
  | "exists"		{ EXISTS }
  | "forAll"		{ FORALL }
  
  | "size()"		{ SIZE }            
  | "intersection"	{ INTERSECTION }
  | "union"		{ UNION }
  | "difference"	{ DIFFERENCE }            
  | "symmetricDifference"	{ SYMMDIFF }
  | "isEmpty()"		{ ISEMPTY }
  | "notEmpty()"	{ ISNOTEMPTY }
  | "including"		{ INCLUDING }
  | "excluding"		{ EXCLUDING }    
  | "includes"		{ INCLUDES } 
  | "includesAll"	{ INCLUDESALL }
  | "count()"		{ COUNT }
  | "first"		{ FIRST}
  | "last"		{ LAST }
  | "at"		{ AT }
  | "append"		{ APPEND }
  | "prepend"		{ PREPEND }

  | "oclType"		{ OCLTYPE }    
  | "oclIsKindOf"	{ OCLISKINDOF }
  | "oclIsTypeOf"	{ OCLISTYPEOF }
  | "oclAsType"   	{ OCLASTYPE }    

  | name		{ NAME (Lexing.lexeme lexbuf) }
  | typename		{ TYPENAME (Lexing.lexeme lexbuf) }
  | number		{ NUMBER (Lexing.lexeme lexbuf) }
  | string_const	{ STRING (Lexing.lexeme lexbuf) }

  | [' ' '\t' '\r' '\n']	{ token lexbuf }
  | eof				{ EOL }
(*
  | eof				{ raise Eof } 
*)
      
