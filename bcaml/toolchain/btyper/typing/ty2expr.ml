open Blast

let rec ty2expr = function
    TypeNatSetRange (e1,e2) -> ExprSet (SetRange (e1,e2)) 
  | PredType (e) -> e
  | RecordsType (fl) ->
      ExprRecords (RecordsWithField (List.map (fun (i,t) -> (i,ty2expr t)) fl))
  | TypeIdentifier(Id(p,n,_,k)) as i -> ExprId(Id(p,n,Some (Fin i),k))
  | PFunType(t1,t2) -> ExprBin(Bbop.PartialFunc,ty2expr t1,ty2expr t2)
  | FunType(t1,t2) -> ExprBin(Bbop.TotalFunc,ty2expr t1,ty2expr t2)
  | ProdType(t1,t2) -> ExprBin(Bbop.SetMul,ty2expr t1,ty2expr t2)
  | Fin(t) -> ExprUn(Bbop.Fin,ty2expr t)
  | Sequence t -> ExprUn(Bbop.Seq,ty2expr t)
  | _ -> failwith "ty2expr:Untyped"




