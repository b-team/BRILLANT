(* $Id$ *)

(* 
      Copyright (C) 2007-2008 INRETS-ESTAS,  Samuel Colin 
*)


(*
   Accessors needed by the Proof Obligation Generator
  @author Samuel Colin

*)

open Blast
open Blast_mk
open Po
open Bbop 
open Project


(* x : POW1 (INT) *)
let mk_PowOne_predtyping paramid=
  mk_PredIn
    (ExprId(paramid))
    (ExprUn(Bbop.PowOne, ExprSet(SetPredefined(INT))))

(*Donne le typage des param�tres de la machine
@param mchhead la machine dont on veut typer les param�tres*)
let typings_of_mchparams mchhead=
  let paramids=Blast_acc.instance_params_of mchhead in
  let setparamids=List.find_all Expr_acc.is_set paramids in
  List.map mk_PowOne_predtyping setparamids


(*Cree le pr�dicat qui dit que l'expression de l'id du set plac� en premier argument est �gal a l'expression du set des id plac� en second argument 
@param id l'ID qui doit �tre distinct de la liste
@param idlist la liste d'ID de reference*)
let setEnumDec_to_pred id idlist=
  let set_id=ExprId(id) in
  let expridlist=List.map (fun i -> ExprId(i)) idlist in
  let set_enum=ExprSet(SetEnum(expridlist)) in
    PredAtom(Bbop.Equal, set_id, set_enum)
    
(* Return predicates that ID are distinct
@param idlist liste of IDs to be check *)
let mkDistinctsIdsPred idlist=
  
  let diff_from_all oneid otherids=
    let exproneid=ExprId(oneid) in
    let exprotherids=List.map (fun i -> ExprId(i)) otherids in
      List.map
	(fun eid -> PredAtom(Bbop.NotEqual, exproneid, eid))
	exprotherids
  in
    
  let rec alldifferents myidlist=
    match myidlist with
      | hid::tid -> 
      	  (diff_from_all hid tid)::(alldifferents tid)
      | [] -> []
  in
    mk_Conjunction (List.flatten (alldifferents idlist))

(*Sert � typer les sets de clause
*)
let typings_of_setclause mchclauses=
  let allsets=Clause_acc.get_Sets mchclauses in
    
  let typing_of_set setdecl=
    match setdecl with
      | Blast.SetAbstDec(i) -> 
	  mk_PowOne_predtyping i
      | Blast.SetEnumDec(setid,idlist) ->
	  (* T = {a,b,c} *)
	  let settyping=mk_PowOne_predtyping setid in
	  let setequality=setEnumDec_to_pred setid idlist in
(*MG On ne genere plus "a<>b b<>c  a<>c"
  let setpred=mkDistinctsIdsPred idlist in
  Pred_acc.mkPredAnd 
  settyping
  (Pred_acc.mkPredAnd setequality setpred)
 *)
(* TODO: SC: C'est tr�s mal... *)
	    PredBin(Bbop.And, settyping, setequality)
	        in
    
    List.map typing_of_set allsets

(*Dit si il y a correspondance machine/composante*)
let correct_machine comp ast=
  match comp with
    | Parameters(id) 
    | Sets(id) 
    | Constraints(id) 
    | Properties(id) 
    | Invariant(id) 
    | Assertions(id) 
      -> id=(Blast_acc.get_name ast)



let get_id_from_request req=
  match req with
    | Parameters(id) 
    | Sets(id) 
    | Constraints(id) 
    | Properties(id) 
    | Invariant(id) 
    | Assertions(id) 
      -> id
	

let name_of clause=
  match clause with
    | Parameters(id) -> "Parameters " ^ (Id_acc.debug_id id)
    | Sets(id) -> "Sets " ^ (Id_acc.debug_id id)
    | Constraints(id) -> "Constraints " ^ (Id_acc.debug_id id)
    | Properties(id) -> "Properties " ^ (Id_acc.debug_id id)
    | Invariant(id) -> "Invariant " ^ (Id_acc.debug_id id)
    | Assertions(id) -> "Assertions " ^ (Id_acc.debug_id id)



let get_component comp ast=
  if not(correct_machine comp ast) then invalid_arg ("get_component:" ^ (name_of comp));
  let mchhead = 
    try Blast_acc.get_head ast 
    with
      | Blast_acc.Empty_tree -> invalid_arg ("get_component:" ^ (name_of comp))
      | err -> (Error.warning "calculate:error"; raise err)
  in
  let mchclauses=
    try Blast_acc.get_clause_list ast 
    with
      | Blast_acc.Empty_tree -> 
	  invalid_arg ("get_component:" ^ (name_of comp))
      | err -> (Error.warning "get_component"; raise err)
  in
(* Good thing all functions raise "Invalid_argument" *)
    try
      match comp with
	| Parameters(_) ->
	    mk_Conjunction (typings_of_mchparams mchhead)
	| Sets(_) -> 
	    mk_Conjunction (typings_of_setclause mchclauses)
	| Constraints(_) ->  
      	    Clause_acc.get_Constraints mchclauses
	| Properties(_) -> 
      	    Clause_acc.get_Properties mchclauses
	| Invariant(_) -> 
	      Clause_acc.get_Invariant mchclauses
	| Assertions(_) -> 
	    mk_Conjunction (Clause_acc.get_Assertions mchclauses)
      with
      | Clause_acc.No_such_clause -> raise Not_found
      | Invalid_argument _ -> raise Not_found
      | Blast_mk.Empty_list -> raise Not_found
      | err -> (Error.warning ("(po_acc.get_component):" ^ (name_of comp)); raise err) 


(* Hypothesis : the root machine of the project is the machine for the PO of
   which the component is requested *)

 (*Donne le composant correspondantdu projet*)

let get_component_from_graph comp project mchid=
  let compid=get_id_from_request comp in
  let machine=(snd (Project.find project mchid)).machine in
    if compid=mchid
    then try 
      get_component comp machine
    with Not_found -> 
      invalid_arg ("get_component_from_graph : Component not found : " ^ (name_of comp) )
    else 
      (* Let's search in the machines it depends on *)
      let depMachines=Bgraph.dependedMachines project.graph mchid in
      let foundMachines=List.filter (fun i -> i = compid) depMachines in
	(* if more than one machine appears, we've got a problem *)
	match foundMachines with
	  | [] -> invalid_arg "get_component_from_graph : machine not found"
	  | mch1::(mch2::_) -> invalid_arg "get_component_from_graph : a machine appears multiple times in the clauses "
	  | [alone] ->
	      try
		let foundid=alone in
		let foundmch=(snd (Project.find project foundid)).machine in	      
		let realcomp=get_component comp foundmch in	      
		let params=Blast_acc.instance_params_of (Blast_acc.get_head foundmch) in
		  if params=[] then realcomp
		  else 
(* SC : faut-il ajouter les machines �tendues? *)
		    let cllist=Blast_acc.get_clause_list machine in
		    let allinsts=
		      List.append
			(Clause_acc.get_Includes cllist)
			(Clause_acc.get_Imports cllist)
		    in
		    let instanciations=
		      try List.assoc compid allinsts 
		      with Not_found -> []
		    in
(* 
   A machine can't include and use another machine at the same time, so 
   if no instanciation is to be done, that means the machine is used, 
  so we don't have to instanciate in the proof obligation 
*)
		      if instanciations=[] then realcomp

(* Instanciation concerns only the first four following clauses *)
		      else match comp with
			| Parameters(_) 
			| Constraints(_) 
			| Invariant(_) 
			| Assertions(_) -> 
			    SubstApply(
			      SubstSetEqualIds(params, instanciations),
			      realcomp)
			| Sets(_)
			| Properties(_) -> realcomp
		      with 
			  Not_found -> 
			    invalid_arg ("get_component_from_graph : Something not found : " ^ (name_of comp) )
			| err -> 
			    (Error.warning "get_component_from_graph : error"; raise err)
	      


 (*fait la liste des composants des hypotheses*)
let mkPoPredicate hypos project mchid=
  let rec mkpopred hypreqs proj id=
    match hypreqs with
      | h::t -> 
	  begin
(* it may happen that the requested component is not present *)
	    try
	      let comp=(get_component_from_graph h proj id) in
		comp::(mkpopred t proj id)
	    with Invalid_argument e ->
	      Error.debug e;
	      (mkpopred t proj id)
	  end
      | [] -> []
  in 
  let predhypos=mkpopred hypos project mchid in
    match predhypos with
      | [] -> None
      | predlist -> Some(mk_Conjunction predlist)
    
(* Alias de la fonction pr�c�dente, cr�e pour clarifier les fonctions*)
let mkHypothesis hypos project mchid=
  mkPoPredicate hypos project mchid

(* Concat�ne les hypoth�ses en argument.*)
let concat_po_hypos comp1 comp2=
  match (comp1,comp2) with
    | (None, None) 
      -> None
    | (None, Some(_) ) 
      -> comp2
    | (Some(_), None) 
      -> comp1
    | (Some(p1), Some(p2) ) 
      -> Some (mk_Conjunction [p1;p2] )



