(*==================================================================================================
  Project : BiCoq3
  Module : wf_proof.v
  This module defines the depth of a B proof as a well-founded measure and derives an associated
  induction principle on proofs (with dependent type).
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Jul 2008, Md: Jul 2008
  ================================================================================================*)

Require Export proof.

(* Depth of a proof ------------------------------------------------------------------------------*)

Fixpoint Bpr_depth(G:Γ)(p:Π)(H:G⊢p){struct H}:nat :=
 match H with
 | Bpr_memb _ _ _ => 0
 | Bpr_weak _ _ _ H' _ => S(Bpr_depth _ _ H')
 | Bpr_andi _ _ _ H1 H2 => S(max (Bpr_depth _ _ H1) (Bpr_depth _ _ H2))
 | Bpr_andl _ _ _ H' => S(Bpr_depth _ _ H')
 | Bpr_andr _ _ _ H' => S(Bpr_depth _ _ H')
 | Bpr_impi _ _ _ H' => S(Bpr_depth _ _ H')
 | Bpr_impe _ _ _ H' => S(Bpr_depth _ _ H')
 | Bpr_absn _ _ _ H1 H2 => S(max (Bpr_depth _ _ H1) (Bpr_depth _ _ H2))
 | Bpr_absp _ _ _ H1 H2 => S(max (Bpr_depth _ _ H1) (Bpr_depth _ _ H2))
 | Bpr_refl _ _ => 0
 | Bpr_fori _ _ _ H' _ => S(Bpr_depth _ _ H')
 | Bpr_fore _ _ _ _ H' => S(Bpr_depth _ _ H')
 | Bpr_cmpi _ _ _ _ _ H1 H2 => S(max (Bpr_depth _ _ H1) (Bpr_depth _ _ H2))
 | Bpr_cmpl _ _ _ _ _ H' => S(Bpr_depth _ _ H')
 | Bpr_cmpr _ _ _ _ _ H' => S(Bpr_depth _ _ H')
 | Bpr_leib _ _ _ _ _ H1 H2 => S(max (Bpr_depth _ _ H1) (Bpr_depth _ _ H2))
 | Bpr_chos _ _ _ _ H' => S(Bpr_depth _ _ H')
 | Bpr_powi _ _ _ _ _ _ H' => S(Bpr_depth _ _ H')
 | Bpr_powe _ _ _ _ _ _ H' => S(Bpr_depth _ _ H')
 | Bpr_extn _ _ _ H1 H2 => S(max (Bpr_depth _ _ H1) (Bpr_depth _ _ H2))
 | Bpr_bige _ _ => 0
 | Bpr_bigd _ _ _ _ => 0
 | Bpr_proi _ _ _ _ _ _ _ _ _ H' => S(Bpr_depth _ _ H')
 | Bpr_proe _ _ _ _ _ _ _ _ _ H' => S(Bpr_depth _ _ H')
 | Bpr_cpll _ _ _ _ _ H' => S(Bpr_depth _ _ H')
 | Bpr_cplr _ _ _ _ _ H' => S(Bpr_depth _ _ H')
 end.
Implicit Arguments Bpr_depth [G p].

(* Induction principle ---------------------------------------------------------------------------*)
(* Induction is done on depth; note that this is one of these cases in which the full power of
   wf_measure_dep is used, as proofs are dependent types - that is, one has to prove results about
   G⊢p using hypotheses of different types G'⊢p'. *)

Theorem wf_proof:forall (P:forall (G:Γ)(p:Π), G⊢p->Type),
                 (forall (G:Γ)(p:Π)(H:G⊢p),
                  (forall (G':Γ)(p':Π)(H':G'⊢p'), Bpr_depth H'<Bpr_depth H->P _ _ H')->P _ _ H)->
                 forall (G:Γ)(p:Π)(H:G⊢p), P _ _ H.
Proof.
 intros P Hind G p H.
 set (Mdep:=fun (s:Bseq)=>Bpr_depth (G:=fst s) (p:=snd s)).
 set (Pdep:=fun (s:Bseq)=>P (fst s) (snd s)).
 generalize (wf_measure_dep Mdep Pdep); intros Hdep.
 replace (P G p H) with (Pdep (G⊦p) H); [ | apply refl_equal].
  apply Hdep.
  intros S HS Hind'.
  unfold Pdep; apply Hind.
  intros G' p' H'.
  apply (Hind' (G'⊦p') H').
Qed.

(*================================================================================================*)