(* Copyright (C) 1998 Ian Zimmerman <itz@rahul.net> *)
(* $Id: spawn_imp.ml,v 1.1 2001/04/27 11:00:15 lefessan Exp $ *)

let bufsize = ref 8092

let trace = ref false

type spawn_kind =
    File of bool                   (*Close desc with connection?*)
  | Process of int                 (*The process id*)

type spawn_table_item =
    spawn_kind * Unix.file_descr * Buffer_imp.t

type id = Unix.file_descr

let spawn_table = Hashtbl.create 7

exception Wrong_kind of string

let to_buffer id =
  let (_,_, buffer) = Hashtbl.find spawn_table id in buffer

let to_descr id =
  let (_, d, _) = Hashtbl.find spawn_table id in d

let to_pid id =
  match Hashtbl.find spawn_table id with
    (Process(pid), _,_) -> pid
  | _ -> raise(Wrong_kind("Process"))

let spawn_file desc leave_open =
  Hashtbl.add spawn_table desc
    (File(leave_open), desc, Buffer_imp.create !bufsize);
  desc

let spawn_process prog args modes =
  match Pty_imp.fork modes with
    (0, _,_) ->                    (*child*)
      Unix.execvp prog args;
      exit 127
  | (pid, fd, _) ->                (*parent*)
      Hashtbl.add spawn_table fd
        (Process(pid), fd, Buffer_imp.create !bufsize);
      fd        

let close id =                     (*close a connection*)
  (match Hashtbl.find spawn_table id with
    (File(false), desc, _) -> Unix.close desc
  | (Process(_), desc, _) -> Unix.close desc
  | _ -> ());
  Hashtbl.remove spawn_table id

let send id s =
  if !trace then prerr_endline ("Sending: " ^ s);
  let pos = ref 0
  and (_, desc, _) = (Hashtbl.find spawn_table id) 
  and len = String.length s in
  while !pos < len do
    pos := !pos + (Unix.write desc s !pos (len - !pos))
  done

let matched_string id =
  Buffer_imp.matched_string (to_buffer id)

let group_beginning id n =
  Buffer_imp.group_beginning (to_buffer id) n

let group_end id n =
  Buffer_imp.group_end (to_buffer id) n
