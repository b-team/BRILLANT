%{
open Blast
open Blast_mk
open Bbop
open Blexemes

exception End_expected of string

let parse_error msg  =
  raise (Blast.Parse_error (Parsing.symbol_start (),msg))

let expr2id = function
    ExprId id -> id
  | ExprFunCall (ExprId id,_) -> id
  | _ -> parse_error ("identifier expected")


let tolerate_parentheses e =
  match e with
    | [ExprParen(ExprNuplet(x))] -> List.map expr2id x
    | _ -> List.map expr2id e


let append_to_Nuple es e =
  match es with
    | ExprNuplet(l) -> ExprNuplet(l@[e])
    | _ -> invalid_arg "append_to_Nuple"

let prepend_to_Nuple es e =
  match es with
    | ExprNuplet(l) -> ExprNuplet(e::l)
    | _ -> invalid_arg "append_to_Nuple"

let single_out_Nuple es =
  match es with
    | ExprNuplet([x]) -> x
    | ExprNuplet([]) -> invalid_arg "single_out_Nuple: empty n-uple"
    | _ -> es

let separate_Nuple es =
  match es with
    | ExprNuplet(x) -> x
    | _ -> invalid_arg "separate_Nuple: not a n-uple"

let unclosed expected = parse_error  (expected^" expected")
%}

/*i*/
%token EOF

/* The two following tokens are never used in the grammar but are useful otherwise */
%token <Blexemes.lex_info>  B_formatting
%token <Blexemes.lex_info>  B_comment

%token <Blexemes.lex_info>  B_NUMBER
%token <Blexemes.lex_info>  B_STRING
%token <Blexemes.lex_info>  B_IDENTIFIER
%token <Blexemes.lex_info>  B_MACHINE
%token <Blexemes.lex_info>  B_CONSTRAINTS
%token <Blexemes.lex_info>  B_SEES
%token <Blexemes.lex_info>  B_SETS
%token <Blexemes.lex_info>  B_CONSTANTS
%token <Blexemes.lex_info>  B_CONCRETE_CONSTANTS
%token <Blexemes.lex_info>  B_ABSTRACT_CONSTANTS
%token <Blexemes.lex_info>  B_VISIBLE_CONSTANTS
%token <Blexemes.lex_info>  B_HIDDEN_CONSTANTS
%token <Blexemes.lex_info>  B_PROPERTIES
%token <Blexemes.lex_info>  B_INCLUDES
%token <Blexemes.lex_info>  B_PROMOTES
%token <Blexemes.lex_info>  B_EXTENDS
%token <Blexemes.lex_info>  B_USES
%token <Blexemes.lex_info>  B_VARIABLES
%token <Blexemes.lex_info>  B_CONCRETE_VARIABLES
%token <Blexemes.lex_info>  B_ABSTRACT_VARIABLES
%token <Blexemes.lex_info>  B_VISIBLE_VARIABLES
%token <Blexemes.lex_info>  B_HIDDEN_VARIABLES
%token <Blexemes.lex_info>  B_INVARIANT
%token <Blexemes.lex_info>  B_INITIALISATION
%token <Blexemes.lex_info>  B_LOCAL_OPERATIONS
%token <Blexemes.lex_info>  B_OPERATIONS
%token <Blexemes.lex_info>  B_REFINEMENT
%token <Blexemes.lex_info>  B_REFINES
%token <Blexemes.lex_info>  B_IMPLEMENTATION
%token <Blexemes.lex_info>  B_IMPORTS
%token <Blexemes.lex_info>  B_DEFINITIONS
%token <Blexemes.lex_info>  B_VALUES
%token <Blexemes.lex_info>  B_ASSERTIONS
%token <Blexemes.lex_info>  B_or
%token <Blexemes.lex_info>  B_setminus
%token <Blexemes.lex_info>  B_not
%token <Blexemes.lex_info>  B_SKIP
%token <Blexemes.lex_info>  B_BEGIN
%token <Blexemes.lex_info>  B_PRE
%token <Blexemes.lex_info>  B_IF
%token <Blexemes.lex_info>  B_THEN
%token <Blexemes.lex_info>  B_ELSIF
%token <Blexemes.lex_info>  B_ELSE
%token <Blexemes.lex_info>  B_ASSERT
%token <Blexemes.lex_info>  B_ANY
%token <Blexemes.lex_info>  B_WHERE
%token <Blexemes.lex_info>  B_LET
%token <Blexemes.lex_info>  B_BE
%token <Blexemes.lex_info>  B_IN
%token <Blexemes.lex_info>  B_CHOICE
%token <Blexemes.lex_info>  B_OR
%token <Blexemes.lex_info>  B_SELECT
%token <Blexemes.lex_info>  B_WHEN
%token <Blexemes.lex_info>  B_CASE
%token <Blexemes.lex_info>  B_OF
%token <Blexemes.lex_info>  B_EITHER
%token <Blexemes.lex_info>  B_VAR
%token <Blexemes.lex_info>  B_WHILE
%token <Blexemes.lex_info>  B_DO
%token <Blexemes.lex_info>  B_VARIANT
%token <Blexemes.lex_info>  B_NAT
%token <Blexemes.lex_info>  B_NATONE
%token <Blexemes.lex_info>  B_NATURAL
%token <Blexemes.lex_info>  B_NATURALONE
%token <Blexemes.lex_info>  B_INT
%token <Blexemes.lex_info>  B_INTEGER
%token <Blexemes.lex_info>  B_false
%token <Blexemes.lex_info>  B_true
%token <Blexemes.lex_info>  B_POW
%token <Blexemes.lex_info>  B_FIN
%token <Blexemes.lex_info>  B_POWONE
%token <Blexemes.lex_info>  B_FINONE
%token <Blexemes.lex_info>  B_union
%token <Blexemes.lex_info>  B_inter
%token <Blexemes.lex_info>  B_UNION
%token <Blexemes.lex_info>  B_INTER
%token <Blexemes.lex_info>  B_id
%token <Blexemes.lex_info>  B_dom
%token <Blexemes.lex_info>  B_ran
%token <Blexemes.lex_info>  B_iterate
%token <Blexemes.lex_info>  B_closure
%token <Blexemes.lex_info>  B_closureone
%token <Blexemes.lex_info>  B_card
%token <Blexemes.lex_info>  B_max
%token <Blexemes.lex_info>  B_min
%token <Blexemes.lex_info>  B_mod
%token <Blexemes.lex_info>  B_SIGMA
%token <Blexemes.lex_info>  B_PI
%token <Blexemes.lex_info>  B_seq
%token <Blexemes.lex_info>  B_iseq
%token <Blexemes.lex_info>  B_seqone
%token <Blexemes.lex_info>  B_iseqone
%token <Blexemes.lex_info>  B_perm
%token <Blexemes.lex_info>  B_conc
%token <Blexemes.lex_info>  B_front
%token <Blexemes.lex_info>  B_tail
%token <Blexemes.lex_info>  B_first
%token <Blexemes.lex_info>  B_last
%token <Blexemes.lex_info>  B_size
%token <Blexemes.lex_info>  B_rev
%token <Blexemes.lex_info>  B_bool
%token <Blexemes.lex_info>  B_END
%token <Blexemes.lex_info>  B_minus
%token <Blexemes.lex_info>  B_tilde
%token <Blexemes.lex_info>  B_before
%token <Blexemes.lex_info>  B_plus
%token <Blexemes.lex_info>  B_power
%token <Blexemes.lex_info>  B_concatseq
%token <Blexemes.lex_info>  B_mul
%token <Blexemes.lex_info>  B_div
%token <Blexemes.lex_info>  B_equal
%token <Blexemes.lex_info>  B_conjunction
%token <Blexemes.lex_info>  B_less
%token <Blexemes.lex_info>  B_greater
%token <Blexemes.lex_info>  B_comma
%token <Blexemes.lex_info>  B_semicolon
%token <Blexemes.lex_info>  B_in
%token <Blexemes.lex_info>  B_setin
%token <Blexemes.lex_info>  B_equivalence
%token <Blexemes.lex_info>  B_subset
%token <Blexemes.lex_info>  B_notsubset
%token <Blexemes.lex_info>  B_implies
%token <Blexemes.lex_info>  B_strictsubset
%token <Blexemes.lex_info>  B_notstrictsubset
%token <Blexemes.lex_info>  B_notin
%token <Blexemes.lex_info>  B_notequal
%token <Blexemes.lex_info>  B_lessorequal
%token <Blexemes.lex_info>  B_greaterorequal
%token <Blexemes.lex_info>  B_relation
%token <Blexemes.lex_info>  B_rel
%token <Blexemes.lex_info>  B_fnc
%token <Blexemes.lex_info>  B_cartesianprod
%token <Blexemes.lex_info>  B_totalfunc
%token <Blexemes.lex_info>  B_partialfunc
%token <Blexemes.lex_info>  B_totalsurject
%token <Blexemes.lex_info>  B_partialsurject
%token <Blexemes.lex_info>  B_totalinject
%token <Blexemes.lex_info>  B_partialinject
%token <Blexemes.lex_info>  B_partialbij
%token <Blexemes.lex_info>  B_bijection
%token <Blexemes.lex_info>  B_operationsig
%token <Blexemes.lex_info>  B_simplesubst
%token <Blexemes.lex_info>  B_CURLYOPEN
%token <Blexemes.lex_info>  B_CURLYCLOSE
%token <Blexemes.lex_info>  B_PARENOPEN
%token <Blexemes.lex_info>  B_PARENCLOSE
%token <Blexemes.lex_info>  B_BRACKOPEN
%token <Blexemes.lex_info>  B_BRACKCLOSE
%token <Blexemes.lex_info>  B_parallelcomp
%token <Blexemes.lex_info>  B_mapletorpair
%token <Blexemes.lex_info>  B_domainrestrict
%token <Blexemes.lex_info>  B_rangerestrict
%token <Blexemes.lex_info>  B_domainsubstract
%token <Blexemes.lex_info>  B_rangesubstract
%token <Blexemes.lex_info>  B_override
%token <Blexemes.lex_info>  B_directprod
%token <Blexemes.lex_info>  B_prependseq
%token <Blexemes.lex_info>  B_appendseq
%token <Blexemes.lex_info>  B_prefixseq
%token <Blexemes.lex_info>  B_suffixseq
%token <Blexemes.lex_info>  B_natsetrange
%token <Blexemes.lex_info>  B_unionsets
%token <Blexemes.lex_info>  B_intersectionsets
%token <Blexemes.lex_info>  B_lambda
%token <Blexemes.lex_info>  B_emptyset
%token <Blexemes.lex_info>  B_seqempty
%token <Blexemes.lex_info>  B_point
%token <Blexemes.lex_info>  B_such
%token <Blexemes.lex_info>  B_forall
%token <Blexemes.lex_info>  B_exists
%token <Blexemes.lex_info>  B_double_equal
%token <Blexemes.lex_info>  B_MAXINT
%token <Blexemes.lex_info>  B_MININT
%token <Blexemes.lex_info>  B_prjone
%token <Blexemes.lex_info>  B_prjtwo
%token <Blexemes.lex_info>  B_succ
%token <Blexemes.lex_info>  B_pred

%token <Blexemes.lex_info>  B_struct
%token <Blexemes.lex_info>  B_rec
%token <Blexemes.lex_info>  B_quote

%token <Blexemes.lex_info>  B_tree
%token <Blexemes.lex_info>  B_btree
%token <Blexemes.lex_info>  B_consconst
%token <Blexemes.lex_info>  B_top
%token <Blexemes.lex_info>  B_sons
%token <Blexemes.lex_info>  B_prefix
%token <Blexemes.lex_info>  B_postfix
%token <Blexemes.lex_info>  B_sizet
%token <Blexemes.lex_info>  B_mirror
%token <Blexemes.lex_info>  B_rank
%token <Blexemes.lex_info>  B_father
%token <Blexemes.lex_info>  B_son
%token <Blexemes.lex_info>  B_subtree
%token <Blexemes.lex_info>  B_arity
%token <Blexemes.lex_info>  B_bin
%token <Blexemes.lex_info>  B_left
%token <Blexemes.lex_info>  B_right
%token <Blexemes.lex_info>  B_infix

%token <Blexemes.lex_info>  B_BOOL
%token <Blexemes.lex_info>  B_STRINGS

/*i*/

%start amn
%type <Blast.amn> amn
%type <Blast.substitution> Substitution
%type <Blast.expr> AmbiguousExpr
%type <Blast.expr> UnambiguousExpr
%type <Blast.predicate> Predicate

/* Priorities */

%left B_PARENOPEN B_PARENCLOSE

/* 0 */
/* ::, :=, <-- : these are given left-associativity but no priority in the BRM */
%left B_setin B_simplesubst B_operationsig

/* 10 */
/* | */
%left B_such

/* 20 */
/* ;, || */
%left B_semicolon B_parallelcomp

/* 30 */
/* => */
%left B_implies

/* 40 */
/* &, or */
%left B_conjunction B_or

/* 60 */
/* :, <=>, = : we left out the ":" as a record field */
%left B_equivalence B_equal

/* 110 */
/* /<:, /<<:, <:, <<: */
%left B_notsubset B_notstrictsubset B_subset B_strictsubset

/* 115 */
/* , */
%left B_comma

/* 120 */
/* : */
%left B_in

/* 125 */
/* +->, +->>, -->, -->>, <->, >+>, >->, >->>, >+>> : we added partial bijection */
%left B_partialfunc B_partialsurject B_totalfunc B_totalsurject B_relation B_partialinject B_totalinject B_bijection B_partialbij

/* 160 */
/* ->, /:, /=, /\, /|\, <, <+, <-, <<|, <=, <|, >, ><, >=, \/, \|/, ^, |->, |>, |>> */
%left B_prependseq B_notin B_notequal B_intersectionsets B_prefixseq B_less B_override B_appendseq B_domainsubstract B_lessorequal B_domainrestrict B_greater B_directprod B_greaterorequal B_unionsets B_suffixseq B_concatseq B_mapletorpair B_rangerestrict B_rangesubstract

/* 170 */
/* .. */
%left B_natsetrange

/* 180 */
/* +, - */
/* The \ notation for set difference is given the same priority as the difference */
%left B_setminus B_plus B_minus

/* 190 */
/* *, /, mod */
/* The >*< notation for cartesian product is given the same priority as the multiplication */
%left B_cartesianprod B_mul B_div B_mod

/* 200 */
/* ** */
%right B_power

/* 210 */
/* -: unary minus, this priority shall only be used in the rules below */
%nonassoc B_uminus

/* 220 */
/* . */
%right B_point

/* 230 */
/* ~ */
%left B_tilde

/* 240 */
/* $0 */
%left B_before

/* 250 */
/* !, #, %, ' */
%nonassoc B_forall B_exists B_lambda B_quote


%%

amn: Entity EOF {$1}
;

Entity:
  Machine        {$1}
| Refinement     {$1}
| Implementation {$1}
;

Machine:
  B_MACHINE MchHead MchClauseList B_END {Machine($2,$3)}
| B_MACHINE MchHead B_END               {Machine($2,[])}
;

Refinement:
  B_REFINEMENT MchHead  Refines RefClauseList B_END {Refinement($2,$3,$4)}
| B_REFINEMENT MchHead  Refines B_END               {Refinement($2,$3,[])}
;

Implementation:
  B_IMPLEMENTATION MchHead Refines ImpClauseList B_END {Implementation($2,$3,$4)}
| B_IMPLEMENTATION MchHead Refines B_END               {Implementation($2,$3,[])}
;

Refines: B_REFINES Refined {$2}
;

Refined: MchName {$1}
;

MchHead: MachineName {$1}
;

MachineName:
  MchName B_PARENOPEN MchParams  B_PARENCLOSE {($1, $3)}
| MchName                                     {($1, [])}
;

MchName: Identifier {$1}
;

MchParams: VariableList {$1}
;

MchClauseList:
  MchClauseList MchClause {$1@[$2]}
| MchClause               {[$1]}
;

RefClauseList:
  RefClauseList RefClause {$1@[$2]}
| RefClause               {[$1]}
;

ImpClauseList:
  ImpClauseList ImpClause {$1@[$2]}
| ImpClause               {[$1]}
;

/* \parsersec{Machine} */

MchClause:
| Constraints       {$1}
| Sees              {$1}
| Includes          {$1}
| Promotes          {$1}
| Extends           {$1}
| Uses              {$1}
| Sets              {$1}
| Properties        {$1}
| Invariant         {$1}
| Assertions        {$1}
| Initialisation    {$1}
| Operations        {$1}
| Constants         {$1}
| ConstantsVisible  {$1}
| ConstantsHidden   {$1}
| ConstantsConcrete {$1}
| ConstantsAbstract {$1}
| Variables         {$1}
| VariablesConcrete {$1}
| VariablesHidden   {$1}
| VariablesAbstract {$1}
| VariablesVisible  {$1}
;

/* \parsersec{Refinement} */

RefClause:
| Sees              {$1}
| Includes          {$1}
| Promotes          {$1}
| Extends           {$1}
| Sets              {$1}
| Properties        {$1}
| Invariant         {$1}
| Assertions        {$1}
| Initialisation    {$1}
| Operations        {$1}
| Constants         {$1}
| ConstantsVisible  {$1}
| ConstantsHidden   {$1}
| ConstantsConcrete {$1}
| ConstantsAbstract {$1}
| Variables         {$1}
| VariablesHidden   {$1}
| VariablesConcrete {$1}
| VariablesAbstract {$1}
| VariablesVisible  {$1}
;

/* \parsersec{Implementation} */

ImpClause:
| Sees              {$1}
| Imports           {$1}
| Promotes          {$1}
| Extends           {$1}
| Sets              {$1}
| Properties        {$1}
| Values            {$1}
| Invariant         {$1}
| Assertions        {$1}
| Initialisation    {$1}
| LocalOperations   {$1}
| Operations        {$1}
| Variables         {$1}
| VariablesVisible  {$1}
| VariablesConcrete {$1}
| Constants         {$1}
| ConstantsVisible  {$1}
| ConstantsConcrete {$1}
;


/*
\parsersec{Clauses}
*/

/*
\parsersubsec{CONSTRAINTS}
*/
Constraints: B_CONSTRAINTS Predicate {Constraints $2}
;

/*
\parsersubsec{INCLUDES}
*/

Includes: B_INCLUDES ImportedList {Includes($2)}
;

/*
\parsersubsec{EXTENDS}
*/

Extends: B_EXTENDS ImportedList {Extends($2)}
;

/*
\parsersubsec{IMPORTS}
*/

Imports: B_IMPORTS ImportedList {Imports($2)}
;

ImportedList:
  ImportedList B_comma Imported {$1@[$3]}
| Imported                      {[$1]}
;

Imported: ImplementationName {$1}
;

ImplementationName:
  ImpName ImpParams {($1,$2)}
| ImpName           {($1,[])}
;

ImpName:
  PrefixMachineName                {$1}
| B_PARENOPEN ImpName B_PARENCLOSE {$2}
;

ImpParams: ExprListParen {$1}
;


PrefixMachineNameList:
  PrefixMachineNameList B_comma PrefixMachineName {$1@[$3]}
| PrefixMachineName                               {[$1]}
| B_PARENOPEN PrefixMachineName B_PARENCLOSE      {[$2]}
;

PrefixMachineName: Identifier {$1}
;

/*
\parsersubsec{USES}
*/

Uses: B_USES  PrefixMachineNameList {Uses($2)}
;

/*
\parsersubsec{PROMOTES}
*/

Promotes: B_PROMOTES PromotedList {Promotes($2)}
;

PromotedList:
  PromotedList B_comma Promoted {$1@[$3]}
| Promoted                      {[$1]}
;

Promoted:
  Identifier {$1}
| B_PARENOPEN Identifier B_PARENCLOSE {$2}
;

/*
\parsersubsec{SEES}
*/

Sees: B_SEES SeenList {Sees($2)}
;

SeenList:
  SeenList B_comma Seen {$1@[$3]}
| Seen                  {[$1]}
;

Seen:
  PrefixMachineName             {$1}
| B_PARENOPEN Seen B_PARENCLOSE {$2}
;

/*
\parsersubsec{SETS}
*/

Sets: B_SETS SetList {Sets($2)}
;

/*
\parsersubsec{CONSTANTS}
*/

Constants: B_CONSTANTS ConstantList {ConstantsConcrete($2)}
;

ConstantsVisible: B_VISIBLE_CONSTANTS ConstantList {ConstantsConcrete($2)}
;

ConstantsHidden: B_HIDDEN_CONSTANTS ConstantList {ConstantsHidden($2)}
;

ConstantsAbstract: B_ABSTRACT_CONSTANTS VariableList {ConstantsAbstract($2)}
;

ConstantsConcrete: B_CONCRETE_CONSTANTS VariableList {ConstantsConcrete($2)}
;

/*
\parsersubsec{PROPERTIES}
*/

Properties: B_PROPERTIES Predicate {Properties($2)}
;

/*
\parsersubsec{VARIABLES}
*/

Variables: B_VARIABLES VariableList {VariablesAbstract($2)}
;

VariablesHidden: B_HIDDEN_VARIABLES VariableList {VariablesHidden($2)}
;

VariablesVisible: B_VISIBLE_VARIABLES VariableList {VariablesConcrete($2)}
;

VariablesConcrete: B_CONCRETE_VARIABLES VariableList {VariablesConcrete($2)}
;

VariablesAbstract: B_ABSTRACT_VARIABLES VariableList {VariablesAbstract($2)}
;

/*
\parsersubsec{INVARIANT}
*/

Invariant: B_INVARIANT InvariantPredicate {Invariant($2)}
;

InvariantPredicate: Predicate {$1}
;

/*
\parsersubsec{ASSERTIONS}
*/

Assertions: B_ASSERTIONS AssertionList {Assertions($2)}
;

AssertionList:
  AssertionList B_semicolon Predicate {$1@[$3]}
| Predicate                           {[$1]}
;

/*
\parsersubsec{INITIALISATION}
*/

Initialisation: B_INITIALISATION Substitution {Initialisation($2)}
;

/*
\parsersubsec{OPERATIONS (normal and local)}
*/

LocalOperations: B_LOCAL_OPERATIONS OperationList {LocalOperations($2)}
;

Operations: B_OPERATIONS OperationList {Operations($2)}
;

/* TODO: operations' list can be empty */
OperationList:
  OperationList B_semicolon Operation {$1@[$3]}
| Operation                           {[$1]}
;

Values: B_VALUES ValuationList {Values($2)}
;

ValuationList:
  ValuationList B_semicolon Valuation {$1@[$3]}
| Valuation                           {[$1]}
;

Valuation: Identifier B_equal UnambiguousExpr {($1,$3)}
;

SetList:
  SetList SetSep Set {$1@[$3]}
| Set                {[$1]}
;

/* TODO: there should be only one set separator normally */
SetSep:
| B_semicolon {$1}
| B_comma     {$1}
;

Set:
  SetName B_equal B_CURLYOPEN ElementList B_CURLYCLOSE {SetEnumDec($1,$4)}
| SetName                                              {SetAbstDec($1)}
;

SetName: Identifier {$1}
;

ElementList: IdentifierList {$1}
;

ConstantList:
  ConstantList B_comma Constant {$1@[$3]}
| Constant                      {[$1]}
;

VariableList:
  VariableList B_comma Variable {$1@[$3]}
| Variable                      {[$1]}
;

Variable: Identifier {$1}
;

Constant: Identifier {$1}
;

Identifier: B_IDENTIFIER {Blast_mk.mk_Id $1.lex_value}
;

IdentifierList:
  IdentifierList B_comma Identifier {$1@[$3]}
| Identifier                        {[$1]}
;

Operation:
  OperSig B_equal OperBody
    {let (name,params_out,params_in) = $1 in
       (name,params_out,params_in,$3)}
| error
    { parse_error "Expected an operation here"}
;

OperBody:
    SubstLevel1 {$1}
;

OperSig:
  VariableList B_operationsig OperHead
    {let (name,paramsOut) = $3 in (name,$1,paramsOut)}
| OperHead
    {let (name,paramsOut) = $1 in (name,[],paramsOut)}
;

OperHead:
  Identifier                                    {($1, [])}
| Identifier B_PARENOPEN VariableList B_PARENCLOSE {($1, $3)}
;

/*
\parsersec{Substitutions}
*/

/* Regrouping shapes beginning by VarList */
AffectMultiple:
  IdentifierList B_in PredParen
    {SubstBecomeSuch($1,$3)}

/* a,b,c := e1,e2,e3 */
| IdentifierList  B_simplesubst ExprList
    {SubstSetEqualIds($1,$3)}

/* f(x)(y) := e */
| FakeFunCall B_simplesubst CommaExpr
    {SubstSetEqualFun($1,$3)}

| FieldSet B_simplesubst CommaExpr
    {SubstSetEqualRecords($1, $3)}
;

FakeFunCall:
  Identifier B_PARENOPEN AmbiguousExpr B_PARENCLOSE  { SetFunExpr(SetFunVar($1),$3) }
| FakeFunCall B_PARENOPEN AmbiguousExpr B_PARENCLOSE { SetFunExpr($1,$3) }
;

FieldSet:
  Identifier B_quote Identifier {SetRecField(SetRecVar($1), $3)}
| FieldSet B_quote Identifier   {SetRecField($1, $3)}
;


/*
  Normally the rule with parameters should be "Identifier ExprListParen".
  Unfortunately this causes a reduce/reduce with the FakeFunCall production.
  Hence we do as if we were expecting a single expression : if it is a
  tuple, then we separate it into several terms which are expected to be
  the parameters of the operation call.
 */
OperCall:
  Identifier               {($1,[])}
| B_PARENOPEN Identifier B_PARENCLOSE {($2,[])}
| Identifier B_PARENOPEN AmbiguousExpr B_PARENCLOSE
    {let params = try separate_Nuple $3 with Invalid_argument(_) -> [$3] in
     ($1, params)}
| B_PARENOPEN Identifier B_PARENCLOSE B_PARENOPEN AmbiguousExpr B_PARENCLOSE
    {let params = try separate_Nuple $5 with Invalid_argument(_) -> [$5] in
     ($2, params)}
;

SubstOperCall:
  IdentifierList B_operationsig OperCall
    {let (f,l) = $3 in SubstOperCall($1,f,l)}
| OperCall
    {let (f,l) = $1 in SubstOperCall([],f,l)}
;

Substitution:
/* Operation body */
  SubstLevel1   {$1}
/* TODO: Can't be operation bodies (reduce/reduce ?) */
| SubstSequence {$1}
| SubstParallel {$1}
;

SubstLevel1:
  SubstDelimited {$1}
| SubstSetIn     {$1}
| AffectMultiple {$1}
| SubstOperCall  {$1}
;

SubstDelimited:
  SubstBlock       {$1}
| SubstPre         {$1}
| SubstIf          {$1}
| SubstChoice      {$1}
| SubstSelect      {$1}
| SubstCase        {$1}
| SubstVar         {$1}
| SubstAny         {$1}
| SubstLet         {$1}
| SubstWhile       {$1}
| SubstAssert      {$1}
| SubstSkip        {$1}
;

/* \parsersubsec{Sequence} */

SubstSequence: Substitution B_semicolon Substitution {SubstSequence($1,$3)}
;

/* \parsersubsec{SetIn} */

SubstSetIn: IdentifierList B_setin UnambiguousExpr {SubstSetIn($1,$3)}
;

/* \parsersubsec{Block} */

SubstBlock: B_BEGIN Substitution B_END {SubstBlock($2)}
;

/* \parsersubsec{Skip} */

SubstSkip: B_SKIP {SubstSkip}
;

/* \parsersubsec{Pre} */

SubstPre: B_PRE Predicate B_THEN Substitution B_END
  {SubstPrecondition($2,$4)}
;

/* \parsersubsec{Assert} */

SubstAssert: B_ASSERT Predicate B_THEN Substitution B_END {SubstAssertion($2,$4)}
;

/* \parsersubsec{Choice} */

SubstChoice: B_CHOICE  OrPart  B_END {SubstChoice($2)}
;

OrPart:
  OrPart B_OR Substitution {$1@[$3]}
| Substitution             {[$1]}
;

/* \parsersubsec{IfThenElse} */

SubstIf:
  B_IF ThenPart B_ELSE Substitution B_END
    {SubstIf($2,Some($4))}
| B_IF ThenPart B_END
    {SubstIf($2,None)}
;

ThenPart:
  ThenPart B_ELSIF Predicate B_THEN Substitution {$1@[($3,$5)]}
| Predicate B_THEN Substitution                  {[($1,$3)]}
;

/* \parsersubsec{Select} */

SubstSelect:
  B_SELECT SelectPart B_ELSE Substitution  B_END
    {SubstSelect($2,Some($4))}
| B_SELECT SelectPart B_END
    {SubstSelect($2,None)}
;

SelectPart:
  SelectPart B_WHEN Predicate B_THEN Substitution {$1@[($3,$5)]}
| Predicate B_THEN Substitution                   {[($1,$3)]}
;

/* \parsersubsec{Case} */

SubstCase:
  B_CASE AmbiguousExpr B_OF B_EITHER CasePart B_END B_END
    {SubstCase($2,$5,None)}
| B_CASE AmbiguousExpr B_OF B_EITHER CasePart B_ELSE Substitution B_END B_END
    {SubstCase($2,$5,Some($7))}
;

CasePart:
  CasePart B_OR ExprList B_THEN Substitution {$1@[($3,$5)]}
| ExprList B_THEN Substitution               {[($1,$3)]}
;

/* \parsersubsec{Any} */

SubstAny: B_ANY VariableList B_WHERE Predicate B_THEN Substitution B_END
  {SubstAny($2,$4,$6)}
;

/* \parsersubsec{Let} */

SubstLet: B_LET VariableList B_BE PredLetList B_IN Substitution B_END
  {SubstLet($2,$4,$6)}
;

PredLetList:
  PredLetList B_conjunction PredLet {PredBin(And, $1, $3)}
| PredLet                           {$1}
;

PredLet: Identifier B_equal AmbiguousExpr {PredAtom(Equal, ExprId($1), $3)}
;

/* \parsersubsec{Var} */

SubstVar: B_VAR IdentifierList B_IN Substitution B_END {SubstVar($2,$4)}
;

/* \parsersubsec{While} */

SubstWhile:
  B_WHILE Predicate
  B_DO Substitution
  B_INVARIANT Predicate
  B_VARIANT AmbiguousExpr
  B_END
    {SubstWhile($2,$4,$8,$6)}
| B_WHILE Predicate
  B_DO Substitution
  B_VARIANT AmbiguousExpr
  B_INVARIANT Predicate
  B_END
    {SubstWhile($2,$4,$6,$8)}
;

/* \parsersubsec{Parallel} */

SubstParallel: Substitution B_parallelcomp Substitution
  {SubstParallel($1,$3)}
;

/*
\parsersec{Expressions}\label{gc:expressions}
*/

Predicate:
  PredImplies {$1}
| PredAnd     {$1}
| PredOr      {$1}
| PredEquiv   {$1}
| PredNot     {$1}
| PredForAll  {$1}
| PredExists  {$1}
| PredParen   {PredParen($1)} /* This brings some syntactic information into the AST */
| PredAtom    {$1}

PredImplies: Predicate B_implies Predicate {PredBin(Implies, $1, $3)}
;

PredAnd: Predicate B_conjunction Predicate {PredBin(And, $1, $3)}
;

PredOr: Predicate B_or Predicate {PredBin(Ou, $1, $3)}
;

PredEquiv: Predicate B_equivalence Predicate {PredBin(Equiv, $1, $3)}
;

PredNot: B_not PredParen {PredNegation($2)}
;

PredForAll:
  B_forall Identifier          B_point PredParen {PredForAll([$2],$4)}
| B_forall BrackIdentifierList B_point PredParen {PredForAll($2,$4)}
;

PredExists:
  B_exists Identifier          B_point PredParen {PredExists([$2],$4)}
| B_exists BrackIdentifierList B_point PredParen {PredExists($2,$4)}
;

PredParen: B_PARENOPEN Predicate B_PARENCLOSE {$2}
;

PredAtom:
  SetMembership    {$1}
| NotSetMembership {$1}
| Subset           {$1}
| NotSubset        {$1}
| StrictSubset     {$1}
| NotStrictSubset  {$1}
| PredEqual        {$1}
| PredNotEqual     {$1}
| LessThan         {$1}
| LessThanEqual    {$1}
| GreaterThan      {$1}
| GreaterThanEqual {$1}
;

SetMembership: AmbiguousExpr B_in AmbiguousExpr
  {PredAtom(In, $1, $3)}
/* See the similar rule for x,y,z := E */
;

NotSetMembership: AmbiguousExpr B_notin AmbiguousExpr
  {PredAtom(NotIn, $1, $3)}
;

Subset: AmbiguousExpr B_subset AmbiguousExpr {PredAtom(SubSet, $1, $3)}
;

NotSubset: AmbiguousExpr B_notsubset AmbiguousExpr {PredAtom(NotSubSet, $1, $3)}
;

StrictSubset: AmbiguousExpr B_strictsubset AmbiguousExpr
  {PredAtom(StrictSubSet, $1, $3)}
;

NotStrictSubset: AmbiguousExpr B_notstrictsubset AmbiguousExpr
  {PredAtom(NotStrictSubSet, $1, $3)}
;

PredEqual: AmbiguousExpr B_equal AmbiguousExpr {PredAtom(Equal, $1, $3)}
;

PredNotEqual: AmbiguousExpr B_notequal AmbiguousExpr {PredAtom(NotEqual, $1, $3)}
;

LessThan: AmbiguousExpr B_less AmbiguousExpr {PredAtom(Less, $1, $3)}
;

LessThanEqual: AmbiguousExpr B_lessorequal AmbiguousExpr
  {PredAtom(LessEqual, $1, $3)}
;

GreaterThan: AmbiguousExpr B_greater AmbiguousExpr {PredAtom(Greater, $1, $3)}
;

GreaterThanEqual: AmbiguousExpr B_greaterorequal AmbiguousExpr
  {PredAtom(GreaterEqual, $1, $3)}
;

/*
Expression:
  UnambiguousExpr {$1}
;
*/

DelimitedExpr:
  Number	   {ExprNumber($1)}
| BoolConstant     {ExprBoolConstant($1)}
| String           {$1}
| BasicSets        {ExprSet(SetPredefined($1))}
| B_bool PredParen {ExprBool($2)}

| ReservedUnExpr   {$1}
| ReservedBinExpr  {$1}
| ReservedTrinExpr {$1}

| RelImage         {$1}
| Inverse          {$1}
| SetComprEnum	   {ExprSet($1)}
| SetEmpty         {ExprSet($1)}
| SequenceEnum	   {ExprSeq(SeqEnum($1))}
| SeqEmpty         {ExprSeq($1)}
| UnionQ           {$1}
| InterQ           {$1}
| GeneralSum       {$1}
| GeneralProd      {$1}
| Lambda	   {$1}

| Identifier       {ExprId($1)}
| Before           {$1}
| FunCall          {$1}

| ExprStruct       {ExprSet($1)}
| ExprRec          {ExprRecords($1)}
| ExprField        {ExprRecords($1)}

| B_PARENOPEN AmbiguousExpr B_PARENCLOSE {ExprParen($2)}
;

ExprStruct: B_struct B_PARENOPEN FieldsDecl B_PARENCLOSE {SetRecords($3)}
;

FieldsDecl:
  FieldDecl                    {[$1]}
| FieldsDecl B_comma FieldDecl {$1@[$3]}
;

FieldDecl: Identifier B_in UnambiguousExpr {($1,$3)}
;

ExprRec: B_rec B_PARENOPEN OptFieldVals B_PARENCLOSE {RecordsFields($3)}
;

OptFieldVals:
  OptFieldVal                      {[$1]}
| OptFieldVals B_comma OptFieldVal {$1@[$3]}
;

OptFieldVal:
  UnambiguousExpr                 {(None, $1)}
| Identifier B_in UnambiguousExpr {(Some($1),$3)}
;

ExprField: DelimitedExpr B_quote Identifier {RecordsAccess($1, $3)}
;

UnambiguousExpr:
  DelimitedExpr     {$1}

| UnambiguousBinaryExpr       {$1}
| B_minus UnambiguousExpr %prec B_uminus {ExprUn(UMinus,$2)}
;

AmbiguousExpr:
  CommaExpr                              {single_out_Nuple $1}
| AmbiguousExpr B_semicolon CommaExpr    {ExprBin(Composition, $1,single_out_Nuple $3)}
| AmbiguousExpr B_parallelcomp CommaExpr {ExprBin(ParallelComp,$1,single_out_Nuple $3)}
;

CommaExpr:
  UnambiguousExpr                   {ExprNuplet([$1])}
| CommaExpr B_comma UnambiguousExpr {append_to_Nuple $1 $3}
;

ExprListParen: B_PARENOPEN ExprList B_PARENCLOSE {$2}
;

ExprList: CommaExpr {separate_Nuple $1}
;

RelImage: /* taken from IRIT */
  DelimitedExpr SequenceEnum
    { match $2 with
      | [x] -> ExprBin(Image,$1,x)
      | _ -> parse_error "RelImage"
    }
;

Inverse: DelimitedExpr B_tilde {ExprUn(Tilde,$1)}
;

Before: Identifier B_before {ExprBefore($1)}
;

ReservedUnExpr: ReservedUnary B_PARENOPEN AmbiguousExpr B_PARENCLOSE {ExprUn($1,$3)}
;

ReservedUnary:
  B_pred       {Pred}
| B_succ       {Succ}

| B_closure    {Closure}
| B_closureone {ClosureOne}
| B_perm       {Perm}
| B_conc       {Conc}

| B_min     {Min}
| B_max     {Max}
| B_card    {Card}
| B_POW     {Pow}
| B_POWONE  {PowOne}
| B_FIN     {Fin}
| B_FINONE  {FinOne}
| B_id      {Identity}
| B_dom     {Dom}
| B_ran     {Ran}

| B_seq     {Seq}
| B_seqone  {SeqOne}
| B_iseq    {ISeq}
| B_iseqone {ISeqOne}

| B_fnc     {RelFnc}
| B_rel     {FncRel}

| B_size    {Size}
| B_first   {First}
| B_last    {Last}
| B_front   {Front}
| B_tail    {Tail}
| B_rev     {Rev}

| B_union   {UnionGen}
| B_inter   {InterGen}

| B_tree    {Tree}
| B_btree   {Btree}

| B_top     {Top}
| B_sons    {Sons}
| B_prefix  {Prefix}
| B_postfix {Postfix}
| B_sizet   {SizeT}
| B_mirror  {Mirror}

| B_left    {Left}
| B_right   {Right}
| B_infix   {Infix}
;

ReservedBinExpr: ReservedBinary B_PARENOPEN UnambiguousExpr B_comma UnambiguousExpr B_PARENCLOSE {ExprBin($1,$3, $5)}
;

ReservedBinary:
| B_power       {Puissance}
| B_plus        {Plus}
| B_mul         {Mul}
| B_div         {Div}
| B_mod         {Mod}
| B_natsetrange {NatSetRange}

| B_iterate     {Iterate}
| B_prjone      {Prj1}
| B_prjtwo      {Prj2}

| B_consconst   {Consconst}
| B_rank        {Rank}
| B_father      {Father}
| B_subtree     {SubTree}
| B_arity       {Arity}
;

ReservedTrinExpr:
  B_son B_PARENOPEN UnambiguousExpr B_comma UnambiguousExpr B_comma UnambiguousExpr B_PARENCLOSE {ExprTrin(Son,$3,$5,$7)}
| B_bin B_PARENOPEN UnambiguousExpr B_comma UnambiguousExpr B_comma UnambiguousExpr B_PARENCLOSE {ExprTrin(Bin3,$3,$5,$7)}
| B_bin B_PARENOPEN UnambiguousExpr B_PARENCLOSE {ExprUn(Bin1,$3)}
;

UnionQ:
  B_UNION BrackIdentifierList PointPart {let (p,e) = $3 in ExprSet(SetUnionQ($2, p, e))}
| B_UNION Identifier          PointPart {let (p,e) = $3 in ExprSet(SetUnionQ([$2], p, e))}
;

InterQ:
  B_INTER BrackIdentifierList PointPart {let (p,e) = $3 in ExprSet(SetInterQ($2, p, e))}
| B_INTER Identifier          PointPart {let (p,e) = $3 in ExprSet(SetInterQ([$2], p, e))}
;

GeneralSum:
  B_SIGMA BrackIdentifierList PointPart {let (p,e) = $3 in ExprSIGMA($2,p,e)}
| B_SIGMA Identifier PointPart          {let (p,e) = $3 in ExprSIGMA([$2],p,e)}
;

GeneralProd:
  B_PI BrackIdentifierList PointPart {let (p,e) = $3 in ExprPI($2,p,e)}
| B_PI Identifier PointPart          {let (p,e) = $3 in ExprPI([$2],p,e)}
;

Lambda:
  B_lambda Identifier          PointPart {let (p,e) = $3 in ExprLambda([$2],p,e)}
| B_lambda BrackIdentifierList PointPart {let (p,e) = $3 in ExprLambda($2,p,e)}
;

PointPart: B_point B_PARENOPEN Predicate B_such AmbiguousExpr B_PARENCLOSE {($3,$5)}
;

UnambiguousBinaryExpr:
  UnambiguousExpr B_plus UnambiguousExpr {ExprBin(Plus,$1,$3)}
| UnambiguousExpr B_minus UnambiguousExpr {ExprBin(Minus,$1,$3)}
| UnambiguousExpr B_mul UnambiguousExpr {ExprBin(Mul,$1,$3)}
| UnambiguousExpr B_div UnambiguousExpr {ExprBin(Div,$1,$3)}
| UnambiguousExpr B_mod UnambiguousExpr {ExprBin(Mod,$1,$3)}
| UnambiguousExpr B_power UnambiguousExpr {ExprBin(Puissance,$1,$3)}

| UnambiguousExpr B_domainrestrict UnambiguousExpr {ExprBin(DomRestrict,$1,$3)}
| UnambiguousExpr B_rangerestrict UnambiguousExpr {ExprBin(RanRestrict,$1,$3)}
| UnambiguousExpr B_domainsubstract UnambiguousExpr {ExprBin(DomSubstract,$1,$3)}
| UnambiguousExpr B_rangesubstract UnambiguousExpr {ExprBin(RanSubstract,$1,$3)}

| UnambiguousExpr B_override UnambiguousExpr {ExprBin(OverRide,$1,$3)}

| UnambiguousExpr B_directprod UnambiguousExpr {ExprBin(DirectProd,$1,$3)}

| UnambiguousExpr B_prependseq UnambiguousExpr {ExprBin(PrependSeq,$1,$3)}
| UnambiguousExpr B_appendseq UnambiguousExpr {ExprBin(AppendSeq,$1,$3)}
| UnambiguousExpr B_prefixseq UnambiguousExpr {ExprBin(PrefixSeq,$1,$3)}
| UnambiguousExpr B_suffixseq UnambiguousExpr {ExprBin(SuffixSeq,$1,$3)}
| UnambiguousExpr B_concatseq UnambiguousExpr {ExprBin(ConcatSeq,$1,$3)}

| UnambiguousExpr B_natsetrange UnambiguousExpr {ExprBin(NatSetRange,$1,$3)}

| UnambiguousExpr B_unionsets UnambiguousExpr {ExprBin(UnionSets,$1,$3)}
| UnambiguousExpr B_intersectionsets UnambiguousExpr {ExprBin(InterSets,$1,$3)}
| UnambiguousExpr B_setminus UnambiguousExpr {ExprBin(SetMinus,$1,$3)}

| UnambiguousExpr B_cartesianprod UnambiguousExpr {ExprBin(CartesianProd, $1, $3)}
| UnambiguousExpr B_relation UnambiguousExpr {ExprBin(Relation, $1, $3)}
| UnambiguousExpr B_partialfunc UnambiguousExpr {ExprBin(PartialFunc, $1, $3)}
| UnambiguousExpr B_totalfunc UnambiguousExpr {ExprBin(TotalFunc, $1, $3)}
| UnambiguousExpr B_partialinject UnambiguousExpr {ExprBin(PartialInj, $1, $3)}
| UnambiguousExpr B_totalinject UnambiguousExpr {ExprBin(TotalInj, $1, $3)}
| UnambiguousExpr B_partialsurject UnambiguousExpr {ExprBin(PartialSurj, $1, $3)}
| UnambiguousExpr B_totalsurject UnambiguousExpr {ExprBin(TotalSurj, $1, $3)}
| UnambiguousExpr B_partialbij UnambiguousExpr {ExprBin(PartialBij, $1, $3)}
| UnambiguousExpr B_bijection UnambiguousExpr {ExprBin(TotalBij, $1, $3)}
/*
  N-uples are mostly a binary operator (same priority as other binary
  operators) but they are handled in a particular way because of some
  optimizations (representation is list-like rather than tree-like).
  Removing this optimization would allow "smoothing" the grammar a bit here.
*/

| UnambiguousExpr B_mapletorpair UnambiguousExpr {
  try
    append_to_Nuple $1 $3
  with
      Invalid_argument _ -> ExprNuplet([$1;$3])
}
;

FunCall: DelimitedExpr B_PARENOPEN AmbiguousExpr B_PARENCLOSE {ExprFunCall($1, $3)}
;

SetComprEnum: B_CURLYOPEN SetComprEnumBody B_CURLYCLOSE {$2}
;

/* This weird subdivision is caused by a reduce/reduce conflict upon:
  { a,b,c...} : not knowing what "..." holds, is it an identifier list
  (case of a comprehension set) or an expression list composed of
  identifiers  ? We allow the parser to accept more but we
  programmatically  restrict what is indeed parsed (expr2id)
*/
SetComprEnumBody:
  ExprList B_such Predicate {SetComprPred((tolerate_parentheses $1),$3)}
| ExprList                  {SetEnum($1)}
;

SequenceEnum: B_BRACKOPEN ExprList B_BRACKCLOSE {$2}
;

SetEmpty: B_emptyset {SetEmpty}
;

SeqEmpty: B_seqempty {SeqEmpty}
;

BrackIdentifierList: B_PARENOPEN IdentifierList B_PARENCLOSE {$2}
;

Number:
  B_NUMBER {Number(Int32.of_string($1.lex_value))}
/* TODO To remove : | B_minus B_NUMBER {Number(Int32.of_string("-"^$2))} */
| B_MAXINT {MaxNumber}
| B_MININT {MinNumber}
;

BoolConstant:
  B_true {TrueConstant}
| B_false {FalseConstant}
;

String: B_STRING {ExprString($1.lex_value)}
;

BasicSets:
  B_NAT          {NAT}
| B_NATONE       {NAT1}
| B_INT          {INT}
| B_NATURAL      {NATURAL}
| B_NATURALONE   {NATURAL1}
| B_INTEGER      {INTEGER}
| B_BOOL         {BOOL}
| B_STRINGS      {STRING}
;

%%
