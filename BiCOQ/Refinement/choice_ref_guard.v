(*==================================================================================================
  Project : Study of the concept of refinement
  Module : choice_ref_guard
  We study the concept of choice refinement, that is the elimination of non-determinism ; it is
  basically the same as choice_ref_pre, except for the fact that we allow for the
  representation of guards instead of preconditions.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/ANSSI & UPMC/LIP6 - St: July 2009, Md: July 2009
  Provided under licence CeCILL-B
  ================================================================================================*)

Require Export relation_gen.

(* Choice-refinement definition ------------------------------------------------------------------*)

Definition ChGRef(dom ran:Set)(Sa Sc:dom↔ran):=∏1(Sa)⊆∏1(Sc) /\ Sc⊆Sa.
Notation "Sa '=≻' Sc":=(ChGRef _ _ Sa Sc) (no associativity, at level 90).
(* Nota: ∏1(Sa)⊆∏1(Sc) ensures that the refinement addresses all the specification and Sc⊆Sa that
   the refinement is compliant. *)

(* Choice-refinement properties ------------------------------------------------------------------*)

Theorem SEqu_ChGRef_l:forall (dom ran:Set)(S:dom↔ran), SEqu_congr_prop (ChGRef dom ran S).
Proof.
 intros dom ran S; unfold SEqu_congr_prop; unfold ChGRef; intros S1 S2 HEq [HD1 HR1]; split.
  apply (SEqu_SInc_l _ ∏1(S) ∏1(S1) ∏1(S2)); [apply SEqu_RDom; apply HEq | apply HD1].
  apply (SEqu_SInc_r _ S S1 S2); [apply HEq | apply HR1].
Qed.

Theorem SEqu_ChGRef_r:forall (dom ran:Set)(S:dom↔ran),
                      SEqu_congr_prop (fun S':dom↔ran=>ChGRef dom ran S' S).
Proof.
 intros dom ran S; unfold SEqu_congr_prop; unfold ChGRef; intros S1 S2 HEq [HD1 HR1]; split.
  apply (SEqu_SInc_r _ ∏1(S) ∏1(S1) ∏1(S2)); [apply (SEqu_RDom _ _ S1 S2); apply HEq | apply HD1].
  apply (SEqu_SInc_l _ S _ _ HEq); apply HR1.
Qed.

Theorem ChGRef_refl:forall (dom ran:Set)(S:dom↔ran), S=≻S. (* Reflexivity *)
Proof.
 intros dom ran S; unfold ChGRef; split; apply SInc_refl.
Qed.

Theorem ChGRef_asym:forall (dom ran:Set)(S1 S2:dom↔ran),  S1=≻S2->S2=≻S1->S1≡S2.
Proof.
 intros dom ran S1 S2; unfold ChGRef; unfold SEqu; unfold SInc; intros [Hd12 Hi12] [Hd21 Hi21];
  split; intros [d r] Hdr.
  assert (Hd:∏1(S1) d). apply RDom_ with (b:=r); apply Hdr.
  apply Hi21; apply Hdr.
  assert (Hd:∏1(S2) d). apply RDom_ with (b:=r); apply Hdr.
  apply Hi12; apply Hdr.
Qed.

Theorem ChGRef_tran:forall (dom ran:Set)(S1 S2 S3:dom↔ran), S1=≻S2->S2=≻S3->S1=≻S3.
Proof.
 intros dom ran S1 S2 S3; unfold ChGRef; unfold SInc; intros [Hd12 Hr21] [Hd23 Hr32]; split;
  intros d Hd; [apply Hd23; apply Hd12 | apply Hr21; apply Hr32]; apply Hd.
Qed.

Theorem ChGRef_dom:forall (dom ran:Set)(S1 S2:dom↔ran), S1=≻S2->∏1(S1)≡∏1(S2).
Proof.
 intros dom ran S1 S2; unfold ChGRef; intros [HD HR]; unfold SEqu; split; [apply HD | ].
  intros d Hd; inversion_clear Hd; apply (RDom_ dom ran S1 d b (HR _ H)).
Qed.

Theorem ChGRef_mon:forall (dom int ran:Set)(Sa Sc:dom↔int)(Sa' Sc':int↔ran),
                   Sa⊳∏1(Sa')=≻Sc->Sa'=≻Sc'->Sa'∘Sa=≻Sc'∘Sc.
Proof.
 intros dom int ran Sa Sc Sa' Sc' Href Href'.
 generalize (ChGRef_dom _ _ _ _ Href); intros HD; destruct Href as [_ HR].
 generalize (ChGRef_dom _ _ _ _ Href'); intros HD'; destruct Href' as [_ HR'].
 split; intros d Hd.
  inversion_clear Hd; inversion_clear H.
  generalize (RDom_ _ _ _ _ _ H1); intros Hb0.
  generalize (RRanR_ _ _ _ _ _ _ H0 Hb0); intros Hdb0.
  generalize ((proj1 HD) _ (RDom_ _ _ _ _ _ Hdb0)); intros Hd.
  inversion_clear Hd; generalize (HR _ H); intros Hdb1.
  inversion_clear Hdb1.
  generalize ((proj1 HD') _ H3); intros H'b0.
  inversion_clear H'b0.
  apply (RDom_ dom ran (Sc'∘Sc) d b2); apply (RCmp_ _ _ _ _ _ _ _ H _ H4).
  inversion_clear Hd.
  generalize (HR _ H); intros Hab; inversion_clear Hab.
  generalize (HR' _ H0); intros Hbc.
  apply (RCmp_ dom int ran Sa Sa' a b H1 c Hbc).
Qed.

Theorem SInc_ChGRef:forall (dom ran:Set)(Sa Sc:dom↔ran), Sc⊆Sa->∏1(Sc)⊲Sa=≻Sc.
Proof.
 intros dom ran Sa Sc HInc; unfold ChGRef; split.
  intros d Hd; inversion_clear Hd; rename b into r; inversion_clear H; apply H1.
  intros [d r] Hdr; apply RDomR_; [apply HInc; apply Hdr | apply (RDom_ _ _ _ _ _ Hdr)].
Qed.

Theorem RFnc_ChGRef:forall (dom ran:Set)(Sa Sc:dom↔ran), RFnc Sa->Sa=≻Sc->Sc≡Sa.
Proof.
 intros dom ran Sa Sc; unfold RFnc; intros HFnc Href; generalize (ChGRef_dom _ _ _ _ Href);
  intros HD; destruct Href as [_ HR]; unfold SEqu; split.
  apply HR.
  intros [d r] Hdr; generalize ((proj1 HD) _ (RDom_ _ _ _ _ _ Hdr)); intros Hd.
  inversion_clear Hd; generalize (HR _ H); intros Hdb.
  rewrite (HFnc _ _ _ Hdr Hdb); apply H.
Qed.

(*================================================================================================*)