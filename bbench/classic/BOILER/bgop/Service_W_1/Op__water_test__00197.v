 

Require Import Blib.

Theorem op:
forall _CAP _DT _M1 _M2 _N1 _N2 _NB_PUMP _PNOMINAL _PUMP1 _PUMP2 _PUMP3 _PUMP4 _U1 _U2 _WMAX _acq_lfam _acq_lim _acq_lrm _acq_pcl_0 _acq_pop_0 _acq_pr _acq_qr _acq_rfam _acq_rim _acq_rr _acq_rrm _acq_sfam _acq_sim _acq_srm _acq_vr _acq_wfam _acq_wim _acq_wpcl_0 _acq_wpop_1 _acq_wrm _controller_number _pa1 _pa2 _pc1 _pc2 _pr _unknown_state _wfam _wfm _wim _wok _wpcl_0 _wpop_1 _wram _wrm _wtk, ( 
 (In BN _controller_number) -> 
(In BBOOL _unknown_state) -> 
(In BBOOL _wim) -> 
(In BBOOL _wrm) -> 
(In BBOOL _wfam) -> 
(In {= 0, _PNOMINAL =} _pr) -> 
(In BBOOL _wpop_1) -> 
(In BBOOL _wpcl_0) -> 
(In {= 0, _PNOMINAL =} _pa1) -> 
(In {= 0, _PNOMINAL =} _pa2) -> 
(_pa1 <= _pa2) -> 
(In {= 0, _PNOMINAL =} _pc1) -> 
(In {= 0, _PNOMINAL =} _pc2) -> 
(_pc1 <= _pc2) -> 
(In BBOOL _wfm) -> 
(In BBOOL _wram) -> 
(In BBOOL _wok) -> 
(In BBOOL _wtk) -> 
(In BN1 _N1) -> 
(In BN1 _N2) -> 
(In BN1 _M1) -> 
(In BN1 _M2) -> 
(In BN1 _CAP) -> 
(_N1 < _N2) -> 
(_M1 < _M2) -> 
(_M1 < _N1) -> 
(_N2 < _M2) -> 
(_M2 < _CAP) -> 
(In BN _NB_PUMP) -> 
(_NB_PUMP = 4) -> 
(In BN _PUMP1) -> 
(In BN _PUMP2) -> 
(In BN _PUMP3) -> 
(In BN _PUMP4) -> 
(_PUMP1 = 1) -> 
(_PUMP2 = 2) -> 
(_PUMP3 = 3) -> 
(_PUMP4 = 4) -> 
(In (interval 1 _NB_PUMP) _PUMP1) -> 
(In (interval 1 _NB_PUMP) _PUMP2) -> 
(In (interval 1 _NB_PUMP) _PUMP3) -> 
(In (interval 1 _NB_PUMP) _PUMP4) -> 
(In BN1 _WMAX) -> 
(In BN1 _U1) -> 
(In BN1 _U2) -> 
(In BN1 _DT) -> 
(In BN1 _PNOMINAL) -> 
(In BN1 (((_PNOMINAL + _PNOMINAL) + _PNOMINAL) + _PNOMINAL)) -> 
(In BN1 ((((_PNOMINAL + _PNOMINAL) + _PNOMINAL) + _PNOMINAL) * _DT)) -> 
(In BN1 (_U2 * _DT)) -> 
(In BN1 (_U1 * _DT)) -> 
(In BN1 ((_U1 * _DT) + _WMAX)) -> 
((_U2 * _DT) <= _WMAX) -> 
(In BN1 (2 * _WMAX)) -> 
(In BN1 (_WMAX * _DT)) -> 
(In BN1 ((_U1 * _DT) * _DT)) -> 
(In BN1 ((_U2 * _DT) * _DT)) -> 
(In BN1 ((((_U1 * _DT) * _DT) + 1) / 2)) -> 
(In BN1 ((((_U2 * _DT) * _DT) + 1) / 2)) -> 
(In BN1 ((4 * _PNOMINAL) + ((1 + ((_U2 * _DT) * _DT)) / 2))) -> 
(In BN1 ((_CAP + ((4 * _PNOMINAL) * _DT)) + ((1 + ((_U2 * _DT) * _DT)) / 2))) -> 
(In BZ (((1 + ((_U2 * _DT) * _DT)) / 2) - (_WMAX * _DT))) -> 
(In BN1 (_CAP + ((4 * _PNOMINAL) * _DT))) -> 
(In BZ (- ((_WMAX * _DT) + ((1 + ((_U2 * _DT) * _DT)) / 2)))) -> 
(In BBOOL _acq_lim) -> 
(In BBOOL _acq_lfam) -> 
(In BBOOL _acq_lrm) -> 
(In BN _acq_qr) -> 
(In BBOOL _acq_sim) -> 
(In BBOOL _acq_sfam) -> 
(In BBOOL _acq_srm) -> 
(In BN _acq_vr) -> 
(In (total_function (interval 1 _NB_PUMP) BBOOL ) _acq_wim) -> 
(In (total_function (interval 1 _NB_PUMP) BBOOL ) _acq_wfam) -> 
(In (total_function (interval 1 _NB_PUMP) BBOOL ) _acq_wrm) -> 
(In (total_function (interval 1 _NB_PUMP) BN ) _acq_pr) -> 
(In {= 0, _PNOMINAL =} (app _acq_pr (1) _)) -> 
(In {= 0, _PNOMINAL =} (app _acq_pr (2) _)) -> 
(In {= 0, _PNOMINAL =} (app _acq_pr (3) _)) -> 
(In {= 0, _PNOMINAL =} (app _acq_pr (4) _)) -> 
(In (total_function (interval 1 _NB_PUMP) BBOOL ) _acq_rim) -> 
(In (total_function (interval 1 _NB_PUMP) BBOOL ) _acq_rfam) -> 
(In (total_function (interval 1 _NB_PUMP) BBOOL ) _acq_rrm) -> 
(In (total_function (interval 1 _NB_PUMP) BBOOL ) _acq_rr) -> 
(In BBOOL _acq_pop_0) -> 
(In BBOOL _acq_pcl_0) -> 
(In BBOOL _acq_wpop_1) -> 
(In BBOOL _acq_wpcl_0) -> 
(In (interval 1 _NB_PUMP) _controller_number)
  -> (
 (~ (((In (interval _pc1 _pc2) _pr) /\ ((_wok = true) \/ (_wrm = true)))) -> ((((NotIn (interval _pc1 _pc2) _pr) /\ ((_wok = true) \/ (_wrm = true))) \/ ((_wfm = true) /\ (_wfam = false))) -> (~ ((((_wim = true) /\ ((_wok = false) \/ (_wrm = false))) /\ ((_wfam = false) \/ (_wfm = true)))) -> (_pa1 <= _pa2)))) ) )
 .
try intuition 
   || unfold BBOOL ; intuition ; apply Full_intro
   || red ;  red ; red ; omega.
Qed.
 