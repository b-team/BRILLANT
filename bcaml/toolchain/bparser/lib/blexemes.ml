
type lex_info = {
  lex_value : string;
  lex_beginning : int * int; (* line * column *)
  lex_end : int * int; (* line * column *)
}

(* This is useful if you want to declare beforehand lexemes for
   keywords : you know their value but now their positions beforehand *)
let default_info s =
  { lex_value = s;
    lex_beginning = (-1, -1);
    lex_end = (-1, -1)
  }

let full_lexeme_info s b e =
  { lex_value = s;
    lex_beginning = b;
    lex_end = e
  }

let update_positions li b e =
  { lex_value = li.lex_value;
    lex_beginning = b;
    lex_end = e
  }

let append li s e =
  { lex_value = li.lex_value ^ s;
    lex_beginning = li.lex_beginning;
    lex_end = e
  }

let super_impose_position li1 li2 =
  update_positions li1 li2.lex_beginning li2.lex_end

let string_of_lex_info li =
  li.lex_value
  ^ " B(l:" ^ (string_of_int (fst li.lex_beginning))
  ^ ",c:"^ (string_of_int (snd li.lex_beginning)) ^ ")"
  ^ " "
  ^ " E(l:" ^ (string_of_int (fst li.lex_end))
  ^ ",c:"^ (string_of_int (snd li.lex_end)) ^ ")"

let to_string li = li.lex_value
