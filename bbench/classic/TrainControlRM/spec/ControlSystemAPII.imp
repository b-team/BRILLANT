IMPLEMENTATION    ControlSystemAPII

REFINES           ControlSystemAPI

SEES              Responses, SectionSets, RailSets, 
		  SystemData, ControlDefs, Bool_TYPE, Bool_TYPE_Ops,
		  Nextsections_set_ctx, Section_fnc_ctx, Track_fnc_ctx,
		  Tracksects_seq_ctx, Trackoffs_seq_ctx, Crossover_fnc_ctx,
		  Station_fnc_ctx, Control_fnc_ctx, Schedule_fnc_ctx, 
		  Timetable_seq_ctx, Crosssched_seq_ctx

IMPORTS           ControlSystem(bubble_size, cross_speed)

PROMOTES	  save, restore, save_railnet, restore_railnet

OPERATIONS

  response , sect <-- start_sectionR(grad , leng , spd) = 
      VAR ok IN
	ok <-- more_sections_query;
	IF ok = TRUE THEN
	    sect <-- start_section(grad, leng, spd);
	    response := OK
	ELSE
	    sect <-- any_section; response := NoMoreSectionTokens
	END
      END;

  response , sect <-- next_sectionR(p_sect , grad , leng , spd) = 
      VAR oka, okb IN
	sect <-- any_section;
	oka <-- section_exists_query(p_sect);
	okb <-- more_sections_query;
        IF oka = FALSE THEN
	   response := NoSuchPreviousSection
	ELSIF okb = FALSE THEN
	   response := NoMoreSectionTokens
	ELSE
	   sect <-- next_section(p_sect, grad, leng, spd);
	   response := OK
	END
      END;
	   
  response , sect <-- merge_sectionR(p_sect1 , p_sect2 , grad , leng , spd) = 
      VAR oka, okb, okc IN
	sect <-- any_section;
	oka <-- section_exists_query(p_sect1);
	okb <-- section_exists_query(p_sect2);
	okc <-- more_sections_query;
	IF oka = FALSE THEN
	   response := NoSuchPreviousSection1
	ELSIF okb = FALSE THEN
	   response := NoSuchPreviousSection2
	ELSIF okc = FALSE THEN
	   response := NoMoreSectionTokens
	ELSE
	   sect <-- merge_section(p_sect1, p_sect2,  grad, leng, spd);
	   response := OK
	END
      END;

  response , grad <-- section_gradientR(sect) = 
      VAR ok IN
	ok <-- section_exists_query(sect);
	IF ok = TRUE THEN
	   grad <-- section_gradient(sect);
	   response := OK 
	ELSE
	   grad := 1;  response := NoSuchSection
	END
      END; 

  response , leng <-- section_lengthR(sect) = 
      VAR ok IN
	ok <-- section_exists_query(sect);
	IF ok = TRUE THEN
	   leng <-- section_length(sect);
	   response := OK
	ELSE
	   leng := 1;  response := NoSuchSection
	END
      END;

  response , spd <-- section_speedlimitR(sect) = 
      VAR ok IN
	ok <-- section_exists_query(sect);
	IF ok = TRUE THEN
	   spd <-- section_speedlimit(sect);
	   response := OK
	ELSE
	   spd := 1; response := NoSuchSection
	END
      END;

  response , trck <-- new_trackR(dir) = 
      VAR ok IN
	ok <-- more_tracks_query;
	IF ok = TRUE THEN
	   trck <-- new_track(dir);
	   response := OK
	ELSE
	   trck <-- any_track; response := NoMoreTrackTokens
	END
      END;  

  response <-- extend_trackR(trk , sect) = 
      VAR oka, okb, okc, okd IN
	response := NoSuchTrack;
	oka <-- track_exists_query(trk);
	okb <-- section_exists_query(sect);
	okc <-- track_section_query(trk, sect);
	okd <-- extend_track_query(sect, trk);
	IF oka = FALSE THEN
	    response := NoSuchTrack
	ELSIF okb = FALSE THEN
	    response := NoSuchSection
	ELSIF okc = TRUE THEN
	    response := TrackAlreadyLaidSection
	ELSIF okd = FALSE THEN
	    response := SectionsNotConnected
	ELSE
	    extend_track(trk, sect);
	    response := OK
	END
      END;

  response , stat <-- new_stationR(trk , off) = 
      VAR oka, okb, okc IN
	stat <-- any_station; 
	oka <-- track_exists_query(trk);
	okb <-- valid_offset_query(trk, off);
	okc <-- more_stations_query;
	IF oka = FALSE THEN
	    response := NoSuchTrack
	ELSIF okb = FALSE THEN
	    response := InvalidOffset
	ELSIF okc = FALSE THEN
	    response := NoMoreStationTokens
	ELSE
	    stat <-- new_station(trk, off);
	    response := OK
	END
      END;
	    
  response , cross <-- add_crossoverR(trk , off , end) = 
      VAR oka, okb, okc, okd, oke, okf IN
	cross <-- any_crossover; 
	oka <-- track_exists_query(trk);
	okb <-- track_exists_query(end);
	okc <-- valid_offset_query(trk, off + 1);
	okd <-- more_crossovers_query;
	oke <-- same_dir_query(trk, end);
	IF oka = FALSE THEN
	    response := NoSuchTrack
	ELSIF okb = FALSE THEN
	    response := NoSuchEnd
	ELSIF okc = FALSE THEN
	    response := InvalidOffset
	ELSIF okd = FALSE THEN
	    response := NoMoreCrossTokens
	ELSIF trk = end THEN
	    response := CrossToSameTrack
	ELSIF oke = FALSE THEN
	    response := DifferentDir
	ELSE
	    /* 
		this precondition depends on above preconditions! Or else 
	       segmentation fault. 
		*/
	    okf <-- add_crossover_query(trk, off, end);
	    IF okf = FALSE THEN
	        response := ImpossibleCross
	    ELSE
	        cross <-- add_crossover(trk, off, end);
	        response := OK
	    END
        END
      END; 

  response <-- connect_crossR(cross) = 
      VAR oka, okb IN
	oka <-- cross_exists_query(cross);
	okb <-- connect_cross_query(cross);
	IF oka = FALSE THEN
	    response := NoSuchCross
	ELSIF okb = FALSE THEN
	    response := UnsafeToConnect
	ELSE
	    connect_cross(cross);
	    response := OK
	END
      END;

  response <-- disconnect_crossR(cross) = 
      VAR ok IN
	ok <-- cross_exists_query(cross);
	IF ok = TRUE THEN
	    disconnect_cross(cross);
	    response := OK
	ELSE
	    response := NoSuchCross
	END
      END; 

  response , sect , off <-- track_queryR(trk , ii) = 
      VAR oka, okb IN
	sect <-- any_section; off := 1; 
	oka <-- track_exists_query(trk);
	okb <-- tracksiz_query(trk, ii);
	IF oka = FALSE THEN
	    response := NoSuchTrack
	ELSIF okb = FALSE THEN
	    response := SequenceOutOfBounds
	ELSE
	    sect, off <-- track_query(trk, ii);
	    response := OK
	END
      END; 
	
  response , trk , off <-- cross_from_queryR(cross) = 
      VAR ok IN
	ok <-- cross_exists_query(cross);
	IF ok = TRUE THEN
	    trk, off <-- cross_from_query(cross);
	    response := OK
	ELSE
	    trk <-- any_track; off := 1; response := NoSuchCross
	END
      END;

  response , trk <-- cross_to_queryR(cross) = 
      VAR ok IN
	ok <-- cross_exists_query(cross);
	IF ok = TRUE THEN
	    trk <-- cross_to_query(cross);
	    response := OK
	ELSE
	    trk <-- any_track; response := NoSuchCross
	END
      END;

  response , cstat <-- cross_stat_queryR(cross) = 
      VAR ok IN
	ok <-- cross_exists_query(cross);
	IF ok = TRUE THEN
	    cstat <-- cross_stat_query(cross);
	    response := OK
	ELSE
	    cstat := c_off; response := NoSuchCross
	END
      END;

  response , len <-- track_length_queryR(trk) = 
      VAR ok IN
	ok <-- track_exists_query(trk);
	IF ok = TRUE THEN
	    len <-- track_length_query(trk);
	    response := OK
	ELSE
	    len := 1; response := NoSuchTrack
	END
      END; 

  response , thissect <-- this_section_queryR(trk , off) = 
      VAR oka, okb, okc IN
	thissect <-- any_section; 
	oka <-- track_exists_query(trk);
	okb <-- tracksiz_query(trk, 1);
	okc <-- valid_offset_query(trk, off);
	IF oka = FALSE THEN
	    response := NoSuchTrack
	ELSIF okb = FALSE THEN
	    response := EmptyTrack
	ELSIF okc = FALSE THEN
	    response := InvalidOffset
	ELSE
	    thissect <-- this_section_query(trk,off);
	    response := OK
	END
      END;  

  response , nextsect <-- next_section_queryR(trk , off) = 
      VAR oka, okb IN
	nextsect <-- any_section; 
	oka <-- track_exists_query(trk);
	okb <-- nextsection_exists_query(trk, off);
	IF oka = FALSE THEN
	    response := NoSuchTrack
	ELSIF okb = FALSE THEN
	    response := NoNextSection
	ELSE
	    nextsect <-- next_section_query(trk,off);
	    response := OK
	END
      END;  

  response , exists <-- nextsection_exists_queryR(trk , off) = 
      VAR ok IN
	ok <-- track_exists_query(trk);
	IF ok = TRUE THEN
	    exists <-- nextsection_exists_query(trk, off);
	    response := OK
	ELSE
	    exists := FALSE; response := NoSuchTrack
	END
      END;

  response , sectoff <-- section_offset_queryR(trk , off) = 
      VAR oka, okb, okc IN
	sectoff := 1; 
	oka <-- track_exists_query(trk);
	okb <-- tracksiz_query(trk, 1);
	okc <-- valid_offset_query(trk, off);
	IF oka = FALSE THEN
	    response := NoSuchTrack
	ELSIF okb = FALSE THEN
	    response := EmptyTrack
	ELSIF okc = FALSE THEN
	    response := InvalidOffset
	ELSE
	    sectoff <-- section_offset_query(trk,off);
	    response := OK
	END
      END; 

  response , trkoff <-- convert_offsetR(trk , sect , off) = 
      VAR oka, okb, okc IN
	trkoff := 1; 
	oka <-- track_exists_query(trk);
	okb <-- section_exists_query(sect);
	okc <-- track_section_query(trk, sect);
	IF oka = FALSE THEN
	    response := NoSuchTrack
	ELSIF okb = FALSE THEN
	    response := NoSuchSection
	ELSIF okc = FALSE THEN
	    response := TrackNotLaidSection
	ELSE
	    trkoff <-- convert_offset(trk, sect, off);
	    response := OK
	END
      END;
	
  response, schd <-- new_scheduleR = 
      VAR ok IN
	ok <-- more_scheds_query;
	IF ok = TRUE THEN
	    schd <-- new_schedule; response := OK
	ELSE
	    response := NoMoreSchedTokens
	END
      END;

  response <-- extend_station_scheduleR(sch , stat) = 
      VAR oka, okb, okc IN
	oka <-- sched_exists_query(sch);
	okb <-- stat_exists_query(stat);
	okc <-- stat_not_in_sched_query(sch, stat);
	IF oka = FALSE THEN
	    response := NoSuchSched
	ELSIF okb = FALSE THEN
	    response := NoSuchStation
	ELSIF okc = FALSE THEN
	    response := StatInSched
	ELSE
	    extend_station_schedule(sch, stat);
	    response := OK
	END
      END;

  response <-- extend_cross_scheduleR(sch , cross) = 
      VAR oka, okb, okc IN
	oka <-- sched_exists_query(sch);
	okb <-- cross_exists_query(cross);
	okc <-- extend_cross_schedule_query(sch, cross);
	IF oka = FALSE THEN
	    response := NoSuchSched
	ELSIF okb = FALSE THEN
	    response := NoSuchCross
	ELSIF okc = FALSE THEN
	    response := InvalidCross
	ELSE 
	    extend_cross_schedule(sch,cross);
	    response := OK
	END
      END; 

  response , idd <-- register_trainR(trk , macc , sacc , schd) = 
      VAR oka, okb, okc, okd IN
	idd <-- any_train; 
	oka <-- track_exists_query(trk);
	okb <-- sched_exists_query(schd);
	okc <-- more_trainid_query;
	okd <-- tracksiz_query(trk, 1);
	IF oka = FALSE THEN
	    response := NoSuchTrack
	ELSIF okb = FALSE THEN
	    response := NoSuchSched
	ELSIF okc = FALSE THEN
	    response := NoMoreTrainTokens
	ELSIF okd = FALSE THEN
	    response := EmptyTrack
	ELSE
	    idd <-- register_train(trk, macc, sacc, schd);
	    response := OK
	END
      END;

  response <-- remove_trainR(idd) = 
      VAR ok IN
	ok <-- trainid_exists_query(idd);
	IF ok = TRUE THEN
	    remove_train(idd);
	    response := OK
	ELSE
	    response := NoSuchTrain
	END
      END;

  response , spd , mde <-- issue_instructionR(idd) = 
      VAR oka, okb IN
	spd := 0; mde := deacc_override; 
	oka <-- trainid_exists_query(idd);
	okb <-- sstate_query(idd, received_update);
	IF oka = FALSE THEN
	    response := NoSuchTrain
	ELSIF okb = FALSE THEN
	    response := Invalid_sstate
	ELSE
	    spd, mde <-- issue_instruction(idd);
	    response := OK
	END
      END;

  response <-- update_control_systemR(idd , trk , off , spd) = 
      VAR oka, okb, okc, okd, oke IN
	oka <-- trainid_exists_query(idd);
	okb <-- track_exists_query(trk);
	okc <-- valid_offset_query(trk, off);
	okd <-- sstate_query(idd, instruction_issued);
	oke <-- tracksiz_query(trk, 1);
	IF oka = FALSE THEN
	    response := NoSuchTrain
	ELSIF okb = FALSE THEN
	    response := NoSuchTrack
	ELSIF okc = FALSE THEN
	    response := InvalidOffset
	ELSIF okd = FALSE THEN
	    response := Invalid_sstate
	ELSIF oke = FALSE THEN
	    response := EmptyTrack
	ELSE
	    update_control_system(idd,trk,off,spd);
	    response := OK
	END
      END

END
