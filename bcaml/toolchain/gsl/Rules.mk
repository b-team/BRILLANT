
FILES = gsl_adt.mli \
 gsl_adt.ml \
 gsl_acc.mli \
 gsl_acc.ml \
 gsl_lib.ml \
 gsl_lib.mli \
 gsl.ml \
 gsl.mli

DEPS = Bcaml

NAME = Gsl
MAKELIB = yes
