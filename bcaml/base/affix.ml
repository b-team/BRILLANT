(* $Id$ *)

let table_Prefix =
  [
   "**"        	
     ; "$"        	 
     ; "("        	     
     ; ")"        	     
     ; "["        	
     ; "]"        	
     ; "'"        	
     ; "%"        	
     ; "{}" 
     ; "<>" 
     ; "[]" 
     ; ":|" 
     ; "|"  
     ; "!"  
     ; "#"  
     ; "{"  
     ; "}"  
     ; "prj1"
     ; "prj2"
 ] 
    
let table_Infix =
  [   "<="
     ; "."  
     ; "*"        	
     ; "/"        	
     ; "="        	
     ; "&"        	
     ; "<"        	
     ; ">"        	
     ; ","        	
     ; ";"        	 
     ; ";;"        	 
     ; ":"        	 
     ; "||"        	
     ; "|->"        	
     ; "<|"        	
     ; "|>"        
     ; "<<|"        
     ; "|>>"        
     ; "<+"        	
     ; "+>"        	
     ; "><"        	
     ; "->"        	
     ; "/|\\"        	
     ; "\\|/"        	
     ; "/\\"        	
     ; "::"        	 
     ; "<=>"          
     ; "<:"        	
     ; "/<:"        	
     ; "=>"        	
     ; "<<:"        	
     ; "/<<:"        	
     ; "/:"        	
     ; "/="        	
     ; ">="
     ; ".."
     ; "\\/"
     ; "^"        	
     ; "<-"        	
     ; "@"         
     ; "=="        	
     ; "==>"           
     ; "\\"        	
     ; "+"  
     ; "**"
     ; "mod"
     ; "+->"        	
     ; "<->"        	
     ; "-->"        	
     ; ">+>"        	
     ; ">->"        	
     ; "+->>"        	
     ; "-->>"        	
     ; ">->>"        	
     ; "<--"        	
     ; ":="        	
     ; "::="        	
 ] 
    ;; 

let table_Postfix = 
  [
   "~"  
 ]
;;

type affixe = 
    Prefix 
  | Infix 
  | Postfix 
;;

module Op_order =
  struct
    type  t = string
    let compare = compare
  end
;;

module Affixe_table =
  Map.Make(Op_order)
;;


let affixe_table = 
  let cons_table info table list_fix= 
    List.fold_right 
      (fun key tbl -> Affixe_table.add key info tbl)
      list_fix
      table
  in
  let prefix = cons_table Prefix  Affixe_table.empty table_Prefix
  in
  let infix_prefix = cons_table Infix prefix table_Infix
  in
  cons_table Postfix infix_prefix table_Postfix
;;

let rec find funname  =
  Affixe_table.find funname affixe_table
         ;;

