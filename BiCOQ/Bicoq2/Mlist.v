(*☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   Project: BiCoq2, formalisation of the B theory into the Coq system
   Section: Mlist, lists
   Lists are defined as a polymorphic type; they are used in the project to implement
   proof environment (set of hypothesis) and list goals for the prover.  Definitions for
   mapping of properties or functions to lists are provided, with some interesting
   results about preservation of implementation principle.
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : October 2005 - September 2006
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

Section Blist.

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Require Import Mbool.

(* Variables to be provided ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* Dom is the type of the list elements; fdequ is a boolean function deciding equality, *)
(* and impdequ is the proof that fdequ implements equality on Dom.                                      *)

 Variable Dom:Set.
 Variable fdequ:Dom->Dom->☆.
 Variable impdequ:(identity (A:=Dom)☵☨☂fdequ).

(* Equality properties ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition decdequ:=decimp2 impdequ.
 Definition dequtp:=imp2tp impdequ.
 Definition dequfr:=imp2fr impdequ.
 Definition dequpt:=imp2pt impdequ.
 Definition dequrf:=imp2rf impdequ.

(* List definition ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Inductive Blst:Set:=
  | lemp : Blst
  | lins : Blst->Dom->Blst.
 Notation "∅":=lemp.
 Infix "∙":=lins (at level 50).

(* Equality function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Fixpoint flequ(L1 L2:Blst){struct L1}:☆:=
  match L1 with
  | ∅ => match L2 with ∅ => ⊤ | _ => ⊥ end
  | L1'∙d1 => match L2 with ∅ => ⊥ | L2'∙d2 => (fdequ d1 d2) ∧☁ (flequ L1' L2') end
  end.

 Theorem refllequ:forall (L:Blst), flequ L L≡ ⊤.
 Proof.
  induction L; simpl; [apply refl_id | rewrite (dequpt _ _ (refl_id d)); apply IHL].
 Qed.

 Theorem implequ:(identity (A:=Blst)☵☨☂flequ).
 Proof.
  unfold Implem2; simpl; intros L1 L2; split; generalize L1 L2; clear L1 L2; induction L1;
   induction L2; simpl; intros H.
   apply refl_id.
   invset_clear H; discriminate H0.
   invset_clear H; discriminate H0.
   destruct (dedfbandt _ _ H); rewrite (IHL1 _ i0); rewrite (dequtp _ _ i); apply refl_id.
   invset_clear H; discriminate H0.
   intros H0; invset H0.
   intros H0; invset H0.
   intros H0; invset H0; rewrite <- H2 in H; rewrite <- H3 in H;
    rewrite <- (sym_id (dequpt _ _ (refl_id d))) in H; rewrite <- (sym_id (refllequ L1)) in H;
    invset_clear H.
 Qed.

 Definition declequ:=decimp2 implequ.
 Definition lequtp:=imp2tp implequ.
 Definition lequfr:=imp2fr implequ.
 Definition lequpt:=imp2pt implequ.
 Definition lequrf:=imp2rf implequ.

(* Membership ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Inductive lin:Dom->Blst->Set:=
  | liyes: forall (d:Dom)(L:Blst), lin d (L∙d)
  | linxt: forall (d1 d2:Dom)(L:Blst), lin d1 L->lin d1 (L∙d2).
 Infix "∊":=lin (at level 50).

 (* Tactic to prove d∊(L∙...∙d∙...) *)
 Ltac BTin:=repeat (apply liyes || apply linxt).

 Lemma linemp:forall (L:Blst), (forall (d:Dom), !(d∊L))->L≡∅.
 Proof.
  induction L; intros H; [apply refl_id | elim (H d); apply liyes].
 Qed.

 Lemma caselin:forall (d1 d2:Dom) (L:Blst), d1∊(L∙d2)->(d1≡d2)+(d1∊L).
 Proof.
  intros d1 d2 L H; invset H; [left; apply refl_id | right; apply H2].
 Qed.

(* Membership function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Fixpoint flin(d:Dom)(L:Blst){struct L}:☆:=
  match L with
  | ∅ => ⊥
  | L'∙d' => match (fdequ d d') with ⊤ => ⊤ | ⊥ => flin d L' end
  end.
 Infix "∊☁":=flin (at level 50).

 Theorem implin:(lin☵☨☂flin).
 Proof.
  unfold Implem2; intros d L; split; intros H; induction L; simpl in H.
  invset_clear H; discriminate H0.
  destruct (decdequ d d0);
   [rewrite i; apply liyes
   | rewrite <- (sym_id (dequrf _ _ e)) in H; apply linxt; apply (IHL H)].
  intros H0; invset H0.
  intros H0; destruct (caselin _ _ _ H0);
   [rewrite <- (sym_id i) in H; rewrite <- (sym_id (dequpt _ _ (refl_id d0))) in H; invset H;
     discriminate H1
   | destruct (fdequ d d0); [invset_clear H; discriminate H1 | apply (IHL H l)]].
 Qed.

 Definition declin:=decimp2 implin.
 Definition linpt:=imp2pt implin.
 Definition linrf:=imp2rf implin.
 Definition lintp:=imp2tp implin.
 Definition linfr:=imp2fr implin.

(* Property mapping ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* Property mapping is the conversion of a property over Dom to a property over lists   *)
(* which holds iff the property holds for any member of the list.                                         *)

 Definition lpmap1(P:Dom->Set):Blst->Set:=fun (L:Blst)=>forall (d:Dom), d∊L->P d.

 Definition lpmap2(A:Set)(P:A->Dom->Set):A->Blst->Set:=
  fun (a:A)(L:Blst)=>forall (d:Dom), (d∊L->P a d).
 Implicit Arguments lpmap2 [A].

 Definition lpmap3(A B:Set)(P:A->B->Dom->Set):A->B->Blst->Set:=
  fun (a:A)(b:B)(L:Blst)=> forall (d:Dom), (d∊L->P a b d).
 Implicit Arguments lpmap3 [A B].

(* Functional mapping ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* Functional mapping is the conversion of boolean function over Dom to a boolean         *)
(* function over lists which is true iff the function is true for all elements of the    *)
(* list.                                                                                                                                                *)

 Definition lfmap1(f:Dom->☆):Blst->☆:=
  fix m (L:Blst):=match L with ∅ => ⊤ | L'∙d' => match (f d') with ⊤ => m L' | ⊥ => ⊥ end end.

 Theorem lfmap1for:forall (f:Dom->☆)(L:Blst), lfmap1 f L≡⊤->forall (d:Dom), d∊L->f d≡⊤.
 Proof.
  intros f L H d H0; induction L.
   invset H0.
   destruct (caselin _ _ _ H0); clear H0.
    rewrite <- i in H; simpl in H; destruct (f d);
     [apply refl_id | invset H; discriminate H0].
    simpl in H; destruct (f d0); [apply (IHL H l) | invset H; discriminate H0].
 Qed.

 Theorem lfmap1exs:forall (f:Dom->☆)(L:Blst), lfmap1 f L≡⊥->{d:Dom & (d∊L)*(f d≡⊥)}.
 Proof.
  intros f L H; induction L.
   invset H; discriminate H0.
   simpl in H; destruct (ElimB1 f d); rewrite <- (sym_id i) in H.
    destruct (IHL H); exists x; split; destruct p; [apply linxt; apply l | apply i0].
    exists d; split; [apply liyes | apply i].
 Qed.

 Theorem lmap1imp:forall (P:Dom->Set)(f:Dom->☆),(P☵☧☂f)->(lpmap1 P☵☧☂lfmap1 f).
 Proof.
  intros P f; intros H L; unfold Implem1; split; unfold lpmap1; intros H0.
   intros d H1; apply (imp1tp H _ (lfmap1for f L H0 d H1)).
   intros H1; destruct (lfmap1exs _ _ H0); destruct p; apply (imp1fr H x i); apply H1; apply l.
 Qed.
 Implicit Arguments lmap1imp [P f].

 Definition lfmap2(A:Set)(f:A->Dom->☆):A->Blst->☆:=
  fun (a:A)=>
   fix m (L:Blst):=
    match L with ∅ => ⊤ | L'∙d' => match (f a d') with ⊤ => m L' | ⊥ => ⊥ end end.
 Implicit Arguments lfmap2 [A].

 Theorem lfmap2for:forall (A:Set)(a:A)(f:A->Dom->☆)(L:Blst),
                                   lfmap2 f a L≡⊤->forall (d:Dom), d∊L->f a d≡⊤.
 Proof.
  intros A a f L H d H0; induction L.
   invset H0.
   destruct (caselin _ _ _ H0); clear H0.
    rewrite <- i in H; simpl in H; destruct (f a d);
     [apply refl_id | invset H; discriminate H0].
    simpl in H; destruct (f a d0); [apply (IHL H l) | invset H; discriminate H0].
 Qed.
 Implicit Arguments lfmap2for [A].

 Theorem lfmap2exs:forall (A:Set)(a:A)(f:A->Dom->☆)(L:Blst),
                                   lfmap2 f a L≡⊥->{d:Dom & (d∊L)*(f a d≡⊥)}.
 Proof.
  intros A a f L H; induction L.
   invset H; discriminate H0.
   simpl in H; destruct (ElimB2 f a d); rewrite <- (sym_id i) in H.
    destruct (IHL H); exists x; split; destruct p; [apply linxt; apply l | apply i0].
    exists d; split; [apply liyes | apply i].
 Qed.
 Implicit Arguments lfmap2exs [A].

 Theorem lmap2imp:forall (A:Set)(P:A->Dom->Set)(f:A->Dom->☆), (P☵☨☂f)->(lpmap2 P☵☨☂lfmap2 f).
 Proof.
  intros A P f; intros H a L; unfold Implem2; split; unfold lpmap2; intros H0.
   intros d H1; apply (imp2tp H _ _ (lfmap2for _ f L H0 d H1)).
   intros H1; destruct (lfmap2exs _ _ _ H0); destruct p; apply (imp2fr H _ x i); apply H1;
    apply l.
 Qed.
 Implicit Arguments lmap2imp [A P f].

 Definition lfmap3(A B:Set)(f:A->B->Dom->☆):A->B->Blst->☆:=
  fun (a:A)(b:B)=>
   fix m (L:Blst):=match L with
                             | ∅ => ⊤
                             | L'∙d' => match (f a b d') with ⊤ => m L' | ⊥ => ⊥ end
                             end.
 Implicit Arguments lfmap3 [A B].

 Theorem lfmap3for:forall (A B:Set)(a:A)(b:B)(f:A->B->Dom->☆)(L:Blst),
                                   lfmap3 f a b L≡⊤->forall (d:Dom), d∊L->f a b d≡⊤.
 Proof.
  intros A B a b f L H d H0; induction L.
   invset H0.
   destruct (caselin _ _ _ H0); clear H0.
    rewrite <- i in H; simpl in H; destruct (f a b d);
    [apply refl_id | invset H; discriminate H0].
    simpl in H; destruct (f a b d0); [apply (IHL H l) | invset H; discriminate H0].
 Qed.
 Implicit Arguments lfmap3for [A B].

 Theorem lfmap3exs:forall (A B:Set)(a:A)(b:B)(f:A->B->Dom->☆)(L:Blst),
                                   lfmap3 f a b L≡⊥->{d:Dom & (d∊L)*(f a b d≡⊥)}.
 Proof.
  intros A B a b f L H; induction L.
   invset H; discriminate H0.
   simpl in H; destruct (ElimB3 f a b d); rewrite <- (sym_id i) in H.
    destruct (IHL H); exists x; split; destruct p; [apply linxt; apply l | apply i0].
    exists d; split; [apply liyes | apply i].
 Qed.
 Implicit Arguments lfmap3exs [A B].

 Theorem lmap3imp:forall (A B:Set)(P:A->B->Dom->Set)(f:A->B->Dom->☆),
                                 (P☵☩☂f)->(lpmap3 P☵☩☂lfmap3 f).
 Proof.
  intros A B P f; intros H a b L; unfold Implem3; split; unfold lpmap3; intros H0.
   intros d H1; apply (imp3tp H _ _ _ (lfmap3for _ _ f L H0 d H1)).
   intros H1; destruct (lfmap3exs _ _ _ _ H0); destruct p; apply (imp3fr H _ _ x i);
    apply H1; apply l.
 Qed.
 Implicit Arguments lmap3imp [A B P f].

(* Inclusion ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* Inclusion is defined here as the mapping of membership...                                                  *)

 Definition linc:=fun L1 L2:Blst=>lpmap2 (fun (L:Blst)(d:Dom)=>d∊L) L2 L1.
 Infix "⊆":=linc (at level 50).

 Lemma refllinc:forall (L:Blst), L⊆L.
 Proof.
  intros L; unfold linc; intros d H; apply H.
 Qed.

 Lemma translinc:forall (L1 L2 L3:Blst), L1⊆L2->L2⊆L3->L1⊆L3.
 Proof.
  intros L1 L2 L3 H H0; unfold linc; intros d H1; apply (H0 _ (H _ H1)).
 Qed.

 Lemma emplinc:forall (L:Blst), ∅⊆L.
 Proof.
  intros L; unfold linc; intros d H; invset H.
 Qed.

 Lemma lincover:forall (d:Dom) (L1 L2:Blst), L1⊆L2->L1⊆(L2∙d).
 Proof.
  intros d L1 L2 H; unfold linc; intros d0 H0; apply linxt; apply (H _ H0).
 Qed.

 Lemma lincemp:forall (L:Blst), L⊆∅->L≡∅.
 Proof.
  induction L; intros H;
   [apply refl_id | generalize (H d (liyes d L)); intros H0; invset H0].
 Qed.

 Lemma lincsub:forall (d:Dom) (L1 L2:Blst), (L1∙d)⊆L2->L1⊆L2.
 Proof.
  intros d L1 L2 H; apply (translinc _ _ _ (lincover _ _ _ (refllinc _)) H).
 Qed.

 Lemma lincdbl:forall (d:Dom) (L1 L2:Blst), L1⊆L2->(L1∙d)⊆(L2∙d).
 Proof.
  intros d L1 L2 H; unfold linc; intros d0 H0; destruct (decdequ d0 d);
   [rewrite i; apply liyes
   | apply linxt; apply H; invset H0; [destruct e; apply H4 | apply H3]].
 Qed.

 Lemma lincswp:forall (d1 d2:Dom) (L1 L2:Blst), (L1∙d1)∙d2⊆L2->(L1∙d2)∙d1⊆L2.
 Proof.
  intros d1 d2 L1 L2 H; unfold linc; intros d0 H0; apply H; invset H0;
   [BTin | invset H3; [apply liyes | apply linxt; apply linxt; apply H7]].
 Qed.

 Lemma lincnot:forall (L1 L2:Blst), !(L1⊆L2)->{d:Dom & (d∊L1)*!(d∊L2)}.
 Proof.
  induction L1; intros L2 H.
   destruct H; apply emplinc.
   destruct (declin d L2).
   assert (H0:!(L1⊆L2)).
    unfold linc; unfold lpmap2; intros H0; apply H; unfold linc; unfold lpmap2;
     intros d0 H1; destruct (caselin _ _ _ H1); [rewrite i; apply l | apply (H0 d0 l0)].
   destruct (IHL1 _ H0); destruct p; exists x; split; [apply linxt; apply l0 | apply e].
   exists d; split; [apply liyes | apply e].
 Qed.

(* Inclusion function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* As for ⊆, the inclusion function is defined by mapping, and allow to automatically    *)
(* inherit of the implementation principle.                                                                              *)

 Definition flinc:=fun L1 L2:Blst=>lfmap2 (fun (L:Blst)(d:Dom)=>d∊☁L) L2 L1.
 Notation "⊆☁":=flinc (at level 50).

 Definition implinc:=lmap2imp (implemswap21 implin).
 Definition declinc:=decimp2 implinc.
 Definition lincpt:=imp2pt implinc.
 Definition lincrf:=imp2rf implinc.
 Definition linctp:=imp2tp implinc.
 Definition lincfr:=imp2fr implinc.

(* Remove element function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* All occurrence of an element are removed from the list.                                                     *)

 Fixpoint flrem(L:Blst)(d:Dom){struct L}:Blst:=
  match L with
  | ∅ => ∅
  | L'∙d' => match fdequ d d' with ⊤ => (flrem L' d) | ⊥ => (flrem L' d)∙d' end
  end.

 Theorem flremin:forall (L:Blst)(d:Dom), !(d∊(flrem L d)).
 Proof.
  induction L; simpl; intros d0 H.
   invset H.
   destruct (decdequ d0 d).
    rewrite <- (sym_id (dequpt _ _ i)) in H; apply (IHL _ H).
    rewrite <- (sym_id (dequrf _ _ e)) in H; generalize (caselin _ _ _ H); clear H; intros H;
     destruct H; [contradiction | apply (IHL _ l)].
 Qed.

 Theorem flreminc:forall (L:Blst)(d:Dom), (flrem L d)⊆L.
 Proof.
  induction L; intros d0.
    apply emplinc.
    unfold flrem; fold flrem; destruct (decdequ d0 d);
     [rewrite (dequpt _ _ i); apply lincover | rewrite (dequrf _ _ e); apply lincdbl];
     apply IHL.
 Qed.

 Theorem inflrem:forall (L:Blst)(d1 d2:Dom), d1∊L->!(d2≡d1)->d1∊(flrem L d2).
 Proof.
  induction L; intros d1 d2 H H0.
   invset H.
   generalize (caselin _ _ _ H); clear H; intros H; destruct H.
    rewrite (sym_id i); clear i; unfold flrem; fold flrem.
     rewrite (dequrf _ _ H0); apply liyes.
     simpl; destruct (decdequ d2 d);
      [rewrite (dequpt _ _ i) | rewrite (dequrf _ _ e); apply linxt]; apply (IHL _ _ l H0).
 Qed.

 Theorem linceqrem:forall (L1 L2:Blst), L1⊆L2->(L2⊆L1)+{d:Dom & L1⊆(flrem L2 d)}.
 Proof.
  intros L1 L2 H; destruct (declinc L1 L2).
   left; apply l.
   right; generalize (lincnot _ _ e); intros H0; destruct H0; destruct p; exists x;
    unfold linc; unfold lpmap2; intros d H2; destruct (decdequ x d).
    rewrite <- (sym_id i) in e0; contradiction.
    apply inflrem; [apply H; apply H2 | apply e1].
 Qed.

(* Append function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Fixpoint flapp(L1 L2:Blst){struct L1}:Blst:=
  match L1 with ∅ => L2 | L1'∙d' => flapp L1' (L2∙d') end.

 Theorem flappincr:forall (L1 L2:Blst), L2⊆flapp L1 L2.
 Proof.
  induction L1; intros L2; unfold flapp; fold flapp;
   [apply refllinc | intros x Hx; apply IHL1; apply linxt; apply Hx].
 Qed.

 Theorem flappincl:forall (L1 L2:Blst), L1⊆flapp L1 L2.
 Proof.
  induction L1; intros L2; unfold flapp; fold flapp.
   apply emplinc.
   intros x Hx; destruct (caselin _ _ _ Hx);
    [rewrite i; apply flappincr; apply liyes | apply IHL1; apply l].
 Qed.

 Theorem flappin:forall (L1 L2:Blst)(d:Dom), d∊(flapp L1 L2)->(d∊L1)+(d∊L2).
 Proof.
  induction L1; simpl; intros L2 d0 H.
   right; apply H.
   destruct (IHL1 _ _ H).
    left; apply linxt; apply l.
    destruct (caselin _ _ _ l); [rewrite i; left; apply liyes | right; apply l0].
 Qed.

 Theorem flappnin:forall (L1 L2:Blst)(d:Dom), !(d∊(flapp L1 L2))->!(d∊L1)*!(d∊L2).
 Proof.
  intros L1 L2 d H; split; intros H0; apply H; [apply flappincl | apply flappincr];
   apply H0.
 Qed.

End Blist.

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)
