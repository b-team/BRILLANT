 

Require Import Blib.

Theorem op:
forall _CAP _DT _M1 _M2 _N1 _N2 _NB_PUMP _PNOMINAL _PUMP1 _PUMP2 _PUMP3 _PUMP4 _U1 _U2 _WMAX, ( 
 (In BN1 _N1) -> 
(In BN1 _N2) -> 
(In BN1 _M1) -> 
(In BN1 _M2) -> 
(In BN1 _CAP) -> 
(_N1 < _N2) -> 
(_M1 < _M2) -> 
(_M1 < _N1) -> 
(_N2 < _M2) -> 
(_M2 < _CAP) -> 
(In BN _NB_PUMP) -> 
(_NB_PUMP = 4) -> 
(In BN _PUMP1) -> 
(In BN _PUMP2) -> 
(In BN _PUMP3) -> 
(In BN _PUMP4) -> 
(_PUMP1 = 1) -> 
(_PUMP2 = 2) -> 
(_PUMP3 = 3) -> 
(_PUMP4 = 4) -> 
(In (interval 1 _NB_PUMP) _PUMP1) -> 
(In (interval 1 _NB_PUMP) _PUMP2) -> 
(In (interval 1 _NB_PUMP) _PUMP3) -> 
(In (interval 1 _NB_PUMP) _PUMP4) -> 
(In BN1 _WMAX) -> 
(In BN1 _U1) -> 
(In BN1 _U2) -> 
(In BN1 _DT) -> 
(In BN1 _PNOMINAL) -> 
(In BN1 (((_PNOMINAL + _PNOMINAL) + _PNOMINAL) + _PNOMINAL)) -> 
(In BN1 ((((_PNOMINAL + _PNOMINAL) + _PNOMINAL) + _PNOMINAL) * _DT)) -> 
(In BN1 (_U2 * _DT)) -> 
(In BN1 (_U1 * _DT)) -> 
(In BN1 ((_U1 * _DT) + _WMAX)) -> 
((_U2 * _DT) <= _WMAX) -> 
(In BN1 (2 * _WMAX)) -> 
(In BN1 (_WMAX * _DT)) -> 
(In BN1 ((_U1 * _DT) * _DT)) -> 
(In BN1 ((_U2 * _DT) * _DT)) -> 
(In BN1 ((((_U1 * _DT) * _DT) + 1) / 2)) -> 
(In BN1 ((((_U2 * _DT) * _DT) + 1) / 2)) -> 
(In BN1 ((4 * _PNOMINAL) + ((1 + ((_U2 * _DT) * _DT)) / 2))) -> 
(In BN1 ((_CAP + ((4 * _PNOMINAL) * _DT)) + ((1 + ((_U2 * _DT) * _DT)) / 2))) -> 
(In BZ (((1 + ((_U2 * _DT) * _DT)) / 2) - (_WMAX * _DT))) -> 
(In BN1 (_CAP + ((4 * _PNOMINAL) * _DT))) -> 
(In BZ (- ((_WMAX * _DT) + ((1 + ((_U2 * _DT) * _DT)) / 2))))
  -> (
 forall _acq_vr_fresh_0000, ((((In BN _acq_vr_fresh_0000) -> forall _acq_srm_fresh_0000, ((((In BBOOL _acq_srm_fresh_0000) -> forall _acq_sfam_fresh_0000, ((((In BBOOL _acq_sfam_fresh_0000) -> forall _acq_sim_fresh_0000, ((((In BBOOL _acq_sim_fresh_0000) -> forall _acq_qr_fresh_0000, ((((In BN _acq_qr_fresh_0000) -> forall _acq_lrm_fresh_0000, ((((In BBOOL _acq_lrm_fresh_0000) -> forall _acq_lfam_fresh_0000, ((((In BBOOL _acq_lfam_fresh_0000) -> forall _acq_lim_fresh_0000, ((((In BBOOL _acq_lim_fresh_0000) -> (In BN _acq_qr_fresh_0000))))))))))))))))))))))))) ) )
 .
try intuition 
   || unfold BBOOL ; intuition ; apply Full_intro
   || red ;  red ; red ; omega.
Qed.
 