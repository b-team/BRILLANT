(*************************************************************)
(*                PROJET BCAML                               *)
(*                                                           *)
(* http://www3.inrets.fr/Public/ESTAS/Mariano.Georges/Bcaml/ *)
(*                                                           *)
(*                   IRIT                                    *)
(*                                                           *)
(*   source genere le 14-05-2001  09:04:43                   *)
(*************************************************************)

open Blast
let map_amn_string f (s:string) = s
let map_amn_int f (i:int32) = i
let map_amn_bool f (b:bool) = b
let map_amn_option f = function Some x -> Some (f x) | None -> None
let rec map_amn_amn f  = fun _x -> f((function
    Machine(_x1, _x2) -> Machine((map_amn_head f _x1), ((List.map (map_amn_clause f)) _x2))
  | Refinement(_x1, _x2) -> Refinement((map_amn_head f _x1), ((List.map (map_amn_clause f)) _x2))
  | Implementation(_x1, _x2) -> Implementation((map_amn_head f _x1), ((List.map (map_amn_clause f)) _x2))
  | EmptyTree -> EmptyTree )_x)
  and map_amn_head f  = fun (_x1, _x2) -> (map_amn_id f _x1), (map_amn_ids f _x2)
  and map_amn_clause f  = function
    Definitions(_x1) -> Definitions(((List.map (map_amn_def f)) _x1))
  | Constraints(_x1) -> Constraints((map_amn_predicate f _x1))
  | Invariant(_x1) -> Invariant((map_amn_predicate f _x1))
  | Sets(_x1) -> Sets(((List.map (map_amn_set f)) _x1))
  | Initialisation(_x1) -> Initialisation((map_amn_substitution f _x1))
  | ConstantsConcrete(_x1) -> ConstantsConcrete((map_amn_ids f _x1))
  | ConstantsAbstract(_x1) -> ConstantsAbstract((map_amn_ids f _x1))
  | ConstantsHidden(_x1) -> ConstantsHidden((map_amn_ids f _x1))
  | Properties(_x1) -> Properties((map_amn_predicate f _x1))
  | Values(_x1) -> Values(((List.map (fun (_x1, _x2) -> ((map_amn_id f _x1), (map_amn_expr f _x2)))) _x1))
  | VariablesConcrete(_x1) -> VariablesConcrete((map_amn_ids f _x1))
  | VariablesAbstract(_x1) -> VariablesAbstract((map_amn_ids f _x1))
  | VariablesHidden(_x1) -> VariablesHidden((map_amn_ids f _x1))
  | Promotes(_x1) -> Promotes((map_amn_ids f _x1))
  | Assertions(_x1) -> Assertions(((List.map (map_amn_predicate f)) _x1))
  | Operations(_x1) -> Operations(((List.map (map_amn_operation f)) _x1))
  | Sees(_x1) -> Sees((map_amn_ids f _x1))
  | Uses(_x1) -> Uses((map_amn_ids f _x1))
  | Extends(_x1) -> Extends(((List.map (map_amn_instance f)) _x1))
  | Includes(_x1) -> Includes(((List.map (map_amn_instance f)) _x1))
  | Imports(_x1) -> Imports(((List.map (map_amn_instance f)) _x1))
  | Refines(_x1) -> Refines((map_amn_id f _x1))
  and map_amn_def f  = function
    Def(_x1, _x2, _x3) -> Def((map_amn_id f _x1), (map_amn_ids f _x2), (map_amn_defBody f _x3))
  | FileDef(_x1) -> FileDef((map_amn_string f _x1))
  and map_amn_defBody f  = function
    | ExprDefBody(_x1) -> ExprDefBody((map_amn_expr f _x1))
    | SubstDefBody(_x1) -> SubstDefBody((map_amn_substitution f _x1))
  and map_amn_set f  = function
    SetAbstDec(_x1) -> SetAbstDec((map_amn_id f _x1))
  | SetEnumDec(_x1, _x2) -> SetEnumDec((map_amn_id f _x1), (map_amn_ids f _x2))
  and map_amn_instance f  = fun (_x1, _x2) -> (map_amn_id f _x1), ((List.map (map_amn_expr f)) _x2)
  and map_amn_type_B f  = function
    TypeNatSetRange(_x1, _x2) -> TypeNatSetRange((map_amn_expr f _x1), (map_amn_expr f _x2))
  | PredType(_x1) -> PredType((map_amn_expr f _x1))
  | TypeIdentifier(_x1) -> TypeIdentifier((map_amn_id f _x1))
  | PFunType(_x1, _x2) -> PFunType((map_amn_type_B f _x1), (map_amn_type_B f _x2))
  | FunType(_x1, _x2) -> FunType((map_amn_type_B f _x1), (map_amn_type_B f _x2))
  | ProdType(_x1, _x2) -> ProdType((map_amn_type_B f _x1), (map_amn_type_B f _x2))
  | Sequence(_x1) -> Sequence((map_amn_type_B f _x1))
  | SubType(_x1, _x2) -> SubType((map_amn_type_B f _x1), (map_amn_expr f _x2))
  | Fin_B(_x1) -> Fin_B((map_amn_type_B f _x1))
  | Untyped -> Untyped 
  and map_amn_field f  = fun (_x1, _x2) -> (map_amn_id f _x1), (map_amn_type_B f _x2)
  and map_amn_predicate f  = function
    PredParen(_x1) -> PredParen((map_amn_predicate f _x1))
  | PredNegation(_x1) -> PredNegation((map_amn_predicate f _x1))
  | PredExists(_x1, _x2) -> PredExists(((List.map (map_amn_id f)) _x1), (map_amn_predicate f _x2))
  | PredForAll(_x1, _x2) -> PredForAll(((List.map (map_amn_id f)) _x1), (map_amn_predicate f _x2))
  | PredBin(_x1, _x2, _x3) -> PredBin(_x1, (map_amn_predicate f _x2), (map_amn_predicate f _x3))
  | PredAtom(_x1, _x2, _x3) -> PredAtom(_x1, (map_amn_expr f _x2), (map_amn_expr f _x3))
  and map_amn_operation f  = fun (_x1, _x2, _x3, _x4) -> (map_amn_id f _x1), (map_amn_ids f _x2), (map_amn_ids f _x3), (map_amn_substitution f _x4)
  and map_amn_expr f  = function
    ExprSequence(_x1, _x2) -> ExprSequence((map_amn_expr f _x1), (map_amn_expr f _x2))
  | ExprParen(_x1) -> ExprParen((map_amn_expr f _x1))
  | ExprId(_x1) -> ExprId((map_amn_id f _x1))
  | ExprFunCall(_x1, _x2) -> ExprFunCall((map_amn_expr f _x1), ((List.map (map_amn_expr f)) _x2))
  | ExprBin(_x1, _x2, _x3) -> ExprBin(_x1, (map_amn_expr f _x2), (map_amn_expr f _x3))
  | ExprUn(_x1, _x2) -> ExprUn(_x1, (map_amn_expr f _x2))
  | ExprSeq(_x1) -> ExprSeq((map_amn_exprSeq f _x1))
  | ExprSet(_x1) -> ExprSet((map_amn_exprSet f _x1))
  | RelSet(_x1, _x2) -> RelSet((map_amn_expr f _x1), (map_amn_expr f _x2))
  | RelSeqComp(_x1) -> RelSeqComp(((List.map (map_amn_expr f)) _x1))
  | ExprNumber(_x1) -> ExprNumber((map_amn_number f _x1))
  | ExprBool(_x1) -> ExprBool((map_amn_bool f _x1))
  | ExprString(_x1) -> ExprString((map_amn_string f _x1))
  | ExprPred(_x1) -> ExprPred((map_amn_predicate f _x1))
  | ExprNuplet(_x1) -> ExprNuplet(((List.map (map_amn_expr f)) _x1))
  | ExprSIGMA(_x1, _x2, _x3) -> ExprSIGMA(((List.map (map_amn_id f)) _x1), (map_amn_predicate f _x2), (map_amn_expr f _x3))
  | ExprPI(_x1, _x2, _x3) -> ExprPI(((List.map (map_amn_id f)) _x1), (map_amn_predicate f _x2), (map_amn_expr f _x3))
  | ExprLambda(_x1, _x2, _x3) -> ExprLambda(((List.map (map_amn_id f)) _x1), (map_amn_predicate f _x2), (map_amn_expr f _x3))
  and map_amn_exprSeq f  = function
    SeqEnum(_x1) -> SeqEnum(((List.map (map_amn_expr f)) _x1))
  | SeqEmpty -> SeqEmpty 
  and map_amn_exprSet f  = function
    SetRange(_x1, _x2) -> SetRange((map_amn_expr f _x1), (map_amn_expr f _x2))
  | SetEnum(_x1) -> SetEnum(((List.map (map_amn_expr f)) _x1))
  | SetCompr(_x1) -> SetCompr(((List.map (map_amn_expr f)) _x1))
  | SetComprPred(_x1, _x2) -> SetComprPred(((List.map (map_amn_id f)) _x1), (map_amn_predicate f _x2))
  | SetEmpty -> SetEmpty 
  | SetUnionQuantify(_x1, _x2, _x3) -> SetUnionQuantify(((List.map (map_amn_id f)) _x1), (map_amn_predicate f _x2), (map_amn_expr f _x3))
  | SetInterQuantify(_x1, _x2, _x3) -> SetInterQuantify(((List.map (map_amn_id f)) _x1), (map_amn_predicate f _x2), (map_amn_expr f _x3))
  and map_amn_number f  = function
    MinNumber -> MinNumber 
  | Number(_x1) -> Number((map_amn_int f _x1))
  | MaxNumber -> MaxNumber 
  and map_amn_substitution f  = function
    SubstOperCall(_x1, _x2, _x3) -> SubstOperCall((map_amn_ids f _x1), (map_amn_id f _x2), ((List.map (map_amn_expr f)) _x3))
  | SubstBlock(_x1) -> SubstBlock((map_amn_substitution f _x1))
  | SubstPrecondition(_x1, _x2) -> SubstPrecondition((map_amn_predicate f _x1), (map_amn_substitution f _x2))
  | SubstAssertion(_x1, _x2) -> SubstAssertion((map_amn_predicate f _x1), (map_amn_substitution f _x2))
  | SubstChoice(_x1) -> SubstChoice(((List.map (map_amn_substitution f)) _x1))
  | SubstIf(_x1, _x2) -> SubstIf(((List.map (fun (_x1, _x2) -> ((map_amn_predicate f _x1), (map_amn_substitution f _x2)))) _x1), ((map_amn_option (map_amn_substitution f)) _x2))
  | SubstSelect(_x1, _x2) -> SubstSelect(((List.map (fun (_x1, _x2) -> ((map_amn_predicate f _x1), (map_amn_substitution f _x2)))) _x1), ((map_amn_option (map_amn_substitution f)) _x2))
  | SubstCase(_x1, _x2, _x3) -> SubstCase((map_amn_expr f _x1), ((List.map (fun (_x1, _x2) -> (((List.map (map_amn_expr f)) _x1), (map_amn_substitution f _x2)))) _x2), ((map_amn_option (map_amn_substitution f)) _x3))
  | SubstAny(_x1, _x2, _x3) -> SubstAny(((List.map (map_amn_id f)) _x1), (map_amn_predicate f _x2), (map_amn_substitution f _x3))
  | SubstLet(_x1, _x2, _x3) -> SubstLet(((List.map (map_amn_id f)) _x1), (map_amn_predicate f _x2), (map_amn_substitution f _x3))
  | SubstVar(_x1, _x2) -> SubstVar(((List.map (map_amn_id f)) _x1), (map_amn_substitution f _x2))
  | SubstWhile(_x1, _x2, _x3, _x4) -> SubstWhile((map_amn_predicate f _x1), (map_amn_substitution f _x2), (map_amn_expr f _x3), (map_amn_predicate f _x4))
  | SubstSkip -> SubstSkip 
  | SubstSequence(_x1, _x2) -> SubstSequence((map_amn_substitution f _x1), (map_amn_substitution f _x2))
  | SubstParallel(_x1, _x2) -> SubstParallel((map_amn_substitution f _x1), (map_amn_substitution f _x2))
  | SubstSetEqualIds(_x1, _x2) -> SubstSetEqualIds((map_amn_ids f _x1), ((List.map (map_amn_expr f)) _x2))
  | SubstSetEqualFun(_x1, _x2, _x3) -> SubstSetEqualFun((map_amn_expr f _x1), ((List.map (map_amn_expr f)) _x2), (map_amn_expr f _x3))
  | SubstBecomeSuch(_x1, _x2) -> SubstBecomeSuch((map_amn_ids f _x1), (map_amn_predicate f _x2))
  | SubstSetIn(_x1, _x2) -> SubstSetIn((map_amn_ids f _x1), (map_amn_expr f _x2))
  and map_amn_ids f  = fun (_x1) -> ((List.map (map_amn_id f)) _x1)
  and map_amn_origine f  = function
    Local -> Local 
  | Remote(_x1) -> Remote(((List.map (fun (_x1, _x2) -> ((map_amn_string f _x1), (map_amn_string f _x2)))) _x1))
  | OrgRefine(_x1, _x2) -> OrgRefine((map_amn_string f _x1), ((List.map (fun (_x1, _x2) -> ((map_amn_string f _x1), (map_amn_string f _x2)))) _x2))
  | Prefix(_x1) -> Prefix(((List.map (map_amn_string f)) _x1))
  and map_amn_id f  = function
    Id(_x1, _x2, _x3, _x4) -> Id((map_amn_origine f _x1), (map_amn_string f _x2), ((map_amn_option (map_amn_type_B f)) _x3), _x4)

let map_head_string f (s:string) = s
let map_head_int f (i:int32) = i
let map_head_bool f (b:bool) = b
let map_head_option f = function Some x -> Some (f x) | None -> None
let rec map_head_amn f  = function
    Machine(_x1, _x2) -> Machine((map_head_head f _x1), ((List.map (map_head_clause f)) _x2))
  | Refinement(_x1, _x2) -> Refinement((map_head_head f _x1), ((List.map (map_head_clause f)) _x2))
  | Implementation(_x1, _x2) -> Implementation((map_head_head f _x1), ((List.map (map_head_clause f)) _x2))
  | EmptyTree -> EmptyTree 
  and map_head_head f  = fun _x -> f((fun (_x1, _x2) -> (map_head_id f _x1), (map_head_ids f _x2))_x)
  and map_head_clause f  = function
    Definitions(_x1) -> Definitions(((List.map (map_head_def f)) _x1))
  | Constraints(_x1) -> Constraints((map_head_predicate f _x1))
  | Invariant(_x1) -> Invariant((map_head_predicate f _x1))
  | Sets(_x1) -> Sets(((List.map (map_head_set f)) _x1))
  | Initialisation(_x1) -> Initialisation((map_head_substitution f _x1))
  | ConstantsConcrete(_x1) -> ConstantsConcrete((map_head_ids f _x1))
  | ConstantsAbstract(_x1) -> ConstantsAbstract((map_head_ids f _x1))
  | ConstantsHidden(_x1) -> ConstantsHidden((map_head_ids f _x1))
  | Properties(_x1) -> Properties((map_head_predicate f _x1))
  | Values(_x1) -> Values(((List.map (fun (_x1, _x2) -> ((map_head_id f _x1), (map_head_expr f _x2)))) _x1))
  | VariablesConcrete(_x1) -> VariablesConcrete((map_head_ids f _x1))
  | VariablesAbstract(_x1) -> VariablesAbstract((map_head_ids f _x1))
  | VariablesHidden(_x1) -> VariablesHidden((map_head_ids f _x1))
  | Promotes(_x1) -> Promotes((map_head_ids f _x1))
  | Assertions(_x1) -> Assertions(((List.map (map_head_predicate f)) _x1))
  | Operations(_x1) -> Operations(((List.map (map_head_operation f)) _x1))
  | Sees(_x1) -> Sees((map_head_ids f _x1))
  | Uses(_x1) -> Uses((map_head_ids f _x1))
  | Extends(_x1) -> Extends(((List.map (map_head_instance f)) _x1))
  | Includes(_x1) -> Includes(((List.map (map_head_instance f)) _x1))
  | Imports(_x1) -> Imports(((List.map (map_head_instance f)) _x1))
  | Refines(_x1) -> Refines((map_head_id f _x1))
  and map_head_def f  = function
    Def(_x1, _x2, _x3) -> Def((map_head_id f _x1), (map_head_ids f _x2), (map_head_defBody f _x3))
  | FileDef(_x1) -> FileDef((map_head_string f _x1))
  and map_head_defBody f  = function
    |  ExprDefBody(_x1) -> ExprDefBody((map_head_expr f _x1))
  | SubstDefBody(_x1) -> SubstDefBody((map_head_substitution f _x1))
  and map_head_set f  = function
    SetAbstDec(_x1) -> SetAbstDec((map_head_id f _x1))
  | SetEnumDec(_x1, _x2) -> SetEnumDec((map_head_id f _x1), (map_head_ids f _x2))
  and map_head_instance f  = fun (_x1, _x2) -> (map_head_id f _x1), ((List.map (map_head_expr f)) _x2)
  and map_head_type_B f  = function
    TypeNatSetRange(_x1, _x2) -> TypeNatSetRange((map_head_expr f _x1), (map_head_expr f _x2))
  | PredType(_x1) -> PredType((map_head_expr f _x1))
  | TypeIdentifier(_x1) -> TypeIdentifier((map_head_id f _x1))
  | PFunType(_x1, _x2) -> PFunType((map_head_type_B f _x1), (map_head_type_B f _x2))
  | FunType(_x1, _x2) -> FunType((map_head_type_B f _x1), (map_head_type_B f _x2))
  | ProdType(_x1, _x2) -> ProdType((map_head_type_B f _x1), (map_head_type_B f _x2))
  | Sequence(_x1) -> Sequence((map_head_type_B f _x1))
  | SubType(_x1, _x2) -> SubType((map_head_type_B f _x1), (map_head_expr f _x2))
  | Fin_B(_x1) -> Fin_B((map_head_type_B f _x1))
  | Untyped -> Untyped 
  and map_head_field f  = fun (_x1, _x2) -> (map_head_id f _x1), (map_head_type_B f _x2)
  and map_head_predicate f  = function
    PredParen(_x1) -> PredParen((map_head_predicate f _x1))
  | PredNegation(_x1) -> PredNegation((map_head_predicate f _x1))
  | PredExists(_x1, _x2) -> PredExists(((List.map (map_head_id f)) _x1), (map_head_predicate f _x2))
  | PredForAll(_x1, _x2) -> PredForAll(((List.map (map_head_id f)) _x1), (map_head_predicate f _x2))
  | PredBin(_x1, _x2, _x3) -> PredBin(_x1, (map_head_predicate f _x2), (map_head_predicate f _x3))
  | PredAtom(_x1, _x2, _x3) -> PredAtom(_x1, (map_head_expr f _x2), (map_head_expr f _x3))
  and map_head_operation f  = fun (_x1, _x2, _x3, _x4) -> (map_head_id f _x1), (map_head_ids f _x2), (map_head_ids f _x3), (map_head_substitution f _x4)
  and map_head_expr f  = function
    ExprSequence(_x1, _x2) -> ExprSequence((map_head_expr f _x1), (map_head_expr f _x2))
  | ExprParen(_x1) -> ExprParen((map_head_expr f _x1))
  | ExprId(_x1) -> ExprId((map_head_id f _x1))
  | ExprFunCall(_x1, _x2) -> ExprFunCall((map_head_expr f _x1), ((List.map (map_head_expr f)) _x2))
  | ExprBin(_x1, _x2, _x3) -> ExprBin(_x1, (map_head_expr f _x2), (map_head_expr f _x3))
  | ExprUn(_x1, _x2) -> ExprUn(_x1, (map_head_expr f _x2))
  | ExprSeq(_x1) -> ExprSeq((map_head_exprSeq f _x1))
  | ExprSet(_x1) -> ExprSet((map_head_exprSet f _x1))
  | RelSet(_x1, _x2) -> RelSet((map_head_expr f _x1), (map_head_expr f _x2))
  | RelSeqComp(_x1) -> RelSeqComp(((List.map (map_head_expr f)) _x1))
  | ExprNumber(_x1) -> ExprNumber((map_head_number f _x1))
  | ExprBool(_x1) -> ExprBool((map_head_bool f _x1))
  | ExprString(_x1) -> ExprString((map_head_string f _x1))

  | ExprPred(_x1) -> ExprPred((map_head_predicate f _x1))
  | ExprNuplet(_x1) -> ExprNuplet(((List.map (map_head_expr f)) _x1))
  | ExprSIGMA(_x1, _x2, _x3) -> ExprSIGMA(((List.map (map_head_id f)) _x1), (map_head_predicate f _x2), (map_head_expr f _x3))
  | ExprPI(_x1, _x2, _x3) -> ExprPI(((List.map (map_head_id f)) _x1), (map_head_predicate f _x2), (map_head_expr f _x3))
  | ExprLambda(_x1, _x2, _x3) -> ExprLambda(((List.map (map_head_id f)) _x1), (map_head_predicate f _x2), (map_head_expr f _x3))
  and map_head_exprSeq f  = function
    SeqEnum(_x1) -> SeqEnum(((List.map (map_head_expr f)) _x1))
  | SeqEmpty -> SeqEmpty 
  and map_head_exprSet f  = function
    SetRange(_x1, _x2) -> SetRange((map_head_expr f _x1), (map_head_expr f _x2))
  | SetEnum(_x1) -> SetEnum(((List.map (map_head_expr f)) _x1))
  | SetCompr(_x1) -> SetCompr(((List.map (map_head_expr f)) _x1))
  | SetComprPred(_x1, _x2) -> SetComprPred(((List.map (map_head_id f)) _x1), (map_head_predicate f _x2))
  | SetEmpty -> SetEmpty 
  | SetUnionQuantify(_x1, _x2, _x3) -> SetUnionQuantify(((List.map (map_head_id f)) _x1), (map_head_predicate f _x2), (map_head_expr f _x3))
  | SetInterQuantify(_x1, _x2, _x3) -> SetInterQuantify(((List.map (map_head_id f)) _x1), (map_head_predicate f _x2), (map_head_expr f _x3))
  and map_head_number f  = function
    MinNumber -> MinNumber 
  | Number(_x1) -> Number((map_head_int f _x1))
  | MaxNumber -> MaxNumber 
  and map_head_substitution f  = function
    SubstOperCall(_x1, _x2, _x3) -> SubstOperCall((map_head_ids f _x1), (map_head_id f _x2), ((List.map (map_head_expr f)) _x3))
  | SubstBlock(_x1) -> SubstBlock((map_head_substitution f _x1))
  | SubstPrecondition(_x1, _x2) -> SubstPrecondition((map_head_predicate f _x1), (map_head_substitution f _x2))
  | SubstAssertion(_x1, _x2) -> SubstAssertion((map_head_predicate f _x1), (map_head_substitution f _x2))
  | SubstChoice(_x1) -> SubstChoice(((List.map (map_head_substitution f)) _x1))
  | SubstIf(_x1, _x2) -> SubstIf(((List.map (fun (_x1, _x2) -> ((map_head_predicate f _x1), (map_head_substitution f _x2)))) _x1), ((map_head_option (map_head_substitution f)) _x2))
  | SubstSelect(_x1, _x2) -> SubstSelect(((List.map (fun (_x1, _x2) -> ((map_head_predicate f _x1), (map_head_substitution f _x2)))) _x1), ((map_head_option (map_head_substitution f)) _x2))
  | SubstCase(_x1, _x2, _x3) -> SubstCase((map_head_expr f _x1), ((List.map (fun (_x1, _x2) -> (((List.map (map_head_expr f)) _x1), (map_head_substitution f _x2)))) _x2), ((map_head_option (map_head_substitution f)) _x3))
  | SubstAny(_x1, _x2, _x3) -> SubstAny(((List.map (map_head_id f)) _x1), (map_head_predicate f _x2), (map_head_substitution f _x3))
  | SubstLet(_x1, _x2, _x3) -> SubstLet(((List.map (map_head_id f)) _x1), (map_head_predicate f _x2), (map_head_substitution f _x3))
  | SubstVar(_x1, _x2) -> SubstVar(((List.map (map_head_id f)) _x1), (map_head_substitution f _x2))
  | SubstWhile(_x1, _x2, _x3, _x4) -> SubstWhile((map_head_predicate f _x1), (map_head_substitution f _x2), (map_head_expr f _x3), (map_head_predicate f _x4))
  | SubstSkip -> SubstSkip 
  | SubstSequence(_x1, _x2) -> SubstSequence((map_head_substitution f _x1), (map_head_substitution f _x2))
  | SubstParallel(_x1, _x2) -> SubstParallel((map_head_substitution f _x1), (map_head_substitution f _x2))
  | SubstSetEqualIds(_x1, _x2) -> SubstSetEqualIds((map_head_ids f _x1), ((List.map (map_head_expr f)) _x2))
  | SubstSetEqualFun(_x1, _x2, _x3) -> SubstSetEqualFun((map_head_expr f _x1), ((List.map (map_head_expr f)) _x2), (map_head_expr f _x3))
  | SubstBecomeSuch(_x1, _x2) -> SubstBecomeSuch((map_head_ids f _x1), (map_head_predicate f _x2))
  | SubstSetIn(_x1, _x2) -> SubstSetIn((map_head_ids f _x1), (map_head_expr f _x2))
  and map_head_ids f  = fun (_x1) -> ((List.map (map_head_id f)) _x1)
  and map_head_origine f  = function
    Local -> Local 
  | Remote(_x1) -> Remote(((List.map (fun (_x1, _x2) -> ((map_head_string f _x1), (map_head_string f _x2)))) _x1))
  | OrgRefine(_x1, _x2) -> OrgRefine((map_head_string f _x1), ((List.map (fun (_x1, _x2) -> ((map_head_string f _x1), (map_head_string f _x2)))) _x2))
  | Prefix(_x1) -> Prefix(((List.map (map_head_string f)) _x1))
  and map_head_id f  = function
    Id(_x1, _x2, _x3, _x4) -> Id((map_head_origine f _x1), (map_head_string f _x2), ((map_head_option (map_head_type_B f)) _x3), _x4)

let map_clause_string f (s:string) = s
let map_clause_int f (i:int32) = i
let map_clause_bool f (b:bool) = b
let map_clause_option f = function Some x -> Some (f x) | None -> None
let rec map_clause_amn f  = function
    Machine(_x1, _x2) -> Machine((map_clause_head f _x1), ((List.map (map_clause_clause f)) _x2))
  | Refinement(_x1, _x2) -> Refinement((map_clause_head f _x1), ((List.map (map_clause_clause f)) _x2))
  | Implementation(_x1, _x2) -> Implementation((map_clause_head f _x1), ((List.map (map_clause_clause f)) _x2))
  | EmptyTree -> EmptyTree 
  and map_clause_head f  = fun (_x1, _x2) -> (map_clause_id f _x1), (map_clause_ids f _x2)
  and map_clause_clause f  = fun _x -> f((function
    Definitions(_x1) -> Definitions(((List.map (map_clause_def f)) _x1))
  | Constraints(_x1) -> Constraints((map_clause_predicate f _x1))
  | Invariant(_x1) -> Invariant((map_clause_predicate f _x1))
  | Sets(_x1) -> Sets(((List.map (map_clause_set f)) _x1))
  | Initialisation(_x1) -> Initialisation((map_clause_substitution f _x1))
  | ConstantsConcrete(_x1) -> ConstantsConcrete((map_clause_ids f _x1))
  | ConstantsAbstract(_x1) -> ConstantsAbstract((map_clause_ids f _x1))
  | ConstantsHidden(_x1) -> ConstantsHidden((map_clause_ids f _x1))
  | Properties(_x1) -> Properties((map_clause_predicate f _x1))
  | Values(_x1) -> Values(((List.map (fun (_x1, _x2) -> ((map_clause_id f _x1), (map_clause_expr f _x2)))) _x1))
  | VariablesConcrete(_x1) -> VariablesConcrete((map_clause_ids f _x1))
  | VariablesAbstract(_x1) -> VariablesAbstract((map_clause_ids f _x1))
  | VariablesHidden(_x1) -> VariablesHidden((map_clause_ids f _x1))
  | Promotes(_x1) -> Promotes((map_clause_ids f _x1))
  | Assertions(_x1) -> Assertions(((List.map (map_clause_predicate f)) _x1))
  | Operations(_x1) -> Operations(((List.map (map_clause_operation f)) _x1))
  | Sees(_x1) -> Sees((map_clause_ids f _x1))
  | Uses(_x1) -> Uses((map_clause_ids f _x1))
  | Extends(_x1) -> Extends(((List.map (map_clause_instance f)) _x1))
  | Includes(_x1) -> Includes(((List.map (map_clause_instance f)) _x1))
  | Imports(_x1) -> Imports(((List.map (map_clause_instance f)) _x1))
  | Refines(_x1) -> Refines((map_clause_id f _x1)))_x)
  and map_clause_def f  = function
    Def(_x1, _x2, _x3) -> Def((map_clause_id f _x1), (map_clause_ids f _x2), (map_clause_defBody f _x3))
  | FileDef(_x1) -> FileDef((map_clause_string f _x1))
  and map_clause_defBody f  = function
    | ExprDefBody(_x1) -> ExprDefBody((map_clause_expr f _x1))
    | SubstDefBody(_x1) -> SubstDefBody((map_clause_substitution f _x1))
  and map_clause_set f  = function
    SetAbstDec(_x1) -> SetAbstDec((map_clause_id f _x1))
  | SetEnumDec(_x1, _x2) -> SetEnumDec((map_clause_id f _x1), (map_clause_ids f _x2))

  and map_clause_instance f  = fun (_x1, _x2) -> (map_clause_id f _x1), ((List.map (map_clause_expr f)) _x2)
  and map_clause_type_B f  = function
    TypeNatSetRange(_x1, _x2) -> TypeNatSetRange((map_clause_expr f _x1), (map_clause_expr f _x2))
  | PredType(_x1) -> PredType((map_clause_expr f _x1))

  | TypeIdentifier(_x1) -> TypeIdentifier((map_clause_id f _x1))
  | PFunType(_x1, _x2) -> PFunType((map_clause_type_B f _x1), (map_clause_type_B f _x2))
  | FunType(_x1, _x2) -> FunType((map_clause_type_B f _x1), (map_clause_type_B f _x2))
  | ProdType(_x1, _x2) -> ProdType((map_clause_type_B f _x1), (map_clause_type_B f _x2))
  | Sequence(_x1) -> Sequence((map_clause_type_B f _x1))
  | SubType(_x1, _x2) -> SubType((map_clause_type_B f _x1), (map_clause_expr f _x2))
  | Fin_B(_x1) -> Fin_B((map_clause_type_B f _x1))
  | Untyped -> Untyped 
  and map_clause_field f  = fun (_x1, _x2) -> (map_clause_id f _x1), (map_clause_type_B f _x2)
  and map_clause_predicate f  = function
    PredParen(_x1) -> PredParen((map_clause_predicate f _x1))
  | PredNegation(_x1) -> PredNegation((map_clause_predicate f _x1))
  | PredExists(_x1, _x2) -> PredExists(((List.map (map_clause_id f)) _x1), (map_clause_predicate f _x2))
  | PredForAll(_x1, _x2) -> PredForAll(((List.map (map_clause_id f)) _x1), (map_clause_predicate f _x2))
  | PredBin(_x1, _x2, _x3) -> PredBin(_x1, (map_clause_predicate f _x2), (map_clause_predicate f _x3))
  | PredAtom(_x1, _x2, _x3) -> PredAtom(_x1, (map_clause_expr f _x2), (map_clause_expr f _x3))
  and map_clause_operation f  = fun (_x1, _x2, _x3, _x4) -> (map_clause_id f _x1), (map_clause_ids f _x2), (map_clause_ids f _x3), (map_clause_substitution f _x4)
  and map_clause_expr f  = function
    ExprSequence(_x1, _x2) -> ExprSequence((map_clause_expr f _x1), (map_clause_expr f _x2))
  | ExprParen(_x1) -> ExprParen((map_clause_expr f _x1))
  | ExprId(_x1) -> ExprId((map_clause_id f _x1))
  | ExprFunCall(_x1, _x2) -> ExprFunCall((map_clause_expr f _x1), ((List.map (map_clause_expr f)) _x2))
  | ExprBin(_x1, _x2, _x3) -> ExprBin(_x1, (map_clause_expr f _x2), (map_clause_expr f _x3))
  | ExprUn(_x1, _x2) -> ExprUn(_x1, (map_clause_expr f _x2))
  | ExprSeq(_x1) -> ExprSeq((map_clause_exprSeq f _x1))
  | ExprSet(_x1) -> ExprSet((map_clause_exprSet f _x1))
  | RelSet(_x1, _x2) -> RelSet((map_clause_expr f _x1), (map_clause_expr f _x2))
  | RelSeqComp(_x1) -> RelSeqComp(((List.map (map_clause_expr f)) _x1))
  | ExprNumber(_x1) -> ExprNumber((map_clause_number f _x1))
  | ExprBool(_x1) -> ExprBool((map_clause_bool f _x1))
  | ExprString(_x1) -> ExprString((map_clause_string f _x1))

  | ExprPred(_x1) -> ExprPred((map_clause_predicate f _x1))
  | ExprNuplet(_x1) -> ExprNuplet(((List.map (map_clause_expr f)) _x1))
  | ExprSIGMA(_x1, _x2, _x3) -> ExprSIGMA(((List.map (map_clause_id f)) _x1), (map_clause_predicate f _x2), (map_clause_expr f _x3))
  | ExprPI(_x1, _x2, _x3) -> ExprPI(((List.map (map_clause_id f)) _x1), (map_clause_predicate f _x2), (map_clause_expr f _x3))
  | ExprLambda(_x1, _x2, _x3) -> ExprLambda(((List.map (map_clause_id f)) _x1), (map_clause_predicate f _x2), (map_clause_expr f _x3))
  and map_clause_exprSeq f  = function
    SeqEnum(_x1) -> SeqEnum(((List.map (map_clause_expr f)) _x1))
  | SeqEmpty -> SeqEmpty 
  and map_clause_exprSet f  = function
    SetRange(_x1, _x2) -> SetRange((map_clause_expr f _x1), (map_clause_expr f _x2))
  | SetEnum(_x1) -> SetEnum(((List.map (map_clause_expr f)) _x1))
  | SetCompr(_x1) -> SetCompr(((List.map (map_clause_expr f)) _x1))
  | SetComprPred(_x1, _x2) -> SetComprPred(((List.map (map_clause_id f)) _x1), (map_clause_predicate f _x2))
  | SetEmpty -> SetEmpty 
  | SetUnionQuantify(_x1, _x2, _x3) -> SetUnionQuantify(((List.map (map_clause_id f)) _x1), (map_clause_predicate f _x2), (map_clause_expr f _x3))
  | SetInterQuantify(_x1, _x2, _x3) -> SetInterQuantify(((List.map (map_clause_id f)) _x1), (map_clause_predicate f _x2), (map_clause_expr f _x3))
  and map_clause_number f  = function
    MinNumber -> MinNumber 
  | Number(_x1) -> Number((map_clause_int f _x1))
  | MaxNumber -> MaxNumber 
  and map_clause_substitution f  = function
    SubstOperCall(_x1, _x2, _x3) -> SubstOperCall((map_clause_ids f _x1), (map_clause_id f _x2), ((List.map (map_clause_expr f)) _x3))
  | SubstBlock(_x1) -> SubstBlock((map_clause_substitution f _x1))
  | SubstPrecondition(_x1, _x2) -> SubstPrecondition((map_clause_predicate f _x1), (map_clause_substitution f _x2))
  | SubstAssertion(_x1, _x2) -> SubstAssertion((map_clause_predicate f _x1), (map_clause_substitution f _x2))
  | SubstChoice(_x1) -> SubstChoice(((List.map (map_clause_substitution f)) _x1))
  | SubstIf(_x1, _x2) -> SubstIf(((List.map (fun (_x1, _x2) -> ((map_clause_predicate f _x1), (map_clause_substitution f _x2)))) _x1), ((map_clause_option (map_clause_substitution f)) _x2))
  | SubstSelect(_x1, _x2) -> SubstSelect(((List.map (fun (_x1, _x2) -> ((map_clause_predicate f _x1), (map_clause_substitution f _x2)))) _x1), ((map_clause_option (map_clause_substitution f)) _x2))
  | SubstCase(_x1, _x2, _x3) -> SubstCase((map_clause_expr f _x1), ((List.map (fun (_x1, _x2) -> (((List.map (map_clause_expr f)) _x1), (map_clause_substitution f _x2)))) _x2), ((map_clause_option (map_clause_substitution f)) _x3))
  | SubstAny(_x1, _x2, _x3) -> SubstAny(((List.map (map_clause_id f)) _x1), (map_clause_predicate f _x2), (map_clause_substitution f _x3))
  | SubstLet(_x1, _x2, _x3) -> SubstLet(((List.map (map_clause_id f)) _x1), (map_clause_predicate f _x2), (map_clause_substitution f _x3))
  | SubstVar(_x1, _x2) -> SubstVar(((List.map (map_clause_id f)) _x1), (map_clause_substitution f _x2))
  | SubstWhile(_x1, _x2, _x3, _x4) -> SubstWhile((map_clause_predicate f _x1), (map_clause_substitution f _x2), (map_clause_expr f _x3), (map_clause_predicate f _x4))
  | SubstSkip -> SubstSkip 
  | SubstSequence(_x1, _x2) -> SubstSequence((map_clause_substitution f _x1), (map_clause_substitution f _x2))
  | SubstParallel(_x1, _x2) -> SubstParallel((map_clause_substitution f _x1), (map_clause_substitution f _x2))
  | SubstSetEqualIds(_x1, _x2) -> SubstSetEqualIds((map_clause_ids f _x1), ((List.map (map_clause_expr f)) _x2))
  | SubstSetEqualFun(_x1, _x2, _x3) -> SubstSetEqualFun((map_clause_expr f _x1), ((List.map (map_clause_expr f)) _x2), (map_clause_expr f _x3))

  | SubstBecomeSuch(_x1, _x2) -> SubstBecomeSuch((map_clause_ids f _x1), (map_clause_predicate f _x2))
  | SubstSetIn(_x1, _x2) -> SubstSetIn((map_clause_ids f _x1), (map_clause_expr f _x2))
  and map_clause_ids f  = fun (_x1) -> ((List.map (map_clause_id f)) _x1)
  and map_clause_origine f  = function
    Local -> Local 
  | Remote(_x1) -> Remote(((List.map (fun (_x1, _x2) -> ((map_clause_string f _x1), (map_clause_string f _x2)))) _x1))
  | OrgRefine(_x1, _x2) -> OrgRefine((map_clause_string f _x1), ((List.map (fun (_x1, _x2) -> ((map_clause_string f _x1), (map_clause_string f _x2)))) _x2))
  | Prefix(_x1) -> Prefix(((List.map (map_clause_string f)) _x1))
  and map_clause_id f  = function
    Id(_x1, _x2, _x3, _x4) -> Id((map_clause_origine f _x1), (map_clause_string f _x2), ((map_clause_option (map_clause_type_B f)) _x3), _x4)

let map_def_string f (s:string) = s
let map_def_int f (i:int32) = i
let map_def_bool f (b:bool) = b
let map_def_option f = function Some x -> Some (f x) | None -> None
let rec map_def_amn f  = function
    Machine(_x1, _x2) -> Machine((map_def_head f _x1), ((List.map (map_def_clause f)) _x2))
  | Refinement(_x1, _x2) -> Refinement((map_def_head f _x1), ((List.map (map_def_clause f)) _x2))
  | Implementation(_x1, _x2) -> Implementation((map_def_head f _x1), ((List.map (map_def_clause f)) _x2))
  | EmptyTree -> EmptyTree 
  and map_def_head f  = fun (_x1, _x2) -> (map_def_id f _x1), (map_def_ids f _x2)
  and map_def_clause f  = function
    Definitions(_x1) -> Definitions(((List.map (map_def_def f)) _x1))
  | Constraints(_x1) -> Constraints((map_def_predicate f _x1))
  | Invariant(_x1) -> Invariant((map_def_predicate f _x1))
  | Sets(_x1) -> Sets(((List.map (map_def_set f)) _x1))
  | Initialisation(_x1) -> Initialisation((map_def_substitution f _x1))
  | ConstantsConcrete(_x1) -> ConstantsConcrete((map_def_ids f _x1))
  | ConstantsAbstract(_x1) -> ConstantsAbstract((map_def_ids f _x1))
  | ConstantsHidden(_x1) -> ConstantsHidden((map_def_ids f _x1))
  | Properties(_x1) -> Properties((map_def_predicate f _x1))
  | Values(_x1) -> Values(((List.map (fun (_x1, _x2) -> ((map_def_id f _x1), (map_def_expr f _x2)))) _x1))
  | VariablesConcrete(_x1) -> VariablesConcrete((map_def_ids f _x1))
  | VariablesAbstract(_x1) -> VariablesAbstract((map_def_ids f _x1))
  | VariablesHidden(_x1) -> VariablesHidden((map_def_ids f _x1))
  | Promotes(_x1) -> Promotes((map_def_ids f _x1))
  | Assertions(_x1) -> Assertions(((List.map (map_def_predicate f)) _x1))
  | Operations(_x1) -> Operations(((List.map (map_def_operation f)) _x1))
  | Sees(_x1) -> Sees((map_def_ids f _x1))
  | Uses(_x1) -> Uses((map_def_ids f _x1))
  | Extends(_x1) -> Extends(((List.map (map_def_instance f)) _x1))
  | Includes(_x1) -> Includes(((List.map (map_def_instance f)) _x1))
  | Imports(_x1) -> Imports(((List.map (map_def_instance f)) _x1))
  | Refines(_x1) -> Refines((map_def_id f _x1))
  and map_def_def f  = fun _x -> f((function
    Def(_x1, _x2, _x3) -> Def((map_def_id f _x1), (map_def_ids f _x2), (map_def_defBody f _x3))
  | FileDef(_x1) -> FileDef((map_def_string f _x1)))_x)
  and map_def_defBody f  = function
    | ExprDefBody(_x1) -> ExprDefBody((map_def_expr f _x1))
  | SubstDefBody(_x1) -> SubstDefBody((map_def_substitution f _x1))
  and map_def_set f  = function
    SetAbstDec(_x1) -> SetAbstDec((map_def_id f _x1))
  | SetEnumDec(_x1, _x2) -> SetEnumDec((map_def_id f _x1), (map_def_ids f _x2))
  and map_def_instance f  = fun (_x1, _x2) -> (map_def_id f _x1), ((List.map (map_def_expr f)) _x2)
  and map_def_type_B f  = function
    TypeNatSetRange(_x1, _x2) -> TypeNatSetRange((map_def_expr f _x1), (map_def_expr f _x2))
  | PredType(_x1) -> PredType((map_def_expr f _x1))

  | TypeIdentifier(_x1) -> TypeIdentifier((map_def_id f _x1))
  | PFunType(_x1, _x2) -> PFunType((map_def_type_B f _x1), (map_def_type_B f _x2))
  | FunType(_x1, _x2) -> FunType((map_def_type_B f _x1), (map_def_type_B f _x2))
  | ProdType(_x1, _x2) -> ProdType((map_def_type_B f _x1), (map_def_type_B f _x2))
  | Sequence(_x1) -> Sequence((map_def_type_B f _x1))
  | SubType(_x1, _x2) -> SubType((map_def_type_B f _x1), (map_def_expr f _x2))
  | Fin_B(_x1) -> Fin_B((map_def_type_B f _x1))
  | Untyped -> Untyped 
  and map_def_field f  = fun (_x1, _x2) -> (map_def_id f _x1), (map_def_type_B f _x2)
  and map_def_predicate f  = function
    PredParen(_x1) -> PredParen((map_def_predicate f _x1))
  | PredNegation(_x1) -> PredNegation((map_def_predicate f _x1))
  | PredExists(_x1, _x2) -> PredExists(((List.map (map_def_id f)) _x1), (map_def_predicate f _x2))
  | PredForAll(_x1, _x2) -> PredForAll(((List.map (map_def_id f)) _x1), (map_def_predicate f _x2))
  | PredBin(_x1, _x2, _x3) -> PredBin(_x1, (map_def_predicate f _x2), (map_def_predicate f _x3))
  | PredAtom(_x1, _x2, _x3) -> PredAtom(_x1, (map_def_expr f _x2), (map_def_expr f _x3))
  and map_def_operation f  = fun (_x1, _x2, _x3, _x4) -> (map_def_id f _x1), (map_def_ids f _x2), (map_def_ids f _x3), (map_def_substitution f _x4)
  and map_def_expr f  = function
    ExprSequence(_x1, _x2) -> ExprSequence((map_def_expr f _x1), (map_def_expr f _x2))
  | ExprParen(_x1) -> ExprParen((map_def_expr f _x1))
  | ExprId(_x1) -> ExprId((map_def_id f _x1))
  | ExprFunCall(_x1, _x2) -> ExprFunCall((map_def_expr f _x1), ((List.map (map_def_expr f)) _x2))
  | ExprBin(_x1, _x2, _x3) -> ExprBin(_x1, (map_def_expr f _x2), (map_def_expr f _x3))
  | ExprUn(_x1, _x2) -> ExprUn(_x1, (map_def_expr f _x2))
  | ExprSeq(_x1) -> ExprSeq((map_def_exprSeq f _x1))
  | ExprSet(_x1) -> ExprSet((map_def_exprSet f _x1))
  | RelSet(_x1, _x2) -> RelSet((map_def_expr f _x1), (map_def_expr f _x2))
  | RelSeqComp(_x1) -> RelSeqComp(((List.map (map_def_expr f)) _x1))
  | ExprNumber(_x1) -> ExprNumber((map_def_number f _x1))
  | ExprBool(_x1) -> ExprBool((map_def_bool f _x1))
  | ExprString(_x1) -> ExprString((map_def_string f _x1))

  | ExprPred(_x1) -> ExprPred((map_def_predicate f _x1))
  | ExprNuplet(_x1) -> ExprNuplet(((List.map (map_def_expr f)) _x1))
  | ExprSIGMA(_x1, _x2, _x3) -> ExprSIGMA(((List.map (map_def_id f)) _x1), (map_def_predicate f _x2), (map_def_expr f _x3))
  | ExprPI(_x1, _x2, _x3) -> ExprPI(((List.map (map_def_id f)) _x1), (map_def_predicate f _x2), (map_def_expr f _x3))
  | ExprLambda(_x1, _x2, _x3) -> ExprLambda(((List.map (map_def_id f)) _x1), (map_def_predicate f _x2), (map_def_expr f _x3))

  and map_def_exprSeq f  = function
    SeqEnum(_x1) -> SeqEnum(((List.map (map_def_expr f)) _x1))
  | SeqEmpty -> SeqEmpty 
  and map_def_exprSet f  = function
    SetRange(_x1, _x2) -> SetRange((map_def_expr f _x1), (map_def_expr f _x2))
  | SetEnum(_x1) -> SetEnum(((List.map (map_def_expr f)) _x1))
  | SetCompr(_x1) -> SetCompr(((List.map (map_def_expr f)) _x1))
  | SetComprPred(_x1, _x2) -> SetComprPred(((List.map (map_def_id f)) _x1), (map_def_predicate f _x2))
  | SetEmpty -> SetEmpty 
  | SetUnionQuantify(_x1, _x2, _x3) -> SetUnionQuantify(((List.map (map_def_id f)) _x1), (map_def_predicate f _x2), (map_def_expr f _x3))
  | SetInterQuantify(_x1, _x2, _x3) -> SetInterQuantify(((List.map (map_def_id f)) _x1), (map_def_predicate f _x2), (map_def_expr f _x3))
  and map_def_number f  = function
    MinNumber -> MinNumber 
  | Number(_x1) -> Number((map_def_int f _x1))
  | MaxNumber -> MaxNumber 
  and map_def_substitution f  = function
    SubstOperCall(_x1, _x2, _x3) -> SubstOperCall((map_def_ids f _x1), (map_def_id f _x2), ((List.map (map_def_expr f)) _x3))
  | SubstBlock(_x1) -> SubstBlock((map_def_substitution f _x1))
  | SubstPrecondition(_x1, _x2) -> SubstPrecondition((map_def_predicate f _x1), (map_def_substitution f _x2))
  | SubstAssertion(_x1, _x2) -> SubstAssertion((map_def_predicate f _x1), (map_def_substitution f _x2))
  | SubstChoice(_x1) -> SubstChoice(((List.map (map_def_substitution f)) _x1))
  | SubstIf(_x1, _x2) -> SubstIf(((List.map (fun (_x1, _x2) -> ((map_def_predicate f _x1), (map_def_substitution f _x2)))) _x1), ((map_def_option (map_def_substitution f)) _x2))
  | SubstSelect(_x1, _x2) -> SubstSelect(((List.map (fun (_x1, _x2) -> ((map_def_predicate f _x1), (map_def_substitution f _x2)))) _x1), ((map_def_option (map_def_substitution f)) _x2))
  | SubstCase(_x1, _x2, _x3) -> SubstCase((map_def_expr f _x1), ((List.map (fun (_x1, _x2) -> (((List.map (map_def_expr f)) _x1), (map_def_substitution f _x2)))) _x2), ((map_def_option (map_def_substitution f)) _x3))
  | SubstAny(_x1, _x2, _x3) -> SubstAny(((List.map (map_def_id f)) _x1), (map_def_predicate f _x2), (map_def_substitution f _x3))
  | SubstLet(_x1, _x2, _x3) -> SubstLet(((List.map (map_def_id f)) _x1), (map_def_predicate f _x2), (map_def_substitution f _x3))
  | SubstVar(_x1, _x2) -> SubstVar(((List.map (map_def_id f)) _x1), (map_def_substitution f _x2))
  | SubstWhile(_x1, _x2, _x3, _x4) -> SubstWhile((map_def_predicate f _x1), (map_def_substitution f _x2), (map_def_expr f _x3), (map_def_predicate f _x4))
  | SubstSkip -> SubstSkip 
  | SubstSequence(_x1, _x2) -> SubstSequence((map_def_substitution f _x1), (map_def_substitution f _x2))
  | SubstParallel(_x1, _x2) -> SubstParallel((map_def_substitution f _x1), (map_def_substitution f _x2))
  | SubstSetEqualIds(_x1, _x2) -> SubstSetEqualIds((map_def_ids f _x1), ((List.map (map_def_expr f)) _x2))
  | SubstSetEqualFun(_x1, _x2, _x3) -> SubstSetEqualFun((map_def_expr f _x1), ((List.map (map_def_expr f)) _x2), (map_def_expr f _x3))

  | SubstBecomeSuch(_x1, _x2) -> SubstBecomeSuch((map_def_ids f _x1), (map_def_predicate f _x2))
  | SubstSetIn(_x1, _x2) -> SubstSetIn((map_def_ids f _x1), (map_def_expr f _x2))
  and map_def_ids f  = fun (_x1) -> ((List.map (map_def_id f)) _x1)
  and map_def_origine f  = function
    Local -> Local 
  | Remote(_x1) -> Remote(((List.map (fun (_x1, _x2) -> ((map_def_string f _x1), (map_def_string f _x2)))) _x1))
  | OrgRefine(_x1, _x2) -> OrgRefine((map_def_string f _x1), ((List.map (fun (_x1, _x2) -> ((map_def_string f _x1), (map_def_string f _x2)))) _x2))
  | Prefix(_x1) -> Prefix(((List.map (map_def_string f)) _x1))
  and map_def_id f  = function
    Id(_x1, _x2, _x3, _x4) -> Id((map_def_origine f _x1), (map_def_string f _x2), ((map_def_option (map_def_type_B f)) _x3), _x4)

let map_defBody_string f (s:string) = s
let map_defBody_int f (i:int32) = i
let map_defBody_bool f (b:bool) = b
let map_defBody_option f = function Some x -> Some (f x) | None -> None
let rec map_defBody_amn f  = function
    Machine(_x1, _x2) -> Machine((map_defBody_head f _x1), ((List.map (map_defBody_clause f)) _x2))
  | Refinement(_x1, _x2) -> Refinement((map_defBody_head f _x1), ((List.map (map_defBody_clause f)) _x2))
  | Implementation(_x1, _x2) -> Implementation((map_defBody_head f _x1), ((List.map (map_defBody_clause f)) _x2))
  | EmptyTree -> EmptyTree 
  and map_defBody_head f  = fun (_x1, _x2) -> (map_defBody_id f _x1), (map_defBody_ids f _x2)
  and map_defBody_clause f  = function
    Definitions(_x1) -> Definitions(((List.map (map_defBody_def f)) _x1))
  | Constraints(_x1) -> Constraints((map_defBody_predicate f _x1))
  | Invariant(_x1) -> Invariant((map_defBody_predicate f _x1))
  | Sets(_x1) -> Sets(((List.map (map_defBody_set f)) _x1))
  | Initialisation(_x1) -> Initialisation((map_defBody_substitution f _x1))
  | ConstantsConcrete(_x1) -> ConstantsConcrete((map_defBody_ids f _x1))
  | ConstantsAbstract(_x1) -> ConstantsAbstract((map_defBody_ids f _x1))
  | ConstantsHidden(_x1) -> ConstantsHidden((map_defBody_ids f _x1))
  | Properties(_x1) -> Properties((map_defBody_predicate f _x1))
  | Values(_x1) -> Values(((List.map (fun (_x1, _x2) -> ((map_defBody_id f _x1), (map_defBody_expr f _x2)))) _x1))
  | VariablesConcrete(_x1) -> VariablesConcrete((map_defBody_ids f _x1))
  | VariablesAbstract(_x1) -> VariablesAbstract((map_defBody_ids f _x1))
  | VariablesHidden(_x1) -> VariablesHidden((map_defBody_ids f _x1))
  | Promotes(_x1) -> Promotes((map_defBody_ids f _x1))
  | Assertions(_x1) -> Assertions(((List.map (map_defBody_predicate f)) _x1))
  | Operations(_x1) -> Operations(((List.map (map_defBody_operation f)) _x1))
  | Sees(_x1) -> Sees((map_defBody_ids f _x1))
  | Uses(_x1) -> Uses((map_defBody_ids f _x1))
  | Extends(_x1) -> Extends(((List.map (map_defBody_instance f)) _x1))
  | Includes(_x1) -> Includes(((List.map (map_defBody_instance f)) _x1))
  | Imports(_x1) -> Imports(((List.map (map_defBody_instance f)) _x1))
  | Refines(_x1) -> Refines((map_defBody_id f _x1))
  and map_defBody_def f  = function
    Def(_x1, _x2, _x3) -> Def((map_defBody_id f _x1), (map_defBody_ids f _x2), (map_defBody_defBody f _x3))
  | FileDef(_x1) -> FileDef((map_defBody_string f _x1))
  and map_defBody_defBody f  = fun _x -> f((function
    ExprDefBody(_x1) -> ExprDefBody((map_defBody_expr f _x1))
  | SubstDefBody(_x1) -> SubstDefBody((map_defBody_substitution f _x1)))_x)
  and map_defBody_set f  = function
    SetAbstDec(_x1) -> SetAbstDec((map_defBody_id f _x1))
  | SetEnumDec(_x1, _x2) -> SetEnumDec((map_defBody_id f _x1), (map_defBody_ids f _x2))

  and map_defBody_instance f  = fun (_x1, _x2) -> (map_defBody_id f _x1), ((List.map (map_defBody_expr f)) _x2)
  and map_defBody_type_B f  = function
    TypeNatSetRange(_x1, _x2) -> TypeNatSetRange((map_defBody_expr f _x1), (map_defBody_expr f _x2))
  | PredType(_x1) -> PredType((map_defBody_expr f _x1))
  | TypeIdentifier(_x1) -> TypeIdentifier((map_defBody_id f _x1))
  | PFunType(_x1, _x2) -> PFunType((map_defBody_type_B f _x1), (map_defBody_type_B f _x2))
  | FunType(_x1, _x2) -> FunType((map_defBody_type_B f _x1), (map_defBody_type_B f _x2))
  | ProdType(_x1, _x2) -> ProdType((map_defBody_type_B f _x1), (map_defBody_type_B f _x2))
  | Sequence(_x1) -> Sequence((map_defBody_type_B f _x1))
  | SubType(_x1, _x2) -> SubType((map_defBody_type_B f _x1), (map_defBody_expr f _x2))
  | Fin_B(_x1) -> Fin_B((map_defBody_type_B f _x1))
  | Untyped -> Untyped 
  and map_defBody_field f  = fun (_x1, _x2) -> (map_defBody_id f _x1), (map_defBody_type_B f _x2)
  and map_defBody_predicate f  = function
    PredParen(_x1) -> PredParen((map_defBody_predicate f _x1))
  | PredNegation(_x1) -> PredNegation((map_defBody_predicate f _x1))
  | PredExists(_x1, _x2) -> PredExists(((List.map (map_defBody_id f)) _x1), (map_defBody_predicate f _x2))
  | PredForAll(_x1, _x2) -> PredForAll(((List.map (map_defBody_id f)) _x1), (map_defBody_predicate f _x2))
  | PredBin(_x1, _x2, _x3) -> PredBin(_x1, (map_defBody_predicate f _x2), (map_defBody_predicate f _x3))
  | PredAtom(_x1, _x2, _x3) -> PredAtom(_x1, (map_defBody_expr f _x2), (map_defBody_expr f _x3))
  and map_defBody_operation f  = fun (_x1, _x2, _x3, _x4) -> (map_defBody_id f _x1), (map_defBody_ids f _x2), (map_defBody_ids f _x3), (map_defBody_substitution f _x4)
  and map_defBody_expr f  = function
    ExprSequence(_x1, _x2) -> ExprSequence((map_defBody_expr f _x1), (map_defBody_expr f _x2))
  | ExprParen(_x1) -> ExprParen((map_defBody_expr f _x1))
  | ExprId(_x1) -> ExprId((map_defBody_id f _x1))
  | ExprFunCall(_x1, _x2) -> ExprFunCall((map_defBody_expr f _x1), ((List.map (map_defBody_expr f)) _x2))
  | ExprBin(_x1, _x2, _x3) -> ExprBin(_x1, (map_defBody_expr f _x2), (map_defBody_expr f _x3))
  | ExprUn(_x1, _x2) -> ExprUn(_x1, (map_defBody_expr f _x2))
  | ExprSeq(_x1) -> ExprSeq((map_defBody_exprSeq f _x1))
  | ExprSet(_x1) -> ExprSet((map_defBody_exprSet f _x1))
  | RelSet(_x1, _x2) -> RelSet((map_defBody_expr f _x1), (map_defBody_expr f _x2))
  | RelSeqComp(_x1) -> RelSeqComp(((List.map (map_defBody_expr f)) _x1))
  | ExprNumber(_x1) -> ExprNumber((map_defBody_number f _x1))
  | ExprBool(_x1) -> ExprBool((map_defBody_bool f _x1))
  | ExprString(_x1) -> ExprString((map_defBody_string f _x1))

  | ExprPred(_x1) -> ExprPred((map_defBody_predicate f _x1))
  | ExprNuplet(_x1) -> ExprNuplet(((List.map (map_defBody_expr f)) _x1))
  | ExprSIGMA(_x1, _x2, _x3) -> ExprSIGMA(((List.map (map_defBody_id f)) _x1), (map_defBody_predicate f _x2), (map_defBody_expr f _x3))
  | ExprPI(_x1, _x2, _x3) -> ExprPI(((List.map (map_defBody_id f)) _x1), (map_defBody_predicate f _x2), (map_defBody_expr f _x3))
  | ExprLambda(_x1, _x2, _x3) -> ExprLambda(((List.map (map_defBody_id f)) _x1), (map_defBody_predicate f _x2), (map_defBody_expr f _x3))
  and map_defBody_exprSeq f  = function
    SeqEnum(_x1) -> SeqEnum(((List.map (map_defBody_expr f)) _x1))
  | SeqEmpty -> SeqEmpty 
  and map_defBody_exprSet f  = function
    SetRange(_x1, _x2) -> SetRange((map_defBody_expr f _x1), (map_defBody_expr f _x2))
  | SetEnum(_x1) -> SetEnum(((List.map (map_defBody_expr f)) _x1))
  | SetCompr(_x1) -> SetCompr(((List.map (map_defBody_expr f)) _x1))
  | SetComprPred(_x1, _x2) -> SetComprPred(((List.map (map_defBody_id f)) _x1), (map_defBody_predicate f _x2))
  | SetEmpty -> SetEmpty 
  | SetUnionQuantify(_x1, _x2, _x3) -> SetUnionQuantify(((List.map (map_defBody_id f)) _x1), (map_defBody_predicate f _x2), (map_defBody_expr f _x3))
  | SetInterQuantify(_x1, _x2, _x3) -> SetInterQuantify(((List.map (map_defBody_id f)) _x1), (map_defBody_predicate f _x2), (map_defBody_expr f _x3))
  and map_defBody_number f  = function
    MinNumber -> MinNumber 
  | Number(_x1) -> Number((map_defBody_int f _x1))
  | MaxNumber -> MaxNumber 
  and map_defBody_substitution f  = function
    SubstOperCall(_x1, _x2, _x3) -> SubstOperCall((map_defBody_ids f _x1), (map_defBody_id f _x2), ((List.map (map_defBody_expr f)) _x3))
  | SubstBlock(_x1) -> SubstBlock((map_defBody_substitution f _x1))
  | SubstPrecondition(_x1, _x2) -> SubstPrecondition((map_defBody_predicate f _x1), (map_defBody_substitution f _x2))
  | SubstAssertion(_x1, _x2) -> SubstAssertion((map_defBody_predicate f _x1), (map_defBody_substitution f _x2))
  | SubstChoice(_x1) -> SubstChoice(((List.map (map_defBody_substitution f)) _x1))
  | SubstIf(_x1, _x2) -> SubstIf(((List.map (fun (_x1, _x2) -> ((map_defBody_predicate f _x1), (map_defBody_substitution f _x2)))) _x1), ((map_defBody_option (map_defBody_substitution f)) _x2))
  | SubstSelect(_x1, _x2) -> SubstSelect(((List.map (fun (_x1, _x2) -> ((map_defBody_predicate f _x1), (map_defBody_substitution f _x2)))) _x1), ((map_defBody_option (map_defBody_substitution f)) _x2))
  | SubstCase(_x1, _x2, _x3) -> SubstCase((map_defBody_expr f _x1), ((List.map (fun (_x1, _x2) -> (((List.map (map_defBody_expr f)) _x1), (map_defBody_substitution f _x2)))) _x2), ((map_defBody_option (map_defBody_substitution f)) _x3))
  | SubstAny(_x1, _x2, _x3) -> SubstAny(((List.map (map_defBody_id f)) _x1), (map_defBody_predicate f _x2), (map_defBody_substitution f _x3))
  | SubstLet(_x1, _x2, _x3) -> SubstLet(((List.map (map_defBody_id f)) _x1), (map_defBody_predicate f _x2), (map_defBody_substitution f _x3))
  | SubstVar(_x1, _x2) -> SubstVar(((List.map (map_defBody_id f)) _x1), (map_defBody_substitution f _x2))
  | SubstWhile(_x1, _x2, _x3, _x4) -> SubstWhile((map_defBody_predicate f _x1), (map_defBody_substitution f _x2), (map_defBody_expr f _x3), (map_defBody_predicate f _x4))
  | SubstSkip -> SubstSkip 
  | SubstSequence(_x1, _x2) -> SubstSequence((map_defBody_substitution f _x1), (map_defBody_substitution f _x2))
  | SubstParallel(_x1, _x2) -> SubstParallel((map_defBody_substitution f _x1), (map_defBody_substitution f _x2))
  | SubstSetEqualIds(_x1, _x2) -> SubstSetEqualIds((map_defBody_ids f _x1), ((List.map (map_defBody_expr f)) _x2))
  | SubstSetEqualFun(_x1, _x2, _x3) -> SubstSetEqualFun((map_defBody_expr f _x1), ((List.map (map_defBody_expr f)) _x2), (map_defBody_expr f _x3))

  | SubstBecomeSuch(_x1, _x2) -> SubstBecomeSuch((map_defBody_ids f _x1), (map_defBody_predicate f _x2))
  | SubstSetIn(_x1, _x2) -> SubstSetIn((map_defBody_ids f _x1), (map_defBody_expr f _x2))
  and map_defBody_ids f  = fun (_x1) -> ((List.map (map_defBody_id f)) _x1)
  and map_defBody_origine f  = function
    Local -> Local 
  | Remote(_x1) -> Remote(((List.map (fun (_x1, _x2) -> ((map_defBody_string f _x1), (map_defBody_string f _x2)))) _x1))
  | OrgRefine(_x1, _x2) -> OrgRefine((map_defBody_string f _x1), ((List.map (fun (_x1, _x2) -> ((map_defBody_string f _x1), (map_defBody_string f _x2)))) _x2))
  | Prefix(_x1) -> Prefix(((List.map (map_defBody_string f)) _x1))
  and map_defBody_id f  = function
    Id(_x1, _x2, _x3, _x4) -> Id((map_defBody_origine f _x1), (map_defBody_string f _x2), ((map_defBody_option (map_defBody_type_B f)) _x3), _x4)

let map_set_string f (s:string) = s
let map_set_int f (i:int32) = i
let map_set_bool f (b:bool) = b
let map_set_option f = function Some x -> Some (f x) | None -> None
let rec map_set_amn f  = function
    Machine(_x1, _x2) -> Machine((map_set_head f _x1), ((List.map (map_set_clause f)) _x2))
  | Refinement(_x1, _x2) -> Refinement((map_set_head f _x1), ((List.map (map_set_clause f)) _x2))
  | Implementation(_x1, _x2) -> Implementation((map_set_head f _x1), ((List.map (map_set_clause f)) _x2))
  | EmptyTree -> EmptyTree 
  and map_set_head f  = fun (_x1, _x2) -> (map_set_id f _x1), (map_set_ids f _x2)
  and map_set_clause f  = function
    Definitions(_x1) -> Definitions(((List.map (map_set_def f)) _x1))
  | Constraints(_x1) -> Constraints((map_set_predicate f _x1))
  | Invariant(_x1) -> Invariant((map_set_predicate f _x1))
  | Sets(_x1) -> Sets(((List.map (map_set_set f)) _x1))
  | Initialisation(_x1) -> Initialisation((map_set_substitution f _x1))
  | ConstantsConcrete(_x1) -> ConstantsConcrete((map_set_ids f _x1))
  | ConstantsAbstract(_x1) -> ConstantsAbstract((map_set_ids f _x1))
  | ConstantsHidden(_x1) -> ConstantsHidden((map_set_ids f _x1))
  | Properties(_x1) -> Properties((map_set_predicate f _x1))
  | Values(_x1) -> Values(((List.map (fun (_x1, _x2) -> ((map_set_id f _x1), (map_set_expr f _x2)))) _x1))
  | VariablesConcrete(_x1) -> VariablesConcrete((map_set_ids f _x1))
  | VariablesAbstract(_x1) -> VariablesAbstract((map_set_ids f _x1))
  | VariablesHidden(_x1) -> VariablesHidden((map_set_ids f _x1))
  | Promotes(_x1) -> Promotes((map_set_ids f _x1))
  | Assertions(_x1) -> Assertions(((List.map (map_set_predicate f)) _x1))
  | Operations(_x1) -> Operations(((List.map (map_set_operation f)) _x1))
  | Sees(_x1) -> Sees((map_set_ids f _x1))
  | Uses(_x1) -> Uses((map_set_ids f _x1))
  | Extends(_x1) -> Extends(((List.map (map_set_instance f)) _x1))
  | Includes(_x1) -> Includes(((List.map (map_set_instance f)) _x1))
  | Imports(_x1) -> Imports(((List.map (map_set_instance f)) _x1))
  | Refines(_x1) -> Refines((map_set_id f _x1))
  and map_set_def f  = function
    Def(_x1, _x2, _x3) -> 
      Def((map_set_id f _x1), (map_set_ids f _x2), (map_set_defBody f _x3))
    | FileDef(_x1) -> FileDef((map_set_string f _x1))
  and map_set_defBody f  = function
    ExprDefBody(_x1) -> ExprDefBody((map_set_expr f _x1))
  | SubstDefBody(_x1) -> SubstDefBody((map_set_substitution f _x1))
  and map_set_set f  = function (* _x -> f((function *)
    SetAbstDec(_x1) -> SetAbstDec((map_set_id f _x1))
  | SetEnumDec(_x1, _x2) -> SetEnumDec((map_set_id f _x1), (map_set_ids f _x2))

  and map_set_instance f  = function (_x1, _x2) 
      -> (map_set_id f _x1), ((List.map (map_set_expr f)) _x2)
  and map_set_type_B f  = function
    TypeNatSetRange(_x1, _x2) -> TypeNatSetRange((map_set_expr f _x1), (map_set_expr f _x2))
  | PredType(_x1) -> PredType((map_set_expr f _x1))

  | TypeIdentifier(_x1) -> TypeIdentifier((map_set_id f _x1))
  | PFunType(_x1, _x2) -> PFunType((map_set_type_B f _x1), (map_set_type_B f _x2))
  | FunType(_x1, _x2) -> FunType((map_set_type_B f _x1), (map_set_type_B f _x2))
  | ProdType(_x1, _x2) -> ProdType((map_set_type_B f _x1), (map_set_type_B f _x2))
  | Sequence(_x1) -> Sequence((map_set_type_B f _x1))
  | SubType(_x1, _x2) -> SubType((map_set_type_B f _x1), (map_set_expr f _x2))
  | Fin_B(_x1) -> Fin_B((map_set_type_B f _x1))
  | Untyped -> Untyped 
  and map_set_field f  = fun (_x1, _x2) -> (map_set_id f _x1), (map_set_type_B f _x2)
  and map_set_predicate f  = function
    PredParen(_x1) -> PredParen((map_set_predicate f _x1))
  | PredNegation(_x1) -> PredNegation((map_set_predicate f _x1))
  | PredExists(_x1, _x2) -> PredExists(((List.map (map_set_id f)) _x1), (map_set_predicate f _x2))
  | PredForAll(_x1, _x2) -> PredForAll(((List.map (map_set_id f)) _x1), (map_set_predicate f _x2))
  | PredBin(_x1, _x2, _x3) -> PredBin(_x1, (map_set_predicate f _x2), (map_set_predicate f _x3))
  | PredAtom(_x1, _x2, _x3) -> PredAtom(_x1, (map_set_expr f _x2), (map_set_expr f _x3))
  and map_set_operation f  = fun (_x1, _x2, _x3, _x4) -> (map_set_id f _x1), (map_set_ids f _x2), (map_set_ids f _x3), (map_set_substitution f _x4)
  and map_set_expr f  = function
    ExprSequence(_x1, _x2) -> ExprSequence((map_set_expr f _x1), (map_set_expr f _x2))
  | ExprParen(_x1) -> ExprParen((map_set_expr f _x1))
  | ExprId(_x1) -> ExprId((map_set_id f _x1))
  | ExprFunCall(_x1, _x2) -> ExprFunCall((map_set_expr f _x1), ((List.map (map_set_expr f)) _x2))
  | ExprBin(_x1, _x2, _x3) -> ExprBin(_x1, (map_set_expr f _x2), (map_set_expr f _x3))
  | ExprUn(_x1, _x2) -> ExprUn(_x1, (map_set_expr f _x2))
  | ExprSeq(_x1) -> ExprSeq((map_set_exprSeq f _x1))
  | ExprSet(_x1) -> ExprSet((map_set_exprSet f _x1))
  | RelSet(_x1, _x2) -> RelSet((map_set_expr f _x1), (map_set_expr f _x2))
  | RelSeqComp(_x1) -> RelSeqComp(((List.map (map_set_expr f)) _x1))
  | ExprNumber(_x1) -> ExprNumber((map_set_number f _x1))
  | ExprBool(_x1) -> ExprBool((map_set_bool f _x1))
  | ExprString(_x1) -> ExprString((map_set_string f _x1))

  | ExprPred(_x1) -> ExprPred((map_set_predicate f _x1))
  | ExprNuplet(_x1) -> ExprNuplet(((List.map (map_set_expr f)) _x1))
  | ExprSIGMA(_x1, _x2, _x3) -> ExprSIGMA(((List.map (map_set_id f)) _x1), (map_set_predicate f _x2), (map_set_expr f _x3))
  | ExprPI(_x1, _x2, _x3) -> ExprPI(((List.map (map_set_id f)) _x1), (map_set_predicate f _x2), (map_set_expr f _x3))
  | ExprLambda(_x1, _x2, _x3) -> ExprLambda(((List.map (map_set_id f)) _x1), (map_set_predicate f _x2), (map_set_expr f _x3))

  and map_set_exprSeq f  = function
    SeqEnum(_x1) -> SeqEnum(((List.map (map_set_expr f)) _x1))
  | SeqEmpty -> SeqEmpty 
  and map_set_exprSet f  = function
    SetRange(_x1, _x2) -> SetRange((map_set_expr f _x1), (map_set_expr f _x2))
  | SetEnum(_x1) -> SetEnum(((List.map (map_set_expr f)) _x1))
  | SetCompr(_x1) -> SetCompr(((List.map (map_set_expr f)) _x1))
  | SetComprPred(_x1, _x2) -> SetComprPred(((List.map (map_set_id f)) _x1), (map_set_predicate f _x2))
  | SetEmpty -> SetEmpty 
  | SetUnionQuantify(_x1, _x2, _x3) -> SetUnionQuantify(((List.map (map_set_id f)) _x1), (map_set_predicate f _x2), (map_set_expr f _x3))
  | SetInterQuantify(_x1, _x2, _x3) -> SetInterQuantify(((List.map (map_set_id f)) _x1), (map_set_predicate f _x2), (map_set_expr f _x3))
  and map_set_number f  = function
    MinNumber -> MinNumber 
  | Number(_x1) -> Number((map_set_int f _x1))
  | MaxNumber -> MaxNumber 
  and map_set_substitution f  = function
    SubstOperCall(_x1, _x2, _x3) -> SubstOperCall((map_set_ids f _x1), (map_set_id f _x2), ((List.map (map_set_expr f)) _x3))
  | SubstBlock(_x1) -> SubstBlock((map_set_substitution f _x1))
  | SubstPrecondition(_x1, _x2) -> SubstPrecondition((map_set_predicate f _x1), (map_set_substitution f _x2))
  | SubstAssertion(_x1, _x2) -> SubstAssertion((map_set_predicate f _x1), (map_set_substitution f _x2))
  | SubstChoice(_x1) -> SubstChoice(((List.map (map_set_substitution f)) _x1))
  | SubstIf(_x1, _x2) -> SubstIf(((List.map (fun (_x1, _x2) -> ((map_set_predicate f _x1), (map_set_substitution f _x2)))) _x1), ((map_set_option (map_set_substitution f)) _x2))
  | SubstSelect(_x1, _x2) -> SubstSelect(((List.map (fun (_x1, _x2) -> ((map_set_predicate f _x1), (map_set_substitution f _x2)))) _x1), ((map_set_option (map_set_substitution f)) _x2))
  | SubstCase(_x1, _x2, _x3) -> SubstCase((map_set_expr f _x1), ((List.map (fun (_x1, _x2) -> (((List.map (map_set_expr f)) _x1), (map_set_substitution f _x2)))) _x2), ((map_set_option (map_set_substitution f)) _x3))
  | SubstAny(_x1, _x2, _x3) -> SubstAny(((List.map (map_set_id f)) _x1), (map_set_predicate f _x2), (map_set_substitution f _x3))
  | SubstLet(_x1, _x2, _x3) -> SubstLet(((List.map (map_set_id f)) _x1), (map_set_predicate f _x2), (map_set_substitution f _x3))
  | SubstVar(_x1, _x2) -> SubstVar(((List.map (map_set_id f)) _x1), (map_set_substitution f _x2))
  | SubstWhile(_x1, _x2, _x3, _x4) -> SubstWhile((map_set_predicate f _x1), (map_set_substitution f _x2), (map_set_expr f _x3), (map_set_predicate f _x4))
  | SubstSkip -> SubstSkip 
  | SubstSequence(_x1, _x2) -> SubstSequence((map_set_substitution f _x1), (map_set_substitution f _x2))
  | SubstParallel(_x1, _x2) -> SubstParallel((map_set_substitution f _x1), (map_set_substitution f _x2))
  | SubstSetEqualIds(_x1, _x2) -> SubstSetEqualIds((map_set_ids f _x1), ((List.map (map_set_expr f)) _x2))
  | SubstSetEqualFun(_x1, _x2, _x3) -> SubstSetEqualFun((map_set_expr f _x1), ((List.map (map_set_expr f)) _x2), (map_set_expr f _x3))

  | SubstBecomeSuch(_x1, _x2) -> SubstBecomeSuch((map_set_ids f _x1), (map_set_predicate f _x2))
  | SubstSetIn(_x1, _x2) -> SubstSetIn((map_set_ids f _x1), (map_set_expr f _x2))
  and map_set_ids f  = fun (_x1) -> ((List.map (map_set_id f)) _x1)
  and map_set_origine f  = function
    Local -> Local 
  | Remote(_x1) -> Remote(((List.map (fun (_x1, _x2) -> ((map_set_string f _x1), (map_set_string f _x2)))) _x1))
  | OrgRefine(_x1, _x2) -> OrgRefine((map_set_string f _x1), ((List.map (fun (_x1, _x2) -> ((map_set_string f _x1), (map_set_string f _x2)))) _x2))
  | Prefix(_x1) -> Prefix(((List.map (map_set_string f)) _x1))
  and map_set_id f  = function
    Id(_x1, _x2, _x3, _x4) -> Id((map_set_origine f _x1), (map_set_string f _x2), ((map_set_option (map_set_type_B f)) _x3), _x4)

let map_instance_string f (s:string) = s
let map_instance_int f (i:int32) = i
let map_instance_bool f (b:bool) = b
let map_instance_option f = function Some x -> Some (f x) | None -> None
let rec map_instance_amn f  = function
    Machine(_x1, _x2) -> Machine((map_instance_head f _x1), ((List.map (map_instance_clause f)) _x2))
  | Refinement(_x1, _x2) -> Refinement((map_instance_head f _x1), ((List.map (map_instance_clause f)) _x2))
  | Implementation(_x1, _x2) -> Implementation((map_instance_head f _x1), ((List.map (map_instance_clause f)) _x2))
  | EmptyTree -> EmptyTree 
  and map_instance_head f  = fun (_x1, _x2) -> (map_instance_id f _x1), (map_instance_ids f _x2)
  and map_instance_clause f  = function
    Definitions(_x1) -> Definitions(((List.map (map_instance_def f)) _x1))
  | Constraints(_x1) -> Constraints((map_instance_predicate f _x1))
  | Invariant(_x1) -> Invariant((map_instance_predicate f _x1))
  | Sets(_x1) -> Sets(((List.map (map_instance_set f)) _x1))
  | Initialisation(_x1) -> Initialisation((map_instance_substitution f _x1))
  | ConstantsConcrete(_x1) -> ConstantsConcrete((map_instance_ids f _x1))
  | ConstantsAbstract(_x1) -> ConstantsAbstract((map_instance_ids f _x1))
  | ConstantsHidden(_x1) -> ConstantsHidden((map_instance_ids f _x1))
  | Properties(_x1) -> Properties((map_instance_predicate f _x1))
  | Values(_x1) -> Values(((List.map (fun (_x1, _x2) -> ((map_instance_id f _x1), (map_instance_expr f _x2)))) _x1))
  | VariablesConcrete(_x1) -> VariablesConcrete((map_instance_ids f _x1))
  | VariablesAbstract(_x1) -> VariablesAbstract((map_instance_ids f _x1))
  | VariablesHidden(_x1) -> VariablesHidden((map_instance_ids f _x1))
  | Promotes(_x1) -> Promotes((map_instance_ids f _x1))
  | Assertions(_x1) -> Assertions(((List.map (map_instance_predicate f)) _x1))
  | Operations(_x1) -> Operations(((List.map (map_instance_operation f)) _x1))
  | Sees(_x1) -> Sees((map_instance_ids f _x1))
  | Uses(_x1) -> Uses((map_instance_ids f _x1))
  | Extends(_x1) -> Extends(((List.map (map_instance_instance f)) _x1))
  | Includes(_x1) -> Includes(((List.map (map_instance_instance f)) _x1))
  | Imports(_x1) -> Imports(((List.map (map_instance_instance f)) _x1))
  | Refines(_x1) -> Refines((map_instance_id f _x1))
  and map_instance_def f  = function
    Def(_x1, _x2, _x3) -> Def((map_instance_id f _x1), (map_instance_ids f _x2), (map_instance_defBody f _x3))
  | FileDef(_x1) -> FileDef((map_instance_string f _x1))
  and map_instance_defBody f  = function
    ExprDefBody(_x1) -> ExprDefBody((map_instance_expr f _x1))
  | SubstDefBody(_x1) -> SubstDefBody((map_instance_substitution f _x1))
  and map_instance_set f  = function
    SetAbstDec(_x1) -> SetAbstDec((map_instance_id f _x1))
  | SetEnumDec(_x1, _x2) -> SetEnumDec((map_instance_id f _x1), (map_instance_ids f _x2))

  and map_instance_instance f  = fun _x -> f((fun (_x1, _x2) -> (map_instance_id f _x1), ((List.map (map_instance_expr f)) _x2))_x)
  and map_instance_type_B f  = function
    TypeNatSetRange(_x1, _x2) -> TypeNatSetRange((map_instance_expr f _x1), (map_instance_expr f _x2))
  | PredType(_x1) -> PredType((map_instance_expr f _x1))

  | TypeIdentifier(_x1) -> TypeIdentifier((map_instance_id f _x1))
  | PFunType(_x1, _x2) -> PFunType((map_instance_type_B f _x1), (map_instance_type_B f _x2))
  | FunType(_x1, _x2) -> FunType((map_instance_type_B f _x1), (map_instance_type_B f _x2))
  | ProdType(_x1, _x2) -> ProdType((map_instance_type_B f _x1), (map_instance_type_B f _x2))
  | Sequence(_x1) -> Sequence((map_instance_type_B f _x1))
  | SubType(_x1, _x2) -> SubType((map_instance_type_B f _x1), (map_instance_expr f _x2))
  | Fin_B(_x1) -> Fin_B((map_instance_type_B f _x1))
  | Untyped -> Untyped 
  and map_instance_field f  = fun (_x1, _x2) -> (map_instance_id f _x1), (map_instance_type_B f _x2)
  and map_instance_predicate f  = function
    PredParen(_x1) -> PredParen((map_instance_predicate f _x1))
  | PredNegation(_x1) -> PredNegation((map_instance_predicate f _x1))
  | PredExists(_x1, _x2) -> PredExists(((List.map (map_instance_id f)) _x1), (map_instance_predicate f _x2))
  | PredForAll(_x1, _x2) -> PredForAll(((List.map (map_instance_id f)) _x1), (map_instance_predicate f _x2))
  | PredBin(_x1, _x2, _x3) -> PredBin(_x1, (map_instance_predicate f _x2), (map_instance_predicate f _x3))
  | PredAtom(_x1, _x2, _x3) -> PredAtom(_x1, (map_instance_expr f _x2), (map_instance_expr f _x3))
  and map_instance_operation f  = fun (_x1, _x2, _x3, _x4) -> (map_instance_id f _x1), (map_instance_ids f _x2), (map_instance_ids f _x3), (map_instance_substitution f _x4)
  and map_instance_expr f  = function
    ExprSequence(_x1, _x2) -> ExprSequence((map_instance_expr f _x1), (map_instance_expr f _x2))
  | ExprParen(_x1) -> ExprParen((map_instance_expr f _x1))
  | ExprId(_x1) -> ExprId((map_instance_id f _x1))
  | ExprFunCall(_x1, _x2) -> ExprFunCall((map_instance_expr f _x1), ((List.map (map_instance_expr f)) _x2))
  | ExprBin(_x1, _x2, _x3) -> ExprBin(_x1, (map_instance_expr f _x2), (map_instance_expr f _x3))
  | ExprUn(_x1, _x2) -> ExprUn(_x1, (map_instance_expr f _x2))
  | ExprSeq(_x1) -> ExprSeq((map_instance_exprSeq f _x1))
  | ExprSet(_x1) -> ExprSet((map_instance_exprSet f _x1))
  | RelSet(_x1, _x2) -> RelSet((map_instance_expr f _x1), (map_instance_expr f _x2))
  | RelSeqComp(_x1) -> RelSeqComp(((List.map (map_instance_expr f)) _x1))
  | ExprNumber(_x1) -> ExprNumber((map_instance_number f _x1))
  | ExprBool(_x1) -> ExprBool((map_instance_bool f _x1))
  | ExprString(_x1) -> ExprString((map_instance_string f _x1))

  | ExprPred(_x1) -> ExprPred((map_instance_predicate f _x1))
  | ExprNuplet(_x1) -> ExprNuplet(((List.map (map_instance_expr f)) _x1))
  | ExprSIGMA(_x1, _x2, _x3) -> ExprSIGMA(((List.map (map_instance_id f)) _x1), (map_instance_predicate f _x2), (map_instance_expr f _x3))
  | ExprPI(_x1, _x2, _x3) -> ExprPI(((List.map (map_instance_id f)) _x1), (map_instance_predicate f _x2), (map_instance_expr f _x3))
  | ExprLambda(_x1, _x2, _x3) -> ExprLambda(((List.map (map_instance_id f)) _x1), (map_instance_predicate f _x2), (map_instance_expr f _x3))

  and map_instance_exprSeq f  = function
    SeqEnum(_x1) -> SeqEnum(((List.map (map_instance_expr f)) _x1))
  | SeqEmpty -> SeqEmpty 
  and map_instance_exprSet f  = function
    SetRange(_x1, _x2) -> SetRange((map_instance_expr f _x1), (map_instance_expr f _x2))
  | SetEnum(_x1) -> SetEnum(((List.map (map_instance_expr f)) _x1))
  | SetCompr(_x1) -> SetCompr(((List.map (map_instance_expr f)) _x1))
  | SetComprPred(_x1, _x2) -> SetComprPred(((List.map (map_instance_id f)) _x1), (map_instance_predicate f _x2))
  | SetEmpty -> SetEmpty 
  | SetUnionQuantify(_x1, _x2, _x3) -> SetUnionQuantify(((List.map (map_instance_id f)) _x1), (map_instance_predicate f _x2), (map_instance_expr f _x3))
  | SetInterQuantify(_x1, _x2, _x3) -> SetInterQuantify(((List.map (map_instance_id f)) _x1), (map_instance_predicate f _x2), (map_instance_expr f _x3))
  and map_instance_number f  = function
    MinNumber -> MinNumber 
  | Number(_x1) -> Number((map_instance_int f _x1))
  | MaxNumber -> MaxNumber 
  and map_instance_substitution f  = function
    SubstOperCall(_x1, _x2, _x3) -> SubstOperCall((map_instance_ids f _x1), (map_instance_id f _x2), ((List.map (map_instance_expr f)) _x3))
  | SubstBlock(_x1) -> SubstBlock((map_instance_substitution f _x1))
  | SubstPrecondition(_x1, _x2) -> SubstPrecondition((map_instance_predicate f _x1), (map_instance_substitution f _x2))
  | SubstAssertion(_x1, _x2) -> SubstAssertion((map_instance_predicate f _x1), (map_instance_substitution f _x2))
  | SubstChoice(_x1) -> SubstChoice(((List.map (map_instance_substitution f)) _x1))
  | SubstIf(_x1, _x2) -> SubstIf(((List.map (fun (_x1, _x2) -> ((map_instance_predicate f _x1), (map_instance_substitution f _x2)))) _x1), ((map_instance_option (map_instance_substitution f)) _x2))
  | SubstSelect(_x1, _x2) -> SubstSelect(((List.map (fun (_x1, _x2) -> ((map_instance_predicate f _x1), (map_instance_substitution f _x2)))) _x1), ((map_instance_option (map_instance_substitution f)) _x2))
  | SubstCase(_x1, _x2, _x3) -> SubstCase((map_instance_expr f _x1), ((List.map (fun (_x1, _x2) -> (((List.map (map_instance_expr f)) _x1), (map_instance_substitution f _x2)))) _x2), ((map_instance_option (map_instance_substitution f)) _x3))
  | SubstAny(_x1, _x2, _x3) -> SubstAny(((List.map (map_instance_id f)) _x1), (map_instance_predicate f _x2), (map_instance_substitution f _x3))
  | SubstLet(_x1, _x2, _x3) -> SubstLet(((List.map (map_instance_id f)) _x1), (map_instance_predicate f _x2), (map_instance_substitution f _x3))
  | SubstVar(_x1, _x2) -> SubstVar(((List.map (map_instance_id f)) _x1), (map_instance_substitution f _x2))
  | SubstWhile(_x1, _x2, _x3, _x4) -> SubstWhile((map_instance_predicate f _x1), (map_instance_substitution f _x2), (map_instance_expr f _x3), (map_instance_predicate f _x4))
  | SubstSkip -> SubstSkip 
  | SubstSequence(_x1, _x2) -> SubstSequence((map_instance_substitution f _x1), (map_instance_substitution f _x2))
  | SubstParallel(_x1, _x2) -> SubstParallel((map_instance_substitution f _x1), (map_instance_substitution f _x2))
  | SubstSetEqualIds(_x1, _x2) -> SubstSetEqualIds((map_instance_ids f _x1), ((List.map (map_instance_expr f)) _x2))
  | SubstSetEqualFun(_x1, _x2, _x3) -> SubstSetEqualFun((map_instance_expr f _x1), ((List.map (map_instance_expr f)) _x2), (map_instance_expr f _x3))

  | SubstBecomeSuch(_x1, _x2) -> SubstBecomeSuch((map_instance_ids f _x1), (map_instance_predicate f _x2))
  | SubstSetIn(_x1, _x2) -> SubstSetIn((map_instance_ids f _x1), (map_instance_expr f _x2))
  and map_instance_ids f  = fun (_x1) -> ((List.map (map_instance_id f)) _x1)
  and map_instance_origine f  = function
    Local -> Local 
  | Remote(_x1) -> Remote(((List.map (fun (_x1, _x2) -> ((map_instance_string f _x1), (map_instance_string f _x2)))) _x1))
  | OrgRefine(_x1, _x2) -> OrgRefine((map_instance_string f _x1), ((List.map (fun (_x1, _x2) -> ((map_instance_string f _x1), (map_instance_string f _x2)))) _x2))
  | Prefix(_x1) -> Prefix(((List.map (map_instance_string f)) _x1))
  and map_instance_id f  = function
    Id(_x1, _x2, _x3, _x4) -> Id((map_instance_origine f _x1), (map_instance_string f _x2), ((map_instance_option (map_instance_type_B f)) _x3), _x4)

let map_type_B_string f (s:string) = s
let map_type_B_int f (i:int32) = i
let map_type_B_bool f (b:bool) = b
let map_type_B_option f = function Some x -> Some (f x) | None -> None
let rec map_type_B_amn f  = function
    Machine(_x1, _x2) -> Machine((map_type_B_head f _x1), ((List.map (map_type_B_clause f)) _x2))
  | Refinement(_x1, _x2) -> Refinement((map_type_B_head f _x1), ((List.map (map_type_B_clause f)) _x2))
  | Implementation(_x1, _x2) -> Implementation((map_type_B_head f _x1), ((List.map (map_type_B_clause f)) _x2))
  | EmptyTree -> EmptyTree 
  and map_type_B_head f  = fun (_x1, _x2) -> (map_type_B_id f _x1), (map_type_B_ids f _x2)
  and map_type_B_clause f  = function
    Definitions(_x1) -> Definitions(((List.map (map_type_B_def f)) _x1))
  | Constraints(_x1) -> Constraints((map_type_B_predicate f _x1))
  | Invariant(_x1) -> Invariant((map_type_B_predicate f _x1))
  | Sets(_x1) -> Sets(((List.map (map_type_B_set f)) _x1))
  | Initialisation(_x1) -> Initialisation((map_type_B_substitution f _x1))
  | ConstantsConcrete(_x1) -> ConstantsConcrete((map_type_B_ids f _x1))
  | ConstantsAbstract(_x1) -> ConstantsAbstract((map_type_B_ids f _x1))
  | ConstantsHidden(_x1) -> ConstantsHidden((map_type_B_ids f _x1))
  | Properties(_x1) -> Properties((map_type_B_predicate f _x1))
  | Values(_x1) -> Values(((List.map (fun (_x1, _x2) -> ((map_type_B_id f _x1), (map_type_B_expr f _x2)))) _x1))
  | VariablesConcrete(_x1) -> VariablesConcrete((map_type_B_ids f _x1))
  | VariablesAbstract(_x1) -> VariablesAbstract((map_type_B_ids f _x1))
  | VariablesHidden(_x1) -> VariablesHidden((map_type_B_ids f _x1))
  | Promotes(_x1) -> Promotes((map_type_B_ids f _x1))
  | Assertions(_x1) -> Assertions(((List.map (map_type_B_predicate f)) _x1))
  | Operations(_x1) -> Operations(((List.map (map_type_B_operation f)) _x1))
  | Sees(_x1) -> Sees((map_type_B_ids f _x1))
  | Uses(_x1) -> Uses((map_type_B_ids f _x1))
  | Extends(_x1) -> Extends(((List.map (map_type_B_instance f)) _x1))
  | Includes(_x1) -> Includes(((List.map (map_type_B_instance f)) _x1))
  | Imports(_x1) -> Imports(((List.map (map_type_B_instance f)) _x1))
  | Refines(_x1) -> Refines((map_type_B_id f _x1))
  and map_type_B_def f  = function
    Def(_x1, _x2, _x3) -> Def((map_type_B_id f _x1), (map_type_B_ids f _x2), (map_type_B_defBody f _x3))
  | FileDef(_x1) -> FileDef((map_type_B_string f _x1))
  and map_type_B_defBody f  = function
    ExprDefBody(_x1) -> ExprDefBody((map_type_B_expr f _x1))
  | SubstDefBody(_x1) -> SubstDefBody((map_type_B_substitution f _x1))
  and map_type_B_set f  = function
    SetAbstDec(_x1) -> SetAbstDec((map_type_B_id f _x1))
  | SetEnumDec(_x1, _x2) -> SetEnumDec((map_type_B_id f _x1), (map_type_B_ids f _x2))

  and map_type_B_instance f  = fun (_x1, _x2) -> (map_type_B_id f _x1), ((List.map (map_type_B_expr f)) _x2)
  and map_type_B_type_B f  = fun _x -> f((function
    TypeNatSetRange(_x1, _x2) -> TypeNatSetRange((map_type_B_expr f _x1), (map_type_B_expr f _x2))
  | PredType(_x1) -> PredType((map_type_B_expr f _x1))

  | TypeIdentifier(_x1) -> TypeIdentifier((map_type_B_id f _x1))
  | PFunType(_x1, _x2) -> PFunType((map_type_B_type_B f _x1), (map_type_B_type_B f _x2))
  | FunType(_x1, _x2) -> FunType((map_type_B_type_B f _x1), (map_type_B_type_B f _x2))
  | ProdType(_x1, _x2) -> ProdType((map_type_B_type_B f _x1), (map_type_B_type_B f _x2))
  | Sequence(_x1) -> Sequence((map_type_B_type_B f _x1))
  | SubType(_x1, _x2) -> SubType((map_type_B_type_B f _x1), (map_type_B_expr f _x2))
  | Fin_B(_x1) -> Fin_B((map_type_B_type_B f _x1))
  | Untyped -> Untyped )_x)
  and map_type_B_field f  = fun (_x1, _x2) -> (map_type_B_id f _x1), (map_type_B_type_B f _x2)
  and map_type_B_predicate f  = function
    PredParen(_x1) -> PredParen((map_type_B_predicate f _x1))
  | PredNegation(_x1) -> PredNegation((map_type_B_predicate f _x1))
  | PredExists(_x1, _x2) -> PredExists(((List.map (map_type_B_id f)) _x1), (map_type_B_predicate f _x2))
  | PredForAll(_x1, _x2) -> PredForAll(((List.map (map_type_B_id f)) _x1), (map_type_B_predicate f _x2))
  | PredBin(_x1, _x2, _x3) -> PredBin(_x1, (map_type_B_predicate f _x2), (map_type_B_predicate f _x3))
  | PredAtom(_x1, _x2, _x3) -> PredAtom(_x1, (map_type_B_expr f _x2), (map_type_B_expr f _x3))
  and map_type_B_operation f  = fun (_x1, _x2, _x3, _x4) -> (map_type_B_id f _x1), (map_type_B_ids f _x2), (map_type_B_ids f _x3), (map_type_B_substitution f _x4)
  and map_type_B_expr f  = function
    ExprSequence(_x1, _x2) -> ExprSequence((map_type_B_expr f _x1), (map_type_B_expr f _x2))
  | ExprParen(_x1) -> ExprParen((map_type_B_expr f _x1))
  | ExprId(_x1) -> ExprId((map_type_B_id f _x1))
  | ExprFunCall(_x1, _x2) -> ExprFunCall((map_type_B_expr f _x1), ((List.map (map_type_B_expr f)) _x2))
  | ExprBin(_x1, _x2, _x3) -> ExprBin(_x1, (map_type_B_expr f _x2), (map_type_B_expr f _x3))
  | ExprUn(_x1, _x2) -> ExprUn(_x1, (map_type_B_expr f _x2))
  | ExprSeq(_x1) -> ExprSeq((map_type_B_exprSeq f _x1))
  | ExprSet(_x1) -> ExprSet((map_type_B_exprSet f _x1))
  | RelSet(_x1, _x2) -> RelSet((map_type_B_expr f _x1), (map_type_B_expr f _x2))
  | RelSeqComp(_x1) -> RelSeqComp(((List.map (map_type_B_expr f)) _x1))
  | ExprNumber(_x1) -> ExprNumber((map_type_B_number f _x1))
  | ExprBool(_x1) -> ExprBool((map_type_B_bool f _x1))
  | ExprString(_x1) -> ExprString((map_type_B_string f _x1))

  | ExprPred(_x1) -> ExprPred((map_type_B_predicate f _x1))
  | ExprNuplet(_x1) -> ExprNuplet(((List.map (map_type_B_expr f)) _x1))
  | ExprSIGMA(_x1, _x2, _x3) -> ExprSIGMA(((List.map (map_type_B_id f)) _x1), (map_type_B_predicate f _x2), (map_type_B_expr f _x3))
  | ExprPI(_x1, _x2, _x3) -> ExprPI(((List.map (map_type_B_id f)) _x1), (map_type_B_predicate f _x2), (map_type_B_expr f _x3))
  | ExprLambda(_x1, _x2, _x3) -> ExprLambda(((List.map (map_type_B_id f)) _x1), (map_type_B_predicate f _x2), (map_type_B_expr f _x3))

  and map_type_B_exprSeq f  = function
    SeqEnum(_x1) -> SeqEnum(((List.map (map_type_B_expr f)) _x1))
  | SeqEmpty -> SeqEmpty 
  and map_type_B_exprSet f  = function
    SetRange(_x1, _x2) -> SetRange((map_type_B_expr f _x1), (map_type_B_expr f _x2))
  | SetEnum(_x1) -> SetEnum(((List.map (map_type_B_expr f)) _x1))
  | SetCompr(_x1) -> SetCompr(((List.map (map_type_B_expr f)) _x1))
  | SetComprPred(_x1, _x2) -> SetComprPred(((List.map (map_type_B_id f)) _x1), (map_type_B_predicate f _x2))
  | SetEmpty -> SetEmpty 
  | SetUnionQuantify(_x1, _x2, _x3) -> SetUnionQuantify(((List.map (map_type_B_id f)) _x1), (map_type_B_predicate f _x2), (map_type_B_expr f _x3))
  | SetInterQuantify(_x1, _x2, _x3) -> SetInterQuantify(((List.map (map_type_B_id f)) _x1), (map_type_B_predicate f _x2), (map_type_B_expr f _x3))
  and map_type_B_number f  = function
    MinNumber -> MinNumber 
  | Number(_x1) -> Number((map_type_B_int f _x1))
  | MaxNumber -> MaxNumber 
  and map_type_B_substitution f  = function
    SubstOperCall(_x1, _x2, _x3) -> SubstOperCall((map_type_B_ids f _x1), (map_type_B_id f _x2), ((List.map (map_type_B_expr f)) _x3))
  | SubstBlock(_x1) -> SubstBlock((map_type_B_substitution f _x1))
  | SubstPrecondition(_x1, _x2) -> SubstPrecondition((map_type_B_predicate f _x1), (map_type_B_substitution f _x2))
  | SubstAssertion(_x1, _x2) -> SubstAssertion((map_type_B_predicate f _x1), (map_type_B_substitution f _x2))
  | SubstChoice(_x1) -> SubstChoice(((List.map (map_type_B_substitution f)) _x1))
  | SubstIf(_x1, _x2) -> SubstIf(((List.map (fun (_x1, _x2) -> ((map_type_B_predicate f _x1), (map_type_B_substitution f _x2)))) _x1), ((map_type_B_option (map_type_B_substitution f)) _x2))
  | SubstSelect(_x1, _x2) -> SubstSelect(((List.map (fun (_x1, _x2) -> ((map_type_B_predicate f _x1), (map_type_B_substitution f _x2)))) _x1), ((map_type_B_option (map_type_B_substitution f)) _x2))
  | SubstCase(_x1, _x2, _x3) -> SubstCase((map_type_B_expr f _x1), ((List.map (fun (_x1, _x2) -> (((List.map (map_type_B_expr f)) _x1), (map_type_B_substitution f _x2)))) _x2), ((map_type_B_option (map_type_B_substitution f)) _x3))
  | SubstAny(_x1, _x2, _x3) -> SubstAny(((List.map (map_type_B_id f)) _x1), (map_type_B_predicate f _x2), (map_type_B_substitution f _x3))
  | SubstLet(_x1, _x2, _x3) -> SubstLet(((List.map (map_type_B_id f)) _x1), (map_type_B_predicate f _x2), (map_type_B_substitution f _x3))
  | SubstVar(_x1, _x2) -> SubstVar(((List.map (map_type_B_id f)) _x1), (map_type_B_substitution f _x2))
  | SubstWhile(_x1, _x2, _x3, _x4) -> SubstWhile((map_type_B_predicate f _x1), (map_type_B_substitution f _x2), (map_type_B_expr f _x3), (map_type_B_predicate f _x4))
  | SubstSkip -> SubstSkip 
  | SubstSequence(_x1, _x2) -> SubstSequence((map_type_B_substitution f _x1), (map_type_B_substitution f _x2))
  | SubstParallel(_x1, _x2) -> SubstParallel((map_type_B_substitution f _x1), (map_type_B_substitution f _x2))
  | SubstSetEqualIds(_x1, _x2) -> SubstSetEqualIds((map_type_B_ids f _x1), ((List.map (map_type_B_expr f)) _x2))
  | SubstSetEqualFun(_x1, _x2, _x3) -> SubstSetEqualFun((map_type_B_expr f _x1), ((List.map (map_type_B_expr f)) _x2), (map_type_B_expr f _x3))

  | SubstBecomeSuch(_x1, _x2) -> SubstBecomeSuch((map_type_B_ids f _x1), (map_type_B_predicate f _x2))
  | SubstSetIn(_x1, _x2) -> SubstSetIn((map_type_B_ids f _x1), (map_type_B_expr f _x2))
  and map_type_B_ids f  = fun (_x1) -> ((List.map (map_type_B_id f)) _x1)
  and map_type_B_origine f  = function
    Local -> Local 
  | Remote(_x1) -> Remote(((List.map (fun (_x1, _x2) -> ((map_type_B_string f _x1), (map_type_B_string f _x2)))) _x1))
  | OrgRefine(_x1, _x2) -> OrgRefine((map_type_B_string f _x1), ((List.map (fun (_x1, _x2) -> ((map_type_B_string f _x1), (map_type_B_string f _x2)))) _x2))
  | Prefix(_x1) -> Prefix(((List.map (map_type_B_string f)) _x1))
  and map_type_B_id f  = function
    Id(_x1, _x2, _x3, _x4) -> Id((map_type_B_origine f _x1), (map_type_B_string f _x2), ((map_type_B_option (map_type_B_type_B f)) _x3), _x4)

let map_field_string f (s:string) = s
let map_field_int f (i:int32) = i
let map_field_bool f (b:bool) = b
let map_field_option f = function Some x -> Some (f x) | None -> None
let rec map_field_amn f  = function
    Machine(_x1, _x2) -> Machine((map_field_head f _x1), ((List.map (map_field_clause f)) _x2))
  | Refinement(_x1, _x2) -> Refinement((map_field_head f _x1), ((List.map (map_field_clause f)) _x2))
  | Implementation(_x1, _x2) -> Implementation((map_field_head f _x1), ((List.map (map_field_clause f)) _x2))
  | EmptyTree -> EmptyTree 
  and map_field_head f  = fun (_x1, _x2) -> (map_field_id f _x1), (map_field_ids f _x2)
  and map_field_clause f  = function
    Definitions(_x1) -> Definitions(((List.map (map_field_def f)) _x1))
  | Constraints(_x1) -> Constraints((map_field_predicate f _x1))
  | Invariant(_x1) -> Invariant((map_field_predicate f _x1))
  | Sets(_x1) -> Sets(((List.map (map_field_set f)) _x1))
  | Initialisation(_x1) -> Initialisation((map_field_substitution f _x1))
  | ConstantsConcrete(_x1) -> ConstantsConcrete((map_field_ids f _x1))
  | ConstantsAbstract(_x1) -> ConstantsAbstract((map_field_ids f _x1))
  | ConstantsHidden(_x1) -> ConstantsHidden((map_field_ids f _x1))
  | Properties(_x1) -> Properties((map_field_predicate f _x1))
  | Values(_x1) -> Values(((List.map (fun (_x1, _x2) -> ((map_field_id f _x1), (map_field_expr f _x2)))) _x1))
  | VariablesConcrete(_x1) -> VariablesConcrete((map_field_ids f _x1))
  | VariablesAbstract(_x1) -> VariablesAbstract((map_field_ids f _x1))
  | VariablesHidden(_x1) -> VariablesHidden((map_field_ids f _x1))
  | Promotes(_x1) -> Promotes((map_field_ids f _x1))
  | Assertions(_x1) -> Assertions(((List.map (map_field_predicate f)) _x1))
  | Operations(_x1) -> Operations(((List.map (map_field_operation f)) _x1))
  | Sees(_x1) -> Sees((map_field_ids f _x1))
  | Uses(_x1) -> Uses((map_field_ids f _x1))
  | Extends(_x1) -> Extends(((List.map (map_field_instance f)) _x1))
  | Includes(_x1) -> Includes(((List.map (map_field_instance f)) _x1))
  | Imports(_x1) -> Imports(((List.map (map_field_instance f)) _x1))
  | Refines(_x1) -> Refines((map_field_id f _x1))
  and map_field_def f  = function
    Def(_x1, _x2, _x3) -> Def((map_field_id f _x1), (map_field_ids f _x2), (map_field_defBody f _x3))
  | FileDef(_x1) -> FileDef((map_field_string f _x1))
  and map_field_defBody f  = function
    ExprDefBody(_x1) -> ExprDefBody((map_field_expr f _x1))
  | SubstDefBody(_x1) -> SubstDefBody((map_field_substitution f _x1))
  and map_field_set f  = function
    SetAbstDec(_x1) -> SetAbstDec((map_field_id f _x1))
  | SetEnumDec(_x1, _x2) -> SetEnumDec((map_field_id f _x1), (map_field_ids f _x2))

  and map_field_instance f  = fun (_x1, _x2) -> (map_field_id f _x1), ((List.map (map_field_expr f)) _x2)
  and map_field_type_B f  = function
    TypeNatSetRange(_x1, _x2) -> TypeNatSetRange((map_field_expr f _x1), (map_field_expr f _x2))
  | PredType(_x1) -> PredType((map_field_expr f _x1))

  | TypeIdentifier(_x1) -> TypeIdentifier((map_field_id f _x1))
  | PFunType(_x1, _x2) -> PFunType((map_field_type_B f _x1), (map_field_type_B f _x2))
  | FunType(_x1, _x2) -> FunType((map_field_type_B f _x1), (map_field_type_B f _x2))
  | ProdType(_x1, _x2) -> ProdType((map_field_type_B f _x1), (map_field_type_B f _x2))
  | Sequence(_x1) -> Sequence((map_field_type_B f _x1))
  | SubType(_x1, _x2) -> SubType((map_field_type_B f _x1), (map_field_expr f _x2))
  | Fin_B(_x1) -> Fin_B((map_field_type_B f _x1))
  | Untyped -> Untyped 
  and map_field_field f  = fun _x -> f((fun (_x1, _x2) -> (map_field_id f _x1), (map_field_type_B f _x2))_x)
  and map_field_predicate f  = function
    PredParen(_x1) -> PredParen((map_field_predicate f _x1))
  | PredNegation(_x1) -> PredNegation((map_field_predicate f _x1))
  | PredExists(_x1, _x2) -> PredExists(((List.map (map_field_id f)) _x1), (map_field_predicate f _x2))
  | PredForAll(_x1, _x2) -> PredForAll(((List.map (map_field_id f)) _x1), (map_field_predicate f _x2))
  | PredBin(_x1, _x2, _x3) -> PredBin(_x1, (map_field_predicate f _x2), (map_field_predicate f _x3))
  | PredAtom(_x1, _x2, _x3) -> PredAtom(_x1, (map_field_expr f _x2), (map_field_expr f _x3))
  and map_field_operation f  = fun (_x1, _x2, _x3, _x4) -> (map_field_id f _x1), (map_field_ids f _x2), (map_field_ids f _x3), (map_field_substitution f _x4)
  and map_field_expr f  = function
    ExprSequence(_x1, _x2) -> ExprSequence((map_field_expr f _x1), (map_field_expr f _x2))
  | ExprParen(_x1) -> ExprParen((map_field_expr f _x1))
  | ExprId(_x1) -> ExprId((map_field_id f _x1))
  | ExprFunCall(_x1, _x2) -> ExprFunCall((map_field_expr f _x1), ((List.map (map_field_expr f)) _x2))
  | ExprBin(_x1, _x2, _x3) -> ExprBin(_x1, (map_field_expr f _x2), (map_field_expr f _x3))
  | ExprUn(_x1, _x2) -> ExprUn(_x1, (map_field_expr f _x2))
  | ExprSeq(_x1) -> ExprSeq((map_field_exprSeq f _x1))
  | ExprSet(_x1) -> ExprSet((map_field_exprSet f _x1))
  | RelSet(_x1, _x2) -> RelSet((map_field_expr f _x1), (map_field_expr f _x2))
  | RelSeqComp(_x1) -> RelSeqComp(((List.map (map_field_expr f)) _x1))
  | ExprNumber(_x1) -> ExprNumber((map_field_number f _x1))
  | ExprBool(_x1) -> ExprBool((map_field_bool f _x1))
  | ExprString(_x1) -> ExprString((map_field_string f _x1))

  | ExprPred(_x1) -> ExprPred((map_field_predicate f _x1))
  | ExprNuplet(_x1) -> ExprNuplet(((List.map (map_field_expr f)) _x1))
  | ExprSIGMA(_x1, _x2, _x3) -> ExprSIGMA(((List.map (map_field_id f)) _x1), (map_field_predicate f _x2), (map_field_expr f _x3))
  | ExprPI(_x1, _x2, _x3) -> ExprPI(((List.map (map_field_id f)) _x1), (map_field_predicate f _x2), (map_field_expr f _x3))
  | ExprLambda(_x1, _x2, _x3) -> ExprLambda(((List.map (map_field_id f)) _x1), (map_field_predicate f _x2), (map_field_expr f _x3))

  and map_field_exprSeq f  = function
    SeqEnum(_x1) -> SeqEnum(((List.map (map_field_expr f)) _x1))
  | SeqEmpty -> SeqEmpty 
  and map_field_exprSet f  = function
    SetRange(_x1, _x2) -> SetRange((map_field_expr f _x1), (map_field_expr f _x2))
  | SetEnum(_x1) -> SetEnum(((List.map (map_field_expr f)) _x1))
  | SetCompr(_x1) -> SetCompr(((List.map (map_field_expr f)) _x1))
  | SetComprPred(_x1, _x2) -> SetComprPred(((List.map (map_field_id f)) _x1), (map_field_predicate f _x2))
  | SetEmpty -> SetEmpty 
  | SetUnionQuantify(_x1, _x2, _x3) -> SetUnionQuantify(((List.map (map_field_id f)) _x1), (map_field_predicate f _x2), (map_field_expr f _x3))
  | SetInterQuantify(_x1, _x2, _x3) -> SetInterQuantify(((List.map (map_field_id f)) _x1), (map_field_predicate f _x2), (map_field_expr f _x3))
  and map_field_number f  = function
    MinNumber -> MinNumber 
  | Number(_x1) -> Number((map_field_int f _x1))
  | MaxNumber -> MaxNumber 
  and map_field_substitution f  = function
    SubstOperCall(_x1, _x2, _x3) -> SubstOperCall((map_field_ids f _x1), (map_field_id f _x2), ((List.map (map_field_expr f)) _x3))
  | SubstBlock(_x1) -> SubstBlock((map_field_substitution f _x1))
  | SubstPrecondition(_x1, _x2) -> SubstPrecondition((map_field_predicate f _x1), (map_field_substitution f _x2))
  | SubstAssertion(_x1, _x2) -> SubstAssertion((map_field_predicate f _x1), (map_field_substitution f _x2))
  | SubstChoice(_x1) -> SubstChoice(((List.map (map_field_substitution f)) _x1))
  | SubstIf(_x1, _x2) -> SubstIf(((List.map (fun (_x1, _x2) -> ((map_field_predicate f _x1), (map_field_substitution f _x2)))) _x1), ((map_field_option (map_field_substitution f)) _x2))
  | SubstSelect(_x1, _x2) -> SubstSelect(((List.map (fun (_x1, _x2) -> ((map_field_predicate f _x1), (map_field_substitution f _x2)))) _x1), ((map_field_option (map_field_substitution f)) _x2))
  | SubstCase(_x1, _x2, _x3) -> SubstCase((map_field_expr f _x1), ((List.map (fun (_x1, _x2) -> (((List.map (map_field_expr f)) _x1), (map_field_substitution f _x2)))) _x2), ((map_field_option (map_field_substitution f)) _x3))
  | SubstAny(_x1, _x2, _x3) -> SubstAny(((List.map (map_field_id f)) _x1), (map_field_predicate f _x2), (map_field_substitution f _x3))
  | SubstLet(_x1, _x2, _x3) -> SubstLet(((List.map (map_field_id f)) _x1), (map_field_predicate f _x2), (map_field_substitution f _x3))
  | SubstVar(_x1, _x2) -> SubstVar(((List.map (map_field_id f)) _x1), (map_field_substitution f _x2))
  | SubstWhile(_x1, _x2, _x3, _x4) -> SubstWhile((map_field_predicate f _x1), (map_field_substitution f _x2), (map_field_expr f _x3), (map_field_predicate f _x4))
  | SubstSkip -> SubstSkip 
  | SubstSequence(_x1, _x2) -> SubstSequence((map_field_substitution f _x1), (map_field_substitution f _x2))
  | SubstParallel(_x1, _x2) -> SubstParallel((map_field_substitution f _x1), (map_field_substitution f _x2))
  | SubstSetEqualIds(_x1, _x2) -> SubstSetEqualIds((map_field_ids f _x1), ((List.map (map_field_expr f)) _x2))
  | SubstSetEqualFun(_x1, _x2, _x3) -> SubstSetEqualFun((map_field_expr f _x1), ((List.map (map_field_expr f)) _x2), (map_field_expr f _x3))

  | SubstBecomeSuch(_x1, _x2) -> SubstBecomeSuch((map_field_ids f _x1), (map_field_predicate f _x2))
  | SubstSetIn(_x1, _x2) -> SubstSetIn((map_field_ids f _x1), (map_field_expr f _x2))
  and map_field_ids f  = fun (_x1) -> ((List.map (map_field_id f)) _x1)
  and map_field_origine f  = function
    Local -> Local 
  | Remote(_x1) -> Remote(((List.map (fun (_x1, _x2) -> ((map_field_string f _x1), (map_field_string f _x2)))) _x1))
  | OrgRefine(_x1, _x2) -> OrgRefine((map_field_string f _x1), ((List.map (fun (_x1, _x2) -> ((map_field_string f _x1), (map_field_string f _x2)))) _x2))
  | Prefix(_x1) -> Prefix(((List.map (map_field_string f)) _x1))
  and map_field_id f  = function
    Id(_x1, _x2, _x3, _x4) -> Id((map_field_origine f _x1), (map_field_string f _x2), ((map_field_option (map_field_type_B f)) _x3), _x4)

let map_predicate_string f (s:string) = s
let map_predicate_int f (i:int32) = i
let map_predicate_bool f (b:bool) = b
let map_predicate_option f = function Some x -> Some (f x) | None -> None
let rec map_predicate_amn f  = function
    Machine(_x1, _x2) -> Machine((map_predicate_head f _x1), ((List.map (map_predicate_clause f)) _x2))
  | Refinement(_x1, _x2) -> Refinement((map_predicate_head f _x1), ((List.map (map_predicate_clause f)) _x2))
  | Implementation(_x1, _x2) -> Implementation((map_predicate_head f _x1), ((List.map (map_predicate_clause f)) _x2))
  | EmptyTree -> EmptyTree 
  and map_predicate_head f  = fun (_x1, _x2) -> (map_predicate_id f _x1), (map_predicate_ids f _x2)
  and map_predicate_clause f  = function
    Definitions(_x1) -> Definitions(((List.map (map_predicate_def f)) _x1))
  | Constraints(_x1) -> Constraints((map_predicate_predicate f _x1))
  | Invariant(_x1) -> Invariant((map_predicate_predicate f _x1))
  | Sets(_x1) -> Sets(((List.map (map_predicate_set f)) _x1))
  | Initialisation(_x1) -> Initialisation((map_predicate_substitution f _x1))
  | ConstantsConcrete(_x1) -> ConstantsConcrete((map_predicate_ids f _x1))
  | ConstantsAbstract(_x1) -> ConstantsAbstract((map_predicate_ids f _x1))
  | ConstantsHidden(_x1) -> ConstantsHidden((map_predicate_ids f _x1))
  | Properties(_x1) -> Properties((map_predicate_predicate f _x1))
  | Values(_x1) -> Values(((List.map (fun (_x1, _x2) -> ((map_predicate_id f _x1), (map_predicate_expr f _x2)))) _x1))
  | VariablesConcrete(_x1) -> VariablesConcrete((map_predicate_ids f _x1))
  | VariablesAbstract(_x1) -> VariablesAbstract((map_predicate_ids f _x1))
  | VariablesHidden(_x1) -> VariablesHidden((map_predicate_ids f _x1))
  | Promotes(_x1) -> Promotes((map_predicate_ids f _x1))
  | Assertions(_x1) -> Assertions(((List.map (map_predicate_predicate f)) _x1))
  | Operations(_x1) -> Operations(((List.map (map_predicate_operation f)) _x1))
  | Sees(_x1) -> Sees((map_predicate_ids f _x1))
  | Uses(_x1) -> Uses((map_predicate_ids f _x1))
  | Extends(_x1) -> Extends(((List.map (map_predicate_instance f)) _x1))
  | Includes(_x1) -> Includes(((List.map (map_predicate_instance f)) _x1))
  | Imports(_x1) -> Imports(((List.map (map_predicate_instance f)) _x1))
  | Refines(_x1) -> Refines((map_predicate_id f _x1))
  and map_predicate_def f  = function
    Def(_x1, _x2, _x3) -> Def((map_predicate_id f _x1), (map_predicate_ids f _x2), (map_predicate_defBody f _x3))
  | FileDef(_x1) -> FileDef((map_predicate_string f _x1))
  and map_predicate_defBody f  = function
    ExprDefBody(_x1) -> ExprDefBody((map_predicate_expr f _x1))
  | SubstDefBody(_x1) -> SubstDefBody((map_predicate_substitution f _x1))
  and map_predicate_set f  = function
    SetAbstDec(_x1) -> SetAbstDec((map_predicate_id f _x1))
  | SetEnumDec(_x1, _x2) -> SetEnumDec((map_predicate_id f _x1), (map_predicate_ids f _x2))

  and map_predicate_instance f  = fun (_x1, _x2) -> (map_predicate_id f _x1), ((List.map (map_predicate_expr f)) _x2)
  and map_predicate_type_B f  = function
    TypeNatSetRange(_x1, _x2) -> TypeNatSetRange((map_predicate_expr f _x1), (map_predicate_expr f _x2))
  | PredType(_x1) -> PredType((map_predicate_expr f _x1))

  | TypeIdentifier(_x1) -> TypeIdentifier((map_predicate_id f _x1))
  | PFunType(_x1, _x2) -> PFunType((map_predicate_type_B f _x1), (map_predicate_type_B f _x2))
  | FunType(_x1, _x2) -> FunType((map_predicate_type_B f _x1), (map_predicate_type_B f _x2))
  | ProdType(_x1, _x2) -> ProdType((map_predicate_type_B f _x1), (map_predicate_type_B f _x2))
  | Sequence(_x1) -> Sequence((map_predicate_type_B f _x1))
  | SubType(_x1, _x2) -> SubType((map_predicate_type_B f _x1), (map_predicate_expr f _x2))
  | Fin_B(_x1) -> Fin_B((map_predicate_type_B f _x1))
  | Untyped -> Untyped 
  and map_predicate_field f  = fun (_x1, _x2) -> (map_predicate_id f _x1), (map_predicate_type_B f _x2)
  and map_predicate_predicate f  = fun _x -> f((function
    PredParen(_x1) -> PredParen((map_predicate_predicate f _x1))
  | PredNegation(_x1) -> PredNegation((map_predicate_predicate f _x1))
  | PredExists(_x1, _x2) -> PredExists(((List.map (map_predicate_id f)) _x1), (map_predicate_predicate f _x2))
  | PredForAll(_x1, _x2) -> PredForAll(((List.map (map_predicate_id f)) _x1), (map_predicate_predicate f _x2))
  | PredBin(_x1, _x2, _x3) -> PredBin(_x1, (map_predicate_predicate f _x2), (map_predicate_predicate f _x3))
  | PredAtom(_x1, _x2, _x3) -> PredAtom(_x1, (map_predicate_expr f _x2), (map_predicate_expr f _x3)))_x)
  and map_predicate_operation f  = fun (_x1, _x2, _x3, _x4) -> (map_predicate_id f _x1), (map_predicate_ids f _x2), (map_predicate_ids f _x3), (map_predicate_substitution f _x4)
  and map_predicate_expr f  = function
    ExprSequence(_x1, _x2) -> ExprSequence((map_predicate_expr f _x1), (map_predicate_expr f _x2))
  | ExprParen(_x1) -> ExprParen((map_predicate_expr f _x1))
  | ExprId(_x1) -> ExprId((map_predicate_id f _x1))
  | ExprFunCall(_x1, _x2) -> ExprFunCall((map_predicate_expr f _x1), ((List.map (map_predicate_expr f)) _x2))
  | ExprBin(_x1, _x2, _x3) -> ExprBin(_x1, (map_predicate_expr f _x2), (map_predicate_expr f _x3))
  | ExprUn(_x1, _x2) -> ExprUn(_x1, (map_predicate_expr f _x2))
  | ExprSeq(_x1) -> ExprSeq((map_predicate_exprSeq f _x1))
  | ExprSet(_x1) -> ExprSet((map_predicate_exprSet f _x1))
  | RelSet(_x1, _x2) -> RelSet((map_predicate_expr f _x1), (map_predicate_expr f _x2))
  | RelSeqComp(_x1) -> RelSeqComp(((List.map (map_predicate_expr f)) _x1))
  | ExprNumber(_x1) -> ExprNumber((map_predicate_number f _x1))
  | ExprBool(_x1) -> ExprBool((map_predicate_bool f _x1))
  | ExprString(_x1) -> ExprString((map_predicate_string f _x1))

  | ExprPred(_x1) -> ExprPred((map_predicate_predicate f _x1))
  | ExprNuplet(_x1) -> ExprNuplet(((List.map (map_predicate_expr f)) _x1))
  | ExprSIGMA(_x1, _x2, _x3) -> ExprSIGMA(((List.map (map_predicate_id f)) _x1), (map_predicate_predicate f _x2), (map_predicate_expr f _x3))
  | ExprPI(_x1, _x2, _x3) -> ExprPI(((List.map (map_predicate_id f)) _x1), (map_predicate_predicate f _x2), (map_predicate_expr f _x3))
  | ExprLambda(_x1, _x2, _x3) -> ExprLambda(((List.map (map_predicate_id f)) _x1), (map_predicate_predicate f _x2), (map_predicate_expr f _x3))

  and map_predicate_exprSeq f  = function
    SeqEnum(_x1) -> SeqEnum(((List.map (map_predicate_expr f)) _x1))
  | SeqEmpty -> SeqEmpty 
  and map_predicate_exprSet f  = function
    SetRange(_x1, _x2) -> SetRange((map_predicate_expr f _x1), (map_predicate_expr f _x2))
  | SetEnum(_x1) -> SetEnum(((List.map (map_predicate_expr f)) _x1))
  | SetCompr(_x1) -> SetCompr(((List.map (map_predicate_expr f)) _x1))
  | SetComprPred(_x1, _x2) -> SetComprPred(((List.map (map_predicate_id f)) _x1), (map_predicate_predicate f _x2))
  | SetEmpty -> SetEmpty 
  | SetUnionQuantify(_x1, _x2, _x3) -> SetUnionQuantify(((List.map (map_predicate_id f)) _x1), (map_predicate_predicate f _x2), (map_predicate_expr f _x3))
  | SetInterQuantify(_x1, _x2, _x3) -> SetInterQuantify(((List.map (map_predicate_id f)) _x1), (map_predicate_predicate f _x2), (map_predicate_expr f _x3))
  and map_predicate_number f  = function
    MinNumber -> MinNumber 
  | Number(_x1) -> Number((map_predicate_int f _x1))
  | MaxNumber -> MaxNumber 
  and map_predicate_substitution f  = function
    SubstOperCall(_x1, _x2, _x3) -> SubstOperCall((map_predicate_ids f _x1), (map_predicate_id f _x2), ((List.map (map_predicate_expr f)) _x3))
  | SubstBlock(_x1) -> SubstBlock((map_predicate_substitution f _x1))
  | SubstPrecondition(_x1, _x2) -> SubstPrecondition((map_predicate_predicate f _x1), (map_predicate_substitution f _x2))
  | SubstAssertion(_x1, _x2) -> SubstAssertion((map_predicate_predicate f _x1), (map_predicate_substitution f _x2))
  | SubstChoice(_x1) -> SubstChoice(((List.map (map_predicate_substitution f)) _x1))
  | SubstIf(_x1, _x2) -> SubstIf(((List.map (fun (_x1, _x2) -> ((map_predicate_predicate f _x1), (map_predicate_substitution f _x2)))) _x1), ((map_predicate_option (map_predicate_substitution f)) _x2))
  | SubstSelect(_x1, _x2) -> SubstSelect(((List.map (fun (_x1, _x2) -> ((map_predicate_predicate f _x1), (map_predicate_substitution f _x2)))) _x1), ((map_predicate_option (map_predicate_substitution f)) _x2))
  | SubstCase(_x1, _x2, _x3) -> SubstCase((map_predicate_expr f _x1), ((List.map (fun (_x1, _x2) -> (((List.map (map_predicate_expr f)) _x1), (map_predicate_substitution f _x2)))) _x2), ((map_predicate_option (map_predicate_substitution f)) _x3))
  | SubstAny(_x1, _x2, _x3) -> SubstAny(((List.map (map_predicate_id f)) _x1), (map_predicate_predicate f _x2), (map_predicate_substitution f _x3))
  | SubstLet(_x1, _x2, _x3) -> SubstLet(((List.map (map_predicate_id f)) _x1), (map_predicate_predicate f _x2), (map_predicate_substitution f _x3))
  | SubstVar(_x1, _x2) -> SubstVar(((List.map (map_predicate_id f)) _x1), (map_predicate_substitution f _x2))
  | SubstWhile(_x1, _x2, _x3, _x4) -> SubstWhile((map_predicate_predicate f _x1), (map_predicate_substitution f _x2), (map_predicate_expr f _x3), (map_predicate_predicate f _x4))
  | SubstSkip -> SubstSkip 
  | SubstSequence(_x1, _x2) -> SubstSequence((map_predicate_substitution f _x1), (map_predicate_substitution f _x2))
  | SubstParallel(_x1, _x2) -> SubstParallel((map_predicate_substitution f _x1), (map_predicate_substitution f _x2))
  | SubstSetEqualIds(_x1, _x2) -> SubstSetEqualIds((map_predicate_ids f _x1), ((List.map (map_predicate_expr f)) _x2))
  | SubstSetEqualFun(_x1, _x2, _x3) -> SubstSetEqualFun((map_predicate_expr f _x1), ((List.map (map_predicate_expr f)) _x2), (map_predicate_expr f _x3))

  | SubstBecomeSuch(_x1, _x2) -> SubstBecomeSuch((map_predicate_ids f _x1), (map_predicate_predicate f _x2))
  | SubstSetIn(_x1, _x2) -> SubstSetIn((map_predicate_ids f _x1), (map_predicate_expr f _x2))
  and map_predicate_ids f  = fun (_x1) -> ((List.map (map_predicate_id f)) _x1)
  and map_predicate_origine f  = function
    Local -> Local 
  | Remote(_x1) -> Remote(((List.map (fun (_x1, _x2) -> ((map_predicate_string f _x1), (map_predicate_string f _x2)))) _x1))
  | OrgRefine(_x1, _x2) -> OrgRefine((map_predicate_string f _x1), ((List.map (fun (_x1, _x2) -> ((map_predicate_string f _x1), (map_predicate_string f _x2)))) _x2))
  | Prefix(_x1) -> Prefix(((List.map (map_predicate_string f)) _x1))
  and map_predicate_id f  = function
    Id(_x1, _x2, _x3, _x4) -> Id((map_predicate_origine f _x1), (map_predicate_string f _x2), ((map_predicate_option (map_predicate_type_B f)) _x3), _x4)

let map_operation_string f (s:string) = s
let map_operation_int f (i:int32) = i
let map_operation_bool f (b:bool) = b
let map_operation_option f = function Some x -> Some (f x) | None -> None
let rec map_operation_amn f  = function
    Machine(_x1, _x2) -> Machine((map_operation_head f _x1), ((List.map (map_operation_clause f)) _x2))
  | Refinement(_x1, _x2) -> Refinement((map_operation_head f _x1), ((List.map (map_operation_clause f)) _x2))
  | Implementation(_x1, _x2) -> Implementation((map_operation_head f _x1), ((List.map (map_operation_clause f)) _x2))
  | EmptyTree -> EmptyTree 
  and map_operation_head f  = fun (_x1, _x2) -> (map_operation_id f _x1), (map_operation_ids f _x2)
  and map_operation_clause f  = function
    Definitions(_x1) -> Definitions(((List.map (map_operation_def f)) _x1))
  | Constraints(_x1) -> Constraints((map_operation_predicate f _x1))
  | Invariant(_x1) -> Invariant((map_operation_predicate f _x1))
  | Sets(_x1) -> Sets(((List.map (map_operation_set f)) _x1))
  | Initialisation(_x1) -> Initialisation((map_operation_substitution f _x1))
  | ConstantsConcrete(_x1) -> ConstantsConcrete((map_operation_ids f _x1))
  | ConstantsAbstract(_x1) -> ConstantsAbstract((map_operation_ids f _x1))
  | ConstantsHidden(_x1) -> ConstantsHidden((map_operation_ids f _x1))
  | Properties(_x1) -> Properties((map_operation_predicate f _x1))
  | Values(_x1) -> Values(((List.map (fun (_x1, _x2) -> ((map_operation_id f _x1), (map_operation_expr f _x2)))) _x1))
  | VariablesConcrete(_x1) -> VariablesConcrete((map_operation_ids f _x1))
  | VariablesAbstract(_x1) -> VariablesAbstract((map_operation_ids f _x1))
  | VariablesHidden(_x1) -> VariablesHidden((map_operation_ids f _x1))
  | Promotes(_x1) -> Promotes((map_operation_ids f _x1))
  | Assertions(_x1) -> Assertions(((List.map (map_operation_predicate f)) _x1))
  | Operations(_x1) -> Operations(((List.map (map_operation_operation f)) _x1))
  | Sees(_x1) -> Sees((map_operation_ids f _x1))
  | Uses(_x1) -> Uses((map_operation_ids f _x1))
  | Extends(_x1) -> Extends(((List.map (map_operation_instance f)) _x1))
  | Includes(_x1) -> Includes(((List.map (map_operation_instance f)) _x1))
  | Imports(_x1) -> Imports(((List.map (map_operation_instance f)) _x1))
  | Refines(_x1) -> Refines((map_operation_id f _x1))
  and map_operation_def f  = function
    Def(_x1, _x2, _x3) -> Def((map_operation_id f _x1), (map_operation_ids f _x2), (map_operation_defBody f _x3))
  | FileDef(_x1) -> FileDef((map_operation_string f _x1))
  and map_operation_defBody f  = function
    ExprDefBody(_x1) -> ExprDefBody((map_operation_expr f _x1))
  | SubstDefBody(_x1) -> SubstDefBody((map_operation_substitution f _x1))
  and map_operation_set f  = function
    SetAbstDec(_x1) -> SetAbstDec((map_operation_id f _x1))
  | SetEnumDec(_x1, _x2) -> SetEnumDec((map_operation_id f _x1), (map_operation_ids f _x2))

  and map_operation_instance f  = fun (_x1, _x2) -> (map_operation_id f _x1), ((List.map (map_operation_expr f)) _x2)
  and map_operation_type_B f  = function
    TypeNatSetRange(_x1, _x2) -> TypeNatSetRange((map_operation_expr f _x1), (map_operation_expr f _x2))
  | PredType(_x1) -> PredType((map_operation_expr f _x1))

  | TypeIdentifier(_x1) -> TypeIdentifier((map_operation_id f _x1))
  | PFunType(_x1, _x2) -> PFunType((map_operation_type_B f _x1), (map_operation_type_B f _x2))
  | FunType(_x1, _x2) -> FunType((map_operation_type_B f _x1), (map_operation_type_B f _x2))
  | ProdType(_x1, _x2) -> ProdType((map_operation_type_B f _x1), (map_operation_type_B f _x2))
  | Sequence(_x1) -> Sequence((map_operation_type_B f _x1))
  | SubType(_x1, _x2) -> SubType((map_operation_type_B f _x1), (map_operation_expr f _x2))
  | Fin_B(_x1) -> Fin_B((map_operation_type_B f _x1))
  | Untyped -> Untyped 
  and map_operation_field f  = fun (_x1, _x2) -> (map_operation_id f _x1), (map_operation_type_B f _x2)
  and map_operation_predicate f  = function
    PredParen(_x1) -> PredParen((map_operation_predicate f _x1))
  | PredNegation(_x1) -> PredNegation((map_operation_predicate f _x1))
  | PredExists(_x1, _x2) -> PredExists(((List.map (map_operation_id f)) _x1), (map_operation_predicate f _x2))
  | PredForAll(_x1, _x2) -> PredForAll(((List.map (map_operation_id f)) _x1), (map_operation_predicate f _x2))
  | PredBin(_x1, _x2, _x3) -> PredBin(_x1, (map_operation_predicate f _x2), (map_operation_predicate f _x3))
  | PredAtom(_x1, _x2, _x3) -> PredAtom(_x1, (map_operation_expr f _x2), (map_operation_expr f _x3))
  and map_operation_operation f  = fun _x -> f((fun (_x1, _x2, _x3, _x4) -> (map_operation_id f _x1), (map_operation_ids f _x2), (map_operation_ids f _x3), (map_operation_substitution f _x4))_x)
  and map_operation_expr f  = function
    ExprSequence(_x1, _x2) -> ExprSequence((map_operation_expr f _x1), (map_operation_expr f _x2))
  | ExprParen(_x1) -> ExprParen((map_operation_expr f _x1))
  | ExprId(_x1) -> ExprId((map_operation_id f _x1))
  | ExprFunCall(_x1, _x2) -> ExprFunCall((map_operation_expr f _x1), ((List.map (map_operation_expr f)) _x2))
  | ExprBin(_x1, _x2, _x3) -> ExprBin(_x1, (map_operation_expr f _x2), (map_operation_expr f _x3))
  | ExprUn(_x1, _x2) -> ExprUn(_x1, (map_operation_expr f _x2))
  | ExprSeq(_x1) -> ExprSeq((map_operation_exprSeq f _x1))
  | ExprSet(_x1) -> ExprSet((map_operation_exprSet f _x1))
  | RelSet(_x1, _x2) -> RelSet((map_operation_expr f _x1), (map_operation_expr f _x2))
  | RelSeqComp(_x1) -> RelSeqComp(((List.map (map_operation_expr f)) _x1))
  | ExprNumber(_x1) -> ExprNumber((map_operation_number f _x1))
  | ExprBool(_x1) -> ExprBool((map_operation_bool f _x1))
  | ExprString(_x1) -> ExprString((map_operation_string f _x1))

  | ExprPred(_x1) -> ExprPred((map_operation_predicate f _x1))
  | ExprNuplet(_x1) -> ExprNuplet(((List.map (map_operation_expr f)) _x1))
  | ExprSIGMA(_x1, _x2, _x3) -> ExprSIGMA(((List.map (map_operation_id f)) _x1), (map_operation_predicate f _x2), (map_operation_expr f _x3))
  | ExprPI(_x1, _x2, _x3) -> ExprPI(((List.map (map_operation_id f)) _x1), (map_operation_predicate f _x2), (map_operation_expr f _x3))
  | ExprLambda(_x1, _x2, _x3) -> ExprLambda(((List.map (map_operation_id f)) _x1), (map_operation_predicate f _x2), (map_operation_expr f _x3))
  
  and map_operation_exprSeq f  = function
    SeqEnum(_x1) -> SeqEnum(((List.map (map_operation_expr f)) _x1))
  | SeqEmpty -> SeqEmpty 
  and map_operation_exprSet f  = function
    SetRange(_x1, _x2) -> SetRange((map_operation_expr f _x1), (map_operation_expr f _x2))
  | SetEnum(_x1) -> SetEnum(((List.map (map_operation_expr f)) _x1))
  | SetCompr(_x1) -> SetCompr(((List.map (map_operation_expr f)) _x1))
  | SetComprPred(_x1, _x2) -> SetComprPred(((List.map (map_operation_id f)) _x1), (map_operation_predicate f _x2))
  | SetEmpty -> SetEmpty 
  | SetUnionQuantify(_x1, _x2, _x3) -> SetUnionQuantify(((List.map (map_operation_id f)) _x1), (map_operation_predicate f _x2), (map_operation_expr f _x3))
  | SetInterQuantify(_x1, _x2, _x3) -> SetInterQuantify(((List.map (map_operation_id f)) _x1), (map_operation_predicate f _x2), (map_operation_expr f _x3))
  and map_operation_number f  = function
    MinNumber -> MinNumber 
  | Number(_x1) -> Number((map_operation_int f _x1))
  | MaxNumber -> MaxNumber 
  and map_operation_substitution f  = function
    SubstOperCall(_x1, _x2, _x3) -> SubstOperCall((map_operation_ids f _x1), (map_operation_id f _x2), ((List.map (map_operation_expr f)) _x3))
  | SubstBlock(_x1) -> SubstBlock((map_operation_substitution f _x1))
  | SubstPrecondition(_x1, _x2) -> SubstPrecondition((map_operation_predicate f _x1), (map_operation_substitution f _x2))
  | SubstAssertion(_x1, _x2) -> SubstAssertion((map_operation_predicate f _x1), (map_operation_substitution f _x2))
  | SubstChoice(_x1) -> SubstChoice(((List.map (map_operation_substitution f)) _x1))
  | SubstIf(_x1, _x2) -> SubstIf(((List.map (fun (_x1, _x2) -> ((map_operation_predicate f _x1), (map_operation_substitution f _x2)))) _x1), ((map_operation_option (map_operation_substitution f)) _x2))
  | SubstSelect(_x1, _x2) -> SubstSelect(((List.map (fun (_x1, _x2) -> ((map_operation_predicate f _x1), (map_operation_substitution f _x2)))) _x1), ((map_operation_option (map_operation_substitution f)) _x2))
  | SubstCase(_x1, _x2, _x3) -> SubstCase((map_operation_expr f _x1), ((List.map (fun (_x1, _x2) -> (((List.map (map_operation_expr f)) _x1), (map_operation_substitution f _x2)))) _x2), ((map_operation_option (map_operation_substitution f)) _x3))
  | SubstAny(_x1, _x2, _x3) -> SubstAny(((List.map (map_operation_id f)) _x1), (map_operation_predicate f _x2), (map_operation_substitution f _x3))
  | SubstLet(_x1, _x2, _x3) -> SubstLet(((List.map (map_operation_id f)) _x1), (map_operation_predicate f _x2), (map_operation_substitution f _x3))
  | SubstVar(_x1, _x2) -> SubstVar(((List.map (map_operation_id f)) _x1), (map_operation_substitution f _x2))
  | SubstWhile(_x1, _x2, _x3, _x4) -> SubstWhile((map_operation_predicate f _x1), (map_operation_substitution f _x2), (map_operation_expr f _x3), (map_operation_predicate f _x4))
  | SubstSkip -> SubstSkip 
  | SubstSequence(_x1, _x2) -> SubstSequence((map_operation_substitution f _x1), (map_operation_substitution f _x2))
  | SubstParallel(_x1, _x2) -> SubstParallel((map_operation_substitution f _x1), (map_operation_substitution f _x2))
  | SubstSetEqualIds(_x1, _x2) -> SubstSetEqualIds((map_operation_ids f _x1), ((List.map (map_operation_expr f)) _x2))
  | SubstSetEqualFun(_x1, _x2, _x3) -> SubstSetEqualFun((map_operation_expr f _x1), ((List.map (map_operation_expr f)) _x2), (map_operation_expr f _x3))

  | SubstBecomeSuch(_x1, _x2) -> SubstBecomeSuch((map_operation_ids f _x1), (map_operation_predicate f _x2))
  | SubstSetIn(_x1, _x2) -> SubstSetIn((map_operation_ids f _x1), (map_operation_expr f _x2))
  and map_operation_ids f  = fun (_x1) -> ((List.map (map_operation_id f)) _x1)
  and map_operation_origine f  = function
    Local -> Local 
  | Remote(_x1) -> Remote(((List.map (fun (_x1, _x2) -> ((map_operation_string f _x1), (map_operation_string f _x2)))) _x1))
  | OrgRefine(_x1, _x2) -> OrgRefine((map_operation_string f _x1), ((List.map (fun (_x1, _x2) -> ((map_operation_string f _x1), (map_operation_string f _x2)))) _x2))
  | Prefix(_x1) -> Prefix(((List.map (map_operation_string f)) _x1))
  and map_operation_id f  = function
    Id(_x1, _x2, _x3, _x4) -> Id((map_operation_origine f _x1), (map_operation_string f _x2), ((map_operation_option (map_operation_type_B f)) _x3), _x4)

let map_expr_string f (s:string) = s
let map_expr_int f (i:int32) = i
let map_expr_bool f (b:bool) = b
let map_expr_option f = function Some x -> Some (f x) | None -> None
let rec map_expr_amn f  = function
    Machine(_x1, _x2) -> Machine((map_expr_head f _x1), ((List.map (map_expr_clause f)) _x2))
  | Refinement(_x1, _x2) -> Refinement((map_expr_head f _x1), ((List.map (map_expr_clause f)) _x2))
  | Implementation(_x1, _x2) -> Implementation((map_expr_head f _x1), ((List.map (map_expr_clause f)) _x2))
  | EmptyTree -> EmptyTree 
  and map_expr_head f  = fun (_x1, _x2) -> (map_expr_id f _x1), (map_expr_ids f _x2)
  and map_expr_clause f  = function
    Definitions(_x1) -> Definitions(((List.map (map_expr_def f)) _x1))
  | Constraints(_x1) -> Constraints((map_expr_predicate f _x1))
  | Invariant(_x1) -> Invariant((map_expr_predicate f _x1))
  | Sets(_x1) -> Sets(((List.map (map_expr_set f)) _x1))
  | Initialisation(_x1) -> Initialisation((map_expr_substitution f _x1))
  | ConstantsConcrete(_x1) -> ConstantsConcrete((map_expr_ids f _x1))
  | ConstantsAbstract(_x1) -> ConstantsAbstract((map_expr_ids f _x1))
  | ConstantsHidden(_x1) -> ConstantsHidden((map_expr_ids f _x1))
  | Properties(_x1) -> Properties((map_expr_predicate f _x1))
  | Values(_x1) -> Values(((List.map (fun (_x1, _x2) -> ((map_expr_id f _x1), (map_expr_expr f _x2)))) _x1))
  | VariablesConcrete(_x1) -> VariablesConcrete((map_expr_ids f _x1))
  | VariablesAbstract(_x1) -> VariablesAbstract((map_expr_ids f _x1))
  | VariablesHidden(_x1) -> VariablesHidden((map_expr_ids f _x1))
  | Promotes(_x1) -> Promotes((map_expr_ids f _x1))
  | Assertions(_x1) -> Assertions(((List.map (map_expr_predicate f)) _x1))
  | Operations(_x1) -> Operations(((List.map (map_expr_operation f)) _x1))
  | Sees(_x1) -> Sees((map_expr_ids f _x1))
  | Uses(_x1) -> Uses((map_expr_ids f _x1))
  | Extends(_x1) -> Extends(((List.map (map_expr_instance f)) _x1))
  | Includes(_x1) -> Includes(((List.map (map_expr_instance f)) _x1))
  | Imports(_x1) -> Imports(((List.map (map_expr_instance f)) _x1))
  | Refines(_x1) -> Refines((map_expr_id f _x1))
  and map_expr_def f  = function
    Def(_x1, _x2, _x3) -> Def((map_expr_id f _x1), (map_expr_ids f _x2), (map_expr_defBody f _x3))
  | FileDef(_x1) -> FileDef((map_expr_string f _x1))
  and map_expr_defBody f  = function
    ExprDefBody(_x1) -> ExprDefBody((map_expr_expr f _x1))
  | SubstDefBody(_x1) -> SubstDefBody((map_expr_substitution f _x1))
  and map_expr_set f  = function
    SetAbstDec(_x1) -> SetAbstDec((map_expr_id f _x1))
  | SetEnumDec(_x1, _x2) -> SetEnumDec((map_expr_id f _x1), (map_expr_ids f _x2))

  and map_expr_instance f  = fun (_x1, _x2) -> (map_expr_id f _x1), ((List.map (map_expr_expr f)) _x2)
  and map_expr_type_B f  = function
    TypeNatSetRange(_x1, _x2) -> TypeNatSetRange((map_expr_expr f _x1), (map_expr_expr f _x2))
  | PredType(_x1) -> PredType((map_expr_expr f _x1))

  | TypeIdentifier(_x1) -> TypeIdentifier((map_expr_id f _x1))
  | PFunType(_x1, _x2) -> PFunType((map_expr_type_B f _x1), (map_expr_type_B f _x2))
  | FunType(_x1, _x2) -> FunType((map_expr_type_B f _x1), (map_expr_type_B f _x2))
  | ProdType(_x1, _x2) -> ProdType((map_expr_type_B f _x1), (map_expr_type_B f _x2))
  | Sequence(_x1) -> Sequence((map_expr_type_B f _x1))
  | SubType(_x1, _x2) -> SubType((map_expr_type_B f _x1), (map_expr_expr f _x2))
  | Fin_B(_x1) -> Fin_B((map_expr_type_B f _x1))
  | Untyped -> Untyped 
  and map_expr_field f  = fun (_x1, _x2) -> (map_expr_id f _x1), (map_expr_type_B f _x2)
  and map_expr_predicate f  = function
    PredParen(_x1) -> PredParen((map_expr_predicate f _x1))
  | PredNegation(_x1) -> PredNegation((map_expr_predicate f _x1))
  | PredExists(_x1, _x2) -> PredExists(((List.map (map_expr_id f)) _x1), (map_expr_predicate f _x2))
  | PredForAll(_x1, _x2) -> PredForAll(((List.map (map_expr_id f)) _x1), (map_expr_predicate f _x2))
  | PredBin(_x1, _x2, _x3) -> PredBin(_x1, (map_expr_predicate f _x2), (map_expr_predicate f _x3))
  | PredAtom(_x1, _x2, _x3) -> PredAtom(_x1, (map_expr_expr f _x2), (map_expr_expr f _x3))
  and map_expr_operation f  = fun (_x1, _x2, _x3, _x4) -> (map_expr_id f _x1), (map_expr_ids f _x2), (map_expr_ids f _x3), (map_expr_substitution f _x4)
  and map_expr_expr f  = fun _x -> f((function
    ExprSequence(_x1, _x2) -> ExprSequence((map_expr_expr f _x1), (map_expr_expr f _x2))
  | ExprParen(_x1) -> ExprParen((map_expr_expr f _x1))
  | ExprId(_x1) -> ExprId((map_expr_id f _x1))
  | ExprFunCall(_x1, _x2) -> ExprFunCall((map_expr_expr f _x1), ((List.map (map_expr_expr f)) _x2))
  | ExprBin(_x1, _x2, _x3) -> ExprBin(_x1, (map_expr_expr f _x2), (map_expr_expr f _x3))
  | ExprUn(_x1, _x2) -> ExprUn(_x1, (map_expr_expr f _x2))
  | ExprSeq(_x1) -> ExprSeq((map_expr_exprSeq f _x1))
  | ExprSet(_x1) -> ExprSet((map_expr_exprSet f _x1))
  | RelSet(_x1, _x2) -> RelSet((map_expr_expr f _x1), (map_expr_expr f _x2))
  | RelSeqComp(_x1) -> RelSeqComp(((List.map (map_expr_expr f)) _x1))
  | ExprNumber(_x1) -> ExprNumber((map_expr_number f _x1))
  | ExprBool(_x1) -> ExprBool((map_expr_bool f _x1))
  | ExprString(_x1) -> ExprString((map_expr_string f _x1))

  | ExprPred(_x1) -> ExprPred((map_expr_predicate f _x1))
  | ExprNuplet(_x1) -> ExprNuplet(((List.map (map_expr_expr f)) _x1))
  | ExprSIGMA(_x1, _x2, _x3) -> ExprSIGMA(((List.map (map_expr_id f)) _x1), (map_expr_predicate f _x2), (map_expr_expr f _x3))
  | ExprPI(_x1, _x2, _x3) -> ExprPI(((List.map (map_expr_id f)) _x1), (map_expr_predicate f _x2), (map_expr_expr f _x3))
  | ExprLambda(_x1, _x2, _x3) -> ExprLambda(((List.map (map_expr_id f)) _x1), (map_expr_predicate f _x2), (map_expr_expr f _x3)))_x)

  and map_expr_exprSeq f  = function
    SeqEnum(_x1) -> SeqEnum(((List.map (map_expr_expr f)) _x1))
  | SeqEmpty -> SeqEmpty 
  and map_expr_exprSet f  = function
    SetRange(_x1, _x2) -> SetRange((map_expr_expr f _x1), (map_expr_expr f _x2))
  | SetEnum(_x1) -> SetEnum(((List.map (map_expr_expr f)) _x1))
  | SetCompr(_x1) -> SetCompr(((List.map (map_expr_expr f)) _x1))
  | SetComprPred(_x1, _x2) -> SetComprPred(((List.map (map_expr_id f)) _x1), (map_expr_predicate f _x2))
  | SetEmpty -> SetEmpty 
  | SetUnionQuantify(_x1, _x2, _x3) -> SetUnionQuantify(((List.map (map_expr_id f)) _x1), (map_expr_predicate f _x2), (map_expr_expr f _x3))
  | SetInterQuantify(_x1, _x2, _x3) -> SetInterQuantify(((List.map (map_expr_id f)) _x1), (map_expr_predicate f _x2), (map_expr_expr f _x3))
  and map_expr_number f  = function
    MinNumber -> MinNumber 
  | Number(_x1) -> Number((map_expr_int f _x1))
  | MaxNumber -> MaxNumber 
  and map_expr_substitution f  = function
    SubstOperCall(_x1, _x2, _x3) -> SubstOperCall((map_expr_ids f _x1), (map_expr_id f _x2), ((List.map (map_expr_expr f)) _x3))
  | SubstBlock(_x1) -> SubstBlock((map_expr_substitution f _x1))
  | SubstPrecondition(_x1, _x2) -> SubstPrecondition((map_expr_predicate f _x1), (map_expr_substitution f _x2))
  | SubstAssertion(_x1, _x2) -> SubstAssertion((map_expr_predicate f _x1), (map_expr_substitution f _x2))
  | SubstChoice(_x1) -> SubstChoice(((List.map (map_expr_substitution f)) _x1))
  | SubstIf(_x1, _x2) -> SubstIf(((List.map (fun (_x1, _x2) -> ((map_expr_predicate f _x1), (map_expr_substitution f _x2)))) _x1), ((map_expr_option (map_expr_substitution f)) _x2))
  | SubstSelect(_x1, _x2) -> SubstSelect(((List.map (fun (_x1, _x2) -> ((map_expr_predicate f _x1), (map_expr_substitution f _x2)))) _x1), ((map_expr_option (map_expr_substitution f)) _x2))
  | SubstCase(_x1, _x2, _x3) -> SubstCase((map_expr_expr f _x1), ((List.map (fun (_x1, _x2) -> (((List.map (map_expr_expr f)) _x1), (map_expr_substitution f _x2)))) _x2), ((map_expr_option (map_expr_substitution f)) _x3))
  | SubstAny(_x1, _x2, _x3) -> SubstAny(((List.map (map_expr_id f)) _x1), (map_expr_predicate f _x2), (map_expr_substitution f _x3))
  | SubstLet(_x1, _x2, _x3) -> SubstLet(((List.map (map_expr_id f)) _x1), (map_expr_predicate f _x2), (map_expr_substitution f _x3))
  | SubstVar(_x1, _x2) -> SubstVar(((List.map (map_expr_id f)) _x1), (map_expr_substitution f _x2))
  | SubstWhile(_x1, _x2, _x3, _x4) -> SubstWhile((map_expr_predicate f _x1), (map_expr_substitution f _x2), (map_expr_expr f _x3), (map_expr_predicate f _x4))
  | SubstSkip -> SubstSkip 
  | SubstSequence(_x1, _x2) -> SubstSequence((map_expr_substitution f _x1), (map_expr_substitution f _x2))
  | SubstParallel(_x1, _x2) -> SubstParallel((map_expr_substitution f _x1), (map_expr_substitution f _x2))
  | SubstSetEqualIds(_x1, _x2) -> SubstSetEqualIds((map_expr_ids f _x1), ((List.map (map_expr_expr f)) _x2))
  | SubstSetEqualFun(_x1, _x2, _x3) -> SubstSetEqualFun((map_expr_expr f _x1), ((List.map (map_expr_expr f)) _x2), (map_expr_expr f _x3))

  | SubstBecomeSuch(_x1, _x2) -> SubstBecomeSuch((map_expr_ids f _x1), (map_expr_predicate f _x2))
  | SubstSetIn(_x1, _x2) -> SubstSetIn((map_expr_ids f _x1), (map_expr_expr f _x2))
  and map_expr_ids f  = fun (_x1) -> ((List.map (map_expr_id f)) _x1)
  and map_expr_origine f  = function
    Local -> Local 
  | Remote(_x1) -> Remote(((List.map (fun (_x1, _x2) -> ((map_expr_string f _x1), (map_expr_string f _x2)))) _x1))
  | OrgRefine(_x1, _x2) -> OrgRefine((map_expr_string f _x1), ((List.map (fun (_x1, _x2) -> ((map_expr_string f _x1), (map_expr_string f _x2)))) _x2))
  | Prefix(_x1) -> Prefix(((List.map (map_expr_string f)) _x1))
  and map_expr_id f  = function
    Id(_x1, _x2, _x3, _x4) -> Id((map_expr_origine f _x1), (map_expr_string f _x2), ((map_expr_option (map_expr_type_B f)) _x3), _x4)


let map_exprSeq_string f (s:string) = s
let map_exprSeq_int f (i:int32) = i
let map_exprSeq_bool f (b:bool) = b
let map_exprSeq_option f = function Some x -> Some (f x) | None -> None
let rec map_exprSeq_amn f  = function
    Machine(_x1, _x2) -> Machine((map_exprSeq_head f _x1), ((List.map (map_exprSeq_clause f)) _x2))
  | Refinement(_x1, _x2) -> Refinement((map_exprSeq_head f _x1), ((List.map (map_exprSeq_clause f)) _x2))
  | Implementation(_x1, _x2) -> Implementation((map_exprSeq_head f _x1), ((List.map (map_exprSeq_clause f)) _x2))
  | EmptyTree -> EmptyTree 
  and map_exprSeq_head f  = fun (_x1, _x2) -> (map_exprSeq_id f _x1), (map_exprSeq_ids f _x2)
  and map_exprSeq_clause f  = function
    Definitions(_x1) -> Definitions(((List.map (map_exprSeq_def f)) _x1))
  | Constraints(_x1) -> Constraints((map_exprSeq_predicate f _x1))
  | Invariant(_x1) -> Invariant((map_exprSeq_predicate f _x1))
  | Sets(_x1) -> Sets(((List.map (map_exprSeq_set f)) _x1))
  | Initialisation(_x1) -> Initialisation((map_exprSeq_substitution f _x1))
  | ConstantsConcrete(_x1) -> ConstantsConcrete((map_exprSeq_ids f _x1))
  | ConstantsAbstract(_x1) -> ConstantsAbstract((map_exprSeq_ids f _x1))
  | ConstantsHidden(_x1) -> ConstantsHidden((map_exprSeq_ids f _x1))
  | Properties(_x1) -> Properties((map_exprSeq_predicate f _x1))
  | Values(_x1) -> Values(((List.map (fun (_x1, _x2) -> ((map_exprSeq_id f _x1), (map_exprSeq_expr f _x2)))) _x1))
  | VariablesConcrete(_x1) -> VariablesConcrete((map_exprSeq_ids f _x1))
  | VariablesAbstract(_x1) -> VariablesAbstract((map_exprSeq_ids f _x1))
  | VariablesHidden(_x1) -> VariablesHidden((map_exprSeq_ids f _x1))
  | Promotes(_x1) -> Promotes((map_exprSeq_ids f _x1))
  | Assertions(_x1) -> Assertions(((List.map (map_exprSeq_predicate f)) _x1))
  | Operations(_x1) -> Operations(((List.map (map_exprSeq_operation f)) _x1))
  | Sees(_x1) -> Sees((map_exprSeq_ids f _x1))
  | Uses(_x1) -> Uses((map_exprSeq_ids f _x1))
  | Extends(_x1) -> Extends(((List.map (map_exprSeq_instance f)) _x1))
  | Includes(_x1) -> Includes(((List.map (map_exprSeq_instance f)) _x1))
  | Imports(_x1) -> Imports(((List.map (map_exprSeq_instance f)) _x1))
  | Refines(_x1) -> Refines((map_exprSeq_id f _x1))
  and map_exprSeq_def f  = function
    Def(_x1, _x2, _x3) -> Def((map_exprSeq_id f _x1), (map_exprSeq_ids f _x2), (map_exprSeq_defBody f _x3))
  | FileDef(_x1) -> FileDef((map_exprSeq_string f _x1))
  and map_exprSeq_defBody f  = function
    ExprDefBody(_x1) -> ExprDefBody((map_exprSeq_expr f _x1))
  | SubstDefBody(_x1) -> SubstDefBody((map_exprSeq_substitution f _x1))
  and map_exprSeq_set f  = function
    SetAbstDec(_x1) -> SetAbstDec((map_exprSeq_id f _x1))
  | SetEnumDec(_x1, _x2) -> SetEnumDec((map_exprSeq_id f _x1), (map_exprSeq_ids f _x2))

  and map_exprSeq_instance f  = fun (_x1, _x2) -> (map_exprSeq_id f _x1), ((List.map (map_exprSeq_expr f)) _x2)
  and map_exprSeq_type_B f  = function
    TypeNatSetRange(_x1, _x2) -> TypeNatSetRange((map_exprSeq_expr f _x1), (map_exprSeq_expr f _x2))
  | PredType(_x1) -> PredType((map_exprSeq_expr f _x1))

  | TypeIdentifier(_x1) -> TypeIdentifier((map_exprSeq_id f _x1))
  | PFunType(_x1, _x2) -> PFunType((map_exprSeq_type_B f _x1), (map_exprSeq_type_B f _x2))
  | FunType(_x1, _x2) -> FunType((map_exprSeq_type_B f _x1), (map_exprSeq_type_B f _x2))
  | ProdType(_x1, _x2) -> ProdType((map_exprSeq_type_B f _x1), (map_exprSeq_type_B f _x2))
  | Sequence(_x1) -> Sequence((map_exprSeq_type_B f _x1))
  | SubType(_x1, _x2) -> SubType((map_exprSeq_type_B f _x1), (map_exprSeq_expr f _x2))
  | Fin_B(_x1) -> Fin_B((map_exprSeq_type_B f _x1))
  | Untyped -> Untyped 
  and map_exprSeq_field f  = fun (_x1, _x2) -> (map_exprSeq_id f _x1), (map_exprSeq_type_B f _x2)
  and map_exprSeq_predicate f  = function
    PredParen(_x1) -> PredParen((map_exprSeq_predicate f _x1))
  | PredNegation(_x1) -> PredNegation((map_exprSeq_predicate f _x1))
  | PredExists(_x1, _x2) -> PredExists(((List.map (map_exprSeq_id f)) _x1), (map_exprSeq_predicate f _x2))
  | PredForAll(_x1, _x2) -> PredForAll(((List.map (map_exprSeq_id f)) _x1), (map_exprSeq_predicate f _x2))
  | PredBin(_x1, _x2, _x3) -> PredBin(_x1, (map_exprSeq_predicate f _x2), (map_exprSeq_predicate f _x3))
  | PredAtom(_x1, _x2, _x3) -> PredAtom(_x1, (map_exprSeq_expr f _x2), (map_exprSeq_expr f _x3))
  and map_exprSeq_operation f  = fun (_x1, _x2, _x3, _x4) -> (map_exprSeq_id f _x1), (map_exprSeq_ids f _x2), (map_exprSeq_ids f _x3), (map_exprSeq_substitution f _x4)
  and map_exprSeq_expr f  = function
    ExprSequence(_x1, _x2) -> ExprSequence((map_exprSeq_expr f _x1), (map_exprSeq_expr f _x2))
  | ExprParen(_x1) -> ExprParen((map_exprSeq_expr f _x1))
  | ExprId(_x1) -> ExprId((map_exprSeq_id f _x1))
  | ExprFunCall(_x1, _x2) -> ExprFunCall((map_exprSeq_expr f _x1), ((List.map (map_exprSeq_expr f)) _x2))
  | ExprBin(_x1, _x2, _x3) -> ExprBin(_x1, (map_exprSeq_expr f _x2), (map_exprSeq_expr f _x3))
  | ExprUn(_x1, _x2) -> ExprUn(_x1, (map_exprSeq_expr f _x2))
  | ExprSeq(_x1) -> ExprSeq((map_exprSeq_exprSeq f _x1))
  | ExprSet(_x1) -> ExprSet((map_exprSeq_exprSet f _x1))
  | RelSet(_x1, _x2) -> RelSet((map_exprSeq_expr f _x1), (map_exprSeq_expr f _x2))
  | RelSeqComp(_x1) -> RelSeqComp(((List.map (map_exprSeq_expr f)) _x1))
  | ExprNumber(_x1) -> ExprNumber((map_exprSeq_number f _x1))
  | ExprBool(_x1) -> ExprBool((map_exprSeq_bool f _x1))
  | ExprString(_x1) -> ExprString((map_exprSeq_string f _x1))

  | ExprPred(_x1) -> ExprPred((map_exprSeq_predicate f _x1))
  | ExprNuplet(_x1) -> ExprNuplet(((List.map (map_exprSeq_expr f)) _x1))
  | ExprSIGMA(_x1, _x2, _x3) -> ExprSIGMA(((List.map (map_exprSeq_id f)) _x1), (map_exprSeq_predicate f _x2), (map_exprSeq_expr f _x3))
  | ExprPI(_x1, _x2, _x3) -> ExprPI(((List.map (map_exprSeq_id f)) _x1), (map_exprSeq_predicate f _x2), (map_exprSeq_expr f _x3))
  | ExprLambda(_x1, _x2, _x3) -> ExprLambda(((List.map (map_exprSeq_id f)) _x1), (map_exprSeq_predicate f _x2), (map_exprSeq_expr f _x3))

  and map_exprSeq_exprSeq f  = fun _x -> f((function
    SeqEnum(_x1) -> SeqEnum(((List.map (map_exprSeq_expr f)) _x1))
  | SeqEmpty -> SeqEmpty )_x)
  and map_exprSeq_exprSet f  = function
    SetRange(_x1, _x2) -> SetRange((map_exprSeq_expr f _x1), (map_exprSeq_expr f _x2))
  | SetEnum(_x1) -> SetEnum(((List.map (map_exprSeq_expr f)) _x1))
  | SetCompr(_x1) -> SetCompr(((List.map (map_exprSeq_expr f)) _x1))
  | SetComprPred(_x1, _x2) -> SetComprPred(((List.map (map_exprSeq_id f)) _x1), (map_exprSeq_predicate f _x2))
  | SetEmpty -> SetEmpty 
  | SetUnionQuantify(_x1, _x2, _x3) -> SetUnionQuantify(((List.map (map_exprSeq_id f)) _x1), (map_exprSeq_predicate f _x2), (map_exprSeq_expr f _x3))
  | SetInterQuantify(_x1, _x2, _x3) -> SetInterQuantify(((List.map (map_exprSeq_id f)) _x1), (map_exprSeq_predicate f _x2), (map_exprSeq_expr f _x3))
  and map_exprSeq_number f  = function
    MinNumber -> MinNumber 
  | Number(_x1) -> Number((map_exprSeq_int f _x1))
  | MaxNumber -> MaxNumber 
  and map_exprSeq_substitution f  = function
    SubstOperCall(_x1, _x2, _x3) -> SubstOperCall((map_exprSeq_ids f _x1), (map_exprSeq_id f _x2), ((List.map (map_exprSeq_expr f)) _x3))
  | SubstBlock(_x1) -> SubstBlock((map_exprSeq_substitution f _x1))
  | SubstPrecondition(_x1, _x2) -> SubstPrecondition((map_exprSeq_predicate f _x1), (map_exprSeq_substitution f _x2))
  | SubstAssertion(_x1, _x2) -> SubstAssertion((map_exprSeq_predicate f _x1), (map_exprSeq_substitution f _x2))
  | SubstChoice(_x1) -> SubstChoice(((List.map (map_exprSeq_substitution f)) _x1))
  | SubstIf(_x1, _x2) -> SubstIf(((List.map (fun (_x1, _x2) -> ((map_exprSeq_predicate f _x1), (map_exprSeq_substitution f _x2)))) _x1), ((map_exprSeq_option (map_exprSeq_substitution f)) _x2))
  | SubstSelect(_x1, _x2) -> SubstSelect(((List.map (fun (_x1, _x2) -> ((map_exprSeq_predicate f _x1), (map_exprSeq_substitution f _x2)))) _x1), ((map_exprSeq_option (map_exprSeq_substitution f)) _x2))
  | SubstCase(_x1, _x2, _x3) -> SubstCase((map_exprSeq_expr f _x1), ((List.map (fun (_x1, _x2) -> (((List.map (map_exprSeq_expr f)) _x1), (map_exprSeq_substitution f _x2)))) _x2), ((map_exprSeq_option (map_exprSeq_substitution f)) _x3))
  | SubstAny(_x1, _x2, _x3) -> SubstAny(((List.map (map_exprSeq_id f)) _x1), (map_exprSeq_predicate f _x2), (map_exprSeq_substitution f _x3))
  | SubstLet(_x1, _x2, _x3) -> SubstLet(((List.map (map_exprSeq_id f)) _x1), (map_exprSeq_predicate f _x2), (map_exprSeq_substitution f _x3))
  | SubstVar(_x1, _x2) -> SubstVar(((List.map (map_exprSeq_id f)) _x1), (map_exprSeq_substitution f _x2))
  | SubstWhile(_x1, _x2, _x3, _x4) -> SubstWhile((map_exprSeq_predicate f _x1), (map_exprSeq_substitution f _x2), (map_exprSeq_expr f _x3), (map_exprSeq_predicate f _x4))
  | SubstSkip -> SubstSkip 
  | SubstSequence(_x1, _x2) -> SubstSequence((map_exprSeq_substitution f _x1), (map_exprSeq_substitution f _x2))
  | SubstParallel(_x1, _x2) -> SubstParallel((map_exprSeq_substitution f _x1), (map_exprSeq_substitution f _x2))
  | SubstSetEqualIds(_x1, _x2) -> SubstSetEqualIds((map_exprSeq_ids f _x1), ((List.map (map_exprSeq_expr f)) _x2))
  | SubstSetEqualFun(_x1, _x2, _x3) -> SubstSetEqualFun((map_exprSeq_expr f _x1), ((List.map (map_exprSeq_expr f)) _x2), (map_exprSeq_expr f _x3))

  | SubstBecomeSuch(_x1, _x2) -> SubstBecomeSuch((map_exprSeq_ids f _x1), (map_exprSeq_predicate f _x2))
  | SubstSetIn(_x1, _x2) -> SubstSetIn((map_exprSeq_ids f _x1), (map_exprSeq_expr f _x2))
  and map_exprSeq_ids f  = fun (_x1) -> ((List.map (map_exprSeq_id f)) _x1)
  and map_exprSeq_origine f  = function
    Local -> Local 
  | Remote(_x1) -> Remote(((List.map (fun (_x1, _x2) -> ((map_exprSeq_string f _x1), (map_exprSeq_string f _x2)))) _x1))
  | OrgRefine(_x1, _x2) -> OrgRefine((map_exprSeq_string f _x1), ((List.map (fun (_x1, _x2) -> ((map_exprSeq_string f _x1), (map_exprSeq_string f _x2)))) _x2))
  | Prefix(_x1) -> Prefix(((List.map (map_exprSeq_string f)) _x1))
  and map_exprSeq_id f  = function
    Id(_x1, _x2, _x3, _x4) -> Id((map_exprSeq_origine f _x1), (map_exprSeq_string f _x2), ((map_exprSeq_option (map_exprSeq_type_B f)) _x3), _x4)

let map_exprSet_string f (s:string) = s
let map_exprSet_int f (i:int32) = i
let map_exprSet_bool f (b:bool) = b
let map_exprSet_option f = function Some x -> Some (f x) | None -> None
let rec map_exprSet_amn f  = function
    Machine(_x1, _x2) -> Machine((map_exprSet_head f _x1), ((List.map (map_exprSet_clause f)) _x2))
  | Refinement(_x1, _x2) -> Refinement((map_exprSet_head f _x1), ((List.map (map_exprSet_clause f)) _x2))
  | Implementation(_x1, _x2) -> Implementation((map_exprSet_head f _x1), ((List.map (map_exprSet_clause f)) _x2))
  | EmptyTree -> EmptyTree 
  and map_exprSet_head f  = fun (_x1, _x2) -> (map_exprSet_id f _x1), (map_exprSet_ids f _x2)
  and map_exprSet_clause f  = function
    Definitions(_x1) -> Definitions(((List.map (map_exprSet_def f)) _x1))
  | Constraints(_x1) -> Constraints((map_exprSet_predicate f _x1))
  | Invariant(_x1) -> Invariant((map_exprSet_predicate f _x1))
  | Sets(_x1) -> Sets(((List.map (map_exprSet_set f)) _x1))
  | Initialisation(_x1) -> Initialisation((map_exprSet_substitution f _x1))
  | ConstantsConcrete(_x1) -> ConstantsConcrete((map_exprSet_ids f _x1))
  | ConstantsAbstract(_x1) -> ConstantsAbstract((map_exprSet_ids f _x1))
  | ConstantsHidden(_x1) -> ConstantsHidden((map_exprSet_ids f _x1))
  | Properties(_x1) -> Properties((map_exprSet_predicate f _x1))
  | Values(_x1) -> Values(((List.map (fun (_x1, _x2) -> ((map_exprSet_id f _x1), (map_exprSet_expr f _x2)))) _x1))
  | VariablesConcrete(_x1) -> VariablesConcrete((map_exprSet_ids f _x1))
  | VariablesAbstract(_x1) -> VariablesAbstract((map_exprSet_ids f _x1))
  | VariablesHidden(_x1) -> VariablesHidden((map_exprSet_ids f _x1))
  | Promotes(_x1) -> Promotes((map_exprSet_ids f _x1))
  | Assertions(_x1) -> Assertions(((List.map (map_exprSet_predicate f)) _x1))
  | Operations(_x1) -> Operations(((List.map (map_exprSet_operation f)) _x1))
  | Sees(_x1) -> Sees((map_exprSet_ids f _x1))
  | Uses(_x1) -> Uses((map_exprSet_ids f _x1))
  | Extends(_x1) -> Extends(((List.map (map_exprSet_instance f)) _x1))
  | Includes(_x1) -> Includes(((List.map (map_exprSet_instance f)) _x1))
  | Imports(_x1) -> Imports(((List.map (map_exprSet_instance f)) _x1))
  | Refines(_x1) -> Refines((map_exprSet_id f _x1))
  and map_exprSet_def f  = function
    Def(_x1, _x2, _x3) -> Def((map_exprSet_id f _x1), (map_exprSet_ids f _x2), (map_exprSet_defBody f _x3))
  | FileDef(_x1) -> FileDef((map_exprSet_string f _x1))
  and map_exprSet_defBody f  = function
    ExprDefBody(_x1) -> ExprDefBody((map_exprSet_expr f _x1))
  | SubstDefBody(_x1) -> SubstDefBody((map_exprSet_substitution f _x1))
  and map_exprSet_set f  = function
    SetAbstDec(_x1) -> SetAbstDec((map_exprSet_id f _x1))
  | SetEnumDec(_x1, _x2) -> SetEnumDec((map_exprSet_id f _x1), (map_exprSet_ids f _x2))

  and map_exprSet_instance f  = fun (_x1, _x2) -> (map_exprSet_id f _x1), ((List.map (map_exprSet_expr f)) _x2)
  and map_exprSet_type_B f  = function
    TypeNatSetRange(_x1, _x2) -> TypeNatSetRange((map_exprSet_expr f _x1), (map_exprSet_expr f _x2))
  | PredType(_x1) -> PredType((map_exprSet_expr f _x1))

  | TypeIdentifier(_x1) -> TypeIdentifier((map_exprSet_id f _x1))
  | PFunType(_x1, _x2) -> PFunType((map_exprSet_type_B f _x1), (map_exprSet_type_B f _x2))
  | FunType(_x1, _x2) -> FunType((map_exprSet_type_B f _x1), (map_exprSet_type_B f _x2))
  | ProdType(_x1, _x2) -> ProdType((map_exprSet_type_B f _x1), (map_exprSet_type_B f _x2))
  | Sequence(_x1) -> Sequence((map_exprSet_type_B f _x1))
  | SubType(_x1, _x2) -> SubType((map_exprSet_type_B f _x1), (map_exprSet_expr f _x2))
  | Fin_B(_x1) -> Fin_B((map_exprSet_type_B f _x1))
  | Untyped -> Untyped 
  and map_exprSet_field f  = fun (_x1, _x2) -> (map_exprSet_id f _x1), (map_exprSet_type_B f _x2)
  and map_exprSet_predicate f  = function
    PredParen(_x1) -> PredParen((map_exprSet_predicate f _x1))
  | PredNegation(_x1) -> PredNegation((map_exprSet_predicate f _x1))
  | PredExists(_x1, _x2) -> PredExists(((List.map (map_exprSet_id f)) _x1), (map_exprSet_predicate f _x2))
  | PredForAll(_x1, _x2) -> PredForAll(((List.map (map_exprSet_id f)) _x1), (map_exprSet_predicate f _x2))
  | PredBin(_x1, _x2, _x3) -> PredBin(_x1, (map_exprSet_predicate f _x2), (map_exprSet_predicate f _x3))
  | PredAtom(_x1, _x2, _x3) -> PredAtom(_x1, (map_exprSet_expr f _x2), (map_exprSet_expr f _x3))
  and map_exprSet_operation f  = fun (_x1, _x2, _x3, _x4) -> (map_exprSet_id f _x1), (map_exprSet_ids f _x2), (map_exprSet_ids f _x3), (map_exprSet_substitution f _x4)
  and map_exprSet_expr f  = function
    ExprSequence(_x1, _x2) -> ExprSequence((map_exprSet_expr f _x1), (map_exprSet_expr f _x2))
  | ExprParen(_x1) -> ExprParen((map_exprSet_expr f _x1))
  | ExprId(_x1) -> ExprId((map_exprSet_id f _x1))
  | ExprFunCall(_x1, _x2) -> ExprFunCall((map_exprSet_expr f _x1), ((List.map (map_exprSet_expr f)) _x2))
  | ExprBin(_x1, _x2, _x3) -> ExprBin(_x1, (map_exprSet_expr f _x2), (map_exprSet_expr f _x3))
  | ExprUn(_x1, _x2) -> ExprUn(_x1, (map_exprSet_expr f _x2))
  | ExprSeq(_x1) -> ExprSeq((map_exprSet_exprSeq f _x1))
  | ExprSet(_x1) -> ExprSet((map_exprSet_exprSet f _x1))
  | RelSet(_x1, _x2) -> RelSet((map_exprSet_expr f _x1), (map_exprSet_expr f _x2))
  | RelSeqComp(_x1) -> RelSeqComp(((List.map (map_exprSet_expr f)) _x1))
  | ExprNumber(_x1) -> ExprNumber((map_exprSet_number f _x1))
  | ExprBool(_x1) -> ExprBool((map_exprSet_bool f _x1))
  | ExprString(_x1) -> ExprString((map_exprSet_string f _x1))

  | ExprPred(_x1) -> ExprPred((map_exprSet_predicate f _x1))
  | ExprNuplet(_x1) -> ExprNuplet(((List.map (map_exprSet_expr f)) _x1))
  | ExprSIGMA(_x1, _x2, _x3) -> ExprSIGMA(((List.map (map_exprSet_id f)) _x1), (map_exprSet_predicate f _x2), (map_exprSet_expr f _x3))
  | ExprPI(_x1, _x2, _x3) -> ExprPI(((List.map (map_exprSet_id f)) _x1), (map_exprSet_predicate f _x2), (map_exprSet_expr f _x3))
  | ExprLambda(_x1, _x2, _x3) -> ExprLambda(((List.map (map_exprSet_id f)) _x1), (map_exprSet_predicate f _x2), (map_exprSet_expr f _x3))

  and map_exprSet_exprSeq f  = function
    SeqEnum(_x1) -> SeqEnum(((List.map (map_exprSet_expr f)) _x1))
  | SeqEmpty -> SeqEmpty 
  and map_exprSet_exprSet f  = fun _x -> f((function
    SetRange(_x1, _x2) -> SetRange((map_exprSet_expr f _x1), (map_exprSet_expr f _x2))
  | SetEnum(_x1) -> SetEnum(((List.map (map_exprSet_expr f)) _x1))
  | SetCompr(_x1) -> SetCompr(((List.map (map_exprSet_expr f)) _x1))
  | SetComprPred(_x1, _x2) -> SetComprPred(((List.map (map_exprSet_id f)) _x1), (map_exprSet_predicate f _x2))
  | SetEmpty -> SetEmpty 
  | SetUnionQuantify(_x1, _x2, _x3) -> SetUnionQuantify(((List.map (map_exprSet_id f)) _x1), (map_exprSet_predicate f _x2), (map_exprSet_expr f _x3))
  | SetInterQuantify(_x1, _x2, _x3) -> SetInterQuantify(((List.map (map_exprSet_id f)) _x1), (map_exprSet_predicate f _x2), (map_exprSet_expr f _x3)))_x)
  and map_exprSet_number f  = function
    MinNumber -> MinNumber 
  | Number(_x1) -> Number((map_exprSet_int f _x1))
  | MaxNumber -> MaxNumber 
  and map_exprSet_substitution f  = function
    SubstOperCall(_x1, _x2, _x3) -> SubstOperCall((map_exprSet_ids f _x1), (map_exprSet_id f _x2), ((List.map (map_exprSet_expr f)) _x3))
  | SubstBlock(_x1) -> SubstBlock((map_exprSet_substitution f _x1))
  | SubstPrecondition(_x1, _x2) -> SubstPrecondition((map_exprSet_predicate f _x1), (map_exprSet_substitution f _x2))
  | SubstAssertion(_x1, _x2) -> SubstAssertion((map_exprSet_predicate f _x1), (map_exprSet_substitution f _x2))
  | SubstChoice(_x1) -> SubstChoice(((List.map (map_exprSet_substitution f)) _x1))
  | SubstIf(_x1, _x2) -> SubstIf(((List.map (fun (_x1, _x2) -> ((map_exprSet_predicate f _x1), (map_exprSet_substitution f _x2)))) _x1), ((map_exprSet_option (map_exprSet_substitution f)) _x2))
  | SubstSelect(_x1, _x2) -> SubstSelect(((List.map (fun (_x1, _x2) -> ((map_exprSet_predicate f _x1), (map_exprSet_substitution f _x2)))) _x1), ((map_exprSet_option (map_exprSet_substitution f)) _x2))
  | SubstCase(_x1, _x2, _x3) -> SubstCase((map_exprSet_expr f _x1), ((List.map (fun (_x1, _x2) -> (((List.map (map_exprSet_expr f)) _x1), (map_exprSet_substitution f _x2)))) _x2), ((map_exprSet_option (map_exprSet_substitution f)) _x3))
  | SubstAny(_x1, _x2, _x3) -> SubstAny(((List.map (map_exprSet_id f)) _x1), (map_exprSet_predicate f _x2), (map_exprSet_substitution f _x3))
  | SubstLet(_x1, _x2, _x3) -> SubstLet(((List.map (map_exprSet_id f)) _x1), (map_exprSet_predicate f _x2), (map_exprSet_substitution f _x3))
  | SubstVar(_x1, _x2) -> SubstVar(((List.map (map_exprSet_id f)) _x1), (map_exprSet_substitution f _x2))
  | SubstWhile(_x1, _x2, _x3, _x4) -> SubstWhile((map_exprSet_predicate f _x1), (map_exprSet_substitution f _x2), (map_exprSet_expr f _x3), (map_exprSet_predicate f _x4))
  | SubstSkip -> SubstSkip 
  | SubstSequence(_x1, _x2) -> SubstSequence((map_exprSet_substitution f _x1), (map_exprSet_substitution f _x2))
  | SubstParallel(_x1, _x2) -> SubstParallel((map_exprSet_substitution f _x1), (map_exprSet_substitution f _x2))
  | SubstSetEqualIds(_x1, _x2) -> SubstSetEqualIds((map_exprSet_ids f _x1), ((List.map (map_exprSet_expr f)) _x2))
  | SubstSetEqualFun(_x1, _x2, _x3) -> SubstSetEqualFun((map_exprSet_expr f _x1), ((List.map (map_exprSet_expr f)) _x2), (map_exprSet_expr f _x3))

  | SubstBecomeSuch(_x1, _x2) -> SubstBecomeSuch((map_exprSet_ids f _x1), (map_exprSet_predicate f _x2))
  | SubstSetIn(_x1, _x2) -> SubstSetIn((map_exprSet_ids f _x1), (map_exprSet_expr f _x2))
  and map_exprSet_ids f  = fun (_x1) -> ((List.map (map_exprSet_id f)) _x1)
  and map_exprSet_origine f  = function
    Local -> Local 
  | Remote(_x1) -> Remote(((List.map (fun (_x1, _x2) -> ((map_exprSet_string f _x1), (map_exprSet_string f _x2)))) _x1))
  | OrgRefine(_x1, _x2) -> OrgRefine((map_exprSet_string f _x1), ((List.map (fun (_x1, _x2) -> ((map_exprSet_string f _x1), (map_exprSet_string f _x2)))) _x2))
  | Prefix(_x1) -> Prefix(((List.map (map_exprSet_string f)) _x1))
  and map_exprSet_id f  = function
    Id(_x1, _x2, _x3, _x4) -> Id((map_exprSet_origine f _x1), (map_exprSet_string f _x2), ((map_exprSet_option (map_exprSet_type_B f)) _x3), _x4)

let map_number_string f (s:string) = s
let map_number_int f (i:int32) = i
let map_number_bool f (b:bool) = b
let map_number_option f = function Some x -> Some (f x) | None -> None
let rec map_number_amn f  = function
    Machine(_x1, _x2) -> Machine((map_number_head f _x1), ((List.map (map_number_clause f)) _x2))
  | Refinement(_x1, _x2) -> Refinement((map_number_head f _x1), ((List.map (map_number_clause f)) _x2))
  | Implementation(_x1, _x2) -> Implementation((map_number_head f _x1), ((List.map (map_number_clause f)) _x2))
  | EmptyTree -> EmptyTree 
  and map_number_head f  = fun (_x1, _x2) -> (map_number_id f _x1), (map_number_ids f _x2)
  and map_number_clause f  = function
    Definitions(_x1) -> Definitions(((List.map (map_number_def f)) _x1))
  | Constraints(_x1) -> Constraints((map_number_predicate f _x1))
  | Invariant(_x1) -> Invariant((map_number_predicate f _x1))
  | Sets(_x1) -> Sets(((List.map (map_number_set f)) _x1))
  | Initialisation(_x1) -> Initialisation((map_number_substitution f _x1))
  | ConstantsConcrete(_x1) -> ConstantsConcrete((map_number_ids f _x1))
  | ConstantsAbstract(_x1) -> ConstantsAbstract((map_number_ids f _x1))
  | ConstantsHidden(_x1) -> ConstantsHidden((map_number_ids f _x1))
  | Properties(_x1) -> Properties((map_number_predicate f _x1))
  | Values(_x1) -> Values(((List.map (fun (_x1, _x2) -> ((map_number_id f _x1), (map_number_expr f _x2)))) _x1))
  | VariablesConcrete(_x1) -> VariablesConcrete((map_number_ids f _x1))
  | VariablesAbstract(_x1) -> VariablesAbstract((map_number_ids f _x1))
  | VariablesHidden(_x1) -> VariablesHidden((map_number_ids f _x1))
  | Promotes(_x1) -> Promotes((map_number_ids f _x1))
  | Assertions(_x1) -> Assertions(((List.map (map_number_predicate f)) _x1))
  | Operations(_x1) -> Operations(((List.map (map_number_operation f)) _x1))
  | Sees(_x1) -> Sees((map_number_ids f _x1))
  | Uses(_x1) -> Uses((map_number_ids f _x1))
  | Extends(_x1) -> Extends(((List.map (map_number_instance f)) _x1))
  | Includes(_x1) -> Includes(((List.map (map_number_instance f)) _x1))
  | Imports(_x1) -> Imports(((List.map (map_number_instance f)) _x1))
  | Refines(_x1) -> Refines((map_number_id f _x1))
  and map_number_def f  = function
    Def(_x1, _x2, _x3) -> Def((map_number_id f _x1), (map_number_ids f _x2), (map_number_defBody f _x3))
  | FileDef(_x1) -> FileDef((map_number_string f _x1))
  and map_number_defBody f  = function
    ExprDefBody(_x1) -> ExprDefBody((map_number_expr f _x1))
  | SubstDefBody(_x1) -> SubstDefBody((map_number_substitution f _x1))
  and map_number_set f  = function
    SetAbstDec(_x1) -> SetAbstDec((map_number_id f _x1))
  | SetEnumDec(_x1, _x2) -> SetEnumDec((map_number_id f _x1), (map_number_ids f _x2))

  and map_number_instance f  = fun (_x1, _x2) -> (map_number_id f _x1), ((List.map (map_number_expr f)) _x2)
  and map_number_type_B f  = function
    TypeNatSetRange(_x1, _x2) -> TypeNatSetRange((map_number_expr f _x1), (map_number_expr f _x2))
  | PredType(_x1) -> PredType((map_number_expr f _x1))

  | TypeIdentifier(_x1) -> TypeIdentifier((map_number_id f _x1))
  | PFunType(_x1, _x2) -> PFunType((map_number_type_B f _x1), (map_number_type_B f _x2))
  | FunType(_x1, _x2) -> FunType((map_number_type_B f _x1), (map_number_type_B f _x2))
  | ProdType(_x1, _x2) -> ProdType((map_number_type_B f _x1), (map_number_type_B f _x2))
  | Sequence(_x1) -> Sequence((map_number_type_B f _x1))
  | SubType(_x1, _x2) -> SubType((map_number_type_B f _x1), (map_number_expr f _x2))
  | Fin_B(_x1) -> Fin_B((map_number_type_B f _x1))
  | Untyped -> Untyped 
  and map_number_field f  = fun (_x1, _x2) -> (map_number_id f _x1), (map_number_type_B f _x2)
  and map_number_predicate f  = function
    PredParen(_x1) -> PredParen((map_number_predicate f _x1))
  | PredNegation(_x1) -> PredNegation((map_number_predicate f _x1))
  | PredExists(_x1, _x2) -> PredExists(((List.map (map_number_id f)) _x1), (map_number_predicate f _x2))
  | PredForAll(_x1, _x2) -> PredForAll(((List.map (map_number_id f)) _x1), (map_number_predicate f _x2))
  | PredBin(_x1, _x2, _x3) -> PredBin(_x1, (map_number_predicate f _x2), (map_number_predicate f _x3))
  | PredAtom(_x1, _x2, _x3) -> PredAtom(_x1, (map_number_expr f _x2), (map_number_expr f _x3))
  and map_number_operation f  = fun (_x1, _x2, _x3, _x4) -> (map_number_id f _x1), (map_number_ids f _x2), (map_number_ids f _x3), (map_number_substitution f _x4)
  and map_number_expr f  = function
    ExprSequence(_x1, _x2) -> ExprSequence((map_number_expr f _x1), (map_number_expr f _x2))
  | ExprParen(_x1) -> ExprParen((map_number_expr f _x1))
  | ExprId(_x1) -> ExprId((map_number_id f _x1))
  | ExprFunCall(_x1, _x2) -> ExprFunCall((map_number_expr f _x1), ((List.map (map_number_expr f)) _x2))
  | ExprBin(_x1, _x2, _x3) -> ExprBin(_x1, (map_number_expr f _x2), (map_number_expr f _x3))
  | ExprUn(_x1, _x2) -> ExprUn(_x1, (map_number_expr f _x2))
  | ExprSeq(_x1) -> ExprSeq((map_number_exprSeq f _x1))
  | ExprSet(_x1) -> ExprSet((map_number_exprSet f _x1))
  | RelSet(_x1, _x2) -> RelSet((map_number_expr f _x1), (map_number_expr f _x2))
  | RelSeqComp(_x1) -> RelSeqComp(((List.map (map_number_expr f)) _x1))
  | ExprNumber(_x1) -> ExprNumber((map_number_number f _x1))
  | ExprBool(_x1) -> ExprBool((map_number_bool f _x1))
  | ExprString(_x1) -> ExprString((map_number_string f _x1))

  | ExprPred(_x1) -> ExprPred((map_number_predicate f _x1))
  | ExprNuplet(_x1) -> ExprNuplet(((List.map (map_number_expr f)) _x1))
  | ExprSIGMA(_x1, _x2, _x3) -> ExprSIGMA(((List.map (map_number_id f)) _x1), (map_number_predicate f _x2), (map_number_expr f _x3))
  | ExprPI(_x1, _x2, _x3) -> ExprPI(((List.map (map_number_id f)) _x1), (map_number_predicate f _x2), (map_number_expr f _x3))
  | ExprLambda(_x1, _x2, _x3) -> ExprLambda(((List.map (map_number_id f)) _x1), (map_number_predicate f _x2), (map_number_expr f _x3))

  and map_number_exprSeq f  = function
    SeqEnum(_x1) -> SeqEnum(((List.map (map_number_expr f)) _x1))
  | SeqEmpty -> SeqEmpty 
  and map_number_exprSet f  = function
    SetRange(_x1, _x2) -> SetRange((map_number_expr f _x1), (map_number_expr f _x2))
  | SetEnum(_x1) -> SetEnum(((List.map (map_number_expr f)) _x1))
  | SetCompr(_x1) -> SetCompr(((List.map (map_number_expr f)) _x1))
  | SetComprPred(_x1, _x2) -> SetComprPred(((List.map (map_number_id f)) _x1), (map_number_predicate f _x2))
  | SetEmpty -> SetEmpty 
  | SetUnionQuantify(_x1, _x2, _x3) -> SetUnionQuantify(((List.map (map_number_id f)) _x1), (map_number_predicate f _x2), (map_number_expr f _x3))
  | SetInterQuantify(_x1, _x2, _x3) -> SetInterQuantify(((List.map (map_number_id f)) _x1), (map_number_predicate f _x2), (map_number_expr f _x3))
  and map_number_number f  = fun _x -> f((function
    MinNumber -> MinNumber 
  | Number(_x1) -> Number((map_number_int f _x1))
  | MaxNumber -> MaxNumber )_x)
  and map_number_substitution f  = function
    SubstOperCall(_x1, _x2, _x3) -> SubstOperCall((map_number_ids f _x1), (map_number_id f _x2), ((List.map (map_number_expr f)) _x3))
  | SubstBlock(_x1) -> SubstBlock((map_number_substitution f _x1))
  | SubstPrecondition(_x1, _x2) -> SubstPrecondition((map_number_predicate f _x1), (map_number_substitution f _x2))
  | SubstAssertion(_x1, _x2) -> SubstAssertion((map_number_predicate f _x1), (map_number_substitution f _x2))
  | SubstChoice(_x1) -> SubstChoice(((List.map (map_number_substitution f)) _x1))
  | SubstIf(_x1, _x2) -> SubstIf(((List.map (fun (_x1, _x2) -> ((map_number_predicate f _x1), (map_number_substitution f _x2)))) _x1), ((map_number_option (map_number_substitution f)) _x2))
  | SubstSelect(_x1, _x2) -> SubstSelect(((List.map (fun (_x1, _x2) -> ((map_number_predicate f _x1), (map_number_substitution f _x2)))) _x1), ((map_number_option (map_number_substitution f)) _x2))
  | SubstCase(_x1, _x2, _x3) -> SubstCase((map_number_expr f _x1), ((List.map (fun (_x1, _x2) -> (((List.map (map_number_expr f)) _x1), (map_number_substitution f _x2)))) _x2), ((map_number_option (map_number_substitution f)) _x3))
  | SubstAny(_x1, _x2, _x3) -> SubstAny(((List.map (map_number_id f)) _x1), (map_number_predicate f _x2), (map_number_substitution f _x3))
  | SubstLet(_x1, _x2, _x3) -> SubstLet(((List.map (map_number_id f)) _x1), (map_number_predicate f _x2), (map_number_substitution f _x3))
  | SubstVar(_x1, _x2) -> SubstVar(((List.map (map_number_id f)) _x1), (map_number_substitution f _x2))
  | SubstWhile(_x1, _x2, _x3, _x4) -> SubstWhile((map_number_predicate f _x1), (map_number_substitution f _x2), (map_number_expr f _x3), (map_number_predicate f _x4))
  | SubstSkip -> SubstSkip 
  | SubstSequence(_x1, _x2) -> SubstSequence((map_number_substitution f _x1), (map_number_substitution f _x2))
  | SubstParallel(_x1, _x2) -> SubstParallel((map_number_substitution f _x1), (map_number_substitution f _x2))
  | SubstSetEqualIds(_x1, _x2) -> SubstSetEqualIds((map_number_ids f _x1), ((List.map (map_number_expr f)) _x2))
  | SubstSetEqualFun(_x1, _x2, _x3) -> SubstSetEqualFun((map_number_expr f _x1), ((List.map (map_number_expr f)) _x2), (map_number_expr f _x3))

  | SubstBecomeSuch(_x1, _x2) -> SubstBecomeSuch((map_number_ids f _x1), (map_number_predicate f _x2))
  | SubstSetIn(_x1, _x2) -> SubstSetIn((map_number_ids f _x1), (map_number_expr f _x2))
  and map_number_ids f  = fun (_x1) -> ((List.map (map_number_id f)) _x1)
  and map_number_origine f  = function
    Local -> Local 
  | Remote(_x1) -> Remote(((List.map (fun (_x1, _x2) -> ((map_number_string f _x1), (map_number_string f _x2)))) _x1))
  | OrgRefine(_x1, _x2) -> OrgRefine((map_number_string f _x1), ((List.map (fun (_x1, _x2) -> ((map_number_string f _x1), (map_number_string f _x2)))) _x2))
  | Prefix(_x1) -> Prefix(((List.map (map_number_string f)) _x1))
  and map_number_id f  = function
    Id(_x1, _x2, _x3, _x4) -> Id((map_number_origine f _x1), (map_number_string f _x2), ((map_number_option (map_number_type_B f)) _x3), _x4)

let map_substitution_string f (s:string) = s
let map_substitution_int f (i:int32) = i
let map_substitution_bool f (b:bool) = b
let map_substitution_option f = function Some x -> Some (f x) | None -> None
let rec map_substitution_amn f  = function
    Machine(_x1, _x2) -> Machine((map_substitution_head f _x1), ((List.map (map_substitution_clause f)) _x2))
  | Refinement(_x1, _x2) -> Refinement((map_substitution_head f _x1), ((List.map (map_substitution_clause f)) _x2))
  | Implementation(_x1, _x2) -> Implementation((map_substitution_head f _x1), ((List.map (map_substitution_clause f)) _x2))
  | EmptyTree -> EmptyTree 
  and map_substitution_head f  = fun (_x1, _x2) -> (map_substitution_id f _x1), (map_substitution_ids f _x2)
  and map_substitution_clause f  = function
    Definitions(_x1) -> Definitions(((List.map (map_substitution_def f)) _x1))
  | Constraints(_x1) -> Constraints((map_substitution_predicate f _x1))
  | Invariant(_x1) -> Invariant((map_substitution_predicate f _x1))
  | Sets(_x1) -> Sets(((List.map (map_substitution_set f)) _x1))
  | Initialisation(_x1) -> Initialisation((map_substitution_substitution f _x1))
  | ConstantsConcrete(_x1) -> ConstantsConcrete((map_substitution_ids f _x1))
  | ConstantsAbstract(_x1) -> ConstantsAbstract((map_substitution_ids f _x1))
  | ConstantsHidden(_x1) -> ConstantsHidden((map_substitution_ids f _x1))
  | Properties(_x1) -> Properties((map_substitution_predicate f _x1))
  | Values(_x1) -> Values(((List.map (fun (_x1, _x2) -> ((map_substitution_id f _x1), (map_substitution_expr f _x2)))) _x1))
  | VariablesConcrete(_x1) -> VariablesConcrete((map_substitution_ids f _x1))
  | VariablesAbstract(_x1) -> VariablesAbstract((map_substitution_ids f _x1))
  | VariablesHidden(_x1) -> VariablesHidden((map_substitution_ids f _x1))
  | Promotes(_x1) -> Promotes((map_substitution_ids f _x1))
  | Assertions(_x1) -> Assertions(((List.map (map_substitution_predicate f)) _x1))
  | Operations(_x1) -> Operations(((List.map (map_substitution_operation f)) _x1))
  | Sees(_x1) -> Sees((map_substitution_ids f _x1))
  | Uses(_x1) -> Uses((map_substitution_ids f _x1))
  | Extends(_x1) -> Extends(((List.map (map_substitution_instance f)) _x1))
  | Includes(_x1) -> Includes(((List.map (map_substitution_instance f)) _x1))
  | Imports(_x1) -> Imports(((List.map (map_substitution_instance f)) _x1))
  | Refines(_x1) -> Refines((map_substitution_id f _x1))
  and map_substitution_def f  = function
    Def(_x1, _x2, _x3) -> Def((map_substitution_id f _x1), (map_substitution_ids f _x2), (map_substitution_defBody f _x3))
  | FileDef(_x1) -> FileDef((map_substitution_string f _x1))
  and map_substitution_defBody f  = function
    ExprDefBody(_x1) -> ExprDefBody((map_substitution_expr f _x1))
  | SubstDefBody(_x1) -> SubstDefBody((map_substitution_substitution f _x1))
  and map_substitution_set f  = function
    SetAbstDec(_x1) -> SetAbstDec((map_substitution_id f _x1))
  | SetEnumDec(_x1, _x2) -> SetEnumDec((map_substitution_id f _x1), (map_substitution_ids f _x2))

  and map_substitution_instance f  = fun (_x1, _x2) -> (map_substitution_id f _x1), ((List.map (map_substitution_expr f)) _x2)
  and map_substitution_type_B f  = function
    TypeNatSetRange(_x1, _x2) -> TypeNatSetRange((map_substitution_expr f _x1), (map_substitution_expr f _x2))
  | PredType(_x1) -> PredType((map_substitution_expr f _x1))

  | TypeIdentifier(_x1) -> TypeIdentifier((map_substitution_id f _x1))
  | PFunType(_x1, _x2) -> PFunType((map_substitution_type_B f _x1), (map_substitution_type_B f _x2))
  | FunType(_x1, _x2) -> FunType((map_substitution_type_B f _x1), (map_substitution_type_B f _x2))
  | ProdType(_x1, _x2) -> ProdType((map_substitution_type_B f _x1), (map_substitution_type_B f _x2))
  | Sequence(_x1) -> Sequence((map_substitution_type_B f _x1))
  | SubType(_x1, _x2) -> SubType((map_substitution_type_B f _x1), (map_substitution_expr f _x2))
  | Fin_B(_x1) -> Fin_B((map_substitution_type_B f _x1))
  | Untyped -> Untyped 
  and map_substitution_field f  = fun (_x1, _x2) -> (map_substitution_id f _x1), (map_substitution_type_B f _x2)
  and map_substitution_predicate f  = function
    PredParen(_x1) -> PredParen((map_substitution_predicate f _x1))
  | PredNegation(_x1) -> PredNegation((map_substitution_predicate f _x1))
  | PredExists(_x1, _x2) -> PredExists(((List.map (map_substitution_id f)) _x1), (map_substitution_predicate f _x2))
  | PredForAll(_x1, _x2) -> PredForAll(((List.map (map_substitution_id f)) _x1), (map_substitution_predicate f _x2))
  | PredBin(_x1, _x2, _x3) -> PredBin(_x1, (map_substitution_predicate f _x2), (map_substitution_predicate f _x3))
  | PredAtom(_x1, _x2, _x3) -> PredAtom(_x1, (map_substitution_expr f _x2), (map_substitution_expr f _x3))
  and map_substitution_operation f  = fun (_x1, _x2, _x3, _x4) -> (map_substitution_id f _x1), (map_substitution_ids f _x2), (map_substitution_ids f _x3), (map_substitution_substitution f _x4)
  and map_substitution_expr f  = function
    ExprSequence(_x1, _x2) -> ExprSequence((map_substitution_expr f _x1), (map_substitution_expr f _x2))
  | ExprParen(_x1) -> ExprParen((map_substitution_expr f _x1))
  | ExprId(_x1) -> ExprId((map_substitution_id f _x1))
  | ExprFunCall(_x1, _x2) -> ExprFunCall((map_substitution_expr f _x1), ((List.map (map_substitution_expr f)) _x2))
  | ExprBin(_x1, _x2, _x3) -> ExprBin(_x1, (map_substitution_expr f _x2), (map_substitution_expr f _x3))
  | ExprUn(_x1, _x2) -> ExprUn(_x1, (map_substitution_expr f _x2))
  | ExprSeq(_x1) -> ExprSeq((map_substitution_exprSeq f _x1))
  | ExprSet(_x1) -> ExprSet((map_substitution_exprSet f _x1))
  | RelSet(_x1, _x2) -> RelSet((map_substitution_expr f _x1), (map_substitution_expr f _x2))
  | RelSeqComp(_x1) -> RelSeqComp(((List.map (map_substitution_expr f)) _x1))
  | ExprNumber(_x1) -> ExprNumber((map_substitution_number f _x1))
  | ExprBool(_x1) -> ExprBool((map_substitution_bool f _x1))
  | ExprString(_x1) -> ExprString((map_substitution_string f _x1))

  | ExprPred(_x1) -> ExprPred((map_substitution_predicate f _x1))
  | ExprNuplet(_x1) -> ExprNuplet(((List.map (map_substitution_expr f)) _x1))
  | ExprSIGMA(_x1, _x2, _x3) -> ExprSIGMA(((List.map (map_substitution_id f)) _x1), (map_substitution_predicate f _x2), (map_substitution_expr f _x3))
  | ExprPI(_x1, _x2, _x3) -> ExprPI(((List.map (map_substitution_id f)) _x1), (map_substitution_predicate f _x2), (map_substitution_expr f _x3))
  | ExprLambda(_x1, _x2, _x3) -> ExprLambda(((List.map (map_substitution_id f)) _x1), (map_substitution_predicate f _x2), (map_substitution_expr f _x3))

  and map_substitution_exprSeq f  = function
    SeqEnum(_x1) -> SeqEnum(((List.map (map_substitution_expr f)) _x1))
  | SeqEmpty -> SeqEmpty 
  and map_substitution_exprSet f  = function
    SetRange(_x1, _x2) -> SetRange((map_substitution_expr f _x1), (map_substitution_expr f _x2))
  | SetEnum(_x1) -> SetEnum(((List.map (map_substitution_expr f)) _x1))
  | SetCompr(_x1) -> SetCompr(((List.map (map_substitution_expr f)) _x1))
  | SetComprPred(_x1, _x2) -> SetComprPred(((List.map (map_substitution_id f)) _x1), (map_substitution_predicate f _x2))
  | SetEmpty -> SetEmpty 
  | SetUnionQuantify(_x1, _x2, _x3) -> SetUnionQuantify(((List.map (map_substitution_id f)) _x1), (map_substitution_predicate f _x2), (map_substitution_expr f _x3))
  | SetInterQuantify(_x1, _x2, _x3) -> SetInterQuantify(((List.map (map_substitution_id f)) _x1), (map_substitution_predicate f _x2), (map_substitution_expr f _x3))
  and map_substitution_number f  = function
    MinNumber -> MinNumber 
  | Number(_x1) -> Number((map_substitution_int f _x1))
  | MaxNumber -> MaxNumber 
  and map_substitution_substitution f  = fun _x -> f((function
    SubstOperCall(_x1, _x2, _x3) -> SubstOperCall((map_substitution_ids f _x1), (map_substitution_id f _x2), ((List.map (map_substitution_expr f)) _x3))
  | SubstBlock(_x1) -> SubstBlock((map_substitution_substitution f _x1))
  | SubstPrecondition(_x1, _x2) -> SubstPrecondition((map_substitution_predicate f _x1), (map_substitution_substitution f _x2))
  | SubstAssertion(_x1, _x2) -> SubstAssertion((map_substitution_predicate f _x1), (map_substitution_substitution f _x2))
  | SubstChoice(_x1) -> SubstChoice(((List.map (map_substitution_substitution f)) _x1))
  | SubstIf(_x1, _x2) -> SubstIf(((List.map (fun (_x1, _x2) -> ((map_substitution_predicate f _x1), (map_substitution_substitution f _x2)))) _x1), ((map_substitution_option (map_substitution_substitution f)) _x2))
  | SubstSelect(_x1, _x2) -> SubstSelect(((List.map (fun (_x1, _x2) -> ((map_substitution_predicate f _x1), (map_substitution_substitution f _x2)))) _x1), ((map_substitution_option (map_substitution_substitution f)) _x2))
  | SubstCase(_x1, _x2, _x3) -> SubstCase((map_substitution_expr f _x1), ((List.map (fun (_x1, _x2) -> (((List.map (map_substitution_expr f)) _x1), (map_substitution_substitution f _x2)))) _x2), ((map_substitution_option (map_substitution_substitution f)) _x3))
  | SubstAny(_x1, _x2, _x3) -> SubstAny(((List.map (map_substitution_id f)) _x1), (map_substitution_predicate f _x2), (map_substitution_substitution f _x3))
  | SubstLet(_x1, _x2, _x3) -> SubstLet(((List.map (map_substitution_id f)) _x1), (map_substitution_predicate f _x2), (map_substitution_substitution f _x3))
  | SubstVar(_x1, _x2) -> SubstVar(((List.map (map_substitution_id f)) _x1), (map_substitution_substitution f _x2))
  | SubstWhile(_x1, _x2, _x3, _x4) -> SubstWhile((map_substitution_predicate f _x1), (map_substitution_substitution f _x2), (map_substitution_expr f _x3), (map_substitution_predicate f _x4))
  | SubstSkip -> SubstSkip 
  | SubstSequence(_x1, _x2) -> SubstSequence((map_substitution_substitution f _x1), (map_substitution_substitution f _x2))
  | SubstParallel(_x1, _x2) -> SubstParallel((map_substitution_substitution f _x1), (map_substitution_substitution f _x2))
  | SubstSetEqualIds(_x1, _x2) -> SubstSetEqualIds((map_substitution_ids f _x1), ((List.map (map_substitution_expr f)) _x2))
  | SubstSetEqualFun(_x1, _x2, _x3) -> SubstSetEqualFun((map_substitution_expr f _x1), ((List.map (map_substitution_expr f)) _x2), (map_substitution_expr f _x3))

  | SubstBecomeSuch(_x1, _x2) -> SubstBecomeSuch((map_substitution_ids f _x1), (map_substitution_predicate f _x2))
  | SubstSetIn(_x1, _x2) -> SubstSetIn((map_substitution_ids f _x1), (map_substitution_expr f _x2)))_x)
  and map_substitution_ids f  = fun (_x1) -> ((List.map (map_substitution_id f)) _x1)
  and map_substitution_origine f  = function
    Local -> Local 
  | Remote(_x1) -> Remote(((List.map (fun (_x1, _x2) -> ((map_substitution_string f _x1), (map_substitution_string f _x2)))) _x1))
  | OrgRefine(_x1, _x2) -> OrgRefine((map_substitution_string f _x1), ((List.map (fun (_x1, _x2) -> ((map_substitution_string f _x1), (map_substitution_string f _x2)))) _x2))
  | Prefix(_x1) -> Prefix(((List.map (map_substitution_string f)) _x1))
  and map_substitution_id f  = function
    Id(_x1, _x2, _x3, _x4) -> Id((map_substitution_origine f _x1), (map_substitution_string f _x2), ((map_substitution_option (map_substitution_type_B f)) _x3), _x4)

let map_ids_string f (s:string) = s
let map_ids_int f (i:int32) = i
let map_ids_bool f (b:bool) = b
let map_ids_option f = function Some x -> Some (f x) | None -> None
let rec map_ids_amn f  = function
    Machine(_x1, _x2) -> Machine((map_ids_head f _x1), ((List.map (map_ids_clause f)) _x2))
  | Refinement(_x1, _x2) -> Refinement((map_ids_head f _x1), ((List.map (map_ids_clause f)) _x2))
  | Implementation(_x1, _x2) -> Implementation((map_ids_head f _x1), ((List.map (map_ids_clause f)) _x2))
  | EmptyTree -> EmptyTree 
  and map_ids_head f  = fun (_x1, _x2) -> (map_ids_id f _x1), (map_ids_ids f _x2)
  and map_ids_clause f  = function
    Definitions(_x1) -> Definitions(((List.map (map_ids_def f)) _x1))
  | Constraints(_x1) -> Constraints((map_ids_predicate f _x1))
  | Invariant(_x1) -> Invariant((map_ids_predicate f _x1))
  | Sets(_x1) -> Sets(((List.map (map_ids_set f)) _x1))
  | Initialisation(_x1) -> Initialisation((map_ids_substitution f _x1))
  | ConstantsConcrete(_x1) -> ConstantsConcrete((map_ids_ids f _x1))
  | ConstantsAbstract(_x1) -> ConstantsAbstract((map_ids_ids f _x1))
  | ConstantsHidden(_x1) -> ConstantsHidden((map_ids_ids f _x1))
  | Properties(_x1) -> Properties((map_ids_predicate f _x1))
  | Values(_x1) -> Values(((List.map (fun (_x1, _x2) -> ((map_ids_id f _x1), (map_ids_expr f _x2)))) _x1))
  | VariablesConcrete(_x1) -> VariablesConcrete((map_ids_ids f _x1))
  | VariablesAbstract(_x1) -> VariablesAbstract((map_ids_ids f _x1))
  | VariablesHidden(_x1) -> VariablesHidden((map_ids_ids f _x1))
  | Promotes(_x1) -> Promotes((map_ids_ids f _x1))
  | Assertions(_x1) -> Assertions(((List.map (map_ids_predicate f)) _x1))
  | Operations(_x1) -> Operations(((List.map (map_ids_operation f)) _x1))
  | Sees(_x1) -> Sees((map_ids_ids f _x1))
  | Uses(_x1) -> Uses((map_ids_ids f _x1))
  | Extends(_x1) -> Extends(((List.map (map_ids_instance f)) _x1))
  | Includes(_x1) -> Includes(((List.map (map_ids_instance f)) _x1))
  | Imports(_x1) -> Imports(((List.map (map_ids_instance f)) _x1))
  | Refines(_x1) -> Refines((map_ids_id f _x1))
  and map_ids_def f  = function
    Def(_x1, _x2, _x3) -> Def((map_ids_id f _x1), (map_ids_ids f _x2), (map_ids_defBody f _x3))
  | FileDef(_x1) -> FileDef((map_ids_string f _x1))
  and map_ids_defBody f  = function
    ExprDefBody(_x1) -> ExprDefBody((map_ids_expr f _x1))
  | SubstDefBody(_x1) -> SubstDefBody((map_ids_substitution f _x1))
  and map_ids_set f  = function
    SetAbstDec(_x1) -> SetAbstDec((map_ids_id f _x1))
  | SetEnumDec(_x1, _x2) -> SetEnumDec((map_ids_id f _x1), (map_ids_ids f _x2))

  and map_ids_instance f  = fun (_x1, _x2) -> (map_ids_id f _x1), ((List.map (map_ids_expr f)) _x2)
  and map_ids_type_B f  = function
    TypeNatSetRange(_x1, _x2) -> TypeNatSetRange((map_ids_expr f _x1), (map_ids_expr f _x2))
  | PredType(_x1) -> PredType((map_ids_expr f _x1))

  | TypeIdentifier(_x1) -> TypeIdentifier((map_ids_id f _x1))
  | PFunType(_x1, _x2) -> PFunType((map_ids_type_B f _x1), (map_ids_type_B f _x2))
  | FunType(_x1, _x2) -> FunType((map_ids_type_B f _x1), (map_ids_type_B f _x2))
  | ProdType(_x1, _x2) -> ProdType((map_ids_type_B f _x1), (map_ids_type_B f _x2))
  | Sequence(_x1) -> Sequence((map_ids_type_B f _x1))
  | SubType(_x1, _x2) -> SubType((map_ids_type_B f _x1), (map_ids_expr f _x2))
  | Fin_B(_x1) -> Fin_B((map_ids_type_B f _x1))
  | Untyped -> Untyped 
  and map_ids_field f  = fun (_x1, _x2) -> (map_ids_id f _x1), (map_ids_type_B f _x2)
  and map_ids_predicate f  = function
    PredParen(_x1) -> PredParen((map_ids_predicate f _x1))
  | PredNegation(_x1) -> PredNegation((map_ids_predicate f _x1))
  | PredExists(_x1, _x2) -> PredExists(((List.map (map_ids_id f)) _x1), (map_ids_predicate f _x2))
  | PredForAll(_x1, _x2) -> PredForAll(((List.map (map_ids_id f)) _x1), (map_ids_predicate f _x2))
  | PredBin(_x1, _x2, _x3) -> PredBin(_x1, (map_ids_predicate f _x2), (map_ids_predicate f _x3))
  | PredAtom(_x1, _x2, _x3) -> PredAtom(_x1, (map_ids_expr f _x2), (map_ids_expr f _x3))
  and map_ids_operation f  = fun (_x1, _x2, _x3, _x4) -> (map_ids_id f _x1), (map_ids_ids f _x2), (map_ids_ids f _x3), (map_ids_substitution f _x4)
  and map_ids_expr f  = function
    ExprSequence(_x1, _x2) -> ExprSequence((map_ids_expr f _x1), (map_ids_expr f _x2))
  | ExprParen(_x1) -> ExprParen((map_ids_expr f _x1))
  | ExprId(_x1) -> ExprId((map_ids_id f _x1))
  | ExprFunCall(_x1, _x2) -> ExprFunCall((map_ids_expr f _x1), ((List.map (map_ids_expr f)) _x2))
  | ExprBin(_x1, _x2, _x3) -> ExprBin(_x1, (map_ids_expr f _x2), (map_ids_expr f _x3))
  | ExprUn(_x1, _x2) -> ExprUn(_x1, (map_ids_expr f _x2))
  | ExprSeq(_x1) -> ExprSeq((map_ids_exprSeq f _x1))
  | ExprSet(_x1) -> ExprSet((map_ids_exprSet f _x1))
  | RelSet(_x1, _x2) -> RelSet((map_ids_expr f _x1), (map_ids_expr f _x2))
  | RelSeqComp(_x1) -> RelSeqComp(((List.map (map_ids_expr f)) _x1))
  | ExprNumber(_x1) -> ExprNumber((map_ids_number f _x1))
  | ExprBool(_x1) -> ExprBool((map_ids_bool f _x1))
  | ExprString(_x1) -> ExprString((map_ids_string f _x1))

  | ExprPred(_x1) -> ExprPred((map_ids_predicate f _x1))
  | ExprNuplet(_x1) -> ExprNuplet(((List.map (map_ids_expr f)) _x1))
  | ExprSIGMA(_x1, _x2, _x3) -> ExprSIGMA(((List.map (map_ids_id f)) _x1), (map_ids_predicate f _x2), (map_ids_expr f _x3))
  | ExprPI(_x1, _x2, _x3) -> ExprPI(((List.map (map_ids_id f)) _x1), (map_ids_predicate f _x2), (map_ids_expr f _x3))
  | ExprLambda(_x1, _x2, _x3) -> ExprLambda(((List.map (map_ids_id f)) _x1), (map_ids_predicate f _x2), (map_ids_expr f _x3))

  and map_ids_exprSeq f  = function
    SeqEnum(_x1) -> SeqEnum(((List.map (map_ids_expr f)) _x1))
  | SeqEmpty -> SeqEmpty 
  and map_ids_exprSet f  = function
    SetRange(_x1, _x2) -> SetRange((map_ids_expr f _x1), (map_ids_expr f _x2))
  | SetEnum(_x1) -> SetEnum(((List.map (map_ids_expr f)) _x1))
  | SetCompr(_x1) -> SetCompr(((List.map (map_ids_expr f)) _x1))
  | SetComprPred(_x1, _x2) -> SetComprPred(((List.map (map_ids_id f)) _x1), (map_ids_predicate f _x2))
  | SetEmpty -> SetEmpty 
  | SetUnionQuantify(_x1, _x2, _x3) -> SetUnionQuantify(((List.map (map_ids_id f)) _x1), (map_ids_predicate f _x2), (map_ids_expr f _x3))
  | SetInterQuantify(_x1, _x2, _x3) -> SetInterQuantify(((List.map (map_ids_id f)) _x1), (map_ids_predicate f _x2), (map_ids_expr f _x3))
  and map_ids_number f  = function
    MinNumber -> MinNumber 
  | Number(_x1) -> Number((map_ids_int f _x1))
  | MaxNumber -> MaxNumber 
  and map_ids_substitution f  = function
    SubstOperCall(_x1, _x2, _x3) -> SubstOperCall((map_ids_ids f _x1), (map_ids_id f _x2), ((List.map (map_ids_expr f)) _x3))
  | SubstBlock(_x1) -> SubstBlock((map_ids_substitution f _x1))
  | SubstPrecondition(_x1, _x2) -> SubstPrecondition((map_ids_predicate f _x1), (map_ids_substitution f _x2))
  | SubstAssertion(_x1, _x2) -> SubstAssertion((map_ids_predicate f _x1), (map_ids_substitution f _x2))
  | SubstChoice(_x1) -> SubstChoice(((List.map (map_ids_substitution f)) _x1))
  | SubstIf(_x1, _x2) -> SubstIf(((List.map (fun (_x1, _x2) -> ((map_ids_predicate f _x1), (map_ids_substitution f _x2)))) _x1), ((map_ids_option (map_ids_substitution f)) _x2))
  | SubstSelect(_x1, _x2) -> SubstSelect(((List.map (fun (_x1, _x2) -> ((map_ids_predicate f _x1), (map_ids_substitution f _x2)))) _x1), ((map_ids_option (map_ids_substitution f)) _x2))
  | SubstCase(_x1, _x2, _x3) -> SubstCase((map_ids_expr f _x1), ((List.map (fun (_x1, _x2) -> (((List.map (map_ids_expr f)) _x1), (map_ids_substitution f _x2)))) _x2), ((map_ids_option (map_ids_substitution f)) _x3))
  | SubstAny(_x1, _x2, _x3) -> SubstAny(((List.map (map_ids_id f)) _x1), (map_ids_predicate f _x2), (map_ids_substitution f _x3))
  | SubstLet(_x1, _x2, _x3) -> SubstLet(((List.map (map_ids_id f)) _x1), (map_ids_predicate f _x2), (map_ids_substitution f _x3))
  | SubstVar(_x1, _x2) -> SubstVar(((List.map (map_ids_id f)) _x1), (map_ids_substitution f _x2))
  | SubstWhile(_x1, _x2, _x3, _x4) -> SubstWhile((map_ids_predicate f _x1), (map_ids_substitution f _x2), (map_ids_expr f _x3), (map_ids_predicate f _x4))
  | SubstSkip -> SubstSkip 
  | SubstSequence(_x1, _x2) -> SubstSequence((map_ids_substitution f _x1), (map_ids_substitution f _x2))
  | SubstParallel(_x1, _x2) -> SubstParallel((map_ids_substitution f _x1), (map_ids_substitution f _x2))
  | SubstSetEqualIds(_x1, _x2) -> SubstSetEqualIds((map_ids_ids f _x1), ((List.map (map_ids_expr f)) _x2))
  | SubstSetEqualFun(_x1, _x2, _x3) -> SubstSetEqualFun((map_ids_expr f _x1), ((List.map (map_ids_expr f)) _x2), (map_ids_expr f _x3))

  | SubstBecomeSuch(_x1, _x2) -> SubstBecomeSuch((map_ids_ids f _x1), (map_ids_predicate f _x2))
  | SubstSetIn(_x1, _x2) -> SubstSetIn((map_ids_ids f _x1), (map_ids_expr f _x2))
  and map_ids_ids f  = fun _x -> f((fun (_x1) -> ((List.map (map_ids_id f)) _x1))_x)
  and map_ids_origine f  = function
    Local -> Local 
  | Remote(_x1) -> Remote(((List.map (fun (_x1, _x2) -> ((map_ids_string f _x1), (map_ids_string f _x2)))) _x1))
  | OrgRefine(_x1, _x2) -> OrgRefine((map_ids_string f _x1), ((List.map (fun (_x1, _x2) -> ((map_ids_string f _x1), (map_ids_string f _x2)))) _x2))
  | Prefix(_x1) -> Prefix(((List.map (map_ids_string f)) _x1))
  and map_ids_id f  = function
    Id(_x1, _x2, _x3, _x4) -> Id((map_ids_origine f _x1), (map_ids_string f _x2), ((map_ids_option (map_ids_type_B f)) _x3), _x4)

let map_origine_string f (s:string) = s
let map_origine_int f (i:int32) = i
let map_origine_bool f (b:bool) = b
let map_origine_option f = function Some x -> Some (f x) | None -> None
let rec map_origine_amn f  = function
    Machine(_x1, _x2) -> Machine((map_origine_head f _x1), ((List.map (map_origine_clause f)) _x2))
  | Refinement(_x1, _x2) -> Refinement((map_origine_head f _x1), ((List.map (map_origine_clause f)) _x2))
  | Implementation(_x1, _x2) -> Implementation((map_origine_head f _x1), ((List.map (map_origine_clause f)) _x2))
  | EmptyTree -> EmptyTree 
  and map_origine_head f  = fun (_x1, _x2) -> (map_origine_id f _x1), (map_origine_ids f _x2)
  and map_origine_clause f  = function
    Definitions(_x1) -> Definitions(((List.map (map_origine_def f)) _x1))
  | Constraints(_x1) -> Constraints((map_origine_predicate f _x1))
  | Invariant(_x1) -> Invariant((map_origine_predicate f _x1))
  | Sets(_x1) -> Sets(((List.map (map_origine_set f)) _x1))
  | Initialisation(_x1) -> Initialisation((map_origine_substitution f _x1))
  | ConstantsConcrete(_x1) -> ConstantsConcrete((map_origine_ids f _x1))
  | ConstantsAbstract(_x1) -> ConstantsAbstract((map_origine_ids f _x1))
  | ConstantsHidden(_x1) -> ConstantsHidden((map_origine_ids f _x1))
  | Properties(_x1) -> Properties((map_origine_predicate f _x1))
  | Values(_x1) -> Values(((List.map (fun (_x1, _x2) -> ((map_origine_id f _x1), (map_origine_expr f _x2)))) _x1))
  | VariablesConcrete(_x1) -> VariablesConcrete((map_origine_ids f _x1))
  | VariablesAbstract(_x1) -> VariablesAbstract((map_origine_ids f _x1))
  | VariablesHidden(_x1) -> VariablesHidden((map_origine_ids f _x1))
  | Promotes(_x1) -> Promotes((map_origine_ids f _x1))
  | Assertions(_x1) -> Assertions(((List.map (map_origine_predicate f)) _x1))
  | Operations(_x1) -> Operations(((List.map (map_origine_operation f)) _x1))
  | Sees(_x1) -> Sees((map_origine_ids f _x1))
  | Uses(_x1) -> Uses((map_origine_ids f _x1))
  | Extends(_x1) -> Extends(((List.map (map_origine_instance f)) _x1))
  | Includes(_x1) -> Includes(((List.map (map_origine_instance f)) _x1))
  | Imports(_x1) -> Imports(((List.map (map_origine_instance f)) _x1))
  | Refines(_x1) -> Refines((map_origine_id f _x1))
  and map_origine_def f  = function
    Def(_x1, _x2, _x3) -> Def((map_origine_id f _x1), (map_origine_ids f _x2), (map_origine_defBody f _x3))
  | FileDef(_x1) -> FileDef((map_origine_string f _x1))
  and map_origine_defBody f  = function
    ExprDefBody(_x1) -> ExprDefBody((map_origine_expr f _x1))
  | SubstDefBody(_x1) -> SubstDefBody((map_origine_substitution f _x1))
  and map_origine_set f  = function
    SetAbstDec(_x1) -> SetAbstDec((map_origine_id f _x1))
  | SetEnumDec(_x1, _x2) -> SetEnumDec((map_origine_id f _x1), (map_origine_ids f _x2))

  and map_origine_instance f  = fun (_x1, _x2) -> (map_origine_id f _x1), ((List.map (map_origine_expr f)) _x2)
  and map_origine_type_B f  = function
    TypeNatSetRange(_x1, _x2) -> TypeNatSetRange((map_origine_expr f _x1), (map_origine_expr f _x2))
  | PredType(_x1) -> PredType((map_origine_expr f _x1))

  | TypeIdentifier(_x1) -> TypeIdentifier((map_origine_id f _x1))
  | PFunType(_x1, _x2) -> PFunType((map_origine_type_B f _x1), (map_origine_type_B f _x2))
  | FunType(_x1, _x2) -> FunType((map_origine_type_B f _x1), (map_origine_type_B f _x2))
  | ProdType(_x1, _x2) -> ProdType((map_origine_type_B f _x1), (map_origine_type_B f _x2))
  | Sequence(_x1) -> Sequence((map_origine_type_B f _x1))
  | SubType(_x1, _x2) -> SubType((map_origine_type_B f _x1), (map_origine_expr f _x2))
  | Fin_B(_x1) -> Fin_B((map_origine_type_B f _x1))
  | Untyped -> Untyped 
  and map_origine_field f  = fun (_x1, _x2) -> (map_origine_id f _x1), (map_origine_type_B f _x2)
  and map_origine_predicate f  = function
    PredParen(_x1) -> PredParen((map_origine_predicate f _x1))
  | PredNegation(_x1) -> PredNegation((map_origine_predicate f _x1))
  | PredExists(_x1, _x2) -> PredExists(((List.map (map_origine_id f)) _x1), (map_origine_predicate f _x2))
  | PredForAll(_x1, _x2) -> PredForAll(((List.map (map_origine_id f)) _x1), (map_origine_predicate f _x2))
  | PredBin(_x1, _x2, _x3) -> PredBin(_x1, (map_origine_predicate f _x2), (map_origine_predicate f _x3))
  | PredAtom(_x1, _x2, _x3) -> PredAtom(_x1, (map_origine_expr f _x2), (map_origine_expr f _x3))
  and map_origine_operation f  = fun (_x1, _x2, _x3, _x4) -> (map_origine_id f _x1), (map_origine_ids f _x2), (map_origine_ids f _x3), (map_origine_substitution f _x4)
  and map_origine_expr f  = function
    ExprSequence(_x1, _x2) -> ExprSequence((map_origine_expr f _x1), (map_origine_expr f _x2))
  | ExprParen(_x1) -> ExprParen((map_origine_expr f _x1))
  | ExprId(_x1) -> ExprId((map_origine_id f _x1))
  | ExprFunCall(_x1, _x2) -> ExprFunCall((map_origine_expr f _x1), ((List.map (map_origine_expr f)) _x2))
  | ExprBin(_x1, _x2, _x3) -> ExprBin(_x1, (map_origine_expr f _x2), (map_origine_expr f _x3))
  | ExprUn(_x1, _x2) -> ExprUn(_x1, (map_origine_expr f _x2))
  | ExprSeq(_x1) -> ExprSeq((map_origine_exprSeq f _x1))
  | ExprSet(_x1) -> ExprSet((map_origine_exprSet f _x1))
  | RelSet(_x1, _x2) -> RelSet((map_origine_expr f _x1), (map_origine_expr f _x2))
  | RelSeqComp(_x1) -> RelSeqComp(((List.map (map_origine_expr f)) _x1))
  | ExprNumber(_x1) -> ExprNumber((map_origine_number f _x1))
  | ExprBool(_x1) -> ExprBool((map_origine_bool f _x1))
  | ExprString(_x1) -> ExprString((map_origine_string f _x1))

  | ExprPred(_x1) -> ExprPred((map_origine_predicate f _x1))
  | ExprNuplet(_x1) -> ExprNuplet(((List.map (map_origine_expr f)) _x1))
  | ExprSIGMA(_x1, _x2, _x3) -> ExprSIGMA(((List.map (map_origine_id f)) _x1), (map_origine_predicate f _x2), (map_origine_expr f _x3))
  | ExprPI(_x1, _x2, _x3) -> ExprPI(((List.map (map_origine_id f)) _x1), (map_origine_predicate f _x2), (map_origine_expr f _x3))
  | ExprLambda(_x1, _x2, _x3) -> ExprLambda(((List.map (map_origine_id f)) _x1), (map_origine_predicate f _x2), (map_origine_expr f _x3))

  and map_origine_exprSeq f  = function
    SeqEnum(_x1) -> SeqEnum(((List.map (map_origine_expr f)) _x1))
  | SeqEmpty -> SeqEmpty 
  and map_origine_exprSet f  = function
    SetRange(_x1, _x2) -> SetRange((map_origine_expr f _x1), (map_origine_expr f _x2))
  | SetEnum(_x1) -> SetEnum(((List.map (map_origine_expr f)) _x1))
  | SetCompr(_x1) -> SetCompr(((List.map (map_origine_expr f)) _x1))
  | SetComprPred(_x1, _x2) -> SetComprPred(((List.map (map_origine_id f)) _x1), (map_origine_predicate f _x2))
  | SetEmpty -> SetEmpty 
  | SetUnionQuantify(_x1, _x2, _x3) -> SetUnionQuantify(((List.map (map_origine_id f)) _x1), (map_origine_predicate f _x2), (map_origine_expr f _x3))
  | SetInterQuantify(_x1, _x2, _x3) -> SetInterQuantify(((List.map (map_origine_id f)) _x1), (map_origine_predicate f _x2), (map_origine_expr f _x3))
  and map_origine_number f  = function
    MinNumber -> MinNumber 
  | Number(_x1) -> Number((map_origine_int f _x1))
  | MaxNumber -> MaxNumber 
  and map_origine_substitution f  = function
    SubstOperCall(_x1, _x2, _x3) -> SubstOperCall((map_origine_ids f _x1), (map_origine_id f _x2), ((List.map (map_origine_expr f)) _x3))
  | SubstBlock(_x1) -> SubstBlock((map_origine_substitution f _x1))
  | SubstPrecondition(_x1, _x2) -> SubstPrecondition((map_origine_predicate f _x1), (map_origine_substitution f _x2))
  | SubstAssertion(_x1, _x2) -> SubstAssertion((map_origine_predicate f _x1), (map_origine_substitution f _x2))
  | SubstChoice(_x1) -> SubstChoice(((List.map (map_origine_substitution f)) _x1))
  | SubstIf(_x1, _x2) -> SubstIf(((List.map (fun (_x1, _x2) -> ((map_origine_predicate f _x1), (map_origine_substitution f _x2)))) _x1), ((map_origine_option (map_origine_substitution f)) _x2))
  | SubstSelect(_x1, _x2) -> SubstSelect(((List.map (fun (_x1, _x2) -> ((map_origine_predicate f _x1), (map_origine_substitution f _x2)))) _x1), ((map_origine_option (map_origine_substitution f)) _x2))
  | SubstCase(_x1, _x2, _x3) -> SubstCase((map_origine_expr f _x1), ((List.map (fun (_x1, _x2) -> (((List.map (map_origine_expr f)) _x1), (map_origine_substitution f _x2)))) _x2), ((map_origine_option (map_origine_substitution f)) _x3))
  | SubstAny(_x1, _x2, _x3) -> SubstAny(((List.map (map_origine_id f)) _x1), (map_origine_predicate f _x2), (map_origine_substitution f _x3))
  | SubstLet(_x1, _x2, _x3) -> SubstLet(((List.map (map_origine_id f)) _x1), (map_origine_predicate f _x2), (map_origine_substitution f _x3))
  | SubstVar(_x1, _x2) -> SubstVar(((List.map (map_origine_id f)) _x1), (map_origine_substitution f _x2))
  | SubstWhile(_x1, _x2, _x3, _x4) -> SubstWhile((map_origine_predicate f _x1), (map_origine_substitution f _x2), (map_origine_expr f _x3), (map_origine_predicate f _x4))
  | SubstSkip -> SubstSkip 
  | SubstSequence(_x1, _x2) -> SubstSequence((map_origine_substitution f _x1), (map_origine_substitution f _x2))
  | SubstParallel(_x1, _x2) -> SubstParallel((map_origine_substitution f _x1), (map_origine_substitution f _x2))
  | SubstSetEqualIds(_x1, _x2) -> SubstSetEqualIds((map_origine_ids f _x1), ((List.map (map_origine_expr f)) _x2))
  | SubstSetEqualFun(_x1, _x2, _x3) -> SubstSetEqualFun((map_origine_expr f _x1), ((List.map (map_origine_expr f)) _x2), (map_origine_expr f _x3))

  | SubstBecomeSuch(_x1, _x2) -> SubstBecomeSuch((map_origine_ids f _x1), (map_origine_predicate f _x2))
  | SubstSetIn(_x1, _x2) -> SubstSetIn((map_origine_ids f _x1), (map_origine_expr f _x2))
  and map_origine_ids f  = fun (_x1) -> ((List.map (map_origine_id f)) _x1)
  and map_origine_origine f  = fun _x -> f((function
    Local -> Local 
  | Remote(_x1) -> Remote(((List.map (fun (_x1, _x2) -> ((map_origine_string f _x1), (map_origine_string f _x2)))) _x1))
  | OrgRefine(_x1, _x2) -> OrgRefine((map_origine_string f _x1), ((List.map (fun (_x1, _x2) -> ((map_origine_string f _x1), (map_origine_string f _x2)))) _x2))
  | Prefix(_x1) -> Prefix(((List.map (map_origine_string f)) _x1)))_x)
  and map_origine_id f  = function
    Id(_x1, _x2, _x3, _x4) -> Id((map_origine_origine f _x1), (map_origine_string f _x2), ((map_origine_option (map_origine_type_B f)) _x3), _x4)

let map_id_string f (s:string) = s
let map_id_int f (i:int32) = i
let map_id_bool f (b:bool) = b
let map_id_option f = function Some x -> Some (f x) | None -> None
let rec map_id_amn f  = function
    Machine(_x1, _x2) -> Machine((map_id_head f _x1), ((List.map (map_id_clause f)) _x2))
  | Refinement(_x1, _x2) -> Refinement((map_id_head f _x1), ((List.map (map_id_clause f)) _x2))
  | Implementation(_x1, _x2) -> Implementation((map_id_head f _x1), ((List.map (map_id_clause f)) _x2))
  | EmptyTree -> EmptyTree 
  and map_id_head f  = fun (_x1, _x2) -> (map_id_id f _x1), (map_id_ids f _x2)
  and map_id_clause f  = function
    Definitions(_x1) -> Definitions(((List.map (map_id_def f)) _x1))
  | Constraints(_x1) -> Constraints((map_id_predicate f _x1))
  | Invariant(_x1) -> Invariant((map_id_predicate f _x1))
  | Sets(_x1) -> Sets(((List.map (map_id_set f)) _x1))
  | Initialisation(_x1) -> Initialisation((map_id_substitution f _x1))
  | ConstantsConcrete(_x1) -> ConstantsConcrete((map_id_ids f _x1))
  | ConstantsAbstract(_x1) -> ConstantsAbstract((map_id_ids f _x1))
  | ConstantsHidden(_x1) -> ConstantsHidden((map_id_ids f _x1))
  | Properties(_x1) -> Properties((map_id_predicate f _x1))
  | Values(_x1) -> Values(((List.map (fun (_x1, _x2) -> ((map_id_id f _x1), (map_id_expr f _x2)))) _x1))
  | VariablesConcrete(_x1) -> VariablesConcrete((map_id_ids f _x1))
  | VariablesAbstract(_x1) -> VariablesAbstract((map_id_ids f _x1))
  | VariablesHidden(_x1) -> VariablesHidden((map_id_ids f _x1))
  | Promotes(_x1) -> Promotes((map_id_ids f _x1))
  | Assertions(_x1) -> Assertions(((List.map (map_id_predicate f)) _x1))
  | Operations(_x1) -> Operations(((List.map (map_id_operation f)) _x1))
  | Sees(_x1) -> Sees((map_id_ids f _x1))
  | Uses(_x1) -> Uses((map_id_ids f _x1))
  | Extends(_x1) -> Extends(((List.map (map_id_instance f)) _x1))
  | Includes(_x1) -> Includes(((List.map (map_id_instance f)) _x1))
  | Imports(_x1) -> Imports(((List.map (map_id_instance f)) _x1))
  | Refines(_x1) -> Refines((map_id_id f _x1))
  and map_id_def f  = function
    Def(_x1, _x2, _x3) -> Def((map_id_id f _x1), (map_id_ids f _x2), (map_id_defBody f _x3))
  | FileDef(_x1) -> FileDef((map_id_string f _x1))
  and map_id_defBody f  = function
    ExprDefBody(_x1) -> ExprDefBody((map_id_expr f _x1))
  | SubstDefBody(_x1) -> SubstDefBody((map_id_substitution f _x1))
  and map_id_set f  = function
    SetAbstDec(_x1) -> SetAbstDec((map_id_id f _x1))
  | SetEnumDec(_x1, _x2) -> SetEnumDec((map_id_id f _x1), (map_id_ids f _x2))

  and map_id_instance f  = fun (_x1, _x2) -> (map_id_id f _x1), ((List.map (map_id_expr f)) _x2)
  and map_id_type_B f  = function
    TypeNatSetRange(_x1, _x2) -> TypeNatSetRange((map_id_expr f _x1), (map_id_expr f _x2))
  | PredType(_x1) -> PredType((map_id_expr f _x1))

  | TypeIdentifier(_x1) -> TypeIdentifier((map_id_id f _x1))
  | PFunType(_x1, _x2) -> PFunType((map_id_type_B f _x1), (map_id_type_B f _x2))
  | FunType(_x1, _x2) -> FunType((map_id_type_B f _x1), (map_id_type_B f _x2))
  | ProdType(_x1, _x2) -> ProdType((map_id_type_B f _x1), (map_id_type_B f _x2))
  | Sequence(_x1) -> Sequence((map_id_type_B f _x1))
  | SubType(_x1, _x2) -> SubType((map_id_type_B f _x1), (map_id_expr f _x2))
  | Fin_B(_x1) -> Fin_B((map_id_type_B f _x1))
  | Untyped -> Untyped 
  and map_id_field f  = fun (_x1, _x2) -> (map_id_id f _x1), (map_id_type_B f _x2)
  and map_id_predicate f  = function
    PredParen(_x1) -> PredParen((map_id_predicate f _x1))
  | PredNegation(_x1) -> PredNegation((map_id_predicate f _x1))
  | PredExists(_x1, _x2) -> PredExists(((List.map (map_id_id f)) _x1), (map_id_predicate f _x2))
  | PredForAll(_x1, _x2) -> PredForAll(((List.map (map_id_id f)) _x1), (map_id_predicate f _x2))
  | PredBin(_x1, _x2, _x3) -> PredBin(_x1, (map_id_predicate f _x2), (map_id_predicate f _x3))
  | PredAtom(_x1, _x2, _x3) -> PredAtom(_x1, (map_id_expr f _x2), (map_id_expr f _x3))
  and map_id_operation f  = fun (_x1, _x2, _x3, _x4) -> (map_id_id f _x1), (map_id_ids f _x2), (map_id_ids f _x3), (map_id_substitution f _x4)
  and map_id_expr f  = function
    ExprSequence(_x1, _x2) -> ExprSequence((map_id_expr f _x1), (map_id_expr f _x2))
  | ExprParen(_x1) -> ExprParen((map_id_expr f _x1))
  | ExprId(_x1) -> ExprId((map_id_id f _x1))
  | ExprFunCall(_x1, _x2) -> ExprFunCall((map_id_expr f _x1), ((List.map (map_id_expr f)) _x2))
  | ExprBin(_x1, _x2, _x3) -> ExprBin(_x1, (map_id_expr f _x2), (map_id_expr f _x3))
  | ExprUn(_x1, _x2) -> ExprUn(_x1, (map_id_expr f _x2))
  | ExprSeq(_x1) -> ExprSeq((map_id_exprSeq f _x1))
  | ExprSet(_x1) -> ExprSet((map_id_exprSet f _x1))
  | RelSet(_x1, _x2) -> RelSet((map_id_expr f _x1), (map_id_expr f _x2))
  | RelSeqComp(_x1) -> RelSeqComp(((List.map (map_id_expr f)) _x1))
  | ExprNumber(_x1) -> ExprNumber((map_id_number f _x1))
  | ExprBool(_x1) -> ExprBool((map_id_bool f _x1))
  | ExprString(_x1) -> ExprString((map_id_string f _x1))

  | ExprPred(_x1) -> ExprPred((map_id_predicate f _x1))
  | ExprNuplet(_x1) -> ExprNuplet(((List.map (map_id_expr f)) _x1))
  | ExprSIGMA(_x1, _x2, _x3) -> ExprSIGMA(((List.map (map_id_id f)) _x1), (map_id_predicate f _x2), (map_id_expr f _x3))
  | ExprPI(_x1, _x2, _x3) -> ExprPI(((List.map (map_id_id f)) _x1), (map_id_predicate f _x2), (map_id_expr f _x3))
  | ExprLambda(_x1, _x2, _x3) -> ExprLambda(((List.map (map_id_id f)) _x1), (map_id_predicate f _x2), (map_id_expr f _x3))
  and map_id_exprSeq f  = function
    SeqEnum(_x1) -> SeqEnum(((List.map (map_id_expr f)) _x1))
  | SeqEmpty -> SeqEmpty 
  and map_id_exprSet f  = function
    SetRange(_x1, _x2) -> SetRange((map_id_expr f _x1), (map_id_expr f _x2))
  | SetEnum(_x1) -> SetEnum(((List.map (map_id_expr f)) _x1))
  | SetCompr(_x1) -> SetCompr(((List.map (map_id_expr f)) _x1))
  | SetComprPred(_x1, _x2) -> SetComprPred(((List.map (map_id_id f)) _x1), (map_id_predicate f _x2))
  | SetEmpty -> SetEmpty 
  | SetUnionQuantify(_x1, _x2, _x3) -> SetUnionQuantify(((List.map (map_id_id f)) _x1), (map_id_predicate f _x2), (map_id_expr f _x3))
  | SetInterQuantify(_x1, _x2, _x3) -> SetInterQuantify(((List.map (map_id_id f)) _x1), (map_id_predicate f _x2), (map_id_expr f _x3))
  and map_id_number f  = function
    MinNumber -> MinNumber 
  | Number(_x1) -> Number((map_id_int f _x1))
  | MaxNumber -> MaxNumber 
  and map_id_substitution f  = function
    SubstOperCall(_x1, _x2, _x3) -> SubstOperCall((map_id_ids f _x1), (map_id_id f _x2), ((List.map (map_id_expr f)) _x3))
  | SubstBlock(_x1) -> SubstBlock((map_id_substitution f _x1))
  | SubstPrecondition(_x1, _x2) -> SubstPrecondition((map_id_predicate f _x1), (map_id_substitution f _x2))
  | SubstAssertion(_x1, _x2) -> SubstAssertion((map_id_predicate f _x1), (map_id_substitution f _x2))
  | SubstChoice(_x1) -> SubstChoice(((List.map (map_id_substitution f)) _x1))
  | SubstIf(_x1, _x2) -> SubstIf(((List.map (fun (_x1, _x2) -> ((map_id_predicate f _x1), (map_id_substitution f _x2)))) _x1), ((map_id_option (map_id_substitution f)) _x2))
  | SubstSelect(_x1, _x2) -> SubstSelect(((List.map (fun (_x1, _x2) -> ((map_id_predicate f _x1), (map_id_substitution f _x2)))) _x1), ((map_id_option (map_id_substitution f)) _x2))
  | SubstCase(_x1, _x2, _x3) -> SubstCase((map_id_expr f _x1), ((List.map (fun (_x1, _x2) -> (((List.map (map_id_expr f)) _x1), (map_id_substitution f _x2)))) _x2), ((map_id_option (map_id_substitution f)) _x3))
  | SubstAny(_x1, _x2, _x3) -> SubstAny(((List.map (map_id_id f)) _x1), (map_id_predicate f _x2), (map_id_substitution f _x3))
  | SubstLet(_x1, _x2, _x3) -> SubstLet(((List.map (map_id_id f)) _x1), (map_id_predicate f _x2), (map_id_substitution f _x3))
  | SubstVar(_x1, _x2) -> SubstVar(((List.map (map_id_id f)) _x1), (map_id_substitution f _x2))
  | SubstWhile(_x1, _x2, _x3, _x4) -> SubstWhile((map_id_predicate f _x1), (map_id_substitution f _x2), (map_id_expr f _x3), (map_id_predicate f _x4))
  | SubstSkip -> SubstSkip 
  | SubstSequence(_x1, _x2) -> SubstSequence((map_id_substitution f _x1), (map_id_substitution f _x2))
  | SubstParallel(_x1, _x2) -> SubstParallel((map_id_substitution f _x1), (map_id_substitution f _x2))
  | SubstSetEqualIds(_x1, _x2) -> SubstSetEqualIds((map_id_ids f _x1), ((List.map (map_id_expr f)) _x2))
  | SubstSetEqualFun(_x1, _x2, _x3) -> SubstSetEqualFun((map_id_expr f _x1), ((List.map (map_id_expr f)) _x2), (map_id_expr f _x3))

  | SubstBecomeSuch(_x1, _x2) -> SubstBecomeSuch((map_id_ids f _x1), (map_id_predicate f _x2))
  | SubstSetIn(_x1, _x2) -> SubstSetIn((map_id_ids f _x1), (map_id_expr f _x2))
  and map_id_ids f  = fun (_x1) -> ((List.map (map_id_id f)) _x1)
  and map_id_origine f  = function
    Local -> Local 
  | Remote(_x1) -> Remote(((List.map (fun (_x1, _x2) -> ((map_id_string f _x1), (map_id_string f _x2)))) _x1))
  | OrgRefine(_x1, _x2) -> OrgRefine((map_id_string f _x1), ((List.map (fun (_x1, _x2) -> ((map_id_string f _x1), (map_id_string f _x2)))) _x2))
  | Prefix(_x1) -> Prefix(((List.map (map_id_string f)) _x1))
  and map_id_id f  = fun _x -> f((function
    Id(_x1, _x2, _x3, _x4) -> Id((map_id_origine f _x1), (map_id_string f _x2), ((map_id_option (map_id_type_B f)) _x3), _x4))_x)
