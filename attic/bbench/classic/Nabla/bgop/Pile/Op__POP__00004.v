 

Require Import Blib.

Theorem op:
forall PILE pile pop push pvide top, ( 
 (In (Power_set1 BZ) PILE) -> 
(In PILE pvide) -> 
(In (partial_function PILE BN) top) -> 
(In (partial_function PILE PILE) pop) -> 
(In (total_function (BN * PILE) PILE ) push) -> 
forall elt pp, (((((In BN elt) /\ (In PILE pp)) -> ((app top ((app push (elt,pp) _)) _) = elt)))) -> 
forall elt pp, (((((In BN elt) /\ (In PILE pp)) -> ((app pop ((app push (elt,pp) _)) _) = pp)))) -> 
(In PILE pile) -> 
(pile <> pvide)
  -> (
 (In PILE (app pop (pile) _)) ) )
 .
intuition.
Qed.
 