

type component_state = 
    Modified
  | Parsed
  | TypeChecked
  | POGenerated
  | AutoProved

type proof_categories = {
  obvious : int;
  normal : int;
  interactive : int;
  automatic : int;
  unproved : int;
  percent : int
}

type potype_statistics = string * proof_categories

type component_status = {
  name : string;
  state : component_state;
  path : string;
  proof_status : (potype_statistics list) option
}

let string_of_component_state cs =
  match cs with 
      Modified -> "Modified"
    | Parsed -> "Parsed"
    | TypeChecked -> "TypeChecked"
    | POGenerated -> "POGenerated"
    | AutoProved -> "AutoProved"

let string_of_proof_categories pc =
  (string_of_int pc.obvious) ^ "\t"
  ^ (string_of_int pc.normal) ^ "\t"
  ^ (string_of_int pc.interactive) ^ "\t"
  ^ (string_of_int pc.automatic) ^ "\t"
  ^ (string_of_int pc.unproved) ^ "\t"
  ^ (string_of_int pc.percent)

let string_of_potype_statistics (name, pc) =
  name ^ ": " ^ (string_of_proof_categories pc)

let string_of_component_status cs =
  cs.name ^ " " 
  ^ (string_of_component_state cs.state) ^ " "
  ^ cs.path ^ "\n"
  ^ (
    match cs.proof_status with
      | None -> ""
      | Some(ps_list) -> 
	  String.concat "\n" (List.map string_of_potype_statistics ps_list)
  )



type component_statistics = {
  typechecked  : bool;
  pogenerated : bool;
  po_obvious : int option;
  po_normal : int option;
  po_unproved : int option;
  po_percent : int option;
  b0_checked : bool; 
}

type project_status = 
  { components_status : (string * component_statistics) list;
    total_status : component_statistics;
  }


let string_of_pooption p =
  match p with
    | None -> "X"
    | Some(i) -> string_of_int i

let string_of_component_statistics cs =
  (string_of_bool cs.typechecked) ^ "\t"
  ^ (string_of_bool cs.pogenerated) ^ "\t"
  ^ (string_of_pooption cs.po_obvious) ^ "\t"
  ^ (string_of_pooption cs.po_normal) ^ "\t"
  ^ (string_of_pooption cs.po_unproved) ^ "\t"
  ^ (string_of_pooption cs.po_percent) ^ "\t"
  ^ (string_of_bool cs.b0_checked)

let string_of_project_status ps =
  let lines = List.map (fun (n,cs) -> n ^ ": " ^ (string_of_component_statistics cs)) ps.components_status in
  let last_line = "Total: " ^ (string_of_component_statistics ps.total_status) 
  in String.concat "\n" (lines @ [last_line])
