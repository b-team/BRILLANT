 

Require Import Blib.

Theorem op:
forall _nn _yy, ( 
 (In (Power_finite BN1) _yy) -> 
(In BN1 _nn)
  -> (
 (In (Power_finite BN1) (Union _yy {= _nn =})) ) )
 .
try intuition 
   || unfold BBOOL ; intuition ; apply Full_intro
   || red ;  red ; red ; omega.
Qed.
 