(*==================================================================================================
  Project : BiCoq3
  Module : proof_env.v
  This module defines specify B proofs environments as lists of predicates; the previous approach
  of pure specification (without implementation) was considered inappropriate.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Jul 2008, Md: Mar 2009
  ================================================================================================*)

Require Export term.
Require Import List.

(* Proof environment -----------------------------------------------------------------------------*)

Notation "'Γ'" := (list Π).

Notation "'∅ˠ'" := (nil (A:=Π)).

Notation "G '·' p" := (cons (A:=Π) p G) (at level 86, left associativity).

(* Membership, removal, inclusion ----------------------------------------------------------------*)

Fixpoint Bg_mem(p:Π)(G:Γ){struct G}:Β :=
 match G with ∅ˠ => ⊥ | G'·p' => Bp_eq p p' || Bg_mem p G' end.
Notation "p '∈ˠ' G" := (Bg_mem p G=⊤) (at level 30, no associativity).

Fixpoint Bg_rem(p:Π)(G:Γ){struct G}:Γ :=
 match G with ∅ˠ => ∅ˠ | G'·p' => if Bp_eq p p' then Bg_rem p G' else (Bg_rem p G')·p' end.
Notation "G '-ˠ' p" := (Bg_rem p G) (at level 28, left associativity).

Fixpoint Bg_inc(G1 G2:Γ){struct G1}:Β :=
 match G1 with ∅ˠ => ⊤ | G'·p' => Bg_mem p' G2 && Bg_inc G' G2 end.
Notation "G1 '⊆ˠ' G2" := (Bg_inc G1 G2=⊤) (at level 30, no associativity).

Definition Bg_eq(G1 G2:Γ):Β := Bg_inc G1 G2 && Bg_inc G2 G1.
Notation "G1 '=ˠ' G2" := (Bg_eq G1 G2=⊤) (at level 30, no associativity).

(* Theorems --------------------------------------------------------------------------------------*)

Theorem Bg_mem_eq:forall (G:Γ)(p:Π), p∈ˠ(G·p).
Proof.
 intros G p; unfold Bg_mem; rewrite (imppt2 Bp_eq_imp _ _ (refl_equal p)); apply refl_equal.
Qed.

Theorem Bg_mem_next:forall (G:Γ)(p1 p2:Π), p1∈ˠG->p1∈ˠ(G·p2).
Proof.
 intros G p1 p2 Hp1; simpl; rewrite Hp1; bool_simpl; apply refl_equal.
Qed.

Theorem Bg_mem_swap:forall (G:Γ)(p1 p2 p3:Π), p1∈ˠ(G·p3)->p1∈ˠ(G·p2·p3).
Proof.
 intros G p1 p2 p3 Hp1; simpl in *; destruct (Bp_eq p1 p3); simpl in *; [ | rewrite Hp1];
  bool_simpl; apply refl_equal.
Qed.

Theorem Bg_mem_rem:forall (G:Γ)(p1 p2:Π), Bg_mem p1 (G-ˠp2)=Bg_mem p1 G && negb (Bp_eq p1 p2).
Proof.
 induction G as [| p G]; intros p1 p2; simpl; [apply refl_equal | ].
  case_eq (Bp_eq p2 p); intros Hp2p; simpl; rewrite IHG.
   destruct (Bg_mem p1 G); bool_simpl; [apply refl_equal | ].
    case_eq (Bp_eq p1 p); intros Hp1p; simpl; [ | apply refl_equal].
     reflect_in Bp_eq_imp Hp2p; rewrite Hp2p; reflect_in Bp_eq_imp Hp1p; rewrite Hp1p;
      rewrite (imppt2 Bp_eq_imp _ _ (refl_equal p)); apply refl_equal.
   destruct (Bg_mem p1 G); case_eq (Bp_eq p1 p2); intros Hp1p2; simpl; bool_simpl;
    try (apply refl_equal); reflect_in Bp_eq_imp Hp1p2; rewrite Hp1p2 in *; apply Hp2p.
Qed.

Theorem Bg_inc_imp:((fun (G1 G2:Γ)=>forall (p:Π), p∈ˠG1->p∈ˠG2)⇝²Bg_inc).
Proof.
 unfold implements2; split; induction t1 as [| p' G1 HG1]; intros G2 Hinc; simpl in *.
  intros p Hmem; inversion Hmem.
  intros p Hmem; destruct (orb_true_elim _ _ Hmem) as [Hpp' | HpG1].
   reflect_in Bp_eq_imp Hpp'; rewrite Hpp' in *; apply (proj1 (andb_prop _ _ Hinc)).
   apply HG1; [apply (proj2 (andb_prop _ _ Hinc)) | apply HpG1].
  inversion Hinc.
  destruct (andb_false_elim _ _ Hinc) as [Hp'G2 | HG1G2].
   intros Hp; generalize (Hp p'); rewrite (imppt2 Bp_eq_imp _ _ (refl_equal p')); bool_simpl;
    intros Hp'; rewrite (Hp' (refl_equal ⊤)) in Hp'G2; inversion Hp'G2.
   intros Hp; apply (HG1 _ HG1G2); intros p HpG1; apply Hp; rewrite HpG1; bool_simpl;
    apply refl_equal.
Qed.

Theorem Bg_inc_refl:forall (G:Γ), G⊆ˠG.
Proof.
 intros G; reflect Bg_inc_imp; intros p Hp; apply Hp.
Qed.

Theorem Bg_inc_trans:forall (G1 G2 G3:Γ), G1⊆ˠG2->G2⊆ˠG3->G1⊆ˠG3.
Proof.
 intros G1 G2 G3 H12 H23; reflect Bg_inc_imp; intros p HG1; reflect_in Bg_inc_imp H23; apply H23;
  reflect_in Bg_inc_imp H12; apply H12; apply HG1.
Qed.

Theorem Bg_eq_imp:(fun (G1 G2:Γ)=>forall (p:Π), Bg_mem p G1=Bg_mem p G2⇝²Bg_eq).
Proof.
 unfold implements2; unfold Bg_eq; split; intros G1 G2 HG1G2.
  destruct (andb_prop _ _ HG1G2) as [H12 H21]; intros p; case_eq (Bg_mem p G1); intros HpG1.
   rewrite (fst Bg_inc_imp _ _ H12 _ HpG1); apply refl_equal.
   case_eq (Bg_mem p G2); intros HpG2;
   [rewrite (fst Bg_inc_imp _ _ H21 _ HpG2) in HpG1; inversion HpG1 | apply refl_equal].
  destruct (andb_false_elim _ _ HG1G2) as [H12 | H21]; intros Hmem.
   assert (H'mem:forall (p:Π), p∈ˠG1->p∈ˠG2).
    intros p; rewrite (Hmem p); intros HpG2; apply HpG2.
   apply (snd Bg_inc_imp _ _ H12 H'mem).
   assert (H'mem:forall (p:Π), p∈ˠG2->p∈ˠG1).
    intros p; rewrite (Hmem p); intros HpG1; apply HpG1.
   apply (snd Bg_inc_imp _ _ H21 H'mem).
Qed.

Theorem Bg_eq_refl:forall (G:Γ), G=ˠG.
Proof.
 intros G; reflect Bg_eq_imp; intros p; apply refl_equal.
Qed.

Theorem Bg_eq_sym:forall (G1 G2:Γ), Bg_eq G1 G2=Bg_eq G2 G1.
Proof.
 intros G1 G2; unfold Bg_eq; destruct (Bg_inc G1 G2); bool_simpl; apply refl_equal.
Qed.

Theorem Bg_eq_trans:forall (G1 G2 G3:Γ), G1=ˠG2->G2=ˠG3->G1=ˠG3.
Proof.
 intros G1 G2 G3 H12 H23; reflect Bg_eq_imp; intros p; reflect_in Bg_eq_imp H12; rewrite H12;
  reflect_in Bg_eq_imp H23; rewrite H23; apply refl_equal.
Qed.

Theorem Bg_inc_nulg:forall (G:Γ), Bg_inc G ∅ˠ=Bg_eq G ∅ˠ.
Proof.
 intros G; unfold Bg_eq; simpl; bool_simpl; apply refl_equal.
Qed.

Theorem Bg_nulg_inc:forall (G:Γ), ∅ˠ⊆ˠG.
Proof.
 intros G; simpl; apply refl_equal.
Qed.

Theorem Bg_inc_add:forall (G:Γ)(p:Π), G⊆ˠ(G·p).
Proof.
 intros G p; reflect Bg_inc_imp; intros p' Hp'; simpl; rewrite Hp'; bool_simpl; apply refl_equal.
Qed.

Theorem Bg_add_comm:forall (G:Γ)(p1 p2:Π), (G·p1·p2)=ˠ(G·p2·p1).
Proof.
 intros G p1 p2; unfold Bg_eq; apply andb_true_intro; split; reflect Bg_inc_imp; intros p Hp;
  simpl in *; destruct (Bp_eq p p1); destruct (Bp_eq p p2); simpl in *; apply Hp.
Qed.

Theorem Bg_mem_inc:forall (G1 G2:Γ)(p:Π), p∈ˠG1->G1⊆ˠG2->p∈ˠG2.
Proof.
 intros G1 G2 p Hmem Hinc; reflect_in Bg_inc_imp Hinc; apply (Hinc _ Hmem).
Qed.

Theorem Bg_inc_split:forall (G1 G2:Γ)(p:Π), p∈ˠG2->G1⊆ˠG2->(G1·p)⊆ˠG2.
Proof.
 intros G1 G2 p Hmem Hinc; simpl; rewrite Hmem; rewrite Hinc; apply refl_equal.
Qed.

Theorem Bg_inc_next:forall (G1 G2:Γ)(p:Π), G1⊆ˠG2->G1⊆ˠ(G2·p).
Proof.
 intros G1 G2 p Hinc; reflect Bg_inc_imp; reflect_in Bg_inc_imp Hinc; intros p' Hp'; simpl in *;
  rewrite (Hinc _ Hp'); bool_simpl; apply refl_equal.
Qed.

Theorem Bg_inc_swap:forall (G1 G2:Γ)(p1 p2:Π), G1⊆ˠ(G2·p2)->G1⊆ˠ(G2·p1·p2).
Proof.
 intros G1 G2 p1 p2 Hinc; reflect Bg_inc_imp; reflect_in Bg_inc_imp Hinc; intros p Hp; simpl in *;
  destruct (orb_true_elim _ _ (Hinc _ Hp)) as [H'p | H'p]; rewrite H'p; bool_simpl;
  apply refl_equal.
Qed.

(* Tactic ----------------------------------------------------------------------------------------*)

Ltac Btac_gamma :=
 match goal with
 | |-_∈ˠ_ => Btac_gmem
 | |-_⊆ˠ_ => Btac_ginc
 end
 with Btac_gmem := assumption ||
                   apply Bg_mem_eq ||
                   (apply Bg_mem_next; Btac_gmem) ||
                   (apply Bg_mem_swap; Btac_gmem) ||
                   (match goal with
                    | Hinc:_⊆ˠ_|-_ => apply Bg_mem_inc with (2:=Hinc); Btac_gmem
                    end)
 with Btac_ginc := assumption ||
                   apply Bg_nulg_inc ||
                   apply Bg_inc_refl ||
                   (apply Bg_inc_split; [Btac_gmem | Btac_ginc]) ||
                   (apply Bg_inc_next; Btac_ginc) ||
                   (apply Bg_inc_swap; Btac_ginc) ||
                   (match goal with
                    | Hinc:_⊆ˠ_|-_ => apply Bg_inc_trans with (1:=Hinc); Btac_ginc
                    end).

(* Pointwise extensions --------------------------------------------------------------------------*)

Fixpoint Bg_free(i:Ι)(G:Γ){struct G}:Β :=
 match G with ∅ˠ => ⊥ | G'·p' => Bp_free_ 0 i p' || Bg_free i G' end.
Notation "i '∖ˠ' G":=(Bg_free i G=false) (at level 96, no associativity).

Fixpoint Bg_fshun(G:Γ):nat := match G with ∅ˠ => 0 | G'·p' => max (Bp_fshun p') (Bg_fshun G') end.
Notation "'⋇ˠ(' G ')'":= (Bg_fshun G).

Definition Bg_fresh(G:Γ):Ι := ¡(Bg_fshun G,0).
Notation "'⋕ˠ(' G ')'":= (Bg_fresh G).

Fixpoint Bg_ground(G:Γ):Β := match G with ∅ˠ => ⊤ | G'·p' => Bp_ground_ 0 p' && Bg_ground G' end.
Notation "'∖∖ˠ(' G ')'":= (Bg_ground G=⊤).

(* Theorems --------------------------------------------------------------------------------------*)

Theorem Bg_free_mem:forall (G:Γ)(i:Ι), (i∖ˠG)->forall (p:Π), p∈ˠG->(i∖p).
Proof.
 induction G as [| p' G']; intros i HiG p HpG; simpl in *.
  inversion HpG.
  destruct (orb_false_elim _ _ HiG) as [Hip' HiG']; destruct (orb_true_elim _ _ HpG) as [Hp' | HG'];
  [reflect_in Bp_eq_imp Hp'; rewrite Hp' in *; apply Hip' | apply IHG'; [apply HiG' | apply HG']].
Qed.

Theorem Bg_mem_free:forall (G:Γ)(i:Ι), (forall (p:Π), p∈ˠG->(i∖p))->(i∖ˠG).
Proof.
 induction G as [| p' G']; intros i HG; simpl in *.
  apply refl_equal.
  generalize (HG p'); rewrite (imppt2 Bp_eq_imp _ _ (refl_equal p')); bool_simpl; intros Hip';
   rewrite (Hip' (refl_equal ⊤)); simpl; apply IHG'; intros p HpG'; apply HG; rewrite HpG';
   bool_simpl; apply refl_equal.
Qed.

Theorem Bg_free_inc:forall (G1 G2:Γ)(i:Ι), (i∖ˠG1)->G2⊆ˠG1->(i∖ˠG2).
Proof.
 intros G1 G2 i HG1 Hinc; reflect_in Bg_inc_imp Hinc; apply Bg_mem_free; intros p Hp;
  apply (Bg_free_mem _ _ HG1 p); apply Hinc; apply  Hp.
Qed.

Theorem Bg_fshun_free:forall (G:Γ)(u:nat), ⋇ˠ(G)<=u->forall (n:nat), ¡(u,n)∖ˠG.
Proof.
 induction G as [| p G]; intros u Hu n; simpl in *.
  apply refl_equal.
  srewrite (Bt_fshun_free p u (le_trans _ _ _ (le_max_l (Bp_fshun p) ⋇ˠ(G)) Hu) n 0);
   rewrite (IHG u (le_trans _ _ _ (le_max_r (Bp_fshun p) ⋇ˠ(G)) Hu) n); apply refl_equal.
Qed.

Theorem Bg_fresh_free:forall (G:Γ), ⋕ˠ(G)∖ˠG.
Proof.
 induction G as [| p G]; unfold Bg_fresh; simpl; [apply refl_equal | ].
  srewrite (Bt_fshun_free p _ (le_max_l (Bp_fshun p) ⋇ˠ(G)) 0 0);
   srewrite (Bg_fshun_free G _ (le_max_r (Bp_fshun p) ⋇ˠ(G)) 0); apply refl_equal.
Qed.

Theorem gfresh:forall (G:Γ), {i:Ι | i∖ˠG}.
Proof.
 intros G; exists ⋕ˠ(G); apply Bg_fresh_free.
Qed.

(*================================================================================================*)