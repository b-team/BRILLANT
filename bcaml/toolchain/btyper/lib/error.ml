(*
 * $Id$

    Copyright (c) 1999 Christian Lindig <lindig@ips.cs.tu-bs.de>. All
    rights reserved. See COPYING for details.
 *
 * Global error and warning treatment.
 *)


(* The universal error exception  *)

exception Error of string

(* [error] raises [Error]
 *)

exception Parse_error of int * string

let error msg	= raise (Error msg)
;;

(* [warning]issues a warning to stderr  *)



(* *)
let message msg = 
  prerr_endline (msg); flush stderr
(*
  prerr_endline ("message: " ^ msg)
  print_string msg ; flush stdout
*)
;;

let debug msg =   prerr_endline ("\n\t<d>"^msg^"</d>") ; flush stderr ;;

let warning msg = 
  prerr_endline ("--Bwarn " ^ msg) ;  flush stderr 
;;

 let parsing_success file =
   (* Reussite Parsing *)
   message ("--Bhappy "^"<"^file^">"^ " parsed");
;;
 
 let env_variable s =
   debug ("You should define the environment variable "^s) ; 
   raise (Sys_error "Variable undefined")
;;

 let parsing_failure file line col msg =
   let mess = ("file:"
	       ^file
	       ^"\n\t"	
	       ^"--Bwarn "
	       ^"["^msg^"] "
	       ^ " l:c " ^ (Printf.sprintf  "%4s"  (string_of_int line))
	       ^(Printf.sprintf  "%4s"  (string_of_int col)) 
	      ) in
     message mess 
(*
;
     Blast.EmptyTree
*)
;;
 
