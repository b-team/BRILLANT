(*==================================================================================================
  Project : BiCoq3
  Module : abstr.v
  Beta-abstraction, capturing a free variable in a term. The operation in fact prepares a term just
  before to insert a head binder.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Jun 2008, Md: Mar 2009
  ================================================================================================*)

Require Export lift.

(* Abstraction function --------------------------------------------------------------------------*)
(* The abstraction function prepares a term by lifting all dangling indexes, and replacing one of
   them by a 0 to be captured by a binder. *)

Fixpoint Bp_abstr_(d:nat)(i:Ι)(p:Π){struct p}:Π:=
 match p with
 | ¬p' => ¬(Bp_abstr_ d i p')
 | ∀Δ(p') => ∀Δ(Bp_abstr_ (S d) (DB_lift_ d i) p')
 | p1⋀p2 => (Bp_abstr_ d i p1)⋀(Bp_abstr_ d i p2)
 | (p1⇒p2) => (Bp_abstr_ d i p1)⇒(Bp_abstr_ d i p2)
 | ∐(_) => p
 | e1≡e2 => (Be_abstr_ d i e1)≡(Be_abstr_ d i e2)
 | e1∈e2 => (Be_abstr_ d i e1)∈(Be_abstr_ d i e2)
 end
with Be_abstr_(d:nat)(i:Ι)(e:Ε){struct e}:Ε :=
 match e with
 | Ω => e
 | ↓(e') => ↓(Be_abstr_ d i e')
 | ↑(e') => ↑(Be_abstr_ d i e')
 | e1↦e2 => (Be_abstr_ d i e1)↦(Be_abstr_ d i e2)
 | e1×e2 => (Be_abstr_ d i e1)×(Be_abstr_ d i e2)
 | ∞(n') => e
 | ∂(i') => ∂(if DB_le d i' && DB_eq i i' then ¡(0,d) else DB_lift_ d i')
 | ⅭΔ(e',p') => ⅭΔ(Be_abstr_ d i e',Bp_abstr_ (S d) (DB_lift_ d i) p')
 end.
(* Nota: the clause for ∂(i') was
   if DB_le d i' then (if DB_eq i i' then ¡(0,d) else DB_lift_ d i') else i'
   but has been simplified to the current equivalent version, as DB_le d i=⊥->DB_lift_ d i=i *)

Definition Bt_abstr_(d:nat)(i:Ι)(t:Τ):Τ :=
 match t with be_ e => Be_abstr_ d i e | bp_ p => Bp_abstr_ d i p end.

Definition Bp_forall(i:Ι)(p:Π):Π := ∀Δ(Bp_abstr_ 0 i p).
Notation "'∀(' v '∙' p ')'" := (Bp_forall v p).
Notation "'∃(' v '∙' p ')'" := (¬∀(v∙¬p)).

Definition Be_cmpset(i:Ι)(e:Ε)(p:Π):Ε := ⅭΔ(e,Bp_abstr_ 0 i p).
Notation "'{' v '∊' e '∣' p '}'" := (Be_cmpset v e p).

(* Theorems --------------------------------------------------------------------------------------*)

Theorem Bt_abstr_lift_:forall (t:Τ)(i:Ι)(d:nat), Bt_free_ d i t=⊥->Bt_abstr_ d i t=Bt_lift_ d t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι)(d:nat), Bt_free_ d i t=⊥->Bt_abstr_ d i t=Bt_lift_ d t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind i d Hi;
  try (apply refl_equal);
  try (irewrite (Hind _ (sdepth t) _ _ Hi); apply refl_equal);
  try (destruct (orb_false_elim _ _ Hi) as [Hi1 Hi2]; irewrite (Hind _ (sdepth_l t1 t2) _ _ Hi1);
       irewrite (Hind _ (sdepth_r t1 t2) _ _ Hi2); apply refl_equal).
  unfold Bt_free_,Be_free_ in Hi; unfold Bt_abstr_,Be_abstr_; destruct (DB_le d i'); rewrite Hi;
   apply refl_equal.
Qed.

Lemma Bt_abstr_free_:forall (t:Τ)(i:Ι)(d:nat), Bt_free_ (S d) (DB_lift_ d i) (Bt_abstr_ d i t)=⊥.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι)(d:nat),
                            Bt_free_ (S d) (DB_lift_ d i) (Bt_abstr_ d i t)=⊥));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind i d;
  try (apply refl_equal); try (apply (Hind _ (sdepth t) i d));
  try (srewrite (Hind _ (sdepth_l t1 t2) i d); apply (Hind _ (sdepth_r t1 t2) i d)).
  unfold Bt_abstr_,Be_abstr_; case_eq (DB_le d i'); intros Hdi'.
   case_eq (DB_eq i i'); intros Hii'.
    unfold Bt_free_,Be_free_; simpl; DB_simpl; bool_simpl; apply refl_equal.
    simpl; DB_simpl; rewrite Hii'; bool_simpl; apply refl_equal.
   simpl; DB_simpl; rewrite Hdi'; apply refl_equal.
  srewrite (Hind _ (sdepth_l t1 t2) i d); apply (Hind _ (sdepth_r t1 t2) (DB_lift_ d i) (S d)).
  apply (Hind _ (sdepth t) (DB_lift_ d i) (S d)).
Qed.

Theorem Bp_forall_free:forall (i:Ι)(p:Π), (i∖∀(i∙p)).
Proof.
 intros i p; simpl; apply (Bt_abstr_free_ p i 0).
Qed.

Theorem Be_cmpset_free:forall (i:Ι)(e:Ε)(p:Π), (i∖e)->(i∖{i∊e∣p}).
Proof.
 intros i e p Hie; simpl; srewrite Hie; apply (Bt_abstr_free_ p i 0).
Qed.

Theorem Bt_abstr_free_free_:forall (t:Τ)(d:nat)(i i':Ι), Bt_free_ d i t=⊥->
                            Bt_free_ (S d) (DB_lift_ d i) (Bt_abstr_ d i' t)=⊥.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (d:nat)(i i':Ι), Bt_free_ d i t=⊥->
                            Bt_free_ (S d) (DB_lift_ d i) (Bt_abstr_ d i' t)=⊥));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind d i i'' Hdit;
  try (apply refl_equal); try (apply (Hind _ (sdepth t) _ _ i'' Hdit));
  try (destruct (orb_false_elim _ _ Hdit) as [Hdit1 Hdit2];
       srewrite (Hind _ (sdepth_l t1 t2) _ _ i'' Hdit1);
       apply (Hind _ (sdepth_r t1 t2) _ _ i'' Hdit2)).
  unfold Bt_free_,Be_free_ in Hdit; unfold Bt_abstr_,Be_abstr_;
   case_eq (DB_le d i'); intros Hdi'.
   case_eq (DB_eq i'' i'); intros Hi''i'.
    destruct i as [[| u] n]; unfold DB_lift_; nat_simpl; bool_simpl.
     destruct (nat_le d n); unfold Bt_free_,Be_free_,DB_le; nat_simpl; apply refl_equal.
     simpl; nat_simpl; apply refl_equal.
    simpl; DB_simpl; apply Hdit.
   simpl; DB_simpl; apply Hdit.
  destruct (orb_false_elim _ _ Hdit) as [Hdit1 Hdit2];
   srewrite (Hind _ (sdepth_l t1 t2) _ _ i'' Hdit1);
   apply (Hind _ (sdepth_r t1 t2) _ _ (DB_lift_ d i'') Hdit2).
  apply (Hind _ (sdepth t) _ _ (DB_lift_ d i'') Hdit).
Qed.

Theorem Bp_forall_free_free:forall (p:Π)(i i':Ι), (i∖p)->(i∖∀(i'∙p)).
Proof.
 intros p i i' Hip; simpl; apply (Bt_abstr_free_free_ p 0 i i' Hip).
Qed.

Theorem Be_cmpset_free_free:forall (e:Ε)(p:Π)(i i':Ι), (i∖e)->(i∖p)->(i∖{i'∊e∣p}).
Proof.
 intros e p i i' Hie Hip; simpl; srewrite Hie; bool_simpl; apply (Bt_abstr_free_free_ p 0 i i' Hip).
Qed.

Theorem Bt_abstr_depth:forall (t:Τ)(i:Ι)(d:nat), ↧(Bt_abstr_ d i t)=↧(t).
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι)(d:nat), ↧(Bt_abstr_ d i t)=↧(t)));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i d;
  try (apply refl_equal); try (srewrite (Hind _ (sdepth t)); apply refl_equal);
  try (srewrite (Hind _ (sdepth_l t1 t2)); srewrite (Hind _ (sdepth_r t1 t2)); apply refl_equal).
Qed.

(* Characterisation ------------------------------------------------------------------------------*)

Theorem is_forall:forall (i:Ι)(p:Π), ?∀Δ(∀(i∙p)).
Proof.
 intros i p; unfold Bp_forall; apply is_bfor_.
Qed.
Notation "'!∀(' i '∙' p ')'":= (is_forall i p).

Theorem is_cmpset:forall (i:Ι)(e:Ε)(p:Π), ?ⅭΔ({i∊e∣p}).
 intros i e p; unfold Be_cmpset; apply is_bcmp_.
Qed.
Notation "'!{' i '∊' e '∣' p ')'":= (is_cmpset i e p).

(*================================================================================================*)
