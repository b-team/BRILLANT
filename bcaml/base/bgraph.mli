(* $Id$ *)

(** AMN Dependencies Graph Library *)

type linkType =
    Promotes
  | Extends
  | Sees
  | Uses
  | Includes
  | Imports
  | RefinedBy
  | Renames

type 'a edge = 'a * linkType * 'a
type 'a t = 'a edge list


val emptyGraph : 'a list
val mk_edge : 'a -> 'b -> 'c -> 'a * 'b * 'c
val graph_to_string : ('a -> string) -> ('a * linkType * 'a) list -> string
exception No_root_node
exception Several_root_nodes
val get_root_node : ('a * 'b * 'a) list -> 'a
val merge : 'a list -> 'a list -> 'a list
val subgraph : ('a * 'b * 'a) list -> 'a -> ('a * 'b * 'a) list
val add : 'a list -> 'a -> 'a list
val remove : 'a list -> 'a -> 'a list
val to_list : 'a -> 'a
val filter_target : ('a * 'b * 'c) list -> 'c -> ('a * 'b * 'c) list
val source : 'a * 'b * 'c -> 'a
val target : 'a * 'b * 'c -> 'c
val dependency : 'a * 'b * 'c -> 'b
val nodes : ('a * 'b * 'a) list -> 'a list
val filter : 'a list -> ('a -> bool) -> 'a list
val filter_targetDep : ('a * 'b * 'c) list -> 'b -> 'c -> ('a * 'b * 'c) list

val filter_sourceDep : ('a * 'b * 'c) list -> 'b -> 'a -> ('a * 'b * 'c) list
val filter_source : ('a * 'b * 'c) list -> 'a -> ('a * 'b * 'c) list
val sort_edges : ('a * linkType * 'b) list -> ('a * linkType * 'b) list
val prefixable_dependency : 'a * linkType * 'b -> bool

(* MG A l'exterieur de lib/ *) 

val dependedMachines : ('a * linkType * 'a) list -> 'a -> 'a list

val dot : ('a -> string) -> ('a * linkType * 'a) list -> string
