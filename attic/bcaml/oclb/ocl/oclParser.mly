/*** $Id$ ***/



/** {5 Fichier OcamlYacc (générateur d'analyseur syntaxique) }
@author 
@version 
*/





%{
open Ocl
%}

/**************** Tokens *****************/
%token <string> STRING
%token <string> NAME
%token <string> TYPENAME
%token <string> NUMBER
%token TRUE FALSE
%token EQUAL SUP LOW SUPEQUAL LOWEQUAL DIFFERENT
%token MINUS NOT PLUS MULTIPLY DIVIDE
%token ARROW DOT 
%token COLON DOUBLECOLON BAR RANGE SEMICOLON COMMA
%token OHOOK CHOOK OPARENTHESIS CPARENTHESIS OACCOLADE CACCOLADE
%token INVARIANTS ENDINVARIANTS FEATURES ENDFEATURES SUPERTYPE
%token SET BAG SEQUENCE COLLECTION
%token AND OR XOR IMPLIES
%token PRE TIME
%token LET IN DEF PRE POST INV PACKAGE ENDPACKAGE CONTEXT ALLINSTANCES
%token IF THEN ELSE ENDIF
%token SELECT COLLECT REJECT ITERATE EXISTS FORALL SIZE INTERSECTION UNION DIFFERENCE SYMMDIFF ISEMPTY ISNOTEMPTY INCLUDING EXCLUDING INCLUDES INCLUDESALL COUNT FIRST LAST AT APPEND PREPEND OCLTYPE OCLISKINDOF OCLISTYPEOF OCLASTYPE
%token EOL



/**************** Precedence rules ******************/

%right DCL3
%right DCL4
%nonassoc FPL1
%nonassoc AI
%nonassoc PNL
%nonassoc DCL12

%nonassoc PCP2
%nonassoc PRC4
%nonassoc QUA
%nonassoc PRC3
%nonassoc PRC5
%nonassoc PRC6
%nonassoc PRC7
%nonassoc PRC8
%nonassoc APL
%nonassoc QEXP

%nonassoc TIME
%nonassoc OPARENTHESIS CPARENTHESIS
%nonassoc CHOOK OHOOK
%left OR 
%left XOR 
%left AND 
%left IMPLIES
%left EQUAL 
%left SUP LOW SUPEQUAL LOWEQUAL DIFFERENT
%nonassoc BAR
%left PLUS
%left MINUS
%left MULTIPLY 
%left DIVIDE

%left LOGOP
%left RELOP
%left ADDOP
%left MULTOP


%left RANGE
%left SEMICOLON
%left COLON
%left ARROW DOT
%left DOUBLECOLON
%left COMMA

%left CONTEXT
%left PRE PREC POST POSTC INV INVA

%nonassoc UNOP
%nonassoc UMINUS

%start main
%type <Ocl.expr> main
%%



/*************** entry point ****************/

main :
  expression EOL		{ $1 }
| contextDeclaration EOL 	{ $1 }
| precondition EOL		{ $1 }
| postcondition EOL		{ $1 }
| invariant EOL			{ $1 }
| oclConstraint EOL 		{ $1 }
;

unaryOperator :
  NOT		{ Not } ;

multiplyOperator :
  MULTIPLY	{ Multiply }
| DIVIDE	{ Divide } ;

addOperator :
  PLUS 	 	{ Plus }
| MINUS 	{ Minus } ;

relationalOperator :
  EQUAL 	{ Equal }
| SUP		{ Sup }
| LOW		{ Low }
| SUPEQUAL	{ Supequal }
| LOWEQUAL	{ Lowequal }
| DIFFERENT	{ Different } ;

collectionKind :
  SET		{ Set }
| BAG		{ Bag }
| SEQUENCE	{ Sequence }
| COLLECTION	{ Collection } ;

logicalOperator :
  AND		{ And }
| OR		{ Or }
| XOR		{ Xor }
| IMPLIES	{ Implies } ;

constant :
  TRUE		{ True }
| FALSE		{ False }

typeNameList : 
  TYPENAME 				
  { Type_lit $1 }
| typeNameList DOUBLECOLON typeNameList 
  { Tlist ($1,$3) }
;
     
pathNameList :
  NAME 				 	
  { Name_lit $1 }
| NUMBER
  { Number_lit $1 }
| STRING
  { String_lit $1 }
| constant
  { Const $1 }
| pathNameList DOUBLECOLON typeNameList
  { Path ($1,$3) }
| pathNameList COMMA pathNameList
  { Nlist ($1,$3) } 
| typeNameList DOUBLECOLON pathNameList 
  { Path ($3,$1) }
;

typeSpecifier :
  typeNameList					
  { Simp_type $1 }
| collectionKind OPARENTHESIS typeNameList CPARENTHESIS	
  { Coll_type ($1,$3) }
;



/*************** Ocl expression ****************/

expression :
  pathNameList %prec PNL
  { Prim_exp $1 }
| pathNameList COLON typeSpecifier %prec DCL12
  { Decl1 ($1,$3) }
| pathNameList SEMICOLON NAME COLON typeSpecifier EQUAL expression %prec DCL3
  { Decl3 ($1,Name_lit $3,$5,$7) }
| pathNameList COLON typeSpecifier SEMICOLON NAME COLON typeSpecifier EQUAL expression %prec DCL4
  { Decl4 ($1,$3,Name_lit $5,$7,$9) }
| expression COMMA expression %prec APL
  { Act_p_list1 ($1,$3) }
| OHOOK expression CHOOK %prec QUA
  { Qualif $2 }
| OPARENTHESIS CPARENTHESIS 
  { Prop_call_par1 }
| OPARENTHESIS expression CPARENTHESIS %prec PCP2
  { Prop_call_par2 $2 } 

| OPARENTHESIS expression CPARENTHESIS COLON typeSpecifier
  { Prop_call_par3 ($2,$5) }

| OPARENTHESIS expression expression CPARENTHESIS
  { Prop_call_par4 ($2,$3) } 
| OPARENTHESIS expression BAR expression CPARENTHESIS
  { Prop_call_par4 ($2,$4) }
| expression RANGE expression
  { Coll_item2 ($1,$3) }
| collectionKind OACCOLADE CACCOLADE
  { Lit_coll1 $1 }
| collectionKind OACCOLADE expression CACCOLADE
  { Lit_coll2 ($1,$3) }



/**************** Property call ******************/

| pathNameList TIME PRE
  { Prop_call2 $1 }
| pathNameList OHOOK expression CHOOK %prec PRC3
  { Prop_call3 ($1,Qualif $3) }

| pathNameList OPARENTHESIS CPARENTHESIS %prec PRC4
  { Prop_call4 ($1,Prop_call_par1) }

| pathNameList OPARENTHESIS expression CPARENTHESIS %prec PRC4
  { Prop_call4 ($1,Prop_call_par2 $3) }

| pathNameList OPARENTHESIS CPARENTHESIS COLON typeSpecifier
  { Prop_call4 ($1,Prop_call_par3(Prop_call_par1,$5)) }

| pathNameList OPARENTHESIS expression CPARENTHESIS COLON typeSpecifier
  { Prop_call4 ($1,Prop_call_par3($3,$6)) }

| pathNameList OPARENTHESIS expression expression CPARENTHESIS %prec PRC4
  { Prop_call4 ($1,Prop_call_par4($3,$4)) }

| pathNameList TIME PRE OHOOK expression CHOOK %prec PRC5
  { Prop_call5 ($1,Qualif $5) }
| pathNameList TIME PRE OPARENTHESIS expression CPARENTHESIS %prec PRC6
  { Prop_call6 ($1,Prop_call_par2 $5) }
| pathNameList TIME PRE OPARENTHESIS expression BAR expression CPARENTHESIS %prec PRC6
  { Prop_call6 ($1,Prop_call_par4($5,$7)) }
| pathNameList OHOOK expression CHOOK OPARENTHESIS expression CPARENTHESIS %prec PRC7
  { Prop_call7 ($1,Qualif $3,Prop_call_par2 $6) }
| pathNameList OHOOK expression CHOOK OPARENTHESIS expression BAR expression CPARENTHESIS %prec PRC7
  { Prop_call7 ($1,Qualif $3,Prop_call_par4($6,$8)) }
| pathNameList TIME PRE OHOOK expression CHOOK OPARENTHESIS expression CPARENTHESIS %prec PRC8
  { Prop_call8 ($1,Qualif $5,Prop_call_par2 $8) }
| pathNameList TIME PRE OHOOK expression CHOOK OPARENTHESIS expression BAR expression CPARENTHESIS %prec PRC8
  { Prop_call8 ($1,Qualif $5,Prop_call_par4($8,$10)) }



/**************** Property call - operations ******************/
| SIZE
  { Size }
| INTERSECTION OPARENTHESIS expression CPARENTHESIS
  { Intersection (Prop_call_par2 $3) }
| UNION OPARENTHESIS expression CPARENTHESIS
  { Union (Prop_call_par2 $3) }
| DIFFERENCE OPARENTHESIS expression CPARENTHESIS
  { Difference (Prop_call_par2 $3) }
| SYMMDIFF OPARENTHESIS expression CPARENTHESIS
  { Symmdiff (Prop_call_par2 $3) }
| ISNOTEMPTY
  { Is_notempty}
| ISEMPTY
  { Is_empty }
| INCLUDING OPARENTHESIS expression CPARENTHESIS
  { Including (Prop_call_par2 $3) }
| EXCLUDING OPARENTHESIS expression CPARENTHESIS
  { Excluding (Prop_call_par2 $3) }
| INCLUDES OPARENTHESIS expression CPARENTHESIS
  { Includes (Prop_call_par2 $3) }
| INCLUDESALL OPARENTHESIS expression CPARENTHESIS
  { Includes_all (Prop_call_par2 $3) }
| COUNT
  { Count }
| FIRST
  { First }
| LAST
  { Last }
| AT OPARENTHESIS expression CPARENTHESIS
  { At (Prop_call_par2 $3) }
| APPEND OPARENTHESIS expression CPARENTHESIS
  { Append (Prop_call_par2 $3) }
| PREPEND OPARENTHESIS expression CPARENTHESIS
  { Prepend (Prop_call_par2 $3) }
| OCLTYPE
  { Ocltype }
| OCLISKINDOF OPARENTHESIS expression CPARENTHESIS
  { Ocliskindof (Prop_call_par2 $3) }
| OCLISTYPEOF OPARENTHESIS expression CPARENTHESIS
  { Oclistypeof (Prop_call_par2 $3) }
| OCLASTYPE OPARENTHESIS expression CPARENTHESIS
  { Oclastype (Prop_call_par2 $3) }



/**************** Property call - query expression ******************/

| SELECT OPARENTHESIS expression CPARENTHESIS
  { Select (Prop_call_par2 $3) }
| SELECT OPARENTHESIS expression BAR expression CPARENTHESIS 
  { Select (Prop_call_par4($3,$5)) }
| REJECT OPARENTHESIS expression CPARENTHESIS
  { Reject (Prop_call_par2 $3) }
| REJECT OPARENTHESIS expression BAR expression CPARENTHESIS
  { Reject (Prop_call_par4($3,$5)) }
| COLLECT OPARENTHESIS expression CPARENTHESIS
  { Collect (Prop_call_par2 $3) }
| COLLECT OPARENTHESIS expression BAR expression CPARENTHESIS 
  { Collect (Prop_call_par4($3,$5)) }
| ITERATE OPARENTHESIS expression CPARENTHESIS
  { Iterate (Prop_call_par2 $3) }
| ITERATE OPARENTHESIS expression BAR expression CPARENTHESIS 
  { Iterate (Prop_call_par4($3,$5)) }
| EXISTS OPARENTHESIS expression CPARENTHESIS
  { Exists (Prop_call_par2 $3) }
| EXISTS OPARENTHESIS expression BAR expression CPARENTHESIS 
  { Exists (Prop_call_par4($3,$5)) }
| FORALL OPARENTHESIS expression CPARENTHESIS
  { Forall (Prop_call_par2 $3) }
| FORALL OPARENTHESIS expression BAR expression CPARENTHESIS
  { Forall (Prop_call_par4($3,$5)) }



/**************** postfix expression ******************/

| expression DOT expression
  { Postf_exp3 ($1,$3) }
| expression ARROW expression
  { Postf_exp4 ($1,$3) }
| typeNameList DOT ALLINSTANCES %prec AI
  { Allinstances ($1) }



/**************** arithmetic expression ******************/

| unaryOperator expression %prec UNOP
  { Unary_exp1 ($1,$2) }
| expression multiplyOperator expression %prec MULTOP
  { Mult_exp2 ($2,$1,$3) }
| expression addOperator expression %prec ADDOP
  { Add_exp2 ($2,$1,$3) }
| expression relationalOperator expression %prec RELOP
  { Rel_exp2 ($2,$1,$3) }
| expression logicalOperator expression %prec LOGOP
  { Log_exp2 ($2,$1,$3) }



/**************** if-then-else expression ******************/

| IF expression THEN expression ELSE expression ENDIF
  { If_exp ($2,$4,$6) }
;



/**************** context declaration ******************/

contextDeclaration :
  CONTEXT expression
  { Context1 $2 }
| CONTEXT typeSpecifier
  { Context2 $2 }
;



/**************** OCL constraint: invariant, precondition, postcondition ******************/

invariant:
  INV NAME COLON expression
  { Inv1 (Name_lit $2,$4) }
| INV COLON expression
  { Inv2 $3 }
| invariant invariant %prec INVA
  { Inv3 ($1,$2) }
;

precondition :
  PRE NAME COLON expression
  { Pre1 (Name_lit $2,$4) }
| PRE COLON expression
  { Pre2 $3 }
| precondition precondition %prec PREC
  { Pre3 ($1,$2) }
;

postcondition :
  POST NAME COLON expression
  { Post1 (Name_lit $2,$4) }
| POST COLON expression
  { Post2 $3 }
| postcondition postcondition %prec POSTC
  { Post3 ($1,$2) }
;

oclConstraint :
  contextDeclaration precondition
  { Constraint1 ($1,$2) }
| contextDeclaration postcondition
  { Constraint2 ($1,$2) }
| contextDeclaration precondition postcondition
  { Constraint3 ($1,$2,$3) }
| contextDeclaration invariant
  { Constraint4 ($1,$2) }
;

