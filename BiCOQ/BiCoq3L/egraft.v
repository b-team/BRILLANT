(*==================================================================================================
  Project : BiCoq3
  Module : egraft.v
  We define here our grafting function, a form of capturing substitution.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Oct 2008, Md: Oct 2008
  ================================================================================================*)

Require Export esubst.
Require Export abstr.
Require Import List.

(* Grafting function -----------------------------------------------------------------------------*)

Fixpoint Bp_egraft(i:Ι)(p:Π)(a:Ε){struct p}:Π:=
 match p with
 | ¬p' => ¬(Bp_egraft i p' a)
 | ∀Δ(p') => ∀Δ(Bp_egraft (ulft i) p' a)
 | p1⋀p2 => (Bp_egraft i p1 a)⋀(Bp_egraft i p2 a)
 | (p1⇒p2) => (Bp_egraft i p1 a)⇒(Bp_egraft i p2 a)
 | ∐(_) => p
 | e1≡e2 => (Be_egraft i e1 a)≡(Be_egraft i e2 a)
 | e1∈e2 => (Be_egraft i e1 a)∈(Be_egraft i e2 a)
 end
with Be_egraft(i:Ι)(e a:Ε){struct e}:Ε :=
 match e with
 | Ω => e
 | ↓(e') => ↓(Be_egraft i e' a)
 | ↑(e') => ↑(Be_egraft i e' a)
 | e1↦e2 => (Be_egraft i e1 a)↦(Be_egraft i e2 a)
 | e1×e2 => (Be_egraft i e1 a)×(Be_egraft i e2 a)
 | ∞(_) => e
 | ∂(i') => if DB_eq i i' then a else ∂(i')
 | ⅭΔ(e',p') => ⅭΔ(Be_egraft i e' a,Bp_egraft (ulft i) p' a)
 end.
Notation "'〈' i '⇙' a '@Π@' p '〉'" := (Bp_egraft i p a).
Notation "'〈' i '⇙' a '@Ε@' e '〉'" := (Be_egraft i e a).

Definition Bt_egraft(i:Ι)(t:Τ)(a:Ε):Τ :=
 match t with be_ e' => Be_egraft i e' a | bp_ p' => Bp_egraft i p' a end.
Notation "'〈' i '⇙' a '@' t '〉'" := (Bt_egraft i t a).

(* Theorems for grafting -------------------------------------------------------------------------*)

Theorem Bt_egraft_depth:forall (t:Τ)(i1 i2:Ι), ↧(〈i1⇙∂(i2)@t〉)=↧(t).
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i1 i2:Ι), ↧(〈i1⇙∂(i2)@t〉)=↧(t)));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i1 i2;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) i1 i2); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) i1 i2); irewrite (Hind _ (sdepth_r t1 t2) i1 i2);
       apply refl_equal).
  destruct (DB_eq i1 i'); apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) i1 i2); irewrite (Hind _ (sdepth_r t1 t2) (ulft i1) i2);
   apply refl_equal.
  irewrite (Hind _ (sdepth t) (ulft i1) i2); apply refl_equal.
Qed.

Theorem Bt_egraft_free:forall (t:Τ)(i:Ι)(a:Ε), (i∖t)->〈i⇙a@t〉=t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι)(a:Ε), (i∖t)->〈i⇙a@t〉=t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i a Hit;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) _ a Hit); apply refl_equal);
  try (destruct (orb_false_elim _ _ Hit) as [Hit1 Hit2];
       irewrite (Hind _ (sdepth_l t1 t2) _ a Hit1); irewrite (Hind _ (sdepth_r t1 t2) _ a Hit2);
       apply refl_equal).
  destruct (DB_eq i i'); [inversion Hit | apply refl_equal].
Qed.

(*================================================================================================*)