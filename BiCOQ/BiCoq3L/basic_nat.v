(*==================================================================================================
  Project : BiCoq3
  Module : basic_nat.v
  This module provides a few results about naturals, in addition to those in the Coq library.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Jun 2008, Md: Jun 2008
  ================================================================================================*)

Require Export Lt.
Require Export basic_logic.

(* Interface for built-in Ocaml functions --------------------------------------------------------*)
(* We declare two variables for equality and comparison, to use native Ocaml functions. *)

Variable nat_eq:nat->nat->Β.
Variable nat_eq_imp:((fun x y:nat=>x=y)⇝²nat_eq).

Variable nat_lt:nat->nat->Β.
Variable nat_lt_imp:((fun x y:nat=>x<y)⇝²nat_lt).

Variable nat_le:nat->nat->Β.
Variable nat_le_imp:((fun x y:nat=>x<=y)⇝²nat_le).

Definition eq_0(n:nat):Β := match n with 0 => ⊤ | _ => ⊥ end.

Theorem nat_eq_refl:forall (u:nat), nat_eq u u=⊤.
Proof.
 intros u; reflect nat_eq_imp; apply refl_equal.
Qed.

Theorem eq_0_0:nat_eq 0 0=⊤.
Proof.
 apply nat_eq_refl.
Qed.

Theorem eq_0_S:forall (u:nat), nat_eq 0 (S u)=⊥.
Proof.
 intros u; reflect nat_eq_imp; intros H; inversion H.
Qed.

Theorem eq_S_0:forall (u:nat), nat_eq (S u) 0=⊥.
Proof.
 intros u; reflect nat_eq_imp; intros H; inversion H.
Qed.

Theorem eq_S_S:forall (u1 u2:nat), nat_eq (S u1) (S u2)=nat_eq u1 u2.
Proof.
 intros u1 u2; case_eq (nat_eq u1 u2); intros Heq; reflect nat_eq_imp; reflect_in nat_eq_imp Heq.
  rewrite Heq; apply refl_equal.
  intros Heq'; injection Heq'; intros Heq''; apply (Heq Heq'').
Qed.

Theorem eq_eq0_l:forall (u:nat), nat_eq 0 u=eq_0 u.
Proof.
 intros [ | u]; simpl; [apply eq_0_0 | apply eq_0_S].
Qed.

Theorem eq_eq0_r:forall (u:nat), nat_eq u 0=eq_0 u.
Proof.
 intros [ | u]; simpl; [apply eq_0_0 | apply eq_S_0].
Qed.

Theorem nat_le_0:forall (n:nat), nat_le 0 n=⊤.
Proof.
 intros n; reflect nat_le_imp; apply Le.le_O_n.
Qed.

Theorem nat_le_S:forall (n:nat), nat_le (S n) 0=⊥.
Proof.
 intros n; reflect nat_le_imp; intros Hle; inversion Hle.
Qed.

Theorem nat_le_S_S:forall (n1 n2:nat), nat_le (S n1) (S n2)=nat_le n1 n2.
Proof.
 intros n1 n2; case_eq (nat_le n1 n2); intros Hle; reflect nat_le_imp; reflect_in nat_le_imp Hle.
  apply (Le.le_n_S _ _ Hle).
  intros Hle'; apply Hle; apply (Le.le_S_n _ _ Hle').
Qed.

Theorem nat_le_refl:forall (n:nat), nat_le n n=⊤.
Proof.
 intros n; reflect nat_le_imp; apply Le.le_refl.
Qed.

(* Well-founded induction principle --------------------------------------------------------------*)
(* wf_measure_dep is the strong induction principle for a measure... To have the most general form
   for this principle, which is required later in the development, we define this induction
   principle with a measure over D, a family of types indexed by T. *)

Theorem wf_measure_dep:forall (T:Type)
                              (D:T->Type)
                              (M:forall (t:T), D t->nat)
                              (P:forall (t:T), D t->Type),
                       (forall (t:T)(d:D t),
                        (forall (t':T)(d':D t'),(M t' d')<(M t d)->P t' d')->P t d)->
                       forall (t:T)(d:D t), P t d.
Proof.
 intros T D M P Hind; assert (H:forall (i:nat)(t:T)(d:D t), (M t d)<i->P t d).
  induction i as [ | i IHi]; intros t d H.
   destruct (lt_n_O (M t d) H).
   apply Hind; intros t' d' H0; apply IHi; inversion_clear H;
    [apply H0 | apply (lt_trans _ _ _ H0 H1)].
 intros t d; apply Hind; intros t' d' Ht'; apply (H (M t d)); apply Ht'.
Qed.
Implicit Arguments wf_measure_dep [T D].

Theorem wf_measure:forall (T:Type)(M:T->nat)(P:T->Type),
                   (forall (t:T), (forall (t':T), (M t')<(M t)->P t')->P t)->forall (t:T), P t.
Proof.
 intros T M P Hind;
  generalize (wf_measure_dep (fun (u:unit)(d:(fun u:unit=>T) u)=>M d)
                             (fun (u:unit)(d:(fun u:unit=>T) u)=>P d)); intros Hdep; simpl in Hdep;
  apply Hdep; [intros _ d H; apply Hind; apply H | ]; apply tt.
Qed.
Implicit Arguments wf_measure [T].

Theorem wf_measure_dbl:forall (T1 T2:Type)(M1:T1->nat)(M2:T2->nat)(P1:T1->Type)(P2:T2->Type),
                       (forall (t:T1),
                        (forall (t':T1), (M1 t')<(M1 t)->P1 t')->
                        (forall (t':T2), (M2 t')<(M1 t)->P2 t')->P1 t)->
                       (forall (t:T2),
                        (forall (t':T1), (M1 t')<(M2 t)->P1 t')->
                        (forall (t':T2), (M2 t')<(M2 t)->P2 t')->P2 t)->
                       (forall (t:T1), P1 t)*(forall (t:T2), P2 t).
Proof.
 intros T1 T2 M1 M2 P1 P2 HP1 HP2; set (D:=fun t:Β=>match t with ⊤ => T1 | ⊥ => T2 end );
  set (M:=fun t:Β=>match t as x return (D x->nat) with ⊤ => fun (d:T1)=>M1 d
                                                     | ⊥ => fun (d:T2)=>M2 d end);
  set (P:=fun t:Β=>match t as x return (D x->Type) with ⊤ => fun (d:T1)=>P1 d
                                                      | ⊥ => fun (d:T2)=>P2 d end);
  generalize (wf_measure_dep M P); intros Hdep; split.
  intros t1; replace (P1 t1) with (P ⊤ t1).
   apply Hdep; induction t as [ | ]; simpl; intros t' Hind.
    apply HP1; [apply (Hind ⊤) | apply (Hind ⊥)].
    apply HP2; [apply (Hind ⊤) | apply (Hind ⊥)].
   apply refl_equal.
  intros t2; replace (P2 t2) with (P ⊥ t2).
   apply Hdep; induction t as [ | ]; simpl; intros t' Hind.
    apply HP1; [apply (Hind ⊤) | apply (Hind ⊥)].
    apply HP2; [apply (Hind ⊤) | apply (Hind ⊥)].
   apply refl_equal.
Qed.
Implicit Arguments wf_measure_dbl [T1 T2].

(* Tactics for equality between natural values ---------------------------------------------------*)

Ltac nat_simpl := repeat (rewrite nat_eq_refl in * ||
                          rewrite eq_S_0  in * ||
                          rewrite eq_0_S in * ||
                          rewrite eq_S_S in * ||
                          rewrite eq_eq0_l in * ||
                          rewrite eq_eq0_r in * ||
                          rewrite nat_le_refl in * ||
                          rewrite nat_le_0 in * ||
                          rewrite nat_le_S in * ||
                          rewrite nat_le_S_S in *).

(*================================================================================================*)