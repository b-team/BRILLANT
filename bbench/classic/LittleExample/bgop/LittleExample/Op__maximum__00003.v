 

Require Import Blib.

Theorem op:
forall _yy, ( 
 (In (Power_finite BN1) _yy) -> 
(_yy <> (Empty_set _))
  -> (
 (In (Power_finite BN1) _yy) ) )
 .
try intuition 
   || unfold BBOOL ; intuition ; apply Full_intro
   || red ;  red ; red ; omega.
Qed.
 