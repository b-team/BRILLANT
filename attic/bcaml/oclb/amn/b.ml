(*** $Id$ ***)



(** {5 Syntaxe abstraite B }
@author 
@version 
*)





(* DEFINITION DES TYPES *)

(** type g�n�rique d'un identificateur *)
type ident = Var of string ;;

type id = ident ;; (* !!! provisoire !!! *) (* blast.ml *)

type ids = id list ;; (* blast.ml *)

type head = id * ids ;; (* blast.ml *)

type instance = ident ;; (* !!! provisoire !!! *) (* blast.ml *)



(** d�finition des ensembles de base *)
type uml_type =
    Seq of uml_type     (* suites d'�l�ments *)
  | SeqOne of uml_type  (* suites non vides *)
  | ISeq of uml_type    (* suites injectives *)
  | ISeqOne of uml_type (* suites injectives non vides *)
  | Perm of uml_type    (* permutations (suites bijectives) *)
  | String              (* cha�nes de caract�res *)
  | Bool                (* bool�ens *)
  | Entier              (* entiers relatifs *)
  | Nat                 (* entiers naturels impl�mentables *)
  | Nat1                (* entiers naturels non nuls impl�mentables *)
  | Nat2 of string      (* ??? *)
  | Var2 of string      (* ??? *)
  | Pow of uml_type     (* sous-ensembles *)
  | PowOne of uml_type  (* sous-ensembles non vides *)
  | Fin of uml_type     (* sous-ensembles finis *)
  | FinOne of uml_type  (* sous-ensembles finis non vides *)
;;



(*** D�finition des relations ***)

(** d�finition des op�rateurs utilis�s dans la syntaxe des pr�dicats *)
type elem_rel =
    Inclus
  | Inclus_ou_egal
  | Appartient
  | Different_rel
  | Est_def
  | Egal
  | Inferieur
  | No_appart
  | Intersection_rel
;;



(** caract�risation d'une relation *)
type type_relation =
    Relation    (* relation quelconque *)
  | PartialFunc (* fonction partielle *)
  | PartialInj  (* fct partielle injective *)
  | PartialSurj (* fct partielle surjective *)
  | PartialBij  (* fct partielle bijective *)
  | TotalFunc   (* application: fonction totale *)
  | TotalInj    (* application injective *)
  | TotalSurj   (* application surjective *)
  | TotalBij    (* application bijective *)
;;



(*** D�finition des op�rateurs ***)

(** les diff�rents op�rateurs de la notation B *)
type op =
(* op�rateurs arithm�tiques *)
    Plus
  | Minus
  | UMinus    (* moins unaire *)
  | Mul
  | Div
  | Mod       (* modulo *)
  | Puissance

(* op�rateurs relationnels *)
  | Equal
  | NotEqual
  | Less
  | LessEqual
  | Greater
  | GreaterEqual

(* op�rateurs logiques *)
  | Not
  | Or
  | And
  | Implies
  | Equiv

(* op�rateurs ensemblistes *)
  | SubSet
  | NotSubSet
  | StrictSubSet
  | NotStrictSubSet
  | In
  | NotIn

  | Card
  | Is_empty
  | InterSets
  | UnionSets
  | Difference
  | Symmdiff          (* diff�rence sym�trique: (A union B) - (A inter B) *)

(* op�rateurs sur les relations (fonctions) *)
  | Identity          (* relation identit� *)
  | CartesianProd     (* produit cart�sien *)
  | Closure           (* fermeture transitive et r�flexive *)
  | ClosureOne        (* fermeture transitive *)
  | DomRestrict       (* restriction de domaine *)
  | RanRestrict       (* restriction de co-domaine *)
  | DomSubstract      (* anti-restriction de domaine: *)
                         (* on conserve les couples dont l'�l�ment de gauche
			    n'appartient pas � un certain sous-ensemble du domaine *)
  | RanSubstract      (* anti-restriction de co-domaine: *)
                         (* on conserve les couples dont l'�l�ment de droite
			    n'appartient pas � un certain sous-ensemble du co-domaine *)
  | OverRideFwd       (* surcharge: *)
                         (* on conserve tous les couples de la relation �crasante,
			    plus les couples de la relation �cras�e dont l'�l�ment de
			    gauche n'appartient pas au domaine de la relation �crasante *)
  | OverRideBck       (* OverRideBck(R1,R2) = OverRideFwd(R2,R1) *)
  | Produit_direct    (* produit direct: *)
                         (* si R1:A<->B et R2:A<->C, alors R = R1><R2 est l'ensemble des
			    couples (a,(b,c)) tels que (a,b):R1 et (a,c):R2 *)
  | Produit_parallel  (* produit parall�le: *)
                         (* si R1:A<->B et R2:B<->C, alors R = R1||R2 est l'ensemble des
			    couples ((a,b1),(b2,c)) tels que (a,b2):R1 et (b1,c):R2 *)
  | Composition       (* composition en avant: f;g = g�f *)
  | RelFnc            (* transform�e en fonction *)
  | FncRel            (* transform�e en relation *)
  | Prj1              (* Prj1(A,B) = ensemble des couples ((a,b),a) tels que a:A et b:B *)
  | Prj2              (* Prj2(A,B) = ensemble des couples ((a,b),b) tels que a:A et b:B *)

(* op�rateurs sur les suites *)
  | Size              (* taille d'une suite *)
  | Count             (* nb d'occurrences d'un �l�ment *)
  | Front             (* premier �l�ment *)
  | Tail              (* queue d'une suite *)
  | Last              (* dernier �l�ment *)
  | At                (* i-�me �l�ment *)
  | AppendSeq         (* ajout � la fin *)
  | PrependSeq        (* ajout en t�te *)
  | PrefixSeq         (* garder la partie suivante de la suite *)
  | SuffixSeq         (* laisser tomber la partie suivante *)
  | ConcatSeq         (* concat�nation de suites *)
  | Rev               (* retournement d'une suite *)

(* op�rateurs sur les arbres *)
  | Tree
  | Btree
  | Left
  | Right
  | Top
  | Sons
  | Prefix
  | Infix
  | Postfix
  | SizeT
  | Rank
  | Father
  | SubTree
  | Arity

(* ??? cat�gorie?  ??? *)
  | Addition          (* ??? diff�rence avec Union ? ??? *)
  | Soustraction      (* ??? cf. Addition ??? *)
  | Vide              (* ??? *)
;;

(*
type op1 = (* !!! blast.ml: op�rateurs restants pour expressions unaires !!! *)
  | Cod 
  | Min
  | Max
  | UnionGen
  | InterGen
  | Conc
  | Mirror
  | Bool
  | Pred
  | Succ
;;
*)

(*
type op2 = (* !!! blast.ml: op�rateurs restants pour expressions binaires !!! *)
  | NatSetRange
  | SetMinus
  | RelProd
  | ParallelComp
  | Image
;;
*)



(*** D�finition d'expressions droite et gauche ***)

(** ??? expression situ�e � gauche d'un op�rateur ??? *)
type expr_left =
    Ima of ident * ident
  | Val of ident
;;

(** ??? expression situ�e � droite d'un op�rateur ??? *)
type expr_right = 
(* expressions logiques *)
  | Forall of ident list * expr_right * expr_right (* quantificateur universel *)
  | Exists of ident list * expr_right * expr_right (* quantificateur existentiel *)
  | If_exp of expr_right * expr_right * expr_right (* if then else *)
  | Rel of expr_right * type_relation * expr_right (* ??? pr�dicat relationnel ??? *)

(* expressions ensemblistes *)
  | Empty_set (* l'ensemble vide *)
  | Tuple of expr_right * expr_right                (* maplet: couple d'�l�ments *)
  | Ens of expr_right                               (* ensemble d'�l�ments *)
  | Def_set of ident list * expr_right              (* ensemble d�fini en compr�hension *)
  | Union_q of ident list * expr_right * expr_right (* union quantifi�e *)
  | Inter_q of ident list * expr_right * expr_right (* intersection quantifi�e *)

(* expressions sur les relations (fonctions) *)
  | Dom of expr_right                       (* domaine d'une relation *)
  | Ran of expr_right                       (* range: co-domaine *)
  | Tilde of expr_right                     (* relation r�ciproque *)
  | Ima2 of expr_right * expr_right         (* image directe (d'une fonction) *)
  | Ima3 of expr_right * expr_right         (* image relationnelle *)
  | Exp_rel of expr_right * op * expr_right (* ??? expression entre 2 relations ??? *)

(* expressions d'objets math�matiques *)
  | Sum_g of ident list * expr_right * expr_right  (* produit quantifi� *)
  | Prod_g of ident list * expr_right * expr_right (* produit quantifi� d'entiers *)

(* expressions li�es par un op�rateur *)
  | Op_un of op * expr_right               (* op�rateur unaire *)
  | Op_bin of expr_right * op * expr_right (* op�rateur binaire *)

  | Type_base of uml_type (* type de base *)
  | Val_av of expr_right        (* valeur pr�c�dente d'une donn�e *)
  | Par_list of expr_right list (* ??? liste de param�tres ??? *)
;; 



(*** D�finition des pr�dicats ***)

(** syntaxe d'un pr�dicat *)
type predicate =
    Predicat of expr_right                    (* pr�dicat simple *)
  | Predicat1 of                              (* pr�dicat avec op�rateur *)
      expr_right
      * elem_rel
      * expr_right
  | Predicat2 of                              (* pr�dicat avec op�rateur et relation (fonction) *)
      expr_right
      * elem_rel
      * expr_right
      * type_relation
      * expr_right
  | Predicat3 of                              (* ??? *)
      expr_right
      * expr_right
  | Predicat4 of string                       (* ??? *)
  | Qlq of ident list * predicate * predicate (* quantificateur universel *)
  | EXT of ident list * predicate * predicate (* quantificateur existentiel *)
  | AND of predicate * predicate              (* conjonction *)
  | OR of predicate * predicate               (* disjonction *)
;;





(*** D�finition des composants d'une machine B ***)

(*
(** un INVARIANT est compos� d'une liste de pr�dicats *)
type invariant = Invariant of predicate list ;;
*)

(** caract�risation des substitutions en B *)
type substitution = 
    Sub of expr_left * expr_right                     (* substitution simple *)
  | Subst of expr_right * expr_right                  (* ??? diff�rence avec Sub ??? *)
  | Dev_tq of ident list * expr_right                 (* devient tel que *)
  | If of predicate * substitution  * substitution    (* si pr�dicat vrai, alors subst1, sinon subst2 *)
  | SELECT of                                         (* substitution gard�e *)
      (predicate * substitution)
      * (predicate list * substitution) list
  | SKIP                                              (* substitution sans effet *)
  | Appel1 of ident * ident list                      (* ??? *)
  | Parallel  of substitution list                    (* substitutions effectu�es simultan�ment *)
  | Any of ident list * predicate list * substitution (* substitution de choix non born� *)
;;

type event = id * ids * substitution ;; (* blast.ml *)


(** d�finition d'un ensemble *)
type set =
    Set1 of ident
  | Set2 of expr_right
;;

(** d�finition d'un alias *)
type def =
    Definition1 of expr_left * expr_right
  | Definition2 of predicate
;;

(*
(** la clause DEFINITIONS regroupe toutes les abr�viations *)
type definitions = Definitions of def list ;;
*)

(** une pr�-condition d'une substitution porte sur une liste de pr�dicats *)
type precond = Pre of predicate list ;;

(*
(** substitution d'initialisation *)
type initialisation = Initialisation of substitution ;;
*)

(*
(** la clause VARIABLES regroupe toutes les variables de la machine *)
type variables = Variables of ident list ;;
*)

(** une op�ration est d�finie par :
- un identificateur
- une liste de param�tres
- une liste de valeurs de retour
- une pr�-condition
- une substitution *)
type operation = Oper of
  ident list     (* ??? valeurs de retour ??? *)
  * ident
  * ident list   (* ??? param�tres ??? *)
  * precond
  * substitution ;;

(*
(** la clause OPERATIONS regroupe toutes les op�rations relatives � la machine *)
type operations = Operations of operation list ;;
*)


(*** Clauses d'architecture ***)

(*
(** la clause USES stipule le
partage des donn�es d'une machine incluse *)
type uses = Uses of ident list ;;
*)

(*
(** la clause SETS regroupe les
d�finitions des ensembles utilis�s dans la machine *)
type sets = Sets of set list ;;
*)

(*
(** la clause INCLUDES stipule le
partage des donn�es et des op�rations d'une autre machine *)
type includes = Includes of ident list ;;
*)

(*
(** la clause EXTENDS �nonce l'inclusion et la promotion
de toutes les op�rations de la machine incluse *)
type extends = Extends of ident list ;;
*)

(*
(** avec la clause PROMOTES, la machine incluante s'approprie les op�rations
de la machine incluse (pas de distinction avec ses propres op�rations) *)
type promotes = Promotes of ident list ;;
*)


(** d�finition g�n�rale d'une clause (blast.ml) *)
type clause = 
  | Definitions of def list
  | Constraints of predicate
  | Invariant of predicate list      (* !!! diff�rent de blast.ml !!! *)
  | Variant of expr_right            (* !!! diff�rent de blast.ml !!! *)
  | Sets of set list
  | Initialisation of substitution
  | ConstantsConcrete of ids
  | ConstantsAbstract of ids
  | ConstantsHidden of ids
  | Properties of predicate
  | Values of (id * expr_right) list (* !!!diff�rent de blast.ml !!! *)
  | VariablesConcrete of ids
  | VariablesAbstract of ids         (* !!! remplace la clause VARIABLES !!! *)
  | VariablesHidden of ids
  | Promotes of ids
  | Assertions of predicate list
  | Operations of operation list
  | Events of event list
  | Sees of ids
  | Uses of ids
  | Extends of instance list
  | Includes of instance list
  | Imports of instance list
;;


(** d�finition d'une machine B *)
type amn =
(*
    Machine of
      ident
      * sets
      * uses
      * extends
      * includes
      * promotes
      * definitions
      * variables
      * invariant
      * initialisation
      * operations
  | Refinement of
      ident
      * ident
      * sets
      * uses
      * extends
      * includes
      * promotes
      * definitions
      * variables
      * invariant
      * initialisation
      * operations
*)
  | Machine of         (* soit une machine abstraite *)
      head
      * clause list
  | Refinement of      (* soit un raffinement *)
      head
      * id
      * clause list
  | Implementation of  (* derni�re �tape du raffinement *) (* blast.ml *)
      head
      * id
      * clause list
  | EmptyTree          (* blast.ml *)
;;





(* DEFINITION DES FONCTIONS *)

(** valeur d'un identificateur
@return la cha�ne de caract�res correspondant au nom de cet identificateur *)
let devar (Var a) = a ;;

(** union de 2 clauses INVARIANT
@return un invariant contenant la fusion des 2 listes de pr�dicats *)
let unionI x y =
  match (x,y) with
      (Invariant a, Invariant b) -> Invariant (a@b)
    | _ -> failwith "unionI()"
;;

(** union de 2 clauses VARIABLES
@return la fusion des 2 listes d'identificateurs *)
let unionV x y =
  match (x,y) with
      (VariablesAbstract a, VariablesAbstract b) ->  a@b
    | _ -> failwith "unionV()"
;;	      



(*** Fonctions d'acc�s aux composants d'une machine B ***)

(** nom d'une machine
@return l'identificateur de la machine *)
let acces_nom u =
  match u with
      Machine ((x, _), _) -> x
    | Refinement ((x, _), _, _) -> x
    | _ -> failwith "acces_nom()"
;;

(** renvoie la liste des ensembles d'une  liste de clauses 
(on suppose que la clause SETS n'appara�t qu'une seule fois) *)
let rec cl_sets l =
  match l with
    | [] -> []
    | (Sets x)::t -> x
    | _::t -> cl_sets t

(** renvoie la liste des identificateurs de la clause USES 
(on suppose que la clause USES n'appara�t qu'une seule fois) *)
and cl_uses l =
  match l with
    | [] -> []
    | (Uses x)::t -> x
    | _::t -> cl_uses t

(** renvoie la liste des instances de la clause EXTENDS 
(on suppose que la clause EXTENDS n'appara�t qu'une seule fois) *)
and cl_extends l =
  match l with
    | [] -> []
    | (Extends x)::t -> x
    | _::t -> cl_extends t

(** renvoie la liste des instances de la clause INCLUDES 
(on suppose que la clause INCLUDES n'appara�t qu'une seule fois) *)
and cl_includes l =
  match l with
    | [] -> []
    | (Includes x)::t -> x
    | _::t -> cl_includes t

(** renvoie la liste des identificateurs de la clause PROMOTES 
(on suppose que la clause PROMOTES n'appara�t qu'une seule fois) *)
and cl_promotes l =
  match l with
    | [] -> []
    | (Promotes x)::t -> x
    | _::t -> cl_promotes t

(** renvoie la liste des d�finitions d'une  liste de clauses 
(on suppose que la clause DEFINITIONS n'appara�t qu'une seule fois) *)
and cl_definitions l =
  match l with
    | [] -> []
    | (Definitions x)::t -> x
    | _::t -> cl_definitions t

(** renvoie la liste des identificateurs des variables abstraites 
(on suppose que la clause VARIABLES n'appara�t qu'une seule fois) *)
and cl_variables l =
  match l with
    | [] -> []
    | (VariablesAbstract x)::t -> x
    | _::t -> cl_variables t

(** renvoie la liste des invariants d'une  liste de clauses 
(on suppose que la clause INVARIANT n'appara�t qu'une seule fois) *)
and cl_invariant l =
  match l with
    | [] -> []
    | (Invariant x)::t -> x
    | _::t -> cl_invariant t

(** renvoie la substitution d'initialisation d'une  liste de clauses 
(on suppose que la clause INITIALISATION n'appara�t qu'une seule fois) *)
and cl_initialisation l =
  match l with
    | [] -> Parallel []
    | (Initialisation x)::t -> x
    | _::t -> cl_initialisation t

(** renvoie la liste des op�rations d'une  liste de clauses 
(on suppose que la clause OPERATIONS n'appara�t qu'une seule fois) *)
and cl_operations l =
  match l with
    | [] -> []
    | (Operations x)::t -> x
    | _::t -> cl_operations t
;;



(** invariant d'une machine
@return la liste des pr�dicats composant l'invariant de la machine *)
let acces_inv u =
  match u with
      Machine (_, clauses) -> cl_invariant clauses
    | Refinement (_, _, clauses)-> cl_invariant clauses
    | _ -> failwith "acces_inv()"
;;

(** op�rations d'une machine
@return la liste des op�rations de la  machine *)
let acces_opers u =
  match u with
      Machine (_, clauses) -> cl_operations clauses
    | Refinement (_, _, clauses) -> cl_operations clauses
    | _ -> failwith "acces_opers"
;;

(** op�rations d'une machine
@return la clause OPERATIONS de la machine B *)
let acceso u =
  (** renvoie la clause OPERATIONS d'une liste de clauses 
    (on suppose que la clause OPERATIONS n'appara�t qu'une seule fois) *)
  let rec cl_oper clauses =
    match clauses with
      | [] -> Operations []
      | (Operations x as h)::t -> h
      | _::t -> cl_oper t
  in
    match u with
	Machine (_, clauses)-> cl_oper clauses
      | Refinement (_, _, clauses)-> cl_oper clauses
      | _ -> failwith "acceso()"
;;

(** nom d'une op�ration
@return la valeur de l'identificateur d'une op�ration *)
let acces_oper_id (Oper(_,Var ident,_,_,_)) = ident ;;




(*** Fonctions de cr�ation de composants d'une machine B ***)

(** cr�ation d'un identificateur
@param u une cha�ne de caract�res 
@return l'identificateur de nom u *)
let rec def_ident u = Var u

(** cr�ation de la clause USES
@param u une liste de noms (cha�nes de caract�res)
@return une clause USES compos�e d'une liste d'identificateurs
        correspondant aux noms de u en majuscules *)
and def_uses u =
  let u_cp = (List.map String.capitalize u)
  in Uses (List.map def_ident u_cp)

(** cr�ation de la clause INCLUDES
@param u une liste de noms (cha�nes de caract�res)
@return une clause INCLUDES compos�e d'une liste d'identificateurs
        correspondant aux noms de u en majuscules *)
and def_includes u =
  let u_cp = (List.map String.capitalize u)
  in Includes (List.map def_ident u_cp)

(** cr�ation de la clause EXTENDS
@param u une liste de noms (cha�nes de caract�res)
@return une clause EXTENDS compos�e d'une liste d'identificateurs
        correspondant aux noms de u *)
and def_extends u = Extends (List.map def_ident u)

(** cr�ation de la clause PROMOTES
@param u une liste de noms (cha�nes de caract�res)
@return une clause PROMOTES compos�e d'une liste d'identificateurs
        correspondant aux noms de u *)
and def_promotes u = Promotes (List.map def_ident u)

(** ??? cr�ation d'une clause DEFINITIONS vide (-> utilit� ?) ??? *)
and def_definitions u = Definitions []

(** cr�ation de la clause VARIABLES
@param u une liste de noms (cha�nes de caract�res)
@return une clause VARIABLES compos�e d'une liste d'identificateurs
        correspondant aux noms de u en minuscules *)
and def_variables u =
  let u_lc = (List.map String.lowercase u)
  in VariablesAbstract (List.map def_ident u_lc)

(** cr�ation de l'INVARIANT
@param u une liste de pr�dicats
@return une clause INVARIANT compos�e de u *)
and def_invariant u = Invariant u

(** cr�ation de la substitution d'initialisation
@param u une liste de noms (cha�nes de caract�res)
@return dans la clause INITIALISATION renvoy�e, on affecte en parall�le
        � chaque variable la valeur ensemble vide, une variable �tant
        d�termin�e par l'identificateur d'un nom de la liste u *)
and def_initialisation_empty x =
  let rec empty_sub u =
    match u with
	[] -> []
      | v::w -> Sub ((Val (Var v)),Empty_set)::empty_sub w
  in Initialisation (Parallel (empty_sub x))

(** cr�ation des OPERATIONS
@param u une liste de noms (cha�nes de caract�res)
@return dans la clause OPERATIONS renvoy�e, chaque op�ration est
        d�sign�e par l'identificateur d'un nom de la liste u
        et est d�finie par une substitution sans effet (SKIP),
        sans pr�-condition, sans param�tre ni valeur de retour *)
and def_operations u = 
  let def_op v = Oper([],Var v ,[],Pre [],SKIP)
  in Operations(List.map def_op u)
;;

(** cr�ation d'une machine B vide *)
let new_machine =
  Machine (
    (def_ident "Vide", []),
    [ Sets []; Uses []; Extends []; Includes []; Promotes [];
      Definitions []; VariablesAbstract []; Invariant [];
      Initialisation(Parallel []); Operations [] ] )
;;

