(* Copyright (C) 1998 Ian Zimmerman <itz@transbay.net> *)
(* $Id: expect_p4.ml,v 1.1 2001/04/27 11:00:12 lefessan Exp $ *)

type conditions =
    Eof
  | Exact of string
  | Regexp of string

let lexer = Genlex.make_lexer ["*"; "=>"; "/"; "Eof"]

let rec parse_condition = parser
    [< 'Genlex.Kwd k when k = "Eof" >] -> Eof
  | [< 'Genlex.String s >] -> Exact(s)
  | [< 'Genlex.Kwd k1 when k1 = "/"; 'Genlex.String s;
     'Genlex.Kwd k2 when k2 = "/" >] -> Regexp(s)

and parse_triple = parser
    [< 'Genlex.Ident spawn_id; 'Genlex.Kwd k1 when k1 = "*";
     condition = parse_condition; 'Genlex.Kwd k2 when k2 = "=>";
     'Genlex.Ident action >] -> (spawn_id, condition, action)

and parse_top = parser
    [< triples = parse_triples None >] -> triples

and parse_triples opt =
  match opt with
    None ->
      (parser
          [< _ = Stream.empty >] -> []
        | [< triple = parse_triple; rest = parse_triples (Some triple) >] ->
            rest)
  | Some(t) ->
      (parser
          [< _ = Stream.empty >] -> [t]
        | [< triple = parse_triple; rest = parse_triples (Some triple) >] ->
            t :: rest)

let expansion_head =
  "let clauses = [ \n"
and expansion_joint =
  "] in match (Expect_lib.Expect.expect clauses) with \n"

let expand_clause_decl n (id, condition, _) =
  (if n = 0 then "" else "; ") 	^ "( " ^
  (string_of_int n) 		^ ", " ^
  id				^ ", " ^
  (match condition with
    Eof -> "Expect_lib.Expect.expect.Eof "
  | Exact(s) -> "Expect_lib.Expect.Exact( \"" ^
      (String.escaped s) ^ "\" )"
  | Regexp(s) -> "Expect_lib.Expect.Regexp( \"" ^
      (String.escaped s) ^ "\")") ^ " )\n"

let expand_clause_act n (id ,_, action) =
  (if n = 0 then "" else "| ")	^ " ( " ^
  (string_of_int n)		^ " , " ^
  id				^ " ,_) -> " ^
  action			^ " ( " ^
  (string_of_int n)		^ " , " ^
  id				^ " )\n"

let rec expand_clause_decls n =
  function
      [] -> ""
    | (t :: triples) ->
        (expand_clause_decl n t) ^
        (expand_clause_decls (n+1) triples)

let rec expand_clause_acts n =
  function
      [] -> "| _ -> raise Match_failure\n"
    | (t :: triples) ->
        (expand_clause_act n t) ^
        (expand_clause_acts (n+1) triples)

let p4_expander = 
  function
      false -> raise Parsing.Parse_error
    | true -> function s ->
        let triples = parse_top (lexer (Stream.of_string s)) in
        expansion_head ^
        (expand_clause_decls 0 triples) ^
        expansion_joint ^
        (expand_clause_acts 0 triples)

let _ = Quotation.add "expect" (Quotation.ExStr p4_expander)
