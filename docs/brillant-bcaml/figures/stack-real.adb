package body stack is
--# invariant stack_top >= 0 
--# and stack_top <= stack_size

  the_stack: array (1..Stack_size) of natural;
  stack_top: 0..Stack_size;

  procedure push(addval: in  natural) is
  begin
     --# pre stack_top < stack_size
     stack_top := stack_top + 1;
     the_stack(stack_top) := addval;
  end push;

  ...

begin --initialisation 
  stack_top := 0
end stack;
