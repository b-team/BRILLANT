 

Require Import Blib.

Theorem op:
forall Edges NODE Weight be ee graphe weight ww, ( 
 (In (Power_set1 BZ) NODE) -> 
(In (relation NODE NODE) Edges) -> 
(In (total_function Edges BN ) Weight) -> 
(Included graphe Edges) -> 
(Included weight Weight) -> 
((domain weight) = graphe) -> 
(In NODE be) -> 
(In NODE ee) -> 
(NotIn graphe be,ee) -> 
(NotIn graphe ee,be) -> 
(In BN ww)
  -> (
 ((domain Override (weight) ((fun o => ((o=be,ee,ww))))) = (Union graphe {= be,ee =})) ) )
 .
intuition.
Qed.
 