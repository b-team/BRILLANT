(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter1.
Require Import Bchapter2.
Require Import Bfinite_subsets.
Require Import Finite_sets_facts.

(**
  * Infinite and finite sets: definitions
  *)

(**
  Finite set, BBook: section 3.4, p.144, array 1, row 1

  To tell if a set is finite or infinite

  Note/TODO: check the priority of these notations
  *)
Implicit Arguments Finite [U].
Notation "'finite' s" := (Finite s) (at level 6, no associativity): eB_scope.

(**
  Infinite set, BBook: section 3.4, p.144, array 1, row 2
*)
Definition Infinite (S: Type) (s: Ensemble S) := ~(Finite s).
Implicit Arguments Infinite [S].
Notation "'infinite' s" := (Infinite s) (at level 6, no associativity): eB_scope.

(**
  * Infinite and finite sets: validity
  *)

(**
  Finite set, BBook: section 3.4, p.144, array 1, row 1
  *)
Theorem valid_finite: forall (S: Type) (s: Ensemble S), Finite s <-> In (Power_finite s) s.
Proof.
intros S s.
split.

intros.
constructor; [ assumption | intuition ].

intros.
induction H; assumption.
Qed.

(**
  Infinite set, BBook: section 3.4, p.144, array 1, row 2

  The notations here make things more confusing than necessary, IMHO(SC)
*)
Theorem valid_infinite: forall (S: Type) (s: Ensemble S), Infinite s <-> ~(Finite s).
Proof.
intros S s.
split.

intros.
exact H.

intros.
assumption.
Qed.

(**
  * Infinite and finite sets: properties
  *)

(**
  property 3.4.1, BBook: section 3.4, p.144
  *)
Theorem inexhaustible_infinite_sets: forall (S: Type) (s t: Ensemble S),
  Infinite s /\ In (Power_finite s) t -> Setminus s t <> (Empty_set S).
Proof.
intros S s t H.
induction H.
intros H1; apply H.
assert (Included s t).
assert (Same_set _ (Setminus s t) (Empty_set _)).
rewrite H1; intuition.
induction H2.
induction H0.
red; intros x H5.
assert (In t x \/ ~(In t x)); [ apply classic | ].
induction H6.
assumption.
assert (In (Setminus s t) x).
constructor; assumption.
generalize (H2 x H7); intros; contradiction.
apply Finite_downward_closed with t.
induction H0; assumption.
assumption.
Qed.

(** TODO: this theorem (theorem 3.4.1)
Theorem dedekind_half1: forall (S: Type) (s: Ensemble S),
  (exists f:(Ensemble (S*S)), In (total_injection s s) f /\ range f <> s) -> Infinite s.
Proof.
intros S s H.
intros H0.
generalize H; clear H.
apply all_not_not_ex.
intros f.
apply or_not_and.
apply imply_to_or.
intros.
intros H1; apply H1; clear H1.
induction H0.

apply Extensionality_Ensembles; split.
red; intros x H0.
induction H0.
inversion H0.
repeat (induction H).
generalize (H (x,b) H1); intros.
inversion H5; contradiction.
red; intros; contradiction.

apply Extensionality_Ensembles; split.
red; intros z H2.
induction H2.
inversion H2.
repeat (induction H).
generalize (H (x0,b) H3); intros.
inversion H7; assumption.
red; intros z H2.
assert (In (image f A) z \/ ~(In (image f A) z)); [ apply classic | ].
induction H3.
inversion H3.
constructor; exists x0; intuition.
assert (In (image f (Singleton x)) z \/ ~(In (image f (Singleton x)) z)); [ apply classic | ].
induction H4.
constructor; exists x.
induction H4.
induction H4.
induction H4; assumption.
assert (~(In (image f (Add S A x)) z)).
assert (~((In (image f A) z) \/ (In (image f (Singleton x)) z))).
intuition.
intros H6; apply H5.
induction H6.
induction H6.
induction H6.
left; red; apply image_intro with x0; intuition.
right; red; apply image_intro with x0; intuition.
induction H.
induction H; induction H6.
assert (In (image f (Full_set S)) z \/ ~(In (image f (Full_set S)) z)); [ apply classic | ].
induction H9.
induction H9.
constructor; exists x0; intuition.





assert (In (partial_function (Add S A x) (Add S A x)) (inverse f) /\ In (domain (inverse f)) z).
split.
apply Membership_laws_02.
induction H; assumption.
constructor.
apply not_all_not_ex.
intros H6.
assert (forall n, ~(In f (n,z))).
intros n; generalize (H6 n); intros H7.
intros H8; apply H7; constructor; assumption.
induction H.
induction H; induction H8.
induction H0.


induction H2.




induction H.
induction H; induction H6.
assert (In (image (domain_subtraction f (Add S A x)) (Full_set S)) z).
red; apply image_intro with z.

esplit.







Qed.
*)
