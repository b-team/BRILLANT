(*==================================================================================================
  Project : BiCoq3
  Module : egraft.v
  We define here a new operation on terms, named "grafting". This operation is a form of
  substitution that allows for capture of variables; this is done by simply not lifting terms when
  crossing a quantifier.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Oct 2008, Md: Mar 2009
  ================================================================================================*)

Require Export esubst.

(* Grafting function -----------------------------------------------------------------------------*)

Fixpoint Bp_egraft_(d:nat)(i:Ι)(p:Π)(a:Ε){struct p}:Π:=
 match p with
 | ¬p' => ¬(Bp_egraft_ d i p' a)
 | ∀Δ(p') => ∀Δ(Bp_egraft_ (S d) (DB_lift_ d i) p' a)
 | p1⋀p2 => (Bp_egraft_ d i p1 a)⋀(Bp_egraft_ d i p2 a)
 | (p1⇒p2) => (Bp_egraft_ d i p1 a)⇒(Bp_egraft_ d i p2 a)
 | ∐(_) => p
 | e1≡e2 => (Be_egraft_ d i e1 a)≡(Be_egraft_ d i e2 a)
 | e1∈e2 => (Be_egraft_ d i e1 a)∈(Be_egraft_ d i e2 a)
 end
with Be_egraft_(d:nat)(i:Ι)(e a:Ε){struct e}:Ε :=
 match e with
 | Ω => e
 | ↓(e') => ↓(Be_egraft_ d i e' a)
 | ↑(e') => ↑(Be_egraft_ d i e' a)
 | e1↦e2 => (Be_egraft_ d i e1 a)↦(Be_egraft_ d i e2 a)
 | e1×e2 => (Be_egraft_ d i e1 a)×(Be_egraft_ d i e2 a)
 | ∞(_) => e
 | ∂(i') => if DB_le d i' && DB_eq i i' then a else e
 | ⅭΔ(e',p') => ⅭΔ(Be_egraft_ d i e' a,Bp_egraft_ (S d) (DB_lift_ d i) p' a)
 end.
Notation "'〈' i '⇙' a '@Π@' p '〉'" := (Bp_egraft_ 0 i p a).
Notation "'〈' i '⇙' a '@Ε@' e '〉'" := (Be_egraft_ 0 i e a).

Definition Bt_egraft_(d:nat)(i:Ι)(t:Τ)(a:Ε):Τ :=
 match t with be_ e' => Be_egraft_ d i e' a | bp_ p' => Bp_egraft_ d i p' a end.
Notation "'〈' i '⇙' a '@' t '〉'" := (Bt_egraft_ 0 i t a).

(* Theorems for grafting -------------------------------------------------------------------------*)

Theorem Bt_egraft_free_:forall (t:Τ)(i:Ι)(d:nat), Bt_free_ d i t=⊥->
                        forall (a:Ε), Bt_egraft_ d i t a=t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι)(d:nat), Bt_free_ d i t=⊥->
                            forall (a:Ε), Bt_egraft_ d i t a=t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind i d Ht a;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) _ _ Ht a); apply refl_equal);
  try (destruct (orb_false_elim _ _ Ht) as [Ht1 Ht2];
       irewrite (Hind _ (sdepth_l t1 t2) _ _ Ht1 a);
       irewrite (Hind _ (sdepth_r t1 t2) _ _ Ht2 a); apply refl_equal).
  unfold Bt_egraft_,Be_egraft_; unfold Bt_free_,Be_free_ in Ht;
   destruct (DB_le d i'); [rewrite Ht | ]; apply refl_equal.
Qed.

Theorem Bt_egraft_free:forall (t:Τ)(i:Ι), (i∖t)->forall (a:Ε), 〈i⇙a@t〉=t.
Proof.
 intros t i Ht; apply Bt_egraft_free_; apply Ht.
Qed.

Theorem Bt_egraft_ground_:forall (t:Τ)(i:Ι)(a:Ε)(d:nat), (forall (n:nat), ¡(0,n)∖a)->
                          Bt_egraft_ d i t a=Bt_esubst_ d i t a.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι)(a:Ε)(d:nat), (forall (n:nat), ¡(0,n)∖a)->
                            Bt_egraft_ d i t a=Bt_esubst_ d i t a));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i a d Ha;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) i _ d Ha); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) i _ d Ha); irewrite (Hind _ (sdepth_r t1 t2) i _ d Ha);
       apply refl_equal).
  irewrite (Hind _ (sdepth_l t1 t2) i _ d Ha);
   irewrite (Hind _ (sdepth_r t1 t2) (DB_lift_ d i) a (S d) Ha);
   cut (forall (n:nat), Bt_free_ d ¡(0,n) a=⊥);
   [intros H'a; irewrite (Bt_lift_ground0_ a d H'a); apply refl_equal |
    intros n; apply (Bt_free_free_ a ¡(0,n) _ _ (le_O_n d) (Ha _))].
  irewrite (Hind _ (sdepth t) (DB_lift_ d i) a (S d) Ha);
   cut (forall (n:nat), Bt_free_ d ¡(0,n) a=⊥);
   [intros H'a; irewrite (Bt_lift_ground0_ a d H'a); apply refl_equal |
    intros n; apply (Bt_free_free_ a ¡(0,n) _ _ (le_O_n d) (Ha _))].
Qed.

Theorem Bt_egraft_ground:forall (t:Τ)(i:Ι)(a:Ε), (forall (n:nat), ¡(0,n)∖a)->〈i⇙a@t〉=〈i←a@t〉.
Proof.
 intros t i a Hg; apply Bt_egraft_ground_; apply Hg.
Qed.

Theorem Bt_egraft_esubst_:forall (t:Τ)(i1 i2:Ι)(e:Ε)(d:nat), ≤Ι(d,i1)->Bt_free_ d i1 t=⊥->
                          Bt_egraft_ d i1 (Bt_esubst_ d i2 t ∂(i1)) e=Bt_egraft_ d i2 t e.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i1 i2:Ι)(e:Ε)(d:nat), ≤Ι(d,i1)->Bt_free_ d i1 t=⊥->
                            Bt_egraft_ d i1 (Bt_esubst_ d i2 t ∂(i1)) e=Bt_egraft_ d i2 t e));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind i1 i2 e d Hdi1 Ht;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) _ i2 e _ Hdi1 Ht); apply refl_equal);
  try (destruct (orb_false_elim _ _ Ht) as [Ht1 Ht2];
       irewrite (Hind _ (sdepth_l t1 t2) _ i2 e _ Hdi1 Ht1);
       irewrite (Hind _ (sdepth_r t1 t2) _ i2 e _ Hdi1 Ht2); apply refl_equal).
  unfold Bt_esubst_,Be_esubst_; case_eq (DB_le d i'); intros Hdi'.
   case_eq (DB_eq i2 i'); intros Hi2i'; simpl; DB_simpl; rewrite Hdi'; rewrite Hi2i'.
    simpl; rewrite Hdi1; apply refl_equal.
    case_eq (DB_eq i1 i'); intros Hi1i'; reflect_in DB_eq_imp Hi1i'; [ | apply refl_equal].
     rewrite Hi1i' in *; simpl in Ht; rewrite Hdi' in Ht; DB_simpl; inversion Ht.
   simpl; rewrite Hdi'; apply refl_equal.
  destruct (orb_false_elim _ _ Ht) as [Ht1 Ht2]; fold Bp_free_ Be_free_ in Ht1,Ht2.
   irewrite (Hind _ (sdepth_l t1 t2) _ i2 e _ Hdi1 Ht1); rewrite <- (DB_le_lift d i1) in Hdi1;
   irewrite (Hind _ (sdepth_r t1 t2) _ (DB_lift_ d i2) e _ Hdi1 Ht2); apply refl_equal.
  rewrite <- (DB_le_lift d i1) in Hdi1;
   irewrite (Hind _ (sdepth t) _ (DB_lift_ d i2) e _ Hdi1 Ht); apply refl_equal.
Qed.

Theorem Bt_egraft_esubst:forall (t:Τ)(i1 i2:Ι)(e:Ε), (i1∖t)->〈i1⇙e@〈i2←∂(i1)@t〉〉=〈i2⇙e@t〉.
Proof.
 intros t i1 i2 e Ht; apply Bt_egraft_esubst_; [apply DB_le_0 | apply Ht].
Qed.

(*================================================================================================*)