open Expect_lib
open Unix

open Bbatch_types
open Bbatch_answer

exception Bbatch_error of string

let debug message = 
  assert (prerr_endline message; true)


(* Add verifications for the paths given *)
let make_b4freerc b4free_path project_path =
  let template = open_in "B4free.rc.template" in
  let template_size = in_channel_length template in
  let b4free_template = String.create template_size in
    really_input template b4free_template 0 template_size;
    close_in template;
    let path_replaced = Str.global_replace (Str.regexp "B4free_path") b4free_path b4free_template in
    let project_path_replaced = Str.global_replace (Str.regexp "B4free_project_path") project_path path_replaced in
      project_path_replaced


(* Useless ? *)
let read_until_prompt spawn_id =
  
  let accumulated = Buffer.create 8092 in
(* \\(.*\013\n\\)*bbatch>\013\n *)
(* bbatch>\013$ *)
  let rec aux_read () =
  let (i,the_id,wait_prompt) = Expect.expect 
    [(0, spawn_id, Expect.Regexp("\\(.*\013\n\\)*bbatch>\013\n")); 
     (1, spawn_id, Expect.Regexp("End of interpretation ([0-9]+ lines)\013$"));
     (2, spawn_id, Expect.Regexp(".+$"))] 
  in
    match i with
      | 0 -> begin
	  Buffer.add_string accumulated (Spawn.matched_string the_id);
	  (false, Buffer.contents accumulated)
	end
      | 1 -> begin
	  Buffer.add_string accumulated (Spawn.matched_string the_id);
	  (true, Buffer.contents accumulated)
	end
      | 2 -> begin
	  Buffer.add_string accumulated (Spawn.matched_string the_id);
	  aux_read ()	  
	end
      | _ -> assert false
  in

    aux_read ()


let bbatch_interaction spawn_id =
  Expect.trace := true;
  let term_desc = Spawn.to_descr spawn_id in
  let term_io = Unix.tcgetattr term_desc in
  let finished = ref false in
    begin
      (*      print_endline (Terminal.string_of_termio term_io); *)
      term_io.c_echo <- false;
      Unix.tcsetattr term_desc TCSANOW term_io;
      (*      print_endline (Terminal.string_of_termio term_io); *)
      while not !finished do
        let (end_status, communication) = read_until_prompt spawn_id in
          begin
            prerr_endline (String.escaped communication);
            prerr_endline "####################################";
            prerr_endline communication;
            finished := end_status
          end;
          if not !finished then begin
            prerr_endline "BBaaaaatch>>>";
            let what_read = read_line () in Spawn.send spawn_id (what_read ^ "\n")
          end
      done
    end


let launch_bbatch bbatch_path project_path =
  let b4free_path = Filename.dirname bbatch_path in
  let b4free_rc = make_b4freerc b4free_path project_path in
  let b4free_rcfile = open_out (project_path ^ "/B4free.rc") in
    output b4free_rcfile b4free_rc 0 (String.length b4free_rc);
    close_out b4free_rcfile;
    let prog_name = bbatch_path 
    and prog_arg = "-r=" ^ project_path ^ "/B4free.rc" in
      Spawn.spawn_process prog_name [| prog_name; prog_arg |] [] 


let tune_session spawn_id = 
  Spawn.trace := true;
  Expect.trace := true;
  let term_desc = Spawn.to_descr spawn_id in
  let term_io = Unix.tcgetattr term_desc in
    begin
      term_io.c_echo <- false;
      Unix.tcsetattr term_desc TCSANOW term_io
    end


type frame_state = 
  | In_projects
  | In_components
  | In_proofs

let string_of_state s =
  match s with
  | In_projects -> "In_projects"
  | In_components -> "In_components"
  | In_proofs -> "In_proofs"
      

class bbatch_session project_path =

  let session = launch_bbatch "/home/scolin/usr/B4free-3.2/bbatch" project_path in
  let () = tune_session session in

object(self)
  val path = project_path
  val archives_path = project_path ^ "/Archives"
  val spawn_id = session
  val mutable frame_state = In_projects

  method path = path
  method archives_path = archives_path

  method private send_command command = 
    Spawn.send spawn_id (command ^ "\n")

  initializer begin
    self#read_beginning;
    self#change_directory path
  end


  method read_beginning = 
    let _ = Expect.expect [(0, spawn_id, Expect.Exact(beginning_answer))] in
    ()


  (** cd *)
  method change_directory directory = 
    let command = "cd " ^ directory in
    let () = self#send_command command in 
    let _ = Expect.expect [(0, spawn_id, Expect.Regexp(change_directory_answer))] in
      ()


  (** spl *)
  method show_projects_list =
    if frame_state <> In_projects then invalid_arg "show_projects_list" else
      let command = "spl" in
      let () = self#send_command command in
      let (i, the_id, wait_prompt) = Expect.expect [(0, spawn_id, Expect.Regexp(show_projects_list_answer))] in
      let answer = Spawn.matched_string the_id 
      and b = Spawn.group_beginning the_id 1
      and e = Spawn.group_end the_id 1 in
      let projects = String.sub answer b (e-b) in
      let without_spaces = Str.global_replace (Str.regexp "[ \t]+") "" projects in
      let separated = Str.split (Str.regexp nl) without_spaces in
	separated
	

  method archive_template name filename kind =
    if frame_state <> In_projects then invalid_arg "archive_template" else
      let command = ("arc " ^ name ^ " " ^ filename ^ " " ^ (string_of_int kind)) in 
      let () = self#send_command command in
      let (i, the_id, wait_prompt) = Expect.expect [(0, spawn_id, Expect.Regexp(archive_answer))] in
      let answer = Spawn.matched_string the_id 
      and b = Spawn.group_beginning the_id 1
      and e = Spawn.group_end the_id 1 in
      let files = String.sub answer b (e-b) in
      let separated = Str.split (Str.regexp nl) files in
	separated


  method archive_source name filename =
    self#archive_template name filename 0

  method archive_proofs name filename =
    self#archive_template name filename 2

  method archive_full name filename =
    self#archive_template name filename 1

  (** arc *)
  method archive name = 
    let sources = archives_path ^ "/" ^ name ^ "-sources.tar" 
    and proofs = archives_path ^ "/" ^ name ^ "-proofs.tar"
    and full = archives_path ^ "/" ^ name ^ "-full.tar" in
    let answer_sources = self#archive_source name sources in
    let answer_proofs = self#archive_proofs name proofs in
    let answer_full = self#archive_full name full in
      (answer_sources, answer_proofs, answer_full)


  method restore_template filename kind name = 
    if frame_state <> In_projects then invalid_arg "archive_template" else
      let command = ("res " ^ filename ^ " " ^ (string_of_int kind) ^ " " ^ name) in 
      let () = self#send_command command in
      let (i, the_id, wait_prompt) = Expect.expect [(0, spawn_id, Expect.Regexp(restore_answer))] in
      let answer = Spawn.matched_string the_id 
      and b = Spawn.group_beginning the_id 1
      and e = Spawn.group_end the_id 1 in
      let files = String.sub answer b (e-b) in
      let separated = Str.split (Str.regexp nl) files in
	separated
 

  method restore_source filename name =
    self#restore_template filename 0 name

  method restore_proofs filename name =
    self#restore_template filename 2 name

  method restore_full filename name =
    self#restore_template filename 1 name

(*SC: faire attention au dossier de restauration (local par défaut ?) *)
  (** res *)
  method restore path_tar name = 
    (* By default, do a full restoration *)
    self#restore_full path_tar name


  (** rp *)
  method remove_project name = 
    if frame_state <> In_projects then invalid_arg "remove_project" else
      let command = "rp " ^ name in
      let () = self#send_command command in
      let _ = Expect.expect [(0, spawn_id, Expect.Regexp(remove_project_answer))] in
	()
	  
    

  (** ip *)
  method infos_project (name : string) = ()
  
  (** crp *)
  method create_project name =  
    if frame_state <> In_projects then invalid_arg "create_project" else
      (*SC: system-specific separator, beware if we port under Windows *)
      let final_path = path ^ "/" ^ name in
      let pdb = final_path ^ "/" ^ "bdp" in
      let lang = final_path ^ "/" ^ "exec" in
      let () = Unix.mkdir final_path 0o755 in
      let () = Unix.mkdir pdb 0o755 in
      let () = Unix.mkdir lang 0o755 in
      let command = "crp " ^ name ^ " " ^ pdb ^ " " ^ lang in
      let () = self#send_command command in
      let _ = Expect.expect [(0, spawn_id, Expect.Regexp(create_project_answer))] in
	()


  (** op *)
  method open_project name =
    if frame_state <> In_projects then invalid_arg "open_project" else
      let command = "op " ^ name in 
      let () = self#send_command command in
      let _ = Expect.expect [(0, spawn_id, Expect.Regexp(open_project_answer))] in
	frame_state <- In_components
  
    


  (** clp *)
  method close_project = 
    if frame_state <> In_components then invalid_arg "close_project" else
      let command = "clp" in 
      let () = self#send_command command in
      let _ = Expect.expect [(0, spawn_id, Expect.Regexp(close_project_answer))] in
	frame_state <- In_projects

  (** sml *)
  method show_machines_list =  (* numerous options, irrelevant for GUI I guess except "own" ? *)
    if frame_state <> In_components then invalid_arg "show_machines_list" else
      let command = "sml" in
      let () = self#send_command command in
      let (i, the_id, wait_prompt) = Expect.expect [(0, spawn_id, Expect.Regexp(show_machines_list_answer))] in
      let answer = Spawn.matched_string the_id 
      and b = Spawn.group_beginning the_id 1
      and e = Spawn.group_end the_id 1 in
      let projects = String.sub answer b (e-b) in
      let without_spaces = Str.global_replace (Str.regexp "[ \t]+") "" projects in
      let separated = Str.split (Str.regexp nl) without_spaces in
	separated
 

    (** af *)
  method add_file (name : string) = () (* full path ? project... *)
    (** rc *)
  method remove_component (name : string) = ()

    (** s *)
  method status name = 
    if frame_state <> In_components then invalid_arg "status" else
      let command = "s " ^ name in
      let () = self#send_command command in
      let (i, the_id, wait_prompt) = Expect.expect [(0, spawn_id, Expect.Regexp(status_answer name))] in
	status_analyse name (Spawn.matched_string the_id)


  (** sg *)
  method status_global =
    if frame_state <> In_components then invalid_arg "status_global" else
      let command = "sg" in
      let () = self#send_command command in
      let (i, the_id, wait_prompt) = 
	Expect.expect 
	  [(0, spawn_id, Expect.Regexp(status_global_answer)); 
	   (1, spawn_id, Expect.Regexp(status_global_alt_answer));
	   (2, spawn_id, Expect.Regexp(catch_all))
	  ] 
      in 
	match i with
	  | 0 -> status_global_analyse (Spawn.matched_string the_id)
	  | 1 -> status_global_alt_analyse (Spawn.matched_string the_id)
	  | 2 -> raise (Bbatch_error(Spawn.matched_string the_id))
	  | _ -> assert false

  (** ic *)
  method infos_component (name : string) = ()

  (** t *)
  method typecheck (name : string) = 
    if frame_state <> In_components then invalid_arg "typecheck" else
      let command = "t " ^ name in
      let () = self#send_command command in
      let (i, the_id, wait_prompt) = Expect.expect [(0, spawn_id, Expect.Regexp(typecheck_answer name)); (1, spawn_id, Expect.Regexp(typecheck_error_1 name))] in
	match i with
	  | 0 -> Spawn.matched_string the_id
	  | 1 -> Spawn.matched_string the_id
	  | _ -> assert false

    (** po *)
  method pogenerate (name : string) = () (* 0 full, 1 differential *)
    (** pr *)
  method prove (name : string) (force : int) = () (* -3 to 3, good *)
    (** u *)
  method unprove (name : string) = ()
    (** b *)
  method browse (name : string) = () (* proof session, see later *)

(** *)

end








