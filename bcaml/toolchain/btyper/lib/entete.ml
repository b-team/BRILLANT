open Unix

let date = 
 let tm = Unix.localtime (time ()) in
 let hh = Printf.sprintf "%02d" tm.tm_hour in
 let mm = Printf.sprintf "%02d" tm.tm_min in
 let ss = Printf.sprintf "%02d" tm.tm_sec in
 let j = Printf.sprintf "%02d" tm.tm_mday in
 let m = Printf.sprintf "%02d" ( 1 + tm.tm_mon) in
 let a = string_of_int (1900 + tm.tm_year) in
 j^"-"^m^"-"^a^"  "^hh^":"^mm^":"^ss
;;


let entete_ml = 
"(*************************************************************)\n"^
"(*                PROJET BCAML                               *)\n"^
"(*                                                           *)\n"^
"(* http://www3.inrets.fr/Public/ESTAS/Mariano.Georges/Bcaml/ *)\n"^
"(*                                                           *)\n"^
"(*                   IRIT                                    *)\n"^
"(*                                                           *)\n"^
"(*   source genere le "     ^date^     "                   *)\n"^
"(*************************************************************)\n\n"

