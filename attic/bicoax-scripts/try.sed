
/^\(MACHINE\|MODEL\)/ {
  s/^[ ]*\(MACHINE\|MODEL\)[ ]*\([^ ]\+\)/Theorem \2: forall (U V W: Type)/
  h
}

/^SETS/ {
  s/^[ ]*SETS[ ]*\([^ ]\+\)/ \1/
  s/;/ /g
  s/\<\([a-zA-Z_]\+\)\>/(\1: Ensemble X)/g
  H
}

/^CONSTANTS/ {
  s/^[ ]*CONSTANTS[ ]*\([^ ]\+\)/ \1/
  s/,/ /g
  s/\<\([a-zA-Z_]\+\)\>/(\1: Ensemble X)/g
  H
}

/^PROPERTIES/ {
  s/^[ ]*PROPERTIES[ ]*\(.*\)$/, %(\1) ->/
  H
}

/^ASSERTIONS/ {
  s/^[ ]*ASSERTIONS[ ]*\(.\+\)/ (\1)/
  H
}

/^END/ {
  s/^[ ]*END[ ]*/./
  H
  x
  p
}
