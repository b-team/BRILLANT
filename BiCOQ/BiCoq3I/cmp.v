(*==================================================================================================
  Project : BiCoq3
  Module : cmp.v
  This module is dedicated to a number of lemmas, some of which are "technical", related to the
  interactions between lifting, abstraction, application and substitution. The main interesting
  results are:
  - substitution is equivalent to abstraction followed by application,
  - alpha-renaming reduces to identity.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Jul 2008, Md: Mar 2009
  ================================================================================================*)

Require Export abstr.
Require Export apply.
Require Export esubst.

(* Substitution as the composition of abstraction and application --------------------------------*)
(* In our theory, we don't need to define substitution, as it is just a composition of abstraction
   and application. Note that in classical approaches representing the application as a term, it is
   easy to "match" (λx.T1)@T2, to obtain T1, T2 and then transform the redex in [x:=T2]T1. In our
   case, (λx.T1)@T2 is not a term but a function whose evaluation yields a new term T3, providing
   directly the result - but deducing T1 and T2 from T3 can, on the other hand, be difficult... *)

Lemma Bt_esubst_dec_:forall (t:Τ)(a:Ε)(i:Ι)(d:nat),
                       Bt_app_ d (Bt_abstr_ d i t) a=Bt_esubst_ d i t a.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (a:Ε)(i:Ι)(d:nat),
                            Bt_app_ d (Bt_abstr_ d i t) a=Bt_esubst_ d i t a));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind a i d;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) a i d); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) a i d); irewrite (Hind _ (sdepth_r t1 t2) a i d);
       apply refl_equal).
  unfold Bt_abstr_,Be_abstr_; case_eq (DB_le d i'); intros Hdi'; simpl in *; rewrite Hdi' in *.
   case_eq (DB_eq i i'); intros Hii'; simpl; nat_simpl; [apply refl_equal | ].
    destruct i' as [[| u'] n']; unfold DB_lift_; nat_simpl; bool_simpl; simpl in *.
     rewrite Hdi'; unfold Bt_esubst_,Be_esubst_,Bt_app_,Be_app_; reflect_in nat_le_imp Hdi';
      rewrite (imppt2 nat_le_imp _ _ (le_S _ _ Hdi')); case_eq (nat_eq d (S n')); intros Hdn'.
      reflect_in nat_eq_imp Hdn'; rewrite Hdn' in *; destruct (le_Sn_n _ Hdi').
      apply refl_equal.
     apply refl_equal.
   destruct i' as [[| u'] n']; simpl in *; [repeat (rewrite Hdi') | ]; apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) a i d);
   irewrite (Hind _ (sdepth_r t1 t2) (Be_lift_ d a) (DB_lift_ d i) (S d)); apply refl_equal.
  irewrite (Hind _ (sdepth t) (Be_lift_ d a) (DB_lift_ d i) (S d)); apply refl_equal.
Qed.

Theorem Bt_esubst_forall:forall (p:Π)(i:Ι)(a:Ε), 〈i←a@p〉=(∀(i∙p)@a|!∀(i∙p)).
Proof.
 intros p i a; apply sym_equal; unfold Bp_forall; unfold B_forapp; apply (Bt_esubst_dec_ p a i 0).
Qed.

Theorem Bt_app_abstr_id:forall (t:Τ)(i:Ι)(d:nat), Bt_app_ d (Bt_abstr_ d i t) ∂(i)=t.
Proof.
 intros t i d; rewrite (Bt_esubst_dec_ t ∂(i) i d); rewrite Bt_esubst_id_; apply refl_equal.
Qed.

(* Alpha-renaming --------------------------------------------------------------------------------*)
(* We prove here that the De Bruijn representation is alpha-quotiented: two terms are alpha
   equivalent iff they are equal. *)

Lemma Bt_abstr_esubst_:forall (t:Τ)(i i':Ι)(d:nat), ≤Ι(d,i)->Bt_free_ d i t=⊥->
                       Bt_abstr_ d i (Bt_esubst_ d i' t ∂(i))=Bt_abstr_ d i' t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i i':Ι)(d:nat), ≤Ι(d,i)->Bt_free_ d i t=⊥->
                            Bt_abstr_ d i (Bt_esubst_ d i' t ∂(i))=Bt_abstr_ d i' t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i'' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i i' d Hdi Ht;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) _ i' _ Hdi Ht); apply refl_equal);
  try (destruct (orb_false_elim _ _ Ht) as [Ht1 Ht2];
       irewrite (Hind _ (sdepth_l t1 t2) _ i' _ Hdi Ht1);
       irewrite (Hind _ (sdepth_r t1 t2) _ i' _ Hdi Ht2); apply refl_equal).
  case_eq (DB_eq i' i''); intros Hi'i''; simpl; DB_simpl.
   case_eq (DB_le d i''); intros Hdi''; simpl;
   [rewrite Hdi'' in *; rewrite Hdi in *; DB_simpl | rewrite Ht]; apply refl_equal.
   case_eq (DB_le d i''); intros Hdi''; simpl; rewrite Ht; apply refl_equal.
  destruct (orb_false_elim _ _ Ht) as [Ht1 Ht2]; irewrite (Hind _ (sdepth_l t1 t2) _ i' _ Hdi Ht1);
   rewrite <- (DB_le_lift d i) in Hdi;
   irewrite (Hind _ (sdepth_r t1 t2) _ (DB_lift_ d i') _ Hdi Ht2); apply refl_equal.
  rewrite <- (DB_le_lift d i) in Hdi; irewrite (Hind _ (sdepth t) _ (DB_lift_ d i') _ Hdi Ht);
   apply refl_equal.
Qed.

Theorem Bp_forall_alpha:forall (p:Π)(i1 i2:Ι), (i2∖p)->∀(i1∙p)=∀(i2∙〈i1←∂(i2)@Π@p〉).
Proof.
 intros p i1 i2 Hp; unfold Bp_forall; irewrite (Bt_abstr_esubst_ p i2 i1 0 (DB_le_0 _ ) Hp);
  apply refl_equal.
Qed.

Theorem Be_cmpset_alpha:forall (p:Π)(e:Ε)(i1 i2:Ι), (i2∖p)->{i1∊e∣p}={i2∊e∣〈i1←∂(i2)@Π@p〉}.
Proof.
 intros p e i1 i2 Hp; unfold Be_cmpset; irewrite (Bt_abstr_esubst_ p i2 i1 0 (DB_le_0 _ ) Hp);
  apply refl_equal.
Qed.

(* Reverse abstraction ---------------------------------------------------------------------------*)

Lemma Bt_abstr_app_:forall (t:Τ)(i:Ι)(d:nat), ≤Ι(d,i)->
                    Bt_free_ (S d) (DB_lift_ d i) t=⊥->Bt_abstr_ d i (Bt_app_ d t ∂(i))=t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι)(d:nat), ≤Ι(d,i)->
                            Bt_free_ (S d) (DB_lift_ d i) t=⊥->Bt_abstr_ d i (Bt_app_ d t ∂(i))=t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [[| u'] n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind i d Hc Hit;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) _ _ Hc Hit); apply refl_equal);
  try (destruct (orb_false_elim _ _ Hit) as [Hit1 Hit2];
       irewrite (Hind _ (sdepth_l t1 t2) _ _ Hc Hit1);
       irewrite (Hind _ (sdepth_r t1 t2) _ _ Hc Hit2); apply refl_equal).
  simpl; case_eq (nat_le d n'); intros Hdn'.
   case_eq (nat_eq d n'); intros H'dn'.
    unfold Be_abstr_; case_eq (DB_le d i); intros Hdi; simpl in *.
     DB_simpl; reflect_in nat_eq_imp H'dn'; rewrite H'dn'; apply refl_equal.
     reflect_in nat_eq_imp H'dn'; rewrite <- H'dn' in *; destruct i as [[ |u] n]; simpl in Hc;
      nat_simpl; simpl in Hdi; [rewrite Hc in Hdi | ]; inversion Hdi.
    destruct n' as [| n'];
    [reflect_in nat_le_imp Hdn'; inversion Hdn'; rewrite H in *; nat_simpl; inversion H'dn' | ].
     unfold pred in *; unfold Be_abstr_; reflect_in nat_le_imp Hdn'; reflect_in nat_eq_imp H'dn';
      destruct (le_lt_or_eq _ _ Hdn') as [H''dn' | H''dn']; [ | destruct (H'dn' H''dn')].
      simpl in *; nat_simpl; rewrite (imppt2 nat_le_imp _ _ (le_S_n _ _ H''dn')) in *; simpl in *;
       case_eq (DB_eq i ¡(0,n')); intros Hin'; reflect_in DB_eq_imp Hin'; [ | apply refl_equal].
       rewrite Hin' in Hit; simpl in Hit;
        rewrite (imppt2 nat_le_imp _ _ (le_S_n _ _ H''dn')) in Hit; nat_simpl; inversion Hit.
   simpl; rewrite Hdn'; apply refl_equal.
  simpl; case_eq (DB_eq i ¡(S u',n')); intros Hiu'n'; [ | apply refl_equal];
   reflect_in DB_eq_imp Hiu'n'; rewrite Hiu'n' in *; simpl in Hit; nat_simpl; inversion Hit.
 destruct (orb_false_elim _ _ Hit) as [Hit1 Hit2]; irewrite (Hind _ (sdepth_l t1 t2) _ _ Hc Hit1);
  rewrite <- DB_le_lift in Hc; irewrite (Hind _ (sdepth_r t1 t2) _ _ Hc Hit2); apply refl_equal.
 rewrite <- DB_le_lift in Hc; irewrite (Hind _ (sdepth t) _ _ Hc Hit); apply refl_equal.
Qed.

Theorem Bp_forall_inv:forall (p:Π)(i:Ι), (↥(i)∖p)->∀Δ(p)=∀(i∙Bp_app_ 0 p ∂(i)).
Proof.
 intros p i Hip; rewrite (Bt_free_lift_S p i 0) in Hip; unfold Bp_forall.
  irewrite (Bt_abstr_app_ p _ _ (DB_le_0 _) Hip); apply refl_equal.
Qed.

Theorem Be_cmpset_inv:forall (p:Π)(e:Ε)(i:Ι), (↥(i)∖p)->ⅭΔ(e,p)={i∊e∣Bp_app_ 0 p ∂(i)}.
Proof.
 intros p e i Hip; rewrite (Bt_free_lift_S p i 0) in Hip; unfold Be_cmpset;
  irewrite (Bt_abstr_app_ p _ _ (DB_le_0 _ ) Hip); apply refl_equal.
Qed.

(* Commutation of abstraction and substitution ---------------------------------------------------*)

Theorem Bt_abstr_esubst:forall (t:Τ)(a:Ε)(is ia:Ι)(d:nat), is<>ia->Bt_free_ d ia a=⊥->
                        Bt_esubst_ (S d) (DB_lift_ d is) (Bt_abstr_ d ia t) (Be_lift_ d a)=
                        Bt_abstr_ d ia (Bt_esubst_ d is t a).
Proof.
 apply (wf_term (fun (t:Τ)=>
        forall (a:Ε)(is ia:Ι)(d:nat), is<>ia->Bt_free_ d ia a=⊥->
        Bt_esubst_ (S d) (DB_lift_ d is) (Bt_abstr_ d ia t) (Be_lift_ d a)=
        Bt_abstr_ d ia (Bt_esubst_ d is t a)));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl;
  intros Hind a is ia d Hisia Hiaa; try (apply refl_equal);
  try (irewrite (Hind _ (sdepth t) _ _ _ _ Hisia Hiaa); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) _ _ _ _ Hisia Hiaa);
       irewrite (Hind _ (sdepth_r t1 t2) _ _ _ _ Hisia Hiaa); apply refl_equal).
  case_eq (DB_eq ia i'); intros Hiai'; DB_simpl; case_eq (DB_eq is i'); intros Hisi'; bool_simpl.
   reflect_in DB_eq_imp Hiai'; rewrite Hiai' in Hisia; reflect_in DB_eq_imp Hisi';
    rewrite Hisi' in Hisia; destruct Hisia; apply refl_equal.
   reflect_in DB_eq_imp Hiai'; rewrite Hiai' in *; case_eq (DB_le d i'); intros Hdi'; simpl in *;
    nat_simpl; bool_simpl; DB_simpl; rewrite Hdi'; apply refl_equal.
   DB_simpl; rewrite Hisi'; case_eq (DB_le d i'); intros Hdi'; simpl in *;
   [apply (sym_equal (Bt_abstr_lift_ a _ _ Hiaa)) | rewrite Hdi'; apply refl_equal].
   DB_simpl; rewrite Hisi'; simpl in *; rewrite Hiai'; bool_simpl; apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) _ _ _ _ Hisia Hiaa);
   assert (H'isia:DB_lift_ d is<>DB_lift_ d ia).
   reflect DB_eq_imp; DB_simpl; reflect DB_eq_imp; apply Hisia.
  srewrite_in (sym_equal (Bt_lift_free_ a ia d)) Hiaa;
   irewrite (Hind _ (sdepth_r t1 t2) _ _ _ _ H'isia Hiaa); apply refl_equal.
  assert (H'isia:DB_lift_ d is<>DB_lift_ d ia).
   reflect DB_eq_imp; DB_simpl; reflect DB_eq_imp; apply Hisia.
  srewrite_in (sym_equal (Bt_lift_free_ a ia d)) Hiaa;
   irewrite (Hind _ (sdepth t) _ _ _ _ H'isia Hiaa); apply refl_equal.
Qed.

Theorem Bp_forall_esubst_diff:forall (p:Π)(a:Ε)(i i':Ι),
                              i'<>i->(i∖a)->〈i'←a@Π@∀(i∙p)〉=∀(i∙〈i'←a@Π@p〉).
Proof.
 intros p a i i' Hi'i Hia; simpl; irewrite (Bt_abstr_esubst p a i' _ _ Hi'i Hia); apply refl_equal.
Qed.

Theorem Be_cmpset_esubst_diff:forall (p:Π)(e a:Ε)(i i':Ι),
                              i'<>i->(i∖a)->〈i'←a@Ε@{i∊e∣p}〉={i∊〈i'←a@Ε@e〉∣〈i'←a@Π@p〉}.
Proof.
 intros p e a i i' Hi'i Hia; simpl; irewrite (Bt_abstr_esubst p a i' _ _ Hi'i Hia);
  apply refl_equal.
Qed.

(*================================================================================================*)