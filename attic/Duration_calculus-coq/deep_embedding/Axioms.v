(*
    Copyright (c) 2002-2009 Samuel Colin

    This file is part of a double implementation of duration calculus
    in Coq as a shallow embedding and a deep embedding, which we shall
    refer to collectively as DC-coq.

    DC-coq is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DC-coq is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DC-coq.  If not, see <http://www.gnu.org/licenses/>.
*)


Require Export Definitions.
Require Export Functions.

(* This file contains necessary axioms and inference rules *)

(* We need to state what is valid, btw *)
Axiom is_valid:(plvalid FTrue).

Open Scope formulas_scope.

(* Modus ponens *)
Axiom plMP:forall f g:Formula, (plvalid f) -> (plvalid (f --> g)) -> (plvalid g). 

(* Generalization *)
Axiom plforalli:forall (f:DCTerm->Formula) (x:Name), (plvalid (f (RVar x)))->(plvalid (FAll f)).

(* Quantification *)
(* The conditions on freeness are not added, because of the special form of *)
(* the "All" construction *)
Axiom plforalle:forall (f:DCTerm->Formula) (i:DCTerm),
(plvalid (FAll f))->((rigid_term i) \/ (chop_free (f (RVal R0)))) ->(plvalid (f i)).

(* Properties of the FOr *)
Axiom plor_il:forall (f g:Formula),(plvalid f) -> (plvalid (f \/ g)).
Axiom plor_ir:forall (f g:Formula),(plvalid g) -> (plvalid (f \/ g)).
Axiom plor_e:forall (f g h:Formula),(plvalid (f --> h)) -> (plvalid (g --> h)) -> (plvalid ((f \/ g) --> h)).

(* Properties of the FNot *)
Axiom notf:forall (f g:Formula),(plvalid (f --> g)) -> (plvalid (f --> ~g)) -> (plvalid (~f)).
Axiom plnnpp:forall (f:Formula),(plvalid ( ~~f --> f)).
Axiom notfalse:(plvalid (~FFalse)). 

(* Excluded middle *)
Axiom plem:forall (f:Formula),(plvalid  (f \/ ~f)).

(* Properties of the FImplies *)
Axiom pl_S:forall (f g:Formula),(plvalid (f --> (g --> f))).
Axiom pl_K:forall (f g h:Formula),
plvalid ((f --> g) --> ((f --> (g-->h)) --> (f-->h))).

(* Properties of the FAnd *)
Axiom pland_el:forall (f g:Formula),(plvalid (f /\ g)) -> (plvalid f).
Axiom pland_er:forall (f g:Formula),(plvalid (f /\ g)) -> (plvalid g).
Axiom pland_i:forall (f g:Formula),(plvalid f) -> (plvalid g) -> (plvalid (f /\ g)).

(* Axioms for the equality *)
Axiom eq_refl: forall (x:DCTerm),(plvalid  (x=x) ).
Axiom eq_sym:forall (x y:DCTerm),(plvalid (x=y) -> (y=x)).
Axiom eq_trans:forall (x y z:DCTerm),(plvalid (x=y) -> (y=z) -> (x=z)).
Axiom eq_replace:forall (x y:DCTerm) (f:(DCTerm -> Formula)),
  (plvalid (always (x=y)))->(plvalid (f x))->(plvalid (f y)).


(* Axioms for inequality "lower than"*) 
Axiom lt_antisym:forall (x y:DCTerm),(plvalid ((x<y) --> ~ (y<x))).
Axiom lt_trans:forall (x y z:DCTerm),(plvalid ((x<y) --> (y<z) --> (x<z))).
Axiom lt_compat:forall (x y z:DCTerm),(plvalid ((x<y) --> (x+z < y+z))).
(* Axiom lt_mono:forall (x y z:DCTerm),(plvalid ((z> (rvar 0%R)) --> (x<y) --> (x*z<y*z))). *)

Axiom total_order:forall (x y:DCTerm),(plvalid  ((x<y) \/ (x=y) \/ (x>y))).

(* Axioms for real arithmetic operations (well, for DCTerm actually)*)
Axiom add_ass:forall (x y z:DCTerm),(plvalid ((x+y)+z=x+(y+z))).
Axiom add_zero:forall (x:DCTerm),(plvalid (x+(RVal R0)=x)).
Axiom add_opp:forall (x:DCTerm),(plvalid (x+(-x)=(RVal R0))).
Axiom add_com:forall (x y:DCTerm),(plvalid (x+y = y+x)).
Axiom mult_ass:forall (x y z:DCTerm),(plvalid ((x*y)*z=x*(y*z))).
Axiom mult_one:forall (x y z:DCTerm),(plvalid (x*(RVal R1)=x)).
Axiom ex_inv:forall (x:DCTerm),(plvalid (x<>(RVal R0)) -> ((DCMult (DCDiv (RVal R1) x)  x)=(RVal R1))).
Axiom mult_com:forall (x y:DCTerm),(plvalid (x*y=y*x)).
Axiom mult_dist:forall (x y z:DCTerm),(plvalid (x*(y+z)=(x*y)+(y*z))).
Axiom zero_not_one:(plvalid ((RVal R0)<>(RVal R1)) ).

(* Axioms for interval logic *)
Axiom pos_interval:(plvalid (l>=(RVal R0))).
Definition A0:=pos_interval.


Axiom chop_and_right:forall (p q r:Formula),
  (plvalid (((p ^^ q)  /\ ~(p ^^ r)) --> (p ^^ (q /\ ~r)))).
Axiom chop_and_left:forall (p q r:Formula),
  (plvalid (((p ^^ q)  /\ ~(r ^^ q)) --> ((p /\ ~r) ^^ q))).
Definition A1_right:=chop_and_right.
Definition A1_left:=chop_and_left.


Axiom chop_assoc:forall (p q r:Formula),
  (plvalid (((p ^^ q) ^^ r) <-> (p ^^ (q ^^ r)))).
Definition A2:=chop_assoc.
Axiom rigid_chop_left:forall (p q:Formula),(rigid p)->(plvalid ((p ^^ q)-->p)).
Axiom rigid_chop_right:forall (p q:Formula),(rigid q)->(plvalid ((p ^^ q)-->q)).
Definition R_left:=rigid_chop_left.
Definition R_right:=rigid_chop_right.

Axiom exists_chop_left:forall  (p:DCTerm->Formula) (q:Formula),
 (plvalid (((Ex x|(p x)) ^^ q)-->(Ex x| ((p x) ^^ q)))).
Axiom exists_chop_right:forall  (p:Formula) (q:DCTerm->Formula),
 (plvalid ((p ^^ (Ex x|(q x)))-->(Ex x| (p ^^ (q x))))).
Definition B_left:=exists_chop_left.
Definition B_right:=exists_chop_right.

Axiom lx_chop_not_right:forall (x:DCTerm) (p:Formula),
  (plvalid (((l=x) ^^ p)--> ~((l=x) ^^ (~p)))).
Axiom lx_chop_not_left:forall (x:DCTerm) (p:Formula),
  (plvalid ((p ^^ (l=x))--> ~((~p) ^^ (l=x)))).
Definition L1_right:=lx_chop_not_right.
Definition L1_left:=lx_chop_not_left.

Axiom interval_chopping:forall (x y:DCTerm),
(plvalid (((x>=(RVal R0)) /\ (y>=(RVal R0)))--> ( (l=x+y) <-> ((l=x) ^^ (l=y))))).
Definition L2:=interval_chopping.

Axiom add_point_left:forall (p:Formula),(plvalid (p-->(p ^^ (l=(RVal R0))))).
Axiom add_point_right:forall (p:Formula),(plvalid (p-->((l=(RVal R0)) ^^ p))).
Definition L3_left:=add_point_left.
Definition L3_right:=add_point_right.

(* inference rules *)

Axiom necessitation_left:forall (p q:Formula),(plvalid p) -> (plvalid (~(~p ^^ q))).
Axiom necessitation_right:forall (p q:Formula),(plvalid p) -> (plvalid (~(q ^^ ~p))).
Axiom mono_chop:forall (p q r:Formula),(plvalid (p-->q)) -> (plvalid (p^^r --> q^^r)).
Axiom chop_mono:forall (p q r:Formula),(plvalid (p-->q)) -> (plvalid (r^^p --> r^^q)).

(* Axioms for duration calculus *)
Axiom DCA1:(plvalid (#SE0#=(RVal R0))).
Axiom DCA2:(plvalid (#SE1#=l)).
Axiom DCA3:forall (s:State_expr),(plvalid (#s#>=(RVal R0))).
Open Scope states_scope.
Axiom DCA4:forall (s1 s2:State_expr), 
  (plvalid ((#s1#+#s2#)=(#s1 \/ s2# + #s1 /\ s2#))  ).
Close Scope states_scope.
Axiom DCA5:forall (x y:DCTerm) (s:State_expr),
  (plvalid (((#s#=x) ^^ (#s#=y)) --> (#s#=x+y))).
Axiom DCA6:forall (s1 s2:State_expr), 
 (plvalid (((proplogic s1)<->(proplogic s2)) --> (#s1#=#s2#))).

(* And induction rules too. But needs list stuff *)

(* Require Export PolyList. *)
Section Lists.

Variable A : Type.

Set Implicit Arguments.

Inductive list : Type := nil : list | cons : A -> list -> list.
End Lists.
Unset Implicit Arguments.


Fixpoint disjunction (LS:(list State_expr)):State_expr:=
  match LS with
  | (cons state sequel) => 
    match sequel with
    | nil => state
    | _ => (SEOr state (disjunction sequel))
    end
  | nil => SE0
  end
.

Fixpoint chop_sharing_left (X:Formula) (LS:(list State_expr)) {struct LS}:Formula:=
 match LS with
 | (cons state sequel) =>
   match sequel with
   | nil => (FChop X (pr state))
   | _ => (FOr (FChop X (pr state) ) (chop_sharing_left X sequel))
   end
 | nil => FFalse
 end
.

Fixpoint chop_sharing_right (X:Formula) (LS:(list State_expr)) {struct LS}:Formula:=
 match LS with
 | (cons state sequel) =>
   match sequel with
   | nil => (FChop (pr state) X)
   | _ => (FOr (FChop (pr state) X) (chop_sharing_right X sequel))
   end
 | nil => FFalse
end.

Axiom IR1: forall (H:Formula->Formula) (LS:(list State_expr)),
(plvalid ((proplogic (disjunction LS))<->FTrue))->
(plvalid (H point))-> 
(forall (X:Formula),(plvalid ((H X)-->(H (X \/ (chop_sharing_left X LS))) ) )) -> 
(plvalid (H FTrue)).

Axiom IR2: forall (H:Formula->Formula) (LS:(list State_expr)),
(plvalid ((proplogic (disjunction LS))<->FTrue))->
(plvalid (H point))-> 
(forall (X:Formula),(plvalid ((H X)-->(H (X \/ (chop_sharing_right X LS))) ) )) -> 
(plvalid (H FTrue)).


Theorem IR1_simple:forall (H:Formula->Formula) (S:State_expr), 
 (plvalid (H point))-> (forall (X:Formula),(plvalid ((H X)-->(H (X \/ (X ^^ (pr S)) \/ (X ^^ (pr (SENot S)))))))) -> (plvalid (H FTrue)).
intros H S Hpoint IR1hypo.
apply IR1 with (LS:=(cons S (cons (SENot S) (nil _)))).
 simpl. unfold FIff. apply pland_i.
  unfold FImplies; apply plor_ir; apply is_valid.
  unfold FImplies; apply plor_ir; apply plem.
 assumption.
 intros; simpl; apply IR1hypo.
Qed.

Theorem IR2_simple:forall (H:Formula->Formula) (S:State_expr),
 (plvalid (H point))-> (forall (X:Formula),(plvalid ((H X)-->(H (X \/ ((pr S) ^^ X) \/ ((pr (SENot S)) ^^ X)))))) -> (plvalid (H FTrue)).
intros H S Hpoint IR2hypo.
apply IR2 with (LS:=(cons S (cons (SENot S) (nil _)))).
 simpl; unfold FIff; apply pland_i.
  unfold FImplies; apply plor_ir; apply is_valid.
  unfold FImplies; apply plor_ir; apply plem.
 assumption.
 intros; simpl; apply IR2hypo.
Qed.

Close Scope formulas_scope.
