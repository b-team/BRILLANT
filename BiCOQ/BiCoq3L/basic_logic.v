(*==================================================================================================
  Project : BiCoq3
  Module : basic_logic.v
  This module provides a few tools about Type and Set (the plan being to not use Prop when it is
  possible), and about boolean functions and their logical interpretation.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Jun 2008, Md: Jun 2008
  ================================================================================================*)

Require Export Bool.

(* Notations -------------------------------------------------------------------------------------*)

Notation "'Β'" := (bool).
Notation "'⊤'" := (true).
Notation "'⊥'" := (false).

(* Implementation properties, arity 1 ------------------------------------------------------------*)

Definition implements1(T:Type)(P:T->Type)(f:T->Β):=
 ((forall (t:T), f t=⊤->P t) * (forall (t:T), f t=⊥->P t->False))%type.
Notation "'(' P '⇝¹' f ')'":=(@implements1 _ P f).

Theorem imppt1:forall (T:Type)(P:T->Type)(f:T->Β), (P⇝¹f)->forall (t:T), P t->f t=⊤.
Proof.
 intros T P f Himp t HP; case_eq (f t); intros Hf;
  [apply refl_equal | destruct (((snd Himp) _ Hf) HP)].
Qed.
Implicit Arguments imppt1 [T P f].

Theorem imprf1:forall (T:Type)(P:T->Type)(f:T->Β), (P⇝¹f)->forall (t:T), (P t->False)->f t=⊥.
Proof.
 intros T P f Himp t HP; case_eq (f t); intros Hf;
  [destruct (HP ((fst Himp) _ Hf)) | apply refl_equal].
Qed.
Implicit Arguments imprf1 [T P f].

Ltac reflect1 H :=
 match goal with
 | |- _=⊤ => apply (imppt1 H)
 | |- _=⊥ => apply (imprf1 H)
 | |- ⊤=_ => apply sym_equal; apply (imppt1 H)
 | |- ⊥=_ => apply sym_equal; apply (imprf1 H)
 | |- _->False => apply (snd H)
 | |- _ => apply (fst H)
 end.

Ltac reflect_hyp1 H H' :=
 (generalize ((fst H) _ H') ||
  generalize ((snd H) _ H') ||
  generalize (imprf1 H _ H') ||
  generalize (imppt1 H _ H'));clear H'; intros H'.

Theorem impdec1:forall (T:Type)(P:T->Prop)(f:T->Β), (P⇝¹f)->forall (t:T), {P t}+{~P t}.
Proof.
 intros T P f Himp t; case_eq (f t); intros Hf; [left | right; unfold not]; reflect1 Himp; apply Hf.
Qed.
Implicit Arguments impdec1 [T P f].

(* Implementation properties, arity 2 ------------------------------------------------------------*)

Definition implements2(T1 T2:Type)(P:T1->T2->Type)(f:T1->T2->Β):=
 ((forall (t1:T1)(t2:T2), f t1 t2=⊤->P t1 t2) *
  (forall (t1:T1)(t2:T2), f t1 t2=⊥->P t1 t2->False))%type.
Notation "'(' P '⇝²' f ')'":=(@implements2 _ _ P f).

Theorem imppt2:forall (T1 T2:Type)(P:T1->T2->Type)(f:T1->T2->Β),
             (P⇝²f)->forall (t1:T1)(t2:T2), P t1 t2->f t1 t2=⊤.
Proof.
 intros T1 T2 P f Himp t1 t2 HP; case_eq (f t1 t2); intros Hf;
  [apply refl_equal | destruct (((snd Himp) _ _ Hf) HP)].
Qed.
Implicit Arguments imppt2 [T1 T2 P f].

Theorem imprf2:forall (T1 T2:Type)(P:T1->T2->Type)(f:T1->T2->Β),
             (P⇝²f)->forall (t1:T1)(t2:T2), (P t1 t2->False)->f t1 t2=⊥.
Proof.
 intros T1 T2 P f Himp t1 t2 HP; case_eq (f t1 t2); intros Hf;
  [destruct (HP ((fst Himp) _ _ Hf)) | apply refl_equal].
Qed.
Implicit Arguments imprf2 [T1 T2 P f].

Ltac reflect2 H :=
 match goal with
 | |- _=⊤ => apply (imppt2 H)
 | |- _=⊥ => apply (imprf2 H)
 | |- ⊤=_ => apply sym_equal; apply (imppt2 H)
 | |- ⊥=_ => apply sym_equal; apply (imprf2 H)
 | |- _->False => apply (snd H)
 | |- _ => apply (fst H)
 end.

Ltac reflect_hyp2 H H' :=
 (generalize ((fst H) _ _ H') ||
  generalize ((snd H) _ _ H') ||
  generalize (imprf2 H _ _ H') ||
  generalize (imppt2 H _ _ H'));clear H'; intros H'.

Theorem impdec2:forall (T1 T2:Type)(P:T1->T2->Prop)(f:T1->T2->Β),
              (P⇝²f)->forall (t1:T1)(t2:T2), {P t1 t2}+{~P t1 t2}.
Proof.
 intros T1 T2 P f Himp t1 t2; case_eq (f t1 t2); intros Hf; [left | right; unfold not];
  reflect2 Himp; apply Hf.
Qed.
Implicit Arguments impdec2 [T1 T2 P f].

(* Implementation properties, arity 3 ------------------------------------------------------------*)

Definition implements3(T1 T2 T3:Type)(P:T1->T2->T3->Type)(f:T1->T2->T3->Β):=
 ((forall (t1:T1)(t2:T2)(t3:T3), f t1 t2 t3=⊤->P t1 t2 t3) *
  (forall (t1:T1)(t2:T2)(t3:T3), f t1 t2 t3=⊥->P t1 t2 t3->False))%type.
Notation "'(' P '⇝³' f ')'":=(@implements3 _ _ _ P f).

Theorem imppt3:forall (T1 T2 T3:Type)(P:T1->T2->T3->Type)(f:T1->T2->T3->Β),
             (P⇝³f)->forall (t1:T1)(t2:T2)(t3:T3), P t1 t2 t3->f t1 t2 t3=⊤.
Proof.
 intros T1 T2 T3 P f Himp t1 t2 t3 HP; case_eq (f t1 t2 t3); intros Hf;
  [apply refl_equal | destruct (((snd Himp) _ _ _ Hf) HP)].
Qed.
Implicit Arguments imppt3 [T1 T2 T3 P f].

Theorem imprf3:forall (T1 T2 T3:Type)(P:T1->T2->T3->Type)(f:T1->T2->T3->Β),
             (P⇝³f)->forall (t1:T1)(t2:T2)(t3:T3), (P t1 t2 t3->False)->f t1 t2 t3=⊥.
Proof.
 intros T1 T2 T3 P f Himp t1 t2 t3 HP; case_eq (f t1 t2 t3); intros Hf;
  [destruct (HP ((fst Himp) _ _ _ Hf)) | apply refl_equal].
Qed.
Implicit Arguments imprf3 [T1 T2 T3 P f].

Ltac reflect3 H :=
 match goal with
 | |- _=⊤ => apply (imppt3 H)
 | |- _=⊥ => apply (imprf3 H)
 | |- ⊤=_ => apply sym_equal; apply (imppt3 H)
 | |- ⊥=_ => apply sym_equal; apply (imprf3 H)
 | |- _->False => apply (snd H)
 | |- _ => apply (fst H)
 end.

Ltac reflect_hyp3 H H' :=
 (generalize ((fst H) _ _ _ H') ||
  generalize ((snd H) _ _ _ H') ||
  generalize (imprf2 H _ _ _ H') ||
  generalize (imppt2 H _ _ _ H'));clear H'; intros H'.

Theorem impdec3:forall (T1 T2 T3:Type)(P:T1->T2->T3->Prop)(f:T1->T2->T3->Β),
              (P⇝³f)->forall (t1:T1)(t2:T2)(t3:T3), {P t1 t2 t3}+{~P t1 t2 t3}.
Proof.
 intros T1 T2 T3 P f Himp t1 t2 t3; case_eq (f t1 t2 t3); intros Hf; [left | right; unfold not];
  reflect3 Himp; apply Hf.
Qed.
Implicit Arguments impdec3 [T1 T2 T3 P f].

(* Generalized tactic for reflection -------------------------------------------------------------*)

Ltac reflect H := unfold not; unfold notT; (reflect3 H || reflect2 H || reflect1 H).

Ltac reflect_in H H' := unfold not in H'; unfold notT in H';
                        (reflect_hyp3 H H' || reflect_hyp2 H H' || reflect_hyp1 H H').

Ltac bool_simpl := repeat (rewrite andb_true_r in * ||
                           rewrite andb_false_r  in * ||
                           rewrite andb_true_l in * ||
                           rewrite andb_false_l  in * ||
                           rewrite orb_true_r in * ||
                           rewrite orb_false_r  in * ||
                           rewrite orb_true_l in * ||
                           rewrite orb_false_l  in *).

(* A remark about eqb ----------------------------------------------------------------------------*)

Theorem eqb_imp:((fun x y:Β=>x=y)⇝²eqb).
Proof.
 unfold implements2; split; intros [ | ] [ | ]; simpl; intros H;
  try (apply refl_equal); try (inversion H); try (intro H'; inversion H').
Qed.

(*================================================================================================*)