(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Export Ensembles.
Require Export Finite_sets.
Require Export Powerset.
Require Import Classical_sets.
Require Import Powerset_facts.
Require Import ClassicalEpsilon.
Require Import Bchapter1.

(**
  * Definition of basic notations
  *)

(* in: see In *)
Implicit Arguments In [U].
Notation "x ∈ y" := (In y x) (at level 11, no associativity): eB_scope.

(* notin *)

Definition NotIn (U: Type) (A:Ensemble U) (x:U) := ~ (In A x).
Implicit Arguments NotIn [U].
Notation "x ∉ y" := (NotIn y x) (at level 11, no associativity): eB_scope.


(* subseteq: see Included *)
Implicit Arguments Included [U].
Notation "x ⊆ y" := (Included x y) (at level 11, no associativity): eB_scope.

(* subset: see Strict_Included *)
Implicit Arguments Strict_Included [U].
Notation "x ⊂ y" := (Strict_Included x y) (at level 11, no associativity): eB_scope.

(* nsubseteq *)

Definition NotIncluded (U: Type) (A B:Ensemble U) := ~ (Included A B).
Implicit Arguments NotIncluded [U].
Notation "x ⊈ y" := (NotIncluded x y) (at level 11, no associativity): eB_scope.

(* nsubset *)

Definition NotStrict_Included (U: Type) (A B:Ensemble U) := ~ (Strict_Included A B).
Implicit Arguments NotStrict_Included [U].
Notation "x ⊄ y" := (NotStrict_Included x y) (at level 11, no associativity): eB_scope.

(* Set defined in comprehension *)

(* Here we see that sets here are exactly how they were defined in BPhoX: witnesses of a property. Hence a set defined in comprehension is the truth of this property wrt its argument.

Hence if in B we have: !x. x : { z | z : Z & z >= 10}, we will have in Coq:
forall x, In (fun z:Z => z >= 10)) x.
or something close.

Here is an example illustrating it:
Theorem truc: Included (fun x => x >=1) (fun x:nat => x >=0).
Proof.
unfold Included.
intros.
compute in H.
compute.
omega.
Qed.
*)

Definition Comprehension (U:Type) (P:U->Prop):=P.
Implicit Arguments Comprehension [U].

Notation "{? x | P ?}" := (Comprehension (fun x => P)) (at level 6, no associativity): eB_scope.

(* Usage: { z | z : Z & z >= 10} is (fun z:Z => z >= 10)) *)

(* part: see Power_set *)
Implicit Arguments Power_set [U].
Notation "ℙ( x )" := (Power_set x) (at level 6, no associativity): eB_scope.

(* Axiom of choice

  It is not the axiom of choice per se, but it nonetheless allows us to define the notion of
  choice in a set.

  It is of absolute importance to note that the use of this axiom and of Classical imply
  that the option "-impredicative-set" *can not be used*: it would render coq inconsistent.
  You have been warned.
*)

(* choice is actually Hilbert's epsilon *)
Definition Bchoice := epsilon.

(* We will need this one for function application *)
Definition iota  (A:Type)(i : inhabited A)(P: A-> Prop) : A :=
  epsilon i (unique P).

(* cartesian product *)

Inductive times (U V: Type) (A: Ensemble U) (B: Ensemble V): Ensemble (prod U V) :=
  times_intro: forall (a: U) (b: V), In A a -> In B b -> times U V A B (pair a b).
Implicit Arguments times [U V].
Notation "x × y" := (times x y) (at level 61, right associativity): eB_scope.

(**
  * Axioms of the BBook
  *)

(**
  axiom SET 1, BBook: section 2.1.2, p.61, array 1, row 1

  (E,F) : (s x t) <=> (E : s /\ F : t)
  *)
Theorem SET_1:
  forall (S T: Type)
  (s: Ensemble S) (t: Ensemble T)
  (E: S) (F: T),
  In (times s t) (E,F) <-> (In s E /\ In t F).
Proof.
intros S T s t E F.
split; intros H.
inversion H; intuition.
constructor; intuition.
Qed.

(**
  axiom SET 2, BBook: section 2.1.2, p.61, array 1, row 2

  s : IP(t) <=> !x . (x : s => x : t)
  *)
Theorem SET_2: forall (T: Type)
  (t: Ensemble T) (s: Ensemble T),
  In (Power_set t) s <-> forall (x:T), (In s x -> In t x).
Proof.
intros T t.
split; intros H.
induction H; intuition.
constructor; intuition.
Qed.

(* TODO: will require a definition of substitution to be really accurate *)

(**
  axiom SET 3, BBook: section 2.1.2, p.61, array 1, row 3

  E : {x | x : s /\ P } <=> (E : s /\ [x:=E]P)
  *)
Theorem SET_3: forall (S: Type)
  (s: Ensemble S) (P: S -> Prop) (E: S),
  In (Comprehension (fun x => In s x /\ (P x))) E <-> (In s E /\ P E).
Proof.
intros S s P E.
split; intros H.
compute in H; intuition.
compute; intuition.
Qed.

(**
  axiom SET 4, BBook: section 2.1.2, p.61, array 1, row 4

  !x . (x : s <=> x : t) => s = t
  *)
Theorem SET_4: forall (S: Type)
  (s t: Ensemble S),
  (forall (x: S), (In s x <-> In t x)) -> s=t.
Proof.
intros S s t H.
apply Extensionality_Ensembles.
unfold Same_set; unfold Included.
split; intros x H0; elim (H x); intuition.
Qed.

(**
  axiom SET 5, BBook: section 2.1.2, p.61, array 1, row 5

  ##x . (x : s) => choice(s) : s
  *)
Theorem SET_5: forall (S: Type)
  (s: Ensemble S),
  (exists x:S, In s x) -> exists inh_s, In s (Bchoice S inh_s s).
Proof.
intros S s H.
elim H; intros x H0.
exists (inhabits x).
pattern (Bchoice S (inhabits x) s).
apply (epsilon_spec (inhabits x) (fun s0 => In s s0)).
exists x; assumption.
Qed.

Variable BIG_type: Set.
Definition BIG:= Full_set BIG_type.

(**
  axiom SET 6, BBook: section 2.1.2, p.61, array 1, row 6

  This axiom really is different, because it's about a specific set rather than
  a property of the introduced connectors
  *)
Axiom SET_6: ~ (Finite _ BIG).


(**
  * Validity of Inclusion definition
  *)


(**
  Checking definition of inclusion, BBook: section 2.1.2, p.62, array 1, row 1
  *)
Theorem valid_inclusion: forall (S: Type)
  (s t: Ensemble S),
  Included s t <-> In (Power_set t) s.
Proof.
intros S s t.
split; intros H.
constructor;intuition.
elim H; intuition.
Qed.

(**
  Checking definition of strict inclusion, BBook: section 2.1.2, p.62, array 1, row 2
  *)
Theorem valid_strict_inclusion: forall (S: Type)
  (s t: Ensemble S),
  Strict_Included s t <-> (Included s t /\ s <>t).
Proof.
intros S s t.
split; intros H.
elim H; intuition.
intuition.
Qed.

(**
  * Manipulation of the choice operator
  *)

(*
Here follow some theorems for easier manipulation of the axiom of choice/definite description.
Most if not all are taken from Pierre Casteran's Epsilon.v, with modifications
to accomodate for non-implicit arguments
*)


Theorem epsilon_ind :
   forall (A:Type)(i : inhabited A)(P:A->Prop)
          (Q:A->Prop), (exists x,  P x)->
                       (forall x : A, P x -> Q x) ->
                       Q (epsilon  i P).
Proof.
 intros A a  P Q exP P_Q.
 apply P_Q.
  apply epsilon_spec; assumption.
Qed.

Lemma iota_e : forall (A:Type) (i: inhabited A) (P : A -> Prop),
                     ex (unique P) ->
                     unique P (iota A i P).
Proof.
 intros A a P  H; unfold iota.
 apply epsilon_ind;auto.
Qed.

Lemma  iota_e_weak : forall (A:Type) (i: inhabited A) (P : A -> Prop),
                     ex (unique P) ->
                      P (iota A i P).
Proof.
 intros.
 case (iota_e A i P H).
 auto.
Qed.

Theorem iota_ind :
   forall (A:Type)(i: inhabited A)(P:A->Prop)
          (Q:A->Prop),
                      (forall b : A, unique P b -> Q b) ->
                       ex (unique P) ->
                       Q (iota A i P).
Proof.
  intros A a P Q H (x,H0).
 apply H.
apply iota_e.
  exists x;auto.
Qed.

Theorem iota_ind_weak :  forall (A:Type)(i: inhabited A)(P:A->Prop)
          (Q:A->Prop),
              (forall b : A,  P b -> Q b) ->
              ex (unique P) ->
              Q (iota A i P).
Proof.
 intros.
 apply H.
 apply iota_e_weak.
 auto.
Qed.


(**
  * Basic constructs: old theorems inherited from BPhoX
  *)

(* TODO: move, remove or rename these in the relevant files *)

(* injective couple *)

Theorem prod001: forall (U V: Type)
  (x a: U) (y b: V), (x,y)= (a,b) -> x=a /\ y=b.
Proof.
intros.
injection H.
intuition.
Qed.

(* Library of theorems about set theory *)


(* comprehension schema *)

Theorem cs001: forall (U: Type)
  (F: U -> Prop) (x: U), (In (Comprehension F) x) = F x.
Proof.
intros.
auto with sets.
Qed.

Theorem part001: forall (U: Type)
  (A X: Ensemble U), In (Power_set A) X <-> (Included X A).
Proof.
intros.
unfold Included.
split.
intros.
induction H.
intuition.
intros.
constructor.
intuition.
Qed.


(* Library of theorems about products *)


Theorem prod002: forall (U V: Type)
  (A: Ensemble U) (B: Ensemble V) (x: U) (y: V),
  In A x -> In B y -> In (times A B) (x,y).
Proof.
intros.
constructor; assumption.
Qed.

Theorem prod003: forall (U V: Type)
  (X: Prop) (A: Ensemble U) (B: Ensemble V) (z: prod U V),
  (forall (x: U) (y: V), In A x -> In B y -> z = (x,y) -> X) -> In (times A B) z -> X.
Proof.
intros.
induction H0.
apply H with a b; intuition.
Qed.

Theorem prod004: forall (U V: Type)
  (X: Prop) (x x': U) (y y': V),
  ((x = x' -> y = y' -> X) -> (x,y) = (x',y') -> X).
Proof.
intros.
apply H; injection H0; intuition.
Qed.

Theorem prod005: forall (U V: Type)
  (A: Ensemble U) (B: Ensemble V) (z: U * V),
  In (times A B) z -> (exists x:U, fst z = x).
Proof.
intros.
induction H.
exists a; intuition.
Qed.

Theorem prod006: forall (U V: Type)
  (A: Ensemble U) (B: Ensemble V) (z: U * V), In (times A B) z -> (exists y: V, snd z = y).
Proof.
intros.
induction H.
exists b; intuition.
Qed.

Theorem prod007: forall (U V: Type)
  (x: U) (y: V), fst (x,y) = x.
Proof.
intuition.
Qed.

Theorem prod008: forall (U V: Type)
  (x: U) (y: V), snd (x,y) = y.
Proof.
intuition.
Qed.

Theorem prod009: forall (U V: Type)
  (A: Ensemble U) (B: Ensemble V) (z: U * V),
  In (times A B) z -> In A (fst z).
Proof.
intros.
induction H.
intuition.
Qed.

(* SC: An alternative to show that bicoax definitions of sets are really close to that of bphox *)
Theorem prod009_2: forall (U V: Type)
  (A: Ensemble U) (B: Ensemble V)
  (z: U * V), In (times A B) z -> A (fst z).
Proof.
intros.
induction H.
intuition.
Qed.

Theorem prod010: forall (U V: Type)
  (A: Ensemble U) (B: Ensemble V) (z: U * V),
  In (times A B) z -> In B (snd z).
Proof.
intros.
induction H.
intuition.
Qed.

Theorem prod011: forall (U V: Type)
  (A: Ensemble U) (B: Ensemble V) (x: U * V),
  In (times A B) x -> (fst x, snd x)=x.
Proof.
intros.
induction H.
intuition.
Qed.

