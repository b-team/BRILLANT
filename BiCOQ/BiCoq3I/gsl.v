(*==================================================================================================
  Project : BiCoq3
  Module : gsl.v
  This modules defines the Generalized Substitution Language as a new syntactical category. We
  therefore, one more time, retain the deep embedding approach, noting that it was possible to
  introduce substitutions in a shallow approach, even in a deep embedding of B, by presenting
  such substitutions only through the result of their application to B terms.
  Note that as far as the B is concerned, substitutions are seen extensionnaly, that is two
  substitutions are equals if they refine each other (if they realise the same predicates).
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Feb 2009, Md: Mar 2009
  ================================================================================================*)

Require Export bmeta.
Require Import List.

(* B generalised substitutions -------------------------------------------------------------------*)
(* Note that we need to define B substitutions of the form [x,y,...:=Ex,Ey,...]. We should normally
   deal with several well-formedness constraints: there should be exactly one variable on the left
   for one expression on the right, and all variables on the left should be distinct. Yet we adopt a
   slightly simplified version by considering this substitution as a predicate transformer that
   creates hypothesis of the form x=Ex=>... If all variables are NOT distinct, this may lead to an
   inconsistent situation, or at least may result in assuming Ex=E'x *)

Inductive Bgsl:Set :=
 | bpar:Φ->Bgsl           (* Parallel substitution *)
 | bpre:Π->Bgsl->Bgsl     (* Pre-condition *)
 | bbch:Bgsl->Bgsl->Bgsl  (* Bounded choice *)
 | bgrd:Π->Bgsl->Bgsl     (* Guard *)
 | buch:Bgsl->Bgsl        (* Unbounded choice *)
 | bseq:Bgsl->Bgsl->Bgsl. (* Sequence *)
(* Nota: skip and elementary substitution are specialised form of bpar. Note also that we use a de
   Bruijn notation here, as the constructor for unbounded choice is not parameterised by an index.
   This is a non trivial design choice,  but the B-Book states that [@x.S]P<=>∀x.[S]P provided that
   x\P; the meaning of x::S for example is given by x::S=@x'.(x'::S==>x:=x), that is @ cannot
   capture a pre-existing variable of the external context but introduces a fresh one. The easiest
   way to tackle that is to work directly with de Bruijn indexes. *)

Notation "'Σ'" := (Bgsl).
Notation "'‹‹' f '››'" := (bpar f).
Notation "'‹' p '‣' s '›'" := (bpre p s).
Notation "'‹' s1 '▯' s2 '›'" := (bbch s1 s2).
Notation "'‹' p '⇛' s '›'" := (bgrd p s).
Notation "'‹@Δ' s '›'" := (buch s).
Notation "'‹' s1 ';' s2 '›'" := (bseq s1 s2).

(* Additional operators --------------------------------------------------------------------------*)

Definition bskp := ‹‹(⊚⊲nil)››.
Notation "'☓'" := (bskp).

Definition baff(i:Ι)(e:Ε) := ‹‹(fun (i':Ι) => if DB_eq i i' then e else ∂(i')⊲i::nil)››.
Notation "'‹' i '≔' e '›'" := (baff i e).

(* Natural notation for unbounded choice ---------------------------------------------------------*)
(* As for other binders, we propose here a form of natural notation through an abstraction function
   operating over substitutions. @x.P==>y:=x (y becomes such that [x:=y]P) will be a notation for
   ‹@Δ Bs_abstr_ 0 x (P(x)==>y:=x)›=‹@Δ (Bp_abstr_ 0 x P)==>(Bs_abstr_ 0 x y:=x)›=
   ‹@Δ (Bp_abstr_ 0 x P)==>↥(y):=∂(¡(0,0)))›. We choose here to allow notations such as
   @x.P==>x:=e, that is the frame of the substitution can be captured by the @, useless but
   acceptable in B. *)

Fixpoint LI_abstr_(d:nat)(i:Ι)(l:list Ι){struct l}:list Ι :=
 match l with
 | nil => nil
 | i'::l' => (if DB_le d i' && DB_eq i i' then ¡(0,d) else DB_lift_ d i')::LI_abstr_ d i l'
 end.

Definition MS_abstr_(d:nat)(i:Ι)(m:Μ):Μ :=
 fun (i':Ι) =>
 Be_abstr_ d
           i
           (m (match i' with
               | ¡(0,n') => if (nat_le d n') then (if nat_eq d n' then i else ¡(0,pred n')) else i'
               | _ => i'
               end)).
(* Nota: in short, Abstr d i m i' = Abstr_ d i (m (App d i' i)). *)

Theorem MS_abstr_valid:forall (i i':Ι)(m:Μ)(d:nat),
(MS_abstr_ d i m) (if DB_le d i' && DB_eq i i' then ¡(0,d) else DB_lift_ d i')=Be_abstr_ d i (m i').
Proof.
 intros i i' m d; unfold MS_abstr_; case_eq (DB_le d i' && DB_eq i i'); intros Hi'; nat_simpl.
  destruct (andb_prop _ _ Hi') as [_ Hii']; reflect_in DB_eq_imp Hii'; rewrite Hii';
   apply refl_equal.
  destruct (andb_false_elim _ _ Hi') as [Hdi' | Hii']; destruct i' as [[| u'] n']; simpl in *.
   repeat (rewrite Hdi'); apply refl_equal.
   apply refl_equal.
   case_eq (nat_le d n'); intros Hdn'.
    reflect_in nat_le_imp Hdn'; repeat (rewrite (imppt2 nat_le_imp _ _ (le_S _ _ Hdn')));
     case_eq (nat_eq d (S n')); intros HdSn'.
     reflect_in nat_eq_imp HdSn'; rewrite HdSn' in Hdn'; destruct (le_Sn_n _ Hdn').
     apply refl_equal.
    rewrite Hdn'; apply refl_equal.
   apply refl_equal.
Qed.
(* Nota: Just a quick check that (Abstr d i m) (Abstr d i i')=Abstr d i (m i') *)

Fixpoint Bs_abstr_(d:nat)(i:Ι)(s:Σ){struct s}:Σ:=
 match s with
 | ‹‹(m'⊲l')›› => ‹‹(MS_abstr_ d i m'⊲LI_abstr_ d i l')››
 | ‹p'‣s'› => ‹Bp_abstr_ d i p'‣Bs_abstr_ d i s'›
 | ‹s1▯s2› => ‹Bs_abstr_ d i s1▯Bs_abstr_ d i s2›
 | ‹p'⇛s'› => ‹Bp_abstr_ d i p'⇛Bs_abstr_ d i s'›
 | ‹@Δs'› => ‹@ΔBs_abstr_ (S d) (DB_lift_ d i) s'›
 | ‹s1;s2› => ‹Bs_abstr_ d i s1;Bs_abstr_ d i s2›
 end.

Definition Bs_unbounded(i:Ι)(s:Σ):Σ := ‹@ΔBs_abstr_ 0 i s›.
Notation "'‹@' i '∙' s '›'" := (Bs_unbounded i s).


(* Semantics -------------------------------------------------------------------------------------*)

Fixpoint Bs_apply(s:Σ)(p:Π){struct s}:Π :=
 match s with
 | ‹‹(m'⊲l')›› => 〈|(m'⊲l')@Π@p|〉
 | ‹p'‣s'› => p'⋀(Bs_apply s' p)
 | ‹s1▯s2› => (Bs_apply s1 p)⋀(Bs_apply s2 p)
 | ‹p'⇛s'› => p'⇒(Bs_apply s' p)
 | ‹@Δs'› => ∀Δ(Bs_apply s' ⇑Π(p))
 | ‹s1;s2› => Bs_apply s1 (Bs_apply s2 p)
 end.
(* Nota: Again we lift an application into Coq rather than representing it in the language. Note
   also in this case that while the semantic of GSL is given by ⇔, we implement it by = on Π. *)

(* Refinement ------------------------------------------------------------------------------------*)

Definition Bs_ref(s1 s2:Σ) := forall (p:Π), ⊢Bs_apply s2 p⇒Bs_apply s1 p.

Theorem Bs_ref_refl:forall (s:Σ), Bs_ref s s.
Proof.
 intros s; unfold Bs_ref; intros p; Btac_prop.
Qed.

Theorem Bs_ref_trans:forall (s1 s2 s3:Σ), Bs_ref s1 s2->Bs_ref s2 s3->Bs_ref s1 s3.
Proof.
 intros s1 s2 s3; unfold Bs_ref; intros H12 H23 p; apply Bpr_modp with (1:=H23 p);
  apply Bpr_modp with (1:=H12 p); Btac_prop.
Qed.

Theorem bc_ref:forall (s1 s2:Σ),  Bs_ref s1 s2->forall (p:Π), ⊢Bs_apply s2 p->⊢Bs_apply s1 p.
Proof.
 intros s1 s2 Href; intros p Hs2; apply (bc_imp _ _ _ (Href p)); apply Hs2.
Qed.

Definition Bs_eq(s1 s2:Σ) := forall (p:Π), ⊢Bs_apply s2 p⇔Bs_apply s1 p.

Theorem Bs_eq_ref_l:forall (s1 s2:Σ), Bs_eq s1 s2->Bs_ref s1 s2.
Proof.
 intros s1 s2 Heq p; apply (Bpr_andl _ _ _ (Heq p)).
Qed.

Theorem Bs_eq_ref_r:forall (s1 s2:Σ), Bs_eq s1 s2->Bs_ref s2 s1.
Proof.
 intros s1 s2 Heq p; apply (Bpr_andr _ _ _ (Heq p)).
Qed.

Theorem Bs_ref_eq:forall (s1 s2:Σ), Bs_ref s1 s2->Bs_ref s2 s1->Bs_eq s1 s2.
Proof.
 intros s1 s2 H12 H21 p; apply (Bpr_andi _ _ _ (H12 p) (H21 p)).
Qed.

(* Rewriting -------------------------------------------------------------------------------------*)

Theorem Bs_apply_bskp:forall (p:Π), Bs_apply ☓ p=p.
Proof.
 intros p; unfold bskp; simpl; irewrite (Bt_fesubst_nil_ p ⊚ 0); apply refl_equal.
Qed.

Theorem Bp_apply_baff:forall (p:Π)(i:Ι)(e:Ε), Bs_apply ‹i≔e› p=〈i←e@Π@p〉.
Proof.
 intros p i e; unfold baff; simpl;
  irewrite (Bt_fesubst_sin p (fun (i':Ι) => if DB_eq i i' then e else ∂(i')) i); DB_simpl;
  apply refl_equal.
Qed.

(* Basic considerations --------------------------------------------------------------------------*)

Theorem Bp_apply_bpre_true:forall (p q:Π)(s:Σ), ⊢p⇒(Bs_apply s q)⇔(Bs_apply ‹p‣s› q).
Proof.
 intros p q s; simpl; Btac_prop.
Qed.

Theorem Bp_apply_bpre_true':forall (p q:Π)(s:Σ), ⊢p->⊢(Bs_apply s q)⇔(Bs_apply ‹p‣s› q).
Proof.
 intros p q s; apply bc_imp; apply Bp_apply_bpre_true.
Qed.

Theorem Bp_apply_bpre_false:forall (p q:Π)(s:Σ), ⊢¬p⇒¬(Bs_apply ‹p‣s› q).
Proof.
 intros p q s; simpl; Btac_prop.
Qed.

Theorem Bp_apply_bpre_false':forall (p q:Π)(s:Σ), ⊢¬p->⊢¬(Bs_apply ‹p‣s› q).
Proof.
 intros p q s; apply bc_imp; apply Bp_apply_bpre_false.
Qed.

Theorem Bp_apply_bpre_false'':forall (G:Γ)(p q r:Π)(s:Σ), G⊢¬p->G⊢Bs_apply ‹p‣s› q->G⊢r.
Proof.
 intros G p q r s Hp Hs; apply Bpr_modp with (1:=Hp); apply Bpr_modp with (1:=Hs); simpl; Btac_prop.
Qed.

Theorem Bp_apply_bgrd_true:forall (p:Π)(s:Σ), ⊢p->Bs_eq ‹p⇛s› s.
Proof.
 intros p s Hp q; simpl; apply Bpr_modp with (1:=Hp); Btac_prop.
Qed.

Theorem Bp_apply_bgrd_false:forall (p q:Π)(s:Σ), ⊢¬p⇒Bs_apply ‹p⇛s› q.
Proof.
 intros p q s; simpl; Btac_prop.
Qed.

(* Abortion --------------------------------------------------------------------------------------*)

Definition Bs_abt0(s:Σ) := forall (p:Π), ⊢¬(Bs_apply s p).
(* For any predicate, it is possible to refute that s establishes p *)

Definition Bs_abt1(s:Σ) := forall (p:Π), ⊢(Bs_apply s p)->False.
(* s does not establish anything. This is probably the best definition according to how abort is
   defined in the B-Book. *)

Definition Bs_abt2(s:Σ) := sigS (fun p:Π=>⊢Bs_apply s p)->False.
(* There is no predicate established by s *)

Theorem Bs_abt01:B_cons0->forall (s:Σ), Bs_abt0 s->Bs_abt1 s.
Proof.
 unfold Bs_abt0,Bs_abt1; intros HB s Hs p Hp; apply (HB _ Hp (Hs p)).
Qed.

Theorem Bs_abt12:forall (s:Σ), Bs_abt1 s->Bs_abt2 s.
Proof.
 unfold Bs_abt1,Bs_abt2; intros s Hs [p Hp]; apply (Hs _ Hp).
Qed.

Theorem Bs_abt0_bprd_false:forall (s:Σ), Bs_abt0 s->
                           forall (p:Π), ⊢(Bs_apply s p)⇔(Bs_apply ‹B_false‣☓› p).
Proof.
 intros s Habt p; unfold Bs_apply at 2; rewrite Bs_apply_bskp;
  apply Bpr_modp with (1:=B_false_false); Btac_prop; apply Bpr_impe; apply Bpr_clear;
  apply Bpr_ctpp; apply Bpr_impi; apply Bpr_clear; apply Habt.
Qed.

(* Termination -----------------------------------------------------------------------------------*)

Definition Bs_trm0(s:Σ) := ⊢Bs_apply s B_true.
Definition Bs_trm1(s:Σ) := sigS (fun p:Π=>⊢Bs_apply s p).
Definition Bs_trm2(s:Σ) := Bs_abt2 s->False.
Definition Bs_trm3(s:Σ) := Bs_abt1 s->False.
Definition Bs_trm4(s:Σ) := Bs_abt0 s->False.

Theorem Bs_trm01:forall (s:Σ), Bs_trm0 s->Bs_trm1 s.
Proof.
 unfold Bs_trm0,Bs_trm1; intros s Hs; exists B_true; apply Hs.
Qed.

Theorem Bs_trm12:forall (s:Σ), Bs_trm1 s->Bs_trm2 s.
Proof.
 unfold Bs_trm1,Bs_trm2,Bs_abt2; intros s Hs H's; apply H's; apply Hs.
Qed.

Theorem Bs_trm23:forall (s:Σ), Bs_trm2 s->Bs_trm3 s.
Proof.
 unfold Bs_trm2,Bs_trm3; intros s Hs H's; apply Hs; apply (Bs_abt12 _ H's).
Qed.

Theorem Bs_trm34:B_cons0->forall (s:Σ), Bs_trm3 s->Bs_trm4 s.
Proof.
 unfold Bs_trm3,Bs_trm4; intros HB s Hs H's; apply Hs; apply (Bs_abt01 HB _ H's).
Qed.

(*================================================================================================*)