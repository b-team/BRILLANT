/*? **************************** DIGILOG ************************************
 * Date : @(#) sccs get %G%
 *
 * File : %M%
 *
 * Version : %I%
 *
 * Directory : %P%
 *
 * Type	:	IMPLEMENTATION
 *
 * Object : Service which realize the pumps's synthesis
 *
 * Copyright : (C) 1996 Jean-Raymond ABRIAL / DIGILOG - STERIA Mediterranee
 *
 *   Permission is granted to make and distribute verbatim copies of
 *   this file provided the copyright notice and this permission notice
 *   are preserved on all copies.
 *
 *   Permission is granted to copy and distribute modified versions of
 *   this file under the conditions for verbatim copying, provided also
 *   that the entire resulting derived work is distributed under the terms
 *   of a permission notice identical to this one.
 *
 *   Permission is granted to copy and distribute translations of this
 *   manual into another language, under the above conditions for modified
 *   versions
 *
 *	 This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *   This program can be redistributed or modified under the terms of
 *   the GNU General Public License. The General Public License can be
 *	 found in the file COPYING tha accompanies the source code.
 *
 *---------------------------------------------------------------------------
 *
 * Functions :
 *		synthesis_pump
 *---------------------------------------------------------------------------
 *
 * Modifications :
 *
 ************************************************************************* ?*/

IMPLEMENTATION
	Service_Y_2

REFINES
	Service_Y_1

SEES
	P1.Cycle_R_1,
	P2.Cycle_R_1,
	P3.Cycle_R_1,
	P4.Cycle_R_1


INITIALISATION
	rtk_tot := TRUE;
	rok_tot := FALSE

OPERATIONS
	synthesis_pump =
		VAR rtk1_l , rtk2_l, rtk3_l, rtk4_l,
			rok1_l, rok2_l, rok3_l, rok4_l
		IN
			rtk1_l <-- P1.read_rtk;
			rtk2_l <-- P2.read_rtk;
			rtk3_l <-- P3.read_rtk;
			rtk4_l <-- P4.read_rtk;
			rok1_l <-- P1.read_rok;
			rok2_l <-- P2.read_rok;
			rok3_l <-- P3.read_rok;
			rok4_l <-- P4.read_rok;
			rtk_tot := bool(rtk1_l = TRUE & rtk2_l = TRUE & 
						rtk3_l = TRUE & rtk4_l = TRUE);
			rok_tot := bool(rok1_l = TRUE or rok2_l = TRUE or 
						rok3_l = TRUE or rok4_l = TRUE)
		END
END

