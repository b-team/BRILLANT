(*** $Id$ ***)



(** {5 Traduction du mod�le UML en machines B }
@author 
@version 
*)





(*** Fonctions auxiliaires ***)

(** met une cha�ne de caract�res en majuscules *)
val uc : string -> string

(** met une cha�ne de caract�res en minuscules *)
val lc : string -> string

(** met en majuscule la premi�re lettre d'un mot *)
val cp : string -> string

(** met en minuscule la premi�re lettre d'un mot *)
val ucp : string -> string




(*** Fonctions de traduction
     de parties du mod�le UML en parties de machines B ***)

(* concepts li�s aux classes *)

(** transcrit la multiplicit� d'une classe � l'aide d'in�galit�s
sur le cardinal de l'ensemble des objets de la classe
(contrainte inject�e par la suite dans un invariant B) *)
val class_card : Uml.multiplicity -> string -> B.predicate list

(** g�n�re une partie d'invariant B correspondant au typage de l'ensemble
des objets d'une classe : l'ensemble des objets existants (d�fini comme variable B)
est inclus dans le l'ensemble de toutes les instances possibles de la classe
(d�fini dans la clause SETS) *)
val class_inv : string -> B.predicate list


(* concepts li�s aux attributs *)

(** prend comme argument une liste d'attributs UML
@return la liste des noms de ces attributs
        (destin�e � une clause VARIABLES) *)
val attrib_var : Uml.attribute list -> string list

(** engendre les substitutions d'initialisation
des variables B correspondant � des attributs UML *)
val attrib_init : Uml.attribute list -> B.substitution list

(** g�n�re le typage des attributs d'une classe (inject� dans une clause INVARIANT) :
la variable B correspondante est soit une relation, soit une fonction partielle,
soit une fonction totale, selon la multiplicit� de l'attribut UML *)
val attrib_inv :
  Uml.attribute list -> string -> B.predicate list


(* notion de g�n�ralisation *)

(** produit une partie d'invariant B qui traduit la notion UML de g�n�ralisation :
l'ensemble des objets d'une classe est inclus dans l'ensemble des objets
de chaque super-classe dont la classe h�rite *)
val generalization_inv : string -> Uml.inheritance list -> B.predicate list


(* op�rations d'une classe *)

(** pour chaque classe est g�n�r�e une op�ration B
symbolisant la cr�ation d'une nouvelle instance de la classe *)
val class_create : string -> B.operation

(** pour chaque classe est g�n�r�e une op�ration B
symbolisant la destruction d'une instance donn�e de la classe *)
val class_destroy : string -> B.operation

(** transcription d'une op�ration UML en une op�ration B *)
val class_oper : Uml.operation -> B.operation


(* concepts li�s aux associations *)

(** cr�ation, dans la machine 'syst�me' (celle qui inclut les autres machines),
d'une clause VARIABLES traduisant les associations entre classes du mod�le UML *)
val assoc_variables : Uml.association list -> B.clause

(** engendre les substitutions d'initialisation des variables B correspondant
aux associations UML : � chaque variable est affect� l'ensemble vide *)
val assoc_init : Uml.association list -> B.substitution list

(** cr�� le typage d'associations UML en fonction de leurs multiplicit�s
(inject� ensuite dans un invariant B) *)
val assoc_inv : Uml.association list -> B.predicate list


(* notion d'agr�gation *)

(** traduction des agr�gations UML (destin� � l'invariant
de la machine 'syst�me', celle qui inclut les autres machines B) *)
val aggregation_inv : Uml.association list -> B.predicate list


(* notion de composition *)

(** prend comme arguments une liste d'associations UML et un nom de classe
@return les noms des associations qui d�notent des relations de composition
        (destin�s � produire des variables B) *)
val compose_var : Uml.association list -> Uml.name -> string list

(** prend comme arguments une liste d'associations UML et un nom de classe
@return les noms des classes qui composent cette classe
        (destin�s � une clause INCLUDES) *)
val compose_includes : Uml.association list -> Uml.name -> string list

(** cr�� les substitutions d'initialisation des variables B
symbolisant les relations UML de composition *)
val compose_init :
  Uml.association list -> Uml.name -> B.substitution list

(** g�n�re une partie d'invariant destin�e � traduire les compositions
(produit des relations B, des fonctions partielles ou des fonctions totales,
selon les cardinalit�s des associations UML) *)
val compose_inv :
  Uml.association list -> Uml.name -> B.predicate list


(* cr�ation de la machine 'syst�me' *)

(** prend comme arguments une liste d'associations et une liste de classes
@return les noms des machines qu'il faudra inclure dans la machine 'syst�me' *)
val system_includes :
  Uml.association list -> Uml.uml_class list -> string list

(** pour une association UML donn�e est g�n�r�e une op�ration B
symbolisant la cr�ation d'une instance de l'association
(entre deux objets existants) *)
val assoc_create : string -> string -> string -> B.operation

(** pour une association UML donn�e est g�n�r�e une op�ration B
symbolisant la destruction d'une instance de l'association
(entre deux objets existants) *)
val assoc_destroy : string -> string -> string -> B.operation

(** engendre une liste d'op�rations de cr�ation et de destruction d'instances
� partir d'une liste d'associations UML *)
val assoc_oper : Uml.association list -> B.operation list


(* concepts li�s aux diagrammes d'�tats-transitions *)

(** cr�ation, � partir de la liste des diagrammes d'�tats-transitions
d'une classe, d'ensembles �num�r�s repr�sentant les ensembles d'�tats possibles
pour les objets de la classe (destin�s � une clause SETS) *)
val state_sets : string -> Uml.state_machine list -> B.set list

(** cr�ation, � partir de la liste des diagrammes d'�tats-transitions
d'une classe, d'une liste de noms destin�s � devenir les variables B
qui symbolisent l'�tat courant de chaque instance de la classe *)
val state_var : Uml.state_machine list -> string -> string list

(** typage des variables B repr�sentant l'�tat courant des instances de classes :
il s'agit, pour chaque diagramme d'�tats-transitions, d'une fonction totale
de l'ensemble des objets existants vers l'ensemble des �tats possibles
(pr�dicats inject�s par la suite dans un invariant B) *)
val state_inv :
  Uml.state_machine list -> string -> B.predicate list

(** substitutions d'initialisation des variables B
symbolisant l'�tat courant des instances de classes :
� chaque variable est affect� l'ensemble vide *)
val state_init : Uml.state_machine list -> string -> B.substitution list

(** renvoie le nom d'un �tat UML (�tat atomique, initial ou final) *)
val state_name : Uml.state -> Uml.name

(** g�n�re les op�rations B correspondant aux transitions
des diagrammes d'�tats-transitions d'une classe
(pr�condition: l'objet est dans l'�tat source,
substitution: on affecte l'�tat destination � l'objet) *)
val state_oper : Uml.state_machine list -> Uml.name -> B.operation list

(** prend comme argument une liste de classes UML
@return une liste de couples (x,y) o� :
        - x est le nom d'une classe de la liste
        - y est la liste des diagrammes d'�tats-transitions li�s � cette classe *)
val flat_classes :
  Uml.uml_class list -> (Uml.name * Uml.state_machine list) list

(** prend comme arguments un nom de classe
et une liste de diagrammes d'�tats-transitions
@return une liste de couples (x,y) o� :
        - x est toujours le nom de classe pass� en param�tre
        - y est la liste des transitions d'un des diagrammes d'�tats-transitions *)
val flat_states :
  Uml.name -> Uml.state_machine list -> (Uml.name * Uml.transition list) list

(** prend comme arguments un nom de classe et une liste de transitions UML
@return une liste de couples (x,y) o� :
        - x est toujours le nom de classe pass� en param�tre
        - y est l'une des transitions de la liste *)
val flat_transit :
  Uml.name -> Uml.transition list -> (Uml.name * Uml.transition) list

(** prend comme argument une liste de classes UML et applique
successivement les fonctions flat_classes, flat_states et flat_transit
@return une liste de couples (x,y) o� :
        - x est le nom d'une classe de la liste
        - y est l'une des transitions de cette classe *)
val flat_event : Uml.uml_class list -> (Uml.name * Uml.transition) list

(** prend comme argument une liste de couples (x,y),
o� x est un nom de classe UML et y l'une des transitions de cette classe,
et extrait l'�v�nement, l'�tat source, l'�tat destination,
la garde et l'action de chaque transition *)
val fact_event :
  (Uml.name * Uml.transition) list ->
  (Uml.event * Uml.name * (Uml.source * Uml.target * Uml.guard * Uml.action) list) list

(** prend comme argument une liste de triplets issue de la fonction fact_event,
et supprime de cette liste les �v�nements anonymes *)
val clean1 : (Uml.event * Uml.name * 'a) list -> (Uml.event * Uml.name * 'a) list

(** prend comme argument une liste de couples issue de la fonction flat_event,
et supprime de cette liste les transitions dont l'action est anonyme *)
val clean2 : (Uml.name * Uml.transition) list -> (Uml.name * Uml.transition) list

(** g�n�re les op�rations B traduisant les �v�nements UML,
qui fonctionnent comme des d�clencheurs de transitions
(op�rations destin�es � la machine 'syst�me' incluant les autres machines) *)
val event_oper : Uml.uml_class list -> B.operation list


(* concepts li�s aux contraintes OCL *)

(** convertit des invariants OCL en pr�dicats B
(inject�s ensuite dans une clause INVARIANT) *)
val constraint_inv : Uml.ocl_constraint list -> B.predicate list

(** convertit des pr� et post-conditions OCL en pr�-conditions et substitutions B
et les incorpore aux op�rations B appropri�es *)
val constraint_oper :
  B.operation list -> Uml.ocl_constraint list -> B.operation list

(** introduit dans une clause OPERATIONS une liste d'op�rations B *)
val merge_oper : B.clause -> B.operation list -> B.clause




(*** Fonction principale de traduction d'un mod�le UML en machines B ***)

(** convertit un mod�le UML en une liste de machines B *)
val gen_b_ast : Uml.model -> B.amn list




(*** D�compilation des machines B ***)

(** sauvegarde dans le r�pertoire sp�cifi� un jeu de machines B
(fichiers .mch et .ref) *)
val store : string -> B.amn list -> unit




(*** Cr�ation d'un raffinement ***)

(** prend en arguments deux machines B
et fait de la seconde un raffinement de la premi�re *)
val gen_ref : B.amn list -> B.amn list -> B.amn list

(** ??? (non utilis�) ??? *)
val gen_ref_ast : Uml.model -> Uml.model -> Uml.model




(*** Ajout d'un pr�fixe devant les noms d'attributs et d'op�rations
qui apparaissent dans plusieurs classes du mod�le ***)

(** prend comme argument une liste de classes
@return un couple form� de la liste des noms qui apparaissent
        dans plusieurs classes, et de la liste de toutes les classes *)
val find_identical :
  Uml.uml_class list ->
  (Uml.name * int) list -> Uml.name list -> Uml.name list * Uml.name list

(** prend comme argument une liste de noms de classes
@return une liste associative (nom_de_classe, pr�fixe_correspondant),
        tous les pr�fixes �tant diff�rents *)
val find_all_prefixes : string list -> (string * string) list

(** prend comme arguments une liste de classes,
une liste de noms qui apparaissent dans plusieurs classes
et une liste associative (nom_de_classe, pr�fixe_correspondant)
@return la liste de classes, dans lesquelles les noms
        d'attributs et/ou d'op�rations qui apparaissaient
        dans d'autres classes ont �t� pr�fix�s *)
val modif_identical :
  Uml.uml_class list -> Uml.name list -> (Uml.name * string) list ->
  Uml.uml_class list

(** ajout d'un pr�fixe devant les noms d'attributs et d'op�rations
qui apparaissent de fa�on identique dans plusieurs classes
(afin d'�viter les conflits de noms lors de la traduction en machines B) *)
val resolve_name_conflicts : Uml.model -> Uml.model




(*** Chargement du mod�le UML ***)

(** prend en argument un fichier XML susceptible d'�tre pars� par ioXML
(il faut au pr�alable avoir appliqu� sur le fichier XMI que g�n�re Poseidon
la feuille de style xmi2uml.xsl: `run <nom>.xmi`) ;
le deuxi�me param�tre sert �ventuellement � produire des raffinements *)
val uml_to_b : string -> string -> string -> unit

