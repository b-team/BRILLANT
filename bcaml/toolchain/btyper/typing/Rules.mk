FILES = except.ml \
 id.mli \
 id.ml \
 tyenv.mli \
 tyenv.ml \
 lecteur.ml \
 utils.ml \
 typing.mli \
 typing.ml \
 typer.ml

DEPS = Btyperlib Btyperblast Btyperparser Btypertxt

NAME = Btypertyping
MAKELIB = yes
