/* Copyright (C) 1998 Ian Zimmerman <itz@transbay.net> */
static char rcsid[] = "$Id: pty.c,v 1.3 2001/04/28 17:52:22 lefessan Exp $";

#include "config.h"
#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <mlvalues.h>
#include <memory.h>
#include <fail.h>
#include <alloc.h>

#if (defined(__linux))
#include <linux/major.h>
#endif

#ifndef PTY_MASTER_MAJOR
#define PTY_MASTER_MAJOR CONFIGURED_PTY_MAJOR
#endif

#ifdef HAVE_SYS_SYSMACROS_H
#include <sys/sysmacros.h>
#endif

static char pty_err_str[100];

value 
alloc_pt(value dummy)
{
  long fdm = -1;

#ifdef HAVE_DEV_PTMX

  fdm = open("/dev/ptmx", O_RDWR, 0666);

#else

  char *p1, *p2;
  static char name[] = "/dev/pty  ";

  for (p1 = "pqrs"; *p1 != 0; p1++) {
    name[8] = *p1;
    for (p2 = "0123456789abcdef"; *p2 != 0; p2++) {
      name[9] = *p2;
      if (0 <= (fdm = open(name, O_RDWR, 0666)) || ENOENT == errno) {
        goto out;
      } /*if*/
    } /*for*/
  } /*for*/
 out:

#endif  

  if (0 > fdm) {
    snprintf(pty_err_str, 100, "allocate(): %s", strerror(errno));
    failwith(pty_err_str);
  } /*if*/
  return Val_long(fdm);
}

value 
grant_pt(value descr)
{
  long d = Long_val(descr);
  int ret;
  pid_t pid;
  int status;
  static char *argv[] = {"pt_chmod", 0};
  char* child_err_str = "";

#ifdef HAVE_GRANTPT

  ret = grantpt(d);
  if (0 > ret) child_err_str = strerror(errno);

#else

  if (0 == (pid = fork())) {
    /* child */
    if (0 > dup2(d, STDIN_FILENO)) {
      exit(7);
    } /*if*/
    execvp("pt_chmod", argv);
    exit(9);
  } else if (0 > pid) {
    /* error */
    ret = pid;
    child_err_str = strerror(errno);
  } else {
    /* parent */
    ret = waitpid(pid, &status, 0);
    if (pid == ret && WIFEXITED(status)) {
      switch(WEXITSTATUS(status)) {
      case 1: child_err_str = "getgrnam"; ret = -1; break;
      case 2: child_err_str = "fstat"; ret = -1; break;
      case 3: child_err_str = "major"; ret = -1; break;
      case 5: child_err_str = "chown"; ret = -1; break;
      case 6: child_err_str = "chmod"; ret = -1; break;
      case 7: child_err_str = "dup2"; ret = -1; break;
      case 8: child_err_str = "setenv"; ret = -1; break;
      case 9: child_err_str = "execvp"; ret = -1; break;
      } /*switch*/
    } else {
      child_err_str = strerror(errno);
    } /*if*/
  } /*if*/

#endif

  if (0 > ret) {
    snprintf(pty_err_str, 100, "grant(%d): %s", d, child_err_str);
    failwith(pty_err_str);
  } /*if*/
  return Val_unit;
}

value
unlock_pt(value descr)
{
  long d = Long_val(descr);
  int ret;

#ifdef HAVE_UNLOCKPT

  ret = unlockpt(d);

#else

  ret = 0;

#endif

  if (0 > ret) {
    snprintf(pty_err_str, 100, "unlock(%d): %s", d, strerror(errno));
    failwith(pty_err_str);
  } /*if*/
  return Val_unit;
}

value
pts_name(value descr)
{
  long d = Long_val(descr);
  char *p = 0;
  struct stat sb;
  static char name[] = "/dev/tty  ";

#ifdef HAVE_PTSNAME

  p = ptsname(d);

#else

  if (0 <= fstat(d, &sb) && S_ISCHR(sb.st_mode)) 
#if (defined(__linux))
    if (major(sb.st_rdev) == PTY_MASTER_MAJOR) 
#endif
      {
        name[8] = 'p' + minor(sb.st_rdev)/16;
        if (minor(sb.st_rdev)%16 < 10) {
          name[9] = '0' + minor(sb.st_rdev)%16;
        } else {
          name[9] = 'a' - 10 + minor(sb.st_rdev)%16;
        } /*if*/
        p = name;
      } /*if*/
#endif

  if (0 == p) {
    snprintf(pty_err_str, 100, "pts_name(%d): %s", d, strerror(errno));
    failwith(pty_err_str);
  } /*if*/
  return copy_string(p);
}


/* Here begins the hack for being able to claim file tags on a file
   descriptor. Most of it is taken from cash (CAml SHell) */

static value convert_flags_to_list (int flags, value list, int * table, int table_size)
{
  CAMLparam1 (list);
  CAMLlocal1 (tail);
  int i = table_size;

  while (--i >= 0 && flags != 0)
    {
      if (flags & table[i])
        {
          flags &= ~table[i];
          tail = list;
          list = caml_alloc_small (2, Tag_cons);
          Field (list, 0) = Val_int (i);
          Field (list, 1) = tail;
        }
    }
  CAMLreturn (list);
}


#ifndef O_NONBLOCK
#define O_NONBLOCK O_NDELAY
#endif
#ifndef O_DSYNC
#define O_DSYNC 0
#endif
#ifndef O_SYNC
#define O_SYNC 0
#endif
#ifndef O_RSYNC
#define O_RSYNC 0
#endif

/* From cash sources */

/* From open.c (so these funcs should probably go there) */
static int open_flag_table[] = {
  O_RDONLY, O_WRONLY, O_RDWR, O_NONBLOCK, O_APPEND, O_CREAT, O_TRUNC, O_EXCL, 
  O_NOCTTY, O_DSYNC, O_SYNC, O_RSYNC
};

#include <assert.h>

CAMLprim value get_file_tags (value fd)
{
  int r, openmode;
  /*
    The doc says it's needed, the examples of unix library don't use
    it. We follow the doc.
  */
  CAMLparam1(fd); 
  CAMLlocal1(res);

  r = fcntl (Int_val (fd), F_GETFL, 0);
  /*  if (r == -1) uerror ("fdes_status", Nothing); */
  if (r == -1) caml_invalid_argument("get_file_tags");
  res = caml_alloc_small (2, Tag_cons);
  openmode = r & O_ACCMODE;
  Field (res, 0) =
    openmode == O_RDONLY ? Val_int (0) :
    openmode == O_WRONLY ? Val_int (1) :
    openmode == O_RDWR ? Val_int (2) :
    (assert (0), Val_int (0));
  Field (res, 1) = Val_emptylist;
  res = convert_flags_to_list (r & ~O_ACCMODE, res, open_flag_table,
                               sizeof (open_flag_table) / sizeof (open_flag_table[0]));
  CAMLreturn (res);
}
