#!/bin/sed -f

# This script transforms LaTeX accents (macros) in simple ASCII
# filename-compatible characters 

s/{\\["'`~^][ \t]*\\\?\([[:alpha:]]\)}/\1/g
# Spaces in filenames are difficult to handle, script-wise
s/ /_/g
# Remove possible braces used for protecting case
s/{\([a-zA-Z]\+\)}/\1/g
# / and \ ? Bad
s+/+_+g
s+\\+_+g

