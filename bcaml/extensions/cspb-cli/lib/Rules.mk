FILES = b2pred.ml \
 b2string.ml \
 outil.ml \
 cli.ml \
 csptypes.ml \
 csp2string.ml \
 change_name.ml \
 variable.ml \
 redondance.ml \
 cspparser.mly \
 csplexer.mll

DEPS = Bcaml Bparser

NAME = CSPB
MAKELIB = yes
