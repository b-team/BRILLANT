(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅
   Project: BiCoq, formalisation of the B theory into the Coq system
   Module: Bcoqinfer, Coq equivalences of B logical operators
   Some theorems describing the relationship between B logical operators and Coq
   logical operators (for the sort Set) are provided here.
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : October 2005 - October 2006
   ♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Require Export Pprop.

(* Conjunction ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem andcb:forall (G:☐)(P Q:☊), (G⊣P)*(G⊣Q)->G⊣P∧Q.
 Proof.
  intros G P Q H; destruct H; apply (biandi _ _ _ b b0).
 Qed.

 Theorem andbc:forall (G:☐)(P Q:☊), G⊣P∧Q->(G⊣P)*(G⊣Q).
 Proof.
  intros G P Q H; split; [apply (biandl _ _ _ H) | apply (biandr _ _ _ H)].
 Qed.

(* Disjunction ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem orcb:forall (G:☐)(P Q:☊), (G⊣P)+(G⊣Q)->G⊣P∨Q.
 Proof.
  intros G P Q H; destruct H; [apply bioril | apply biorir]; apply b.
 Qed.

 Theorem orbcpl:forall (G:☐)(P Q:☊), G⊣P->G⊣P∨Q->(G⊣P)+(G⊣Q).
 Proof.
  intros G P Q H H0; left; apply H.
 Qed.

 Theorem orbcpr:forall (G:☐)(P Q:☊), G⊣Q->G⊣P∨Q->(G⊣P)+(G⊣Q).
 Proof.
  intros G P Q H H0; right; apply H.
 Qed.

 Theorem orbcrl:forall (G:☐)(P Q:☊), G⊣¬P->G⊣P∨Q->(G⊣P)+(G⊣Q).
 Proof.
  intros G P Q H H0; right; apply (ornotl G P Q H H0).
 Qed.

 Theorem orbcrr:forall (G:☐)(P Q:☊), G⊣¬Q->G⊣P∨Q->(G⊣P)+(G⊣Q).
 Proof.
  intros G P Q H H0; left; apply (ornotr G P Q H H0).
 Qed.
 (* Nota: ∨ does NOT allow to proof \/, additional hypothesis are required. Yet this was *)
 (* to be expected as should G⊣P∨Q->(G⊣P \/ G⊣Q) be provable then, combined with the EM   *)
 (* principle of B, G⊣P∨¬P, it would allow to prove G⊣P \/ G⊣¬P for any predicate P i.e.    *)
 (* the completeness of B predicate calculus. Consider for example S⊆T☑★x∈T⊣(x∈S∨x∈☃S)     *)
 (* while it is not possible to build a proof of S⊆T☑★x∈T⊣x∈S \/ S⊆T☑★x∈T⊣x∈S.                       *)

(* Implication ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem impbc:forall (G:☐)(P Q:☊), G⊣P☲Q->(G⊣P->G⊣Q).
 Proof.
  intros G P Q H H0; apply mp with (P:=P); [apply H0 | apply H].
 Qed.

 Theorem impcbpl:forall (G:☐)(P Q:☊), G⊣P->(G⊣P->G⊣Q)->G⊣P☲Q.
 Proof.
  intros G P Q H H0; apply biimpi; apply biins; apply (H0 H).
 Qed.

 Theorem impcbpr:forall (G:☐)(P Q:☊), G⊣Q->(G⊣P->G⊣Q)->G⊣P☲Q.
 Proof.
  intros G P Q H H0; apply biimpi; apply biins; apply H.
 Qed.

 Theorem impcbrl:forall (G:☐)(P Q:☊), G⊣¬P->(G⊣P->G⊣Q)->G⊣P☲Q.
 Proof.
  intros G P Q H H0; apply biimpi; apply biabsn with (P:=P); [BTprop | BThyp; apply H].
 Qed.
 (* Nota: -> does NOT allow to proof ☲, additional hypothesis are required. Once more     *)
 (* this was expected as illustrated by:                                                                                      *)
 (* Theorem Bfalse:forall (S T:☌)(x:☈), x⍀☍S->x⍀☍T->∅☑☑★S⊆T☑★χ(x)∈T⊣χ(x)∈S->∅☑☑★S⊆T☑★χ(x)∈T⊣S♃T.   *)
 (* Proof.                                                                                                                                             *)
 (*  intros S T x HS HT H; assert (H':∅☑☑★S⊆T⊣T⊆S).                                                                             *)
 (*   unfold pinc; apply (bipowi (∅☑☑★S⊆T) x T S HT HS); apply bifori; simpl.                                *)
 (*    apply ginfdec;                                                                                                                           *)
 (*    [unfold pinc; apply infins; [apply HS | apply infpow; apply HT] | apply ginfempty].  *)
 (*    apply biimpi; apply H.                                                                                                              *)
 (*  apply bieqsi; [unfold pinc; BTprop | apply biins; apply H'].                                              *)
 (* Qed.                                                                                                                                                 *)
 (* ... while no proof of ∅☑☑★S⊆T☑★χ(x)∈T⊣χ(x)∈S☲S♃T can be provided.                                            *)

(* Universal quantification ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem forallcb:forall (G:☐)(v:☈)(P:☊), (forall (w:☈), G⊣ 〈v♇☍χ w@☋P〉)->G⊣ ☬☢(v∙P).
 Proof.
  intros G v P H; destruct (exfgnewf (G☑★P)); destruct (ginfinc _ _ _ g); clear g;
   rewrite (fpbdforalpha _ v _ p); apply (bifori _ _ 〈v♇☍χ(x)@☋P〉 g0); apply H.
 Qed.

 Theorem forallcbexp:forall (G:☐)(v:☈)(P:☊), (forall (E:☌), G⊣ 〈v♇☍E@☋P〉)->G⊣ ☬☢(v∙P).
 Proof.
  intros G v P H; apply forallcb; intros w; apply H.
 Qed.

 Theorem forallbcexp:forall (G:☐)(v:☈)(P:☊), G⊣ ☬☢(v∙P)->forall (E:☌), G⊣ 〈v♇☍E@☋P〉.
 Proof.
  intros G v P H E; apply bifore; apply H.
 Qed.

 Theorem forallbc:forall (G:☐)(v:☈)(P:☊), G⊣ ☬☢(v∙P)->(forall (w:☈), G⊣ 〈v♇☍χ w@☋P〉).
 Proof.
  intros G v P H w; apply forallbcexp; apply H.
 Qed.

(* Existential quantification ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem existcbexp:forall (G:☐)(v:☈)(P:☊), {E:☌ & G⊣ 〈v♇☍E@☋P〉}->G⊣ ☬☣(v∙P).
 Proof.
  intros G v P H; destruct H; rewrite fpbdforexs; apply biabsp with (P:=〈v♇☍x@☋P〉).
   apply biins; apply b.
   replace (¬ 〈v♇☍x@☋P〉) with 〈v♇☍x@☋¬P〉; [apply bifore; BTprop | apply refl_equal].
 Qed.

 Theorem existcb:forall (G:☐)(v:☈)(P:☊), {x:☈ & G⊣ 〈v♇☍χ(x)@☋P〉}->G⊣ ☬☣(v∙P).
 Proof.
  intros G v P H; destruct H; apply existcbexp; exists χ(x); apply b.
 Qed.

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)
