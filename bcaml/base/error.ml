(* $Id$ *)

(*
  Copyright (c) 1999 Christian Lindig <lindig@ips.cs.tu-bs.de>. 
  Allrights reserved. See COPYING for details.
  Global error and warning treatment.
*)

(*
  Updated for BCaml use...
*)

(* The universal error exception  *)

exception Error of string

(* 
   With all the beta and alpha tools around here, 
   this exception is not superfluous imho 
*)

exception Not_implemented of string


let error msg	= raise (Error msg)

let is_debug = ref false

let debug_set () = is_debug := true

let debug_mode () = (!is_debug = true)  

let debug msg =   
  if  (debug_mode ()) then 
    prerr_string (msg^"\n") ; flush stderr

let message msg = 
  prerr_string msg; flush stderr

(* [warning] issues a warning to stderr. All critical errors should be
reported by this function *)

let warning msg =  
  prerr_endline ("--Bwarn " ^ msg); flush stderr


let env_variable s =
  warning ("You should define the environment variable " ^ s); 
  raise (Sys_error "Variable undefined")

(* Some tools can go on working partially, so this function does not
raise an exception *)
let not_implemented msg =
  debug ( msg ^ ": not yet implemented, sorry")


(* This function should replace the two next *)
let success description action=
  message ("--Bhappy" ^ " <" ^ description ^ "> " ^ action)


let parsing_success file =   
  message ("--Bhappy "^"<"^file^">"^ " parsed")


let scopping_success file =   
  message ("--Bhappy "^"<"^file^">"^ " scopped")


let parsing_failure file line col msg =
  let mess = ("--Bwarn "
	      ^" <"^file^">"
	      ^ " l:c " ^ (Printf.sprintf  "%4s"  (string_of_int line))
	      ^(Printf.sprintf  "%4s"  (string_of_int col)) 
	      ^"\t["^msg^"] "
	     ) in
    message mess 
    
