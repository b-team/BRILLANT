exception NotYetImplemented of (string * string)
exception NoInitialisation of string
exception NoInvariant of string
val amn : Blast.amn -> Bhll.BMod.definition
val head : Blast.head -> Blast.id * Blast.id list
val clause : Blast.clause -> Blast.clause list -> Bhll.BMod.definition list
val setname_to_btype : Bhll.B.setName -> Bhll.B.btype
val btype_of_setName : Bhll.B.setName -> Bhll.B.btype
val search_btype_in_exprSet : Bhll.B.exprSet -> Bhll.B.btype
val search_btype_in_expr : Bhll.B.expr -> Bhll.B.btype
val search_btype : Blast.predicate -> Modules.Ident.t -> Bhll.B.btype
val string_of_id : Modules.Ident.t -> string
val equalid : Modules.path -> Modules.path -> bool
val mem_id_in_idlist : Modules.path -> Bhll.paths -> bool
val search_sub : Blast.substitution -> Modules.Ident.t -> Blast.substitution
val operation : Blast.operation -> Bhll.BMod.definition
val predicate : Blast.predicate -> Blast.predicate
val instance : Blast.instance -> Modules.path
val expr : Blast.expr -> Blast.expr
val exprSeq : Bhll.B.exprSeq -> Bhll.B.exprSeq
val exprSet : 'a -> 'a
val setname : 'a -> 'a
val number : 'a -> 'a
val substitution : Blast.substitution -> Blast.substitution
val set : Blast.set -> Bhll.BMod.definition
val id : Blast.id -> Blast.id
val processing : Blast.amn -> Bhll.BMod.definition
