(*******************************************************)
(*                                                     *)
(*           Copyright 2004 J�r�me Rocheteau           *)
(*                                                     *)
(*   This file is distributed under the terms of the   *)
(*    GNU Lesser General Public License Version 2.1    *)
(*                                                     *)
(*******************************************************)

(** Multi Prover Library *)

let get = 
  let rec f = function
      Scan.Tag ("symb",["value",x],l) -> 
	(try 
	  let rec h = fun t -> function 
	      [] -> t 
	    | u :: l -> h (Term.App (t,u)) l
	  in 
	  let r = Base.find_term x 
	  in h (Term.Cst (Symb.symbol_of x r)) (List.map (fun u -> f u) l)
	with _ -> Term.Var x)
    | Scan.Tag ("bind",("symb",x)::xs,[t]) -> 
	let rec h = fun u w -> function 
	    [] -> w
	  | ("value",v) :: l -> h u (Term.App (u,Term.Abs (v,w))) l
	  | _ -> assert false
	in 
	  (try 
	     let r = Base.find_term x in
	     let s = Term.Cst (Symb.symbol_of x r) 
	     in h s (f t) (List.rev xs)
	   with _ -> h (Term.Var x) (f t) (List.rev xs))
    | _ -> assert false
  in function 
	Scan.Tag ("let",["value",x],[t]) -> (x,Base.Theo (f t))
    | _ -> assert false

let check = function 
    (x,Base.Theo t) -> 
      if (Term.base t) = []
      then  
	let ty = Term.infer t in 
	  if ty = Type.prop 
	  then let nt = Term.eval t in (t,nt,ty) 
	  else assert false
      else assert false
  | _ -> assert false

let rec run = fun f -> function
    Term.Var x -> x
  | Term.Cst c -> f (Symb.string_of c)
  | Term.App (Term.Cst c,Term.Abs (x,t)) -> 
      (f (Symb.string_of c)) ^ x ^ " " ^ (run f t)
  | Term.App (Term.App (Term.Cst c,u),v) -> 
      "(" ^ (run f u) ^ " " ^ (f (Symb.string_of c)) ^ " " ^ (run f v) ^ ")"
  | Term.App (u,v) -> (run f u) ^ " " ^ (run f v)
  | Term.Abs (x,t) -> (f "abs") ^ x ^ " " ^ (run f t)

let text = function
    (x,Base.Theo t) -> "\nfact [" ^ x ^ "] " ^ (run Base.text t) ^ "\n"
  | _ -> assert false

let latex = function 
    (x,Base.Theo t) -> 
      "\n\\begin{fact}\n" 
      ^ "\\label{fact:" ^ x ^ "}\n" 
      ^ "\\begin{math}\n"
      ^ (run Base.latex t) ^ "\n" 
      ^ "\\end{math}\n"
      ^ "\\end{fact}\n" 
  | _ -> assert false

let phox = function
    (x,Base.Theo t) -> 
      "\nfact " ^ x ^ " " ^ (run Base.phox t) ^ ".\n"
      ^ "trivial.\n" 
      ^ "save.\n" 
  | _ -> assert false

let provers = ref ["text"]

let prover = ref "text"

let extension = ref ".txt"

let select = function 
    "text" -> prover := "text"; extension := ".txt"
  | "latex" -> prover := "latex"; extension := ".tex"
  | "phox" -> prover := "phox"; extension := ".phx"
  | _ -> prover := "text"; extension := ".txt"

let init = fun oc -> 
  match !prover with 
      "text" -> output_string oc ""
    | "latex" -> output_string oc ""
    | "phox" -> output_string oc ""
    | _ -> output_string oc ""

let achieve = fun oc -> 
  match !prover with 
      "text" -> output_string oc "\n"
    | "latex" -> output_string oc "\n"
    | "phox" -> output_string oc "\n"
    | _ -> output_string oc "\n"

let print = fun oc t -> 
  let u = get t in
  let (tt,nt,ty) = check u in 
    match !prover with 
	"text" -> output_string oc (text u)
      | "latex" -> output_string oc (latex u)
      | "phox" -> output_string oc (phox u)
      | _ -> output_string oc (text u)
