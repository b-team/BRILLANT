(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter2.
Require Import Classical_sets.
Require Import Bfinite_subsets.
Require Import Bintegers_basics.
Require Import ZArith.

Open Local Scope Z_scope.

(**
  * Minimum: definitions
  *)

Definition Bmin (s: Ensemble Z) := iota _ inhabited_Z (fun x => In s x /\ (forall y, In s y -> x <= y)).

(** TODO: move this in Bintegers_basics *)
Definition BNegatives := Comprehension (fun x => x < 0).

(**
  * Minimum: theorems
  *)

Theorem minimum_property_implies_is_minimum: forall (s: Ensemble Z) (x: Z),
  In s x -> (forall (n: Z), In s n -> x <= n) -> x = Bmin s.
Proof.
intros s x H H0.
unfold Bmin; apply iota_ind_weak.
intros.
destruct H1.
apply Zle_antisym; intuition.
exists x.
split; [ intuition | ].
intros.
destruct H1.
apply Zle_antisym; intuition.
Qed.

Definition has_unique_least_element (A:Type) (R:A->A->Prop) (P:A->Prop) :=
  exists! x, P x /\ forall x', P x' -> R x x'.

Lemma dec_inh_Z_subset_with_infimum_has_unique_least_element : forall P:Z->Prop,
  (forall n, P n \/ ~ P n) ->
  (exists infimum, forall n, P n -> Zle infimum n) ->
  (exists n, P n) -> has_unique_least_element Z Zle P.
Proof.
intros P Pdec Hinf.
destruct Hinf.
assert
  (forall n, (exists n', n'<n /\ P n' /\ forall n'', P n'' -> n'<=n'')
    \/(forall n', P n' -> n<=n')).
intros n; replace n with ((n-x)+x).
pattern (n-x).
apply Zind.
right; intuition.
intros x0 IHn0; destruct IHn0.
left; destruct H0 as (n', (Hlt', HPn')).
exists n'; split.
rewrite <- Zsucc_succ'; auto with zarith.
assumption.
destruct (Pdec (x0 + x)).
left; exists (x0 + x); split.
rewrite <- Zsucc_succ'; auto with zarith.
split; assumption.
right.
intros n' HPn'.
destruct (Ztrichotomy (x0+x) n') as [ H2 | H2 ].
rewrite <- Zsucc_succ'; auto with zarith.
destruct H2.
destruct H1; rewrite H2; assumption.
assert (~ x0 + x > n').
apply Zle_not_gt; apply H0; assumption.
contradiction.
(* The additional part wrt nat (i.e. downwards comparisons) starts here *)
intros x0 IHn0; destruct IHn0.
destruct H0 as (n', (Hlt', HPn')).
assert (n' <= Zpred (x0 + x)) as H0.
replace n' with (Zpred (Zsucc n')).
unfold Zpred; auto with zarith.
symmetry; apply Zpred_succ.
generalize (Zle_lt_or_eq _ _ H0); intros H1.
destruct H1.
left; exists n'; rewrite <- Zpred_pred'.
split; [ unfold Zpred in H1 |- *; auto with zarith | intuition ].
assert (n' = Zpred' x0 + x) as H2.
rewrite <- Zpred_pred'; unfold Zpred in H1 |- *; auto with zarith.
right; rewrite <- H2; exact (proj2 HPn').
right.
intros n' HPn'; rewrite <- Zpred_pred'; unfold Zpred.
specialize (H0 n' HPn'); auto with zarith.
auto with zarith.
intros H1; destruct H1 as [ n0 HPn0 ].
destruct (H0 n0) as [(n,(Hltn,(Hmin,Huniqn)))|]; [exists n | exists n0];
  repeat split;
    assumption || intros n' (HPn',Hminn'); apply Zle_antisym; auto.
Qed.

Lemma Zge_antisym : forall n m:Z, n >= m -> m >= n -> n = m.
Proof.
intros n m H H0.
apply Zle_antisym; intuition.
Qed.

Lemma dec_inh_Z_subset_with_supremum_has_unique_greatest_element : forall P:Z->Prop,
  (forall n, P n \/ ~ P n) ->
  (exists supremum, forall n, P n -> Zge supremum n) ->
  (exists n, P n) -> has_unique_least_element Z Zge P.
Proof.
intros P Pdec Hsup.
destruct Hsup.
assert
  (forall n, (exists n', n'>n /\ P n' /\ forall n'', P n'' -> n'>=n'')
    \/(forall n', P n' -> n>=n')).
intros n; replace n with ((n-x)+x).
pattern (n-x).
apply Zind.
(** First part *)
right; intuition.
(** Second part *)
intros x0 IHn0; destruct IHn0.
destruct H0 as (n', (Hgt', HPn')).
assert (n' >= Zsucc (x0 + x)) as H0.
replace n' with (Zsucc (Zpred n')).
unfold Zpred; auto with zarith.
symmetry; apply Zsucc_pred.
generalize (Zge_le _ _ H0); intros H1.
generalize (Zle_lt_or_eq _ _ H1); intros H2.
destruct H2.
left; exists n'; rewrite <- Zsucc_succ'.
split; [ unfold Zsucc in H2 |- *; auto with zarith | intuition ].
assert (n' = Zsucc' x0 + x) as H3.
rewrite <- Zsucc_succ';auto with zarith.
right; rewrite <- H3; exact (proj2 HPn').
right.
intros n' HPn'; rewrite <- Zsucc_succ'.
specialize (H0 n' HPn'); auto with zarith.
(** Third part *)
intros x0 IHn0; destruct IHn0.
left; destruct H0 as (n', (Hgt', HPn')).
exists n'; split.
rewrite <- Zpred_pred'; unfold Zpred; auto with zarith.
assumption.
destruct (Pdec (x0 + x)).
left; exists (x0 + x); split.
rewrite <- Zpred_pred'; unfold Zpred; auto with zarith.
split; assumption.
right.
intros n' HPn'.
destruct (Ztrichotomy (x0+x) n') as [ H2 | H2 ].
assert (~ x0 + x < n').
apply Zle_not_lt; apply Zge_le; apply H0; assumption.
contradiction.
destruct H2.
destruct H1; rewrite H2; assumption.
rewrite <- Zpred_pred'; unfold Zpred; auto with zarith.
auto with zarith.
intros H1; destruct H1 as [ n0 HPn0 ].
destruct (H0 n0) as [(n,(Hgtn,(Hmin,Huniqn)))|]; [exists n | exists n0];
  repeat split;
    assumption || intros n' (HPn',Hminn'); apply Zge_antisym; auto.
Qed.

Theorem finite_has_extremums: forall (s : Ensemble Z),
  In (Power_finite1 BZ) s ->
  (exists inf:Z, exists sup: Z, forall n, In s n -> (Zle inf n /\ Zge sup n)).
Proof.
intros s H.
destruct H as [ H Hnotempty ].
assert (Inhabited Z s) as Hinhs.
apply not_empty_Inhabited.
intros H0; apply Hnotempty; rewrite H0; intuition.
clear Hnotempty.
destruct H as [ H HsincZ ].
induction H as [ H | t H IHn x H0 ].
absurd (Empty_set Z = Empty_set Z); [ apply Inhabited_not_empty; assumption | reflexivity ].
assert (t = Empty_set Z \/ ~ (t = Empty_set Z)) as H1; [ apply classic | ].
destruct H1.
exists x; exists x.
intros n H2.
destruct H2.
rewrite H1 in H2; contradiction.
destruct H2; intuition.
assert (Inhabited Z t) as H2; [ apply not_empty_Inhabited; assumption | ].
assert (Included t BZ) as H3; [ intuition | ].
specialize (IHn H3 H2).
destruct IHn as [ inf IHn ].
destruct IHn as [ sup IHn ].
assert (inf <= sup) as H4.
destruct H2.
specialize (IHn x0 H2); destruct IHn.
apply Zle_trans with x0; intuition.
specialize (Zle_or_lt x inf); intros H5; destruct H5 as [ H5 | H5 ].
exists x; exists sup.
intros n H6.
destruct H6.
specialize (IHn x0 H6); destruct IHn; intuition.
destruct H6; intuition.
specialize (Zle_or_lt sup x); intros H6; destruct H6 as [ H6 | H6 ].
exists inf; exists x.
intros n H7.
destruct H7.
specialize (IHn x0 H7); destruct IHn; intuition.
destruct H7; intuition.
exists inf; exists sup.
intros n H7.
destruct H7.
specialize (IHn x0 H7); destruct IHn; intuition.
destruct H7; intuition.
Qed.

Theorem downwards_finite_has_infimum: forall (s : Ensemble Z),
  In (Power_finite BZ) (Intersection s BNegatives) ->
  (exists infimum:Z, forall n, In s n -> Zle infimum n).
Proof.
intros s H.
assert (exists infimum:Z, forall n, In (Intersection s BNegatives) n -> Zle infimum n).
destruct H as [ H H0 ].
induction H as [ H | t H IHn x H2 ].
exists Z0; intros; contradiction.
assert (Included t BZ) as H1; [ intuition | ].
specialize (IHn H1).
destruct IHn as [ x0 H3 ].
specialize (Zle_or_lt x x0); intros H4; destruct H4 as [ H4 | H4 ].
exists x; intros n H5.
destruct H5.
apply Zle_trans with x0; [ | apply H3 ]; assumption.
destruct H5; intuition.
exists x0; intros n H5.
destruct H5.
apply H3; assumption.
destruct H5; intuition.
destruct H0 as [ x H0 ].
assert (forall (n: Z), In (Intersection s BN) n -> 0 <= n) as H1.
intros n H1.
destruct H1 as [ n H1 H2 ].
do 3 (red in H2); intuition.
assert (s = (Union (Intersection s BNegatives) (Intersection s BN))) as H2.
rewrite <- distributivity_1.
replace (Union BNegatives BN) with BZ.
symmetry; apply neutral_element_2.
red; intros; constructor.
apply Extensionality_Ensembles; split; red; intros.
destruct (Zle_or_lt 0 x0) as [ H3 | H3 ].
right; do 3 red; intuition.
left; do 3 red; intuition.
constructor.
assert ((Intersection s BNegatives) = (Empty_set Z) \/ ~((Intersection s BNegatives) = (Empty_set Z))) as H3; [ apply classic | ].
destruct H3.
exists 0.
rewrite H2; rewrite H3.
rewrite commutativity_1; rewrite neutral_element_1; assumption.
exists x.
rewrite H2; intros n H4.
destruct H4 as [ n H4 | n H4 ].
apply H0; assumption.
specialize (not_empty_Inhabited _ _ H3); intros H5.
destruct H5 as [ x0 H5 ].
destruct H4 as [ n H4 H6 ].
do 3 (red in H6).
apply Zle_trans with x0.
apply H0; intuition.
apply Zle_trans with 0.
destruct H5 as [ x0 H5 H7].
do 3 (red in H7); intuition.
intuition.
Qed.

Theorem upwards_finite_has_supremum: forall (s : Ensemble Z),
  In (Power_finite BZ) (Intersection s BN) ->
  (exists supremum:Z, forall n, In s n -> Zge supremum n).
Proof.
intros s H.
assert (exists supremum:Z, forall n, In (Intersection s BN) n -> Zge supremum n).
destruct H as [ H H0 ].
induction H as [ H | t H IHn x H2 ].
exists Z0; intros; contradiction.
assert (Included t BZ) as H1; [ intuition | ].
specialize (IHn H1).
destruct IHn as [ x0 H3 ].
specialize (Zle_or_lt x x0); intros H4; destruct H4 as [ H4 | H4 ].
exists x0; intros n H5.
destruct H5.
apply H3; assumption.
destruct H5; intuition.
exists x; intros n H5.
destruct H5.
apply Zge_trans with x0; [ | apply H3 ]; intuition.
destruct H5; intuition.
destruct H0 as [ x H0 ].
assert (forall (n: Z), In (Intersection s BNegatives) n -> 0 >= n) as H1.
intros n H1.
destruct H1 as [ n H1 H2 ].
do 3 (red in H2); intuition.
assert (s = (Union (Intersection s BNegatives) (Intersection s BN))) as H2.
rewrite <- distributivity_1.
replace (Union BNegatives BN) with BZ.
symmetry; apply neutral_element_2.
red; intros; constructor.
apply Extensionality_Ensembles; split; red; intros.
destruct (Zle_or_lt 0 x0) as [ H3 | H3 ].
right; do 3 red; intuition.
left; do 3 red; intuition.
constructor.
assert ((Intersection s BN) = (Empty_set Z) \/ ~((Intersection s BN) = (Empty_set Z))) as H3; [ apply classic | ].
destruct H3.
exists 0.
rewrite H2; rewrite H3.
rewrite neutral_element_1; assumption.
exists x.
rewrite H2; intros n H4.
destruct H4 as [ n H4 | n H4 ].
specialize (not_empty_Inhabited _ _ H3); intros H5.
destruct H5 as [ x0 H5 ].
destruct H4 as [ n H4 H6 ].
do 3 (red in H6).
apply Zge_trans with x0.
apply H0; intuition.
apply Zge_trans with 0.
destruct H5 as [ x0 H5 H7].
do 3 (red in H7); intuition.
intuition.
apply H0; assumption.
Qed.

Theorem Bmin_is_minimal: forall (s: Ensemble Z), s <> (Empty_set Z) -> In (Power_finite BZ) (Intersection s BNegatives) ->
  (forall (n:Z), In s n -> Bmin s <= n).
Proof.
intros s H H0 n H1.
assert (forall (z: Z), In s z \/ ~(In s z)); [ intros z; apply classic | ].
assert (exists infimum, forall n, In s n -> Zle infimum n); [ apply downwards_finite_has_infimum; assumption | ].
assert (exists x, In s x); [ exists n; assumption | ].
generalize (dec_inh_Z_subset_with_infimum_has_unique_least_element s H2 H3 H4); intros.
assert (In s (Bmin s) /\ (forall (y: Z), In s y -> Bmin s <= y)).
pattern (Bmin s).
unfold Bmin; apply iota_ind_weak.
intuition.
unfold has_unique_least_element in H5.
exact H5.
destruct H6 as [ H6 H7 ].
apply H7; assumption.
Qed.

(**
  * Minimum: miscellaneous properties
  *)
Theorem Bmin_singleton: forall (a: Z), Bmin (Singleton a) = a.
Proof.
intros a.
unfold Bmin; apply iota_ind_weak.
intros b H.
destruct H as [ H H0 ]; destruct H; reflexivity.
exists a.
split.
split; [ intuition | intros y H1; destruct H1; intuition ].
intros.
destruct H as [ H H0 ]; destruct H; reflexivity.
Qed.

Theorem Bmin_doubleton1: forall (a b:Z), a <= b -> Bmin {= a, b =} = a.
Proof.
intros a b H.
unfold Bmin; apply iota_ind_weak.
intros b0 H0.
destruct H0 as [ H0 H1 ].
destruct H0.
destruct H0; reflexivity.
destruct H0.
specialize H1 with a.
apply Zle_antisym; [ apply H1; intuition | assumption ].
exists a.
split.
split; [ intuition | ].
intros y H0.
destruct H0; destruct H0; intuition.
intros.
destruct H0 as [ H0 H1 ].
destruct H0.
destruct H0; reflexivity.
destruct H0.
specialize H1 with a.
apply Zle_antisym; [ assumption | apply H1; intuition ].
Qed.

Theorem Bmin_doubleton2: forall (a b:Z), b <= a -> Bmin {= a, b =} = b.
Proof.
intros a b H.
rewrite commutativity_1.
apply Bmin_doubleton1; assumption.
Qed.
