/* config.h.  Generated automatically by configure.  */
/* config.h.in.  Generated automatically from configure.in by autoheader.  */

/* Define as the return type of signal handlers (int or void).  */
#define RETSIGTYPE void

/* Define to `unsigned' if <sys/types.h> doesn't define.  */
/* #undef size_t */

/* Define if you have the ANSI C header files.  */
#define STDC_HEADERS 1

/* Define if the X Window System is missing or not being used.  */
/* #undef X_DISPLAY_MISSING */

#define WITH_LIBDL 1
/* #undef WITH_LIBDLD */

/* #undef CONFIGURED_PTY_MAJOR */
#define HAVE_DEV_PTMX 1

/* Leave that blank line there-- autoheader needs it! */

#define HAVE_JPEG 1
#define HAVE_TIFF 1
#define HAVE_FREETYPE 1
#define HAVE_FREETYPE2 1
#define HAVE_GIF 1
#define HAVE_PNG 1
#define HAVE_XPM 1

/* #undef BOGUS_LIBGIF */

/* The number of bytes in a char.  */
#define SIZEOF_CHAR 1

/* The number of bytes in a double.  */
#define SIZEOF_DOUBLE 8

/* The number of bytes in a float.  */
#define SIZEOF_FLOAT 4

/* The number of bytes in a int.  */
#define SIZEOF_INT 4

/* The number of bytes in a long.  */
#define SIZEOF_LONG 4

/* The number of bytes in a short.  */
#define SIZEOF_SHORT 2

/* The number of bytes in a void *.  */
#define SIZEOF_VOID_P 4

/* Define if you have the grantpt function.  */
#define HAVE_GRANTPT 1

/* Define if you have the ptsname function.  */
#define HAVE_PTSNAME 1

/* Define if you have the unlockpt function.  */
#define HAVE_UNLOCKPT 1

/* Define if you have the <GL/gl.h> header file.  */
#define HAVE_GL_GL_H 1

/* Define if you have the <GL/glu.h> header file.  */
#define HAVE_GL_GLU_H 1

/* Define if you have the <GL/glx.h> header file.  */
#define HAVE_GL_GLX_H 1

/* Define if you have the <X11/Xlib.h> header file.  */
#define HAVE_X11_XLIB_H 1

/* Define if you have the <bzlib.h> header file.  */
/* #undef HAVE_BZLIB_H */

/* Define if you have the <dlfcn.h> header file.  */
#define HAVE_DLFCN_H 1

/* Define if you have the <errno.h> header file.  */
#define HAVE_ERRNO_H 1

/* Define if you have the <fcntl.h> header file.  */
#define HAVE_FCNTL_H 1

/* Define if you have the <glade/glade.h> header file.  */
/* #undef HAVE_GLADE_GLADE_H */

/* Define if you have the <gtk-xmhtml/gtk-xmhtml.h> header file.  */
/* #undef HAVE_GTK_XMHTML_GTK_XMHTML_H */

/* Define if you have the <gtkgl/gtkglarea.h> header file.  */
/* #undef HAVE_GTKGL_GTKGLAREA_H */

/* Define if you have the <pgsql/libpq-fe.h> header file.  */
/* #undef HAVE_PGSQL_LIBPQ_FE_H */

/* Define if you have the <pgsql/libpq/libpq-fs.h> header file.  */
/* #undef HAVE_PGSQL_LIBPQ_LIBPQ_FS_H */

/* Define if you have the <plot.h> header file.  */
/* #undef HAVE_PLOT_H */

/* Define if you have the <pthread.h> header file.  */
#define HAVE_PTHREAD_H 1

/* Define if you have the <signal.h> header file.  */
#define HAVE_SIGNAL_H 1

/* Define if you have the <string.h> header file.  */
#define HAVE_STRING_H 1

/* Define if you have the <sys/mman.h> header file.  */
#define HAVE_SYS_MMAN_H 1

/* Define if you have the <sys/select.h> header file.  */
#define HAVE_SYS_SELECT_H 1

/* Define if you have the <sys/stat.h> header file.  */
#define HAVE_SYS_STAT_H 1

/* Define if you have the <sys/sysmacros.h> header file.  */
#define HAVE_SYS_SYSMACROS_H 1

/* Define if you have the <sys/time.h> header file.  */
#define HAVE_SYS_TIME_H 1

/* Define if you have the <sys/types.h> header file.  */
#define HAVE_SYS_TYPES_H 1

/* Define if you have the <tcl.h> header file.  */
/* #undef HAVE_TCL_H */

/* Define if you have the <tk.h> header file.  */
/* #undef HAVE_TK_H */

/* Define if you have the <unistd.h> header file.  */
#define HAVE_UNISTD_H 1

/* Define if you have the <zlib.h> header file.  */
#define HAVE_ZLIB_H 1

/* Define if you have the bz2 library (-lbz2).  */
/* #undef HAVE_LIBBZ2 */

/* Define if you have the m library (-lm).  */
#define HAVE_LIBM 1

/* Define if you have the z library (-lz).  */
/* #undef HAVE_LIBZ */

#ifdef __CYGWIN__
#ifdef USE_DL_IMPORT
#define DL_IMPORT(RTYPE) __declspec(dllimport) RTYPE
#define DL_EXPORT(RTYPE) __declspec(dllexport) RTYPE
#else
#define DL_IMPORT(RTYPE) __declspec(dllexport) RTYPE
#define DL_EXPORT(RTYPE) __declspec(dllexport) RTYPE
#endif
#endif
