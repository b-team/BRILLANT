 

Require Import Blib.

Theorem op:
(In (Power_finite BN1) (Empty_set _)).
try intuition 
   || unfold BBOOL ; intuition ; apply Full_intro
   || red ;  red ; red ; omega.
Qed.
 