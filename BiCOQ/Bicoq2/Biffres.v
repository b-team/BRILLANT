(*☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   Project: BiCoq, formalisation of the B theory into the Coq system
   Module: Biffres, results about equivalence
   ☳ is a powerful tool allowing replacement of predicates in sequents ; various
   theorems are provided in this module providing either ☳ or replacement schemes.
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : May 2006 - June 2006
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Require Export Pinfer2.

(* Basic ☲-replacement rules for propositional calculus ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem imprepgoal:forall (G:☐)(P P':☊), G⊣P☲P'->G⊣P->G⊣P'.
 Proof.
  intros G P P' H H0; apply mp with (P:=P); [apply H0 | apply H].
 Qed.

 Theorem imprepnot:forall (G:☐)(P P':☊), G⊣P'☲P->G⊣¬P->G⊣¬P'.
 Proof.
  intros G P P' H H0; apply biabsp with (P:=P);
   [apply biimpe; apply H | apply biins; apply H0].
 Qed.

 Theorem imprepandl:forall (G:☐)(P P' Q:☊), G⊣P☲P'->G⊣P∧Q->G⊣P'∧Q.
 Proof.
  intros G P P' Q H H0; destruct (andbc _ _ _ H0); apply biandi;
   [apply (imprepgoal _ _ _ H b) | apply b0].
 Qed.

 Theorem imprepandr:forall (G:☐)(P Q Q':☊), G⊣Q☲Q'->G⊣P∧Q->G⊣P∧Q'.
 Proof.
  intros G P Q Q' H H0; destruct (andbc _ _ _ H0); apply biandi;
   [apply b | apply (imprepgoal _ _ _ H b0)].
 Qed.

 Theorem imprepimpl:forall (G:☐)(P P' Q:☊), G⊣P'☲P->G⊣P☲Q->G⊣P'☲Q.
 Proof.
  intros G P P' Q H H0; apply contrapn; apply biimpi;
   apply (imprepnot _ _ _ (biins _ _ (¬Q) H)); apply biimpe; apply contrapp; apply H0.
 Qed.

 Theorem imprepimpr:forall (G:☐)(P Q Q':☊), G⊣Q☲Q'->G⊣P☲Q->G⊣P☲Q'.
 Proof.
  intros G P Q Q' H H0; apply biimpi; apply (imprepgoal _ _ _ (biins _ _ P H)); apply biimpe;
   apply H0.
 Qed.

 Theorem imprephyp:forall (G:☐)(P P' Q:☊), G⊣P'☲P->G ☑★ P⊣Q->G ☑★ P'⊣Q.
 Proof.
  intros G P P' Q H H0; apply biimpe; apply (imprepimpl _ _ _ Q H); apply biimpi; apply H0.
 Qed.

(* Basic ☳-replacement rules for propositional calculus ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem iffrepgoal:forall (G:☐)(P P':☊), G⊣P☳P'->G⊣P->G⊣P'.
 Proof.
  intros G P P' H H0; apply (imprepgoal _ _ _ (diffimp _ _ _ H) H0).
 Qed.

 Theorem iffrepnot:forall (G:☐)(P P':☊), G⊣P☳P'->G⊣¬P->G⊣¬P'.
 Proof.
  intros G P P' H H0; apply (imprepnot _ _ _ (riffimp _ _ _ H) H0).
 Qed.

 Theorem iffrepandl:forall (G:☐)(P P' Q:☊), G⊣P☳P'->G⊣P∧Q->G⊣P'∧Q.
 Proof.
  intros G P P' Q H H0; apply (imprepandl _ _ _ _ (diffimp _ _ _ H) H0).
 Qed.

 Theorem iffrepandr:forall (G:☐)(P Q Q':☊), G⊣Q☳Q'->G⊣P∧Q->G⊣P∧Q'.
 Proof.
  intros G P Q Q' H H0; apply (imprepandr _ _ _ _ (diffimp _ _ _ H) H0).
 Qed.

 Theorem iffrepimpl:forall (G:☐)(P P' Q:☊), G⊣P☳P'->G⊣P☲Q->G⊣P'☲Q.
 Proof.
  intros G P P' Q H H0; apply (imprepimpl _ _ _ _ (riffimp _ _ _ H) H0).
 Qed.

 Theorem iffrepimpr:forall (G:☐)(P Q Q':☊), G⊣Q☳Q'->G⊣P☲Q->G⊣P☲Q'.
 Proof.
  intros G P Q Q' H H0; apply (imprepimpr _ _ _ _ (diffimp _ _ _ H) H0).
 Qed.

 Theorem iffrephyp:forall (G:☐)(P P' Q:☊), G⊣P☳P'->G ☑★ P⊣Q->G ☑★ P'⊣Q.
 Proof.
  intros G P P' Q H H0; apply (imprephyp _ _ _ _ (riffimp _ _ _ H) H0).
 Qed.

(* ☳-contraposition ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem iffcontrapp:forall (G:☐)(P P':☊), G⊣P☳P'->G⊣¬P☳¬P'.
 Proof.
  intros G P P' H; apply (mp _ _ (¬P☳¬P') H); BTprop.
 Qed.

 Theorem iffcontrapn:forall (G:☐)(P P':☊), G⊣¬P☳¬P'->G⊣P☳P'.
 Proof.
  intros G P P' H; apply (mp _ _ (P☳P') H); BTprop.
 Qed.

(* ☳-version of inference rules ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem bieqs:forall (G:☐)(S T:☌), G⊣((S∈ ☬T)∧(T∈ ☬S))☳(S♃T).
 Proof.
  intros G S T; destruct (exfgnewf (G☑★S♃T)); destruct (ginfinc _ _ _ g); clear g;
   invset_clear p; BTprop.
   apply bieqsi; BTprop.
   replace (S∈ ☬T) with 〈x♇☍S@☋χ(x)∈ ☬T〉.
    apply (bieque (G☑★S♃T) x (χ(x)∈ ☬T) T S).
     apply symequ; BTprop.
     simpl; rewrite reflfiequ; rewrite (feaffexpnul _ T _ H0); apply biemp; apply bipowi2;
      intros i; BTprop.
     simpl; rewrite reflfiequ; rewrite (feaffexpnul _ S _ H0); apply refl_equal.
   replace (T∈ ☬S) with 〈x♇☍T@☋χ(x)∈ ☬S〉.
    apply (bieque (G☑★S♃T) x (χ(x)∈ ☬S) S T).
     BTprop.
     simpl; rewrite reflfiequ; rewrite (feaffexpnul _ S _ H); apply biemp; apply bipowi2;
      intros i; BTprop.
     simpl; rewrite reflfiequ; rewrite (feaffexpnul _ T _ H); apply refl_equal.
 Qed.

(* ☬☢(☲)-conversion into ☲ ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem bfimpimp:forall (G:☐)(v:☈)(P P':☊), G⊣ ☬☢(v∙P☲P')->G⊣P☲P'.
 Proof.
  intros G v P P' H; apply bifore2 with (v:=v); apply H.
 Qed.
 (* Nota: using this theorem, all previous ☲-replacement rules for propositional          *)
 (* calculus can be extended to ☬☢(☲)-replacement rules.                                                          *)

(* ☬☢(☲)-replacement rules ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem bfimprepaff:forall (G:☐)(v:☈)(E:☌)(P P':☊), G⊣ ☬☢(v∙P☲P')->G⊣ 〈v♇☍E@☋P〉->G⊣ 〈v♇☍E@☋P'〉.
 Proof.
  intros G v E P P' H H0; apply (mp _ _ 〈v♇☍E@☋P'〉 H0); apply (bifore _ _ _ E H).
 Qed.
 (* Nota: G⊣P☲P'->G⊣[v♇☍E@☋P]->G⊣[v♇☍E@☋P'] is refutable, as shown by the example             *)
 (* x♃2☑★y>x⊣x>3☲y>3 and x♃2☑★y>x⊣[x♇4@x>3] but x♃2☑★y>x⊣[x♇4@y>3] reduces to 2>3.            *)

 Theorem bfimprepbf:forall (G:☐)(v:☈)(P P':☊), G⊣ ☬☢(v∙P☲P')->G⊣ ☬☢(v∙P)->G⊣ ☬☢(v∙P').
 Proof.
  intros G v P P' H H0; destruct (exfgnewf (G☑★P')); apply (bifori2 _ v _ _ g);
   apply (bfimprepaff _ _ (χ x) _ _ H); apply (bifore _ _ _ (χ x) H0).
 Qed.

 Theorem bfimprepbc:forall (G:☐)(v:☈)(S:☌)(P P':☊), G⊣ ☬☢(v∙P☲P')->G⊣ ☬☤(v∙S∙P)⊆ ☬☤(v∙S∙P').
 Proof.
  intros G v S P P' H; unfold pinc; apply bipowi2; intros i; apply biimpi; apply bicmpi.
   apply bicmpl with (f:=v)(P:=P); BTprop.
   apply mp with (P:=〈v♇☍χ(i)@☋P〉).
    apply bicmpr with (S:=S); BTprop.
    apply biins; apply (bifore _ v _ χ(i) H).
 Qed.

 Theorem bfimprepimpbf:forall (G:☐)(v:☈)(P P':☊), G⊣ ☬☢(v∙P☲P')->G⊣ ☬☢(v∙P) ☲ ☬☢(v∙P').
 Proof.
  intros G v P P' H; apply biimpi; apply forallcb; intros w; apply mp with (P:=〈v♇☍χ(w)@☋P〉).
   apply bifore; BTprop.
   apply biins; apply (bifore _ v _ χ(w) H).
 Qed.

(* ☬☢(☳)-conversion into ☬☢(☲) ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem bfiffbfimp:forall (G:☐)(v:☈)(P P':☊), G⊣ ☬☢(v∙P☳P')->G⊣ ☬☢(v∙P☲P').
 Proof.
  intros G v P P' H; destruct (exfgnewf (G ☑★ P☲P')); apply (bifori2 _ v _ _ g);
   apply (diffimp _ _ _ (bifore _ _ _ (χ x) H)).
 Qed.

(* ☬☢(☳)-symmetry ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem bfiffsym:forall (G:☐)(v:☈)(P P':☊), G⊣ ☬☢(v∙P☳P')->G⊣ ☬☢(v∙P'☳P).
 Proof.
  intros G v P P' H; destruct (exfgnewf (G☑★(P'☳P))); apply (bifori2 _ v _ _ g);
   apply (symiff _ _ _ (bifore _ _ _ (χ x) H)).
 Qed.

(* ☬☢(☳)-replacement rules ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem bfiffrepiffbf:forall (G:☐)(v:☈)(P P':☊), G⊣ ☬☢(v∙P☳P')->G⊣ ☬☢(v∙P) ☳ ☬☢(v∙P').
 Proof.
  intros G v P P' H; BTprop.
   apply bfimprepbf with (P:=P); [apply bfiffbfimp; apply biins; apply H | BTprop].
   apply bfimprepbf with (P:=P');
    [apply bfiffbfimp; apply biins; apply (bfiffsym _ _ _ _ H) | BTprop].
 Qed.

 Theorem bfiffrepeqs:forall (G:☐)(v:☈)(S:☌)(P P':☊), G⊣ ☬☢(v∙P☳P')->G⊣ ☬☤(v∙S∙P)♃ ☬☤(v∙S∙P').
 Proof.
  intros G v S P P' H; apply bieqsi;
   [apply (bfimprepbc G v S P P' (bfiffbfimp _ _ _ _ H))
   | apply (bfimprepbc G v S P' P (bfiffbfimp _ _ _ _ (bfiffsym _ _ _ _ H)))].
 Qed.

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)
