(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter1.
Require Import Bbasic.
Require Import Bderived_constructs.
Require Import Brelations.

Open Scope eB_scope.

(**
  property 2.6.10, BBook: section 2.6.4, p.114, array 1, row 1
  *)
Theorem Equality_laws_override_01: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)) (r: Ensemble (S*T)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t) ∧ r ∈ (s ↔ t) ) ⇒(p ⥷ (q ⥷ r) = (p ⥷ q) ⥷ r).
Proof.
intros S T s t p q r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

inversion H0.
induction H1.
constructor.
left.
split.
constructor.
left; split; [ intuition | ].
intros H3; apply (proj2 H1).
induction H3.
inversion H3.
constructor.
exists x0.
constructor.
left.
split; [ assumption | ].
intros H5; induction H5.
inversion H5.
apply (proj2 H1).
constructor; exists x1.
constructor; right; assumption.
intros H3; apply (proj2 H1).
induction H3; inversion H3.
constructor; exists x0.
constructor; right; assumption.
induction H1.
induction H1.
constructor.
left.
split; [ | intuition ].
constructor; right; intuition.
constructor; right; intuition.

induction H0.
induction H0.
induction H0.
inversion H0.
induction H3.
constructor.
left; split; [ intuition | ].
intros H5.
apply H1.
inversion H5.
inversion H6.
inversion H8.
induction H10.
absurd (In (domain (override q r)) x).
intros H12.
apply (proj2 H3).
constructor; exists x1; intuition.
constructor; exists x1.
constructor; left; assumption.
constructor; exists x1; assumption.
constructor; right.
constructor; left; intuition.
constructor; right.
constructor; right; assumption.
Qed.

(**
  BBook: section 2.6.4, p.114, array 1, row 2
  *)
Theorem Equality_laws_override_02: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t) ∧ domain(q) ◁ p = domain(p) ◁ q) ⇒(p ⥷ q = p ∪ q).
Proof.
intros S T s t p q H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
left; intuition.
right; intuition.

assert (In q x \/ ~(In q x)); [ apply classic | ].
induction x.
induction H1.
constructor; right; assumption.
constructor; left.
inversion H0.
inversion H0.
split; [ assumption | ].
intros H6; apply H1.
assert (In (domain_restriction q (domain p)) (a,b)).
induction H.
rewrite <- H7.
constructor; assumption.
induction H7; assumption.
split; [ assumption | ].
intros H6; apply H1.
assumption.
inversion H0.
split; [ assumption | ].
intros H6; apply H1; assumption.
contradiction.
Qed.

(**
  BBook: section 2.6.4, p.114, array 1, row 3
  *)
Theorem Equality_laws_override_03: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)), (r ∈ (s ↔ t) ) ⇒(∅ ⥷ r = r).
Proof.
intros S T s t r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
induction H0; contradiction.
assumption.

induction x.
constructor; right; assumption.
Qed.

(**
  BBook: section 2.6.4, p.114, array 1, row 4
  *)
Theorem Equality_laws_override_04: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)), (r ∈ (s ↔ t) ) ⇒(r ⥷ ∅ = r).
Proof.
intros S T s t r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0; [ intuition | contradiction ].

induction x.
constructor; left.
split; [ intuition | ].
intros H1; induction H1.
induction H1; contradiction.
Qed.

(**
  BBook: section 2.6.4, p.114, array 1, row 5
  *)
Theorem Equality_laws_override_05: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (x: S) (y: T) (v: Ensemble T), (x ∈ s ∧ v ⊆ t ∧ y ∈ t) ⇒(((Singleton (x))×v) ⥷ (Singleton (x↦y)) = (Singleton (x↦y))).
Proof.
intros S T s t x y v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros z H0.

inversion H0.
induction H1.
induction H1.
inversion H1.
induction H6.
assert (In (domain (Singleton (x,y))) x).
constructor; exists y; intuition.
contradiction.
assumption.

induction H0.
constructor; right.
intuition.
Qed.

(**
  BBook: section 2.6.4, p.114, array 1, row 6
  *)
Theorem Equality_laws_override_06: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (x: S) (y: T) (u: Ensemble S), (u ⊆ s ∧ y ∈ t ∧ x ∈ s) ⇒((u × (Singleton (y))) ⥷ (Singleton (x↦y)) = (u ∪ (Singleton (x))) × (Singleton (y))).
Proof.
intros S T s t x y u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros z H0.

induction H0.
induction H0.
induction H0.
inversion H0.
constructor.
left; assumption.
assumption.
induction H0.
constructor.
right; intuition.
intuition.

induction H0.
constructor.
assert (a=x \/ a <> x); [ apply classic | ].
induction H2.
right; induction H1; rewrite H2; intuition.
induction H0.
left.
split; [ constructor; [ assumption | intuition ] | ].
intros H3; apply H2.
inversion H3.
inversion H4.
inversion H6; reflexivity.
induction H0.
absurd (x=x); [ assumption | reflexivity ].
Qed.


Close Scope eB_scope.
