//
// pageEditeurB.h
//	environnement graphique
//


#ifndef _PAGEEDITEURB_H
#define _PAGEEDITEURB_H

#include <gtkmm.h>

class PageEditeurB : public Gtk::VBox
{
public:
	PageEditeurB();
	virtual ~PageEditeurB();

	void setFileName(const char* filename);
	void setText(const char* contenu);
	Glib::ustring getText();

	int getPageSauvegardee();
	void setPageSauvegardee(int state);
	void setFilename(const std::string& filename);
	std::string getFilename() const;
	void insererSymbole(const char * symbole);
	void paste_clipboard(Glib::RefPtr<Gtk::Clipboard>);
	void cut_clipboard(Glib::RefPtr<Gtk::Clipboard>);
	void copy_clipboard(Glib::RefPtr<Gtk::Clipboard>);

private :
	// Attributs
	// Nom du fichier contenu dans la page
	std::string _filename;
	// Flag donnant si la page a ete modifiee (1) ou non (0)
	int _modifications;
	//flag donnant si le texte a ete modifie ou non (pr sauvegarde)
	int _textHasChanged;

	// Composants graphiques
	Gtk::TextView m_TextEdit;
	Glib::RefPtr<Gtk::TextBuffer> m_bufferBEditor;
	Gtk::Statusbar m_Statusbar;
	Gtk::ScrolledWindow m_ScrWindow;

	// Fonction : fonctions liees a l'ecoute du changement dans la page
	void text_changed();
	void text_mark_set(const Gtk::TextIter& new_location, const Glib::RefPtr<Gtk::TextBuffer::Mark>& mark);
	
	// Fonction : fonctions d'ecoute sur le changement dans la page
	void rafraichirBar();
	void on_text_changed();
	void on_text_mark_set();
};

#endif //_PAGEEDITEURB_H

