IMPLEMENTATION    
	TrainComputerI

REFINES           
	TrainComputer

SEES
	SystemData, Time, SectionSets, RailSets, Bool_TYPE, Bool_TYPE_Ops,
	Section_fnc_ctx, Track_fnc_ctx, Crossover_fnc_ctx, Station_fnc_ctx,
	Nextsections_set_ctx, Tracksects_seq_ctx, Trackoffs_seq_ctx

IMPORTS       
	RailNet,
	TrainCalculations,     
	trnid_Vvar(TRAIN_ID),
	ontrack_Vvar(track),
	atoffset_Vvar(NAT),
	curspd_Vvar(NAT),
	curacc_Vvar(NAT),
	accsign_Vvar(SIGN),
	controlmode_Vvar(CSTATE),
	trainstate_Vvar(TSTATE),
	targetspeed_Vvar(NAT) /* ,
	TrainSim_file_io */

PROMOTES	
	section_speedlimit, section_length, section_gradient, 
	merge_section, next_section, start_section, new_station, extend_track,
	new_track, restore_railnet, save_railnet, track_query, 
	disconnect_cross, connect_cross, add_crossover,  cross_stat_query, 
	cross_from_query, cross_to_query, section_offset_query, next_section_query, 
	nextsection_exists_query, this_section_query, track_length_query, convert_offset,
	more_sections_query, section_exists_query, any_section, more_tracks_query, 
	track_exists_query, any_track, extend_track_query, more_stations_query, 
	any_station, more_crossovers_query, any_crossover, same_dir_query, tracksiz_query, 
	cross_exists_query, track_section_query, stat_exists_query,
	cross_follows_query, connect_cross_query, dc_connect_crosses, new_train_position

INVARIANT         
	trn_id = trnid_Vvar &
	on_track = ontrack_Vvar &
	at_offset = atoffset_Vvar &
	cur_spd = curspd_Vvar &
	cur_acc = curacc_Vvar &
	acc_sign = accsign_Vvar &
	control_mode = controlmode_Vvar &
	train_state = trainstate_Vvar &
	train_target_speed = targetspeed_Vvar 

INITIALISATION    
	controlmode_STO_VAR(begin);
	trainstate_STO_VAR(new_status);
	targetspeed_STO_VAR(0);
	curspd_STO_VAR(0);
	curacc_STO_VAR(0);
	accsign_STO_VAR(zero);
	atoffset_STO_VAR(0)
	/* not sure how to handle trnid and ontrack */
	/* perhaps assign them to any values */  	
	
OPERATIONS

/*
	save = 
	BEGIN
		
		trnid_SAV_VAR;
		ontrack_SAV_VAR;
		atoffset_SAV_VAR;
		curspd_SAV_VAR;
		curacc_SAV_VAR;
		accsign_SAV_VAR;
		controlmode_SAV_VAR;
		trainstate_SAV_VAR;
		targetspeed_SAV_VAR
		
	END; 

	restore = 
	BEGIN
		
		trnid_RST_VAR;
		ontrack_RST_VAR;
		atoffset_RST_VAR;
		curspd_RST_VAR;
		curacc_RST_VAR;
		accsign_RST_VAR;
		controlmode_RST_VAR;
		trainstate_RST_VAR;
		targetspeed_RST_VAR
		
	END;
*/

	send_trk,m_acc,s_acc <-- request_init(trk) = 
  	    BEGIN
		send_trk := trk;
		m_acc := max_train_acc;
		s_acc := stand_train_acc;
		atoffset_STO_VAR(0);
		ontrack_STO_VAR(trk);
		controlmode_STO_VAR(init_pending)
	    END; 
	

	init_train(idd) = 
	    BEGIN
		trnid_STO_VAR(idd);	
		controlmode_STO_VAR(manual)
	    END;
		 
	state_update(tme) = 	
	VAR trainstate, controlmode, accsign, curspd, curacc, 
	    ontrack, atoffset, targetspeed, dist, curspdx, 
	    time_to_zero_curacc, time_to_zero_maxacc, time_to_target, const_speed_dist,
	    accelerating_dist, deaccelerating_dist_curacc, deaccelerating_dist_maxacc, 
	    dist_to_zero_curacc, dist_to_zero_maxacc, dist_to_target  IN

		trainstate <-- trainstate_VAL_VAR;
		controlmode <-- controlmode_VAL_VAR;
		accsign <-- accsign_VAL_VAR;
		curspd <-- curspd_VAL_VAR;
		curacc <-- curacc_VAL_VAR;
		ontrack <-- ontrack_VAL_VAR;
		atoffset <-- atoffset_VAL_VAR;
		targetspeed <-- targetspeed_VAL_VAR;
		
		IF controlmode = manual THEN
			IF accsign = positive THEN 
				curspdx <-- calc_plus_times(curspd,curacc,tme);
				IF curspdx >= targetspeed THEN 
					curspd_STO_VAR(targetspeed);
					curacc_STO_VAR(0);
					accsign_STO_VAR(zero);
					IF curspd <= targetspeed THEN
					    time_to_target <-- calc_time(targetspeed,curspd, curacc);
					    IF time_to_target <= tme THEN
						const_speed_dist <-- calc_dist(targetspeed, tme, time_to_target);
					  	dist_to_target <-- calc_accelerating_dist(curacc, curspd, time_to_target);
						ontrack, atoffset <--
							new_train_position(ontrack, atoffset, dist_to_target + const_speed_dist);
						ontrack_STO_VAR(ontrack);
						atoffset_STO_VAR(atoffset)
					    ELSE
						ontrack, atoffset <-- 
							new_train_position(ontrack, atoffset, targetspeed * tme)
					    END
					END 
				ELSE 
					accelerating_dist <-- calc_accelerating_dist(curacc, curspd, tme);
					curspd_STO_VAR(curspdx);
					ontrack, atoffset <-- 
						new_train_position(ontrack,atoffset, accelerating_dist);
					ontrack_STO_VAR(ontrack);
					atoffset_STO_VAR(atoffset) 
				END 
			ELSIF accsign = negative THEN
				IF curspd >= curacc * tme  THEN  
					deaccelerating_dist_curacc <-- calc_deaccelerating_dist(curacc, curspd, tme);
					curspd <-- calc_minus_times(curspd,curacc, tme);
					curspd_STO_VAR(curspd);
					ontrack, atoffset <-- 
					new_train_position(ontrack,atoffset, deaccelerating_dist_curacc); 
					ontrack_STO_VAR(ontrack);
					atoffset_STO_VAR(atoffset) 
				ELSE
					time_to_zero_curacc <-- calc_time(curspd, 0, curacc);
					dist_to_zero_curacc <--	calc_deaccelerating_dist(curacc, curspd, time_to_zero_curacc); 
					curacc_STO_VAR(0);
					accsign_STO_VAR(zero);
					curspd_STO_VAR(0);
					ontrack, atoffset <-- 
					new_train_position(ontrack,atoffset, dist_to_zero_curacc);
					ontrack_STO_VAR(ontrack);
					atoffset_STO_VAR(atoffset) 
				END
			ELSE 
				dist <-- calc_plus_times(0,curspd, tme);
				ontrack, atoffset <-- 
					new_train_position(ontrack,atoffset,dist);
				ontrack_STO_VAR(ontrack);
				atoffset_STO_VAR(atoffset)  
			END
		ELSIF controlmode = deacc_override THEN
			IF curspd >= max_train_acc * tme THEN
				deaccelerating_dist_maxacc  <-- calc_deaccelerating_dist(max_train_acc, curspd, tme); 
				curspd <-- calc_minus_times(curspd, max_train_acc, tme);
				curspd_STO_VAR(curspd);	
				accsign_STO_VAR(negative);
				curacc_STO_VAR(max_train_acc);
				ontrack, atoffset <-- 
					new_train_position(ontrack,atoffset, deaccelerating_dist_maxacc);
				ontrack_STO_VAR(ontrack);
				atoffset_STO_VAR(atoffset)  
			ELSE 
				time_to_zero_maxacc <-- calc_time(curspd, 0 , max_train_acc);
				dist_to_zero_maxacc <-- calc_deaccelerating_dist(max_train_acc, curspd, time_to_zero_maxacc);
				curspd_STO_VAR(0);
				curacc_STO_VAR(0);
				accsign_STO_VAR(zero);
				ontrack, atoffset <-- 
					new_train_position(ontrack,atoffset, dist_to_zero_maxacc);
				ontrack_STO_VAR(ontrack);
				atoffset_STO_VAR(atoffset)
			END 
		END 
	END;  

  	receive_instruction(mode,spd) = 
	    BEGIN 
		trainstate_STO_VAR(received_instruction);
		controlmode_STO_VAR(mode);
		targetspeed_STO_VAR(spd); 
  		IF mode = deacc_override THEN 
			curacc_STO_VAR(max_train_acc);
			accsign_STO_VAR(negative)
		END 				
	    END;

  	new_acc,new_sgn <-- computer_change_acc(acc,sgn) = 
	VAR cont_mode IN 
		cont_mode <-- controlmode_VAL_VAR;
		IF cont_mode = manual THEN 
			new_acc := acc;	
			new_sgn := sgn;
			curacc_STO_VAR(acc);
			accsign_STO_VAR(sgn) 
		ELSIF cont_mode = deacc_override THEN
			new_acc := max_train_acc;	
			new_sgn := negative;
			curacc_STO_VAR(max_train_acc);
			accsign_STO_VAR(negative)
		ELSE
			new_acc := 0;
			new_sgn := zero
		END 
	END;

	spd <-- return_speed =
	  BEGIN
		spd <-- curspd_VAL_VAR
	  END;

	off <-- return_offset =
	  BEGIN
		off <-- atoffset_VAL_VAR
	  END;
	
	trk <-- return_track =
	  BEGIN
		trk <-- ontrack_VAL_VAR
	  END;

	acc <-- return_acc =
	  BEGIN
		acc <-- curacc_VAL_VAR
	  END;

	accsign <-- return_acc_sgn =
	  BEGIN
		accsign <-- accsign_VAL_VAR
	  END;

	cm <-- return_control_mode =
	  BEGIN
		cm <-- controlmode_VAL_VAR
	  END;

	/*
	The following are operations added recently by Hong Chen, they are used 
	to check preconditions. 
	*/	

	ok <-- valid_offset_query(trk, off) = 
	  VAR trkleng IN
	    trkleng <-- track_length_query(trk);
	    IF off <= trkleng THEN
		ok := TRUE
	    ELSE
		ok := FALSE
	    END
	  END;

	ok <-- add_crossover_query(trk, off, end) =
	  VAR thissect IN
	    thissect <-- this_section_query(trk, off);
	    ok <-- track_section_query(end, thissect)
	  END;
	
	ok <-- control_mode_query(state) =
	  BEGIN
		ok <-- controlmode_EQL_VAR(state)
	  END

END
