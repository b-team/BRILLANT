(*==================================================================================================
  BiCoq3L
  Developed for Coq 8.2, using UTF-8 and the Everson Mono Unicode font
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/ANSSI & UPMC/LIP6 - St: Jun 2008, Md: Oct 2008 (approximately)
  Provided under licence CeCILL-B
  ================================================================================================*)

This is the source code of BiCoq3, a deep embedding of B in Coq. The final L indicates that it is
based in de Bruijn levels.

This version is a variant of BiCoq3I, exploring the differences between de Bruijn indexes and de
Bruijn levels. It is not as complete as BiCoq3I and is considered deprecated, indexes having more
interesting characteristics.

The file make.txt describes the dependencies between the files.
