(*
    Copyright (c) 2002-2009 Samuel Colin

    This file is part of a double implementation of duration calculus
    in Coq as a shallow embedding and a deep embedding, which we shall
    refer to collectively as DC-coq.

    DC-coq is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DC-coq is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DC-coq.  If not, see <http://www.gnu.org/licenses/>.
*)


Require Import IL_base.
Require Import IL_axioms.
Require Import IL_theorems.
Require Import DC_base.
Require Import DC_axioms.
Require Import Common_functions.
Require Import Classical.

Theorem DC1 :
 forall P : DCState, point \/ chop True (pr P) \/ chop True (pr (SNot P)).
intro P.
set (phi := point \/ chop True (pr P) \/ chop True (pr (SNot P))) in *.
cut (always (True -> phi)).
 intro alwTruephi.
 cut (True -> phi); [ tauto | apply always_imp_present; assumption ].
apply IR1_simple with (S := P).
 apply present_imp_always; intros; left; assumption.
 
 intros X alwXphi.
 apply always_distr_implies with (A := X -> phi); [ idtac | assumption ].
 apply present_imp_always.
 intros Xphi XPNP.
 elim XPNP; [ tauto | intro sxornotsx; elim sxornotsx ]. 
 intro XP; right; left; Monotony; tauto.
 intro XNP; right; right; Monotony; tauto.
Qed.

Theorem DC2 :
 forall P : DCState, point \/ chop (pr P) True \/ chop (pr (SNot P)) True.
intro P.
set (phi := point \/ chop (pr P) True \/ chop (pr (SNot P)) True) in *.
cut (always (True -> phi)).
 intro alwTruephi.
 cut (True -> phi); [ tauto | apply always_imp_present; assumption ].
apply IR2_simple with (S := P).
 apply present_imp_always; intros; left; assumption.
 
 intros X alwXphi.
 apply always_distr_implies with (A := X -> phi); [ idtac | assumption ].
 apply present_imp_always.
 intros Xphi XPNP.
 elim XPNP; [ tauto | intro sxornotsx; elim sxornotsx ]. 
 intro XP; right; left; Monotony; tauto.
 intro XNP; right; right; Monotony; tauto.
Qed.

Open Scope states_scope.

Theorem DC3a: forall (S:DCState) (x y:R), #S#=x /\ #~S#=y -> l=x+y.
intros S x y H.
elim H; intros.
elim DCA2 with (x+y)%R.
intros.
apply H2.



Theorem DC3 : forall S : DCState, (dur S + dur (SNot S))%R = l.
intro S.
replace l with (dur SOne); [ idtac | apply DCA2 ].
replace (dur S + dur (SNot S))%R with
 (dur (SOr S (SNot S)) + dur (SAnd S (SNot S)))%R;
 [ idtac | symmetry  in |- *; apply DCA4 ].
replace (dur (SAnd S (SNot S))) with (dur SZero);
 [ idtac | apply DCA6; simpl in |- *; tauto ].
replace (dur SZero) with 0%R; [ idtac | symmetry  in |- *; apply DCA1 ].
replace (dur (SOr S (SNot S))) with (dur SOne).
 apply Rplus_0_r.
 apply DCA6; simpl in |- *; split; [ intros; apply classic; tauto | tauto ].
Qed.

Theorem max_len : forall P : DCState, (dur P <= l)%R.
intro P.
replace l with (dur P + dur (SNot P))%R; [ idtac | apply DC3 ].
cut (dur (SNot P) >= 0)%R; [ intro durpos | apply DCA3 ].
apply plus_le_is_le with (dur (SNot P)).
 apply Rge_le; assumption.
 apply Req_le; reflexivity.
Qed.

Definition DC4 := max_len.

Theorem DC5 : forall P Q : DCState, (dur P >= dur (SAnd P Q))%R.
intros P Q.
apply Rle_ge.
apply Rplus_le_reg_l with (dur (SNot P)).
replace (dur (SNot P) + dur P)%R with l;
 [ idtac | symmetry  in |- *; ring; apply DC3 ].
replace (dur (SNot P) + dur (SAnd P Q))%R with
 (dur (SOr (SNot P) (SAnd P Q)) + dur (SAnd (SNot P) (SAnd P Q)))%R;
 [ idtac | symmetry  in |- *; apply DCA4 ].
replace (dur (SAnd (SNot P) (SAnd P Q))) with (dur SZero);
 [ idtac | apply DCA6; simpl in |- *; tauto ].
replace (dur SZero) with 0%R; [ idtac | symmetry  in |- *; apply DCA1 ].
ring (dur (SOr (SNot P) (SAnd P Q)) + 0)%R.
apply max_len.
Qed.

Theorem dur_negation : forall P : DCState, dur (SNot P) = l -> dur P = 0%R.
intros P nPlen.
apply Rplus_eq_reg_l with (dur (SNot P)).
ring (dur (SNot P) + 0)%R.
replace (dur (SNot P) + dur P)%R with (dur P + dur (SNot P))%R;
 [ idtac | ring ].
replace (dur P + dur (SNot P))%R with l;
 [ idtac | symmetry  in |- *; apply DC3 ].
symmetry  in |- *; assumption.
Qed.

Theorem dur_implies :
 forall P Q : DCState, dur (SImply P Q) = l -> (dur P <= dur Q)%R.
intros P Q durPiQl.
cut
 (dur P = (dur Q - dur (SAnd (SNot P) Q))%R /\ (dur (SAnd (SNot P) Q) >= 0)%R).
 intro PQlehyps; elim PQlehyps.
 intros Peqsum nPQpos. 
 rewrite Peqsum.
 replace (dur Q) with (dur Q + 0)%R;
  [ idtac | ring (dur Q + 0)%R; reflexivity ].
 replace (dur Q + 0 - dur (SAnd (SNot P) Q))%R with
  (dur Q + - dur (SAnd (SNot P) Q))%R;
  [ idtac
  | ring (dur Q + 0 - dur (SAnd (SNot P) Q))%R
        (dur Q + - dur (SAnd (SNot P) Q))%R; reflexivity ].
 apply Rplus_le_compat_l.
 replace 0%R with (-0)%R; [ idtac | ring (-0)%R; reflexivity ].
 apply Ropp_ge_le_contravar; assumption.
split; [ idtac | apply DCA3 ].
apply Rplus_eq_reg_l with (dur (SNot P)).
replace (dur (SNot P) + dur P)%R with l;
 [ idtac | symmetry  in |- *; ring; apply DC3 ].
replace (dur (SNot P) + (dur Q - dur (SAnd (SNot P) Q)))%R with
 (dur (SOr (SNot P) Q)).
 replace (dur (SOr (SNot P) Q)) with (dur (SImply P Q));
  [ symmetry  in |- *; assumption | idtac ].
 replace (SImply P Q) with (SOr (SNot P) Q);
  [ reflexivity | symmetry  in |- *; apply SImply_def ].
apply Rplus_eq_reg_l with (dur (SAnd (SNot P) Q)).
ring
    (dur (SAnd (SNot P) Q) + (dur (SNot P) + (dur Q - dur (SAnd (SNot P) Q))))%R.
replace (dur (SAnd (SNot P) Q) + dur (SOr (SNot P) Q))%R with
 (dur (SOr (SNot P) Q) + dur (SAnd (SNot P) Q))%R; 
 [ idtac | ring ].
replace (dur Q + dur (SNot P))%R with (dur (SNot P) + dur Q)%R;
 [ idtac | ring ].
symmetry  in |- *; apply DCA4.
Qed.

Theorem dur_and :
 forall P Q : DCState, dur (SAnd P Q) = l <-> dur P = l /\ dur Q = l.
intros P Q. 
split.
intro pql; split.
 apply Rle_antisym.
 apply max_len.
 apply Rle_trans with (dur (SAnd P Q));
  [ right; symmetry  in |- *; assumption | idtac ].
 apply dur_implies.
 replace l with (dur SOne); [ idtac | apply DCA2 ].
 apply DCA6; simpl in |- *; tauto.

 apply Rle_antisym.
 apply max_len.
 apply Rle_trans with (dur (SAnd P Q));
  [ right; symmetry  in |- *; assumption | idtac ].
 apply dur_implies.
 replace l with (dur SOne); [ idtac | apply DCA2 ].
 apply DCA6; simpl in |- *; tauto.
intro plaql; elim plaql; clear plaql; intros pl ql.
cut (dur (SAnd P Q) = l /\ dur (SOr P Q) = l); [ tauto | idtac ].
cut ((dur (SAnd P Q) + dur (SOr P Q))%R = (l + l)%R).
 intro PaQPoQ2l.
 split.
  apply Rle_antisym.
  apply max_len.
  replace (dur (SAnd P Q)) with (l + l + - dur (SOr P Q))%R.
  replace l with (l + 0)%R;
   [ ring (l + 0 + (l + 0) + - dur (SOr P Q))%R | ring ].
  ring (- dur (SOr P Q) + (l + l))%R.
  apply Rplus_le_compat_l.
  apply Rplus_le_reg_l with (dur (SOr P Q)).
  ring (dur (SOr P Q) + 0)%R (dur (SOr P Q) + (l + - dur (SOr P Q)))%R.
  apply max_len.
  apply Rplus_eq_reg_l with (dur (SOr P Q)); ring; symmetry  in |- *;
   assumption.
  apply Rle_antisym.
  apply max_len.
  replace (dur (SOr P Q)) with (l + l + - dur (SAnd P Q))%R.
  replace l with (l + 0)%R;
   [ ring (l + 0 + (l + 0) + - dur (SAnd P Q))%R | ring ].
  ring (- dur (SAnd P Q) + (l + l))%R.
  apply Rplus_le_compat_l.
  apply Rplus_le_reg_l with (dur (SAnd P Q)).
  ring (dur (SAnd P Q) + 0)%R (dur (SAnd P Q) + (l + - dur (SAnd P Q)))%R.
  apply max_len.
  apply Rplus_eq_reg_l with (dur (SAnd P Q));
   ring (dur (SAnd P Q) + (l + l + - dur (SAnd P Q)))%R; 
   symmetry  in |- *; assumption.
 replace (dur (SAnd P Q) + dur (SOr P Q))%R with (dur P + dur Q)%R;
  [ idtac | ring (dur (SAnd P Q) + dur (SOr P Q))%R; apply DCA4 ].
 rewrite pl; rewrite ql; reflexivity.
Qed.

Theorem dur_or :
 forall P Q : DCState, dur (SOr P Q) = l -> (dur P + dur Q >= l)%R.
intros P Q PoQl.
apply Rge_trans with (dur (SOr P Q)).
 replace (dur P + dur Q)%R with (dur (SOr P Q) + dur (SAnd P Q))%R.
 replace (dur (SOr P Q)) with (dur (SOr P Q) + 0)%R;
  [ ring (dur (SOr P Q) + 0 + dur (SAnd P Q))%R | ring ];
  ring (dur (SAnd P Q) + dur (SOr P Q))%R.
 apply Rle_ge; apply Rplus_le_compat_l.
 apply Rge_le; apply DCA3.
 symmetry  in |- *; apply DCA4.
right; assumption.
Qed.

Theorem dur_equivalence :
 forall P Q : DCState, dur (SIff P Q) = l -> dur P = dur Q.
intros P Q PeqQl.
cut
 ( (dur (SAnd (SImply P Q) (SImply Q P))) = l <->
   (dur (SImply P Q)) = l /\ (dur (SImply Q P)) = l ); [ idtac | apply dur_and ].
intro bigequiv; elim bigequiv; clear bigequiv.
intros deq_eqd eqd_deq; clear eqd_deq.
cut (dur (SImply P Q) = l /\ dur (SImply Q P) = l).
 intro piq_qip; elim piq_qip; clear piq_qip; intros piq qip.
 apply Rle_antisym;
  [ apply dur_implies; assumption | apply dur_implies; assumption ].
apply deq_eqd.
rewrite <- PeqQl.
apply DCA6; simpl in |- *; tauto.
Qed.


Theorem DC10 : forall P Q : DCState, pr P /\ pr Q <-> pr (SAnd P Q).
intros P Q; split.
intros dPdQ; elim dPdQ; clear dPdQ; unfold pr in |- *; intros dP dQ. 
elim dP; elim dQ; clear dP dQ; intros durQ lpos1 durP lpos2.
split.
 cut (dur (SAnd P Q) = l <-> dur P = l /\ dur Q = l);
  [ idtac | apply dur_and ].
 intro durand; elim durand; clear durand; intros dPQidPdQ dPdQidPQ.
 apply dPdQidPQ; split; [ assumption | assumption ].
 assumption.
unfold pr in |- *; intro dPQ; elim dPQ; clear dPQ; intros dPQl lpos.
cut (dur P = l /\ dur Q = l).
 intro PlQl; elim PlQl; clear PlQl; intros Pl Ql.
 split;
  [ split; [ assumption | assumption ] | split; [ assumption | assumption ] ].
cut (dur (SAnd P Q) = l <-> dur P = l /\ dur Q = l);
 [ idtac | apply dur_and ].
intro durand; elim durand; clear durand; intros dPQidPdQ dPdQidPQ.
apply dPQidPdQ; assumption.
Qed.