/* ----------------------------
 * Copyright (c) 2004 - INRETS
 * ----------------------------
 *  Project: 	RLC
 *  Created:	Wed Feb 11 11:32:21 2004
 *  Author: 	Rafael MARCANO
 */
MACHINE Barrier

USES BarrierSensor

SETS BARRIER

VARIABLES
    barrier, bState
INVARIANT
    barrier <: BARRIER &
    bState : barrier --> bSTATE
INITIALISATION
    barrier,bState := {},{}
OPERATIONS

    openBarrier(obj) =
    PRE
	obj:barrier &
	bState(obj)=Closed
    THEN
	bState(obj):=Opened
    END;

    closeBarrier(obj) =
    PRE
	obj:barrier &
	bState(obj)=Opened
    THEN
	bState(obj):=Closed
    END;
    	
    setBState(obj,newBState) =
    PRE
	obj : barrier &
	newBState : bSTATE
    THEN
	bState := bState <+ {obj|->newBState}
    END;

    res<--getBState(obj) =
    PRE obj : barrier THEN
	res:=bState(obj)
    END;
        
    obj<--createBarrier =
    PRE
	barrier/=BARRIER
    THEN
	ANY new WHERE new:BARRIER-barrier	THEN
	    barrier :=barrier \/ {new} ||
	    bState := bState \/ {(new|->Opened)} ||
	    obj:=new
	END
    END;
    
    supBarrier(obj) =
    PRE
	obj <: barrier
    THEN
	barrier := barrier - obj ||
	bState := obj <<| bState
    END
END
    