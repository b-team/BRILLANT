#!/usr/bin/make -f

BIBFILE = Bmethod

OUT_DIR=output

BOOK_DIR=$(OUT_DIR)/books
AUTHOR_DIR=$(OUT_DIR)/authors
YEAR_DIR=$(OUT_DIR)/years
ONLINE_DIR=$(OUT_DIR)/online
KEYWORD_DIR=$(OUT_DIR)/keywords

hascleangoals:=$(if $(sort $(filter clean,$(MAKECMDGOALS))),1)

ECHO=@
#ECHO=

define keys
  $(patsubst %,%.keys,$(1))
endef

define crossrefs
$(shell scripts/crossrefs $(1))
endef

define years
$(shell scripts/years $(1))
endef

BOOKS=$(call crossrefs,$(BIBFILE).bib)
BOOKS_FILES=$(patsubst %,$(BOOK_DIR)/$(BIBFILE)-%.bib,$(BOOKS))
BOOKS_KEYS=$(patsubst %,$(BOOK_DIR)/%,$(call keys,$(BOOKS)))
BOOKS_HTML=$(patsubst %.bib,%.html,$(BOOKS_FILES))
BOOKS_BIBHTML=$(patsubst %.bib,%_bib.html,$(BOOKS_FILES))


# We exploit the fact that make can remake its included files for
# building the authors list. authors.d ensures the existence of
# $(BIBFILE).authors
ifeq "$(hascleangoals)" ""
-include authors.d
-include keywords.d
endif

AUTHORS=$(call getfilenames,$(BIBFILE).authors)
AUTHORS_FILES=$(patsubst %,$(AUTHOR_DIR)/$(BIBFILE)-%.bib,$(AUTHORS))
AUTHORS_KEYS=$(patsubst %,$(AUTHOR_DIR)/%,$(call keys,$(AUTHORS)))
AUTHORS_HTML=$(patsubst %.bib,%.html,$(AUTHORS_FILES))
AUTHORS_BIBHTML=$(patsubst %.bib,%_bib.html,$(AUTHORS_FILES))

YEARS=$(call years,$(BIBFILE).bib)
YEARS_FILES=$(patsubst %,$(YEAR_DIR)/$(BIBFILE)-%.bib,$(YEARS))
YEARS_KEYS=$(patsubst %,$(YEAR_DIR)/%,$(call keys,$(YEARS)))
YEARS_HTML=$(patsubst %.bib,%.html,$(YEARS_FILES))
YEARS_BIBHTML=$(patsubst %.bib,%_bib.html,$(YEARS_FILES))

ONLINES=online offline
ONLINES_FILES=$(patsubst %,$(ONLINE_DIR)/$(BIBFILE)-%.bib,$(ONLINES))
ONLINES_KEYS=$(patsubst %,$(ONLINE_DIR)/%,$(call keys,$(ONLINES)))
ONLINES_HTML=$(patsubst %.bib,%.html,$(ONLINES_FILES))
ONLINES_BIBHTML=$(patsubst %.bib,%_bib.html,$(ONLINES_FILES))

KEYWORDS=$(call getfilenames,$(BIBFILE).keywords)
KEYWORDS_FILES=$(patsubst %,$(KEYWORD_DIR)/$(BIBFILE)-%.bib,$(KEYWORDS))
KEYWORDS_KEYS=$(patsubst %,$(KEYWORD_DIR)/%,$(call keys,$(KEYWORDS)))
KEYWORDS_HTML=$(patsubst %.bib,%.html,$(KEYWORDS_FILES))
KEYWORDS_BIBHTML=$(patsubst %.bib,%_bib.html,$(KEYWORDS_FILES))



# Note: these functions take the last matching name ("tail -n 1"),
# hence if there are homonyms among the authors, or people for who the
# translated filename is identical (Colin and C{\" o}l{\' \i}n), for
# instance, then only the last will appear.

# These functions are strongly dependent on the format of
# $(BIBFILE).authors, hence also on scripts/make_authors_list

# Ditto for keywords

# $(call getfield,word,<association list>,field number)
define getfield 
$(shell grep "\(^\|:\)$(1)\($$\|:\)" $(2) |
  sort |
  tail -n 1 |
  cut -f $(3) -d ':' -s 
)
endef

define getbasename
$(call getfield,$(1),$(2),1)
endef

define getfilenames
$(shell cut -f 2 -d ':' -s $(1))
endef

define getfilename
$(call getfield,$(1),$(2),2)
endef

define authorutf8name
$(call getfield,$(1),$(2),3)
endef

define authorbib2bibname
$(call getfield,$(1),$(2),4)
endef

define keywordbib2bibname
$(call getfield,$(1),$(2),3)
endef

# We want to keep all intermediate files
.SECONDARY:

help:
	$(ECHO)echo Beware, \"all\" generates a ton of files -- ok, in the \"$(OUT_DIR)\" directory, but still
	$(ECHO)echo To clean absolutely everything: \"clean\" \(recursively removes \"$(OUT_DIR)\" directory \)
	$(ECHO)echo To clean everything *but* the generated files \(*.bib, *.keys, *.html\): \"mostlyclean\"

all: books authors years onlines keywords

authors.d: $(BIBFILE).authors
	$(ECHO)touch $@

%.authors: %.bib
# We do a list of first authors only
# 	$(ECHO)echo Generating list of authors
	$(ECHO)scripts/make_authors_list first $< $@

keywords.d: $(BIBFILE).keywords
	$(ECHO)touch $@

%.keywords: %.bib
# We do a list of keywords (compound by default, see the called script)
	$(ECHO)echo Generating list of keywords
	$(ECHO)scripts/make_keywords_list $< $@


books-bib: $(BOOKS_FILES)
books-keys: $(BOOKS_KEYS)
books-html: $(BOOKS_HTML) $(BOOKS_BIBHTML)
books: books-bib books-keys books-html

authors-bib: $(AUTHORS_FILES)
authors-keys: $(AUTHORS_KEYS)
authors-html: $(AUTHORS_HTML) $(AUTHORS_BIBHTML)
authors: authors-bib authors-keys authors-html

years-bib: $(YEARS_FILES)
years-keys: $(YEARS_KEYS)
years-html: $(YEARS_HTML) $(YEARS_BIBHTML)
years: years-bib years-keys years-html

onlines-bib: $(ONLINES_FILES)
onlines-keys: $(ONLINES_KEYS)
onlines-html: $(ONLINES_HTML) $(ONLINES_BIBHTML)
onlines: onlines-bib onlines-keys onlines-html

keywords-bib: $(KEYWORDS_FILES)
keywords-keys: $(KEYWORDS_KEYS)
keywords-html: $(KEYWORDS_HTML) $(KEYWORDS_BIBHTML)
keywords: keywords-bib keywords-keys keywords-html


%.keys: $(BIBFILE)-%.bib
	$(ECHO)echo Generating keys: $* \(from $<\)
	$(ECHO)bib2bib -q -oc $@ $< >/dev/null

$(BOOK_DIR)/$(BIBFILE)-%.bib: $(BIBFILE).bib
	$(ECHO)if [ ! -d "$(dir $@)" ]; then mkdir -p "$(dir $@)"; fi
	$(ECHO)echo Generating Books: $*
	$(ECHO)bib2bib -q -ob $@ -c 'crossref : "$*"' $< >/dev/null 2>&1

$(BOOK_DIR)/$(BIBFILE)-%.html \
$(BOOK_DIR)/$(BIBFILE)-%_bib.html \
 : $(BOOK_DIR)/$(BIBFILE)-%.bib
	$(ECHO)echo Generating Books in html and _bib.html: $*
	$(ECHO)cd $(dir $@) && bibtex2html -q -t "B-Method @ $*" $(notdir $<)

$(AUTHOR_DIR)/$(BIBFILE)-%.bib: $(BIBFILE).bib
	$(ECHO)if [ ! -d "$(dir $@)" ]; then mkdir -p "$(dir $@)"; fi
	$(ECHO)echo Generating Authors: $(call authorutf8name,$*,$(BIBFILE).authors) \(from normalised form: $*\)
	$(ECHO)bib2bib -q -ob $@ -c 'author : "\(^\|and \)$(call authorbib2bibname,$*,$(BIBFILE).authors)\(,\|$$\)"' $< >/dev/null 2>&1

$(AUTHOR_DIR)/$(BIBFILE)-%.html \
$(AUTHOR_DIR)/$(BIBFILE)-%_bib.html \
 : $(AUTHOR_DIR)/$(BIBFILE)-%.bib
	$(ECHO)echo Generating Authors in html and _bib.html: $(call authorutf8name,$*,$(BIBFILE).authors) \(from normalised form: $*\)
	$(ECHO)cd $(dir $@) && bibtex2html -q -t "B-Method by $(call authorutf8name,$*,$(BIBFILE).authors)" $(notdir $<)

$(YEAR_DIR)/$(BIBFILE)-%.bib: $(BIBFILE).bib
	$(ECHO)if [ ! -d "$(dir $@)" ]; then mkdir -p "$(dir $@)"; fi
	$(ECHO)echo Generating Years: $*
	$(ECHO)bib2bib -q -ob $@ -c 'year : "$*"' $< >/dev/null  2>&1

$(YEAR_DIR)/$(BIBFILE)-%.html \
$(YEAR_DIR)/$(BIBFILE)-%_bib.html \
 : $(YEAR_DIR)/$(BIBFILE)-%.bib
	$(ECHO)echo Generating Years in html and _bib.html: $*
	$(ECHO)cd $(dir $@) && bibtex2html -q -t "B-Method in $*" $(notdir $<)

$(ONLINE_DIR)/$(BIBFILE)-%.bib: $(BIBFILE).bib
	$(ECHO)if [ ! -d "$(dir $@)" ]; then mkdir -p "$(dir $@)"; fi
	$(ECHO)echo Generating $* references
	$(ECHO)condition="(exists doi) or (exists ps) or (exists pdf) or (exists URL)";\
	case $* in \
	online) ;;\
	*) condition="not ($$condition)";;\
	esac;\
	bib2bib -q -ob $@ -c "$$condition" $< >/dev/null  2>&1

$(ONLINE_DIR)/$(BIBFILE)-%.html \
$(ONLINE_DIR)/$(BIBFILE)-%_bib.html \
 : $(ONLINE_DIR)/$(BIBFILE)-%.bib
	$(ECHO)echo Generating References in html and _bib.html: $*
	$(ECHO)cd $(dir $@) && bibtex2html -q -t "B-Method $*" $(notdir $<)

$(KEYWORD_DIR)/$(BIBFILE)-%.bib: $(BIBFILE).bib
	$(ECHO)if [ ! -d "$(dir $@)" ]; then mkdir -p "$(dir $@)"; fi
	$(ECHO)echo Generating Keywords: $(call getbasename,$*,$(BIBFILE).keywords) \(from normalised form: $*\)
	$(ECHO)bib2bib -q -ob $@ -c 'keywords : "\(^\| \)$(call keywordbib2bibname,$*,$(BIBFILE).keywords)\(,\|$$\)"' $< >/dev/null 2>&1

$(KEYWORD_DIR)/$(BIBFILE)-%.html \
$(KEYWORD_DIR)/$(BIBFILE)-%_bib.html \
 : $(KEYWORD_DIR)/$(BIBFILE)-%.bib
	$(ECHO)echo Generating Keywords in html and _bib.html: $(call getbasename,$*,$(BIBFILE).keywords) \(from normalised form: $*\)
	$(ECHO)cd $(dir $@) && bibtex2html -q -t "B-Method by $(call getbasename,$*,$(BIBFILE).keywords)" $(notdir $<)

clean: mostlyclean
	$(ECHO)-rm -rf $(OUT_DIR)

distclean: clean

mostlyclean:
	$(ECHO)-rm -f $(BIBFILE).authors authors.d
	$(ECHO)-rm -f $(BIBFILE).keywords keywords.d

maintainer-clean: distclean

# Emacs variables
# 
# Local Variables:
# mode:makefile
# End:
