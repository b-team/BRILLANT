(** Library of operation on GSL
@author Samuel Colin *)

(**This function turns B substitution to generalized <<standard>> substitution, as near as possible as in BBook
@param ids_to_avoid the set of already used IDs
@param substB the substitution to be turned
@author Samuel Colin *)
val subst_to_gsl : Id_handling.Idset.t -> Blast.substitution -> Gsl_adt.gsl

(**This function apply a GSL substitution to a predicate
@param substgsl the substitution to apply
@predicate the predicate to be substitued
@author Samuel Colin *)
val apply : Gsl_adt.gsl -> Blast.predicate -> Blast.predicate

(**Calculate the given predicate
@predicate the predicate to be calculated
@author Samuel Colin *)
val calculate : Blast.predicate -> Blast.predicate
